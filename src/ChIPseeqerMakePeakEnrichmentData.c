//
// This program returns average normalized read density data for a given set of genes
// prom, first exon, first intron, second exon, second intron, exons, introns, downtream
//  
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <search.h>
#include "hashtable.h"
#include "dataio.h"
#include "statistics.h"
#include "sequences.h"
#include <math.h>


#define MAX 0
#define RPKM 1

//#include "protos.h"
#define NUMCHROMS 25

#define min(a,b) (a<b?a:b)
#define max(a,b) (a>b?a:b)

typedef struct _GenInt {
  int   i;
  int   j;
  int   ci;
  int   cj;
  char* chr;
  char* tsname;
  char* genename;
  int   fr; // frame
  int   ef; // exon frame
  int   num; // exon number
  char  utrtype;
	
  int   mut[4];
  int   N;
  int   k;
  float score;
  char* dbsnp;
  float entropy;
  char* str;
  int** exons;
  int   numexons;
	
} GenInt;

void readFileSumUpColumnPerChrom(char* file, int col, int colchr, int** counts, int** countgenes);
void readRefGenePromoters(char* file, GenInt** a_proms, int* numproms, int up, int dn);

void QuantizeReadCountsOverInterval(int i, int j, int q, unsigned short* counts, double** qavgcounts);
void readGenesAndExonDataFromRefGene(char* file, GenInt** a_genes, int* numgenes);
void readGeneListIntoHash(char* file, struct my_hsearch_data** hash_genes);

int verbose = 0;

int main(int argc, char** argv) {
	
  // general
  long i;
	
  unsigned short*	chip_counts;
  //unsigned short*	input_counts;
	
  long			chrlen			= 0; 	
  int				fraglen         = -1;
  int				readlen         = 0;
  int				numreads_chip   = 0;
  int				numreads_input  = 0;
  int				t				= 5;
  char*			chrname			= 0;
  double			mappability		= 1.0;
  int				format			= 0; // default = 0 ... eland?
  char*			formattext		= 0;
	
  char*			intervals		= 0;
  //char*			targets			= 0;

  //int				j;
  char*           chrdata         = 0;
  char**          chrnames        = 0;
  int*			chrlens         = 0;
  float*			chrmap			= 0;
  int				numchroms		= 24;
  char*			chipdir			= 0;
	
  char			readfile[1000];
  int			normalize		= MAX;

  double			normfactor		= 0;
  int				numreads		= 0;
  int				totalnumreads	= 0;
  int				normto			= 10000000;
  int				c;
  int				genome			= 0;
  char*			inputdir		= 0;
  int				ext				= 0;
  char*			output_text		= 0;
  int				output			= 0;
	
  int				uniquereads		= 1;
  int				q				= 5; 
  double*			qavgcounts		= 0;
  GenInt*			a_genes;
  //int				numgenes;
  int				nq;
  int				promlen			= 3000;
  //int				efnq;
  char*			onlychr			        = 0;
  char*			onlyts			        = 0;
  //int				posexon			= 0;
  char*			genelist		        = 0;
  //char*			labelsALL[8]		        = {"Promoter", "Exon 1", "Intron 1", "Exon 2", "Intron 2", "Other Exons", "Other Introns", "Downstream"};
  //char*                   labelsBO[3]                     = {"Upstream", "Gene Body", "Downtream"};
  //struct			my_hsearch_data* hash_genes     = 0;
  //ENTRY			e;
  //ENTRY*			ep                              = 0;
  //int			le=0, li=0;
  int			numbins                         = 10;  // will be used for geneparts bodies (q ignored)
  int			numreadsclonal_chip          	= 0;
  //int			numreadsclonal_input	        = 0;
  char*                   geneparts                       = "all";
  //int                     numgp                           = 0;
  char*                   outdata                         = 0;
  FILE*                   fpod                            = 0;
  //achar*                   lab                             = 0;

  if (argc < 2) {
    die("Usage: ChIPseeqerMakePeakEnrichmentData -intervals FILE -chipdir DIR [ -geneparts [all|body] -numbins INT ]\n");
  }
	
  if (exist_parameter(argc, argv, "-intervals"))
    intervals = get_parameter(argc, argv, "-intervals");
	
  if (exist_parameter(argc, argv, "-genelist"))
    genelist = get_parameter(argc, argv, "-genelist");
	
  if (exist_parameter(argc, argv, "-onlychr"))
    onlychr = get_parameter(argc, argv, "-onlychr");
	
  if (exist_parameter(argc, argv, "-onlyts"))
    onlyts = get_parameter(argc, argv, "-onlyts");
	
  if (exist_parameter(argc, argv, "-q"))
    q = atoi(get_parameter(argc, argv, "-q"));
	
  if (exist_parameter(argc, argv, "-chipdir"))
    chipdir  = get_parameter(argc, argv, "-chipdir");
	
  if (exist_parameter(argc, argv, "-inputdir"))
    inputdir  = get_parameter(argc, argv, "-inputdir");
	
  if (exist_parameter(argc, argv, "-promlen"))
    promlen  = atoi(get_parameter(argc, argv, "-promlen"));
	
  if (exist_parameter(argc, argv, "-chrname"))
    chrname  = get_parameter(argc, argv, "-chrname");
	
  // type of plot
  if (exist_parameter(argc, argv, "-geneparts"))
    geneparts  = get_parameter(argc, argv, "-geneparts");
  if (exist_parameter(argc, argv, "-numbins"))
    numbins  = atoi(get_parameter(argc, argv, "-numbins"));

	

  if (exist_parameter(argc, argv, "-uniquereads"))
    uniquereads  = atoi(get_parameter(argc, argv, "-uniquereads"));
	
  if (exist_parameter(argc, argv, "-t"))
    t  = atoi(get_parameter(argc, argv, "-t"));
	
  if (exist_parameter(argc, argv, "-normalize")) {
    char* normalizetxt  = get_parameter(argc, argv, "-normalize");
    if (strcmp(normalizetxt, "rpkm") == 0)
      normalize = RPKM;
    else if (strcmp(normalizetxt, "max") == 0)
      normalize = MAX;
    else 
      die("Don't know this norm scheme\n");
  }
	
  if (exist_parameter(argc, argv, "-fraglen"))
    fraglen  = atoi(get_parameter(argc, argv, "-fraglen"));
	
  if (exist_parameter(argc, argv, "-readlen"))
    readlen  = atoi(get_parameter(argc, argv, "-readlen"));
	
  if (exist_parameter(argc, argv, "-normto"))
    normto  = atoi(get_parameter(argc, argv, "-normto"));
	
  if (exist_parameter(argc, argv, "-mappability"))
    mappability = atof(get_parameter(argc, argv, "-mappability"));
	
  if (exist_parameter(argc, argv, "-ext"))
    ext = atoi(get_parameter(argc, argv, "-ext"));
	
  if (exist_parameter(argc, argv, "-output")) {
    output_text = get_parameter(argc, argv, "-output");
    if (strcmp(output_text, "avg") == 0)
      output = 0;
    else if (strcmp(output_text, "max") == 0)
      output = 1;	  
  }
	
  if (exist_parameter(argc, argv, "-verbose"))
    verbose = atoi(get_parameter(argc, argv, "-verbose"));
	
  if (exist_parameter(argc, argv, "-format")) {
    formattext = get_parameter(argc, argv, "-format");
    if (strcmp(formattext, "mit") == 0)
      format = 1;
    else if (strcmp(formattext, "bed") == 0)
      format = 2;
    else if (strcmp(formattext, "sam") == 0)
      format = 3;
    else if (strcmp(formattext, "bowtiesam") == 0)
      format = 4;
  }
	
  if (exist_parameter(argc, argv, "-chrdata")) {
    chrdata  = get_parameter(argc, argv, "-chrdata");
    readChrData(chrdata, &chrnames, &chrlens, &chrmap, &numchroms);
    genome   = 0;
  }

  // where to store the profiles (1 per gene)
  if (exist_parameter(argc, argv, "-outdata")) {
    outdata  = get_parameter(argc, argv, "-outdata");
    fpod     = fopen(outdata, "w");
    if (!fpod) {
      printf("cannot open %s\n", outdata);
      exit(1);
    }
  }	
  if (verbose == 1) {
    printf("About to start reading intervals \n");
  }
	

  FILE* fp = fopen(intervals, "r");
  if (fp == 0) 
    die("cannot open intervals\n");
  int mynmax = 100000;

  char* buff = (char*)calloc(mynmax, sizeof(char));
  char** p;
  int m;
  int numlines	= nbLinesInFile(intervals);
  GenInt* a_int = (GenInt*)calloc(numlines, sizeof(GenInt));

  int numint = 0;
  while (fgets(buff, mynmax, fp) != 0) {
    split_line_delim(buff, "\t", &p, &m);

    a_int[numint].chr = strdup(p[0]);
    a_int[numint].i   = atoi(p[1]);
    a_int[numint].j   = atoi(p[2]);

    int l =  a_int[numint].j -  a_int[numint].i + 1;
    a_int[numint].i -= l;
    a_int[numint].j += l;

    
	
    numint++;
  }

  if (normalize == RPKM)
    fprintf(stderr, "# norm=RPKM\n");
  else 
    fprintf(stderr, "# norm=MAX\n");
	
	
  // calc total number of reads for RPKM-style (not used right now)
  if (normalize == RPKM) {
    totalnumreads = 0;
    for (c=0; c<numchroms; c++) {
      chrname = chrnames[c];
      sprintf(readfile, "%s/reads.%s", chipdir, chrname); 
      numreads = CountReads(readfile, format);
      if (verbose == 1) {
	fprintf(stderr, "Found %d aligned reads in chr %s\n", numreads, chrname);
      }
      totalnumreads += numreads;
	    
    }
    if (verbose == 1) {
      fprintf(stderr, "Found a total of %d aligned reads\n", totalnumreads);
    }
	  
    normfactor = normto / (double)totalnumreads;
    if (verbose == 1) {
      fprintf(stderr, "Normalization factor = %4.3f\n", normfactor);
    }	  
  }

  //
  // normalize
  //
  for (c=0; c<numchroms; c++) {
		
    if (chrdata == 0)
      chrname = 0; //(char*)(chroms[c]);
    else // means that chrdata was read from file
      chrname = chrnames[c];
		
    if (genome == 0) {
      chrlen      = chrlens[c];
    } else {
      die("not supportted yet\n");
    }
		
		
    if ((onlychr != 0) && (strcmp(onlychr, chrname) != 0))
      continue;
		
    if (verbose == 1)
      printf("loading data for %s\n", chrname);
		
    // alloc counte vectors
    chip_counts = (unsigned short*)calloc(chrlen, sizeof(unsigned short));
    if (chip_counts == 0) {	    
      die("Problem allocating chip_counts.\n");
    }

    // build a chr-wide read count profile
    sprintf(readfile, "%s/reads.%s", chipdir, chrname); 
    getCountFromReadFile(readfile, format, chrname, chrlen, readlen, fraglen, uniquereads, 1, &chip_counts, &numreads_chip, &numreadsclonal_chip);
		
		
    if (verbose == 1)
      fprintf(stderr, "Number of reads = %d (chip) and %d (input)\n", numreads_chip, numreads_input);
		
		
    // now go thru all genes
    for (i=0; i<numint; i++) {
			
      if ( strcmp(a_int[i].chr, chrname) != 0 )
	continue;
			
      int st = a_int[i].i;
      int en = a_int[i].j;
						
      //
      // quantize promoter (same for all geneparts, bins[0] ha been set to numbins )
      //
      QuantizeReadCountsOverInterval(st, en, numbins, chip_counts, &qavgcounts);
   
      // norma and print
      printf("%s-%d-%d", chrname, st, en);
      for (nq=0; nq<numbins; nq++) {
	printf("\t%4.3f", qavgcounts[nq] * normfactor);
      }
      printf("\n");

		
    } // loop over intervals
    
    free(chip_counts);
		
    // loop over chr
  }
	

  return 0;
	
}

void readGeneListIntoHash(char* file, struct my_hsearch_data** hash_genes)
{
	
  char*  buff;
  int    mynmax = 100000;
	
  char** a;
  int    m;
  FILE*  f;
  //int    cidx = -1;
  //int    i;
  //char*  line = 0;
  int    hashret;
  ENTRY  e;
  ENTRY* ep;
	
  //char** a_e_st;
  //char** a_e_en;
  //char** a_e_fr;
  //int    m_e_st;
  //int    m_e_en;
  //int    m_e_fr;
  //int**  a_myexons;
  //int    i, ii;
	
  // initialize num rnas per chr
  int numgenes = nbLinesInFile(file);
	
  /* build a hash table */
  *hash_genes = (struct my_hsearch_data*)calloc(1,  sizeof(struct my_hsearch_data));
  hashret = my_hcreate_r( numgenes *2, *hash_genes);
  if (hashret == 0) {
    printf("Could not create hash table ...\n");
    exit(0);
  }
	
  // read set of intervals
  buff  = (char*)malloc(mynmax * sizeof(char));  
  f = fopen(file, "r");
  if (f == 0) 
    die("Cannot open refGene file.\n");
	
  while (fgets(buff, mynmax, f) != 0) {
    chomp(buff);
		
		
    split_line_delim(buff, "\t", &a, &m);
		
    char* n      = a[0];
		
    /* query */
    e.key = n;        
    my_hsearch_r(e, FIND, &ep, *hash_genes) ;  
    if (ep) {
      /* success, already rgere */
      free(a);
      continue;
    } else {
			
      // add
      /* enter key/value pair into hash */
      e.key   = strdup( n );
      e.data  = (char*)(1);
      hashret = my_hsearch_r(e, ENTER, &ep, *hash_genes);
      if (hashret == 0) {
	printf("Could not enter entry into hash table ...\n");
	exit(0);
      }
    }
    free(a);
  }
}



//
// this function calculates 
//
void QuantizeReadCountsOverInterval(int i, int j, int q, unsigned short* counts, double** qavgcounts)
{
	
  int le = j - i + 1;
  int lq = le / q;  // floored on purpose
  int iq = 0;
  int jq = 0;
  int nq = 0;
	
  double avgcount = 0;
  int k = 0;
	
  if (j <= i) {
    die("j <= i ! shouldn't happen\n");
  }

  if (lq == 0) {
    die("lq = 0, why ?\n");
  }
	
  *qavgcounts = (double*)calloc(q, sizeof(double));  
	
  for (nq=0; nq<q; nq++) {
    iq = i + nq * lq;
    jq = iq + lq;
		
    if (j < jq)
      jq = j;
		
    // get average read count in that interval
    avgcount = 0;
    for (k=iq; k<=jq; k++) {
      avgcount += counts[k]; 
    }
    avgcount /= lq;
		
    //if (verbose == 1)
    //  printf("Avg over [%d;%d] = %f\n", iq, jq, avgcount);
		
    (*qavgcounts)[nq] = avgcount;
  }
	
}

