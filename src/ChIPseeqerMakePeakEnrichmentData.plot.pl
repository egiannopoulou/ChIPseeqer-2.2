#!/usr/bin/perl
use lib "$ENV{CHIPSEEQERDIR}/SCRIPTS/PostScript-Simple-0.07/lib";
use lib "$ENV{CHIPSEEQERDIR}";

use PostScript::Simple;
use Table;
use Sets;
use Getopt::Long;
use strict;

my $promlen          = 3000;
my $scriptdir        = "."; 
my $profiles         = undef;
my $summaryfile      = undef;
my $epsfile          = undef;
my $seqlen           = undef;
my $ps2pdf           = 1;
my $leftlabel        = 'leftlabel';
my $rightlabel       = 'rightlabel';
my $motifs_i         = undef;
my $motifs_k         = undef;
my $motifs_m         = undef;
my $clusters         = undef;
my $expfile          = undef;
my $outeps           = undef;
my $rna              = undef;
my $intersection     = 0;
my $overrep          = 0;
my $h                = 500;
my $fullmatrixfile   = undef;
my $forcea4          = 0;
my $fillup           = 0;
my $fastafile        = undef;
my $drawlegend       = 'bottom';
my $draw             = 'open';
my $genelist         = undef;
my $geneprofile      = undef;
my $argmotifname     = undef;
my $genedesc         = undef;

my $xstep            = 10;
my $w                = 5;
my $suffix           = undef;
my $ymax             = undef;
my $title            = undef;
my $targets          = undef;
my $annotation       = undef;
my $chipdir          = undef;
my $q                = 50;
my $format           = undef;
my $genome			 = "hg18";
my $chrdata			 = undef;
my $db				 = "refSeq"; # could be refSeq, AceView, UCSCGenes or Ensembl
my $getprofiles      = 1;
my $geneparts        = "all";
my $numbins          = 20;
my $normalize        = "max";
my $useprofiles      = undef;
my $plot             = 1;

if (@ARGV == 0) {
  die "Usage: ChIPseeqerPlotAverageReadDensityInGenes --chipdir=DIR [ --genelist=[all;FILE] --h=INT(500) --ymax=[0.0;1.0](0.5) --q=INT --numbins=INT --geneparts=[all|body] --promlen=INT(3000) --format=STR(eland) --normalize=STR(max) ] 
	Where:
	--chipdir=DIR       points to reads
	--genelist=STR|FILE all or file (1 col)
	--q=INT             specifies bin size (bp)
	--numbins=INT       specifies num bins (for --geneparts=body)
	--geneparts=STR     all or body
	--promlen=INT       length of flanking regions    
	--ymax=FLOAT        maximum y in plot
	--h=INT             plot height
	--normalize=STR     max or rpkm
	--getprofiles=INT   0 if just replot
	--useprofiles=FILE  use an existing db of profiles (will average out in this script)
	--plot=INT          0 = no plot
	\n";
}


GetOptions (
	    "profiles=s"    => \$profiles,
	    'useprofiles=s'	=> \$useprofiles,
	    "geneparts=s"   => \$geneparts,
	    "numbins=s"     => \$numbins,
	    'targets=s'	=> \$targets,
	    'epsfile=s'     => \$epsfile,
	    'rightlabel=s'	=> \$rightlabel,
	    'leftlabel=s'	=> \$leftlabel,
	    'seqlen=s'	=> \$seqlen,
	    'ymax=s'        => \$ymax,
	    'chipdir=s'     => \$chipdir,
	    'forcea4=s'     => \$forcea4,
	    'title=s'       => \$title,
	    'h=s'           => \$h,
	    'plot=s'        => \$plot,
	    'w=s'           => \$w,
	    'promlen=s'     => \$promlen,
	    'xstep=s'       => \$xstep,
	    'suffix=s'      => \$suffix,
	    'draw=s'        => \$draw,
	    'format=s'	=> \$format,
	    'genelist=s'	=> \$genelist,
	    'geneprofile=s'	=> \$geneprofile,
	    'motifname=s'   => \$argmotifname,
	    'q=s'           => \$q,
	    'genedesc=s'	=> \$genedesc,
	    "getprofiles=s" => \$getprofiles,
	    "genome=s"	=> \$genome,
	    "normalize=s"   => \$normalize,
	    "db=s"		=> \$db);



# AVG
open IN, $profiles or die "Cannot open $profiles\n";
my $cnt = 0;
my @sum = 0;
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  my $cgi = shift @a;
  for (my $i=0; $i<@a; $i++) {
    $sum[$i] += $a[$i];
  }
  $cnt++;
}
close IN;

my $ymax_data = 0;
foreach my $s  (@sum)   {
  $s /= $cnt;
  if ($s > $ymax_data) {
    $ymax_data = $s;
  }
}

my $numints = @sum;

my @a_data = ();
push @a_data, \@sum;

if (!defined($ymax)) {
  $ymax = 1.1 * $ymax_data;
}

my $xbase  = 100;
my $ybase  = 100;
my $xright = 150;
my $xleft  = $xbase;
my $xsize  = $xbase + $w * $numints + $xbase;
#my $xscale = ($xsize - $xright - $xleft) / $seqlen;
my $ybefleg = 35;

my $yscale = $h / $ymax ;

my $ysize  = $ybase + $h + $ybase + 100; 

if ($forcea4 == 1) {
  $ysize = Sets::min(842, $ysize);
}

my $p = new PostScript::Simple(xsize     => $xsize,
			       ysize     => $ysize,		
			       colour    => 1,
			       eps       => 1,
			       units     => "pt");

$p->setlinewidth(3);
$p->setcolour("black");
$p->setfont("Arial", 20);


# we want 5 ticks at most; what's the best increment ?
my $inc = 0.1;
while (($ymax/$inc) > 10) {
  $inc *= 10;
}

# vert + tick marls
$p->line($xleft , $ysize - ($ybase + $h), $xleft , $ysize - $ybase);
for (my $i=0.0; $i<=$ymax*1.01; $i+=$inc) {
  $p->line($xleft-2, $ysize - ($ybase + $h - $i * $yscale), $xleft+2, $ysize - ($ybase + $h - $i * $yscale));
  $p->text({ align => 'center', rotate => 0 }, $xleft-25, $ysize - ($ybase  + $h - $i * $yscale + 4), sprintf("%2.1f", $i));
}

$p->setfont("Arial", 35);

$p->text({ align => 'center', rotate => 90 }, $xleft-60, $ysize - ($ybase  + $h/2), "Normalized Read Density");

# horiz line
$p->line($xleft, $ysize - ($ybase + $h), $xleft + $numints * $w, $ysize - ($ybase+$h));

if (defined($title)) {
  $p->text({ align => 'center', rotate => 0 }, $xleft + $numints * $w / 2, $ysize - ($ybase-50) , $title);
}

# vert cpg start
$p->line($xleft +$w*10, $ysize - ($ybase + $h), $xleft + $w*10 , $ysize - $ybase);
$p->text({ align => 'center', rotate => 0 }, $xleft + $w * 5 , $ysize - ($ybase+$h+50) , "-1");

# vert cpg end
$p->line($xleft +$w*20, $ysize - ($ybase + $h), $xleft + $w*20 , $ysize - $ybase);
$p->text({ align => 'center', rotate => 0 }, $xleft + $w * 15 , $ysize - ($ybase+$h+50) , "CpG Island");

# vert last
$p->line($xleft + $w * 30, $ysize - ($ybase + $h), $xleft + $w * 30, $ysize - $ybase);
$p->text({ align => 'center', rotate => 0 }, $xleft + $w * 25 , $ysize - ($ybase+$h+50) , "+1");


# cycle thru line
my $idx = 0;
my $cntregions = 0;
foreach my $r (@a_data) {
	
  $p->setcolour("black");
	
  #my $n = shift @$r;
	
  my $y =  $ysize - ($ybase + $h + 20);
  if (($cntregions+1) % 2 == 0) {
    #  $y -= 25;
  } 
	
  #$p->text({align => "center"}, $xleft + $w * $idx  + $w * scalar(@$r) / 2, $y, $n);
  
  #$p->text({align => "left", rotate => 300}, $xleft + $w * $idx  + $w * scalar(@$r) / 2 - $w/2, $y, $n);
	
  #$p->setcolour("red");
	
  $p->setlinewidth(1);
  foreach my $v (@$r) {
		
    my $x1 = $xbase + $idx * $w;
    my $y1 = $ysize - ($ybase + $h);
		
    my $x2 = $xbase + ($idx + 1) * $w;
    my $y2 = $ysize - ($ybase + $h - $v * $yscale);
		
    $p->box($x1, $y1, $x2, $y2);
		
    $idx ++;
  }
	
  $p->setlinewidth(3);
  #$p->setcolour("black");
	
	
	
  $cntregions ++;
}


$outeps = "$profiles.plot";
$outeps .= ".eps";

print "Creating $outeps\n";
$p->output("$outeps");


if ($ps2pdf == 1) {
  my $outpdf = $outeps; $outpdf =~ s/\.eps/\.pdf/;
  print "Creating $outpdf\n"; 
  system("ps2pdf -dEPSCrop -dAutoRotatePages=/None $outeps $outpdf");
	
	
  if ($draw eq 'open') {
    system("open $outpdf");
  } elsif ($draw eq "evince") {
    system("evince $outpdf");
  }
	
}
