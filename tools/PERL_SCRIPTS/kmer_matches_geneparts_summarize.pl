#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

use strict;

my $kmer = undef;
if ($ARGV[2] ne "") {
  $kmer = $ARGV[2];
}

open IN, "ensGtp.txt" or die "Cannot open Gtp\n";
my %IDX = ();
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  next if ($a[2] =~ /^ENS/);
  $IDX{$a[0]} = $a[2];
}
close IN;

# read universe of genes
my @GENES = ();
my %GE = ();
open IN, "ensGene.txt.longest" or die "Cannot open ensGene.txt.longest\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  #push @GENES, $a[12];
  $GE{$a[12]} = 1;
}
close IN;
print STDERR "reading genes diff exp\n";

@GENES = keys(%GE);


my %EXP = ();
open IN, $ARGV[1] or die "Cannot open $ARGV[1]\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  $EXP{$a[0]} = $a[3];
}
close IN;


print STDERR "# reading $ARGV[0]\n";
open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
my %GP = ();
my %H  = ();

while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  next if (defined($kmer) && ($a[2] ne $kmer));
  push @{ $H{$a[1]}{$a[6]} }, $a[7];
  $GP{$a[6]} = 1;
}
close IN;

print STDERR "ok going into loop\n";

my @gp = sort(keys(%GP));

#print "\t"; # . join("\t", @gp) . "\n";
print "ENS\tGENE";
foreach my $g (@gp) {
  print "\t$g\_count\t$g\_pos";
}
print "\tEXP";
print "\n";
foreach my $r (@GENES) {
  print "$r";
  if (defined($IDX{$r})) {
    print "\t$IDX{$r}";
  } else {
    print "\t-";
  }
  foreach my $g (@gp) {
    my $num =  undef; 
    if (defined($H{$r}{$g})) {
      $num = @{$H{$r}{$g}};
    } else {
      $num = 0;
    }
    my $avg = undef;
    if (defined($H{$r}{$g})) {
      $avg = Sets::median($H{$r}{$g});
    } else {
      $avg = "NA";
    }
    print "\t$num\t$avg";
  }
  if (defined($EXP{$r})) {
    print "\t$EXP{$r}";
  } else {
    print "\tNA";
  }
  print "\n";
}



