#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;


open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;

  my $n = $a[0];
  my $m = $a[$ARGV[1]+1];
  
  my @b = split /\//, $m;

  if ($b[0] > Sets::log10(0.01)) {
    next;
  }
  
  print "$n\t$b[0]\n";

}
close IN;

