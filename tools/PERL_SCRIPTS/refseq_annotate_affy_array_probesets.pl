BEGIN{ $home = `echo \$HOME`; chomp $home}
use lib "$home/PERL_MODULES";

use Table;
use Sets;
use strict;

my $ta = Table->new;

if (@ARGV == 0) {
  die "Args: probes refgene\n";
}

#
# /home/elemento/PEOPLE/WEIMIN/METHYLATION/PLATFORM/prom_coordinates.txt /home/elemento/PEOPLE/WEIMIN/DESIGN/OLIVIER/BCL6_bound_peak_regions.txt
#

#
# load transcripts
#
$ta->loadFile($ARGV[1]);
my $a_ref_transcripts = $ta->getArray();

print STDERR "# found " . @$a_ref_transcripts . " ts\n";


open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
my $l = <IN>; 
my %G = ();
my %CNTPG = ();  # keep count of probes per gid
my $cnt = 0;
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  next if ($l =~ /\#/);
  next if ($l =~ /^probeset/);

  my $m   = \@a;
  my $pid = $m->[0];
  my $chr = $m->[1];
  my $sta = $m->[3];
  my $end = $m->[4];
  my $gid = $m->[6];

  my $ov  = 0;
  my %TS  = ();  # TSS -> 
  my @OV  = ();

  foreach my $b (@$a_ref_transcripts) { # loop over refGene
    
    # skip if not same chromosome
    next if ($chr ne $b->[2]);
        
    # if probe set contained in transcript 
    if (($sta >= $b->[4]) && ($end <= $b->[5])) {

      # determine tss, depending on strand
      my $tss = undef;
      if ($b->[3] eq '+') {
	$tss = $b->[4];
      } else {
	$tss = $b->[5];
      }
    

      #print "\t$b->[2]\t$b->[4]\t$b->[5] ov w $sta,$end?\n";
      #print "YES\n";

      # now go over exons
      my $e_st = $b->[9];
      my $e_en = $b->[10];
      my $e_fr = $b->[15];

      $e_st =~ s/\,$//;
      $e_en =~ s/\,$//;
      $e_fr =~ s/\,$//;
      
      my @a_e_st = split /\,/, $e_st;
      my @a_e_en = split /\,/, $e_en;
      my @a_e_fr = split /\,/, $e_fr;

 
      my $maxoverlap = 0;
      for (my $i=0; $i<@a_e_st; $i++) {		
	my $d = Sets::sequencesOverlap($sta, $end, $a_e_st[$i], $a_e_en[$i]);	
	#print "$sta, $end, $a_e_st[$i], $a_e_en[$i]\n";
	if ($d > $maxoverlap) {
	  $maxoverlap = $d;
	}	
      }
      
      if ($maxoverlap >= 25) {
	push @{$TS{$tss}}, "$b->[1]"; #($maxoverlap)"; # if (!defined($TS{$tss}));
      }
      
    } # if oberlap

  } # loop over ts

  my $nmraw = "$m->[10]"; #$nm =~ s/\ .+//;
  my @a_nmraw = split /\ /, $nmraw;
  my $nm = join("/", grep( /^N[MR]/, @a_nmraw));
  my @tst = ();
  foreach my $tss (keys(%TS)) {
    push @tst, Sets::get_smallest_NM($TS{$tss});
  }


  #print "$pid\t$chr\t$sta\t$end\t" . join("/", @tst) . "\t$nm\n";
  
  push @{$G{$gid}}, @tst;
  $CNTPG{$gid} ++;

  if (0) {
    print "$pid\t$gid\t";  
    if (@tst > 0) {
      print join("\t", @tst) . "\n";
    } else {
      print "NA\n";
    }
  }

  #print "2: $l\n";
  
  $cnt ++;

  #if ($cnt == 100) {
  #  last;
  #}

}


#print "# GROUP\n";
foreach my $gid (keys(%G)) {
  
  my @a_nms = @{$G{$gid}};

  my %C = ();
  foreach my $nm (@a_nms) {
    next if ($nm eq "NA");
    $C{$nm} ++;
  }

  my $n = @a_nms;

  my @good = ();
  foreach my $nm (keys(%C)) {
    if ($C{$nm}/$CNTPG{$gid} >= 0.75) {
      push @good, $nm;
    }
  }
  
  print "$gid\t";  
  if (@good > 0) {
    print join("\t", @good) . "\n";
  } else {
    print "NA\n";
  }
  
  
}
