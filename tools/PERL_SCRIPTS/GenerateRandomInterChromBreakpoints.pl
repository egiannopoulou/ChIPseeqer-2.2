#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

use Getopt::Long;

if (@ARGV == 0) {
  die "Args --chrdata=FILE --breakpoints=FILE\n";
}

my $chrdata     = undef;
my $breakpoints = undef;
my $extend      = 5000;
my $seed        = 1234;

GetOptions("chrdata=s"     => \$chrdata,
	   "seed=s"        => \$seed,
	   "extend=s"      => \$extend,
           "breakpoints=s" => \$breakpoints);

srand($seed);

# chrlen
my %CHRLEN = ();
open IN, $chrdata or die "Cannot open $chrdata\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  $CHRLEN{$a[0]} = $a[1];
}
close IN;

# 
open IN, $breakpoints or die "Cannot open $breakpoints\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  
  # breakpoint 1
  my $c1    = $a[0];
  my $p1    = $a[1];
  my $p1_st = $a[1] - $extend;
  my $p1_en = $a[1] + $extend;
  my $chrlen1 = $CHRLEN{$a[0]}; 
  my $p1_st_rand = undef;
  my $m1         = undef;
  my $cnt        = 0;
  while (1) {
    $p1_st_rand = Sets::max(0,int(0.5+rand() * $chrlen1 - 2*$extend));
    $m1    = getEncodeMappability($c1, $p1_st_rand, $p1_st_rand+2*$extend);
    $cnt++;
    if (($m1 > 0.25) || ($cnt >= 10)) {
      last;
    }
  }
  $p1_st_rand += $extend;

  # breakpoint 2
  my $c2    = $a[2];
  my $p2    = $a[3];
  my $p2_st = $a[3] - $extend;
  my $p2_en = $a[3] + $extend;
  my $chrlen2 = $CHRLEN{$a[2]}; 
  my $p2_st_rand = undef;
  my $m2         = undef;
  $cnt        = 0;
  while (1) {
    $p2_st_rand = Sets::max(0,int(0.5+rand() * $chrlen2 - 2*$extend));
    $m2    = getEncodeMappability($c2, $p2_st_rand, $p2_st_rand+2*$extend);
    $cnt++;
    if (($m2 > 0.25) || ($cnt >= 10)) {
      last;
    }
  }
  $p2_st_rand += $extend;


  print "$c1\t$p1_st_rand\t$c2\t$p2_st_rand\t$m1\t$m2\n";

}
close IN;





sub getEncodeMappability {

  my ($c, $i, $j) = @_;
  my $tmpfile = Sets::getTempFile("/tmp/map");
  my $todo = "$ENV{HOME}/PROGRAMS/SOFT/bigWigToWig -chrom=$c -start=$i -end=$j $ENV{HOME}/PEOPLE/MARK_WGS/wgEncodeCrgMapabilityAlign50mer.bw $tmpfile ";
  system($todo) == 0 or die "cannot exec $todo\n";
  
  open IN1, $tmpfile or die "Cannot open $tmpfile\n";
  my $len = 0;
  my $map = 0;
  while (my $l = <IN1>) {
    chomp $l;
    next if ($l =~ /\#/);
    my @a = split /\t/, $l, -1;
    my $l = $a[2] - $a[1];
    $len += $l;
    $map += $l * $a[3];
  }
  close IN1;

  if ($len == 0) {
    #print "$todo\n";
    return -1;
  }

  unlink $tmpfile;

  return $map / $len;
}
