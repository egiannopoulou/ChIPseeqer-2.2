#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;

  next if ($a[7] !~ /^INDEL/);

#  NM_005080       540     .       CGCAGCACTCAGACTACGTGCACCTCTGCAGCA       CGCAGCA,CGCAGCCCTCAGACTACGTGCACCTCTGCAGCACTCAGACTACGTGCACCTCTGCAGCA     0       .       INDEL;DP=438;I16=239,77,12,12,21528,2250786,4722,1031942,13268,557096,545,13771,4410,74328,361,6365   PL      0,255,255,255,255,255


  my $reflen = length($a[3]);
  my @a_alt = split /\,/, $a[4];
  
  my $minaltlen = 100000;
  foreach my $s (@a_alt) {
    if (length($s) < $minaltlen) {
      $minaltlen = length($s);
    }
  }

  next if ($minaltlen >= $reflen);
  
  next if ( ($reflen - $minaltlen) < 15 );

  my ($depth, $r1, $r2, $a1, $a2) = $a[7] =~ /DP\=(\d+)\;I16\=(\d+),(\d+),(\d+),(\d+),/;

  next if ($a1+$a2 < 5);
  
  print "$l\n";
  

}
close IN;

