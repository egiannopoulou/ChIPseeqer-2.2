#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
my $l = <IN>; print $l;

my @SUMS = ();
my @NUMS = ();

while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  shift @a;
  for (my $i=0; $i<@a; $i++) {
    $SUMS[$i] += $a[$i];
    $NUMS[$i] ++;
  }
}
close IN;


print "AVG";
for (my $i=0; $i<@SUMS; $i++) {
  my $r = $SUMS[$i]/$NUMS[$i];
  print "\t$r";
}
print "\n";
