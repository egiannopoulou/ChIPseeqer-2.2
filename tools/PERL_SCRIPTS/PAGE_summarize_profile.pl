my $sum = 0;
my $sum1 = 0;
open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
my $l = <IN>;
while (my $l = <IN>) {
  chomp $l;
  my @a =split /\t/, $l, -1;

  $sum1 += $a[0];
  $sum++;

}
close IN;

my $ra = sprintf("%4.1f%%", 100*$sum1/$sum);

print "$ra\n";
