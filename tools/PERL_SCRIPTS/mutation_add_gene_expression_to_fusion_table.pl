#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

use Getopt::Long;

# 1. load gene expression
# 2. load fusion table

if (@ARGV == 0) {
  die "Args --fustable=FILE --expfile=FILE\n";
}

my $fustable = undef;
my $expfile  = undef;

GetOptions("fustable=s" => \$fustable,
           "expfile=s"  => \$expfile);


open IN, $expfile or die "Cannot open $expfile\n";
my $l = <IN>; chomp $l;
my @h = split /\t/, $l, -1;
shift @h;
my %EXP = ();
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  my $g = shift @a;
  
  for (my $i=0; $i<@a; $i++) {
    $EXP{$g}{$h[$i]} = $a[$i];
  }
  
}
close IN;

open IN, $fustable or die "Cannot open $fustable\n";
$l = <IN>; chomp $l;
my @he = split /\t/, $l, -1;
shift @he;
print "$l\tpe\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  
  my $g = shift @a;

  if (defined($EXP{$g})) {
    
    #print "$g";
    my @v = ();
    for (my $i=0; $i<@a; $i++) {
      push @v, $EXP{$g}{$he[$i]};
    }
    my $vn = Sets::normalize_by_max(\@v);
    
    #print "$g";
    my @fus = ();
    for (my $i=0; $i<@a; $i++) {
      if ($a[$i] ne "") {
	if ($a[$i] =~ /fusion/) { 
	  push @fus, "color:blue";
	} else {
	  push @fus, "color:yellow";
	}
	#print "\t-1";
      } else {
	#print "\t0";
	push @fus, 0;
      }
    }

    my $pe = - Sets::pearson(\@fus, $vn);

    print "$g\t". join("\t", @$vn) . "\t$pe\n";    
    print "$g\t". join("\t", @fus) . "\t$pe\n";

    #print "$l\n";
  }
  

}
close IN;

