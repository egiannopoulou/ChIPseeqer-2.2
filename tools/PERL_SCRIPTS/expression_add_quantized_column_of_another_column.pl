#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;


my $col = $ARGV[1];
open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
my @MAT = ();
my @colmed  = ();
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  push @MAT, \@a; 
  push @colmed, $a[$col];
}
close IN;

my $med = Sets::median(\@colmed);

my $numcols = @{$MAT[0]};
$MAT[0][$numcols] = "med";
for (my $i=1; $i<@MAT; $i++) {
  if ($MAT[$i][$col] > $med) {
    $MAT[$i][$numcols] = 1;
  } else {
    $MAT[$i][$numcols] = 0;
  }
}


foreach my $m (@MAT)  {
  print join("\t", @$m) . "\n";
}
