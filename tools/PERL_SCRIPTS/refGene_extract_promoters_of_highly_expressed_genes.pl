#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

use Getopt::Long;

if (@ARGV == 0) {
  die "Args --refgene=FILE --expfile=FILE --col=\n";
}

my $refgene = undef;
my $expfile = undef;
my $col     = "CB";
my $high    = 1;
my $verbose = 0;

GetOptions("refgene=s" => \$refgene,
	   "high=s"    => \$high,
	   "verbose=s" => \$verbose,
	   "col=s"     => \$col,
           "expfile=s" => \$expfile);

# load refgene
my %PROM = ();
open IN, $refgene or die "Cannot open $refgene\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;

  if ($a[3] eq "+") {
    $PROM{$a[1]} = [ $a[2], $a[4]-1000, $a[4]+1000 ];
  } else {
    $PROM{$a[1]} = [ $a[2], $a[5]-1000, $a[5]+1000 ];
  }
}

close IN;



my %MAT = ();
open IN, $expfile or die "Cannot open $expfile\n";
my $l = <IN>; chomp $l;
my @h = split /\t/, $l;
my $idxcol = -1;
for (my $i=0; $i<@h; $i++)  {
  if ($h[$i] eq $col) {
    $idxcol = $i;
    last;
  }
}
if ($idxcol == -1) {
  die "fuck\n";
}
my @exp = ();
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  push @exp, [$a[0], $a[$idxcol] ];
}
close IN;

@exp = sort { $b->[1] <=> $a->[1] } @exp;

if ($high == 0) {
  @exp = reverse @exp;
}

my $numgenes = @exp;

my $cnt = 0;
foreach my $e (@exp) {

  next if !defined($PROM{$e->[0]});
  
  print "$e->[0]\t$e->[1]\t" if ($verbose == 1);

  print join("\t", @{$PROM{$e->[0]}}) . "\n";

  $cnt ++;
  if ($cnt > 0.1*$numgenes) {
    last;
  }
  
}

