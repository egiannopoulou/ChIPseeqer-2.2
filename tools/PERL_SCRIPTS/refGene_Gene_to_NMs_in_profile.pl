#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;


# load refGene
my %H = ();
open IN, $ARGV[1] or die "Cannot open $ARGV[1]\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  push @{ $H{$a[12]} }, $a[1] if (!Sets::in_array($a[1], @{ $H{$a[12]} }));
}
close IN;



open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
my $l = <IN>; print $l;
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;

  #$a[1] =~ s/\.\d+$//;
  
  if (defined($H{$a[0]})) {

    foreach my $nm (@{$H{$a[0]}}) {      
      print "$nm\t$a[1]\n";
    }
  }

}
close IN;

