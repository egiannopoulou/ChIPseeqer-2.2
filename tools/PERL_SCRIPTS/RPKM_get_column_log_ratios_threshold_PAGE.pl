#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;


use Getopt::Long;

if (@ARGV == 0) {
  die "Args --matrix=FILE --col1=INT --col2=INT --header=INT --outfile=FILE \n";
}

my $matrix = undef;
my $col1   = undef;
my $col2   = undef;
my $header = 1;
my $outfile= undef;
my $lrth     = 1;
my $minexp   = 5;

GetOptions("matrix=s" => \$matrix,
           "col1=s"   => \$col1,
	   "lr=s"     => \$lrth,
           "col2=s"   => \$col2,
	   "minexp=s" => \$minexp,
           "header=s" => \$header,
	   "outfile=s"=> \$outfile
	  );

print STDERR "# will use minexp = $minexp\n";

open IN, $matrix or die "Cannot open file";

open OUT1, ">$outfile"    or die "cannot open $outfile\n";
open OUTU, ">$outfile.UP" or die "cannot open $outfile.UP\n";
open OUTD, ">$outfile.DN" or die "cannot open $outfile.DN\n";

my $i1 = $col1;
my $i2 = $col2;

my $l = <IN>; chomp $l;

my @a = split /\t/, $l;

if ($header == 1) {
  print OUT1 "GENE\t$a[$i1]/$a[$i2]\n";
  print OUTU "GENE\t$a[$i1]/$a[$i2]\n";
  print OUTD "GENE\t$a[$i1]/$a[$i2]\n";
}
my $numup = 0;
my $numdn = 0;

while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  
  
  my $lr = Sets::log2( ($a[$i1]+1) / ($a[$i2]+1) );
  my $ma = Sets::max($a[$i1], $a[$i2]);

  if ($lr >= $lrth) {
    if ($ma >= $minexp) {
      print OUT1 "$a[0]\t2\n";
      print OUTU "$a[0]\t1\n";
      print OUTD "$a[0]\t0\n";
      $numup++;
    } else {
      print OUT1 "$a[0]\t0\n";      
      print OUTU "$a[0]\t0\n";      
      print OUTD "$a[0]\t0\n";      
    }
  } elsif ($lr <= -$lrth) {
    if ($ma >= $minexp) {	  
      print OUT1 "$a[0]\t1\n";
      print OUTU "$a[0]\t0\n";
      print OUTD "$a[0]\t1\n";
      $numdn++;
    } else {
      print OUT1 "$a[0]\t0\n";      
      print OUTU "$a[0]\t0\n";      
      print OUTD "$a[0]\t0\n";      

    }
  } else {
    print OUT1 "$a[0]\t0\n";
    print OUTU "$a[0]\t0\n";
    print OUTD "$a[0]\t0\n";
  }

}
close IN;

close OUT1;
close OUTD;
close OUTU;

print STDERR "# numup = $numup\n";
print STDERR "# numdn = $numdn\n";
print STDERR "# output created in $outfile\n# $outfile.DN\n# $outfile.UP\n";
