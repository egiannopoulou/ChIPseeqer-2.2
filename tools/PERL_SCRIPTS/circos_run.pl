#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

use Getopt::Long;

if (@ARGV == 0) {
  die "Args --genome=FILE --bedfile=FILE\n";
}

my $bedfile = undef;
my $genome  = "hg19";
my $outfile = "circos-test.png";
my $max     = 10;
my $title   = undef;
GetOptions("bedfile=s" => \$bedfile,
	   "title=s"   => \$title,
	   "outfile=s" => \$outfile,
           "genome=s"  => \$genome);


my $chromosomes      = "chr1;chr2;chr3;chr4;chr5;chr6;chr7;chr8;chr9;chr10;chr11;chr12;chr13;chr14;chr15;chr16;chr17;chr18;chr19;chr20;chr21;chr22;chrX";
my $chromosome_order = "chr1;chr2;chr3;chr4;chr5;chr6;chr7;chr8;chr9;chr10;chr11;chr12;chr13;chr14;chr15;chr16;chr17;chr18;chr19;chr20;chr21;chr22;chrX";

if ($genome eq "mm9") {
  $chromosomes = "chr1;chr2;chr3;chr4;chr5;chr6;chr7;chr8;chr9;chr10;chr11;chr12;chr13;chr14;chr15;chr16;chr17;chr18;chr19;chrX";
  $chromosome_order = $chromosomes;
}

use Template;
my $tt = Template->new( ABSOLUTE     => 1 );

my %data = ( chromosomes      => $chromosomes,
	     chromosome_order => $chromosome_order,
	     karyotype        => "/Users/ole2001/PROGRAMS/SNPseeqer/REFDATA/mm9.karyotype",
	     outfile          => $outfile,
	     max              => $max,
	     bedfile          => $bedfile
	   );

$tt->process('/Users/ole2001/PROGRAMS/SNPseeqer/REFDATA/circos.conf.tmpl', \%data, "circos.conf")
    || die $tt->error;



system("~/CLEANUP/circos-0.56/bin/circos -debug -conf circos.conf") == 0 or die "cannot run circos\n";


if (defined($title)) {
  system("convert $outfile -pointsize 72 -background White  label:'$title' +swap  -gravity Center -append  $outfile");
}


system("open $outfile");

