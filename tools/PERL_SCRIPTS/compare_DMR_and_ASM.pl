#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;


# load ASM

my %ASM = ();
open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;

  push @{$ASM{$a[0]}}, \@a;
  
}
close IN;


open IN, $ARGV[1] or die "Cannot open $ARGV[1]\n";
while (my $l = <IN>) {
  chomp $l;
  #print "$l\n";
  my @a = split /\t/, $l, -1;

  my $chr = $a[0];
  my $hasSNP = 0;
  my $hasSS  = 0;
  foreach my $a (@{$ASM{$chr}}) {
    if (Sets::sequencesOverlap($a[1], $a[2], $a->[1], $a->[1])) {
      #print join("\t", @$a) . "\n";
      $hasSNP++;
      
      if (($a->[3] eq "SS") || ($a->[4] eq "SS")) {
	$hasSS ++;
      }

    }
  }

  if ($hasSNP > 0) {
    my $idx = ($hasSS>0?1:0);
    print "$l\t$hasSS\t$idx\n";
  }
  

}
close IN;


