#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;
use MyBlast;
use ClustalW;
use Fasta;
use Getopt::Long;

if (@ARGV == 0) {
  die "Args --mutlist=STR\n";
}
my $gene         = undef;
my $mutlist      = undef;
my $mutation     = undef;
my $gene2NP_file = "$ENV{HOME}/PROGRAMS/SNPseeqer/REFDATA/refLinkrefGene.txt.26Dec2009";
my $protfile     = "$ENV{HOME}/PROGRAMS/SNPseeqer/REFDATA/human.protein.faa";
my $pdb          = "$ENV{HOME}/DATA/PDB/pdb_seqres.txt";

GetOptions("mutlist=s" => \$mutlist,
           "gene=s"    => \$gene);


my %PDB_MATCHES = ();
my @A_MATCHES   = ();
open INM, $mutlist or die "Cannot open $mutlist\n";
my $l = <INM>; # header
while (my $l = <INM>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  
  $mutation = $a[2];
  $gene     = $a[0];
  
  # separate mutations, take last (TO BE CHANGED)
  my @muts = split /\//, $mutation;
  $mutation = pop @muts;
  my @a_mut = $mutation =~ /(.)(\d+)(.)/;
  
  # find all NPs for that gene in dictionery
  my @NPs = ();
  open IN, $gene2NP_file or die "Cannot open gene 2 NP\n";
  while (my $l = <IN>) {
    chomp $l;
    my @b = split /\t/, $l, -1;
    if ($b[2] eq $gene) {
      push @NPs, $b[1] if (!Sets::in_array($b[1], @NPs));
    }
  }
  close IN;

  my $numnp = scalar(@NPs);
  print STDERR "# Found $numnp\n";
  
  #
  # blast each protein against PDB
  #

  my $mb = MyBlast->new;
  if ($ENV{BLASTDIR} ne "") {
    $mb->setBlastDir($ENV{BLASTDIR});
  }
  $mb->setBlastProgram("blastp");
  $mb->setDatabaseDatabase($pdb);
  $mb->setNbProcessors(2);
  $mb->setEvalueThreshold("1e-10");
  $mb->setVerbose(0);
    
  my $fa = Fasta->new;
  $fa->setFile($protfile);
  my $tmpfile1 = Sets::getTempFile("/tmp/blast.1");
  
  my $bestpdb = undef;
  my $bestE   = 100000;
  my $bestnp  = undef;
  my $bestnps = undef;
  my $bestpdbseq = undef;
  
  # traverse all proteins, in search of NPs in list
  while (my $a_ref = $fa->nextSeq()) {
    my ($n, $s) = @$a_ref;
    # >gi|14150147|ref|NP_115725.1| syndesmos [Homo sapiens]
    
    my @ns = split /\|/, $n;
    my $np = $ns[3];
    $np =~ s/\.\d+$//;
    #print "$np\n";
    
    # if that protein is one in the list
    if (Sets::in_array($np, @NPs)) {
      
      #
      # blast against PDB
      #
      
      # create a query file
      $fa->writeSeq($tmpfile1, $np, $s);      
      $mb->setQueryDatabase($tmpfile1);
      
      # blast
      my $a_ref = $mb->blastallUnique;
      
      # next if no matcg
      next if (scalar(@$a_ref) == 0);
      
      #  get the homologous protein name
      my $d_id    = $mb->getUniqueHitName();
      
      # get best evalue
      my $eval    = $mb->getLowestEvalue($a_ref);
      print "# Found match $d_id $eval \n";
      if ($eval < $bestE) {
	$bestpdb = $d_id;
	$bestE   = $eval;
	$bestnp  = $np;
	$bestnps = $s;
      }
    }    
  }
    
  #
  # determine where in the PDB chain the mutation is located 
  #

  # 1. get full PDB seq
  $fa = Fasta->new;
  $fa->setFile($pdb);
  while (my $a_ref = $fa->nextSeq()) {
    my ($n, $s) = @$a_ref;
    my @nn = split /\ /, $n;
    if ($nn[0] eq $bestpdb) {
      $bestpdbseq = $s;
      last;
    }
  }  
  print "# found $bestpdb $bestE $bestpdbseq\n";


  # 2. create temp file and align
  open OUT, ">$tmpfile1";
  print OUT ">$bestnp\n$bestnps\n>$bestpdb\n$bestpdbseq\n";
  close OUT;
  system("clustalw $tmpfile1");
  
  # 3. parse aln
  my $posmutinpdb = undef;
  my $cl = ClustalW->new;
  $cl->setFile("/tmp/blast.aln");
  my $a_ref_aln = $cl->getSeqsWithNames();
  my $s1 = $a_ref_aln->[0]->[1];
  my $s2 = $a_ref_aln->[1]->[1];
  print "$s1\n";
  print "$s2\n";
  my @a1 = split //, $s1;
  my @a2 = split //, $s2;
  while ($a1[0] eq '-') {
    shift @a1;
    shift @a2;
  }
  #print join("", @a1) . "\n";
  my $i1 = 0;
  my $i2 = 0;
  for (my $j=0; $j<@a1; $j++) { 
    print "$a1[$j]$a2[$j]\n";
    if ($a1[$j] ne '-') {
      $i1++;
    }
    if ($a2[$j] ne '-') {
      $i2++;
    }
    if ($i1 == $a_mut[1]) {
      print "$i2 ($a2[$j])\n";
      $posmutinpdb = $i2;
      last;
    }
  }
  
  # ok
  my @b = split /\_/, $bestpdb;
  my $p = $b[0];
  my $chain = $b[1];

  # add to array
  my @a_tmp = ($p, $chain, $posmutinpdb, $gene, $mutation);
  push @{$PDB_MATCHES{$p}}, \@a_tmp;
  push @A_MATCHES, \@a_tmp;
  
}
  
open OUTM, ">$mutlist.PDB" or die "Cannot open PDB out\n";
foreach my $m (@A_MATCHES) {
  print OUTM join("\t", @A_MATCHES) . "\n";
}
close OUTM;
foreach my $p (keys(%PDB_MATCHES)) {
 
  # write script
  open OUT, ">$p.txt";
  print OUT "wireframe off
spacefill off
cartoon 100
";
  foreach my $r (@{$PDB_MATCHES{$p}}) {
    print OUT "select $r->[2]:$r->[1]
wireframe 100
color yellow
";
  }
  close OUT;
  # end of script
  
  # download PDB
  if (! -e "pdb$p.ent") {
    my $f = "http://dx.doi.org/10.2210/pdb$p/pdb";
    system("wget $f");
    system("gunzip pdb$p.ent.gz");
  }
}

  #system("sh $ENV{HOME}/PROGRAMS/SNPseeqer/PROGRAMS/jmol-11.8.15/jmol.sh pdb$p.ent -s script.txt");
