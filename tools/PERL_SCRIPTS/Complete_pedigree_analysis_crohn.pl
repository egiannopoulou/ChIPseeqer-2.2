#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

# look for SNP het present in all children, present in dad, absent in mom

# GS01446-DNA_C01 CGR0335 ST001RM 10200   1319    

use Getopt::Long;

if (@ARGV == 0) {
  die "Args --pattern=FILE --f2=FILE\n";
}
my $pattern = undef;
my $annindex = 0;
my $nohom = 1;
GetOptions("pattern=s" => \$pattern,
	   "nohom=s"    => \$nohom,
	   "annindex=s" => \$annindex
           );


my %S = ();
open IN, "samples.txt" or die "Cannot open samples.txt\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  $S{$a[0]} = $a[4];
}
close IN;

my $todo = "find . -name \"*.CDS2.txt\"";

my $todotxt = `$todo`;
my @a_sums = split /\n/, $todotxt;

my @a_sums_samples = ();

my %PRES = ();
my %ANN  = ();

# build matrix of presence
foreach my $f (@a_sums) {
  
  my @a= split /\//, $f;
  push @a_sums_samples, $S{$a[3]};

  open IN, $f or die "Cannot open $f\n";
  my $l = <IN>;
  while (my $l = <IN>) {
    chomp $l;
    my @a = split /\t/, $l, -1;
    $PRES{"$a[2]\t$a[3]"}{$f} = "$a[6]/$a[5]/$a[14]/$a[21]/$a[22]/$a[8]/$a[9]";
    $ANN {"$a[2]\t$a[3]"}{$f} = $a[25];
  }
  close IN;


}

print "chr\tpos\t" . join("\t", @a_sums_samples) . "\tnum\tpattern\tgene\tdesc\n";

my @OUT = ();
foreach my $p (sort(keys(%PRES))) {
  my @a_tmp = ();
  push @a_tmp, $p;
  my $num = 0;
  my $txt = "";
  my $hom = 0;
  foreach my $f (@a_sums) {
    push @a_tmp, $PRES{$p}{$f};

    if (defined($PRES{$p}{$f})) {
      $num ++;
      $txt .= "1";

      if ($PRES{$p}{$f} =~ /hom/) {
	$hom++;
      }
      
    } else {
      $txt .= "0";
    }
  }
  #print "\t$num\t$txt";
  if (($nohom == 1) && ($hom > 0)) {
    next;
  }
  if ($txt ne $pattern) {
    next;
  }

  push @a_tmp, ($num, $txt);

  my @b = split /\;/, $ANN{$p}{$a_sums[$annindex]};
  my @genes = ();
  foreach my $s (@b) {
    my @c = split /\:/, $s;
    if (($c[4] =~ /SENSE/)  || ($c[4] =~ /FRAME/)) {
      push @genes, $c[2] if (!Sets::in_array($c[2], @genes));
    }
  }
  
  push @a_tmp, join("/", @genes);
  push @a_tmp, $ANN{$p}{$a_sums[$annindex]};
  push @OUT, \@a_tmp;
}


@OUT =  sort { $b->[9] <=> $a->[9] } @OUT;
@OUT =  sort { $a->[0] cmp $b->[0] } @OUT;

@OUT =  sort { $a->[1] <=> $b->[1] } @OUT;

foreach my $r (@OUT) {
  foreach my $s (@$r) {
    if ($s eq "") {
      $s = "-";
    }
  }
  print join("\t", @$r) . "\n";
}
