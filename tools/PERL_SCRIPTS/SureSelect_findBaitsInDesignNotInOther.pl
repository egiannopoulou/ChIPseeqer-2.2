#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

if (@ARGV == 0) {
  die "Args: RMoff RMon\n";
}

# load RM on
my @BAITS2 = ();
open IN, $ARGV[1] or die "Cannot open $ARGV[1]\n";
my $l = <IN>;
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  my @b = split /[\:\-]/, $a[1];
  push @BAITS2, \@b;
}
close IN;

# load RM off
open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
my $cnt = 0;
my $l = <IN>;
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  my @b = split /[\:\-]/, $a[1];

  my $in = 0;
  foreach my $b2 (@BAITS2) {  # does the bait overlap with any RMon baits by  
    #print "$b[1], $b[2], $b2->[1], $b2->[2]\n";
    my $ov = Sets::sequencesOverlap($b[1], $b[2], $b2->[1], $b2->[2]);
    if ($ov >= 40) {
      $in = 1;
    } 
  }
  if ($in == 0) {
    print join("\t", @a) . "\n";
  } else {
    #print "Skipped\n";
  }
  
  print STDERR "$cnt      \r";
$cnt ++;
}
close IN;

