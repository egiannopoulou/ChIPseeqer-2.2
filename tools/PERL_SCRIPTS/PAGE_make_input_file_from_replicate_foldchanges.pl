open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
my $l = <IN>; chomp $l;
my @a = split /\t/, $l, -1;
print "$a[0]\t$a[1]/$a[2]\n";

while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;

  if (($a[1] > 1) && ($a[2] > 1)) {
    print "$a[0]\t2\n";
  } elsif (($a[1] < -1) && ($a[2] < -1)) {
    print "$a[0]\t1\n";
  } else {
    print "$a[0]\t0\n";
  }
}
close IN;

