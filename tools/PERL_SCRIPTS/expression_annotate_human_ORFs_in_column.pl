#!/usr/bin/perl

BEGIN{ $home = `echo \$HOME`; chomp $home}
use lib "$home/PERL_MODULES";

use Table;
use strict;

use Getopt::Long;

my $col  = $ARGV[1];
my $head = 1;



my $ta = Table->new;
$ta->loadFile("$ENV{HOME}/PROGRAMS/SNPseeqer/REFDATA/hg18/refLink.txt.07Jun2010.colreag");
my $h_ref = undef;
$h_ref = $ta->getIndex(0);


$ta->loadFile($ARGV[0]);
my $a_ref = $ta->getArray;

if ($head == 1) {
  my $r = shift @$a_ref;
  print join("\t", @$r);
  print "\tDescription\n";
}

foreach my $r (@$a_ref) {
  print join("\t", @$r) . "\t" . $h_ref->{$r->[$col]}->[2] . "\n"; 
}

