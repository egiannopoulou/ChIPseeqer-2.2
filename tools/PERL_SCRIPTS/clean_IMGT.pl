#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

use Fasta;

my $fa = Fasta->new;
$fa->setFile($ARGV[0]);

while (my $a_ref = $fa->nextSeq()) {
  my ($n, $s) = @$a_ref;
  
  my @a = split /\|/, $n;
  $s =~ s/\.//g;
  $s = uc($s);
  print ">$a[1]\n$s\n";
  
}
