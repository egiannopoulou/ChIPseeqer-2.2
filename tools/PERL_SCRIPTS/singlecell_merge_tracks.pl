open IN, "samples.txt" or die "Cannot open samp\n";
my %S = ();
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  $S{$a[1]} = $a[2];
}
close IN;



foreach my $f (@ARGV) {
  open IN, $f or die "Cannot open $f\n";

  my $cnt = 1;
  while (my $l = <IN>) {
    chomp $l;
    my @a = split /\t/, $l, -1;
    
    $H{"$cnt/$a[0]"}{$f} = $a[1];
    
    $cnt ++;
  }
  close IN;


}

foreach my $f (@ARGV) {
  my ($num) = $f =~ /s\_(\d+)\_/;
  print "\t$S{$num}";
}
print "\n";

foreach my $k (sort { $a <=> $b } (keys(%H))) {
  print "$k";
  foreach my $f (@ARGV) {
    print "\t$H{$k}{$f}";
  }
  print "\n";
}
