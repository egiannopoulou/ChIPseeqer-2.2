open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
my $l      = <IN>;
my @a      = split /\t/, $l;
my $b1     = $a[2];
my $b2     = $a[3];
my $b1_cnt = 0;
my $b2_cnt = 0;

while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;

  if ($a[2] >= $b1) {
    $b1_cnt++;
  }
  if ($a[3] >= $b2) {
    $b2_cnt++;
  }
  
}
close IN;

$b1_cnt = $b1_cnt / 100;
$b2_cnt = $b2_cnt / 100;

print "$ARGV[0]\tp=$b1_cnt (ARE)\tp=$b2_cnt (U-rich)\n";

