#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

# mouse 10090
my %LEN3 = ();
open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
my $l = <IN>;
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  
  if ($a[3] eq "10090") {
    $a[4] =~ s/\-//g;
    $a[4] =~ s/U/T/g;    
    $a[4] = uc($a[4]);
    my $len = length($a[4]);

    if (!defined($LEN3{$a[2]}) || ($len > length($LEN3{$a[2]}->[1]))) {
      $LEN3{$a[2]} = [ "$a[0] $a[2]", $a[4] ];
    }

    #print ">$a[0] $a[2]\n$a[4]\n";
  }
  
}
close IN;


foreach my $g (keys(%LEN3)) {
  print ">$LEN3{$g}->[0]\n$LEN3{$g}->[1]\n";
}
