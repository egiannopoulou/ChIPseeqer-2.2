my %H = ();
open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  my $n = shift @a;
  $H{$n} = \@a;
}
close IN;


open IN, $ARGV[1] or die "Cannot open $ARGV[1]\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;

  if (defined($H{$a[0]})) {
    print "$l\t" . join("\t", @{$H{$a[0]}}) . "\n";
  }
  
}
close IN;


