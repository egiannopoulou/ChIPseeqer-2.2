#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

use Getopt::Long;

if (@ARGV == 0) {
  die "Args --expfile=FILE --motifs=FILE\n";
}
my $expfile = undef;
my $profile = undef;
my $motifs  = undef;

GetOptions("expfile=s" => \$expfile,
           "motifs=s" => \$motifs);

my @a_mot   = split /\,/, $motifs;

my $file    = Sets::filename($expfile);
my $profile = "$expfile\_FIRE/RNA/$file.profiles";
# 
my %EXP = ();
open IN, $expfile or die "Cannot open $expfile\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  $EXP{$a[0]} = $a[1];
}
close IN;

# profile
my %CNT = ();
open IN, $profile or die "Cannot open $profile\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  if (Sets::in_array($a[0], @a_mot)) {
    $CNT{$a[1]} ++;
  }
}
close IN;


my @peaks = keys(%EXP);
my $num1m = 0;
my $num0m = 0;
my $num1 = 0;
my $num0 = 0;
my $numjust1 = 0;
my %NUM = ();
foreach my $p (@peaks) {
  
  if ($EXP{$p} == 1) {
    if ($CNT{$p} > 0) {
      $num1m ++;
    } 
    $NUM{$CNT{$p}} ++;

    $num1++;
  } elsif ($EXP{$p} == 0) {
    if ($CNT{$p} > 0) {
      $num0m ++;
    }
    $num0++;
  }

}

my $rat = $num1m/$num1;
print "num1=$num1m/$num1 $rat\n";
print "num0=$num0m/$num0\n";
foreach my $p (sort(keys(%NUM))) {
  my $rat = sprintf("%3.1f%", 100*$NUM{$p}/$num1);
  print "$p\t$NUM{$p}\t$rat\n";
}
