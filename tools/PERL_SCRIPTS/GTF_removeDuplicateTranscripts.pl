open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";

my $prev_ts = undef;
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;

  my $e_st = $a[3];
  my $e_en = $a[4];
  my $e_fr = $a[6];
  
  my $desc = $a[8];

  my ($gene, $ts) = $desc =~ /gene_id \"(.+?)\"; transcript_id \"(.+?)\"/;

  if ($ts ne $prev_ts) {
    # new ts
    
    if (

    $prev_ts = $ts;

  } else {
    print "$l\n";
  }

}
close IN;

