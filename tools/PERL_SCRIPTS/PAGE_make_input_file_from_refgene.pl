#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

use Getopt::Long;

if (@ARGV == 0) {
  die "Args --geneset=FILE --refgene=FILE\n";
}

my $refgene = undef;
my $geneset = undef;
my $NM      = 0;
my $isNM    = 0;

GetOptions("refgene=s" => \$refgene,
	   "isNM=s"    => \$isNM,
	   "NM=s"      => \$NM,
           "geneset=s" => \$geneset);

open IN, $refgene or die "Cannot open $refgene\n";

my %GENES = ();
my %NM2GENE = ();
my $idxgene = 12;
if ($isNM == 1) {
  $idxgene = 1;
}
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  $GENES{$a[$idxgene]}  = 1;
  $NM2GENE{$a[1]} = $a[$idxgene];
}
close IN;


my %SET = ();
open IN, $geneset or die "Cannot open $geneset\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  
  my $gene = $a[0];

  if (($NM == 1) && (defined($NM2GENE{$gene}))) {
    $gene = $NM2GENE{$gene};
  }
  
  $SET{$gene} = 1;
}
close IN;
print "GENE\tBIN\n";
foreach my $g (keys(%GENES)) {

 
  
  #next if (!defined($g));

  print "$g\t";

  if (defined($SET{$g})) {
    print "1\n";
  } else {
    print "0\n";
  }

}



