#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

use Getopt::Long;

if (@ARGV == 0) {
  die "Args --refgene=FILE --geneset=FILE\n";
}

my $refgene = undef;
my $geneset = undef;
GetOptions("refgene=s" => \$refgene,
           "geneset=s" => \$geneset);

# load genes
my $h_ref_genes = Sets::getIndex($geneset);

# read refgene
open IN, $refgene or die "Cannot open $refgene\n";

my %CHRSEG = ();
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;

  if (defined($h_ref_genes->{$a[12]})) {
    
    push @{$CHRSEG{$a[2]}}, [$a[4], $a[5]];
    

  }

}
close IN;

foreach my $c (keys(%CHRSEG)) {
  
  my @a = @{$CHRSEG{$c}};
  
  my $a_ref_a = Sets::mergeOverlappingIntervals(\@a);
  
  foreach my $r (@$a_ref_a) {
    print "$c\t$r->[0]\t$r->[1]\n";
  }

}
