#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;


use XML::DOM;

use Getopt::Long;

if (@ARGV == 0) {
  die "Args --dir=DIR --f2=FILE\n";
}

my $dirs      = undef;
my $submit    = 0; 
my $species   = undef;
my $samples   = undef;
my $remdup    = 1;
my $descatend = 0;

GetOptions("dirs=s"      => \$dirs,
	   "remdup=s"    => \$remdup,
	   "descatend=s" => \$descatend,
	   "species=s"   => \$species,
	   "samples=s"   => \$samples,
	   "submit=s"    => \$submit);

my $genedesc = undef;
my $idxorf   = undef;
my $idxdesc  = undef;
my $idxnm    = undef;
if ($species eq "mouse") {
  $idxnm    = 0;
  $idxorf   = 1;
  $idxdesc  = 2;
  $genedesc = "$ENV{HOME}/PROGRAMS/SNPseeqer/REFDATA/mm9/refGene.txt.mm9.3JULY20111.desc";
} else {
  $idxnm    = 1;
  $idxorf   = 0;
  $idxdesc  = 2;
  $genedesc = "$ENV{HOME}/PROGRAMS/ChIPseeqer/DATA/hg18/refLink.txt.07Jun2010.colreag";
}

my $ta = Table->new;
$ta->loadFile($genedesc);
my $h_ref = $ta->getIndex($idxnm);


# open sample data
my %DESC = ();
my @SAMPLES = ();
open IN, $samples or die "Cannot open samples $samples\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  $DESC{$a[0]} = $a[1];
  push @SAMPLES, $a[0];
}
close IN;



my %M = (); # data matrix
my %SPE = ();
my @a_dirs = split /\,/, $dirs;

foreach my $dir (@a_dirs) {
  
  my $xmlfile = "$dir/Summary.xml";
  if (! -e $xmlfile) {
    die "Can't find $xmlfile\n";
  }

  print STDERR "# analyzing $xmlfile\n";
  
  my $parser = new XML::DOM::Parser;
  my $doc    = $parser->parsefile($xmlfile);

  my $samples = $doc->getElementsByTagName("Samples")->item(0);
  
  foreach my $lane ($samples->getElementsByTagName("Lane")) {
    my $lanenum = $lane->getElementsByTagName("laneNumber")->item(0)->getChildNodes->item(0)->getData;
    my $sampleId = $lane->getElementsByTagName("sampleId")->item(0)->getChildNodes->item(0)->getData;
    my $expspecies  = $lane->getElementsByTagName("species")->item(0)->getChildNodes->item(0)->getData;
    $expspecies = lc($expspecies);
    my $barcode  = $lane->getElementsByTagName("barcode")->item(0)->getChildNodes->item(0)->getData;
    $SPE{$sampleId} = $expspecies;
    next if (defined($species) && ($expspecies ne $species));
    
    print STDERR "$lanenum\t$sampleId\t$barcode\t$expspecies\n";
    
    my $genome = undef;
    if ($expspecies eq "human") {
      $genome = "hg18";
    } elsif ($expspecies eq "mouse") {
      $genome = "mm9";
    } else {
      die "Cannot find $expspecies\n";
    }
    
    my $file = "$dir/s_$lanenum\_sequence.txt.tophat/transcripts.expr";
    
    if (! -e $file) {
      die "Problem ... $file not found\n";
    } 
    
        
    open IN1, $file or die "Cannot open $file\n";  # data RPKM
    my $l2 = <IN1>; 
    while (my $l2 = <IN1>) {
      chomp $l2;
      my @b = split /\t/, $l2, -1;
      
      $M{$b[0]}{$sampleId} = sprintf("%3.2f", $b[5]);   # gene x sample

      

    }
    close IN1;

  } # for loop on samples in xml
 
  
} # for



print "TRANSCRIPT";
print "\tORF\tDescription";
foreach my $l (@SAMPLES) {
  next if (defined($species) && ($SPE{$l} ne $species));
  print "\t$l/$DESC{$l}";
}
if ($descatend == 1) {
  print "\tORF\tDescription";
}
print "\n";
foreach my $g (keys(%M)) {
  next if (($remdup == 1) && ($g =~ /dup\d+$/));
  print $g;
  
  print "\t$h_ref->{$g}->[$idxorf]\t$h_ref->{$g}->[$idxdesc]";

  foreach my $l (@SAMPLES) {
    next if (defined($species) && ($SPE{$l} ne $species));

    print "\t$M{$g}{$l}";
  }
  if ($descatend == 1) {
    print "\t$h_ref->{$g}->[$idxorf]\t$h_ref->{$g}->[$idxdesc]";
  }
  print "\n";
}
