#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

if (@ARGV == 0) {
  die "Args: matrix col \n";
}
open IN, $ARGV[0] or die "Cannot open file";

my $i1 = $ARGV[1];

my $l = <IN>; chomp $l;
my @a = split /\t/, $l;
my @b = @a;

for (my $i=2; $i<@a; $i++) {
  $a[$i] = "$b[$i]/$b[$i1]";
}
print join("\t", @a) . "\n";

while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;

  next if ($a[0] =~ /^MIR\d+/);
  next if ($a[0] =~ /^SNOR/);

  my @b = @a;

  for (my $i=2; $i<@a; $i++) {
    my $lr = Sets::log2( ($b[$i]+1) / ($b[$i1]+1) );
    $a[$i] = sprintf("%4.3f", $lr);
  }
  
  print join("\t", @a) . "\n";


}
close IN;

