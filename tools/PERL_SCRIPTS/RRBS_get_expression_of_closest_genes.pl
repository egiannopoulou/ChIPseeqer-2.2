#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;


# load log ratios
open IN, $ARGV[1] or die "Cannot open $ARGV[1]\n";


my %EXP = ();
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  $EXP{$a[0]} = $a[3];
}
close IN;


# load closest genes
my %GENES = ();
open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  
  next if (defined($GENES{$a[3]}));

  print "$l\t" . $EXP{$a[3]} . "\n" if (defined($EXP{$a[3]}));

  $GENES{$a[3]} = 1;

}
close IN;


