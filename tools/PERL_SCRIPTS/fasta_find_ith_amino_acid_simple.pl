#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

use Fasta;

my $fa = Fasta->new;
$fa->setFile($ARGV[0]);

my $a_ref = $fa->nextSeq();
my ($n, $s) = @$a_ref;
    
my @a_s = split //, $s;

for (my $i=0; $i<@a_s; $i++) {
  my $ii = $i+1;
  print $ii . "\t" . $a_s[$i] . "\n";
}
