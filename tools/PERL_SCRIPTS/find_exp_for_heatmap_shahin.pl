#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

# open 
#my $d = "../../allrnaseqdatanodup.txt";
my $list =  "../../expfiles.txt.V3"; # "../../human_rnaseq_run1_PE_fixed_oldlabels.txt";

open LI, $list or die "cannot open $list\n";
my %M = ();

while (my $d = <LI>) {
  chomp $d;
  open IN, "../../$d" or die "Cannot open ../../$d\n";
  my $l = <IN>;
  my @h = split /\t/, $l;
  shift @h;
  while (my $l = <IN>) {
    chomp $l;
    my @a = split /\t/, $l, -1;
    my $nm = shift @a;
    for (my $i=0; $i<@a; $i++) {
      $M{$nm}{$h[$i]} = $a[$i];
    }
  }
  close IN;
}
close LI;


open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
my $l  = <IN>; chomp $l;  # line 1 ... how they are in data
my @h0  = split /\t/, $l;
shift @h0;
shift @h0;
#shift @h0;

my $l  = <IN>; chomp $l;  # line 2 how they shpould be shown
my @h1  = split /\t/, $l;
shift @h1;
shift @h1;
#shift @h1;

foreach my $s (@h0) {
  $s =~ s/\"//g;
}

foreach my $s (@h1) {
  print "\t$s";
}
print "\n";
my $lo = undef;
while (my $l = <IN>) {
  
  chomp $l;
  if ($l =~ /^N/) {

    my @a = split /\t/, $l, -1;
    my $nm   = $a[0];
    my $orf  = $a[1];
    
    print "$orf";

    my $max = -10;
    my @v = ();
    foreach my $s (@h0) {
      if (defined($M{$nm}{$s})) {
	$lo = Sets::log2($M{$nm}{$s}+1);
	#$lo = $M{$nm}{$s};
	if ($lo > $max) {
	  $max = $lo;
	}
	push @v, $lo;
	#print "\t$M{$nm}{$s}";
      } else {
	print "\t-";
	print STDERR "#                     Cannot find M{$nm}{$s}\n";
      }      
    }
    #print STDERR "# $nm: max=$max\n";
    if ($max > 0) {
      foreach my $s (@v) {
	$s = $s/$max;
      }
    }
    print "\t" . join("\t", @v);
    print "\n";
  }
  
}
close IN;


