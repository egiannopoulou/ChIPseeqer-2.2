open IN, "bunzip2 -c $ARGV[0] |" or die "Cannot open $ARGV[0]\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
 
  
  if (($a[0] =~ /^\>/) || 
      (($a[6] eq "snp") && ($a[18] !~ /dbsnp/) && ($a[25] =~ /SENSE/)) || 
      ((($a[6] eq "del") || ($a[6] eq "ins")) && ($a[18] !~ /dbsnp/) && ($a[25] =~ /FRAMESHIFT/)) )  {

    print "$l\n";
  }
}
close IN;


