#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

my $col      = $ARGV[0];
my $i_nbbins = $ARGV[1];
my $i_min    = $ARGV[2];
my $i_max    = $ARGV[3];

# initialize 
my @count = ();
for (my $b=0; $b <$i_nbbins; $b++) {
  $count[$b] = 0;
}

# calculate the size of the bin from the 
my $f_binsize = ($i_max - $i_min) / $i_nbbins;

my $l = <STDIN>;
my $total = 0;
while (my $l = <STDIN>) {
  
  # get value
  my @a = split /\t/, $l;
  my $c = $a[$col];

  for (my $b=0; $b <$i_nbbins; $b++) {
    if ( ($c > $i_min + $b * $f_binsize) && 
	 ($c < $i_min + ($b+1) * $f_binsize)) {
      $count[$b] ++;		
      last;
    }
  }
  $total++;
}
    

# create an array of (bin middle, bin freq)
my $t     = 0;
my @dist  = ();
my @fracs = ();
for (my $b=0; $b <$i_nbbins; $b++) {	
  my @tmp =( $i_min + $b * $f_binsize + $f_binsize/2.0, $count[$b]/$total);
  #print "$tmp[0]\t$tmp[1]\n";
  push @fracs, $tmp[1];
}

my $e = Sets::frac_entropy(\@fracs);

print "$e\n";
