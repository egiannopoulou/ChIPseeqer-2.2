use XML::DOM;

use strict;

my %T = ();
open IN, $ARGV[1] or die "Cannot open $ARGV[1]\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  $T{$a[0]} = $a[1];
}
close IN;



my $parser = new XML::DOM::Parser;
my $doc    = $parser->parsefile($ARGV[0]);
  

# drugs

my $drugs = $doc->getElementsByTagName("drugs")->item(0);;
foreach my $d ($drugs->getElementsByTagName("drug")) {
  my $name  = $d->getElementsByTagName("name")->item(0);
  
  if ($name) {
    $name = $name->getChildNodes->item(0)->getData;
  
    
    my $targets = $d->getElementsByTagName("targets")->item(0);
    if ($targets) {
      foreach my $t ($targets->getElementsByTagName("target")) {
	my $id = $t->getAttributeNode ("partner")->getValue();
	print "$name\t$id\t$T{$id}\n";
      }
    }
    #<target partner="54">

    #print "$id\t$gene\n";
  }
}
