#!/usr/bin/perl
BEGIN{ $home = `echo \$HOME`; chomp $home}
use lib "$home/PERL_MODULES";

use Table;
use Sets;

open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
my $l = <IN>; print $l;
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  my $r = \@a;
  
  my $n = shift @$r;
  
  my @a = (); 
  foreach my $t (@$r) {
    push @a, $t; #Sets::log2($t+1);
  }
  
  my $s = Sets::stddev(\@a);
  
  if ($s > $ARGV[1]) {
    print "$n\t" . join("\t", @$r);
    if ($ARGV[2] ne "") {
      print "\t$s";
    }
    print "\n";
  }
  
}
close IN;


