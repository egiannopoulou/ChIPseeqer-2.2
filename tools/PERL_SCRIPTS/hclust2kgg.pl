#!/usr/bin/perl

BEGIN{ $home = `echo \$HOME`; chomp $home}

use lib "$home/PERL_MODULES";
use Getopt::Long;
use strict;

my $cdtfile       = undef;
my $gtrfile       = undef;
my $atrfile       = undef;
my $nummaxclust   = 10;

# handling missing arguments
if (@ARGV == 0) {
  die "Usage: perl hlust2kgg --cdt=FILE --gtr=FIRE --clusters=INT\n";
}

GetOptions("cdt=s"         => \$cdtfile,
	   "gtr=s"	   => \$gtrfile,
	   "atr=s"         => \$atrfile,
	   "clusters=s"    => \$nummaxclust	);

# how many genes do we have, read CDT
my $numgenes = 0;
my %SETS = ();

open IN, $cdtfile;
if (defined($gtrfile)) {

  my $l = <IN>;
  $l = <IN>;  
  
  while (my $l = <IN>) {  
    chomp $l;
    my @a = split /\t/, $l, -1;
    push @{$SETS{$a[0]}}, $a[1];
    $numgenes ++;
  }

} else {

  
  my $l = <IN>;  chomp $l;
  my @a = split /\t/, $l, -1;

  $l = <IN>;  chomp $l;
  my @b = split /\t/, $l, -1;
  for (my $i=4; $i<@a; $i++) {
    push @{$SETS{$b[$i]}}, $a[$i];
    #print "$b[$i] <0 $a[$i]\n";
    $numgenes++;
  }
  
  

}


close IN;




# now agglomerate gene sets
if (defined($gtrfile)) {
  open IN, $gtrfile;
} else {
  open IN, $atrfile;  
}
my $numclust = $numgenes;
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;

  if (!defined($SETS{$a[1]})) {
    die "pb SETS{$a[1]} not efined\n";
  }
  push @{$SETS{$a[0]}}, @{$SETS{$a[1]}};
  push @{$SETS{$a[0]}}, @{$SETS{$a[2]}};
  
  $SETS{$a[1]} = undef;
  $SETS{$a[2]} = undef;
  
  $numclust --;
  
  if ($numclust == $nummaxclust) {
    my $p = 0;
    print "GENE\tEXP\n";
    foreach my $k (keys(%SETS)) {
      if (defined($SETS{$k})) {
	foreach my $r (@{$SETS{$k}}) {
	  print "$r\t$p\n";
	}
	$p ++;
      }
      
    }
    
    last;
  }
  
}
close IN;

