#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
my $l = <IN>; print $l;
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  
  if ($a[1] == 2) {
    $a[1] = 0;
  }
  
  print join("\t", @a) . "\n";
  
}
close IN;

