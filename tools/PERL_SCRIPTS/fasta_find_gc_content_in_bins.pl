#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

use Fasta;

my $m = $ARGV[1];
my $fa = Fasta->new;
$fa->setFile($ARGV[0]);

while (my $a_ref = $fa->nextSeq()) {
    my ($n, $s) = @$a_ref;
 
    
    print ">$n\n";
    my @a = split //, $s;

    my $l = int(length($s)/$m);

    for (my $i=0; $i<$m; $i++) {
      my %CO = ();
      
      for (my $j=0; $j<$l; $j++) {
	
	my $idx = $i*$l+$j;
	$CO{$a[$idx]} ++	

      }
      
      my $gc = ($CO{G} + $CO{C})/$l;
      my $g  = $CO{G}/$l;
      my $c  = $CO{C}/$l;
      print "$i\t$gc\t$g\t$c\n";
      
    }
   
}
