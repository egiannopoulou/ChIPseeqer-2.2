#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

use Getopt::Long;

if (@ARGV == 0) {
  die "Args --misvrfile=FILE --refgene=FILE --prefix=STR\n";
}

my $mirsvrfile = undef;
my $refgene   = undef;
my $prefix    = undef;

GetOptions("mirsvrfile=s" => \$mirsvrfile,
	   "refgene=s" => \$refgene,
           "prefix=s" => \$prefix);

# open refgene to get gene universe
my %GENES = ();
open IN, $refgene or die "Cannot open $refgene\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  $GENES{$a[12]} = 1;
}
close IN;



my %H = ();
my %CATS = ();
open IN, $mirsvrfile or die "Cannot open $mirsvrfile\n";
my $l = <IN>;
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;

  push @{$H{$a[3]}}, $a[1] if !Sets::in_array($a[1], @{$H{$a[3]}});
  $CATS{$a[1]} = 1;


}
close IN;

open OUT1, ">$prefix\_index.txt" or die "cannot open index\n";
open OUT2, ">$prefix\_names.txt" or die "cannot open names\n";

foreach my $m (keys(%GENES)) {
  print OUT1 "$m\t";
  if (defined($H{$m})) {
    print OUT1 join("\t", @{$H{$m}});
  }
  print OUT1 "\n";
}

foreach my $c (keys(%CATS)) {
  print OUT2 "$c\t$c\tP\n";
}

close OUT1;



close OUT2;

print "$prefix\_index.txt created\n";
print "$prefix\_names.txt created\n";
