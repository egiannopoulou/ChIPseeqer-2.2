#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;


# mouse 10090

# load UTRs

use Fasta;

my $fa = Fasta->new;
$fa->setFile($ARGV[1]);

my %SEQ = ();
while (my $a_ref = $fa->nextSeq()) {
    my ($n, $s) = @$a_ref;


    my @a = split /\ /, $n;

    #print "$a[0]>$s\n";

    $SEQ{$a[0]} = $s;
    
}



open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
my $l = <IN>;
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  
  next if ($a[4] ne "10090");
  next if (!defined($SEQ{$a[3]}));
  my $ll = $a[6]-$a[5]+1;
  #print "$a[3]\t$SEQ{$a[3]}\t$a[5]\t$ll\n";
  my $ss = substr($SEQ{$a[3]}, $a[5]-1, $a[6]-$a[5]+1);

  print "$l\t$ss\n";


}
close IN;

