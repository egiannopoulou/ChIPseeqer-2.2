my $sum = 0;
open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  
  my $l = $a[2] - $a[1];
  $sum += $l;

}
close IN;

print "$sum\n";
