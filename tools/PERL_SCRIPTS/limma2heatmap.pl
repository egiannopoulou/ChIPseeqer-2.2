open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";

my $l = <IN>; chomp $l;
my @a = split /\t/, $l;
my $idx = -1;
for (my $i=0; $i<@a; $i++) {
  if ($a[$i] eq "t") {
    $idx = $i;
    last;
  }
}

my $numcols = $idx - 6;

print "GENE";
for (my $i=3; $i<3+$numcols; $i++) {
  print "\t$a[$i]";
}
print "\n";

my $cnt = 0;
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;


  if ($cnt < 200) {
    print "$a[0] $a[1] $a[2]";
    for (my $i=3; $i<3+$numcols; $i++) {
      print "\t$a[$i]";
    }
    print "\n";
  } else {
    last;
  }

  $cnt ++;
}
close IN;

