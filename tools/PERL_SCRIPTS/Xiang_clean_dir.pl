#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

my $allfiles = Sets::getFiles("*");
foreach my $f (@$allfiles) {
  
  my ($dir,$file) = $f =~ /^(\d{3})\_*(.*?)$/;

  if (! -e $dir) { mkdir $dir };
  system("mv $f $dir/$file"); 

}
