#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;


open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";

my $cnt = 0;
my $sum = 0;
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l;

  next if ($a[1] !~ /mRNA/);
  
  if ($a[4] =~ /$ARGV[1]/) {
    $cnt++;
  }
  $sum ++;
  
}
close IN;


my $ra = 100 * $cnt / $sum;
print "$ARGV[0]\t$ra\n";
