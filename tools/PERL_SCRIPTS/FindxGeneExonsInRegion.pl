#!/usr/bin/perl
BEGIN{ $home = `echo \$HOME`; chomp $home}
use lib "$home/PERL_MODULES";

use Sets;
use strict;

use Getopt::Long;

if (@ARGV == 0) {
  die "Args --chr=STR --start=INT --end=INT --genefiles=FILE1,FILE2\n";
}
my $chr   = undef;
my $start = undef;
my $end   = undef;
my $genefiles = undef;
my $prom = 1;
GetOptions("start=s" => \$start,
           "end=s"   => \$end,
	   "prom=s"  => \$prom,
	   "chr=s"   => \$chr,
	   "genefiles=s" => \$genefiles);

my @a_files = split /\,/, $genefiles; 

foreach my $file (@a_files) {  # cycle thru annot files

  
  open IN, $file or die "cannot open $file\n";
  
  my $idxchr   = 2;
  my $idxexons = 9;
  my $idxstrand = 3;
  if ($file =~ /knownGene/) {
    $idxchr --;
    $idxexons --;
    $idxstrand--;
  }

  while (my $l = <IN>) {
    
    my @a = split /\t/, $l, -1;
    
    my $n    = $a[1];
    my $c    = $a[$idxchr];
    next if ($c ne $chr);  # skip if not right chr
    my $fr   = $a[3];
    my $e_st = $a[9];
    my $e_en = $a[10];
    my $e_fr = $a[15];
    
    $e_st =~ s/\,$//;
    $e_en =~ s/\,$//;
    $e_fr =~ s/\,$//;
    
    my @a_e_st = split /\,/, $e_st;
    my @a_e_en = split /\,/, $e_en;
    my @a_e_fr = split /\,/, $e_fr;
    
    my $ts_st = $a[4];
    my $ts_en = $a[5];
    my $tss   = $a[4];
    if ($a[$idxstrand] eq "-") {
      $tss = $a[5];
    }
    my $promst = $tss-1000;
    my $promen = $tss+1000;

    next if (!Sets::sequencesOverlap($start, $end, $ts_st, $ts_en));  #  skip if ts not in interval

    if (($prom == 1)  && Sets::sequencesOverlap($start, $end, $promst, $promen)) {
      print "$chr\t$promst\t$promen\n";
    }
    
    for (my $i=0; $i<@a_e_st; $i++) {
      my $ex_st = $a_e_st[$i]-100;
      my $ex_en = $a_e_en[$i]+100;
      if (Sets::sequencesOverlap($start, $end, $ex_st, $ex_en)) {	
	print "$chr\t$ex_st\t$ex_en\n";
      }      
    }
    
  } 

  close IN;

} # loop over
