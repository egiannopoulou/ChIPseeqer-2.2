#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;


my $cnt = 0;
open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";


while (my $l = <IN>) {
  chomp $l;
  my @a = split //, $l, -1;

  print "$cnt";
  foreach my $c (@a) {
    $c = uc($c);
    if ($c eq "A") {
      print "\t1\t0\t0\t0";
    } elsif ($c eq "C") {
      print "\t0\t1\t0\t0";
    } elsif ($c eq "G") {     
      print "\t0\t0\t1\t0";
    } elsif ($c eq "T") {
      print "\t0\t0\t0\t1";      
    } else {
      print "\t0\t0\t0\t0";
    }

  }

  print "\n";

  $cnt ++;
}
close IN;

