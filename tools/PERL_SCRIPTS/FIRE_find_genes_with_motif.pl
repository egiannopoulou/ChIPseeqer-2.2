#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;


use Getopt::Long;

if (@ARGV == 0) {
  die "Args --expfile=FILE --array=FILE\n";
}
my $expfile = undef;
my $array   = undef;
my $motif   = undef;
my $bins    = undef;

GetOptions("expfile=s" => \$expfile,
	   "motif=s"   => \$motif,
	   "bins=s"    => \$bins,
           "array=s"   => \$array);

my @a_bins = ();
my %EXP = ();
my %GENES_TOKEEP = ();
if (defined($bins)) {
  @a_bins = split /\,/, $bins;
  open IN, $expfile or die "Cannot open $expfile\n";
  while (my $l = <IN>) {
    chomp $l;
    my @a = split /\t/, $l, -1;
    $EXP{$a[0]} = $a[1];
    if (Sets::in_array($a[1], @a_bins)) {
      $GENES_TOKEEP{$a[0]} = 1;
      #print "keep $a[0]\n";
    }
  }
  close IN;


}

my %NM2PS = ();
if (defined($array)) {
  open IN, $array or die "Cannot open $array\n";
  while (my $l = <IN>) {
    chomp $l;
    my @a = split /\t/, $l, -1;
    my $ps = shift @a;
    foreach my $r (@a) {
      push @{$NM2PS{$r}}, $ps;
    }
  }
  close IN;
}

my $ff      = Sets::filename($expfile);
my $profile = "$expfile\_FIRE/DNA/$ff.profiles";

open IN, $profile or die "Cannot open $profile\n";
my %GENE = ();
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  
  if ($a[0] eq $motif) {
    $GENE{$a[1]} ++;
  }

}
close IN;

if (!defined($array)) {
  print join("\n", keys(%GENE)) . "\n";
} else {
  my @a_genes = keys(%GENE);
  foreach my $g (@a_genes) {
    
    if (defined($bins) && !defined($GENES_TOKEEP{$g})) {
      next;
    }
    
    if (defined($NM2PS{$g})) {
      print join("\n", @{$NM2PS{$g}}) . "\n";
    }
  }
}
