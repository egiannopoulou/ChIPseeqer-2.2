#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

my %H = ();
open IN, $ARGV[1] or die "Cannot open $ARGV[1]\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  # 12
  $H{$a[1]} = $a[12];
}
close IN;



my %TAB = ();
open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  
  #my $o = $H{$a[0]};

  my @b = @a;
  shift @b;
  push @{$TAB{$a[0]}}, \@b;

}
close IN;

print "Transcript\tGene\tMatches\n";
foreach my $t (keys(%TAB)) {
  my $o = $H{$t};
  print "$t\t$o\t";
  my @a_ff = ();
  foreach my $m (@{$TAB{$t}}) {
    my $txt = join(",", @$m);
    push @a_ff, $txt;
  }
  print join("/", @a_ff);
  print "\n";
  
}
