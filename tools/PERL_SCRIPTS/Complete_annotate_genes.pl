#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;


# annotate using one of the gene files

use Getopt::Long;

my $f = Sets::loadBLOSUM62("/Users/ole2001/PERL_MODULES/PROGRAMS/BLAST/data/BLOSUM62");

if (@ARGV == 0) {
  die "Args --resfile=FILE --genefile=FILE\n";
}
my $resfile =  undef;
my $genefile = undef;
my $verbose = undef;
GetOptions("resfile=s" => \$resfile,
	   "verbose=s" => \$verbose,
           "genefile=s" => \$genefile);

# load res file
my %POS = ();
open IN, $resfile or die "Cannot open $resfile\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  $POS{"$a[0]\t$a[1]"} = 1;
 
}
close IN;



# load gene file
my %ANN = ();
open IN, "bunzip2 -c $genefile |" or die "Cannot open $genefile\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;

  my $p = "$a[3]\t$a[4]";
  #print "$p\n";
  next if ($a[6] eq "ref");
  next if ($a[15] ne "CDS");
  next if ($a[18] eq "SYNONYMOUS");

  if (defined($POS{$p})) {
    $ANN{$p} = \@a;

    if ($verbose ==1 ) {
      print STDERR "#  found $l\n";
    }
  }
 
}
close IN;

# reload res file
my %POS = ();
open IN, $resfile or die "Cannot open $resfile\n";
my $l = <IN>; chomp $l;
print "$l\tAAchange\tBLOSUM62\tPFAM\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  my $p = "$a[0]\t$a[1]";
  foreach my $r (@a) {
    if ($r eq "") {
      $r = "-";
    }
  }
  print join("\t", @a);
  if ($a[2] =~ /^snp/) {
    print "\t" . $ANN{$p}->[21]. $ANN{$p}->[20] .  $ANN{$p}->[22];
    print "\t" . $f->{$ANN{$p}->[21]}{$ANN{$p}->[22]};
  } else {
    print "\tNA\tNA";
  }
  print "\t" . $ANN{$p}->[24];
  
  print "\n";
  
}
close IN;





