#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;
use Getopt::Long;


# load refgene

my $refgene = undef;
my $matrix  = undef;
my $profile = undef;
my $txt     = undef;

GetOptions("refgene=s"   => \$refgene,
	   "matrix=s"    => \$matrix,
	   "txt=s"       => \$txt,
	   "profile=s"   => \$profile);

# refgene
open IN, $refgene or die "Cannot open $refgene\n";
my %GENES = ();
my %NM2GENE = ();
my $idxgene = 12;
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  $GENES{$a[$idxgene]}  = 1;
  $NM2GENE{$a[1]} = $a[$idxgene];
}
close IN;

# profile gene based
open IN, $profile or die "Cannot open $profile\n";
my $l2 = <IN>;
my %H2 = ();
chomp $l2;
my @h2 = split /\t/, $l2;

while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;

  $H2{ $a[0] } = $a[1];

}
close IN;



open IN, $matrix or die "Cannot open $matrix\n";
my $l = <IN>;
chomp $l;
print "$l\t$h2[1]\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;

  print join("\t", @a);
  my $gene = $NM2GENE{$a[0]};

  if (defined($H2{$gene}) && ($H2{$gene} > 0)) {
    if (defined($txt)) {
      print "\t$txt";
    } else {
      print "\t1";
    }

  } else {
     if (defined($txt)) {
      print "\t-";
    } else {
      print "\t0";
    }
  }

  print "\n";
}
close IN;

