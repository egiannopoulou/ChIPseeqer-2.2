#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

my @LIST = ();

open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
my $num = 0;
while (my $l = <IN>) {
  chomp $l;

  $num++;
  #if (($num % 1000) == 0) {
  #  print STDERR "# $num reads processed         \r";
  #}
  next if (Sets::VDJsimilarity($l) < 0.8);

  print "$l\n";

}
close IN;
