#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

use Getopt::Long;

if (@ARGV == 0) {
  die "Args --mutations=FILE --domaindata=FILE\n";
}

my $mutations  = undef;
my $domaindata = undef;
my $nm         = undef;

GetOptions("mutations=s"  => \$mutations,
	   "nm=s"         => \$nm,
           "domaindata=s" => \$domaindata);

my @a_muts = split /\,/, $mutations;
my @a_pos  = ();
my $i = 0;
foreach my $m (@a_muts) {
  my @aas = split //, $m;
  shift @aas; pop @aas;
  $a_pos[$i] = join("", @aas);
  $i++;
  #}
}

open IN, "gunzip -c $domaindata |" or die "Cannot open $domaindata\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;  
  if ($a[1] eq $nm) {
    my $txtpos = $a[4];
    my @a_regpos = split /\,/, $txtpos;    
    my @ov = ();
    foreach my $r (@a_regpos) {
      my @xy = split /\.\./, $r;
      if (@xy == 1) {
	$xy[1] = $xy[0];
      }
      my $i = 0;
      foreach my $p (@a_pos) {
	if (Sets::sequencesOverlap($p, $p, $xy[0], $xy[1])) {
	  push @ov, $a_muts[$i];
	}
	$i++;
      }
    }  
    if (@ov > 0) {
      print "$l";    
      print "\t" . join(",", @ov);
      print "\n";          
    }

  }
}
close IN;


