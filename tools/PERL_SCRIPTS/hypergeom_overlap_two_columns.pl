#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

use Hypergeom;

open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";

my $col1 = $ARGV[1];
my $col2 = $ARGV[2];

my $ov = 0;
my $s1 = 0;
my $s2 = 0;
my $nn = 0;

while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  
  if ($a[$col1] > 0) {
    $s1 ++;
  }
 if ($a[$col2] > 0) {
    $s2 ++;
  }

  if (($a[$col1] > 0) && ($a[$col2] > 0)) {
    $ov ++;
  }

  $nn++;

}
close IN;

my $p  = Hypergeom::cumhyper($ov, $s1, $s2, $nn);

print sprintf("$ov $s1 $s2 $nn %3.2e\n", $p);
