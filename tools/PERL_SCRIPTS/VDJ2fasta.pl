#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use Fasta;
use bl2seq;

use strict;

use Getopt::Long;

if (@ARGV == 0) {
  die "Args --vdjfile=FILE --vdj=FILE\n";
}
my $vdjfile = undef;
my $vrefseq   = "IGHV-clean.fa";
my $drefseq   = "IGHD-clean.fa";
my $jrefseq   = "IGHJ-clean.fa";
my $vdj       = undef;
my $verbose   = 0;
my $showjunctions = 0;
my $outfile   = undef;
my $showname  = 0;
my $stopafter = undef;
my $singleseq = undef;

GetOptions("vdjfile=s"       => \$vdjfile,
	   "verbose=s"       => \$verbose,
	   "showname=s"      => \$showname,
	   "singleseq=s"     => \$singleseq,
	   "outfile=s"       => \$outfile,
	   "stopafter=s"     => \$stopafter,
	   "showjunctions=s" => \$showjunctions,
	   "vdj=s"           => \$vdj);



# load ref sequences
my %V = ();
my %D = ();
my %J = ();
# load V, D, J ref seq
my $fa = Fasta->new;
$fa->setFile($vrefseq);
while (my $a_ref = $fa->nextSeq()) {
    my ($n, $s) = @$a_ref;
    $V{$n} = $s;
}
$fa->dispose;

$fa = Fasta->new;
$fa->setFile($drefseq);
while (my $a_ref = $fa->nextSeq()) {
    my ($n, $s) = @$a_ref;
    $D{$n} = $s;
}
$fa->dispose;

$fa = Fasta->new;
$fa->setFile($jrefseq);
while (my $a_ref = $fa->nextSeq()) {
    my ($n, $s) = @$a_ref;
    $J{$n} = $s;
}


my @aVDJ = split /\ /, $vdj;
  
my $seqV = $V{$aVDJ[0]};
my $lenV = length($V{$aVDJ[0]});

my $seqD = $D{$aVDJ[1]};
my $lenD = length($D{$aVDJ[1]});

my $seqJ = $J{$aVDJ[2]};
my $lenJ = length($J{$aVDJ[2]});


my $cnt_processed = 0;
open IN, $vdjfile or die "Cannot open $vdjfile\n";

print ">ref\n$seqV$seqD$seqJ\n";

my @ref = split //, "$seqV $seqD $seqJ";

my $cnt = 0;
while (my $l = <IN>) {

  chomp $l;
  #print "$l\n";
  my @a = split /\t/, $l;
  my @b = split //, $a[1];

  my @newb = ();
  for (my $i=0; $i<@b; $i++) {
    if ($b[$i] =~ /[\.\-]/) {
      $b[$i] = $ref[$i];
      push @newb, uc($b[$i]);
    } elsif ($b[$i] ne " ") {
      push @newb, uc($b[$i]);
    }
  }
  
  if ($a[0] !~ /cons/) {
    $a[0] = $cnt;
  }

  $cnt ++;
  print ">$a[0]\n" . join("", @newb) . "\n";
}
  
