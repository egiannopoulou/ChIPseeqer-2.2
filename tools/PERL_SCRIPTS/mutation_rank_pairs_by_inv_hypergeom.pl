#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;
use Hypergeom;

open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
my $l = <IN>;
my @MAT = ();
my @GENES = ();
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  my $n = shift @a;
  push @GENES, $n;
  push @MAT, \@a;
}
close IN;


my $n = @GENES;
for (my $i=0; $i<$n-1; $i++) {
  for (my $j=$i+1; $j<$n; $j++) {
    my $p = _getProfileHypergeomPvalue($MAT[$i], $MAT[$j], 0);
    print "$GENES[$i]\t$GENES[$j]\t$p\n";
  }
}



sub _getProfileHypergeomPvalue {
  my ($a_ref_p1, $a_ref_p2, $neg) = @_;

  my $n1 = @$a_ref_p1;
  my $n2 = @$a_ref_p2;
  
  if ($n1 != $n2) {
    die "problen $n1 != $n2\n";
  }

  my $ov = 0;
  my $s1 = 0;
  my $s2 = 0;
  my $nn = 0;
  
  for (my $i=0; $i<$n1; $i++) {
    if ($a_ref_p1->[$i] == 1) {
      $s1++;
    }
    if ($a_ref_p2->[$i] == 1) {
      $s2++;
    }
    if (($a_ref_p1->[$i] == 1) &&  ($a_ref_p2->[$i] == 1)) {
      $ov++;
    }
    $nn++;
  }
  
  #print "$ov $s1 $s2 $nn\n";

  if ($neg == 1) {
    return (1 - Hypergeom::cumhyper($ov+1, $s1, $s2, $nn));    
  } else {
    return Hypergeom::cumhyper($ov, $s1, $s2, $nn);
  }
}

