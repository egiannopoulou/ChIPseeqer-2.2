#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;


open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
my $l = <IN>; chomp $l;
my @a = split /\t/, $l;
print "$a[0]\t$a[1]\t$a[3]\n";

my @MAT = ();
my @col1 = ();
my $maxcol1 = -100;
my @linmat = ();
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;

  my $n  = $a[0];
  my $v1 = log($a[2]);
  my $v2 = log($a[4]);

  push @MAT, [$n, $v1, $v2];

  push @linmat, ($v1, $v2);
  
  if ($v1 > $maxcol1) {
    $maxcol1 = $v1;
  }

  
  
}
close IN;

my $a_r_linmax = Sets::ranks(\@linmat);
my @matranks = ();
for (my $i=0; $i<@linmat/2; $i++) {
  $matranks[$i*2][0] = $a_r_linmax->[$i*2];
  $matranks[$i*2][1] = $a_r_linmax->[$i*2+1];  
}

my $i = 0;

my @newmat = ();
foreach my $r (@MAT) {
  
  #my $normcol1 = $r->[1]/$maxcol1;
  #my $fold     = $r->[2]/$r->[1];
  #my $normcol2 = $normcol1 * $fold;

  #print "$r->[0]\t$r->[1](" . $matranks[$i*2][0]/@linmat . ")\t$r->[2]\n";
  my $normcol1 = $matranks[$i*2][0]/@linmat;
  my $fold     = $r->[2] - $r->[1];
  my $normcol2 = $normcol1 + $fold;
  push @newmat, [$r->[0], $normcol1, $normcol2, $fold]; 

  $i++;
}


@newmat = sort { $a->[1] <=> $b->[1] } @newmat;

foreach my $r (@newmat) {
  pop @$r;
  print join("\t", @$r) . "\n";
}

