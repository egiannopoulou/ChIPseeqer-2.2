open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
my %CNT = ();
while (my $l = <IN>) {
  chomp $l;
  #my @a = split /\t/, $l, -1;

  if ($l =~ /Top V/) {
    $l = <IN>;
    $l = <IN>;
    
    chop $l;
    chop $l;

    next if (($l !~ /IGHD/) || ($l !~ /IGHJ/) || ($l !~ /IGHV/)); 
    
    #print "$l\n";
    $l =~ s/VH .*$//;
    
    $CNT{$l} ++;
   

  }

}
close IN;


my $cnt = 0;
foreach my $v (values(%CNT)) {
  $cnt += $v;
}

foreach my $k (keys(%CNT)) {
  print "$k\t" . sprintf("%3.2f", 100*$CNT{$k}/$cnt) . "\n";
}
