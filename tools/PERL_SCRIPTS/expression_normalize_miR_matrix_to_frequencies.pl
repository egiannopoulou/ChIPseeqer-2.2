#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;


open IN, $ARGV[0];
my $l = <IN>; chomp $l;
print "$l\n";

my @M = ();

my @sums = ();
while (my $l = <IN>) {
  
  chomp $l;

  my @a = split /\t/, $l, -1;

  for (my $i=1; $i<@a; $i++) {
    $sums[$i]  += $a[$i];
  }

  push @M, \@a;

} 
close IN;

foreach my $r (@M) {
  
  for (my $i=1; $i<@$r; $i++) {
    if ($sums[$i] == 0) {
      die "sums[$i] = 0?\n";
    }
    $r->[$i] = $r->[$i]/$sums[$i];
  }

  print join("\t", @$r) . "\n";

}




