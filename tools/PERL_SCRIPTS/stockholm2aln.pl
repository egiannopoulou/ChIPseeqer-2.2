#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;
use ClustalW;

my @a_n = ();
my @a_s = ();
open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\ +/, $l, -1;
  next if ($l =~ /^\#/);
  next if ($l =~ /\/\//);
  #print "$a[0]\t$a[1]\n";
  push @a_n, $a[0];
  push @a_s, $a[1];  

}
close IN;


my $cl = ClustalW->new;
$cl->setSequences(\@a_n, \@a_s);
open OUT, ">$ARGV[0].aln";
print OUT $cl->getClustalWformat();
close OUT;
