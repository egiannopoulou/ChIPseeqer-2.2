#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

use Getopt::Long;

if (@ARGV == 0) {
  die "Args --mastervar=FILE --snvfile=FILE\n";
}

my $snvfile = undef;
my $mastervar = undef;

GetOptions("mastervar=s" => \$mastervar,
           "snvfile=s" => \$snvfile);

# 
my %int = ();
open IN, $snvfile or die "Cannot open $snvfile\n";
my $l = <IN>;
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  push @{$int{$a[0]}}, \@a;
}
close IN;

open IN, "bunzip2 -c $mastervar |" or die "Cannot open $mastervar\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;

  next if ($a[0] =~ /\#/);

  my $ov = 0;
  #print "$a[2]\n";
  foreach my $in (@{$int{$a[2]}}) {
    #print "$in->[1], $in->[1], $a[3], $a[4]\n";
    if (Sets::sequencesOverlap($in->[1], $in->[1], $a[3], $a[4])) {
      print join("\t", @{$in}) . "\t", join("\t", @a) . "\n";
    }
  }
  
}
close IN;






