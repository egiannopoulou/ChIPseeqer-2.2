#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

use Getopt::Long;

if (@ARGV == 0) {
  die "Args --samples=FILE --f2=FILE\n";
}
my $samples  = undef;
my $muttable = undef;

GetOptions("samples=s"  => \$samples, 
	   "muttable=s" => \$muttable);

# get samples order
open IN, "$samples" or die "Cannot open $samples\n";
my @SAMPLES = ();
my %ORDER  = ();
my $cnt = 1;
my $l = <IN>; # header 
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  push @SAMPLES, $a[0];
  $ORDER{$a[0]} = $cnt;
  $cnt ++;
}
close IN;

print "\t" . join("\t", @SAMPLES) . "\n";
# get matrixx
open IN, $muttable or die "Cannot open $muttable\n";
$l = <IN>; chomp $l;
my @h = split /\t/, $l;
 
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;

  my @b = ();
  $b[0] = $a[0];
  for (my $i=1; $i<@a; $i++) {
    my $pos = $ORDER{$h[$i]};
    #print "pos $a[$i] $h[$i] =$pos\n";
    $b[ $pos ] = $a[ $i ];
  }

  my $i=0;
  foreach my $r (@b) {
    if ($i > 0) {
      if ($r ne "") {
	if ($r =~ /point/) {
	  $r = "1";
	} else {
	  $r = "0";
	}
	#$r = "X";
      } else {
	$r = "0";
      }
    }
    $i++;
  }

  print join("\t", @b) . "\n";
}
close IN;





