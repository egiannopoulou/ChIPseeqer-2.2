open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
#my $l = <IN>;

my @MAT = ();
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  push @MAT, \@a;
}
close IN;


my $h = shift @MAT;
for (my $i=1; $i<@$h; $i++) {
  print STDERR "open $ARGV[0].$h->[$i]\n";
  open OUT, ">$ARGV[0].$h->[$i]" or die "Cannot open $_\n";
  
  foreach my $m (@MAT) {
    if ($m->[$i] > 0) {
      print OUT "$m->[0]\n";
    }
  }
  
  close OUT;
  system("PAGE_make_input_file_from_refgene.pl --geneset=$ARGV[0].$h->[$i] --refgene=/Users/ole2001/PROGRAMS/ChIPseeqer/data/mm9/refSeq --NM=1 > $ARGV[0].$h->[$i].PAGE.txt");
  system("PAGE_make_input_file_from_refgene.pl --geneset=$ARGV[0].$h->[$i] --refgene=/Users/ole2001/PROGRAMS/ChIPseeqer/data/mm9/refSeq --isNM=1 > $ARGV[0].$h->[$i].NM.PAGE.txt");

}
