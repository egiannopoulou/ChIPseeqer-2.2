#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

use Getopt::Long;

if (@ARGV == 0) {
  die "Args --refprot=FILE\n";
}

my $refprot = undef;
GetOptions("refprot=s" => \$refprot);

my $acc        = undef;
my $nm         =  undef;
my $gene       = undef;
my $posregion  = undef;

my @regions = ();

open(IN, "gunzip -c $refprot |") || die "can't open pipe to $refprot";

while (my $l = <IN>) {
  chomp $l;
  #my @a = split /\t/, $l, -1;
  
  if ($l =~ /^ACCESSION/) {
    ($acc) = $l =~ /^ACCESSION\ +([NYX]P\_\d+)/;
    if (!defined($acc)) {
      die "proble wih $acc $l\n";
    }
  } elsif ($l =~ /^DBSOURCE/) {
    ($nm) = $l =~ /^DBSOURCE\ +REFSEQ\:\ accession\ (N[MR\_\d\.]+)/;
  } elsif ($l =~ /^\/\//) {
    
   

  } elsif ($l =~ /gene\=\"(.+)\"/) {
    #/gene="PPIAL4F"
    $gene = $1;
    #print "$acc\t$nm\t$gene\n";
  } elsif ($l =~ /\ +Region\ +?([\<\d\.\>]+)/) {
    $posregion = $1;
    $posregion =~ s/[\>\<]//g;

    $l = <IN>;
    my ($regname) = $l =~ /^\ +\/region_name\=\"(.+)\"/;
    push @regions, "Region\t$posregion\t$regname\n";
  } elsif ($l =~ /^\ +Site\ +(.+)/) {
    $posregion = $1;
    $posregion =~ s/order\(//;
    $posregion =~ s/\)//;
    $posregion =~ s/[\>\<]//g;
    $l = <IN>;
    my ($sitetype) = $l =~ /^\ +\/site_type\=\"(.+)\"/;
    if ($sitetype eq "other") {
      while ($l !~ /\/note\=/) {
	$l = <IN>;      
      }
      #print "$l\n";
      my ($note) = $l =~ /^\ +\/note\=\"(.+)\"{0,1}/;
      $note =~ s/[\"\;]$//;
      $sitetype="$note";
    }
    push @regions, "Site\t$posregion\t$sitetype\n";
  } if ($l =~ /^ORIGIN/) {
    
    my $aa = "";
    while ($l !~ /\/\//) {
      $l = <IN>;
      if ($l =~ /\ +\d+\ (.+?)$/) {
	$aa .= $1;
      }
    }
    $aa =~ s/\ //g;
    $aa = uc($aa);
    $nm =~ s/\.\d+$//;
    print "$acc\t$nm\t$gene\tSequence\t$aa\n";
    foreach my $r (@regions) {
      print "$acc\t$nm\t$gene\t$r";
    }
    
    $acc = undef;
    $nm  = undef;
    
    @regions = ();
  }


  
  
  

}
close IN;

