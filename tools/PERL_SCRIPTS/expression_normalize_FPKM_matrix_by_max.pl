#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;


open IN, $ARGV[0];
my $l = <IN>; chomp $l;
my @a = split /\t/, $l;
my $ts = shift @a;
shift @a; shift @a;
print "GENE\t" . join("\t", @a) . "\n";

my $lo = undef;
while (my $l = <IN>) {
  
  chomp $l;
  if ($l =~ /^N/) {

    my @a = split /\t/, $l, -1;
    my $nm   = $a[0];
    my $orf  = $a[1];
    
    
    my $max = -10;
    my @v = ();
    for (my $i=3; $i<@a; $i++) {
      $lo = Sets::log2($a[$i]+1);
      if ($lo > $max) {
	$max = $lo;
      }
      push @v, $lo;
      
    }

    if ($max > 0) {
      foreach my $s (@v) {
	$s = $s/$max;
      }
    }
    print "$nm  $orf  $a[2]\t" . join("\t", @v);
    print "\n";
  }
  
}
close IN;


