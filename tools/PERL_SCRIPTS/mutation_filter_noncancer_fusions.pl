use Getopt::Long;

if (@ARGV == 0) {
  die "Args --fustable=FILE \n";
}


my $fustable = undef;
GetOptions("fustable=s" => \$fustable);

my $h_ref_cancer = Sets::getIndex("/home/ole2001/PROGRAMS/SNPseeqer/REFDATA/new_cancer_genes_2011-11-15.txt");


open IN, $fustable or die "Cannot open $fustable\n";
my $l = <IN>; 
print $l;
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  if (defined($h_ref_cancer->{$a[0]})) {
    print "$l\n";
  }
}
close IN;

