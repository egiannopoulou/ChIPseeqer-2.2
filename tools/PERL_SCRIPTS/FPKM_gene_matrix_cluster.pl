#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;


# 1. rem gene desc
# 2. filter by sd
# 3. normalize
# 4. cluster


use Getopt::Long;

if (@ARGV == 0) {
  die "Args --FPKM=FILE --f2=FILE\n";
}

my $FPKM = undef;
my $sd   = 0.5;

GetOptions("FPKM=s" => \$FPKM,
           "sd=s" => \$sd);

# rem desc
my $FPKM_nodesc = $FPKM;
$FPKM_nodesc =~ s/\.txt/\_nodesc\.txt/;
open OUT, ">$FPKM_nodesc" or die "Cannot open $FPKM_nodesc\n";
open IN, $FPKM or die "Cannot open $FPKM\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  my $n = shift @a;
  shift @a;
  print OUT "$n\t" . join("\t", @a) . "\n";
}
close IN;
close OUT;

# sd
my $FPKM_sd = $FPKM;
$FPKM_sd =~ s/\.txt/\_sd$sd\.txt/;
my $todo = "expression_keep_genes_with_standard_deviation__above_value.pl $FPKM_nodesc $sd > $FPKM_sd";
system($todo) == 0 or die "cannot $todo\n";

system("wc -l $FPKM_sd");

# normalize
my $FPKM_sdnorm = $FPKM_sd;
$FPKM_sdnorm =~ s/\.txt/\_norm\.txt/;
$todo = "expression_normalize_RPKMs.pl < $FPKM_sd > $FPKM_sdnorm";
system($todo) == 0 or die "cannot $todo\n";

# cluster 
$todo = "cluster -f $FPKM_sdnorm -g 2 -e 2 -m c";
system($todo) == 0 or die "cannot $todo\n";





