use lib "$ENV{FIREDIR}/SCRIPTS";
use lib "$ENV{FIREDIR}/SCRIPTS/PostScript-Simple-0.07/lib";

use PostScript::Simple;
use Table;
use Sets;
use Getopt::Long;
use Fasta;
use strict;

use Getopt::Long;

if (@ARGV == 0) {
  die "Args --vdjfile=FILE\n";
}

my $vdjfile = undef;

GetOptions(
	   "vdjfile=s" => \$vdjfile,
	  );


# load VDJ
my @M = ();
open IN, $vdjfile or die "Cannot open $vdjfile\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  my @b = split //, $a[1];
  push @M, [$a[0], \@b];    
}
close IN;

my $numseqs = scalar(@M);
my $sizeseq = scalar(@{$M[0]->[1]});

my $ybase   = 50;
my $xright  = 50;
my $xleft   = 30;
my $xsize   = 595;

my $xscale  = ($xsize - $xright - $xleft) / $sizeseq;  # per charac
my $h       = 10;

my $ysize   = $h * $numseqs + $ybase + 100;

my $p = new PostScript::Simple(xsize     => $xsize,
			       ysize     => $ysize,	       
			       colour    => 1,
			       eps       => 1,
			       units     => "pt");

$p->setlinewidth(10);


my @col = ("brightred", "brightgreen", "brightblue");
$p->setcolour("black");
$p->setfont("Courrier", 8);

# header

my ($label, $r) = @{$M[0]};

my $idxcurr = 0;
my $idxprev = 0;
my $idxcol  = 0;
foreach my $c (@$r) {
  
  if ($c eq " ") {

    my $x1    = $xleft + $idxprev * $xscale; 
    my $x2    = $xleft + $idxcurr * $xscale;    
    $p->setcolour($col[$idxcol]);

    $p->line($x1, $ysize - ($ybase/2),
	     $x2, $ysize - ($ybase/2) );
    $p->setfont("Courrier", 15);   


    $p->setcolour("black");
    $p->text({ align => 'left' }, ($x1+$x2)/2-5 ,  $ysize - ($ybase/2-10), ($idxcol==0?"V":"D") ); 
    
    $idxprev = $idxcurr;
    $idxcol ++;
  }
  
  $idxcurr++;
  
}
my $x1    = $xleft + $idxprev * $xscale; 
my $x2    = $xleft + $idxcurr * $xscale;  
$p->setcolour($col[$idxcol]);  
$p->line($x1, $ysize - ($ybase/2),
	 $x2, $ysize - ($ybase/2) );
$p->setcolour("black");
$p->text({ align => 'left' }, ($x1+$x2)/2-5 ,  $ysize - ($ybase/2-10), "J" ); 


$p->setlinewidth(0.5);

# traverse data matrix
my $ri = 0;
for (my $i=0; $i<@M; $i++) {

  my ($label, $r) = @{$M[$i]};
    
  my $x1 = undef;
  my $x2 = undef;
  
  $x1    = $xleft; 
  $x2    = $xleft+$sizeseq*$xscale;
  
  my $y1 = $ri * $h;
 
  $p->setcolour("black");
  
  $p->setfont("Courrier", 6);
  if (($i > 0) && ($i < (@M-1))) {
    $label = $M[$#M]->[0]; $label =~ s/\ .+$//;
  }
  $p->text({ align => 'left' }, $xleft , $ysize - ($ybase  + $y1 - 3), $label ); 

  $p->line($x1, $ysize - ($ybase + $y1),
	   $x2, $ysize - ($ybase + $y1) );

 # $p->line($x1, $ybase + $y1,
#	   $x2, $ybase + $y1 );


  for (my $j=0; $j<@$r; $j++) {
    if (($r->[$j] ne ".") && ($r->[$j] ne "-")) {
      my $x  = $xleft + $j*$xscale;
      my $y  = $y1;
      my $col = "black";
      if ($r->[$j] eq "a") {
	$col = "red";
      } elsif ($r->[$j] eq "c") {
	$col = "green";
      } elsif ($r->[$j] eq "g") {
	$col = "blue";
      } elsif ($r->[$j] eq "t") {
	$col = "darkred";
      } 
      $p->setcolour($col);

      $p->line($x, $ysize - ($ybase + $y+3),
	       $x, $ysize - ($ybase + $y-3) );
    }
  }

  if (($i == 0) || ($i == (@M-2))) {
    $ri ++;
  }
  
  $ri++;

}


my $outeps = "$vdjfile.eps";
print "Producing $outeps\n";
$p->output("$outeps");
system("ps2pdf  -dEPSCrop -dAutoRotatePages=/None $outeps\n");
my $outpdf = $outeps; 
$outpdf =~ s/\.eps/\.pdf/;

system("open $outpdf");
