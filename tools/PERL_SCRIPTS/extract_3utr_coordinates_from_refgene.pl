#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

use Getopt::Long;

if (@ARGV == 0) {
  die "Args --refgene=FILE --genefile=FILE\n";
}

my $refgene = undef;
my $genefile = undef;
GetOptions("refgene=s"  => \$refgene,
           "genefile=s" => \$genefile);
	   



# REFGENE
#my $refgene = "refGene.txt.13FEB2011";
open IN, $refgene or die "Cannot open $refgene\n";
my %H = ();
my %SEEN = ();
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;

  my $c     = $a[2];
  my $g     = $a[12];
  my $t     = $a[1];
  my $st    = $a[3];

  my $ts_st = $a[4];
  my $ts_en = $a[5];
  my $cs_st = $a[6];
  my $cs_en = $a[7];
 
  if (defined($SEEN{$t})) {
    next;
  }

  my $nr = 0;
  if ($cs_st == $cs_en) {
    $nr = 1;
  }

  $SEEN{$t} = 1;
  #push @{$H{$g}}, \@a;

  if ($nr == 1) {
    print "$t\t$c\t$ts_st\t$ts_en\t$st\n";    
  }  elsif ($st eq '+') {
    print "$t\t$c\t$cs_en\t$ts_en\t$st\n";
  } else {
    print "$t\t$c\t$ts_st\t$cs_st\t$st\n";    
  }

}
close IN;
