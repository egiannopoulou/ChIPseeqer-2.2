#585     241     303     222     31      chr1    26582   26790   -249223831      -       L2c     LINE    L2      -14     3373    3116    1
#585     2070    95      4       0       chr1    26790   27053   -249223568      +       AluSp   SINE    Alu     1       264     -49     1
#585     241     303     222     31      chr1    27053   27137   -249223484      -       L2c     LINE    L2      -272    3115    3026    1


open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  print "$a[10] $a[11] $a[12]\t$a[5]\t$a[6]\t$a[7]\t$a[9]\n";
}
close IN;

