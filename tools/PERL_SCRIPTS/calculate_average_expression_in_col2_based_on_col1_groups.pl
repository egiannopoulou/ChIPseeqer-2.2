#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;


open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
my $l = <IN>;
my %H = ();
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  push @{$H{$a[1]}}, $a[2];
}
close IN;

foreach my $k (sort(keys(%H))) {
  print "$k\t" . Sets::average($H{$k}) . "\n";
}

