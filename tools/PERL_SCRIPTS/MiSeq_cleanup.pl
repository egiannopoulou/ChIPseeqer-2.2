#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

use Getopt::Long;

if (@ARGV == 0) {
  die "Args --sampleinfo=FILE \n";
}

my $sampleinfo = undef;
GetOptions("sampleinfo=s" => \$sampleinfo);

open IN, $sampleinfo or die "Cannot open $sampleinfo\n";
my %H = ();
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  $H{$a[0]} = $a[1];
  mkdir "$a[1]";
}
close IN;



my $a_ref = Sets::getFiles("*.gz");
foreach my $f (@$a_ref) {
  my @a = split /\./, $f;
  my $num = $a[$#a-1];
  
  system("mv $f $H{$num}") == 0 or die "cannot move\n";
  
}

