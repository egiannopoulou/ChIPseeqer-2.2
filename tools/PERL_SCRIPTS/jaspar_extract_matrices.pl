#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

# Parse http://jaspar.genereg.net/html/DOWNLOAD/all_data/matrix_only/matrix_only.txt
#>MA0002.1 RUNX1
#A  [10 12  4  1  2  2  0  0  0  8 13 ]
#C  [ 2  2  7  1  0  8  0  0  1  2  2 ]
#G  [ 3  1  1  0 23  0 26 26  0  0  4 ]
#T  [11 11 14 24  1 16  0  0 25 16  7 ]


open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
while (my $l = <IN>) {
  chomp $l;
  #my @a = split /\t/, $l, -1;
  
  if ($l =~ /^>/) {
    my @s = split /\ +/, $l;
    my $name = $s[1];
    $s[0] =~ s/\>//g;
    open OUT, ">$name-$s[0].jaspar";
    for (my $i=0; $i<4; $i++) {
      my $l = <IN>; chomp $l;
      
      $l =~ s/^.{4}//;
      $l =~ s/[\ \]]$//;
      print OUT "$l\n";
    }
    close OUT;
    

  }
  
}
close IN;

