#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

use Getopt::Long;

if (@ARGV == 0) {
  die "Args --refgene=FILE --genefile=FILE\n";
}

my $refgene = undef;
my $genefile = undef;
GetOptions("refgene=s"  => \$refgene,
           "genefile=s" => \$genefile);
	   



# REFGENE
#my $refgene = "refGene.txt.13FEB2011";
open IN, $refgene or die "Cannot open $refgene\n";
my %H = ();
my %SEEN = ();
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;

  my $g = $a[12];
  my $t = $a[1];
  if (defined($SEEN{$t})) {
    next;
  }

  $SEEN{$t} = 1;
  push @{$H{$g}}, \@a;

}
close IN;


open IN, $genefile or die "Cannot open $genefile\n";
my %TSS = ();
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  my $g = shift @a;

  if (defined($H{$g})) {
    
    foreach my $t (@{$H{$g}}) {
      
      my $c    = $t->[2]; # chr
      my $st   = $t->[3]; # strand
      
      my $tss  = $t->[4]; # TSS
      if ($st eq '-') {
	$tss = $t->[5];
      }

      if (defined($TSS{$tss})) {
	next;
      }

      $TSS{$tss} = 1;

      my $utss = Sets::max(0,$tss - 1000);
      my $dtss = $tss + 1000;
      
      print "$c\t$utss\t$dtss\tpromoter\t$g\tpromoter\n";

    }
    
    

  }

}
close IN;





