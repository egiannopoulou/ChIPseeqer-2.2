#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

use Getopt::Long;

if (@ARGV == 0) {
  die "Args --expfile=FILE --num1=INT --num2=INT -group1=STR --group2=STR --eps=INT/STR(1)\n";
}

my $expfile = undef;
my $num1    = undef;
my $num2    = undef;
my $group1  = undef;
my $group2  = undef;
my $transcripts = 0;
my $genes       = 0;
my $mirnas      = 0;
my $eps         = 1;
my $minsd       = undef;

GetOptions("expfile=s"     => \$expfile,
           "num1=s"        => \$num1,
	   "eps=s"         => \$eps,
	   "minsd=s"       => \$minsd,
	   "num2=s"        => \$num2,
	   "transcripts=s" => \$transcripts,
	   "mirnas=s"      => \$mirnas,
	   "genes=s"       => \$genes,
	   "group1=s"      => \$group1,
	   "group2=s"      => \$group2);

 
use Statistics::R ;

# create a design file
open IN, $expfile;
my $l = <IN>; chomp $l;
my $tag = $group1 . "vs" . $group2;

my @a = split /\t/, $l;
open OUT, ">design.txt";
shift @a;
if ($transcripts == 1) {
  shift @a; shift @a;
} elsif ($genes == 1) {
  shift @a;
} elsif ($mirnas == 1) {
  
}

print OUT "S\tW\t$tag\n";
my $num = 0;
foreach my $r (@a) {
  print OUT "$r\t1\t";
  if ($num < $num1) {
    print OUT "1";
  } else {
    print OUT "0";
  }
  print OUT "\n";
  $num++;
}
close OUT;

my $R = Statistics::R->new() ;

$R->startR ;


$R->send(q`library(limma)`);
$R->send(q`design <- read.table("design.txt", header=T, row.names=1)`);
$R->send("eset <- read.csv(\"$expfile\", header=T, row.names=1,check.names=F, sep=\"\\t\")");
$R->send(q`eset_ori <- eset`);
if ($transcripts == 1) {
  $R->send(q`eset <- eset[,-c(1,2)]`);
} elsif ($genes == 1) {
  $R->send(q`eset <- eset[,-c(1)]`);
} #elsif miRNAs

if (defined($minsd)) {
  $R->send("varfilter <- apply(eset, 1, sd)>$minsd");
  $R->send("eset <- eset[varfilter,]");
  $R->send("eset_ori <- eset_ori[varfilter,]");
}

#esettmp <- esettmp[varfilter,]
if ($eps eq "min") {
  $R->send("eps <- min(eset[eset>0])");
} else {
  $R->send("eps <- $eps");
}
$R->send("leset <- log2(eset+eps)");
#$R->send(q`leset <- eset`);
$R->send(q`fit <- lmFit(leset, design)`);
$R->send(q`fit <- eBayes(fit)`);
$R->send(q`numrows <- dim(eset)[1]`);

$R->send("oo <- topTable(fit, coef=\"$tag\", adjust=\"BH\", number=numrows)");
$R->send("write.table(cbind(eset_ori[oo[,1],], oo), file=\"$expfile.limma.txt\", sep=\"\\t\", quote=FALSE, col.names=NA)");

#system("table2xls.pl Cathy_table_genes_LIMMA_log2.txt --draw=1")
#$R->send(q`postscript("file.ps" , horizontal=FALSE , width=500 , height=500 , pointsize=1)`) ;
#$R->send(q`plot(c(1, 5, 10), type = "l")`) ;
#$R->send(qq`x = 123 \n print(x)`) ;

my $ret = $R->read ;

$R->stopR() ;

print STDERR "# created $expfile.limma.txt\n";
