#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";

use Sets;
use Statistics::Test::WilcoxonRankSum;

use strict;

open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";

my @hyperm = split /\,/, $ARGV[3];

my $l = undef;
my @dist   = ();
my %codes  = ();

my $in = 0;
while ($l = <IN>) { 
  chomp $l;
  $l =~ s/\r//g;
  if ($l =~ /^\[/) {
    $in = 1;
  }

  if ($in == 1) {
    if ($l =~ /^\[\ *(\d+)\]\ \#(.+?)$/) {

      #print "$2 => $1\n";
      $codes{$2} = $1;

    } elsif ($l =~ /^\[\ *(\d+)\]\ \ (0.+?)$/) {

      my $lab  = $1;
      my $rest = $2;

      #print "$lab $rest\n";

      my @a = split /\ +/, $rest;
      #shift @a;

      for (my $i=0; $i<@a; $i++) {
	$dist[$lab][$i+1] = $a[$i];
	$dist[$i+1][$lab] = $a[$i];
      }

      #print "$lab\t" . scalar(@a) . "\t$l\n";

    }
  }

}


close IN;

my @seqs = keys(%codes);


my @set1 = ();

for (my $i=1; $i<@seqs-1; $i++) { 
  my $s1 = $seqs[$i];

  next if ($s1 !~ /$ARGV[1]/);
  next if (Sets::in_array($s1, @hyperm));

  for (my $j=$i+1; $j<@seqs; $j++) { 
    my $s2 = $seqs[$j];
  
    next if ($i == $j);
    
    next if ($s2 !~ /$ARGV[1]/);
    next if (Sets::in_array($s2, @hyperm));

    my $d = $dist[ $codes{$s1} ][ $codes{$s2} ];
    

    print "$ARGV[2]\t$s1\t$s2\t$d\n";
    
    if (!defined($d)) {
      die "Problem with $s1 and $s2\n";
    }

    push @set1, $d;

  }

}


my @set2 = ();
for (my $i=1; $i<@seqs-1; $i++) { 
  my $s1 = $seqs[$i];

  next if ($s1 !~ /$ARGV[2]/);
  next if (Sets::in_array($s1, @hyperm));

  for (my $j=$i+1; $j<@seqs; $j++) { 
    my $s2 = $seqs[$j];
  
    next if ($i == $j);
    
    next if ($s2 !~ /$ARGV[2]/);
    next if (Sets::in_array($s2, @hyperm));

    my $d = $dist[ $codes{$s1} ][ $codes{$s2} ];
    

    print "$ARGV[2]\t$s1\t$s2\t$d\n";
    
    if (!defined($d)) {
      die "Problem with $s1 and $s2\n";
    }
    
    push @set2, $d;

  }

}



my $wc =  Statistics::Test::WilcoxonRankSum->new( { exact_upto => 30 } );
$wc->load_data(\@set1, \@set2);


my $prob = $wc->probability();

my $pf = sprintf '%f', Sets::log10($prob); # prints 0.091022
#print "$pf\n";
my $pf = sprintf("%3e", $prob);

print Sets::median(\@set1); print "\n";
print Sets::median(\@set2); print "\n";

print Sets::average(\@set1); print "\n";
print Sets::average(\@set2); print "\n";

print "$pf\n";


#use Statistics::TTest;
#my $ttest = new Statistics::TTest;
#$ttest->set_significance(90);
#$ttest->load_data(\@set1,\@set2);
#$ttest->output_t_test();
