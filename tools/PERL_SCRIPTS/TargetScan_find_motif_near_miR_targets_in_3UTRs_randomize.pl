#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;


# mouse 10090

# load UTRs
use Getopt::Long;

if (@ARGV == 0) {
  die "Args --f1=FILE --f2=FILE\n";
}
my $utrfile = undef;
my $motiffile1 = undef;
my $motiffile2 = undef;

my $targetfile = undef;
my $seed = undef;
my $mirna = "miR-200bc/429/548a";
my $outfile = undef;

GetOptions("utrfile=s"    => \$utrfile,
	   "seed=s"       => \$seed,
	   "outfile=s"    => \$outfile,
	   "mirna=s"      => \$mirna,
	   "targetfile=s" => \$targetfile,
	   "motiffile2=s" => \$motiffile2,
           "motiffile1=s" => \$motiffile1);

for (my $i=; $i<=100; $i++) {
  my $todo = "perl  ~/PERL_SCRIPTS/TargetScan_find_motif_near_miR_targets_in_3UTRs.pl --targetfile=Predicted_Targets_Info.txt --utrfile=UTR_Sequences.txt.longest.fa  --motiffile1=../AREs.txt --motiffile2=../HuR_kmers.txt --seed=$s --mirna=miR-126-3p --outfile=/dev/null";

if (!defined($outfile)) {
  
  $outfile = "Predicted_Targets_Info_$mirna\_AREs_Urich.txt";
  $outfile =~ s/\//\_/g;
}

open OUT, ">$outfile" or die "Cannot open $outfile\n";

use Fasta;

my $fa = Fasta->new;
$fa->setFile($utrfile);

my %SEQ = ();
while (my $a_ref = $fa->nextSeq()) {
    my ($n, $s) = @$a_ref;


    my @a = split /\ /, $n;

    #print "$a[0]>$s\n";

    $SEQ{$a[0]} = $s;
    
}


# load AREs
open IN, $motiffile1 or die "Cannot open $motiffile1\n";
my @MOTIFS1 = ();
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  push @MOTIFS1, $a[0];


}
close IN;


# load AREs
open IN, $motiffile2 or die "Cannot open $motiffile2\n";
my @MOTIFS2 = ();
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  push @MOTIFS2, $a[0];


}
close IN;

my $maxdist = 50;


my $cnttgt = 0;
my $cnttgt_withmotif1 = 0;
my $cnttgt_withmotif2 = 0;
 
open IN, $targetfile or die "Cannot open $targetfile\n";
my $header = <IN>;
srand($seed);
chomp $header;
print OUT "$header\tseed\tposAREs\tposU-Rich\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  next if ($a[0] ne $mirna);
  next if ($a[4] ne "10090");
  next if (!defined($SEQ{$a[3]}));
  my $ll = $a[6]-$a[5]+1;
  #print "$a[3]\t$SEQ{$a[3]}\t$a[5]\t$ll\n";
  my $ss = substr($SEQ{$a[3]}, $a[5]-1, $a[6]-$a[5]+1);

  my $se = $SEQ{$a[3]};


  if (defined($seed)) {
    $a[5] = int(0.5+rand(length($se)));
  }

  my @matches1 = ();
  foreach my $m (@MOTIFS1) {
    while ($se =~ /$m/g) {

      my $pos = @-[0];
      if (abs($pos-$a[5]) < $maxdist) {
	push @matches1, $pos;
      }
    }
  }
  
  my @matches2 = ();
  foreach my $m (@MOTIFS2) {
    while ($se =~ /$m/g) {

      my $pos = @-[0];
      if (abs($pos-$a[5]) < $maxdist) {
	push @matches2, $pos;
      }
    }
  }
  

  if ((@matches1 > 0) || (@matches2 > 0)) {
    print OUT "$l\t$ss";
    print OUT "\t" . join("/", @matches1);
    print OUT "\t" . join("/", @matches2);
    print OUT "\n";
  }

  $cnttgt ++;
  if (@matches1 > 0 ) {
    $cnttgt_withmotif1++;
  }
 if (@matches2 > 0 ) {
    $cnttgt_withmotif2++;
  }
}
close IN;


print STDERR "$mirna\t$cnttgt\t$cnttgt_withmotif1\t$cnttgt_withmotif2\n";
close OUT;

system("table2xls.pl $outfile");
