#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

use Getopt::Long;

if (@ARGV == 0) {
  die "Args --expfile=FILE --orthologs=FILE\n";
}
my $expfile = undef;
my $orthologs = "$ENV{HOME}/DATA/GENESETS/hum_mou_orthologs_MGD.txt";

GetOptions("expfile=s" => \$expfile,
           "orthologs=s" => \$orthologs);

# load orth
my %H = ();
open IN, $orthologs or die "Cannot open $orthologs\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  
  $H{$a[0]} = $a[1];
  
}
close IN;


open IN, $expfile or die "Cannot open $expfile\n";
my $l = <IN>; print $l;
my $num = 0;
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  if (defined($H{$a[0]})) {
    print "$H{$a[0]}\t$a[1]\n";
    $num++;
  }
}
close IN;

print STDERR "# $num orthologs found\n";


