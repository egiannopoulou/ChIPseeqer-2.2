#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;

my $a_ref1 = Sets::readSetFromCol($ARGV[0], 0);
my $a_ref2 = Sets::readSetFromCol($ARGV[1], 0);

my $a_union = Sets::getOverlapSet($a_ref1, $a_ref2);

Sets::printSet($a_union);
