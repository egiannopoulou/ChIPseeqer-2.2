#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

use Getopt::Long;

if (@ARGV == 0) {
  die "Args --dirs=DIRS --showall=INT\n";
}
my $dirs = undef;
my $showall = 0;

GetOptions("dirs=s" => \$dirs, 
	   "showall=s" => \$showall);



my @a_dirs = split /\,/, $dirs;


my %H = ();  # index containign all the data
my %SAMPLES = ();

foreach my $d (@a_dirs) {
  
  my $todo = "find $d -name \"summary*.tsv\"";

  my $todotxt = `$todo`;
  my @a_sums = split /\n/, $todotxt;
  
  foreach my $f (@a_sums) {

    my $sample = undef;

    open IN, $f or die "Cannot open $f\n";

    
    while (my $l = <IN>) {
      chomp $l;
      my @a = split /\t/, $l, -1;
      if ($l =~ /^\#SAMPLE/) {
	$sample = $a[1];
	$SAMPLES{$sample} = 1;
      }
      next if ($l =~ /^[\>\#]/);
      next if ($a[3] eq "ALL");
      next if ($l eq "");
      
      if (!defined($sample)) {
	die "odd, sample should be defined here\n";
      }

      $H{"$a[0]\t$a[1]"}{$sample} = $a[2];
      
      

    }
    close IN;
  }

}



my @fields = sort(keys(%H));

if ($showall == 0) {
  @fields = ("Miscellaneous	Gender", 
	     "Genome coverage	Gross mapping yield (Gb)",

	     "Genome coverage	Genome fraction where weightSumSequenceCoverage >= 10x",
	     "Genome coverage	Genome fraction where weightSumSequenceCoverage >= 20x",
	     "Genome coverage	Genome fraction where weightSumSequenceCoverage >= 30x",
	     "Genome coverage	Fully called genome fraction",	    

	     "Genome variations	Heterozygous SNP count",
	     "Genome variations	Heterozygous SNP novel fraction",
	     "Genome variations	SNP transitions/transversions ratio",

	     "CNV	Total CNV segment count",
	     "CNV	Fraction of novel CNV (by segment count)",

	     "Exome coverage	Exome fraction where weightSumSequenceCoverage >= 10x",
	     "Exome coverage	Exome fraction where weightSumSequenceCoverage >= 20x",
	     "Exome coverage	Exome fraction where weightSumSequenceCoverage >= 30x",
	     "Exome coverage	Fully called exome fraction",

	     "Exome variations	Heterozygous SNP count",
	     "Exome variations	Heterozygous SNP novel fraction",

	     "Functional impact	Non-synonymous SNP loci",
	     
	    );
}

my @a_samples = keys(%SAMPLES);
print "\t\t" . join("\t", @a_samples) . "\n";
foreach my $r (@fields) {

  #next unless (($r =~ /Gender/) || ());

  print "$r";
  foreach my $a (@a_samples) {
    print "\t" . $H{$r}{$a};
  }
  print "\n";
}
