#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

my @LIST = ();

open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
my $num = 0;
while (my $l = <IN>) {
  chomp $l;

  $num++;
  #if (($num % 1000) == 0) {
  #  print STDERR "# $num reads processed         \r";
  #}
  #next if (Sets::VDJsimilarity($l) < 0.8);

  my @a = split /\t/, $l, -1;
  


  $l =~ s/\-/\./g;
  push @LIST, $l;

  #print "$l\n";

}
close IN;


@LIST = sort { $a cmp $b } @LIST;


my @SEQS = ();

my $cnt = 1;
my $pl  = shift @LIST;
foreach my $l (@LIST) {
  if ($l ne $pl) {
    #print "$cnt\t$pl\n";
    push @SEQS, [$cnt, $pl];
    $pl = $l;
    $cnt = 1;
  } else {
    $cnt++;
  }
}


@SEQS = sort { $a->[0] <=> $b->[0] } @SEQS;

foreach my $s (@SEQS) {
  print "$s->[0]\t$s->[1]\n";
}
