#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

use Getopt::Long;

if (@ARGV == 0) {
  die "Args --col=INT --numbins=INT --min=FLOAT --max=FLOAT --file=FILE \n";
}

my $col      = 1;  # $ARGV[0];
my $i_nbbins = 20; # $ARGV[1];
my $i_min    = 0;  # $ARGV[2];
my $i_max    = 5;  # $ARGV[3];
my $file     = undef;
my $verbose  = 0;

GetOptions("col=s"     => \$col,
	   "verbose=s" => \$verbose,
           "numbins=s" => \$i_nbbins,
	   "min=s"     => \$i_min,
	   "file=s"    => \$file,
	   "max=s"     => \$i_max);



# initialize 
my @count = ();
for (my $b=0; $b <$i_nbbins; $b++) {
  $count[$b] = 0;
}

# calculate the size of the bin from the 
my $f_binsize = ($i_max - $i_min) / $i_nbbins;

open IN, $file or die "cannot open $file\n";
my $l = <IN>;
my $total = 0;
while (my $l = <IN>) {
  
  # get value
  my @a = split /\t/, $l;
  my $c = $a[$col];

  for (my $b=0; $b <$i_nbbins; $b++) {
    if ( ($c > $i_min + $b * $f_binsize) && 
	 ($c < $i_min + ($b+1) * $f_binsize)) {
      $count[$b] ++;		
      last;
    }
  }
  $total++;
}
close IN;    

# create an array of (bin middle, bin freq)
my $t     = 0;
my @dist  = ();
my @fracs = ();
for (my $b=0; $b <$i_nbbins; $b++) {	
  my @tmp =( $i_min + $b * $f_binsize + $f_binsize/2.0, $count[$b]/$total);
  if ($verbose == 1) {
    print "$tmp[0]\t$tmp[1]\n";
  }
  push @fracs, $tmp[1];
}

my $e = Sets::frac_entropy(\@fracs);

print "$file\t$e\n";
