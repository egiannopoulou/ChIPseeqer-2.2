#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

use Getopt::Long;

if (@ARGV == 0) {
  die "Args --refvdjfile=FILE --cmpvdjfile=FILE --refnummax=INT\n";
}

my $cmpvdjfile = undef;
my $refvdjfile = undef;
my $refnummax  = undef;
my $numclosest = 25;
my $minratio   = undef;
GetOptions("cmpvdjfile=s" => \$cmpvdjfile,
	   "refnummax=s"  => \$refnummax,
	   "numclosest=s" => \$numclosest,
	   "minratio=s"   => \$minratio,
           "refvdjfile=s" => \$refvdjfile);


my ($refpref) = split /\_/, $refvdjfile;
my ($cmppref) = split /\_/, $cmpvdjfile;

# build a probability vector based on refvdj
my $refcnt = 0;
my @REFNTFREQ = ();
open IN, $refvdjfile or die "Cannot open $refvdjfile\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split //, $l, -1;
  for (my $i=0; $i<@a; $i++) {
    if ($a[$i] eq "-") {
      $a[$i] = ".";
    }
    $REFNTFREQ[$i]{$a[$i]}++;
  }
  
  $refcnt++;
  last if (defined($refnummax) && ($refcnt == $refnummax));
}
close IN;


# build a probability vector based on cmpvdj
my @CMPNTFREQ = ();
my $cmpcnt = 0;
open IN, $cmpvdjfile or die "Cannot open $cmpvdjfile\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split //, $l, -1;
  for (my $i=0; $i<@a; $i++) {
    if ($a[$i] eq "-") {
      $a[$i] = ".";
    }
    $CMPNTFREQ[$i]{$a[$i]}++;
  }
  
  $cmpcnt++;
  #last if (defined($refnummax) && ($cnt == $refnummax));
}
close IN;


my @nts = ("a", "c", "g", "t", ".", " ");

#for (my $i=0; $i<@NTFREQ; $i++) {
#  foreach my $n (@nts) {
    #print "\t$n=" . $NTFREQ[$i]{$n};
#  }
  #print "\n";
#}


my @a_seq = ();
open IN, $cmpvdjfile or die "Cannot open $cmpvdjfile\n";
my $num = 0;
while (my $l = <IN>) {
  chomp $l;
  my @a = split //, $l, -1;
  
  my $score1 = 0;
  my $score0 = 0;
  for (my $i=0; $i<@a; $i++) {    
    if ($a[$i] eq "-") {
      $a[$i] = ".";
    }
    $score1 += $REFNTFREQ[$i]{$a[$i]} / $refcnt;   
    $score0 += $CMPNTFREQ[$i]{$a[$i]} / $cmpcnt;    
 
  }
  
  #print "$score\t$l\n";
  push @a_seq, [$num, $score1/$score0, $l];
  $num ++;
}
close IN;

@a_seq = sort { $b->[1] <=> $a->[1] } @a_seq;

print "\t$refpref consensus\t";
for (my $i=0; $i<@REFNTFREQ-1; $i++) {
  my $max_cnt = -1;
  my $max_nt  = undef;
  foreach my $n (@nts) {
    if ($REFNTFREQ[$i]{$n} > $max_cnt) {
      $max_cnt = $REFNTFREQ[$i]{$n} ;
      $max_nt  = $n;
    }
    
  }
  print "$max_nt";
}
print "\n";

my $i = 0;
while ($i < @a_seq) {
  #for (my $i=0; $i<$numclosest; $i++) {
  $a_seq[$i]->[1] = sprintf("%3.2f", $a_seq[$i]->[1]);
  print join("\t", @{$a_seq[$i]}) . "\n";

  $i++;

  # if next one 
  if (defined($minratio)) {
    if ($a_seq[$i]->[1] < $minratio) {
      last;
    }
  } elsif (defined($numclosest)) {
    if ($numclosest == $i) {
      last;
    }
  }

} 


print "\t$cmppref consensus\t";
for (my $i=0; $i<@CMPNTFREQ-1; $i++) {
  my $max_cnt = -1;
  my $max_nt  = undef;
  foreach my $n (@nts) {
    if ($CMPNTFREQ[$i]{$n} > $max_cnt) {
      $max_cnt = $CMPNTFREQ[$i]{$n} ;
      $max_nt  = $n;
    }
    
  }
  print "$max_nt";
}
print "\n";
