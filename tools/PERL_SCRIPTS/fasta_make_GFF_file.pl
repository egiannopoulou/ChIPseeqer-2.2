#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

#chr6    hg18_refGene    CDS     116680620       116681864       0.000000        -       0       gene_id "TSPYL4"; transcript_id "NM_021648"; g_id "TSPYL4"; tss_id "TSPYL4.1";
#chr6    hg18_refGene    exon    116677824       116681954       0.000000        -       .       gene_id "TSPYL4"; transcript_id "NM_021648"; g_id "TSPYL4"; tss_id "TSPYL4.1";


use Fasta;

my $fa = Fasta->new;
$fa->setFile($ARGV[0]);

open OUT, ">vector.fa";

my $coord = 1;
my $seq   = "";
while (my $a_ref = $fa->nextSeq()) {
  my ($n, $s) = @$a_ref;
  
  my $l = length($s);
  my $end = $coord + $l-1;
  
  $n =~ s/vector\-//g;
  
  if ($n !~ /backbone/) {
    print "vector\thg18_refGene\tCDS\t$coord\t$end\t0.00000\t+\t0\tgene_id \"$n\"; transcript_id \"$n\"; g_id \"$n\"; tss_id \"$n\";\n";
    print "vector\thg18_refGene\texon\t$coord\t$end\t0.00000\t+\t.\tgene_id \"$n\"; transcript_id \"$n\"; g_id \"$n\"; tss_id \"$n\";\n";
  }
  
  $coord = $end+1;
  $seq .= $s;
}
$seq = uc($seq);
print OUT ">vector\n$seq\n";
close OUT;

print STDERR "wrote vector.fa\n";
