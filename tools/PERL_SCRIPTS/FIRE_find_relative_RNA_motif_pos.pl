#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

use Getopt::Long;

if (@ARGV == 0) {
  die "Args --profile=FILE --expfile=FILE\n";
}
my $profile = undef;
my $expfile = undef;
GetOptions("profile=s" => \$profile,
           "expfile=s" => \$expfile);

my %E = ();
open IN, $expfile or die "Cannot open $expfile\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;  
  $E{$a[0]} = $a[1];
}
close IN;

open IN, $profile or die "Cannot open $profile\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  next if ($E{$a[1]} == 1);

  my @a_p = split /\-/, $a[1];

  my $len = $a_p[2] - $a_p[1];
  my $f   = 100*$a[2]/$len;
  print "$a[0]\t$f\n";
}
close IN;

