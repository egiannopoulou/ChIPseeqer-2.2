#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;
use String::Approx 'amatch';

use Getopt::Long;

if (@ARGV == 0) {
  die "Args --expfiles=FILE --cols=FILE\n";
}
my $expfiles = undef;
my $cols     = undef;
my $outfile  = undef;

GetOptions("cols=s"     => \$cols,
	   "outfile=s"  => \$outfile,
           "expfiles=s" => \$expfiles);

# expfiles
my @EXPF = (); 
open IN, $expfiles or die "Cannot open expfiles $expfiles\n";
while (my $l = <IN>) {
  chomp $l;
  push @EXPF, $l;
}
close IN;

# load colfile
my @COLS = ();
open IN, $cols or die "Cannot open $cols\n";
while (my $l = <IN>) {
  chomp $l;
  next if ($l =~ /^\#/);
  next if ($l eq "");
  my @a = split /\t/, $l;
  push @COLS, \@a;
}
close IN;

# load the full expfiles in memory why not

my %M = ();
my %G = ();
my %H = (); # headers
my $cntcol = 0;
foreach my $file (@EXPF) {
  open IN, $file or die "Cannot open $file\n";
  print "# Opended $file\n";
  # get header
  my $l = <IN>;
  chomp $l;
  my @header = split /\t/, $l, -1;
  for (my $i=3; $i<@header; $i++) {
    print "# add $header[$i]\n";
    $H{$header[$i]} = $cntcol;
    $cntcol++;
  }
  #
  
  while (my $l = <IN>) {
    chomp $l;
    my @a = split /\t/, $l, -1;    
    $G{$a[0]} = [ $a[1], $a[2] ];  
    for (my $i=3; $i<@a; $i++) {
      $M{$a[0]}{$header[$i]} = $a[$i];
    }    
  }  

}
close IN;

# matrix loaded
my @headersfromfiles = keys(%H);

# translate from colnames in colfile to colnames in expfiles
my @TRUCOLS = ();
my @TRUCOLNAMES = ();
foreach my $col (@COLS) {
  
  my $copycol = $col->[0];
  my $num     = undef;
  #if ($copycol =~ s/(^\d+\)\ )//) {
  #  $num = $1;
  #}
  
  my $trucol = undef;
  # does col match colnames form files
  print "Pattern = '$copycol'\n";
  #print "Matches\n";
  #foreach my $h (@headersfromfiles) {
  #  print "'$h'\n";
  #}
  my @tmatches = grep (/^\Q$copycol\E$/, @headersfromfiles);
  print scalar(@tmatches) . " matches found\n";
  if (@tmatches == 1) {
    print "t $copycol => " . join("\t", @tmatches); print "\n";
    $trucol = shift @tmatches;
  } elsif (@tmatches > 1) {
    die "problem col $copycol matches " . join(" //// ", @tmatches) . "\n";
  }  else {
    my @matches = amatch($copycol, @headersfromfiles);    
    if (@matches > 0) {
      print "a $copycol => " . join("\t", @matches); print "\n";
      $trucol = shift @matches;
      if (@matches > 1) {
	die "Danger, multiple matches\n";
      }
    } else {
      die "No approx matches\n";
    }
    print "Selected $trucol\n";
  }
  
  #my ($oldnum) = $trucol =~ s/(^\d+\)\ )//;

  push @TRUCOLS, "$trucol";

  #if (defined($col->[1])) {
  push @TRUCOLNAMES, $col->[1];
  #} else {
  #  die "undefined $col->[1]\n";
  #}
  
}


my @MOUT = ();


print "TRANSCRIPT\tGENE\tDESC\t";
if (@TRUCOLNAMES == 0) {
  print join("\t", @TRUCOLS) . "\n";
} else {
  print join("\t", @TRUCOLNAMES) . "\n";
}

my @NMs = keys(%M);

foreach my $n (@NMs) {
  my @a_tmp = ($n, $G{$n}->[0], $G{$n}->[1]);
  foreach my $t (@TRUCOLS) {
    push @a_tmp, $M{$n}{$t};
  }
  push @MOUT, \@a_tmp;
}




my @SMOUT = sort {$a->[1] cmp $b->[1]} @MOUT;

open OUT, ">$outfile" or die "Cannot open $outfile\n";
print OUT "TRANSCRIPT\tGENE\tDESC\t" . join("\t", (@TRUCOLNAMES>0?@TRUCOLNAMES:@TRUCOLS)) . "\n";
foreach my $m (@SMOUT) {
  print OUT join("\t", @$m) . "\n";
}
close OUT;
