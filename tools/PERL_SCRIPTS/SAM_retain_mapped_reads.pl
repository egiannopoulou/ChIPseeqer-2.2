#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
while (my $l = <IN>) {
  chomp $l;

  if ($l =~ /^\@/) {
    print "$l\n";
  } else {
    my @a = split /\t/, $l, -1;

    if ($a[2] ne "*") {
      print "$l\n";
    }
  }

}
close IN;

