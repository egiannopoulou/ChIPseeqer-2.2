open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";


my %NODES = ();
my $nodenum = 0;
my @NODENAME = ();
my @EDGES    = ();
my $numedges = 0;
my $l = <IN>;
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;

  my $g1 = $a[0];

  if (!defined($NODES{$g1})) {
    $NODENAME[$nodenum] = $g1;
    $NODES{$g1} = $nodenum++;
  }

  my $g2s = $a[13];
  
  $g2s =~ s/\"//g;
  my @a_g2s = split /[\,\ ]+/, $g2s;
  

  foreach my $g (@a_g2s) {
    if (!defined($NODES{$g})) {
      $NODENAME[$nodenum] = $g;
      $NODES{$g} = $nodenum++;
    }

    next if ($g eq "");
    #next if (@EDGES > 10);

    push @EDGES, [$NODES{$g1}, $NODES{$g}];
      

  }
  
  
  #<STDIN>;


}
close IN;
print "{
     \"nodes\": [
";
my $cnt = 0;
foreach my $n (@NODENAME) {
  print "{ \"name\": \"$n\", \"group\": 1 }";
  if ($cnt < $#NODENAME) { print ",\n" };
  $cnt++;
}
print "],\n";
print "    \"links\": [
";
$cnt = 0;
foreach my $e (@EDGES) {
  print "{ \"source\": $e->[0], \"target\": $e->[1], \"value\": 1 }";
  if ($cnt < $#EDGES) { print ",\n" };

  

  $cnt++;
}
print "]\n";
print "}
";
