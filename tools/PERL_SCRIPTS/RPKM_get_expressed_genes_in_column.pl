#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

if (@ARGV == 0) {
  die "Args: matrix col1 col2 (ratios=col1/col2)[ threshold]\n";
}

use Getopt::Long;

if (@ARGV == 0) {
  die "Args --matrix=FILE --col=INT --min=INT--outfile=FILE \n";
}

my $matrix = undef;
my $col    = undef;
my $col2   = undef;
my $header = 1;
my $outfile= undef;
my $lrth   = 1;
my $min    = 5;

GetOptions("matrix=s" => \$matrix,
           "col=s"   => \$col,
	   "min=s"     => \$min,
           "col2=s"   => \$col2,
           "header=s" => \$header,
	   "outfile=s"=> \$outfile
	  );



open IN, $matrix or die "Cannot open file";

open OUT1, ">$outfile"    or die "cannot open $outfile\n";

my $l = <IN>; chomp $l;

my @a = split /\t/, $l;

if ($header == 1) {
  print OUT1 "GENE\t$a[$col]\n";
}
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  
  print OUT1 "$a[0]";
  if ($a[$col] >= $min) {
    print OUT1 "\t1\n";
  } else {
    print OUT1 "\t0\n";
  }
}
close IN;

close OUT1;

