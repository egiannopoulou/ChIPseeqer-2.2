#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use Fasta;
use bl2seq;

use strict;

use Getopt::Long;

if (@ARGV == 0) {
  die "Args --vdjfile=FILE --vdj=FILE\n";
}
my $vdjfile = undef;
my $vrefseq   = "IGHV-clean.fa";
my $drefseq   = "IGHD-clean.fa";
my $jrefseq   = "IGHJ-clean.fa";
my $vdj       = undef;
my $verbose   = 0;
my $showjunctions = 0;
my $outfile   = undef;
my $showname  = 0;
my $stopafter = undef;
my $singleseq = undef;
my $labelo    = "R";
my $label     = "D";
my $germline  = 1;

GetOptions("vdjfile=s"       => \$vdjfile,
	   "verbose=s"       => \$verbose,
	   "germline=s"      => \$germline,
	   "label=s"         => \$label,
	   "labelo=s"        => \$labelo,
	   "showname=s"      => \$showname,
	   "singleseq=s"     => \$singleseq,
	   "outfile=s"       => \$outfile,
	   "stopafter=s"     => \$stopafter,
	   "showjunctions=s" => \$showjunctions,
	   "vdj=s"           => \$vdj);



# load ref sequences
my %V = ();
my %D = ();
my %J = ();
# load V, D, J ref seq
my $fa = Fasta->new;
$fa->setFile($vrefseq);
while (my $a_ref = $fa->nextSeq()) {
    my ($n, $s) = @$a_ref;
    $V{$n} = $s;
}
$fa->dispose;

$fa = Fasta->new;
$fa->setFile($drefseq);
while (my $a_ref = $fa->nextSeq()) {
    my ($n, $s) = @$a_ref;
    $D{$n} = $s;
}
$fa->dispose;

$fa = Fasta->new;
$fa->setFile($jrefseq);
while (my $a_ref = $fa->nextSeq()) {
    my ($n, $s) = @$a_ref;
    $J{$n} = $s;
}


my @aVDJ = split /\ /, $vdj;
  
my $seqV = $V{$aVDJ[0]};
my $lenV = length($V{$aVDJ[0]});

my $seqD = $D{$aVDJ[1]};
my $lenD = length($D{$aVDJ[1]});

my $seqJ = $J{$aVDJ[2]};
my $lenJ = length($J{$aVDJ[2]});


my $cnt_processed = 0;
open IN, $vdjfile or die "Cannot open $vdjfile\n";

#print ">Relapse Major Clone\n$seqV$seqD$seqJ\n";
if ($germline == 1) {
  print ">germline(1)\n$seqV$seqD$seqJ\n";
}

my @ref = split //, "$seqV $seqD $seqJ";

#x	x	x	REF	........................................................................................C............................T............#................C.................T.A.G....T........G............................................T..G..G.........G...A.........................ACA..C............#...C...................................................................
#966	-4.6	0.9	29	........................................................................................c............................t............#................c.................t.a.g....t........g............................................t..g..g.........g...a.........................aca..c......... ..#...c............... ...................................................

# get rid of ref
my $l = <IN>;


# select N clones closest to ref
my $cnt = 0;
while ($cnt < 10) {
  my $l = <IN>;
  chomp $l;

  my @a = split /\t/, $l;

  # sequence
  my @b = split //, $a[4];

  my @newb = ();
  for (my $i=0; $i<@b; $i++) {
    if ($b[$i] =~ /[\.\-]/) {
      $b[$i] = $ref[$i];
      push @newb, uc($b[$i]);
    } elsif ($b[$i] ne " ") {
      push @newb, uc($b[$i]);
    }
  }
  
  $cnt ++;
  print ">$a[0]-$label" . "$labelo" . "like($a[3])\n" . join("", @newb) . "\n";
}
  

# store all remaining clones
my @CLONES = ();
while (my $l = <IN>) {
  chomp $l;

  my @a = split /\t/, $l;

  # sequence
  my @b = split //, $a[4];

  my @newb = ();
  for (my $i=0; $i<@b; $i++) {
    if ($b[$i] =~ /[\.\-]/) {
      $b[$i] = $ref[$i];
      push @newb, uc($b[$i]);
    } elsif ($b[$i] ne " ") {
      push @newb, uc($b[$i]);
    }
  }
  
  $cnt ++;
  #print ">$a[0]-D (R-like) ($a[3])\n" . join("", @newb) . "\n";

  push @CLONES, [$a[3], "$a[0]-$label" . "major($a[3])",  join("", @newb) ];

}

@CLONES = sort { $b->[0] <=> $a->[0] } @CLONES;

for (my $i=0; $i<10; $i++) {
  print ">$CLONES[$i]->[1]\n$CLONES[$i]->[2]\n";
  shift @CLONES;
}

srand(1234);
my $n = @CLONES;
for (my $i=0; $i<10; $i++) {
  my $m = rand($n)+0.5;
  $CLONES[$m]->[1] =~ s/major/random/;
  print ">$CLONES[$m]->[1]\n$CLONES[$m]->[2]\n";
}


