use Getopt::Long;

if (@ARGV == 0) {
  die "Args --sampleinfo=FILE --expfile=FILE --mirnas=INT\n";
}

my $mirnas     = 0;
my $sampleinfo = undef;
my $expfile    = undef;
my $minsd      = undef;

GetOptions("expfile=s"    => \$expfile,
	   "minsd=s"      => \$minsd,
	   "mirnas=s"     => \$mirnas,
           "sampleinfo=s" => \$sampleinfo);

my $eps = "min";
if ($mirnas == 0) {
  $eps = 1;
}

# load sampleinfo
my %SAMPLES = ();
open IN, $sampleinfo or die "Cannot open $sampleinfo\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  #print "$a[6]\n";
  push @{$SAMPLES{$a[6]}}, $a[2];

}
close IN;

# read in matrix, build hidx
open IN, $expfile or die "Cannot open $expfile\n";
my $l = <IN>;
chomp $l;
my @MAT = ();

my @h = split /\t/, $l, -1;
push @MAT, \@h;
my %HIDX = ();

my $st = 2; # default for genes
if ($mirnas == 1) {
  $st = 1;
}
for (my $i=$st; $i<@h; $i++) {
  $HIDX{$h[$i]} = $i;
}
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  push @MAT, \@a;
}
close IN;

# categories
my @CATS = sort(keys(%SAMPLES));
for (my $i=0; $i<@CATS-1; $i++) {
  for (my $j=$i+1; $j<@CATS; $j++) {
    
    my $outfile = "$expfile.$CATS[$i]_$CATS[$j]";
    open OUT, ">$outfile" or die "cannot open $outfile\n";
    #print "$CATS[$i] $CATS[$j]\n";
    my @headers = (); 
    push @headers, @{$SAMPLES{$CATS[$i]}};
    push @headers, @{$SAMPLES{$CATS[$j]}};
    #print join("\t", @headers) . "\n";
    foreach my $m (@MAT) {
      print OUT "$m->[0]";
      if ($mirnas == 0) {
	print OUT "\t$m->[1]";	
      }
      foreach my $h (@headers) {
	print OUT "\t$m->[$HIDX{$h}]";
      }
      print OUT "\n";
    }
    close OUT;

    my $n1 = @{$SAMPLES{$CATS[$i]}};
    my $n2 = @{$SAMPLES{$CATS[$j]}};
    
    my $todo = "perl ~/PERL_SCRIPTS/expression_limma.pl --num1=$n1 --num2=$n2 --expfile=$outfile --mirnas=$mirnas --eps=$eps";    
    if ($mirnas == 0) {
      $todo .= " --genes=1 ";
    }
    if (defined($minsd)) {
      $todo .= " --minsd=$minsd ";
    }
    print "$todo\n";
    system($todo) == 0 or die "cannot exec $todo\n";
    
  }
}

