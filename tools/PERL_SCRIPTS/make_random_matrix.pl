#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

srand(1233);

# load genes 
my $f = "$ENV{HOME}/PROGRAMS/SNPseeqer/REFDATA/hg19/refGene.txt.17Jan2012";

my @GENES = ();
open IN, $f or die "Cannot open $f\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  
  push @GENES, $a[12];


}
close IN;


#print "GENE\tBCL6\tBCOR\tSMRT\tH3K4me1\tH3K27Ac\tH3K4me3\tH3K9Ac\tH3K79me2\tH3K27me3\tDNAm\n";



for (my $i=0; $i<100; $i++) {
  
  my $idx = rand(@GENES);
  print "$GENES[$idx]";

  for (my $j=0; $j<1; $j++) {
    
    my $d = sprintf("%3.1f", rand(100));
    #if ($i == 0) {
    #  $d = 0;
    #}
    print "\t\t$d";
  }

  print "\n";
  
}

