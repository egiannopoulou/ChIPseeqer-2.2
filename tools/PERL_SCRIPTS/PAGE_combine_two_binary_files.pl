#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

my %H = ();
open IN, $ARGV[1] or die "Cannot open $ARGV[1]\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  $H{$a[0]} = $a[1];
}
close IN;


open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
my $l = <IN>;
print "GENE\tEXP\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;

  print "$a[0]\t";
  if (($a[1] == 1) && ($H{$a[0]} == 1)) {
    print "3";
  } elsif (($a[1] == 1) && ($H{$a[0]} == 0)) {
    print "2";
  } elsif (($a[1] == 0) && ($H{$a[0]} == 1)) {
    print "1";
  } else {
    print "0";
  }
  print "\n";

}
close IN;


