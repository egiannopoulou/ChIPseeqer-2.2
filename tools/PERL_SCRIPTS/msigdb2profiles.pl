# read human genes
my %G = ();
open IN, $ARGV[1] or die "Cannot open $ARGV[1]\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  $G{$a[12]} = 1;
}
close IN;

my @genes = keys(%G);
my $dir   = "PROFILES"; mkdir $dir;

open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /[\t\ ]/, $l, -1;
  my $n = shift @a;
  shift @a;
  
  my %P = ();
  foreach my $g (@a) {
    $P{$g} = 1;
  }
  
  open OUT, ">$dir/$n.txt";
  foreach my $g (@genes) {   
    print OUT "$g\t";
    if (defined($P{$g})) {
      print OUT "1";
    } else {
      print OUT "0";
    }
    print OUT "\n";
  }
  close OUT;
  
}
close IN;





