#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

if (@ARGV == 0) {
  die "Args: psl tgtrepeats\n";
}

my $maxnumsecondary = 2;

open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
my %CNTHIGH = ();
my %CNTALL  = ();
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  next if ($a[9] !~ /chr/);

  if ($a[0] > 60) {
    $CNTHIGH{$a[9]} ++;
  }
  
  $CNTALL{$a[9]} ++;
}
close IN;

my @DIST = ();


my $num = scalar(keys(%CNTALL));
foreach my $c (keys(%CNTHIGH)) {
  $DIST[$CNTHIGH{$c}] ++;
  #print "$c\t$CNTHIGH{$c}\n";
}

my $numhigh =  scalar(keys(%CNTHIGH));
#print "$numhigh\t$num\n";
for (my $i=1; $i<=10; $i++) {
  my $rat = sprintf("%3.1f", 100 * $DIST[$i] / $num);
  print STDERR "# $i\t$DIST[$i]\t$rat%\n";
}

my $cntgood = 0;
for (my $i=1; $i<=$maxnumsecondary+1; $i++) {
  $cntgood += $DIST[$i];
}


print STDERR "# Rescued $cntgood probes\n";


# load TDT
my %TDT = ();
open IN, $ARGV[1] or die "Cannot open $ARGV[1]\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  
  # ID, seq, inter, bait loc, acc, gene, desc, strand
  $a[2] = uc($a[2]);
  $TDT{$a[1]} = [$a[5], $a[2], $a[0], $a[1], "", "", "", $a[4] ];
}
close IN;


foreach my $c (keys(%CNTHIGH)) {
  if ($CNTHIGH{$c} <= ($maxnumsecondary+1)) {
    print join("\t", @{$TDT{$c}}) . "\n";
  }
}

