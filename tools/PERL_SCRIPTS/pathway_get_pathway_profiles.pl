use strict;

# read pathways

open IN, $ARGV[1] or die "Cannot open $ARGV[1]\n";
my %PATHWAYS = ();
my %GP       = ();
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  my $g = shift @a;
  foreach my $p (@a) {
      $PATHWAYS{$p} = 1;      
      $GP{$g}{$p}   = 1;
  }
}
close IN;

# read genes
open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
my %GC = ();
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  $GC{$a[0]} = $a[1];
}
close IN;

print "GENE\tM";
foreach my $p (keys(%PATHWAYS)) {
    print "\t$p";
}
print "\n";
foreach my $g (keys(%GP)) {
    #if (defined($GP{$g})) {
	print "$g";
	if (defined($GC{$g})) {
	    print "\t1";
	} else {
	    print "\t0";
	}
	foreach my $p (keys(%PATHWAYS)) {
	    if (defined($GP{$g}{$p})) {
		print "\t1";
	    } else {
		print "\t0";
	    }
	}
	print "\n";
    #}
}



