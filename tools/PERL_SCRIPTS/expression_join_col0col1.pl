open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
my $cnt = 0;
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;

  my $n = shift @a;
  $a[0] = "$n-$a[0]";

  if ($cnt > 0) {
    for (my $i=1; $i<@a; $i++) {
      $a[$i] = sprintf("%4.3f", $a[$i]);
    }
  }
  print join("\t", @a) . "\n";
  $cnt ++;
}
close IN;

