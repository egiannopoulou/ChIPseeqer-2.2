#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

# find top miRNA
open IN, $ARGV[2] or die "Cannot open $ARGV[2]\n";
my $l = <IN>;
my $cnt = 0;
my %TOPMIR = ();
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;

  $a[0] =~ s/mir/miR/;
  
  #print "# add $a[0]\n";
  $TOPMIR{$a[0]} = $a[1];

  $cnt ++;
  last if ($cnt == 25);
}
close IN;

# load miRNAs pred
open IN, $ARGV[1] or die "Cannot open $ARGV[1]\n";
my %PRED = ();
my $hl = <IN>;
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  push @{$PRED{$a[3]}}, \@a;
}
close IN;





# now find targets
open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
print "gene\tfold\t$hl";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;

  if ($a[1] > 0) {
    
    my $g = $a[2];

    # lookup predictions for that bene
    if (defined($PRED{$g})) {
      
      foreach my $p (@{$PRED{$g}}) {
	if (!defined($TOPMIR{$p->[1]})) {
	  #print "# found $p->[1] pred, but not in top 10 miRNAs\n";
	  #print "$g\t$a[1]\t" . join("\t", @$p) . "\n";

	} else {
	  print "$g\t$a[1]\t" . join("\t", @$p) . "\n";
	}
      }
      
    }

  }

}
close IN;



