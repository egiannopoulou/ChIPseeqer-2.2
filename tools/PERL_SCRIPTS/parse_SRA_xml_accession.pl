open IN, $ARGV[1] or die "Cannot open mat_entropy.txt\n";
my %H = ();
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  $a[0] =~ s/\.fastq.sorted.bam.100k//;
  $a[0] =~ s/\/pbtech_mounts\/oelab01_scratch001\/ole2001\/SINGLECELL\///;
  print STDERR "$a[0]\n";
  $H{$a[0]} = $a[1];
}
close IN;



open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
while (my $l = <IN>) {
  chomp $l;
  #my @a = split /\t/, $l, -1;
  
  my ($acc) = $l =~ /accession=\"(.+?)\"/;
  my ($ali) = $l =~ /alias=\"(.+?)\"/;
  if (-e "/pbtech_mounts/oelab01_scratch001/ole2001/SINGLECELL/$acc.fastq.gz") {
    
    my @b = split /\./, $ali;
    my $di = 0;
    if (($b[1] eq "F2") || ($b[1] eq "P2")) {
      $di = 1;
    }

    print "$acc\t$ali\t$di\t$H{$acc}\n";
  }

}
close IN;

