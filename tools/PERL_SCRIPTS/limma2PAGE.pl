#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

use Getopt::Long;

if (@ARGV == 0) {
  die "Args --logfold=FILE --fdr=FILE --limma=FILE --pv=FLOAT --fdr=FLOAT \n";
}
my $fdr     = 0.05;
my $pv      = undef;
my $logfold = 1;
my $limma   = undef;
my $group2  = undef;
GetOptions("fdr=s"     => \$fdr,
	   "limma=s"   => \$limma,
	   "pv=s"      => \$pv,
	   "group2=s"  => \$group2,
           "logfold=s" => \$logfold);


open IN, $limma or die "Cannot open $limma\n";

my $l = <IN>; chomp $l;
my @a = split /\t/, $l;
my $idxt = -1;
for (my $i=0; $i<@a; $i++) {
  if ($a[$i] eq "t") {
    $idxt = $i;
    last;
  }
}

my $idxfdr = $idxt + 2;
my $idxpv  = $idxt + 1;
my $idxfol = $idxt - 2;

open OUT, ">$limma.PAGE.txt";
open OUTUP, ">$limma.UP.txt";
open OUTDN, ">$limma.DN.txt";
print OUTUP "$l\n";
print OUTDN "$l\n";

print OUT "GENE\tEXP\n";
my $cnt1 = 0;
my $cnt2 = 0;
my $cnt = 0;
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;

  print OUT "$a[0]";
  #if ($cnt < 200) {
  #print "\t$a[$idxfdr]\t$a[$idxfol]\n";
  if ( ((defined($pv) && ($a[$idxpv] < $pv)) || ($a[$idxfdr] < $fdr)) && (abs($a[$idxfol]) > $logfold))  {
    
    if ($a[$idxt] < 0) {
      print OUT "\t2\n";  # up in the second group
      print OUTUP "$l\n";
      $cnt2++;
    } else {
      print OUT "\t1\n";
      print OUTDN "$l\n";
      $cnt1++;
    }
    
  } else {
    print OUT "\t0\n";
  }

  $cnt ++;
}
close IN;
close OUT;
close OUTDN;
close OUTUP;

open OUT, ">$limma.PAGE.txt.cols";
print OUT "0\tStable genes\n";
print OUT "1\tDown-regulated in $group2\n";
print OUT "2\tUp-regulated in $group2\n";
close OUT;

print STDERR "# created $limma.PAGE.txt\n";
print STDERR "# created $limma.PAGE.txt.cols\n";
print STDERR "# 2 = up in second group ($group2)\n";
print STDERR "# found $cnt1 in group 1 \n";
print STDERR "# found $cnt2 in group 2 \n";
