#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

use Getopt::Long;

if (@ARGV == 0) {
  die "Args --expfile=FILE --motifs=FILE\n";
}
my $expfile = undef;
my $profile = undef;
my $motifs  = undef;
my $peaks   = undef;

GetOptions("expfile=s" => \$expfile,
	   "peaks=s"   => \$peaks,
           "motifs=s"  => \$motifs);


use Fasta;

my $fa = Fasta->new;
$fa->setFile($peaks);

my %SIZE = ();
while (my $a_ref = $fa->nextSeq()) {
  my ($n, $s) = @$a_ref;
  $SIZE{$n} = length($s);
}

my @a_mot   = split /\,/, $motifs;

my $file    = Sets::filename($expfile);
$profile = "$expfile\_FIRE/RNA/$file.profiles";
# 
my %EXP = ();
open IN, $expfile or die "Cannot open $expfile\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  $EXP{$a[0]} = $a[1];
}
close IN;

# profile
my %POS = ();
open IN, $profile or die "Cannot open $profile\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  if (Sets::in_array($a[0], @a_mot) && ($EXP{$a[1]} == 1)) {
    
    my $pos = ($a[2]+4.5)/$SIZE{$a[1]};

    #print "$pos\n";
    push @{$POS{$a[1]}}, $pos;
    

  }
}
close IN;

foreach my $p (keys(%POS)) {
  if (@{$POS{$p}} == 1) {
    #print "$p\n";
    #print "$POS{$p}->[0]\n";
    
    my @a = split /\-/, $p;
    print join("\t", @a) . "\n";
  }
}
