#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use strict;

use Sets;

my $refseq = $ARGV[0];

my %ORFPROM = ();
my %ORFNM   = ();
my %ORFLEN = ();

open IN, $refseq;
my %NM = ();
while (my $l = <IN>) {  
  chomp $l;
  my @a = split /\t/, $l, -1;

  next if ($a[2] =~ /\_/);

  my $orf = $a[12];
  my $nm  = $a[1];

  if (defined($ORFNM{$orf}{$nm})) { # we have seen this NM already
    next;
  }
  $ORFNM{$orf}{$nm} = 1;

  my $len = $a[5] - $a[4];

  my @a_tmp = ($nm, $len);

  push @{ $ORFLEN{$orf} }, \@a_tmp; 
  $NM{$nm} = \@a;
}
close IN;



foreach my $g (keys(%ORFLEN)) {
  
  my @so = sort { $b->[1] <=> $a->[1] } @{$ORFLEN{$g}};

  my $nm0 = $so[0]->[1];
  if (@so > 1) {
    my $nm1 = $so[1]->[1];
    if ($nm1 > $nm0) {
      die "Inverted\n";
    }
  }


  my $nm = $so[0]->[0];
  print join("\t", @{$NM{$nm}}) . "\n";
}
