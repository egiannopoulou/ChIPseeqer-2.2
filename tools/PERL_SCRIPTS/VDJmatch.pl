#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

use Fasta;
use Sets;
use MyBlast;
use strict;

use Getopt::Long;

if (@ARGV == 0) {
  die "Args --fastq1=FILE --fastq2=FILE\n";
}

my $fastq2 = undef;
my $fastq1 = undef;
my $numbatch = 1000;
my $ighvfile = "IGHV-clean.fa";
my $vrefseq   = "IGHV-clean.fa";
my $drefseq   = "IGHD-clean.fa";
my $jrefseq   = "IGHJ-clean.fa";
my $singleseq = undef;
my $verbose   = 0;
my $outfile   = undef;
my $numcpus   = 4;
my $debug     = 0;

GetOptions("fastq1=s"    => \$fastq1,
	   "vrefseq=s"   => \$vrefseq,
	   "verbose=s"   => \$verbose,
	   "debug=s"     => \$debug,
	   "outfile=s"   => \$outfile,
	   "numcpus=s"   => \$numcpus,
	   "singleseq=s" => \$singleseq,
	   "drefseq=s"   => \$drefseq,
	   "jrefseq=s"   => \$jrefseq,	   
	   "numbatch=s"  => \$numbatch,
           "fastq2=s"    => \$fastq2);



open IN1, "gunzip -c $fastq1 |" or die "Cannot open $fastq1\n";
open IN2, "gunzip -c $fastq2 |" or die "Cannot open $fastq2\n";

open OUT1, ">$outfile" or die "Cannot open $outfile\n";
open OUT2, ">$outfile.unmapped" or die "Cannot open $outfile.unmapped\n";

my $start = time;
my $totalcnt = 0;
my %CNT   = ();
my $exitnow = 0;
while (!$exitnow) {			# master loop
  
  my $tmpfile = Sets::getTempFile("/tmp/tmpVDJ");
  open TMP, ">$tmpfile" or die "Cannot open $tmpfile\n";
  my $cnt = 0;
  my @a_reads = ();
  while (1) {

    my $l1_1 = <IN1>; if (!$l1_1) { $exitnow = 1; last; }
    my $l2_1 = <IN2>;
    my $l1_2 = <IN1>;
    my $l2_2 = <IN2>;
    my $l1_3 = <IN1>;
    my $l2_3 = <IN2>;
    my $l1_4 = <IN1>;
    my $l2_4 = <IN2>;
    
    chomp $l1_2;
    chomp $l2_2;
    chomp $l1_1;
    next if (defined($singleseq) && ($l1_1 ne $singleseq));
    
    $l2_2 = Sets::getComplement($l2_2);
    my $readseq = "$l1_2" . "NNNNNNNNNNNNNNNNNNNNNNNNNNNNN$l2_2";

    print TMP ">$l1_1\n$readseq\n";
    
    push @a_reads, [$l1_1, $readseq];
    
    $cnt ++;
    
    if ($cnt == $numbatch) {
      last;
    }
  }				# end this loop
  close TMP;
    
  my $h_ref_V = getTopBlastMatches($tmpfile, $vrefseq, 1, $numcpus, $verbose);
  my $h_ref_D = getTopBlastMatches($tmpfile, $drefseq, 1, $numcpus, $verbose);
  my $h_ref_J = getTopBlastMatches($tmpfile, $jrefseq, 1, $numcpus, $verbose);

  foreach my $r (@a_reads) {

    if (defined($h_ref_V->{$r->[0]}) && ($h_ref_V->{$r->[0]}->[4] > 60) && defined($h_ref_D->{$r->[0]}) && defined($h_ref_J->{$r->[0]})) {
      print OUT1 "$r->[0]\t$r->[1]\n";
      print OUT1 join("\t", @{$h_ref_V->{$r->[0]}}) . "\n";
      print OUT1 join("\t", @{$h_ref_D->{$r->[0]}}) . "\n";
      print OUT1 join("\t", @{$h_ref_J->{$r->[0]}}) . "\n";    
      print OUT1 "\n";     
      $CNT{"$h_ref_V->{$r->[0]}->[2] $h_ref_D->{$r->[0]}->[2] $h_ref_J->{$r->[0]}->[2]"} ++;         
    } else {
      
      print OUT2 "$r->[0]\n";
      print OUT2 join("\t", @{$h_ref_V->{$r->[0]}}) . "\n" if defined($h_ref_V->{$r->[0]});
      print OUT2 join("\t", @{$h_ref_D->{$r->[0]}}) . "\n" if defined($h_ref_D->{$r->[0]});
      print OUT2 join("\t", @{$h_ref_J->{$r->[0]}}) . "\n" if defined($h_ref_J->{$r->[0]});
      print OUT2 "\n";     

    }
    
  }
  
 
  

  $totalcnt += $cnt;

  my $time_taken = time - $start;

  if (($verbose == 1) && (($totalcnt % $numbatch) == 0)) {
    print STDERR "# processed $totalcnt in $time_taken seconds\n";
    foreach my $c (keys(%CNT)) {
      print "$c:$CNT{$c}    \n"; 
    }
    #print "        \r";   
  }

  unlink $tmpfile if ($debug == 0);

} # while (1)

close OUT1;
close OUT2;

sub getTopBlastMatches {
  my ($tmpfile, $refseq, $evalue, $numcpus, $verbose) = @_;
 
  my $mb = MyBlast->new;
  $mb->setBlastProgram("blastn");
  
  $mb->setVerbose($verbose);
  $mb->setDatabaseDatabase($refseq);
  $mb->setNbProcessors($numcpus);
  $mb->setEvalueThreshold($evalue);
  $mb->setFilter(0);
  $mb->setGapOpening(2);
  $mb->setGapExtension(1);
  $mb->setq(-1);
  $mb->setWordLength(7);
  $mb->setQueryDatabase($tmpfile);

  if ($verbose ==1 ) {system("cat $tmpfile");}
  
  my $a_ref_reads = $mb->blastallMultiple_Unique(100); # blastall that returns top hit of each read, 100 HSP at most
  
  #open IN, $ARGV[0]; my @lines = <IN>; close IN; 
  #my $txt = join("", @lines);
  
  #my $a_ref_reads = $mb->_analyzeMultiple_UniqueXML($txt, 100);
  
  #print "qname\tqlen\thitname\tevalue\tqfrom\tqto\tqframe\tdfrom\tdto\tdframe\n";
  

  my %res = ();  # results to return
  
  foreach my $r (@$a_ref_reads) {


    my $numhsps = @{$r->{HSPS}};
    my $hsp     = $r->{HSPS};

    # top match
    #my $i1 = Sets::min($hsp->[0]->{QFROM}, $hsp->[0]->{QTO});
    #my $i2 = Sets::max($hsp->[0]->{QFROM}, $hsp->[0]->{QTO});

    # size of matching region 
    my $w  = abs($hsp->[0]->{QTO} - $hsp->[0]->{QFROM} + 1);
    my $fr = sprintf("%3.2f", 100*$w/$r->{QUERY}->{LENGTH});
    
    # identity
    my $dahsp = $mb->getHSPdata($hsp->[0]->{QSEQ}, $hsp->[0]->{DSEQ}); #  return [ $ug_qseq_len, $cnt_matches, $numins, $numdel ];
    my $id    = sprintf("%3.2f", 100*$dahsp->[1] / (length($hsp->[0]->{QSEQ}) - $dahsp->[2] - $dahsp->[3])); 
    

    # else best match is ok
    # print hsp0
    my $txt = "$r->{QUERY}->{NAME}\t$r->{QUERY}->{LENGTH}\t$r->{HIT}->{NAME}";
    
    $res{$r->{QUERY}->{NAME}} = [ $r->{QUERY}->{NAME}, $r->{QUERY}->{LENGTH}, $r->{HIT}->{NAME}, $hsp->[0]->{EVALUE}, $fr, $id,
		 $hsp->[0]->{QFROM}, $hsp->[0]->{QTO}, $hsp->[0]->{QFRAME},
		 $hsp->[0]->{DFROM}, $hsp->[0]->{DTO}, $hsp->[0]->{DFRAME}  ];
		 
		 
  } # loop over reads, read from batch
  
  return \%res;
}
