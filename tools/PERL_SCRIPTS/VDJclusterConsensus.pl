#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

use Getopt::Long;

if (@ARGV == 0) {
  die "Args --vdjfile=FILE --kggfile=FILE\n";
}

my $vdjfile = undef;
my $kggfile = undef;

GetOptions("vdjfile=s" => \$vdjfile,
	   "kggfile=s" => \$kggfile);


my %G = ();
# load kgg
open IN, $kggfile or die "Cannot open $kggfile\n";
my $l = <IN>;

while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  $G{$a[0]} = $a[1];
}
close IN;


my @nts = ("a", "c", "g", "t", ".", " ");


my $refcnt = 0;
my %REFNTFREQ = ();
open IN, $vdjfile or die "Cannot open $vdjfile\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split //, $l, -1;
  for (my $i=0; $i<@a; $i++) {
    if ($a[$i] eq "-") {
      $a[$i] = ".";
    }
    $REFNTFREQ{$G{$refcnt}}[$i]{$a[$i]}++;
  }
  
  $refcnt++;
}
close IN;

foreach my $k (sort(keys(%REFNTFREQ))) {

  if (defined($REFNTFREQ{$k})) {

    print "$k\t";
    my @ntfreq = @{$REFNTFREQ{$k}};
    for (my $i=0; $i<@ntfreq-1; $i++) {
      my $max_cnt = -1;
      my $max_nt  = undef;
      foreach my $n (@nts) {
	if ($ntfreq[$i]{$n} > $max_cnt) {
	  $max_cnt = $ntfreq[$i]{$n} ;
	  $max_nt  = $n;
	}
	
      }
      print "$max_nt";
    }
    print "\n";
  } else {
    print STDERR "# $k not defined\n";
  }
}
