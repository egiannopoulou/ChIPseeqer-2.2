open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
my $l = <IN>; print $l;
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  
  if (($a[0] =~ /^Mir\d/) || ($a[0] =~ /^Snor[ad]/)) {
    next;
  }

  print "$l\n";
}
close IN;

