#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

open IN, $ARGV[1] or die "Cannot open $ARGV[1]\n";
my $l2 = <IN>;
my %H2 = ();
chomp $l2;
my @h2 = split /\t/, $l2;
if ($ARGV[2]) {
  $h2[1] = $ARGV[2];
}
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;

  $H2{ $a[0] } = $a[1];

}
close IN;



open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
my $l = <IN>;
chomp $l;
print "$l\t$h2[1]\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;

  print join("\t", @a);
  
  if (defined($H2{$a[0]}) && ($H2{$a[0]} > 0)) {
    if ($ARGV[3]) {
      print "\tBOUND";
    } else {
      print "\t1";
    }
  } else {
    if ($ARGV[3]) {
      print "\t-";
    } else {
      print "\t0";
    }  }

  print "\n";
}
close IN;

