#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

# load refGee
open IN, $ARGV[1] or die "Cannot open $ARGV[1]\n";
my %ORF = ();
my %TSS = ();
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;

  my $n    = $a[1];
  my $o    = $a[12];

  my $tss  = undef;
  if ($a[3] eq '+') {
    $tss = $a[4];
  } else {
    $tss = $a[5];
  }
  
  # a
  #push @{$TSS{$o}{$tss}}, $tss if (defined($TSS{$o}{$tss}));

  $TSS{$n} = $tss;
  $ORF{$n} = $o;

}
close IN;

my %H = ();
open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
my $l = <IN>;
print $l;
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  my $orf = $ORF{$a[0]};
  push @{$H{$orf}}, $a[0]; # ORF GRP -> NM

}
close IN;

foreach my $o (keys(%H)) {
  foreach my $grp (keys(%{$H{$o}})) {
    my @nms = @{$H{$o}{$grp}};
    # find unque TS
    my %T = ();
    foreach my $nm (@nms) {
      next if (!defined($TSS{$nm}));
      my $tss = $TSS{$nm};
      if (!defined($T{$tss})) {  # not yet seen
	print "$nm\t$grp\n";    
      }
      $T{$tss} = 1;
    }		   
  }
}
