BEGIN{ $home = `echo \$HOME`; chomp $home}
use lib "$home/PERL_MODULES";

use Sets;
use Table;

if (@ARGV == 0) {
  die "ARgs: table fasta col\n";
}

my $ta = Table->new;
$ta->loadFile($ARGV[0]);
my $h_ref = $ta->getIndex($ARGV[2]);

use Fasta;

my $fa = Fasta->new;
$fa->setFile($ARGV[1]);

while (my $a_ref = $fa->nextSeq()) {
  my ($n, $s) = @$a_ref;
  #$n = "L$n";
  if ( defined($h_ref->{$n}) ) {
    print ">$n\n$s\n\n";
  }
}
