open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
while (my $l = <IN>) {
  chomp $l;

  next if ($l !~ /insertLength\=200\;/);
  next if ($l =~ /objStatus\=replaced/);
  my @a = split /\t/, $l, -1;
  next if ($a[0] !~ /fastq\.tgz/);
  my ($cell) = $l =~ /cell\=(.+?)\;/;

  if (!$cell) {
    die "don't know line\n";
  }

  print "$cell\t$l\n";

  mkdir "$cell" if ! -e $cell;
  
  my $todo = "wget -P $cell http://hgdownload.cse.ucsc.edu/goldenPath/hg19/encodeDCC/wgEncodeCaltechRnaSeq/$a[0]";

  system($todo) == 0 or die "cannot exec $todo\n";

}
close IN;

