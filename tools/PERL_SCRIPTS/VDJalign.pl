#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use Fasta;
use bl2seq;

use strict;

use Getopt::Long;

if (@ARGV == 0) {
  die "Args --vdjfile=FILE --f2=FILE\n";
}
my $vdjfile = undef;
my $vrefseq   = "IGHV-clean.fa";
my $drefseq   = "IGHD-clean.fa";
my $jrefseq   = "IGHJ-clean.fa";
my $vdj       = undef;
my $verbose   = 0;
my $showjunctions = 0;
my $outfile   = undef;
my $showname  = 0;
my $stopafter = undef;
my $singleseq = undef;

GetOptions("vdjfile=s"       => \$vdjfile,
	   "verbose=s"       => \$verbose,
	   "showname=s"      => \$showname,
	   "singleseq=s"     => \$singleseq,
	   "outfile=s"       => \$outfile,
	   "stopafter=s"     => \$stopafter,
	   "showjunctions=s" => \$showjunctions,
	   "vdj=s"           => \$vdj);



# load ref sequences
my %V = ();
my %D = ();
my %J = ();
# load V, D, J ref seq
my $fa = Fasta->new;
$fa->setFile($vrefseq);
while (my $a_ref = $fa->nextSeq()) {
    my ($n, $s) = @$a_ref;
    $V{$n} = $s;
}
$fa->dispose;

$fa = Fasta->new;
$fa->setFile($drefseq);
while (my $a_ref = $fa->nextSeq()) {
    my ($n, $s) = @$a_ref;
    $D{$n} = $s;
}
$fa->dispose;

$fa = Fasta->new;
$fa->setFile($jrefseq);
while (my $a_ref = $fa->nextSeq()) {
    my ($n, $s) = @$a_ref;
    $J{$n} = $s;
}


my @aVDJ = ();

if (!defined($vdj)) {
  # count
  open IN, $vdjfile or die "Cannot open $vdjfile\n";
  my %CNT = ();
  while (1) {
    my @a_l = ();
    my $l1 = <IN>; if (!$l1) { last; }; 
    my $l2 = <IN>;
    my $l3 = <IN>;
    my $l4 = <IN>;
    my $l5 = <IN>;
    
    chomp $l1;
    my @a_l1 = split /\t/, $l1, -1;
    
    chomp $l2;
    my @a_l2 = split /\t/, $l2, -1;
    
    chomp $l3;
    my @a_l3 = split /\t/, $l3, -1;
    
    chomp $l4;
    my @a_l4 = split /\t/, $l4, -1;
    
    $CNT{"$a_l2[2] $a_l3[2] $a_l4[2]"} ++;
    
  }
  close IN;
  
  my $a_ref_CNT = Sets::hash_order(\%CNT);
  foreach my $r (@$a_ref_CNT) {
    my @a = split /\ /, $r;
    
    #print "$r\t$CNT{$r}\n";
    #print "$V{$a[0]}\n";
  }
  
  print STDERR "# " . $a_ref_CNT->[ scalar(@$a_ref_CNT) - 1] . "\n";
  
  # create ref files
  @aVDJ = split /\ /, $a_ref_CNT->[ scalar(@$a_ref_CNT) - 1];
  
  
  
} else {

  
  @aVDJ = split /\ /, $vdj;
  
}


# V
my $tmprefseqV = Sets::getTempFile("/tmp/V");
open O, ">$tmprefseqV" or die "cannot open $tmprefseqV\n";
print O ">$aVDJ[0]\n$V{$aVDJ[0]}\n";
my $seqV = $V{$aVDJ[0]};
close O;
#system("cat $tmprefseqV");
my $lenV = length($V{$aVDJ[0]});

# D
my $tmprefseqD = Sets::getTempFile("/tmp/D");
open O, ">$tmprefseqD" or die "cannot open $tmprefseqD\n";
print O ">$aVDJ[1]\n$D{$aVDJ[1]}\n";
close O;
#system("cat $tmprefseqD");
my $lenD = length($D{$aVDJ[1]});

# J
my $tmprefseqJ = Sets::getTempFile("/tmp/J");
open O, ">$tmprefseqJ" or die "cannot open $tmprefseqJ\n";
print O ">$aVDJ[2]\n$J{$aVDJ[2]}\n";
close O;
#system("cat $tmprefseqJ");
my $lenJ = length($J{$aVDJ[2]});


open OUT, ">$outfile" or die "cannot open $outfile\n";

my $cnt_processed = 0;
open IN, $vdjfile or die "Cannot open $vdjfile\n";
while (1) {
  my @a_l = ();
  my $l1 = <IN>; if (!$l1) { last; }; 
  my $l2 = <IN>;
  my $l3 = <IN>;
  my $l4 = <IN>;
  my $l5 = <IN>;

  chomp $l1;
  my @a_l1 = split /\t/, $l1, -1;

  chomp $l2;
  my @a_l2 = split /\t/, $l2, -1;

  chomp $l3;
  my @a_l3 = split /\t/, $l3, -1;

  chomp $l4;
  my @a_l4 = split /\t/, $l4, -1;

  my $seq = $a_l1[1];
  if ($a_l2[11] == -1) {
    $seq = Sets::getComplement($seq);
  } elsif ($a_l2[11] != 1) {
    die "Prob\n";
  }

  # compare V to ref
  my $thisv = $a_l2[2]; $thisv =~ s/\*.+$//;
  if ($aVDJ[0] !~ /$thisv/) {
    next;
  }
  # compare D to ref
  my $thisd = $a_l3[2]; $thisd =~ s/\*.+$//;
  if ($aVDJ[1] !~ /$thisd/) {
    next;
  }
  # compare J to ref
  my $thisj = $a_l4[2]; $thisj =~ s/\*.+$//;
  if ($aVDJ[2] !~ /$thisj/) {
    next;
  }

 
 
  

  next if (defined($singleseq) && ($a_l1[0] ne $singleseq));

  if ($showname == 1) {
    print OUT "$a_l1[0]\t";
  }

  #
  # realign V region
  #
  my $tmpfile = Sets::getTempFile("/tmp/aVDJ");
  open O, ">$tmpfile" or die "cannot open $tmpfile\n";
  print O ">$a_l1[0]\n$seq\n";
  close O;

  my $bl = bl2seq->new;
  $bl->setVerbose($verbose);
  $bl->setD(0);
  my $a_ref_m1 = $bl->bl2seq($tmpfile, $tmprefseqV);
  my ($qseq, $dseq, $qstV, $qenV, $dstV) = @$a_ref_m1;

  my @a_qseq = split //, $qseq;
  my @a_dseq = split //, $dseq;

  #print "$a_l2[2] $a_l3[2] $a_l4[2]\n";
  #print "$qseq\n";
  #print "$dseq\n";
  
  my $idx = $dstV-1;
  print OUT "-" x ($dstV-1);  # match starts at dstV in ref V
  for (my $i=0; $i<@a_qseq; $i++) {
    if ($a_dseq[$i] ne '-') {
      
      if ($a_qseq[$i] eq 'n') {
	print OUT "-";
      } elsif ($a_qseq[$i] ne $a_dseq[$i]) {
	print OUT $a_qseq[$i];
      } else {
	print OUT ".";
      }
      $idx++;
    }
  }  
  print OUT "-" x ($lenV - $idx);
  # end V

  

  
  #
  # realign D region
  #
  print OUT " ";
  my $bl = bl2seq->new;
  $bl->setVerbose($verbose);
  $bl->setD(0);
  my $a_ref_m1 = $bl->bl2seq($tmpfile, $tmprefseqD);
  my ($qseq, $dseq, $qstD, $qenD, $dstD) = @$a_ref_m1;


  if ($showjunctions == 1) {
    # junction    
    #print "$qenV\t$qstD\t$qenD\n";
    print OUT substr($seq, $qenV, $qstD-$qenV);
  }

  if ($qseq ne "") {
    my @a_qseq = split //, $qseq;
    my @a_dseq = split //, $dseq;
    
    #print "$a_l2[2] $a_l3[2] $a_l4[2]\n";
    #print "$qseq\n";
    #print "$dseq\n";
    
    my $idx = $dstD-1;
    print OUT "-" x ($dstD-1);
    for (my $i=0; $i<@a_qseq; $i++) {
      if ($a_dseq[$i] ne '-') {
	
	if ($a_qseq[$i] eq 'n') {
	  print OUT "-";
	} elsif ($a_qseq[$i] ne $a_dseq[$i]) {
	  print OUT $a_qseq[$i];
	} else {
	  print OUT ".";
	}
	$idx++;
      }
    }  
    print OUT "-" x ($lenD - $idx);
  } else {
    print OUT "-" x $lenD;
  }

  
  #
  # realign J region
  #
  print OUT " ";
  my $bl = bl2seq->new;
  $bl->setVerbose($verbose);
  $bl->setD(0);
  my $a_ref_m1 = $bl->bl2seq($tmpfile, $tmprefseqJ);
  my ($qseq, $dseq, $qstJ, $qenJ, $dstJ) = @$a_ref_m1;

  if ($qseq ne "") {
    
    my @a_qseq = split //, $qseq;
    my @a_dseq = split //, $dseq;
    
    #print "$a_l2[2] $a_l3[2] $a_l4[2]\n";
    #print "$qseq\n";
    #print "$dseq\n";
    
    my $idx = $dstJ-1;
    print OUT "-" x ($dstJ-1);
    for (my $i=0; $i<@a_qseq; $i++) {
      if ($a_dseq[$i] ne '-') {
	
	if ($a_qseq[$i] eq 'n') {
	  print OUT "-";
	} elsif ($a_qseq[$i] ne $a_dseq[$i]) {
	  print OUT $a_qseq[$i];
	} else {
	  print OUT ".";
	}
	$idx++;
      }
    }  
    print OUT "-" x ($lenJ - $idx);
  } else {
    print OUT "-" x $lenJ;
  }
  
  print OUT "\n";
    
    #print join("\t", @$a_ref_m1) . "\n";
  
  
  #foreach my $r (@$a_ref_m1) {
  #  print join("\t", @$r) . "\n";
  #  last;
  #}
  #unlink $tmpfile;
  
  $cnt_processed++;

  if (($cnt_processed % 100) == 0) {
    print STDERR "# processed $cnt_processed            \r";
  }

  if (defined($stopafter) && ($cnt_processed >= $stopafter)) {
    last;
  }

  if (defined($singleseq) && ($a_l1[0] eq $singleseq)) {
    last;
  }


}
close IN;
