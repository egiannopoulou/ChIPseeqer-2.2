#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;


open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
my $l = <IN>; chomp $l;
my @h = split /\t/, $l;
shift @h;

my @SUM = ();
my $cntlines = 0;
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  shift @a;
  for (my $i=0; $i<@a; $i++) {
    $SUM[$i] += $a[$i];
  }
  $cntlines++;
}
close IN;


for (my $i=0; $i<@h; $i++) {
  my $s = $SUM[$i]/$cntlines;
  print "$h[$i]\t$s\n";
}
