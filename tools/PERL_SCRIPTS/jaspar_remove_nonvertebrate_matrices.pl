#PH0154.1        14.9612739285114        Prrx1   Helix-Turn-Helix        ; acc "P63013" ; collection "PBM_HOMEO" ; comment "Data is from Uniprobe database" ; family "Homeo
# " ; medline "18585359" ; pazar_tf_id "" ; species "10090" ; tax_group "vertebrates" ; type "universal protein binding microarray (PBM)" 

open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  
  my $txt = $a[4];

  if ($txt =~ /tax_group \"(.+?)\"/) {
    
    my $tax = $1;
    next if ($tax eq "urochordates");
    next if ($tax eq "plants");
    next if ($tax eq "insects");
    next if ($tax eq "fungi");
    next if ($tax eq "nematodes");

    print "$a[0]\t$a[1]\t$a[2]\t$1\n";
    $a[2] =~ s/\ +$//;
    my $file = "$a[2]-$a[0].jaspar";
    if (-e $file) {
      print "Found\n";
      system("mv $file VERTEBRATES/");
    } else {
      print "Not Found $file\n";
    }

  }
  
}
close IN;

