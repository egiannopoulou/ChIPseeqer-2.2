#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use PostScript::Simple;

use strict;


use Getopt::Long;

if (@ARGV == 0) {
  die "Args --treefile=FILE --f2=FILE\n";
}
my $treefile = undef;
my $ybase    = 100;
my $xbase    = 250;  # x base for dendrogram
my $barfactor = 5;

GetOptions("treefile=s" => \$treefile,
           "xbase=s"   => \$xbase,
	   "barfactor=s" => \$barfactor,
           "ybase=s"   => \$ybase);

my $treetxt = "";
open IN, $treefile or die "Cannot open $treefile\n";

while (my $l = <IN>) {
  chomp $l;
  $treetxt .= $l;
}
close IN;



my $t = Sets::readNewickTree($treetxt);

my $n = @{$t->[0]};

my @pos      = ();

my @lengths = ();
for (my $i=0; $i<$n; $i++) {
  print "$i\t$t->[0]->[$i]\t$t->[1]->[$i]->[0]\t$t->[1]->[$i]->[1]\t$t->[2]->[$i]\n";
  #$lengths[$i] = 1;
  $pos[$i] = $i;
}

@lengths = @{$t->[4]};


my $h            = 10;

my $ysize        = $ybase + 2*$h * $t->[ scalar(@$t) - 1 ] + 50;
#$ysize = Sets::max($ysize, 500);
my $xsize        = 1850;

my $w            = 10;


my $p = new PostScript::Simple(xsize     => $xsize,
			    ysize     => $ysize,
			    colour    => 1,
			    eps       => 1,
			    units     => "pt");

$p->setlinewidth(0.5);

$p->setcolour("black");


#
#  draw the tree here, using a DFS 
# 
my $treeroot = 0;
my $curlen   = 0;
&visit($treeroot, $curlen, $t->[1], $t->[3], \@lengths, $p);   # second param = children

$p->setcolour("blue");
$p->box({filled => 1}, $xbase, $ysize - ($ybase - 50 ), $xbase + 20, $ysize - ($ybase - 50 + 20 )) ; 
$p->setcolour("black");
$p->setfont("Times", 16);
$p->text($xbase+20+5, $ysize - ($ybase - 50 + 15 ), "Diagnostic clones") ; 

$p->setcolour("red");
$p->box({filled => 1}, $xbase+200, $ysize - ($ybase - 50 ), $xbase+200 + 20, $ysize - ($ybase - 50 + 20 )) ; 
$p->setcolour("black");
$p->setfont("Times", 16);
$p->text($xbase+200+20+5, $ysize - ($ybase - 50 + 15 ), "Relapse clones") ; 



sub visit {
  my ($node, $curlen, $children, $pos, $len, $p) = @_;
  
  my $verbose = 1;
  my ($l, $r) = @{ $children->[$node] };
  #my $ybase        = 100;
  my $h       = 10;
  my $s       = 5000;

  if (!defined($l) && !defined($r)) {
    print "NODE: $node, $t->[2]->[$node]\n";
    $p->setcolour("black");
    $p->setfont("Courrier", 12);
    my $x = 5+$xbase + $curlen * $s; 
    if ($t->[2]->[$node] =~ /germ/) {
      $x = $xbase - (1.3*$curlen) * $s; 
    } 
    my $y = $ysize - ( $ybase + $pos->[$node] * $h + $h/2);
    
    if ($t->[2]->[$node] =~ /germ/) {
      $t->[2]->[$node] =~ s/\_\d+\_//;
      $p->text($x, $y-4, $t->[2]->[$node]) ; 
    }
    my $col = undef;
    if ($t->[2]->[$node] =~ /\-D/) {
      $col = "blue";
    } elsif ($t->[2]->[$node] =~ /\-R1/) {
      $col = "red";
    } elsif ($t->[2]->[$node] =~ /\-R3/) {
      $col = "green";
    } else {
      #die "problem interpreting $t->[2]->[$node]\n";
      $col = "black";
    }

    $p->setcolour($col);

    my ($f) = $t->[2]->[$node] =~ /\_(\d+)\_/;

    $p->box({filled => 1}, $x, $y-4, $x+$f/$barfactor, $y+4) ; 


    return;
  }
  $p->setlinewidth(0.5);

#  print "cl = $curlen\n";
 # print "ll = $len->[$l]\n";
  #print "lr = $len->[$r]\n";

  &visit($l, $curlen + $len->[$l], $children, $pos, $len, $p);
  &visit($r, $curlen + $len->[$r], $children, $pos, $len, $p);
    $p->setcolour("black");

  print "NODE: $node, $t->[2]->[$node]\n";

  # set the y for this node as the average between the nodes of the children
  $pos->[$node] = ($pos->[$l] + $pos->[$r])/2;


  # draw two horizontal bars of height given by current node
  
 

  # nodeid => child1, child2, nodeid, cor, y

  # height children 1
  my $x1_1 = $xbase + $curlen * $s; 
  my $x2_1 = $xbase + ($curlen + $len->[$l]) * $s; 
  if ($t->[2]->[$l] =~ /germ/) {
      $x2_1 = $xbase + ($curlen - $len->[$l]) * $s; 
  }
  my $y1_1 = $ysize - ($ybase + $pos->[$l] * $h + $h/2);
  my $y2_1 = $y1_1;

  print "H1:line($x1_1, $y1_1, $x2_1, $y2_1);\n";
  $p->line($x1_1, $y1_1, $x2_1, $y2_1);

  # height children 2
  my $x1_2 = $xbase + $curlen * $s;  
  my $x2_2 = $xbase + ($curlen + $len->[$r]) * $s;
  if ($t->[2]->[$r] =~ /germ/) {
    $x2_2 = $xbase + ($curlen - $len->[$r]) * $s; 
  }
  my $y1_2 = $ysize - ( $ybase + $pos->[$r] * $h + $h/2);
  my $y2_2 = $y1_2;

  $p->line($x1_2, $y1_2, $x2_2, $y2_2);
  print "H2:line($x1_2, $y1_2, $x2_2, $y2_2);\n";

  # draw one vertical bar
  $p->line($x1_1, $y2_1, $x1_2, $y2_2);
  print "V:line($x2_1, $y2_1, $x2_2, $y2_2);\n";

}

#$p->output("toto.eps");

my $outeps = "$treefile.eps";
print "Producing $outeps\n";
$p->output("$outeps");
system("ps2pdf  -dEPSCrop -dAutoRotatePages=/None $outeps\n");
my $outpdf = $outeps; 
$outpdf =~ s/\.eps/\.pdf/;

system("open $outpdf");
