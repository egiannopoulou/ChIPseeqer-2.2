#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;


my @mot = ("[ACG][AG]TGACT[ACG][CGT]", ".[CT]GA[CGT]T[AC]A[CGT]");

my %IDX = ();
open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  if (Sets::in_array($a[0], @mot)) {
    $IDX{$a[1]} = 1;
  }
}
close IN;


open IN, $ARGV[1] or die "Cannot open $ARGV[1]\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  my $peak = "$a[0]-$a[1]-$a[2]";
  
  if (defined($IDX{$peak})) {
    print "$l\t1\n"; 
  } else {
    print "$l\t0\n"; 
  }

}
close IN;


