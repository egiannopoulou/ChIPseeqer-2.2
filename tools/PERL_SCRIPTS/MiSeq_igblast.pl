#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;


open IN1, "gunzip -c $ARGV[0] |" or die "Cannot open $ARGV[0]\n";
open IN2, "gunzip -c $ARGV[1] |" or die "Cannot open $ARGV[1]\n";

my $cnt = 0;
while (1) {
  my $l1_1 = <IN1>;
  my $l2_1 = <IN2>;
  my $l1_2 = <IN1>;
  my $l2_2 = <IN2>;
  my $l1_3 = <IN1>;
  my $l2_3 = <IN2>;
  my $l1_4 = <IN1>;
  my $l2_4 = <IN2>;

  chomp $l1_2;
  chomp $l2_2;
  
  $l2_2 = Sets::getComplement($l2_2);
  print ">$l1_1$l1_2" . "NNNNNNNNNNNNNNNNNNNNNNNNNNNNN$l2_2\n";

  #my @a1 = split /\t/, $l1, -1;
  #my @a2 = split /\t/, $l1, -1;

  $cnt ++;

  if ($cnt == 500) {
    last;
  }

}
close IN;

