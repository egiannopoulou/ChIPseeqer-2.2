#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;


open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
my $l = <IN>;
print $l;

my @sum = ();
my @MAT = ();
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  for (my $i=1; $i<@a; $i++) {
    $sum[$i] += $a[$i];
  }
  
  push @MAT, \@a;
}
close IN;

foreach my $r (@MAT) {
  
  for (my $i=1; $i<@$r; $i++) {
    $r->[$i] = sprintf("%3.2f", 1000000 * $r->[$i] / $sum[$i]);
  }
  print join("\t", @$r) . "\n";
}
