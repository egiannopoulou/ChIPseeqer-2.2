#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use Fasta;
use bl2seq;

use strict;

use Getopt::Long;

if (@ARGV == 0) {
  die "Args --vdjfile=FILE --f2=FILE\n";
}
my $vdjfile = undef;
my $vrefseq   = "IGHV-clean.fa";
my $drefseq   = "IGHD-clean.fa";
my $jrefseq   = "IGHJ-clean.fa";
my $vdj       = undef;
my $verbose   = 0;
my $showjunctions = 0;
my $outfile   = undef;
my $showname  = 0;
my $stopafter = undef;
my $singleseq = undef;

GetOptions("vdjfile=s"       => \$vdjfile,
	   "verbose=s"       => \$verbose,
	   "showname=s"      => \$showname,
	   "singleseq=s"     => \$singleseq,
	   "outfile=s"       => \$outfile,
	   "stopafter=s"     => \$stopafter,
	   "showjunctions=s" => \$showjunctions,
	   "vdj=s"           => \$vdj);



# load ref sequences
my %V = ();
my %D = ();
my %J = ();
# load V, D, J ref seq
my $fa = Fasta->new;
$fa->setFile($vrefseq);
while (my $a_ref = $fa->nextSeq()) {
    my ($n, $s) = @$a_ref;
    $V{$n} = $s;
}
$fa->dispose;

$fa = Fasta->new;
$fa->setFile($drefseq);
while (my $a_ref = $fa->nextSeq()) {
    my ($n, $s) = @$a_ref;
    $D{$n} = $s;
}
$fa->dispose;

$fa = Fasta->new;
$fa->setFile($jrefseq);
while (my $a_ref = $fa->nextSeq()) {
    my ($n, $s) = @$a_ref;
    $J{$n} = $s;
}


my @aVDJ = ();

# count
open IN, $vdjfile or die "Cannot open $vdjfile\n";
my %CNT = ();
while (1) {
  my @a_l = ();
  my $l1 = <IN>; if (!$l1) { last; }; 
  my $l2 = <IN>;
  my $l3 = <IN>;
  my $l4 = <IN>;
  my $l5 = <IN>;
  
  chomp $l1;
  my @a_l1 = split /\t/, $l1, -1;
  
  chomp $l2;
  my @a_l2 = split /\t/, $l2, -1;
  
  chomp $l3;
  my @a_l3 = split /\t/, $l3, -1;
    
  chomp $l4;
  my @a_l4 = split /\t/, $l4, -1;
  
  $CNT{"$a_l2[2] $a_l3[2] $a_l4[2]"} ++;
  
}
close IN;

my $a_ref_CNT = Sets::hash_order(\%CNT);
foreach my $r (@$a_ref_CNT) {
  my @a = split /\ /, $r;
  
  print "$r\t$CNT{$r}\n";
  #print "$V{$a[0]}\n";
}

print STDERR "# " . $a_ref_CNT->[ scalar(@$a_ref_CNT) - 1] . "\n";

