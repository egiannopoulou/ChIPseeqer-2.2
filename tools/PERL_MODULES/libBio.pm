package      libBio;
require      Exporter;
@ISA       = qw(Exporter);
@EXPORT    = qw(load_fasta getMajorNt);


# get the most frequent nucleotide 
# input is in the order A/C/G/T
sub getMajorNt {
	my $ans= '-';
	my $tmp= shift;
	my @nc= @$tmp;
	# print "major: ". join(",", @nc). "\n";
	my $maxpct= 0;
	my @nt= qw (A C G T);
	for (my $i=0; $i < 4; $i++){
		if ($nc[$i] =~ /-/){next;}      # next if SNV does not exist
		if ($nc[$i] > $maxpct){
			$ans= $nt[$i];          # base
			$maxpct= $nc[$i];       # pct
		}
	}
	return $ans;
}

# make this a module
#	load_fasta(fname, column, nosuffix)
sub load_fasta {
	my $fname= shift;
	my $col= shift;
	my $nosuffix= shift;

	if (defined $nosuffix && $nosuffix =~ /^nos/){
		$nosuffix= 1;
	}else{
		$nosuffix= 0;
	}

	if (! defined $col){
		$col= 1;	# second column is id by default
	}

	my %seq= ();
	my $id;
	if (! -e $fname ){
		print STDERR "$fname does not exist!\n";
		return undef;
	}	
	open my $fh, $fname or die $!;
	while (my $line= <$fh>){
		chomp $line;
		if ($line =~ /^>/){
			$line= substr $line, 1;
			$id= (split(/\|| /, $line))[$col];
			if ($nosuffix){
				$id=~ s/\..+$//;
				# print "loading $id\n";
			}
		}else {	
			$line=~ s/ +//;
			$seq{$id}.= $line if ($id);
		}
	}
	close $fh;
	return \%seq;	
}
