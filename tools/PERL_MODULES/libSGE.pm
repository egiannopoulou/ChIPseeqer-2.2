package      libSGE;
require      Exporter;
@ISA       = qw(Exporter);
@EXPORT    = qw(makeSGE);

use strict;
use libFile;
use Cwd;

sub makeSGE{
	my $name_run= shift;
	my $name_log= shift;
	my $target_dir= shift;
	my $hrs= shift;
	my $mem= shift;
	my $inst= shift;	# other instructions, e.g. -t 1-10
	my $cmd= shift;

# print SGE script
print "generating SGE script $name_run\n";
my $fout;
	open $fout, ">$name_run" or die $!;
print $fout 
"#!/bin/sh -l
#\$ -S /bin/sh 
# #\$ -e /dev/null 
#\$ -e $target_dir/$name_log
#\$ -o /dev/null 
#\$ -l h_rt=$hrs
#\$ -l h_vmem=$mem
";

print $fout "#\$ $inst\n";

print $fout "
. /etc/profile.d/sge.sh
. ~/.bashrc

cd $target_dir

";


print $fout "
logfile=$name_log
echo `hostname` > \$logfile
echo `date` >> \$logfile

time1=`date +%s`

#####

$cmd  

#####

time2=`date +%s`

let runtime=(\$time2-\$time1)/60

echo job finished >> \$logfile
echo `date` >> \$logfile

echo runtime=\$runtime min >> \$logfile
echo End of job \$fname >> \$logfile

";

close $fout;

}
