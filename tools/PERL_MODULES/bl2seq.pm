package bl2seq;

use Sequence;
use strict;
#use DataFiles;

#my $df = DataFiles->new;

sub new {
    my ($self) = {};
    
    $self->{BL2SEQ_BIN}        = "bl2seq"; #"$ENV{HOME}/PERL_MODULES/PROGRAMS/BLAST/bin/bl2seq"; #$df->get("BLASTDIR");
    #if (! -e $self->{BL2SEQ_BIN}) { 
    #  die "Cannot find bl2seq binary\n"; 
    #:}
    $self->{DATABASE}         = undef;
    $self->{QUERY}            = undef;
    $self->{VERBOSE}          = 0;
    $self->{EVALUE_THRESHOLD} = undef;
    $self->{STORE}            = 1;
    $self->{QUERY_LENGTH}     = undef;
    $self->{HIT_LENGTH}       = undef;
    $self->{HIT_NAME}         = undef;
    $self->{BLAST_PROGRAM}    = "blastn";
    $self->{NB_PROCESSORS}    = undef;
    $self->{CLINE}            = undef;
    $self->{EXITONCRASH}      = 0;
    $self->{CRASHED}          = 0;
    $self->{LOG}              = 0;
    $self->{D}                = 1; # 1 = table, 0 = normal to parse
    $self->{RAWOUTPUT}        = undef;
    $self->{UNAME}            = `uname`; $self->{UNAME} =~ s/\n//g;
    bless($self);
    return $self;
}

sub DESTROY {
    my ($self) = @_;

    close LOG;
}


sub setExitOnCrash {
  my ($self, $e) = @_;
  $self->{EXITONCRASH} = $e;
}


sub crashed {
  my ($self) = @_;
  return $self->{CRASHED};
}


sub getRawOutput {
  my ($self) = @_;
  return $self->{RAWOUTPUT};
}

sub setD {
  my ($self, $d) = @_;
  $self->{D} = $d;
}

sub log {

    my ($self, $f) = @_;
    $self->{LOG} = $f;

    open LOG, ">log.txt" if ($f == 1);
}


sub setNbProcessors {
    my ($self, $f) = @_;
     $self->{NB_PROCESSORS} = $f;
}


sub setBlastProgram {
    
    my ($self, $f) = @_;
    $self->{BLAST_PROGRAM} = $f;
    
}

sub setBlastDir {
    my ($self, $f) = @_;
    $self->{BLAST_DIR} = $f;
}

sub setVerbose {
    my ($self, $f) = @_;
    $self->{VERBOSE} = $f;
}

sub setDatabase {
    my ($self, $f) = @_;
    $self->{DATABASE} = $f;
}


sub setQuery {
    my ($self, $f) = @_;
    $self->{QUERY} = $f;
}

#
# run bl2seq
#
sub bl2seq {
  my ($self, $s_file1, $s_file2) = @_;

  my $s = "$self->{BL2SEQ_BIN} -i $s_file1 -j $s_file2 -p blastn -W 5 -S 1 -e 1 -q -1 -F F -G 2 -E 1 -D $self->{D} ";
  
  if ($self->{VERBOSE} == 1) { print STDERR "$s\n"; }
  
  my $e = `$s`;
  my @lines = split /\n/, $e; chomp @lines;
  if ($self->{VERBOSE} == 1) { print STDERR "$e\n"; }
  if ($self->{D} == 1) {
    my @a_mat = ();    
    foreach my $l (@lines) {      
      next if ($l =~ /^\#/);
      #print "$l\n";
      my @a = split /\t/, $l;      
      push @a_mat, \@a;
    }    
    my @a_new = sort { $a->[10] <=> $b->[10] } @a_mat;
    return \@a_new;
  } else {
    
    #print $e;
    my $i = 0;
    while (($i < @lines) && ($lines[$i] !~ /Score/)) { 
      $i++ 
    }; # advance to score line
    $i+= 5;

    my $qseq = "";
    my $dseq = "";

    # where do we start
    my ($qst) = $lines[$i  ] =~ /Query: (\d+)/;
    my ($dst) = $lines[$i+2] =~ /Sbjct: (\d+)/;
    my $qen   = undef;
    #print "# START $st\n";
    while (($i <@lines) && ($lines[$i] !~ /Score/) && ($lines[$i] !~ /Lambda/)) {    
      #print "$lines[$i]\n";
      my @a = split /\ +/, $lines[$i];
     
      if ($lines[$i] =~ /Query/) {
	$qseq .= $a[2];
	$qen   = $a[3];  # will be replaced
	
      }
      if ($lines[$i] =~ /Sbjct/) {
	$dseq .= $a[2];
      }

      
      $i++;
    }
    
    return [ $qseq, $dseq, $qst,  $qen, $dst  ];
    #print "$qseq\n$dseq\n";

  }

}

sub cleanup {
  my ($self) = @_;

  unlink $self->{RESFILE};
}

sub getExactMatches {

  my ($self, $myoutfile) = @_;

  my $outfile = undef;

  if (defined($myoutfile)) {
    $outfile = $myoutfile;
  } elsif (defined($self->{RESFILE})) {
    $outfile = $self->{RESFILE};
  } else {
    die "Cannot find any output file for Blat\n";
  }

  open IN, $outfile or die "Cannot open $outfile.\n";

  my $l = <IN>;
  my $l = <IN>;
  my $l = <IN>;
  my $l = <IN>;
  my $l = <IN>;

  my %matches = ();

  while (my $l = <IN>) {
    chomp $l;
    my @a = split /\t/, $l;

    # enforce exact matches
    if (($a[0] == $a[10]) && ($a[1] == 0) && ($a[4] == 0) && ($a[6] == 0)) { 
      
      my $id = $a[9];
      my %M = ( HIT_NAME => $a[13],
		DFROM    => $a[15],
		DTO      => $a[16],
		DSTRAND  => $a[8]  );
      push @{ $matches{$id} }, \%M;
    }
    
  }
  close IN;

  
  return \%matches;
}



1;
