// #include "hashtable.h"

typedef struct _GenInt {
	int   i;
	int   j;
	int   ci;
	int   cj;
	char* c;
	char* chr;

	char* id;
	char* dbsnp;
	int   numexons;
	int*  exon_starts;
	int*  exon_ends;
	int   strand;
	char* tsname;

	char* genename;
	int   fr;
	int   len;
	int   cdsst;
	int   cdsen;
	int   ef; // exon frame
	int   num; // exon number
	char  utrtype;
	int   mut[4];
	int   N;
	int   k;
	float score;
	char  nc;
	float entropy;
} GenInt;


void readRefGenePromoters(char* file, GenInt** a_proms, int* numproms, int up, int dn);

void readExonsFromRefGene(char* file, GenInt** a_int, int* numexons);
//void readPromotersAndExonsFromRefGene(char* file, GenInt*** a_exons, int** numexons, GenInt*** a_proms, int** numproms, int up, int dn, int numchroms, HASH* hc);
void readPromotersAndExonsFromRefGene(char* file, GenInt*** a_exons, int** numexons, GenInt*** a_introns, int** numintrons, GenInt*** a_proms, int** numproms, int up, int dn, int numchroms, HASH* hc);

void readPromotersAndExonsFromKnownGene(char* file, GenInt*** a_exons, int** numexons, GenInt*** a_proms, int** numproms, int up, int dn, int numchroms, HASH* hc);
void readPromotersAndExonsFromAceView(char* file, GenInt*** a_exons, int** numexons, GenInt*** a_proms, int** numproms, int up, int dn, int numchroms, HASH* hc);
void readPeaks(char* file, GenInt*** a_peaks, int** numpeaks, int numchroms, HASH* hc);

//void readRnas(char* file, GenInt** a_int, int* numrnas, int ext);
void readRnas(char* file, GenInt*** a_rnas, int** numrnas, int up, int dn, int numchroms, HASH* hc);

//void readPromotersFromRefGene(char* file, GenInt** a_int, int* numproms, int up, int dn);
//void readGenesFromRefGene(char* file, GenInt** a_int, int* numgenes, int extend);

//void readPromotersFromKnownGene(char* file, GenInt** a_int, int* numproms, int up, int dn);
int readFileSumUpColumn(char* file, int col, int numchroms, HASH* hc);

void readFileSumUpColumnPerChrom(char* file, int col, int colchr, int** counts, int** countgenes, int numchroms, HASH* hc);

void nbLinesPerChromInFile(char* file, int colchr, int** counts, int numchroms, HASH* hc);


void readGenesFromRefGene(char* file, GenInt** a_genes, int* numgenes, struct my_hsearch_data* hashgenes);

