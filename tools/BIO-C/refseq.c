#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <search.h>

#include "dataio.h"
#include "prefix.h"
#include "sequences.h"
#include "statistics.h"
#include "hashtable.h"

#include "refseq.h"




void readRefGenePromoters(char* file, GenInt** a_proms, int* numproms, int up, int dn)
{

  char*  buff;
  int    mynmax = 100000;

  char** a;
  int    m;
  FILE*  f;
  //int    cidx = -1;
  //int    i;
  char*  line = 0;
  int    hashret;
  ENTRY  e;
  ENTRY* ep;
  struct my_hsearch_data* hash_genes;

  // initialize num rnas per chr
  *numproms = nbLinesInFile(file);

  /* build a hash table */
  hash_genes = (struct my_hsearch_data*)calloc(1,  sizeof(struct my_hsearch_data));
  hashret = my_hcreate_r((*numproms)*2, hash_genes);
  if (hashret == 0) {
    printf("Could not create hash table ...\n");
    exit(0);
  }



  // alloc intervals
  *a_proms = (GenInt*)malloc( (*numproms) * sizeof(GenInt));
  if (*a_proms == 0) {
    die("Problem allocating a_rnas.\n");
  }

  //
  // read set of intervals
  //
  buff  = (char*)malloc(mynmax * sizeof(char));
  f = fopen(file, "r");
  if (f == 0)
    die("Cannot open refGene file.\n");
  *numproms = 0;
  while (fgets(buff, mynmax, f) != 0) {
    chomp(buff);

    line = strdup(buff);

    split_line_delim(buff, "\t", &a, &m);

    char* n      = a[1];

    /* query */
    e.key = n;
    my_hsearch_r(e, FIND, &ep, hash_genes) ;
    if (ep) {
      /* success, already rgere */
      free(a);
      continue;
    } else {

      // add
      /* enter key/value pair into hash */
      e.key   = strdup( n );
      e.data  = (char*)(*numproms);
      hashret = my_hsearch_r(e, ENTER, &ep, hash_genes);
      if (hashret == 0) {
	printf("Could not enter entry into hash table ...\n");
	exit(0);
      }

    }

    char* c      = a[2];     //chr
    char  fr     = a[3][0];
    int   tss    = (fr=='+'?atoi(a[4]):atoi(a[5]));
    char* g      = a[12];

    //(*a_proms)[ (*numproms) ].str      = line;
    (*a_proms)[ (*numproms) ].tsname    = strdup(n);
    (*a_proms)[ (*numproms) ].chr       = strdup(c);
    (*a_proms)[ (*numproms) ].genename  = strdup(g);

    // prom
    if (fr == '+') {
      (*a_proms)[ (*numproms) ].i        = tss-up;
      (*a_proms)[ (*numproms) ].j        = tss+dn;
    } else {
      (*a_proms)[ (*numproms) ].i        = tss-dn;
      (*a_proms)[ (*numproms) ].j        = tss+up;
    }

    // frame
    (*a_proms)[ (*numproms)].fr        = (fr=='+'?1:-1);


    (*numproms) ++;

    free(a);

  }


  free(buff);
  fclose(f);

}



void readGenesFromRefGene(char* file, GenInt** a_genes, int* numgenes, struct my_hsearch_data* hash_genes)
{
  char*  buff;
  int    mynmax = 100000;
  char** a;
  int    m;
  FILE*  f;

  char** a_e_st;
  char** a_e_en;
  char** a_e_fr;
  int    m_e_st;
  int    m_e_en;
  int    m_e_fr;
  int    i, j, l;
  int*   a_exon_starts;
  int*   a_exon_ends;
  ENTRY* ep;
  ENTRY  e;
  int    hashret;

  int    relposinexon;
  int    numexons       = 0;
  int    poscdsstingene = 0;
  int    poscdseningene = 0;
  int    posingene = 0;
  // estimate line numbers
  *numgenes = nbLinesInFile(file);

  *a_genes = (GenInt*)calloc(*numgenes, sizeof(GenInt));
  if (*a_genes == 0) {
    die("Cannot alloc data for a_genes\n");
  }

  hashret = my_hcreate_r( (*numgenes) + 10000, hash_genes);
  if (hashret == 0) {
    printf("Could not create hash table with %d genes ...\n", *numgenes);
    exit(0);
  }

  // read genes
  *numgenes = 0;
  f = fopen(file, "r");
  if (f == 0)
    die("Cannot open refGene file.\n");
  buff = (char*)calloc(mynmax, sizeof(char));
  int numlines = 0;
  while (!feof(f)) {
    fgets(buff, mynmax, f);
    if (feof(f))
      break;
    chomp(buff);

    split_line_delim(buff, "\t", &a, &m);

    char* n      = a[1];  // refseq id
    char* c      = a[2];
    char  fr     = a[3][0];
    char* e_st   = a[9];
    char* e_en   = a[10];
    char* e_fr   = a[15];
    char* g      = a[12];
    int   cds_st = atoi(a[6]);
    char* unk    = a[13];  // unk for ncRNA

    // int   cds_en = atoi(a[7]);
    int   cds_en = atoi(a[7])- 1;		// debug: fix 1-based coordinate

    // lookup
    e.key = n;
    my_hsearch_r(e, FIND, &ep, hash_genes) ;
    if (ep) {
      free(a);   // gene already found, skip
      continue;
    }

    // add
    e.key   = strdup( n );
    e.data  = (char*)(*numgenes);
    hashret = my_hsearch_r(e, ENTER, &ep, hash_genes);
    if (hashret == 0) {
      printf("Could not enter entry into hash table ...\n");
      exit(0);
    }

    //
    // EXONS
    //

    // chomp
    int  l_e_st = strlen(e_st);
    if (e_st[l_e_st-1] == ',')
      e_st[l_e_st-1] = '\0';
    int  l_e_en = strlen(e_en);
    if (e_en[l_e_en-1] == ',')
      e_en[l_e_en-1] = '\0';
    int  l_e_fr = strlen(e_fr);
    if (e_fr[l_e_fr-1] == ',')
      e_fr[l_e_fr-1] = '\0';

    // split
    split_line_delim(e_st, ",", &a_e_st, &m_e_st);
    split_line_delim(e_en, ",", &a_e_en, &m_e_en);
    split_line_delim(e_fr, ",", &a_e_fr, &m_e_fr);

    a_exon_starts   = (int*)malloc(m_e_st * sizeof(int));
    a_exon_ends     = (int*)malloc(m_e_st * sizeof(int));

    (*a_genes)[ *numgenes ].len = 0;
    (*a_genes)[ *numgenes ].i     = atoi(a[4]);    // TS
    (*a_genes)[ *numgenes ].j     = atoi(a[5]) - 1;
    (*a_genes)[ *numgenes ].ci     = atoi(a[6]);    // TS
    (*a_genes)[ *numgenes ].cj     = atoi(a[7]) - 1;

    // calculate ts length
    for (i=0; i<m_e_st; i++) {
      a_exon_starts[i] = atoi(a_e_st[i]);
      a_exon_ends[i]   = atoi(a_e_en[i])-1;		// fix 1-based coordinate
      int l = a_exon_ends[i] - a_exon_starts[i] + 1;
      (*a_genes)[ *numgenes ].len += l;
    }

    (*a_genes)[ *numgenes ].tsname      = strdup(n);
    (*a_genes)[ *numgenes ].chr         = strdup(c);
    (*a_genes)[ *numgenes ].genename    = strdup(g);
    (*a_genes)[ *numgenes ].fr          = (fr=='+'?1:-1);
    (*a_genes)[ *numgenes ].exon_starts = a_exon_starts;
    (*a_genes)[ *numgenes ].exon_ends   = a_exon_ends;
    (*a_genes)[ *numgenes ].numexons    = m_e_st;

    if (strcmp(unk, "unk") == 0) { // ncRNA
      (*a_genes)[ *numgenes ].nc = 1;
    } else {
      (*a_genes)[ *numgenes ].nc = 0;
    }

    // calculate pos cds in
    numexons       = m_e_st;
    poscdsstingene = 0;
    poscdseningene = 0;

    if (fr == '+') {

      posingene = 0;
      // traverse exons
      for (j=0; j<numexons; j++) {

	// if cds_st in exon
	if ((a_exon_starts[j] <= cds_st) && (cds_st <= a_exon_ends[j])) {

	  // relative positon wrt begining of exon
	  // relposinexon = cds_st - a_exon_starts[j];
	  relposinexon = cds_st - a_exon_starts[j] + 1;		// length (

	  // pos cds wrt begining of gene
	  poscdsstingene = posingene + relposinexon;

	}

	// if cds_en in exon
	if ((a_exon_starts[j] <= cds_en) && (cds_en <= a_exon_ends[j])) {

	  // relative positon wrt begining of exon
	  // relposinexon = cds_en - a_exon_starts[j];
	  relposinexon = cds_en - a_exon_starts[j] + 1;  	// length (

	  // pos cds wrt begining of gene
	  poscdseningene = posingene + relposinexon;

	}

	l = a_exon_ends[j] - a_exon_starts[j] + 1;
	posingene += l;

      }
      poscdsstingene--;	// debug
      poscdseningene--;	// debug
    } // if (fr == +
    else {
      // fr == -

      posingene = 0;
      // traverse exons
      for (j=numexons-1; j>=0; j--) {
	//printf("exon %d\n", j);
	// if cds_st in exon
	if ((a_exon_starts[j] <= cds_st) && (cds_st <= a_exon_ends[j])) {

	  // relative positon wrt begining of exon
	  relposinexon   = a_exon_ends[j] - cds_st +1 ;  //leading length (

	  // pos cds en wrt begining of gene (cds_st is actually cds_en of course)
	  poscdseningene = posingene + relposinexon;

	}

	// if cds_en in exon
	if ((a_exon_starts[j] <= cds_en) && (cds_en <= a_exon_ends[j])) {
	  //printf("found cds_en\n");
	  // relative positon wrt begining of exon
	  relposinexon = a_exon_ends[j] - cds_en + 1;  // trailing length

	  // pos cds st wrt begining of gene
	  poscdsstingene = posingene + relposinexon;

	}

	l = a_exon_ends[j] - a_exon_starts[j] + 1;
	posingene += l;
	//printf("posingene becomes %d\n", posingene);
      }

      //poscdsstingene++;
      poscdsstingene--;	//debug: length -> position
      poscdseningene--;
    } // else fr == -

    (*a_genes)[ *numgenes ].cdsst = poscdsstingene;
    (*a_genes)[ *numgenes ].cdsen = poscdseningene;


    (*numgenes) ++;

    free(a_e_st);
    free(a_e_en);
    free(a_e_fr);
    free(a);
    numlines++;
    fprintf(stderr, "# %d genes read          \r", numlines);
  }


  free(buff);
  fclose(f);
  //printf("Exit\n");
}







void readPromotersAndExonsFromRefGene(char* file, GenInt*** a_exons, int** numexons, GenInt*** a_introns, int** numintrons, GenInt*** a_proms, int** numproms, int up, int dn, int numchroms, HASH* hc)
{
  char*  buff;
  int    mynmax = 100000;

  char** a;
  int    m;
  FILE*  f;
  int    numgenes = 0;
  char** a_e_st;
  char** a_e_en;
  char** a_e_fr;
  int    m_e_st;
  int    m_e_en;
  int    m_e_fr;
  int**  a_myexons;
  int**  a_myintrons;

  int*   a_utrtype;
  int    i;
  int    ii = 0;
  int*   maxexons = 0;
  int*   maxgenes = 0;
  int    cidx;


  // estimate exon and gene numbers per chr
  readFileSumUpColumnPerChrom(file, 8, 2, &maxexons, &maxgenes, numchroms, hc);

  // initialize actual exon numbers
  *numexons = (int*)calloc(numchroms, sizeof(int));
  // alloc memory for exons
  *a_exons = (GenInt**)malloc(numchroms * sizeof(GenInt*));
  if (*a_exons == 0) {
    die("Problem allocating a_exons.\n");
  }
  for (i=0; i<numchroms; i++) {
    (*a_exons)[i] = (GenInt*)calloc(maxexons[i], sizeof(GenInt));
  }

  // initialize actual intron numbers
  *numintrons = (int*)calloc(numchroms, sizeof(int));
  // alloc memory for exons
  *a_introns = (GenInt**)malloc(numchroms * sizeof(GenInt*));
  if (*a_introns == 0) {
    die("Problem allocating a_introns.\n");
  }
  for (i=0; i<numchroms; i++) {
    (*a_introns)[i] = (GenInt*)calloc(maxexons[i], sizeof(GenInt)); //use maxexons (upper bound to num intons)
    if ((*a_introns)[i] == 0) {
      die("Problem allocating a_introns[i].\n");
    }
  }


  // alloc memory for promoters
  *numproms = (int*)calloc(numchroms, sizeof(int));
  *a_proms = (GenInt**)malloc(numchroms * sizeof(GenInt*));
  if (*a_proms == 0) {
    die("Problem allocating a_proms.\n");
  }
  for (i=0; i<numchroms; i++) {
    (*a_proms)[i] = (GenInt*)calloc(maxgenes[i], sizeof(GenInt));
    if ((*a_proms)[i] == 0) {
      die("Problem allocating a_proms[i].\n");
    }
  }


  buff  = (char*)malloc(mynmax * sizeof(char));

  // read second set of intervals
  numgenes = 0;
  f = fopen(file, "r");
  if (f == 0)
    die("Cannot open refGene file.\n");

  while (fgets(buff, mynmax, f) != 0) {

    chomp(buff);
    split_line_delim(buff, "\t", &a, &m);

    char* n      = a[1];
    char* c      = a[2];

    if (!HASH_find(hc, c, &cidx)) {
      //printf("Warning: %s not recognized\n", c);
      free(a);
      continue;
    }

    //cidx = chr2index(c);
    //if (cidx == -1) {
    //  free(a);
    //  continue;
    //}

    char  fr     = a[3][0];
    char* e_st   = a[9];
    char* e_en   = a[10];
    char* e_fr   = a[15];
    char* g      = a[12];
    int   cds_st = atoi(a[6]);
    int   cds_en = atoi(a[7]);
    int   tss    = (fr=='+'?atoi(a[4]):atoi(a[5]));

    //
    // EXONS
    //

    // chomp
    int  l_e_st = strlen(e_st);
    if (e_st[l_e_st-1] == ',')
      e_st[l_e_st-1] = '\0';
    int  l_e_en = strlen(e_en);
    if (e_en[l_e_en-1] == ',')
      e_en[l_e_en-1] = '\0';
    int  l_e_fr = strlen(e_fr);
    if (e_fr[l_e_fr-1] == ',')
      e_fr[l_e_fr-1] = '\0';

    // split
    split_line_delim(e_st, ",", &a_e_st, &m_e_st);
    split_line_delim(e_en, ",", &a_e_en, &m_e_en);
    split_line_delim(e_fr, ",", &a_e_fr, &m_e_fr);

    //printf("m_e_st = %d\n", m_e_st);

    // store whether exon is clear 3'UTR or 5'UTR
    a_utrtype = (int*) calloc(m_e_st,  sizeof(int));

    a_myexons   = (int**)malloc(m_e_st * sizeof(int*));
    for (i=0; i<m_e_st; i++) {
      a_myexons[i]    = (int*)calloc(4, sizeof(int));
      a_myexons[i][0] = atoi(a_e_st[i]);
      a_myexons[i][1] = atoi(a_e_en[i]);
      a_myexons[i][2] = a_myexons[i][0];
      a_myexons[i][3] = a_myexons[i][1];
    }

    // SAME FOR INTRONS
    a_myintrons   = (int**)malloc(m_e_st * sizeof(int*));
    if (a_myintrons == 0)
      die("Cannot allocate a_myintrons\n");

    for (i=0; i<m_e_st-1; i++) {
      a_myintrons[i]    = (int*)calloc(2, sizeof(int));
      a_myintrons[i][0] = atoi(a_e_en[i]);    // end of exon
      a_myintrons[i][1] = atoi(a_e_st[i+1]); // start of next exon
    }


    for (i=0; i<m_e_st; i++) {
      if ((cds_st >= a_myexons[i][0]) && (cds_st <= a_myexons[i][1])) {
	a_myexons[i][2] = cds_st;
	a_myexons[i][3] = a_myexons[i][1];
	break;
      } else {
	a_myexons[i][2] = -1;
	a_myexons[i][3] = -1;
	if (fr == '+')
	  a_utrtype[i] = 1;
	else
	  a_utrtype[i] = 2;
      }
    }

    for (i=m_e_st-1; i>=0; i--) {
      if ((cds_en >= a_myexons[i][0]) && (cds_en <= a_myexons[i][1])) {
	a_myexons[i][2] = a_myexons[i][0];
	a_myexons[i][3] = cds_en;
	break;
      } else {
	a_myexons[i][2] = -1;
	a_myexons[i][3] = -1;
	if (fr == '+')
	  a_utrtype[i] = 2;
	else
	  a_utrtype[i] = 1;
      }
    }

    for (i=0; i<m_e_st; i++) {

      if (fr == '+')
	ii = i;
      else
	ii = m_e_st - i - 1;

      // names
      (*a_exons)[cidx][ (*numexons)[cidx] ].tsname    = strdup(n);
      (*a_exons)[cidx][ (*numexons)[cidx] ].chr       = strdup(c);
      (*a_exons)[cidx][ (*numexons)[cidx] ].genename  = strdup(g);

      // num
      (*a_exons)[cidx][ (*numexons)[cidx] ].num       = i+1;

      // whole exon
      (*a_exons)[cidx][ (*numexons)[cidx] ].i         = a_myexons[ii][0] + 1; // added due to refGene format
      (*a_exons)[cidx][ (*numexons)[cidx] ].j         = a_myexons[ii][1];

      // coding exon
      (*a_exons)[cidx][ (*numexons)[cidx] ].ci        = a_myexons[ii][2] + 1; // added due to refGene format
      (*a_exons)[cidx][ (*numexons)[cidx] ].cj        = a_myexons[ii][3];

      // frame
      (*a_exons)[cidx][ (*numexons)[cidx] ].fr        = (fr=='+'?1:-1);

      // exon frame
      (*a_exons)[cidx][ (*numexons)[cidx] ].ef        = atoi(a_e_fr[ii]);

      // utr type
      (*a_exons)[cidx][ (*numexons)[cidx] ].utrtype   = a_utrtype[ii];

      // num exons
      (*numexons)[cidx] ++;
    }
    //
    // END EXONS
    //

    //
    // ADD INTRONS
    //
    for (i=0; i<m_e_st-1; i++) {

      if (fr == '+')
	ii = i;
      else
	ii = (m_e_st - 1) - i - 1; // nothe the -1

      // names
      (*a_introns)[cidx][ (*numintrons)[cidx] ].tsname    = strdup(n);
      (*a_introns)[cidx][ (*numintrons)[cidx] ].chr       = strdup(c);
      (*a_introns)[cidx][ (*numintrons)[cidx] ].genename  = strdup(g);

      // num
      (*a_introns)[cidx][ (*numintrons)[cidx] ].num       = i+1;

      // whole intron
      (*a_introns)[cidx][ (*numintrons)[cidx] ].i         = a_myintrons[ii][0] + 1; // added due to refGene format
      (*a_introns)[cidx][ (*numintrons)[cidx] ].j         = a_myintrons[ii][1];

      // frame
      (*a_introns)[cidx][ (*numintrons)[cidx] ].fr        = (fr=='+'?1:-1);

      // num introns
      (*numintrons)[cidx] ++;
    }
    //
    // END INTRONS
    //




    //
    // START PROMOTERS
    //
    (*a_proms)[cidx][ (*numproms)[cidx] ].tsname    = strdup(n);
    (*a_proms)[cidx][ (*numproms)[cidx] ].chr       = strdup(c);
    (*a_proms)[cidx][ (*numproms)[cidx] ].genename  = strdup(g);

    // prom
    if (fr == '+') {
      (*a_proms)[cidx][ (*numproms)[cidx] ].i        = tss-up;
      (*a_proms)[cidx][ (*numproms)[cidx] ].j        = tss+dn;
    } else {
      (*a_proms)[cidx][ (*numproms)[cidx] ].i        = tss-dn;
      (*a_proms)[cidx][ (*numproms)[cidx] ].j        = tss+up;
    }

    // frame
    (*a_proms)[cidx][ (*numproms)[cidx] ].fr        = (fr=='+'?1:-1);

    (*numproms)[cidx] ++;

    //
    // END PROMOTERS
    //


    free(a_myexons);
    free(a_myintrons);
    free(a_utrtype);
    free(a_e_st);
    free(a_e_en);
    free(a_e_fr);
    free(a);

  }


  free(buff);
  fclose(f);

}

//
// KNOWNGENES
//
void readPromotersAndExonsFromKnownGene(char* file, GenInt*** a_exons, int** numexons, GenInt*** a_proms, int** numproms, int up, int dn, int numchroms, HASH* hc)
{
  char*  buff;
  int    mynmax = 100000;

  char** a;
  int    m;
  FILE*  f;
  int    numgenes = 0;
  char** a_e_st;
  char** a_e_en;
  //char** a_e_fr;
  int*   a_int_fr;
  int    m_e_st;
  int    m_e_en;
  //int    m_e_fr;
  int**  a_myexons;
  int*   a_utrtype;
  int    i;
  int    ii = 0;
  int*   maxexons = 0;
  int*   maxgenes = 0;
  int    cidx;


  // estimate exon and gene numbers per chr
  readFileSumUpColumnPerChrom(file, 7, 1, &maxexons, &maxgenes, numchroms, hc);

  // initialize actual exon numbers
  *numexons = (int*)calloc(numchroms, sizeof(int));

  // alloc memory for exons
  *a_exons = (GenInt**)malloc(numchroms * sizeof(GenInt*));
  if (*a_exons == 0) {
    die("Problem allocating a_exons.\n");
  }
  for (i=0; i<numchroms; i++) {
    (*a_exons)[i] = (GenInt*)calloc(maxexons[i], sizeof(GenInt));
  }

  // alloc memory for promoters
  *numproms = (int*)calloc(numchroms, sizeof(int));
  *a_proms = (GenInt**)malloc(numchroms * sizeof(GenInt*));
  if (*a_proms == 0) {
    die("Problem allocating a_proms.\n");
  }
  for (i=0; i<numchroms; i++) {
    //printf("Alloc %d genes\n", maxgenes[i]); fflush(stdout);
    (*a_proms)[i] = (GenInt*)calloc(maxgenes[i], sizeof(GenInt));
    if ((*a_proms)[i] == 0) {
      die("Problem allocating a_proms[i].\n");
    }
  }


  buff  = (char*)malloc(mynmax * sizeof(char));

  // read second set of intervals
  numgenes = 0;
  f = fopen(file, "r");
  if (f == 0)
    die("Cannot open refGene file.\n");

  while (!feof(f)) {
    fgets(buff, mynmax, f);
    if (feof(f))
      break;
    // printf("buff=%s\n", buff);
    chomp(buff);
    split_line_delim(buff, "\t", &a, &m);

    char* n      = a[0]; // mRNA name
    char* c      = a[1]; // chr

    if (!HASH_find(hc, c, &cidx)) {
      //printf("Warning: %s not recognized\n", c);
      free(a);
      continue;
    }

    char  fr     = a[2][0]; // frame
    char* e_st   = a[8];    // exon starts
    char* e_en   = a[9];   // exon ends
    //char* e_fr   = a[15];   // does not exist
    char* g      = a[12];   // alias
    int   cds_st = atoi(a[5]); // cds start
    int   cds_en = atoi(a[6]); // cds end
    int   tss    = (fr=='+'?atoi(a[3]):atoi(a[4]));  // tss

    //
    // EXONS
    //

    // chomp
    int  l_e_st = strlen(e_st);
    if (e_st[l_e_st-1] == ',')
      e_st[l_e_st-1] = '\0';
    int  l_e_en = strlen(e_en);
    if (e_en[l_e_en-1] == ',')
      e_en[l_e_en-1] = '\0';
    /* refseq
       int  l_e_fr = strlen(e_fr);
       if (e_fr[l_e_fr-1] == ',')
       e_fr[l_e_fr-1] = '\0';
    */

    // split
    split_line_delim(e_st, ",", &a_e_st, &m_e_st);
    split_line_delim(e_en, ",", &a_e_en, &m_e_en);
    //split_line_delim(e_fr, ",", &a_e_fr, &m_e_fr); // refseq

    a_int_fr = (int*)calloc(m_e_st, sizeof(int));

    // store whether exon is clear 3'UTR or 5'UTR
    a_utrtype = (int*) calloc(m_e_st,  sizeof(int));

    a_myexons   = (int**)malloc(m_e_st * sizeof(int*));
    for (i=0; i<m_e_st; i++) {
      a_myexons[i]    = (int*)calloc(4, sizeof(int));
      a_myexons[i][0] = atoi(a_e_st[i]);
      a_myexons[i][1] = atoi(a_e_en[i]);
      a_myexons[i][2] = a_myexons[i][0];
      a_myexons[i][3] = a_myexons[i][1];
    }

    if (cds_st != cds_en) {

      for (i=0; i<m_e_st; i++) {
	if ((cds_st >= a_myexons[i][0]) && (cds_st <= a_myexons[i][1])) {
	  a_myexons[i][2] = cds_st;
	  a_myexons[i][3] = a_myexons[i][1];
	  break;
	} else {
	  a_myexons[i][2] = -1;
	  a_myexons[i][3] = -1;
	  if (fr == '+')
	    a_utrtype[i] = 1;
	  else
	    a_utrtype[i] = 2;
	}
      }

      for (i=m_e_st-1; i>=0; i--) {
	if ((cds_en >= a_myexons[i][0]) && (cds_en <= a_myexons[i][1])) {
	  a_myexons[i][2] = a_myexons[i][0];
	  a_myexons[i][3] = cds_en;
	  break;
	} else {
	  a_myexons[i][2] = -1;
	  a_myexons[i][3] = -1;
	  if (fr == '+')
	    a_utrtype[i] = 2;
	  else
	    a_utrtype[i] = 1;

	}
      }

      //
      // CALC FRAME
      //
      if (fr == '+') {

	int codinglen = 0;
	for (i=0; i<m_e_st; i++) {
	  int ef = -1;
	  if (a_utrtype[i] == 0) {

	    // how much of the previous exon do I need ?
	    int mod = codinglen % 3;
	    if (mod > 0)
	      ef = mod;
	    else
	      ef = 0;

	    // calc coding exon length and add it to codinglen
	    int codexonlen = a_myexons[i][3] - a_myexons[i][2];
	    codinglen += codexonlen;
	  } else {
	    ef = -1;
	  }
	  //printf("ef=%d\n", ef);
	  a_int_fr[i] = ef;
	}
      } else {

	// fr == '-'
	int codinglen = 0;
	for (i=m_e_st-1; i>=0; i--) {
	  int ef = -1;
	  if (a_utrtype[i] == 0) {

	    // how much of the previous exon do I need ?
	    int mod = codinglen % 3;
	    if (mod > 0)
	      ef = mod;
	    else
	      ef = 0;

	    // calc coding exon length and add it to codinglen
	    int codexonlen = a_myexons[i][3] - a_myexons[i][2];
	    codinglen += codexonlen;
	  } else {
	    ef = -1;
	  }
	  a_int_fr[i] = ef;
	  //printf("ef=%d\n", ef);
	}
      }

    } // if (cds_st != cds_en)
    else {
      for (i=0; i<m_e_st; i++) {
	a_int_fr[i] = -1;
      }

    }

    /* debugging code
       int len = 0;
       for (i=0; i<m_e_st; i++) {
       len += (a_myexons[i][3] - a_myexons[i][2]);
       printf("%d <%d-%d> %d\tl=%d\t%d\t%d\n", a_myexons[i][0], a_myexons[i][2], a_myexons[i][3], a_myexons[i][1], len, a_int_fr[i], atoi(a_e_fr[i]));
       }
       printf("\n");

       for (i=0; i<m_e_st; i++) {
       if (a_int_fr[i] != atoi(a_e_fr[i])) {
       printf("DiSCP = %d and %d\n", a_int_fr[i], atoi(a_e_fr[i]));
       //exit(0);
       getchar();
       }
       }
       printf("\n");
    */

    for (i=0; i<m_e_st; i++) {

      if (fr == '+')
	ii = i;
      else
	ii = m_e_st - i - 1;

      // names
      (*a_exons)[cidx][ (*numexons)[cidx] ].tsname    = strdup(n);
      (*a_exons)[cidx][ (*numexons)[cidx] ].chr       = strdup(c);
      (*a_exons)[cidx][ (*numexons)[cidx] ].genename  = strdup(g);

      // num
      (*a_exons)[cidx][ (*numexons)[cidx] ].num       = i+1;

      // whole exon
      (*a_exons)[cidx][ (*numexons)[cidx] ].i         = a_myexons[ii][0] + 1; // added due to refGene format
      (*a_exons)[cidx][ (*numexons)[cidx] ].j         = a_myexons[ii][1];

      // coding exon
      (*a_exons)[cidx][ (*numexons)[cidx] ].ci        = a_myexons[ii][2] + 1; // added due to refGene format
      (*a_exons)[cidx][ (*numexons)[cidx] ].cj        = a_myexons[ii][3];

      // frame
      (*a_exons)[cidx][ (*numexons)[cidx] ].fr        = (fr=='+'?1:-1);

      // exon frame
      // (*a_exons)[cidx][ (*numexons)[cidx] ].ef        = atoi(a_e_fr[ii]);      // refseq
      (*a_exons)[cidx][ (*numexons)[cidx] ].ef        = a_int_fr[ii];      // refseq

      // utr type
      (*a_exons)[cidx][ (*numexons)[cidx] ].utrtype   = a_utrtype[ii];

      // num exons
      (*numexons)[cidx] ++;
    }

    //if (*numexons == MAXNUMINT) {
    //  die("Max number of intervals reached.\n");
    //}

    //
    // END EXONS
    //


    //
    // START PROMOTERS
    //
    (*a_proms)[cidx][ (*numproms)[cidx] ].tsname    = strdup(n);
    (*a_proms)[cidx][ (*numproms)[cidx] ].chr       = strdup(c);
    (*a_proms)[cidx][ (*numproms)[cidx] ].genename  = strdup(g);

    // prom
    if (fr == '+') {
      (*a_proms)[cidx][ (*numproms)[cidx] ].i        = tss-up;
      (*a_proms)[cidx][ (*numproms)[cidx] ].j        = tss+dn;
    } else {
      (*a_proms)[cidx][ (*numproms)[cidx] ].i        = tss-dn;
      (*a_proms)[cidx][ (*numproms)[cidx] ].j        = tss+up;
    }

    // frame
    (*a_proms)[cidx][ (*numproms)[cidx] ].fr        = (fr=='+'?1:-1);

    (*numproms)[cidx] ++;

    //
    // END PROMOTERS
    //


    free(a_myexons);
    free(a_utrtype);
    free(a_e_st);
    free(a_e_en);
    // free(a_e_fr); refseq
    free(a_int_fr);
    free(a);

  }


  free(buff);
  fclose(f);

}




//
// ACEVIEW
//
void readPromotersAndExonsFromAceView(char* file, GenInt*** a_exons, int** numexons, GenInt*** a_proms, int** numproms, int up, int dn, int numchroms, HASH* hc)
{
  char*  buff;
  int    mynmax = 100000;

  char** a;
  int    m;
  FILE*  f;
  int    numgenes = 0;
  char** a_e_st;
  char** a_e_en;
  //char** a_e_fr;
  int*   a_int_fr;
  int    m_e_st;
  int    m_e_en;
  //int    m_e_fr;
  int**  a_myexons;
  int*   a_utrtype;
  int    i;
  int    ii = 0;
  int*   maxexons = 0;
  int*   maxgenes = 0;
  int    cidx;


  // estimate exon and gene numbers per chr
  readFileSumUpColumnPerChrom(file, 8, 2, &maxexons, &maxgenes, numchroms, hc);

  // initialize actual exon numbers
  *numexons = (int*)calloc(numchroms, sizeof(int));

  // alloc memory for exons
  *a_exons = (GenInt**)malloc(numchroms * sizeof(GenInt*));
  if (*a_exons == 0) {
    die("Problem allocating a_exons.\n");
  }
  for (i=0; i<numchroms; i++) {
    (*a_exons)[i] = (GenInt*)calloc(maxexons[i], sizeof(GenInt));
  }

  // alloc memory for promoters
  *numproms = (int*)calloc(numchroms, sizeof(int));
  *a_proms = (GenInt**)malloc(numchroms * sizeof(GenInt*));
  if (*a_proms == 0) {
    die("Problem allocating a_proms.\n");
  }
  for (i=0; i<numchroms; i++) {
    //printf("Alloc %d genes\n", maxgenes[i]); fflush(stdout);
    (*a_proms)[i] = (GenInt*)calloc(maxgenes[i], sizeof(GenInt));
    if ((*a_proms)[i] == 0) {
      die("Problem allocating a_proms[i].\n");
    }
  }


  buff  = (char*)malloc(mynmax * sizeof(char));

  // read second set of intervals
  numgenes = 0;
  f = fopen(file, "r");
  if (f == 0)
    die("Cannot open refGene file.\n");

  while (!feof(f)) {
    fgets(buff, mynmax, f);
    if (feof(f))
      break;
    // printf("buff=%s\n", buff);
    chomp(buff);
    split_line_delim(buff, "\t", &a, &m);

    char* n      = a[1]; // mRNA name
    char* c      = a[2]; // chr
    if (!HASH_find(hc, c, &cidx)) {
      //printf("Warning: %s not recognized\n", c);
      free(a);
      continue;
    }

    //cidx = chr2index(c);
    //if (cidx == -1) {
    //  free(a);
    //  continue;
    //}

    char  fr     = a[3][0]; // frame
    char* e_st   = a[9];    // exon starts
    char* e_en   = a[10];   // exon ends
    //char* e_fr   = a[15];   // does not exist
    char* g      = a[1];   // alias
    int   cds_st = atoi(a[6]); // cds start
    int   cds_en = atoi(a[7]); // cds end
    int   tss    = (fr=='+'?atoi(a[4]):atoi(a[5]));  // tss

    //
    // EXONS
    //

    // chomp
    int  l_e_st = strlen(e_st);
    if (e_st[l_e_st-1] == ',')
      e_st[l_e_st-1] = '\0';
    int  l_e_en = strlen(e_en);
    if (e_en[l_e_en-1] == ',')
      e_en[l_e_en-1] = '\0';
    /* refseq
       int  l_e_fr = strlen(e_fr);
       if (e_fr[l_e_fr-1] == ',')
       e_fr[l_e_fr-1] = '\0';
    */

    // split
    split_line_delim(e_st, ",", &a_e_st, &m_e_st);
    split_line_delim(e_en, ",", &a_e_en, &m_e_en);
    //split_line_delim(e_fr, ",", &a_e_fr, &m_e_fr); // refseq

    a_int_fr = (int*)calloc(m_e_st, sizeof(int));

    // store whether exon is clear 3'UTR or 5'UTR
    a_utrtype = (int*) calloc(m_e_st,  sizeof(int));

    a_myexons   = (int**)malloc(m_e_st * sizeof(int*));
    for (i=0; i<m_e_st; i++) {
      a_myexons[i]    = (int*)calloc(4, sizeof(int));
      a_myexons[i][0] = atoi(a_e_st[i]);
      a_myexons[i][1] = atoi(a_e_en[i]);
      a_myexons[i][2] = a_myexons[i][0];
      a_myexons[i][3] = a_myexons[i][1];
    }

    if ((cds_st == 0) && (cds_en == 0)) {

      for (i=0; i<m_e_st; i++) {
	if ((cds_st >= a_myexons[i][0]) && (cds_st <= a_myexons[i][1])) {
	  a_myexons[i][2] = cds_st;
	  a_myexons[i][3] = a_myexons[i][1];
	  break;
	} else {
	  a_myexons[i][2] = -1;
	  a_myexons[i][3] = -1;
	  if (fr == '+')
	    a_utrtype[i] = 1;
	  else
	    a_utrtype[i] = 2;
	}
      }

      for (i=m_e_st-1; i>=0; i--) {
	if ((cds_en >= a_myexons[i][0]) && (cds_en <= a_myexons[i][1])) {
	  a_myexons[i][2] = a_myexons[i][0];
	  a_myexons[i][3] = cds_en;
	  break;
	} else {
	  a_myexons[i][2] = -1;
	  a_myexons[i][3] = -1;
	  if (fr == '+')
	    a_utrtype[i] = 2;
	  else
	    a_utrtype[i] = 1;

	}
      }

      //
      // CALC FRAME
      //
      if (fr == '+') {

	int codinglen = 0;
	for (i=0; i<m_e_st; i++) {
	  int ef = -1;
	  if (a_utrtype[i] == 0) {

	    // how much of the previous exon do I need ?
	    int mod = codinglen % 3;
	    if (mod > 0)
	      ef = mod;
	    else
	      ef = 0;

	    // calc coding exon length and add it to codinglen
	    int codexonlen = a_myexons[i][3] - a_myexons[i][2];
	    codinglen += codexonlen;
	  } else {
	    ef = -1;
	  }
	  //printf("ef=%d\n", ef);
	  a_int_fr[i] = ef;
	}
      } else {

	// fr == '-'
	int codinglen = 0;
	for (i=m_e_st-1; i>=0; i--) {
	  int ef = -1;
	  if (a_utrtype[i] == 0) {

	    // how much of the previous exon do I need ?
	    int mod = codinglen % 3;
	    if (mod > 0)
	      ef = mod;
	    else
	      ef = 0;

	    // calc coding exon length and add it to codinglen
	    int codexonlen = a_myexons[i][3] - a_myexons[i][2];
	    codinglen += codexonlen;
	  } else {
	    ef = -1;
	  }
	  a_int_fr[i] = ef;
	  //printf("ef=%d\n", ef);
	}
      }

    } // if (cds_st != cds_en)
    else {
      for (i=0; i<m_e_st; i++) {
	a_int_fr[i] = -1;
      }

    }

    /* debugging code
       int len = 0;
       for (i=0; i<m_e_st; i++) {
       len += (a_myexons[i][3] - a_myexons[i][2]);
       printf("%d <%d-%d> %d\tl=%d\t%d\t%d\n", a_myexons[i][0], a_myexons[i][2], a_myexons[i][3], a_myexons[i][1], len, a_int_fr[i], atoi(a_e_fr[i]));
       }
       printf("\n");

       for (i=0; i<m_e_st; i++) {
       if (a_int_fr[i] != atoi(a_e_fr[i])) {
       printf("DiSCP = %d and %d\n", a_int_fr[i], atoi(a_e_fr[i]));
       //exit(0);
       getchar();
       }
       }
       printf("\n");
    */

    for (i=0; i<m_e_st; i++) {

      if (fr == '+')
	ii = i;
      else
	ii = m_e_st - i - 1;

      // names
      (*a_exons)[cidx][ (*numexons)[cidx] ].tsname    = strdup(n);
      (*a_exons)[cidx][ (*numexons)[cidx] ].chr       = strdup(c);
      (*a_exons)[cidx][ (*numexons)[cidx] ].genename  = strdup(g);

      // num
      (*a_exons)[cidx][ (*numexons)[cidx] ].num       = i+1;

      // whole exon
      (*a_exons)[cidx][ (*numexons)[cidx] ].i         = a_myexons[ii][0] + 1; // added due to refGene format
      (*a_exons)[cidx][ (*numexons)[cidx] ].j         = a_myexons[ii][1];

      // coding exon
      (*a_exons)[cidx][ (*numexons)[cidx] ].ci        = a_myexons[ii][2] + 1; // added due to refGene format
      (*a_exons)[cidx][ (*numexons)[cidx] ].cj        = a_myexons[ii][3];

      // frame
      (*a_exons)[cidx][ (*numexons)[cidx] ].fr        = (fr=='+'?1:-1);

      // exon frame
      // (*a_exons)[cidx][ (*numexons)[cidx] ].ef        = atoi(a_e_fr[ii]);      // refseq
      (*a_exons)[cidx][ (*numexons)[cidx] ].ef        = a_int_fr[ii];      // refseq

      // utr type
      (*a_exons)[cidx][ (*numexons)[cidx] ].utrtype   = a_utrtype[ii];

      // num exons
      (*numexons)[cidx] ++;
    }

    //if (*numexons == MAXNUMINT) {
    //  die("Max number of intervals reached.\n");
    //}

    //
    // END EXONS
    //


    //
    // START PROMOTERS
    //
    (*a_proms)[cidx][ (*numproms)[cidx] ].tsname    = strdup(n);
    (*a_proms)[cidx][ (*numproms)[cidx] ].chr       = strdup(c);
    (*a_proms)[cidx][ (*numproms)[cidx] ].genename  = strdup(g);

    // prom
    if (fr == '+') {
      (*a_proms)[cidx][ (*numproms)[cidx] ].i        = tss-up;
      (*a_proms)[cidx][ (*numproms)[cidx] ].j        = tss+dn;
    } else {
      (*a_proms)[cidx][ (*numproms)[cidx] ].i        = tss-dn;
      (*a_proms)[cidx][ (*numproms)[cidx] ].j        = tss+up;
    }

    // frame
    (*a_proms)[cidx][ (*numproms)[cidx] ].fr        = (fr=='+'?1:-1);

    (*numproms)[cidx] ++;

    //
    // END PROMOTERS
    //


    free(a_myexons);
    free(a_utrtype);
    free(a_e_st);
    free(a_e_en);
    // free(a_e_fr); refseq
    free(a_int_fr);
    free(a);

  }


  free(buff);
  fclose(f);

}



void readRnas(char* file, GenInt*** a_rnas, int** numrnas, int up, int dn, int numchroms, HASH* hc)
{

  char*  buff;
  int    mynmax = 100000;

  char** a;
  int    m;
  FILE*  f;
  int    cidx = -1;
  int    i;

  //char** a_e_st;
  //char** a_e_en;
  //char** a_e_fr;
  //int    m_e_st;
  //int    m_e_en;
  //int    m_e_fr;
  //int**  a_exons;
  //int*   a_utrtype;
  //int    i;
  //int    ii = 0;

  // initialize num rnas per chr
  *numrnas = (int*)calloc(numchroms, sizeof(int));

  int* maxnumrnas = 0;
  nbLinesPerChromInFile(file,1, &maxnumrnas, numchroms, hc);

  // alloc intervals
  *a_rnas = (GenInt**)malloc(numchroms * sizeof(GenInt*));
  if (*a_rnas == 0) {
    die("Problem allocating a_rnas.\n");
  }
  for (i=0; i<numchroms; i++) {
    (*a_rnas)[i] = (GenInt*)calloc(maxnumrnas[i], sizeof(GenInt));
    if ((*a_rnas)[i] == 0) {
      die("Problem allocating a_rnas[i].\n");
    }
  }

  //
  // read set of intervals
  //
  buff  = (char*)malloc(mynmax * sizeof(char));
  f = fopen(file, "r");
  if (f == 0)
    die("Cannot open wgRNA file.\n");
  while (!feof(f)) {
    fgets(buff, mynmax, f);
    if (feof(f))
      break;
    chomp(buff);
    split_line_delim(buff, "\t", &a, &m);

    char* n      = a[4];     //
    char* c      = a[1];     //chr
    if (!HASH_find(hc, c, &cidx)) {
      //printf("Warning: %s not recognized\n", c);
      free(a);
      continue;
    }

    //cidx = chr2index(c);
    //if (cidx == -1) {
    //  free(a);
    //  continue;
    //}

    char  fr     = a[6][0];
    char* g      = a[4];

    int   tss    = (fr=='+'?atoi(a[2]):atoi(a[3]));

    // gene
    (*a_rnas)[cidx][ (*numrnas)[cidx] ].ci        = atoi(a[2]);
    (*a_rnas)[cidx][ (*numrnas)[cidx] ].cj        = atoi(a[3]);

    // prom
    if (fr == '+') {
      (*a_rnas)[cidx][ (*numrnas)[cidx] ].i        = tss-up;
      (*a_rnas)[cidx][ (*numrnas)[cidx] ].j        = tss+dn;
    } else {
      (*a_rnas)[cidx][ (*numrnas)[cidx] ].i        = tss-dn;
      (*a_rnas)[cidx][ (*numrnas)[cidx] ].j        = tss+up;
    }

    (*a_rnas)[cidx][ (*numrnas)[cidx] ].tsname    = strdup(n);
    (*a_rnas)[cidx][ (*numrnas)[cidx] ].chr       = strdup(c);
    (*a_rnas)[cidx][ (*numrnas)[cidx] ].genename  = strdup(g);

    // frame
    (*a_rnas)[cidx][ (*numrnas)[cidx] ].fr        = (fr=='+'?1:-1);

    (*numrnas)[cidx] ++;

    free(a);

  }


  free(buff);
  fclose(f);

}


void readPeaks(char* file, GenInt*** a_peaks, int** numpeaks, int numchroms, HASH* hc)
{

  char*  buff;
  int    mynmax = 100000;

  char** a;
  int    m;
  FILE*  f;
  int    cidx = -1;
  int    i;

  // initialize num rnas per chr
  *numpeaks = (int*)calloc(numchroms, sizeof(int));

  // num peaks per chrom
  int* maxnumpeaks = 0;
  nbLinesPerChromInFile(file,0, &maxnumpeaks, numchroms, hc);

  // alloc intervals
  *a_peaks = (GenInt**)malloc(numchroms * sizeof(GenInt*));
  if (*a_peaks == 0) {
    die("Problem allocating a_rnas.\n");
  }
  for (i=0; i<numchroms; i++) {
    (*a_peaks)[i] = (GenInt*)calloc(maxnumpeaks[i], sizeof(GenInt));
    if ((*a_peaks)[i] == 0) {
      die("Problem allocating a_peaks[i].\n");
    }
  }

  //
  // read set of intervals
  //
  buff  = (char*)malloc(mynmax * sizeof(char));
  f = fopen(file, "r");
  if (f == 0)
    die("Cannot open wgRNA file.\n");
  while (fgets(buff, mynmax, f) != 0) {
    chomp(buff);
    split_line_delim(buff, "\t", &a, &m);

    char* c      = a[0];     //chr
    if (!HASH_find(hc, c, &cidx)) {
      //printf("Warning: %s not recognized\n", c);
      free(a);
      continue;
    }
    //cidx = chr2index(c);
    //if (cidx == -1) {
    //  free(a);
    //  continue;
    //}

    (*a_peaks)[cidx][ (*numpeaks)[cidx] ].chr      = strdup(c);

    // coordinates
    (*a_peaks)[cidx][ (*numpeaks)[cidx] ].i        = atoi(a[1]);
    (*a_peaks)[cidx][ (*numpeaks)[cidx] ].j        = atoi(a[2]);

    (*numpeaks)[cidx] ++;

    free(a);

  }


  free(buff);
  fclose(f);

}







//
// read file and sum up elements in column
//
int readFileSumUpColumn(char* file, int col, int numchroms, HASH* hc)
{

  char*  buff;
  int    mynmax = 1000000;

  char** a;
  int    m;
  FILE*  f;


  // initialize
  int sum = 0;
  buff  = (char*)malloc(mynmax * sizeof(char));
  if (buff == 0)
    die("can't even allocate mynmax bytes\n");
  f = fopen(file, "r");
  if (f == 0)
    die("Cannot open refGene file.\n");
  while (!feof(f)) {
    fgets(buff, mynmax, f);
    if (feof(f))
      break;
    chomp(buff);
    split_line_delim(buff, "\t", &a, &m);
    sum += atoi(a[col]);
    free(a);
  }

  free(buff);
  fclose(f);

  return sum;

}




//
// read file and sum up elements in column
//
void readFileSumUpColumnPerChrom(char* file, int col, int colchr, int** counts, int** countgenes, int numchroms, HASH* hc)
{

  char*  buff;
  int    mynmax = 1000000;

  char** a;
  int    m;
  FILE*  f;
  int    cidx;


  // alloc count vector
  *counts = (int*)calloc(numchroms, sizeof(int));
  *countgenes = (int*)calloc(numchroms, sizeof(int));

  // initialize
  //int sum = 0;
  buff  = (char*)malloc(mynmax * sizeof(char));
  if (buff == 0)
    die("can't even allocate mynmax bytes\n");
  f = fopen(file, "r");
  if (f == 0)
    die("Cannot open refGene file.\n");
  while (!feof(f)) {
    fgets(buff, mynmax, f);
    if (feof(f))
      break;
    chomp(buff);
    split_line_delim(buff, "\t", &a, &m);

    // else get index for chrom
    //cidx = chr2index(a[colchr]);
    if (HASH_find(hc, a[colchr], &cidx)) {


      // update count
      (*counts)[cidx] += atoi(a[col]);
      (*countgenes)[cidx] ++;
    } else {
      //printf("Warning (readFileSumUpColumnPerChrom): %s not recognized\n", a[colchr]);
    }



    free(a);
  }

  free(buff);
  fclose(f);

  //return sum;

}




//
// read file and sum up elements in column
//
void nbLinesPerChromInFile(char* file, int colchr, int** counts, int numchroms, HASH* hc)
{

  char*  buff;
  int    mynmax = 1000000;

  char** a;
  int    m;
  FILE*  f;
  int    cidx;


  // alloc count vector
  *counts = (int*)calloc(numchroms, sizeof(int));
  //*countgenes = (int*)calloc(numchroms, sizeof(int));

  // initialize
  //int sum = 0;
  buff  = (char*)malloc(mynmax * sizeof(char));
  if (buff == 0)
    die("can't even allocate mynmax bytes\n");
  f = fopen(file, "r");
  if (f == 0)
    die("Cannot open refGene file.\n");
  while (!feof(f)) {
    fgets(buff, mynmax, f);
    if (feof(f))
      break;
    chomp(buff);
    split_line_delim(buff, "\t", &a, &m);

    // else get index for chrom
    // cidx = chr2index(a[colchr]);
    if (HASH_find(hc, a[colchr], &cidx)) {



      // update count
      //(*counts)[cidx] += atoi(a[col]);
      (*counts)[cidx] ++;
    } else {
      //printf("Warning: %s not recognized\n", a[colchr]);
    }

    free(a);
  }

  free(buff);
  fclose(f);

  //return sum;

}








