/*
***************************************************************
*
*   Filename    :   documentWidget.h
*   Description :   This is the class describes a widget
*                   used to handle pdf documents
*   Version     :   1.0
*   Created     :   02/11/2010
*
***************************************************************
*
*   Compiler    :   gcc
*   IDE         :   Qt Creator 1.3.1
*
***************************************************************
*
*   Author      :   Eugenia G. Giannopoulou
*   Email       :   eug2002@med.cornell.edu
*
***************************************************************
*/

#ifndef DOCUMENTWIDGET_H
#define DOCUMENTWIDGET_H

#include <QWidget>

namespace Ui {
    class DocumentWidget;
}

class DocumentWidget : public QWidget
{

public:
    DocumentWidget(QWidget *parent);
};

#endif // DOCUMENTWIDGET_H
