/*
***************************************************************
*
*   Filename    :   compareGenesProcess.cpp
*   Description :   This is the class that wraps the
*                   CompareGenes process.
*   Version     :   1.0
*   Created     :   08/02/2010
*
***************************************************************
*
*   Compiler    :   gcc
*   IDE         :   Qt Creator 1.3.1
*
***************************************************************
*
*   Author      :   Eugenia G. Giannopoulou
*   Email       :   eug2002@med.cornell.edu
*
***************************************************************
*/

#include <QString>
#include "compareIntervalsProcess.h"
#include "compareGenesProcess.h"
#include "commonPaths.h"

CompareGenesProcess::CompareGenesProcess()
{
}

QString CompareGenesProcess::getGenesFile1() {
    return this->genesFile1;
}

void CompareGenesProcess::setGenesFile1(QString genes1) {
    this->genesFile1 = genes1;
}

QString CompareGenesProcess::getGenesFile2() {
    return this->genesFile2;
}

void CompareGenesProcess::setGenesFile2(QString genes2) {
    this->genesFile2 = genes2;
}

QString CompareGenesProcess::getOutputFile() {
    return this->outputFile;
}

void CompareGenesProcess::setOutputFile(QString oFile) {
    this->outputFile = oFile;
}

bool CompareGenesProcess::getKeepUniqRec() {
    return this->keepUniqRec;
}

void CompareGenesProcess::setKeepUniqRec(bool unique) {
    this->keepUniqRec = unique;
}

bool CompareGenesProcess::getFromError() {
    return this->fromError;
}

void CompareGenesProcess::setFromError(bool err) {
    this->fromError = err;
}
