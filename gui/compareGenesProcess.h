/*
***************************************************************
*
*   Filename    :   compareGenesProcess.h
*   Description :   This is the header of the class that wraps
*                   the CompareGenes process.
*   Version     :   1.0
*   Created     :   08/02/2010
*
***************************************************************
*
*   Compiler    :   gcc
*   IDE         :   Qt Creator 1.3.1
*
***************************************************************
*
*   Author      :   Eugenia G. Giannopoulou
*   Email       :   eug2002@med.cornell.edu
*
***************************************************************
*/

#ifndef COMPAREGENESPROCESS_H
#define COMPAREGENESPROCESS_H

#include <QProcess>
#include <QString>

namespace Ui {
    class CompareGenesProcess;
}

class CompareGenesProcess : public QProcess
{
public:
    CompareGenesProcess();
    QString getGenesFile1();
    void setGenesFile1(QString genes1);
    QString getGenesFile2();
    void setGenesFile2(QString genes2);
    QString getOutputFile();
    void setOutputFile(QString oFile);
    bool getKeepUniqRec();
    void setKeepUniqRec(bool unique);
    bool getFromError();
    void setFromError(bool err);

private:
    QString genesFile1;
    QString genesFile2;
    QString outputFile;
    bool    keepUniqRec;
    bool    fromError;
};

#endif // COMPAREGENESPROCESS_H
