/*
***************************************************************
*
*   Filename    :   compareIntervalsProcess.h
*   Description :   This is the header of the class that wraps
*                   the CompareIntervals process.
*   Version     :   1.0
*   Created     :   07/28/2010
*
***************************************************************
*
*   Compiler    :   gcc
*   IDE         :   Qt Creator 1.3.1
*
***************************************************************
*
*   Author      :   Eugenia G. Giannopoulou
*   Email       :   eug2002@med.cornell.edu
*
***************************************************************
*/

#ifndef COMPAREINTERVALSPROCESS_H
#define COMPAREINTERVALSPROCESS_H

#include <QProcess>
#include <QString>

namespace Ui {
    class CompareIntervalsProcess;
}

class CompareIntervalsProcess : public QProcess
{
public:
    CompareIntervalsProcess();
    QString getPeaksFile1();
    void setPeaksFile1(QString peaks1);
    QString getPeaksFile2();
    void setPeaksFile2(QString peaks2);
    QString getOutputFile();
    void setOutputFile(QString oFile);
    QString getPrefix();
    void setPrefix(QString pfx);
    int getIterations();
    void setIterations(int i);
    bool getHasID1();
    void setHasID1(bool id1);
    bool getHasID2();
    void setHasID2(bool id2);
    bool getHasHeader1();
    void setHasHeader1(bool header1);
    bool getHasHeader2();
    void setHasHeader2(bool header2);
    bool getUseCenter1();
    void setUseCenter1(bool center1);
    bool getUseCenter2();
    void setUseCenter2(bool center2);
    QString getCompType();
    void setCompType(QString type);
    bool getShowUnPeaks();
    void setShowUnPeaks(bool unPeaks);
    bool getShowOvPeaks();
    void setShowOvPeaks(bool ovPeaks);
    bool getShowDesc();
    void setShowDesc(bool desc);
    QString getRandmode();
    void setRandmode(QString rm);
    bool getFromError();
    void setFromError(bool err);

private:
    QString peaksFile1;
    QString peaksFile2;
    QString outputFile;
    QString prefix;
    int     iterations;
    bool    hasID1;
    bool    hasID2;
    bool    hasHeader1;
    bool    hasHeader2;
    bool    useCenter1;
    bool    useCenter2;
    QString compType;
    bool    showUnPeaks;
    bool    showOvPeaks;
    bool    showDesc;
    QString randmode;
    bool    fromError;
};

#endif // COMPAREINTERVALSPROCESS_H
