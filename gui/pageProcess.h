/*
***************************************************************
*
*   Filename    :   pageProcess.h
*   Description :   This is the header file for the class that
*                   wraps the CSiPAGE process.
*   Version     :   1.0
*   Created     :   18/01/2011
*
***************************************************************
*
*   Compiler    :   gcc
*   IDE         :   Qt Creator 1.3.1
*
***************************************************************
*
*   Author      :   Eugenia G. Giannopoulou
*   Email       :   eug2002@med.cornell.edu
*
***************************************************************
*/

#ifndef PAGEPROCESS_H
#define PAGEPROCESS_H

#include <QProcess>
#include <QString>

namespace Ui {
    class PageProcess;
}

class PageProcess : public QProcess
{
public:
    PageProcess();
    QString getPeaksFile();
    void setPeaksFile(QString peaks);
    QString getOutputFile();
    void setOutputFile(QString oFile);
    QString getSpecies();
    void setSpecies(QString sp);
    QString getPathwaysDB();
    void setPathwaysDB(QString pdb);
    QString getGenesDB();
    void setGenesDB(QString gdb);
    QString getGeneparts();
    void setGeneparts(QString gp);
    bool getFromError();
    void setFromError(bool err);

private:
    QString peaksFile;
    QString outputFile;
    QString species;
    QString pathwaysDB;
    QString genesDB;
    QString geneparts;
    bool    fromError;
};

#endif // PAGEPROCESS_H
