/*
***************************************************************
*
*   Filename    :   findPathwayProcess.h
*   Description :   This is the header file for the class that
*                   wraps the CSPathwayMatch process.
*   Version     :   1.0
*   Created     :   04/02/2010
*
***************************************************************
*
*   Compiler    :   gcc
*   IDE         :   Qt Creator 1.3.1
*
***************************************************************
*
*   Author      :   Eugenia G. Giannopoulou
*   Email       :   eug2002@med.cornell.edu
*
***************************************************************
*/

#ifndef FINDPATHWAYPROCESS_H
#define FINDPATHWAYPROCESS_H

#include <QProcess>
#include <QString>

namespace Ui {
    class FindPathwayProcess;
}

class FindPathwayProcess : public QProcess
{
public:
    FindPathwayProcess();
    QString getPeaksFile();
    void setPeaksFile(QString peaks);
    QString getOutputFile();
    void setOutputFile(QString oFile);
    QString getSpecies();
    void setSpecies(QString sp);
    QString getPathwaysDB();
    void setPathwaysDB(QString pdb);
    QString getDatabase();
    void setDatabase(QString db);
    QString getPathwayName();
    void setPathwayName(QString pathway);
    QString getGeneparts();
    void setGeneparts(QString gp);
    bool getFromError();
    void setFromError(bool err);

private:
    QString peaksFile;
    QString outputFile;
    QString species;
    QString pathwaysDB;
    QString database;
    QString pathwayName;
    QString geneparts;
    bool    fromError;
};

#endif // FINDPATHWAYPROCESS_H
