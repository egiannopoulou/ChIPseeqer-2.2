/*
***************************************************************
*
*   Filename    :   annotateProcess.h
*   Description :   This is the header of the class that wraps
*                   the CSAnnotate process.
*   Version     :   1.0
*   Created     :   04/02/2010
*
***************************************************************
*
*   Compiler    :   gcc
*   IDE         :   Qt Creator 1.3.1
*
***************************************************************
*
*   Author      :   Eugenia G. Giannopoulou
*   Email       :   eug2002@med.cornell.edu
*
***************************************************************
*/

#ifndef ANNOTATEPROCESS_H
#define ANNOTATEPROCESS_H

#include <QProcess>
#include <QString>

namespace Ui {
    class AnnotateProcess;
}

class AnnotateProcess : public QProcess
{
public:
    AnnotateProcess();
    QString getPeaksFile();
    void setPeaksFile(QString peaks);
    QString getOutputFile();
    void setOutputFile(QString oFile);
    QString getDatabase();
    void setDatabase(QString db);
    QString getSpecies();
    void setSpecies(QString sp);
    int getLenPU();
    void setLenPU(int ldwu);
    int getLenPD();
    void setLenPD(int ldwd);
    int getLenDWU();
    void setLenDWU(int ldwu);
    int getLenDWD();
    void setLenDWD(int ldwd);
    int getMinDistAway();
    void setMinDistAway(int mnda);
    int getMaxDistAway();
    void setMaxDistAway(int mxda);
    bool getType();
    void setType(bool t);
    int getIterations();
    void setIterations(int i);
    QString getMode();
    void setMode(QString mode);
    QString getTFname();
    void setTFname(QString tfname);
    QString getPrefix();
    void setPrefix(QString prefix);
    bool getFromError();
    void setFromError(bool err);

private:
    QString peaksFile;
    QString outputFile;
    QString database;
    QString species;
    int     lenPU;
    int     lenPD;
    int     lenDWU;
    int     lenDWD;
    int     minDistAway;
    int     maxDistAway;
    bool    type;
    int     iterations;
    QString mode;
    QString TFname;
    QString prefix;
    bool    fromError;
};

#endif // ANNOTATEPROCESS_H

