/*
***************************************************************
*
*   Filename    :   splitProcess.h
*   Description :   This is the class that wraps the
*                   split_raw_data process.
*   Version     :   1.0
*   Created     :   04/02/2010
*
***************************************************************
*
*   Compiler    :   gcc
*   IDE         :   Qt Creator 1.3.1
*
***************************************************************
*
*   Author      :   Eugenia G. Giannopoulou
*   Email       :   eug2002@med.cornell.edu
*
***************************************************************
*/

#include <QString>
#include "splitProcess.h"
#include "commonPaths.h"

SplitProcess::SplitProcess() {
}

QString SplitProcess::getDataFormat() {
    return this->dataFormat;
}

void SplitProcess::setDataFormat(QString format) {
    this->dataFormat = format;
}

QString SplitProcess::getDataFolder() {
    return this->dataFolder;
}

void SplitProcess::setDataFolder(QString dFolder) {
    this->dataFolder = dFolder;
}

QString SplitProcess::getOutputFolder() {
    return this->outputFolder;
}

void SplitProcess::setOutputFolder(QString oFolder) {
    this->outputFolder = oFolder;
}

bool SplitProcess::getChipFlag() {
    return this->chipFlag;
}

void SplitProcess::setChipFlag(bool flag) {
    this->chipFlag = flag;
}

bool SplitProcess::getFromError() {
    return this->fromError;
}

void SplitProcess::setFromError(bool err) {
    this->fromError = err;
}
