/*
***************************************************************
*
*   Filename    :   pageProcess.cpp
*   Description :   This is the class that wraps the
*                   CSiPAGE process.
*   Version     :   1.0
*   Created     :   18/01/2011
*
***************************************************************
*
*   Compiler    :   gcc
*   IDE         :   Qt Creator 1.3.1
*
***************************************************************
*
*   Author      :   Eugenia G. Giannopoulou
*   Email       :   eug2002@med.cornell.edu
*
***************************************************************
*/

#include <QString>
#include "pageProcess.h"
#include "commonPaths.h"

PageProcess::PageProcess()
{
}

QString PageProcess::getPeaksFile() {
    return this->peaksFile;
}

void PageProcess::setPeaksFile(QString peaks) {
    this->peaksFile = peaks;
}

QString PageProcess::getOutputFile() {
    return this->outputFile;
}

void PageProcess::setOutputFile(QString oFile) {
    this->outputFile = oFile;
}

QString PageProcess::getSpecies() {
    return this->species;
}

void PageProcess::setSpecies(QString sp) {
    this->species = sp;
}

QString PageProcess::getPathwaysDB() {
    return this->pathwaysDB;
}

void PageProcess::setPathwaysDB(QString pdb) {
    this->pathwaysDB = pdb;
}

QString PageProcess::getGenesDB() {
    return this->genesDB;
}

void PageProcess::setGenesDB(QString gdb) {
    this->genesDB = gdb;
}

QString PageProcess::getGeneparts() {
    return this->geneparts;
}

void PageProcess::setGeneparts(QString gp) {
    this->geneparts = gp;
}

bool PageProcess::getFromError() {
    return this->fromError;
}

void PageProcess::setFromError(bool err) {
    this->fromError = err;
}
