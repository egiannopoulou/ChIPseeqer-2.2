/*
***************************************************************
*
*   Filename    :   computeJaccardProcess.cpp
*   Description :   This is the class that wraps the
*                   ComputeJaccard process.
*   Version     :   1.0
*   Created     :   04/02/2010
*
***************************************************************
*
*   Compiler    :   gcc
*   IDE         :   Qt Creator 1.3.1
*
***************************************************************
*
*   Author      :   Eugenia G. Giannopoulou
*   Email       :   eug2002@med.cornell.edu
*
***************************************************************
*/

#include <QString>
#include "computeJaccardProcess.h"
#include "commonPaths.h"

ComputeJaccardProcess::ComputeJaccardProcess()
{
}

QString ComputeJaccardProcess::getPeaksFile() {
    return this->peaksFile;
}

void ComputeJaccardProcess::setPeaksFile(QString peaks) {
    this->peaksFile = peaks;
}

QString ComputeJaccardProcess::getOutputFile() {
    return this->outputFile;
}

void ComputeJaccardProcess::setOutputFile(QString oFile) {
    this->outputFile = oFile;
}

bool ComputeJaccardProcess::getFromError() {
    return this->fromError;
}

void ComputeJaccardProcess::setFromError(bool err) {
    this->fromError = err;
}
