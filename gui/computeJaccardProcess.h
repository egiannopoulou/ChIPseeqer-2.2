/*
***************************************************************
*
*   Filename    :   computeJaccardProcess.h
*   Description :   This is the header of the class that wraps
*                   the ComputeJaccard process.
*   Version     :   1.0
*   Created     :   13/01/2011
*
***************************************************************
*
*   Compiler    :   gcc
*   IDE         :   Qt Creator 1.3.1
*
***************************************************************
*
*   Author      :   Eugenia G. Giannopoulou
*   Email       :   eug2002@med.cornell.edu
*
***************************************************************
*/

#ifndef COMPUTEJACCARDPROCESS_H
#define COMPUTEJACCARDPROCESS_H

#include <QProcess>
#include <QString>

namespace Ui {
    class ComputeJaccardProcess;
}

class ComputeJaccardProcess : public QProcess
{
public:
    ComputeJaccardProcess();
    QString getPeaksFile();
    void setPeaksFile(QString peaks);
    QString getOutputFile();
    void setOutputFile(QString oFile);
    bool getFromError();
    void setFromError(bool err);

private:
    QString peaksFile;
    QString outputFile;
    bool    fromError;
};

#endif // COMPUTEJACCARDPROCESS_H
