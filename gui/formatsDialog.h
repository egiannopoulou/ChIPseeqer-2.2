/*
***************************************************************
*
*   Filename    :   formatsDialog.h
*   Description :   This is the header of the formatsDialog
*                   class.
*   Version     :   1.0
*   Created     :   04/02/2010
*
***************************************************************
*
*   Compiler    :   gcc
*   IDE         :   Qt Creator 1.3.1
*
***************************************************************
*
*   Author      :   Eugenia G. Giannopoulou
*   Email       :   eug2002@med.cornell.edu
*
***************************************************************
*/

#ifndef FORMATSDIALOG_H
#define FORMATSDIALOG_H

#include <QDialog>
#include <QWidget>
#include <QEvent>

namespace Ui {
    class FormatsDialog;
}

class FormatsDialog : public QDialog {
    Q_OBJECT
public:
    FormatsDialog(QWidget *parent = 0);
    ~FormatsDialog();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::FormatsDialog *ui;

private slots:
    void on_pushButton_clicked();
};

#endif // FORMATSDIALOG_H
