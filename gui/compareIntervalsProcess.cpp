/*
***************************************************************
*
*   Filename    :   compareIntervalsProcess.cpp
*   Description :   This is the class that wraps the
*                   CompareIntervals process.
*   Version     :   1.0
*   Created     :   07/29/2010
*
***************************************************************
*
*   Compiler    :   gcc
*   IDE         :   Qt Creator 1.3.1
*
***************************************************************
*
*   Author      :   Eugenia G. Giannopoulou
*   Email       :   eug2002@med.cornell.edu
*
***************************************************************
*/

#include <QString>
#include "compareIntervalsProcess.h"
#include "commonPaths.h"

CompareIntervalsProcess::CompareIntervalsProcess()
{
}

QString CompareIntervalsProcess::getPeaksFile1() {
    return this->peaksFile1;
}

void CompareIntervalsProcess::setPeaksFile1(QString peaks1) {
    this->peaksFile1 = peaks1;
}

QString CompareIntervalsProcess::getPeaksFile2() {
    return this->peaksFile2;
}

void CompareIntervalsProcess::setPeaksFile2(QString peaks2) {
    this->peaksFile2 = peaks2;
}

QString CompareIntervalsProcess::getOutputFile() {
    return this->outputFile;
}

void CompareIntervalsProcess::setOutputFile(QString oFile) {
    this->outputFile = oFile;
}

QString CompareIntervalsProcess::getPrefix() {
    return this->prefix;
}

void CompareIntervalsProcess::setPrefix(QString pfx) {
    this->prefix = pfx;
}

int CompareIntervalsProcess::getIterations() {
    return this->iterations;
}

void CompareIntervalsProcess::setIterations(int i) {
    this->iterations = i;
}

bool CompareIntervalsProcess::getHasID1() {
    return this->hasID1;
}

void CompareIntervalsProcess::setHasID1(bool id1) {
    this->hasID1 = id1;
}

bool CompareIntervalsProcess::getHasID2() {
    return this->hasID2;
}

void CompareIntervalsProcess::setHasID2(bool id2) {
    this->hasID2 = id2;
}

bool CompareIntervalsProcess::getHasHeader1() {
    return this->hasHeader1;
}

void CompareIntervalsProcess::setHasHeader1(bool header1) {
    this->hasHeader1 = header1;
}

bool CompareIntervalsProcess::getHasHeader2() {
    return this->hasHeader2;
}

void CompareIntervalsProcess::setHasHeader2(bool header2) {
    this->hasHeader2 = header2;
}

bool CompareIntervalsProcess::getUseCenter1() {
    return this->useCenter1;
}

void CompareIntervalsProcess::setUseCenter1(bool center1) {
    this->useCenter1 = center1;
}

bool CompareIntervalsProcess::getUseCenter2() {
    return this->useCenter2;
}

void CompareIntervalsProcess::setUseCenter2(bool center2) {
    this->useCenter2 = center2;
}

QString CompareIntervalsProcess::getCompType() {
    return this->compType;
}

void CompareIntervalsProcess::setCompType(QString type) {
    this->compType = type;
}

bool CompareIntervalsProcess::getShowUnPeaks() {
    return this->showUnPeaks;
}

void CompareIntervalsProcess::setShowUnPeaks(bool unPeaks) {
    this->showUnPeaks = unPeaks;
}

bool CompareIntervalsProcess::getShowOvPeaks() {
    return this->showOvPeaks;
}

void CompareIntervalsProcess::setShowOvPeaks(bool ovPeaks) {
    this->showOvPeaks = ovPeaks;
}

bool CompareIntervalsProcess::getShowDesc() {
    return this->showDesc;
}

void CompareIntervalsProcess::setShowDesc(bool desc) {
    this->showDesc = desc;
}

QString CompareIntervalsProcess::getRandmode() {
    return this->randmode;
}

void CompareIntervalsProcess::setRandmode(QString rm) {
    this->randmode = rm;
}

bool CompareIntervalsProcess::getFromError() {
    return this->fromError;
}

void CompareIntervalsProcess::setFromError(bool err) {
    this->fromError = err;
}
