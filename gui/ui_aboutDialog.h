/********************************************************************************
** Form generated from reading UI file 'aboutDialog.ui'
**
** Created by: Qt User Interface Compiler version 4.8.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ABOUTDIALOG_H
#define UI_ABOUTDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_AboutDialog
{
public:
    QWidget *layoutWidget;
    QGridLayout *gridLayout;
    QLabel *logoLabel;
    QVBoxLayout *verticalLayout;
    QLabel *nameLabel;
    QLabel *aboutLabel;
    QPushButton *pushButton;

    void setupUi(QDialog *AboutDialog)
    {
        if (AboutDialog->objectName().isEmpty())
            AboutDialog->setObjectName(QString::fromUtf8("AboutDialog"));
        AboutDialog->resize(436, 278);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/Icons/CS_logo_64.png"), QSize(), QIcon::Normal, QIcon::Off);
        AboutDialog->setWindowIcon(icon);
        layoutWidget = new QWidget(AboutDialog);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(8, 4, 421, 266));
        gridLayout = new QGridLayout(layoutWidget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        logoLabel = new QLabel(layoutWidget);
        logoLabel->setObjectName(QString::fromUtf8("logoLabel"));
        logoLabel->setMaximumSize(QSize(130, 16777215));
        logoLabel->setTextFormat(Qt::PlainText);
        logoLabel->setPixmap(QPixmap(QString::fromUtf8(":/Icons/CS_logo_128.png")));

        gridLayout->addWidget(logoLabel, 0, 0, 1, 1);

        verticalLayout = new QVBoxLayout();
#ifndef Q_OS_MAC
        verticalLayout->setSpacing(-1);
#endif
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        nameLabel = new QLabel(layoutWidget);
        nameLabel->setObjectName(QString::fromUtf8("nameLabel"));
        QFont font;
        font.setPointSize(18);
        font.setBold(true);
        font.setWeight(75);
        nameLabel->setFont(font);
        nameLabel->setTextFormat(Qt::RichText);
        nameLabel->setWordWrap(true);

        verticalLayout->addWidget(nameLabel);

        aboutLabel = new QLabel(layoutWidget);
        aboutLabel->setObjectName(QString::fromUtf8("aboutLabel"));
        QFont font1;
        font1.setPointSize(13);
        font1.setBold(false);
        font1.setWeight(50);
        aboutLabel->setFont(font1);
        aboutLabel->setTextFormat(Qt::AutoText);
        aboutLabel->setWordWrap(true);

        verticalLayout->addWidget(aboutLabel);

        verticalLayout->setStretch(0, 10);

        gridLayout->addLayout(verticalLayout, 0, 1, 1, 2);

        pushButton = new QPushButton(layoutWidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setMaximumSize(QSize(100, 16777215));
        pushButton->setIconSize(QSize(12, 12));

        gridLayout->addWidget(pushButton, 1, 2, 1, 1);


        retranslateUi(AboutDialog);

        QMetaObject::connectSlotsByName(AboutDialog);
    } // setupUi

    void retranslateUi(QDialog *AboutDialog)
    {
        AboutDialog->setWindowTitle(QApplication::translate("AboutDialog", "About ChIPseeqer", 0, QApplication::UnicodeUTF8));
        nameLabel->setText(QApplication::translate("AboutDialog", "ChIPseeqer v2.1", 0, QApplication::UnicodeUTF8));
        aboutLabel->setText(QApplication::translate("AboutDialog", "Developed on Qt 4.8.2 \n"
"\n"
"For questions/comments contact: \n"
"\n"
"Eugenia Giannopoulou (eug2002@med.cornell.edu) \n"
"\n"
"Institute for Computational Biomedicine \n"
"Weill Cornell Medical College\n"
"", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("AboutDialog", "Close", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class AboutDialog: public Ui_AboutDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ABOUTDIALOG_H
