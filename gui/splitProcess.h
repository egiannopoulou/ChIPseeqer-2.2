/*
***************************************************************
*
*   Filename    :   splitProcess.h
*   Description :   This is the header file for the class that
*                   wraps the split_raw_data process.
*   Version     :   1.0
*   Created     :   04/02/2010
*
***************************************************************
*
*   Compiler    :   gcc
*   IDE         :   Qt Creator 1.3.1
*
***************************************************************
*
*   Author      :   Eugenia G. Giannopoulou
*   Email       :   eug2002@med.cornell.edu
*
***************************************************************
*/

#ifndef SPLITPROCESS_H
#define SPLITPROCESS_H

#include <QProcess>
#include <QString>

namespace Ui {
    class SplitProcess;
}

class SplitProcess : public QProcess
{
public:
    SplitProcess();
    QString getDataFormat();
    void setDataFormat(QString format);
    QString getDataFolder();
    void setDataFolder(QString dFolder);
    QString getOutputFolder();
    void setOutputFolder(QString oFolder);
    bool getChipFlag();
    void setChipFlag(bool flag);
    bool getFromError();
    void setFromError(bool err);

private:
    QString dataFormat;
    QString dataFolder;
    QString outputFolder;
    bool    chipFlag;
    bool    fromError;
};

#endif // SPLITPROCESS_H
