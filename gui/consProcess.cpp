/*
***************************************************************
*
*   Filename    :   consProcess.cpp
*   Description :   This is the class that wraps the
*                   CS2Cons process.
*   Version     :   1.0
*   Created     :   04/02/2010
*
***************************************************************
*
*   Compiler    :   gcc
*   IDE         :   Qt Creator 1.3.1
*
***************************************************************
*
*   Author      :   Eugenia G. Giannopoulou
*   Email       :   eug2002@med.cornell.edu
*
***************************************************************
*/

#include <QString>
#include "consProcess.h"
#include "commonPaths.h"

ConsProcess::ConsProcess()
{
}

QString ConsProcess::getPeaksFile() {
    return this->peaksFile;
}

void ConsProcess::setPeaksFile(QString peaks) {
    this->peaksFile = peaks;
}

QString ConsProcess::getOutputFile() {
    return this->outputFile;
}

void ConsProcess::setOutputFile(QString oFile) {
    this->outputFile = oFile;
}

QString ConsProcess::getOutputRandomFile() {
    return this->outputRandomFile;
}

void ConsProcess::setOutputRandomFile(QString orFile) {
    this->outputRandomFile = orFile;
}

QString ConsProcess::getConsFolder() {
    return this->consFolder;
}

void ConsProcess::setConsFolder(QString cFolder) {
    this->consFolder = cFolder;
}

QString ConsProcess::getFormat() {
    return this->format;
}

void ConsProcess::setFormat(QString form) {
    this->format = form;
}

QString ConsProcess::getCategory() {
    return this->category;
}

void ConsProcess::setCategory(QString cat) {
    this->category = cat;
}

QString ConsProcess::getMethod() {
    return this->method;
}

void ConsProcess::setMethod(QString met) {
    this->method = met;
}

QString ConsProcess::getGenome() {
    return this->genome;
}

void ConsProcess::setGenome(QString gen) {
    this->genome = gen;
}

bool ConsProcess::getMakeRandom() {
    return this->makeRandom;
}

void ConsProcess::setMakeRandom(bool rand) {
    this->makeRandom = rand;
}

int ConsProcess::getRanDistance() {
    return this->ranDist;
}

void ConsProcess::setRanDistance(int t) {
    this->ranDist = t;
}

bool ConsProcess::getShowProfiles() {
    return this->showProfiles;
}

void ConsProcess::setShowProfiles(bool prof) {
    this->showProfiles = prof;
}

int ConsProcess::getWindowSize() {
    return this->windowSize;
}

void ConsProcess::setWindowSize(int ws) {
    this->windowSize = ws;
}

double ConsProcess::getThreshold() {
    return this->threshold;
}

void ConsProcess::setThreshold(double t) {
    this->threshold = t;
}

bool ConsProcess::getAroundSummit() {
    return this->aroundSummit;
}

void ConsProcess::setAroundSummit(bool as) {
    this->aroundSummit = as;
}

bool ConsProcess::getFromError() {
    return this->fromError;
}

void ConsProcess::setFromError(bool err) {
    this->fromError = err;
}


