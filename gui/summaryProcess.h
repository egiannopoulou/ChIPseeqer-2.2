/*
***************************************************************
*
*   Filename    :   summaryProcess.h
*   Description :   This is the header of the class that wraps
*                   the CSSummary process.
*   Version     :   1.0
*   Created     :   04/02/2010
*
***************************************************************
*
*   Compiler    :   gcc
*   IDE         :   Qt Creator 1.3.1
*
***************************************************************
*
*   Author      :   Eugenia G. Giannopoulou
*   Email       :   eug2002@med.cornell.edu
*
***************************************************************
*/

#ifndef SUMMARYPROCESS_H
#define SUMMARYPROCESS_H

#include <QProcess>
#include <QString>

namespace Ui {
    class SummaryProcess;
}

class SummaryProcess : public QProcess
{
public:
    SummaryProcess();
    QString getPeaksFile();
    void setPeaksFile(QString peaks);
    QString getOutputFile();
    void setOutputFile(QString oFile);
    QString getDatabase();
    void setDatabase(QString db);
    QString getSpecies();
    void setSpecies(QString sp);
    int getLenU();
    void setLenU(int lu);
    int getLenD();
    void setLenD(int ld);
    bool getFromError();
    void setFromError(bool err);

private:
    QString peaksFile;
    QString outputFile;
    QString database;
    QString species;
    int     lenU;
    int     lenD;
    bool    fromError;
};

#endif // SUMMARYPROCESS_H

