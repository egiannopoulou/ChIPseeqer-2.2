/*
***************************************************************
*
*   Filename    :   annotateProcess.cpp
*   Description :   This is the class that wraps the
*                   CSAnnotate process.
*   Version     :   1.0
*   Created     :   04/02/2010
*
***************************************************************
*
*   Compiler    :   gcc
*   IDE         :   Qt Creator 1.3.1
*
***************************************************************
*
*   Author      :   Eugenia G. Giannopoulou
*   Email       :   eug2002@med.cornell.edu
*
***************************************************************
*/

#include <QString>
#include "annotateProcess.h"
#include "commonPaths.h"

AnnotateProcess::AnnotateProcess()
{
}

QString AnnotateProcess::getPeaksFile() {
    return this->peaksFile;
}

void AnnotateProcess::setPeaksFile(QString peaks) {
    this->peaksFile = peaks;
}

QString AnnotateProcess::getOutputFile() {
    return this->outputFile;
}

void AnnotateProcess::setOutputFile(QString oFile) {
    this->outputFile = oFile;
}

QString AnnotateProcess::getDatabase() {
    return this->database;
}

void AnnotateProcess::setDatabase(QString db) {
    this->database = db;
}

QString AnnotateProcess::getSpecies() {
    return this->species;
}

void AnnotateProcess::setSpecies(QString sp) {
    this->species = sp;
}

int AnnotateProcess::getLenPU() {
    return this->lenPU;
}

void AnnotateProcess::setLenPU(int lpu) {
    this->lenPU = lpu;
}

int AnnotateProcess::getLenPD() {
    return this->lenPD;
}

void AnnotateProcess::setLenPD(int lpd) {
    this->lenPD = lpd;
}

int AnnotateProcess::getLenDWU() {
    return this->lenDWU;
}

void AnnotateProcess::setLenDWU(int ldwu) {
    this->lenDWU = ldwu;
}

int AnnotateProcess::getLenDWD() {
    return this->lenDWD;
}

void AnnotateProcess::setLenDWD(int ldwd) {
    this->lenDWD = ldwd;
}

int AnnotateProcess::getMinDistAway() {
    return this->minDistAway;
}

void AnnotateProcess::setMinDistAway(int mnda) {
    this->minDistAway = mnda;
}

int AnnotateProcess::getMaxDistAway() {
    return this->maxDistAway;
}

void AnnotateProcess::setMaxDistAway(int mxda) {
    this->maxDistAway = mxda;
}

bool AnnotateProcess::getType() {
    return this->type;
}

void AnnotateProcess::setType(bool t) {
    this->type = t;
}

int AnnotateProcess::getIterations() {
    return this->iterations;
}

void AnnotateProcess::setIterations(int i) {
    this->iterations = i;
}

QString AnnotateProcess::getMode() {
    return this->mode;
}

void AnnotateProcess::setMode(QString m) {
    this->mode = m;
}

QString AnnotateProcess::getTFname() {
    return this->TFname;
}

void AnnotateProcess::setTFname(QString tfname) {
    this->TFname = tfname;
}

QString AnnotateProcess::getPrefix() {
    return this->prefix;
}

void AnnotateProcess::setPrefix(QString pfx) {
    this->prefix = pfx;
}

bool AnnotateProcess::getFromError() {
    return this->fromError;
}

void AnnotateProcess::setFromError(bool err) {
    this->fromError = err;
}
