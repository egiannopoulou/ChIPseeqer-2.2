/*
***************************************************************
*
*   Filename    :   chipseeqerProcess.h
*   Description :   This is the header of the class that wraps
*                   the chipseeqer process.
*   Version     :   1.0
*   Created     :   04/02/2010
*
***************************************************************
*
*   Compiler    :   gcc
*   IDE         :   Qt Creator 1.3.1
*
***************************************************************
*
*   Author      :   Eugenia G. Giannopoulou
*   Email       :   eug2002@med.cornell.edu
*
***************************************************************
*/

#ifndef CHIPSEEQERPROCESS_H
#define CHIPSEEQERPROCESS_H

#include <QProcess>
#include <QString>

namespace Ui {
    class ChipseeqerProcess;
}

class ChipseeqerProcess : public QProcess
{
public:
    ChipseeqerProcess();
    QString getChipFolder();
    void setChipFolder(QString cFolder);
    QString getInputFolder();
    void setInputFolder(QString iFolder);
    QString getOutputFile();
    void setOutputFile(QString oFile);
    QString getDataFormat();
    void setDataFormat(QString format);
    QString getChrdata();
    void setChrdata(QString chr);
    double getFold();
    void setFold(double f);
    int getThreshold();
    void setThreshold(int t);
    int getFragmentLength();
    void setFragmentLength(int t);
    int getMinimumLength();
    void setMinimumLength(int t);
    int getMinimumDistance();
    void setMinimumDistance(int t);
    int getMinPeakHeight();
    void setMinPeakHeight(int t);
    bool getUniqueReads();
    void setUniqueReads(bool t);
    bool getFromError();
    void setFromError(bool err);

private:
    QString chipFolder;
    QString inputFolder;
    QString outputFile;
    QString dataFormat;
    QString chrdata;
    double  fold;
    int     threshold;
    int     fragmentLength;
    int     minimumLength;
    int     minimumDistance;
    int     minPeakHeight;
    bool    uniqueReads;
    bool    fromError;
};

#endif // CHIPSEEQERPROCESS_H
