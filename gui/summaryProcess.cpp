/*
***************************************************************
*
*   Filename    :   summaryProcess.cpp
*   Description :   This is the class that wraps the CSSummary
*                   process.
*   Version     :   1.0
*   Created     :   04/02/2010
*
***************************************************************
*
*   Compiler    :   gcc
*   IDE         :   Qt Creator 1.3.1
*
***************************************************************
*
*   Author      :   Eugenia G. Giannopoulou
*   Email       :   eug2002@med.cornell.edu
*
***************************************************************
*/

#include <QString>
#include "summaryProcess.h"
#include "commonPaths.h"

SummaryProcess::SummaryProcess()
{
}

QString SummaryProcess::getPeaksFile() {
    return this->peaksFile;
}

void SummaryProcess::setPeaksFile(QString peaks) {
    this->peaksFile = peaks;
}

QString SummaryProcess::getOutputFile() {
    return this->outputFile;
}

void SummaryProcess::setOutputFile(QString oFile) {
    this->outputFile = oFile;
}

QString SummaryProcess::getDatabase() {
    return this->database;
}

void SummaryProcess::setDatabase(QString db) {
    this->database = db;
}

QString SummaryProcess::getSpecies() {
    return this->species;
}

void SummaryProcess::setSpecies(QString sp) {
    this->species = sp;
}

int SummaryProcess::getLenU() {
    return this->lenU;
}

void SummaryProcess::setLenU(int lu) {
    this->lenU = lu;
}

int SummaryProcess::getLenD() {
    return this->lenD;
}

void SummaryProcess::setLenD(int ld) {
    this->lenD = ld;
}

bool SummaryProcess::getFromError() {
    return this->fromError;
}

void SummaryProcess::setFromError(bool err) {
    this->fromError = err;
}
