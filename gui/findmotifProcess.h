/*
***************************************************************
*
*   Filename    :   findmotifProcess.h
*   Description :   This is the header file for the class that
*                   wraps the CSFindPeaksWithMotifsMatch process.
*   Version     :   1.0
*   Created     :   04/02/2010
*
***************************************************************
*
*   Compiler    :   gcc
*   IDE         :   Qt Creator 1.3.1
*
***************************************************************
*
*   Author      :   Eugenia G. Giannopoulou
*   Email       :   eug2002@med.cornell.edu
*
***************************************************************
*/

#ifndef FINDMOTIFPROCESS_H
#define FINDMOTIFPROCESS_H

#include <QProcess>
#include <QString>

namespace Ui {
    class FindmotifProcess;
}

class FindmotifProcess : public QProcess
{
public:
    FindmotifProcess();
    QString getPeaksFile();
    void setPeaksFile(QString peaks);
    QString getOutputFile();
    void setOutputFile(QString oFile);
    QString getGenomeFile();
    void setGenomeFile(QString gFile);
    QString getMotifName();
    void setMotifName(QString name);
    QString getMotifSequence();
    void setMotifSequence(QString seq);
    int getThreshold();
    void setThreshold(int t);
    bool getFromError();
    void setFromError(bool err);

private:
    QString peaksFile;
    QString outputFile;
    QString genome;
    QString motifName;
    QString motifSequence;
    int     threshold;
    bool    fromError;
};

#endif // FINDMOTIFPROCESS_H
