/********************************************************************************
** Form generated from reading UI file 'csDesktop.ui'
**
** Created by: Qt User Interface Compiler version 4.8.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CSDESKTOP_H
#define UI_CSDESKTOP_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QDoubleSpinBox>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLayout>
#include <QtGui/QLineEdit>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>
#include <QtGui/QSpinBox>
#include <QtGui/QStackedWidget>
#include <QtGui/QStatusBar>
#include <QtGui/QTabWidget>
#include <QtGui/QTextEdit>
#include <QtGui/QToolBar>
#include <QtGui/QToolBox>
#include <QtGui/QToolButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CSDesktop
{
public:
    QAction *pathwaysAnalysisAction;
    QAction *action;
    QAction *aboutAction;
    QAction *comparePeaksMenu;
    QAction *compareGenesMenu;
    QAction *repMaskerAction;
    QAction *cpgIslandsAction;
    QAction *segDupsAction;
    QAction *findMotifMenu;
    QAction *loadDataAction;
    QAction *peakDetectionAction_2;
    QAction *genesSummaryAction;
    QAction *genomicDistributionAction;
    QAction *fireMenu;
    QAction *createTracksAction;
    QAction *actionTutorial;
    QAction *actionAbout;
    QWidget *centralWidget;
    QToolBox *toolBox;
    QWidget *peakDetectionPage;
    QWidget *layoutWidget;
    QVBoxLayout *peakDetectionVerticalLayout;
    QPushButton *loadRawDataButton;
    QPushButton *peakDetectionButton;
    QPushButton *createTracksButton;
    QWidget *genelevelAnnotationpage;
    QWidget *layoutWidget1;
    QVBoxLayout *genelevelAnnotationVerticalLayout;
    QPushButton *genesSummaryButton;
    QPushButton *genomicDistButton;
    QPushButton *rnaGenesButton;
    QWidget *annotationPage;
    QWidget *layoutWidget2;
    QVBoxLayout *annotationVerticalLayout;
    QPushButton *encodeButton;
    QPushButton *repeatsButton;
    QPushButton *cpgIslandsButton;
    QPushButton *segDuplicatesButton;
    QWidget *motifsAnalysisPage;
    QWidget *layoutWidget3;
    QVBoxLayout *motifsAnalysisVerticalLayout;
    QPushButton *findmotifButton;
    QPushButton *fireButton;
    QWidget *pathwaysAnalysisPage;
    QWidget *layoutWidget4;
    QVBoxLayout *pathwaysAnalysisVerticalLayout;
    QPushButton *findPathwayButton;
    QPushButton *pageButton;
    QWidget *consAnalysisPage;
    QWidget *layoutWidget_3;
    QVBoxLayout *consAnalysisVerticalLayout;
    QPushButton *consButton;
    QWidget *comparisonToolsPage;
    QWidget *layoutWidget_4;
    QVBoxLayout *comparisonToolsVerticalLayout;
    QPushButton *comparePeaksButton;
    QPushButton *compareGenesButton;
    QPushButton *jaccardButton;
    QStackedWidget *stackedWidget;
    QWidget *initialStackedPage;
    QVBoxLayout *verticalLayout_11;
    QLabel *CSlogoLabel;
    QLabel *developedByLabel;
    QWidget *loadRawDataStackedPage;
    QVBoxLayout *verticalLayout_6;
    QTabWidget *loadTabWidget;
    QWidget *loadParametersTab;
    QLabel *loadTitleLabel;
    QFrame *loadLine1;
    QWidget *layoutWidget5;
    QGridLayout *loadParametersGridLayout;
    QLabel *selectFormatLabel;
    QComboBox *loadFormatComboBox;
    QPushButton *loadFormatInfoButton;
    QLabel *selectChipRawLabel;
    QToolButton *selectChipRawFolderToolButton;
    QLabel *selectInputRawLabel;
    QToolButton *selectInputRawFolderToolButton;
    QLineEdit *selectChipRawFolderLineEdit;
    QLineEdit *selectInputRawFolderLineEdit;
    QFrame *loadLine2;
    QWidget *layoutWidget6;
    QHBoxLayout *loadButtonsHorizontalLayout;
    QPushButton *loadClearButton;
    QPushButton *splitButton;
    QWidget *loadProgressTab;
    QTextEdit *loadProgressTextEdit;
    QPushButton *stopSplitProcessButton;
    QWidget *peakDetectionStackedPage;
    QVBoxLayout *verticalLayout_7;
    QTabWidget *peakDetectionTabWidget;
    QWidget *peakDetectionParametersTab;
    QLabel *peakDetectionTitleLabel;
    QFrame *peakDetectionLine1;
    QWidget *layoutWidget7;
    QGridLayout *peakDetectionGridLayout;
    QLabel *selectChipFolderLabel;
    QLineEdit *selectChipFolderLineEdit;
    QToolButton *selectChipFolderToolButton;
    QPushButton *selectChipFolderInfoButton;
    QLabel *selectInputFolderLabel;
    QLineEdit *selectInputFolderLineEdit;
    QToolButton *selectInputFolderToolButton;
    QPushButton *selectInputFolderInfoButton;
    QLabel *peakDetectionOutputFileLabel;
    QLineEdit *peakDetectionOutputFileLineEdit;
    QPushButton *peakDetectionOutputFileInfoButton;
    QLabel *peakDetectionFormatLabel;
    QComboBox *peakDetectionFormatComboBox;
    QPushButton *peakDetectionFormatInfoButton;
    QLabel *peakDetectionFoldLabel;
    QDoubleSpinBox *peakDetectionFoldSpinBox;
    QPushButton *peakDetectionFoldInfoButton;
    QLabel *peakDetectionThresholdLabel;
    QSpinBox *peakDetectionThresholdSpinBox;
    QPushButton *peakDetectionThresholdInfoButton;
    QLabel *peakDetectionFragLenLabel;
    QSpinBox *peakDetectionFragLenSpinBox;
    QPushButton *peakDetectionFragLenInfoButton;
    QLabel *peakDetectionMinLenLabel;
    QSpinBox *peakDetectionMinLenSpinBox;
    QPushButton *peakDetectionMinLenInfoButton;
    QLabel *peakDetectionMinDistLabel;
    QSpinBox *peakDetectionMinDistSpinBox;
    QPushButton *peakDetectionMinDistInfoButton;
    QCheckBox *peakDetectionUniqueCheckBox;
    QPushButton *peakDetectionUniqueInfoButton;
    QLabel *peakDetectionMinPeakHeightLabel;
    QSpinBox *peakDetectionMinPeakHeightSpinBox;
    QPushButton *peakDetectionMinPeakHeightInfoButton;
    QLabel *peakDetectionSpeciesLabel;
    QComboBox *peakDetectionSpeciesComboBox;
    QPushButton *peakDetectionSpeciesInfoButton;
    QFrame *peakDetectionLine2;
    QWidget *layoutWidget8;
    QHBoxLayout *peakDetectionButtonsHorizontalLayout;
    QPushButton *runPeakDetectionInverseButton;
    QPushButton *peakDetectionClearButton;
    QPushButton *runCSButton;
    QWidget *peakDetectionProgressTab;
    QTextEdit *peakDetectionProgressTextEdit;
    QPushButton *stopRunCSProcessButton;
    QWidget *summaryStackedPage;
    QVBoxLayout *verticalLayout_4;
    QTabWidget *genesSummaryTabWidget;
    QWidget *genesSummaryParametersTab;
    QLabel *genesSummaryTitleLabel;
    QFrame *genesSummaryLine1;
    QWidget *layoutWidget_2;
    QGridLayout *genesSummaryGridLayout;
    QLabel *genesSummarySelectPeaksLabel;
    QLineEdit *genesSummarySelectPeaksLineEdit;
    QToolButton *genesSummarySelectPeaksToolButton;
    QPushButton *genesSummarySelectPeaksInfoButton;
    QLabel *genesSummaryLenULabel;
    QLabel *genesSummaryLenDLabel;
    QSpinBox *genesSummaryLenDSpinBox;
    QSpinBox *genesSummaryLenUSpinBox;
    QLabel *genesSummaryOutputFileLabel;
    QLineEdit *genesSummaryOutputFileLineEdit;
    QPushButton *genesSummaryOutputFileInfoButton;
    QLabel *genesSummarySuffixLabel;
    QLabel *genesSummarySpeciesLabel;
    QComboBox *genesSummarySpeciesComboBox;
    QComboBox *genesSummaryDatabaseComboBox;
    QLabel *genesSummaryDatabaseLabel;
    QPushButton *genesSummarySpeciesInfoButton;
    QPushButton *genesSummaryDatabaseInfoButton;
    QPushButton *genesSummaryLenUInfoButton;
    QPushButton *genesSummaryLenDInfoButton;
    QFrame *genesSummaryLine2;
    QWidget *layoutWidget9;
    QHBoxLayout *summaryButtonsHorizontalLayout;
    QPushButton *runSummaryClearButton;
    QPushButton *runSummaryButton;
    QWidget *genesSummaryProgressTab;
    QTextEdit *genesSummaryProgressTextEdit;
    QPushButton *stopRunSummaryProcessButton;
    QWidget *genomicDistStackedPage;
    QVBoxLayout *verticalLayout_2;
    QTabWidget *genomicDistTabWidget;
    QWidget *genomicDistParametersTab;
    QLabel *genomicDistTitleLabel;
    QFrame *genomicDistLine1;
    QWidget *layoutWidget_7;
    QGridLayout *genomicDistGridLayout;
    QLabel *genomicDistSelectPeaksLabel;
    QLineEdit *genomicDistSelectPeaksLineEdit;
    QToolButton *genomicDistSelectPeaksToolButton;
    QPushButton *genomicDistSelectPeaksInfoButton;
    QLabel *genomicDistOutputFileLabel;
    QLineEdit *genomicDistOutputFileLineEdit;
    QLabel *genomicDistSuffixLabel;
    QPushButton *genomicDistOutputFileInfoButton;
    QLabel *genomicDistLenPLabel;
    QSpinBox *genomicDistLenPUSpinBox;
    QLabel *genomicDistLenDWLabel;
    QSpinBox *genomicDistLenPDSpinBox;
    QPushButton *genomicDistLenPInfoButton;
    QPushButton *genomicDistLenDWInfoButton;
    QSpinBox *genomicDistLenDWUSpinBox;
    QSpinBox *genomicDistLenDWDSpinBox;
    QPushButton *genomicDistTypeInfoButton;
    QLabel *genomicDistTypeLabel;
    QLabel *genomicDistMindistawayLabel;
    QSpinBox *genomicDistMindistawaySpinBox;
    QPushButton *genomicDistMindistawayInfoButton;
    QSpinBox *genomicDistMaxdistawaySpinBox;
    QRadioButton *peakBasedRadioButton;
    QRadioButton *geneBasedRadioButton;
    QLabel *genomicDistSpeciesLabel;
    QComboBox *genomicDistSpeciesComboBox;
    QPushButton *genomicDistSpeciesInfoButton;
    QLabel *genomicDistDatabaseLabel;
    QComboBox *genomicDistDatabaseComboBox;
    QPushButton *genomicDistDatabaseInfoButton;
    QFrame *genomicDistLine2;
    QWidget *layoutWidget_8;
    QHBoxLayout *genomicDistButtonsHorizontalLayout;
    QPushButton *runGenomicDistClearButton;
    QPushButton *runGenomicDistButton;
    QWidget *genomicDistProgressTab;
    QTextEdit *genomicDistProgressTextEdit;
    QPushButton *stopRunGenomicDistProcessButton;
    QWidget *rnaGenesStackedPage;
    QVBoxLayout *verticalLayout_17;
    QTabWidget *rnaGenesTabWidget;
    QWidget *rnaGenesParametersTab;
    QLabel *rnaGenesTitleLabel;
    QFrame *rnaGenesLine1;
    QWidget *layoutWidget_19;
    QGridLayout *rnaGenesGridLayout;
    QLabel *rnaGenesSelectPeaksLabel;
    QLineEdit *rnaGenesSelectPeaksLineEdit;
    QToolButton *rnaGenesSelectPeaksToolButton;
    QPushButton *rnaGenesSelectPeaksInfoButton;
    QLabel *rnaGenesOutputFileLabel;
    QLineEdit *rnaGenesOutputFileLineEdit;
    QLabel *rnaGenesSuffixLabel;
    QPushButton *rnaGenesOutputFileInfoButton;
    QFrame *rnaGenesLine2;
    QWidget *layoutWidget_20;
    QHBoxLayout *rnaGenesButtonsHorizontalLayout;
    QPushButton *runFindRNAGenesClearButton;
    QPushButton *runFindRNAGenesButton;
    QWidget *rnaGenesProgressTab;
    QTextEdit *rnaGenesProgressTextEdit;
    QPushButton *stopRunFindRNAGenesProcessButton;
    QWidget *encodeStackedPage;
    QVBoxLayout *verticalLayout_16;
    QTabWidget *encodeTabWidget;
    QWidget *encodeParametersTab;
    QFrame *encodeLine1;
    QWidget *layoutWidget_35;
    QGridLayout *encodeGridLayout;
    QComboBox *encodeSpeciesComboBox;
    QLabel *encodeSelectPeaksLabel;
    QLineEdit *encodeSelectPeaksLineEdit;
    QPushButton *encodeSelectPeaksInfoButton;
    QLabel *encodePrefixFileLabel;
    QLineEdit *encodePrefixFileLineEdit;
    QPushButton *encodePrefixFileInfoButton;
    QRadioButton *encodeSelectDatasetRadioButton;
    QPushButton *encodeSelectDatasetInfoButton;
    QRadioButton *encodeWriteDatasetRadioButton;
    QLineEdit *encodeWriteDatasetLineEdit;
    QPushButton *encodeWriteDatasetInfoButton;
    QComboBox *encodeSelectDatasetComboBox;
    QSpinBox *encodeIterationsSpinBox;
    QToolButton *encodeSelectPeaksToolButton;
    QLabel *encodeIterationsLabel;
    QPushButton *encodeIterationsInfoButton;
    QCheckBox *encodeALLCheckBox;
    QPushButton *encodeALLInfoButton;
    QLabel *encodeSpeciesLabel;
    QPushButton *encodeSpeciesInfoButton;
    QFrame *encodeLine2;
    QWidget *layoutWidget_36;
    QHBoxLayout *encodeButtonsHorizontalLayout;
    QPushButton *runEncodeClearButton;
    QPushButton *runEncodeButton;
    QLabel *encodeTitleLabel;
    QWidget *encodeProgressTab;
    QTextEdit *encodeProgressTextEdit;
    QPushButton *stopRunEncodeProcessButton;
    QWidget *createTracksStackedPage;
    QVBoxLayout *verticalLayout;
    QTabWidget *createTracksTabWidget;
    QWidget *createTracksParametersTab;
    QLabel *createTracksTitleLabel1;
    QFrame *createTracksLine1;
    QWidget *layoutWidget_11;
    QGridLayout *peaksTrackGridLayout1;
    QLabel *peaksTrackSelectPeaksLabel;
    QLineEdit *peaksTrackSelectPeaksLineEdit;
    QToolButton *peaksTrackSelectPeaksToolButton;
    QPushButton *peaksTrackSelectPeaksInfoButton;
    QLabel *peaksTrackNameLabel;
    QPushButton *peaksTrackNameInfoButton;
    QLineEdit *peaksTrackNameLineEdit;
    QLabel *peaksTrackOutputFileLabel;
    QLineEdit *peaksTrackOutputFileLineEdit;
    QLabel *peaksTrackSuffixLabel;
    QPushButton *peaksTrackOutputFileInfoButton;
    QFrame *createTracksLine2;
    QWidget *layoutWidget_12;
    QHBoxLayout *createTracksHorizontalLayout1;
    QPushButton *peaksTrackClearButton;
    QPushButton *createPeaksTrackButton;
    QFrame *createTracksLine3;
    QWidget *layoutWidget_13;
    QHBoxLayout *createTracksHorizontalLayout2;
    QPushButton *readsTrackClearButton;
    QPushButton *createReadsTrackButton;
    QLabel *createTracksTitleLabel2;
    QWidget *layoutWidget_14;
    QGridLayout *peaksTrackGridLayout2;
    QLabel *readsTrackSelectReadsLabel;
    QLineEdit *readsTrackSelectReadsLineEdit;
    QToolButton *readsTrackSelectReadsToolButton;
    QPushButton *readsTrackSelectReadsInfoButton;
    QLabel *readsTrackNameLabel;
    QPushButton *readsTrackNameInfoButton;
    QLineEdit *readsTrackNameLineEdit;
    QPushButton *readsTrackUniqueInfoButton;
    QCheckBox *readsTrackUniqueCheckBox;
    QLabel *readsTrackOutputFileLabel;
    QPushButton *readsTrackOutputFileInfoButton;
    QLineEdit *readsTrackOutputFileLineEdit;
    QLabel *readsTrackSuffixLabel;
    QCheckBox *readsTrackBigWigCheckBox;
    QPushButton *readsTrackBigWigInfoButton;
    QFrame *createTracksLine4;
    QWidget *createTracksProgressTab;
    QTextEdit *createTracksProgressTextEdit;
    QWidget *layoutWidget10;
    QHBoxLayout *horizontalLayout;
    QPushButton *openGenomeBrowserButton;
    QPushButton *stopCreateTracksProcessButton;
    QWidget *repeatsStackedPage;
    QVBoxLayout *verticalLayout_5;
    QTabWidget *repeatsTabWidget;
    QWidget *repeatsParametersTab;
    QLabel *repeatsTitleLabel;
    QFrame *repeatsLine1;
    QWidget *layoutWidget_9;
    QGridLayout *repeatsGridLayout;
    QComboBox *repeatsSpeciesComboBox;
    QPushButton *repeatsSpeciesInfoButton;
    QLabel *repeatsSelectPeaksLabel;
    QLineEdit *repeatsSelectPeaksLineEdit;
    QToolButton *repeatsSelectPeaksToolButton;
    QPushButton *repeatsSelectPeaksInfoButton;
    QLabel *repeatsOutputFileLabel;
    QLineEdit *repeatsOutputFileLineEdit;
    QLabel *repeatsSuffixLabel;
    QPushButton *repeatsOutputFileInfoButton;
    QLabel *repeatsSpeciesLabel;
    QFrame *repeatsLine2;
    QWidget *layoutWidget_10;
    QHBoxLayout *repeatsButtonsHorizontalLayout;
    QPushButton *runFindRepeatsClearButton;
    QPushButton *runFindRepeatsButton;
    QWidget *repeatsProgressTab;
    QTextEdit *repeatsProgressTextEdit;
    QPushButton *stopRunFindRepeatsProcessButton;
    QWidget *cpgIslandsStackedPage;
    QVBoxLayout *verticalLayout_8;
    QTabWidget *cpgIslandsTabWidget;
    QWidget *cpgIslandsParametersTab;
    QLabel *cpgIslandsTitleLabel;
    QFrame *cpgIslandsLine1;
    QWidget *layoutWidget_15;
    QGridLayout *cpgIslandsGridLayout;
    QComboBox *cpgIslandsSpeciesComboBox;
    QPushButton *cpgIslandsSpeciesInfoButton;
    QLabel *cpgIslandsSpeciesLabel;
    QLabel *cpgIslandsSelectPeaksLabel;
    QLineEdit *cpgIslandsSelectPeaksLineEdit;
    QToolButton *cpgIslandsSelectPeaksToolButton;
    QPushButton *cpgIslandsSelectPeaksInfoButton;
    QLabel *cpgIslandsOutputFileLabel;
    QLineEdit *cpgIslandsOutputFileLineEdit;
    QLabel *cpgIslandsSuffixLabel;
    QPushButton *cpgIslandsOutputFileInfoButton;
    QFrame *cpgIslandsLine2;
    QWidget *layoutWidget_16;
    QHBoxLayout *cpgIslandsButtonsHorizontalLayout;
    QPushButton *runFindCpgIslandsClearButton;
    QPushButton *runFindCpgIslandsButton;
    QWidget *cpgIslandsProgressTab;
    QTextEdit *cpgIslandsProgressTextEdit;
    QPushButton *stopRunFindCpgIslandsProcessButton;
    QWidget *segDuplicatesStackedPage;
    QVBoxLayout *verticalLayout_9;
    QTabWidget *segDuplicatesTabWidget;
    QWidget *segDuplicatesParametersTab;
    QLabel *segDuplicatesTitleLabel;
    QFrame *segDuplicatesLine1;
    QWidget *layoutWidget_17;
    QGridLayout *segDuplicatesGridLayout;
    QLabel *segDuplicatesSelectPeaksLabel;
    QLineEdit *segDuplicatesSelectPeaksLineEdit;
    QToolButton *segDuplicatesSelectPeaksToolButton;
    QPushButton *segDuplicatesSelectPeaksInfoButton;
    QLabel *segDuplicatesOutputFileLabel;
    QLineEdit *segDuplicatesOutputFileLineEdit;
    QLabel *segDuplicatesSuffixLabel;
    QPushButton *segDuplicatesOutputFileInfoButton;
    QComboBox *segDuplicatesSpeciesComboBox;
    QPushButton *segDuplicatesSpeciesInfoButton;
    QLabel *segDuplicatesSpeciesLabel;
    QFrame *segDuplicatesLine2;
    QWidget *layoutWidget_18;
    QHBoxLayout *segDuplicatesButtonsHorizontalLayout;
    QPushButton *runFindSegDuplicatesClearButton;
    QPushButton *runFindSegDuplicatesButton;
    QWidget *segDuplicatesProgressTab;
    QTextEdit *segDuplicatesProgressTextEdit;
    QPushButton *stopRunFindSegDuplicatesProcessButton;
    QWidget *findmotifStackedPage;
    QVBoxLayout *verticalLayout_10;
    QTabWidget *findmotifTabWidget;
    QWidget *findmotifParametersTab;
    QLabel *findmotifTitleLabel;
    QFrame *findmotifLine1;
    QWidget *layoutWidget_21;
    QGridLayout *findmotifGridLayout;
    QLabel *findmotifSelectPeaksLabel;
    QLineEdit *findmotifSelectPeaksLineEdit;
    QToolButton *findmotifSelectPeaksToolButton;
    QPushButton *findmotifSelectPeaksInfoButton;
    QLabel *findmotifScoreLabel;
    QDoubleSpinBox *findmotifScoreSpinBox;
    QLabel *findmotifOutputFileLabel;
    QLineEdit *findmotifOutputFileLineEdit;
    QLabel *findmotifSuffixLabel;
    QPushButton *findmotifOutputFileInfoButton;
    QPushButton *findmotifScoreInfoButton;
    QLineEdit *findmotifGenomeLineEdit;
    QLabel *findmotifGenomeLabel;
    QPushButton *findmotifGenomeInfoButton;
    QToolButton *findmotifGenomeToolButton;
    QRadioButton *findmotifNameRadioButton;
    QPushButton *findmotifNameInfoButton;
    QRadioButton *findmotifJasparRadioButton;
    QComboBox *findmotifJasparMotifComboBox;
    QRadioButton *findmotifBulykRadioButton;
    QComboBox *findmotifBulykMotifComboBox;
    QRadioButton *findmotifSequenceRadioButton;
    QLineEdit *findmotifSequenceLineEdit;
    QPushButton *findmotifSequenceInfoButton;
    QFrame *findmotifLine2;
    QWidget *layoutWidget_22;
    QHBoxLayout *findmotifButtonsHorizontalLayout;
    QPushButton *runFindmotifClearButton;
    QPushButton *runFindmotifButton;
    QWidget *findmotifProgressTab;
    QTextEdit *findmotifProgressTextEdit;
    QPushButton *stopRunFindmotifProcessButton;
    QWidget *fireStackedPage;
    QVBoxLayout *verticalLayout_14;
    QTabWidget *fireTabWidget;
    QWidget *fireParametersTab;
    QLabel *fireTitleLabel;
    QFrame *fireLine1;
    QWidget *layoutWidget_29;
    QGridLayout *fireGridLayout;
    QLabel *fireSelectPeaksLabel;
    QLineEdit *fireSelectPeaksLineEdit;
    QToolButton *fireSelectPeaksToolButton;
    QPushButton *fireSelectPeaksInfoButton;
    QLineEdit *fireGenomeLineEdit;
    QLabel *fireGenomeLabel;
    QPushButton *fireGenomeInfoButton;
    QToolButton *fireGenomeToolButton;
    QLabel *fireSpeciesLabel;
    QComboBox *fireSpeciesComboBox;
    QPushButton *fireSpeciesInfoButton;
    QPushButton *fireSelectPeaksFolderInfoButton;
    QToolButton *fireSelectPeaksFolderToolButton;
    QLabel *fireSelectPeaksFolderLabel;
    QLineEdit *fireSelectPeaksFolderLineEdit;
    QLabel *fireRandomeModeLabel;
    QComboBox *fireRandomModeComboBox;
    QPushButton *fireRandmodeInfoButton;
    QCheckBox *fireSeedCheckBox;
    QPushButton *fireSeedInfoButton;
    QFrame *fireLine2;
    QWidget *layoutWidget_30;
    QHBoxLayout *fireButtonsHorizontalLayout;
    QPushButton *runFIREClearButton;
    QPushButton *runFIREButton;
    QWidget *fireProgressTab;
    QTextEdit *fireProgressTextEdit;
    QPushButton *stopRunFIREProcessButton;
    QWidget *findPathwayStackedPage;
    QVBoxLayout *verticalLayout_3;
    QTabWidget *findPathwayTabWidget;
    QWidget *findPathwayParametersTab;
    QLabel *findPathwayTitleLabel;
    QFrame *findPathwayLine1;
    QWidget *layoutWidget_31;
    QGridLayout *findPathwayGridLayout;
    QLabel *findPathwaySelectPeaksLabel;
    QLineEdit *findPathwaySelectPeaksLineEdit;
    QPushButton *findPathwaySelectPeaksInfoButton;
    QLabel *findPathwayOutputFileLabel;
    QLineEdit *findPathwayOutputFileLineEdit;
    QPushButton *findPathwayOutputFileInfoButton;
    QLineEdit *findPathwayWritePathwayLineEdit;
    QPushButton *findPathwayWritePathwayInfoButton;
    QLabel *findPathwaySelectDatabaseLabel;
    QPushButton *findPathwaySelectDatabaseInfoButton;
    QComboBox *findPathwaySelectDatabaseComboBox;
    QLabel *findPathwaySpeciesLabel;
    QComboBox *findPathwaySpeciesComboBox;
    QPushButton *findPathwaySpeciesInfoButton;
    QComboBox *findPathwaySelectPathwayComboBox;
    QPushButton *findPathwaySelectPathwayInfoButton;
    QRadioButton *findPathwayWritePathwayRadioButton;
    QRadioButton *findPathwaySelectPathwayRadioButton;
    QCheckBox *findPathwaysPromotersCheckBox;
    QLabel *findPathwaySelectGenepartsLabel;
    QPushButton *findPathwaysSelectGenepartsInfoButton;
    QToolButton *findPathwaySelectPeaksToolButton;
    QCheckBox *findPathwaysIntronsCheckBox;
    QCheckBox *findPathwaysIntron1CheckBox;
    QCheckBox *findPathwaysIntron2CheckBox;
    QLabel *findPathwaySuffixLabel;
    QLabel *findPathwaysDatabaseLabel;
    QComboBox *findPathwaysDatabaseComboBox;
    QPushButton *findPathwaysDatabaseInfoButton;
    QCheckBox *findPathwaysExonsCheckBox;
    QCheckBox *findPathwaysIntergenicCheckBox;
    QCheckBox *findPathwaysDownstreamCheckBox;
    QCheckBox *findPathwaysDistalCheckBox;
    QFrame *findPathwayLine2;
    QWidget *layoutWidget_32;
    QHBoxLayout *findPathwayButtonsHorizontalLayout;
    QPushButton *runFindPathwayClearButton;
    QPushButton *runFindPathwayButton;
    QWidget *findPathwayProgressTab;
    QTextEdit *findPathwayProgressTextEdit;
    QPushButton *stopRunFindPathwayProcessButton;
    QWidget *pageStackedPage;
    QVBoxLayout *verticalLayout_15;
    QTabWidget *pageTabWidget;
    QWidget *pageParametersTab;
    QLabel *pageTitleLabel;
    QFrame *pageLine1;
    QWidget *layoutWidget_33;
    QGridLayout *pageGridLayout;
    QLabel *pageSelectPeaksLabel;
    QLineEdit *pageSelectPeaksLineEdit;
    QPushButton *pageSelectPeaksInfoButton;
    QLabel *pageOutputFileLabel;
    QLineEdit *pageOutputFileLineEdit;
    QPushButton *pageOutputFileInfoButton;
    QLabel *pageSpeciesLabel;
    QComboBox *pageSpeciesComboBox;
    QLabel *pageDatabaseLabel;
    QComboBox *pageGenesDBComboBox;
    QLabel *pageSelectDatabaseLabel;
    QComboBox *pagePathwaysDBComboBox;
    QLabel *pageSelectGenepartsLabel;
    QPushButton *pageSelectGenepartsInfoButton;
    QPushButton *pageSelectDatabaseInfoButton;
    QPushButton *pageSpeciesInfoButton;
    QPushButton *pageDatabaseInfoButton;
    QCheckBox *pagePromotersCheckBox;
    QLabel *pageSuffixLabel;
    QToolButton *pageSelectPeaksToolButton;
    QCheckBox *pageIntronsCheckBox;
    QCheckBox *pageIntron1CheckBox;
    QCheckBox *pageIntron2CheckBox;
    QCheckBox *pageExonsCheckBox;
    QCheckBox *pageIntergenicCheckBox;
    QCheckBox *pageDownstreamCheckBox;
    QCheckBox *pageDistalCheckBox;
    QFrame *pageLine2;
    QWidget *layoutWidget_34;
    QHBoxLayout *pageButtonsHorizontalLayout;
    QPushButton *runPAGEClearButton;
    QPushButton *runPAGEButton;
    QWidget *pageProgressTab;
    QTextEdit *pageProgressTextEdit;
    QPushButton *stopRunPAGEProcessButton;
    QWidget *consStackedPage;
    QVBoxLayout *verticalLayout_20;
    QTabWidget *consTabWidget;
    QWidget *consParametersTab;
    QLabel *consTitleLabel;
    QFrame *consLine1;
    QWidget *layoutWidget_39;
    QGridLayout *consGridLayout;
    QLabel *consSelectPeaksLabel;
    QLineEdit *consSelectPeaksLineEdit;
    QToolButton *consSelectPeaksToolButton;
    QPushButton *consSelectPeaksInfoButton;
    QLabel *consOutputFileLabel;
    QLineEdit *consOutputFileLineEdit;
    QPushButton *consOutputFileInfoButton;
    QLabel *consMethodLabel;
    QComboBox *consMethodComboBox;
    QPushButton *consMethodInfoButton;
    QPushButton *consCategoryInfoButton;
    QLabel *consConsDirLabel;
    QLineEdit *consConsDirLineEdit;
    QToolButton *consConsDirToolButton;
    QPushButton *consConsDirInfoButton;
    QLabel *consFormatLabel;
    QComboBox *consFormatComboBox;
    QPushButton *consFormatInfoButton;
    QLabel *consOutputRandomLabel;
    QLineEdit *consOutputRandomLineEdit;
    QPushButton *consOutputRandomInfoButton;
    QCheckBox *consShowProfilesCheckBox;
    QPushButton *consShowProfilesInfoButton;
    QLabel *consRandistLabel;
    QSpinBox *consRandistSpinBox;
    QPushButton *consRandistInfoButton;
    QPushButton *consWindowSizeInfoButton;
    QLabel *consWindowSizeLabel;
    QSpinBox *consWindowSizeSpinBox;
    QCheckBox *consAroundSummitCheckBox;
    QSpinBox *consDistanceSpinBox;
    QLabel *consDistanceLabel;
    QPushButton *consThresholdInfoButton;
    QDoubleSpinBox *consThresholdSpinBox;
    QLabel *consThresholdLabel;
    QComboBox *consSpeciesComboBox;
    QLabel *consSpeciesLabel;
    QPushButton *consSpeciesInfoButton;
    QPushButton *consMakeRandInfoButton;
    QCheckBox *consMakeRandCheckBox;
    QComboBox *consCategoryComboBox;
    QLabel *consCategoryLabel;
    QFrame *consLine2;
    QWidget *layoutWidget_40;
    QHBoxLayout *consButtonsHorizontalLayout;
    QPushButton *runConsClearButton;
    QPushButton *runConsButton;
    QWidget *consProgressTab;
    QTextEdit *consProgressTextEdit;
    QPushButton *stopRunConsProcessButton;
    QWidget *compPeaksStackedPage;
    QVBoxLayout *verticalLayout_12;
    QTabWidget *compPeaksTabWidget;
    QWidget *compPeaksParametersTab;
    QLabel *compPeaksTitleLabel;
    QFrame *compPeaksLine1;
    QWidget *layoutWidget_23;
    QGridLayout *compPeaksGridLayout;
    QLabel *compPeaksSelectPeaks1Label;
    QLineEdit *compPeaksSelectPeaks1LineEdit;
    QPushButton *compPeaksSelectPeaks1InfoButton;
    QLabel *compPeaksSelectPeaks2Label;
    QLineEdit *compPeaksSelectPeaks2LineEdit;
    QLabel *compPeaksPrefixFileLabel;
    QLineEdit *compPeaksPrefixFileLineEdit;
    QCheckBox *compPeaksCenter1CheckBox;
    QCheckBox *compPeaksHeader1CheckBox;
    QCheckBox *compPeaksID1CheckBox;
    QToolButton *compPeaksSelectPeaks1ToolButton;
    QToolButton *compPeaksSelectPeaks2ToolButton;
    QCheckBox *compPeaksCenter2CheckBox;
    QCheckBox *compPeaksHeader2CheckBox;
    QCheckBox *compPeaksID2CheckBox;
    QPushButton *compPeaksColumnHeaderCenter1InfoButton;
    QPushButton *compPeaksSelectPeaks2InfoButton;
    QPushButton *compPeaksColumnHeaderCenter2InfoButton;
    QPushButton *compPeaksPrefixInfoButton;
    QCheckBox *compPeaksOvPeaksCheckBox;
    QCheckBox *compPeaksUnPeaksCheckBox;
    QCheckBox *compPeaksDescPeaks2CheckBox;
    QRadioButton *compPeaksANDRadioButton;
    QRadioButton *compPeaksANDNOTRadioButton;
    QLabel *compPeaksANDLabel;
    QLabel *compPeaksANDNOTLabel;
    QCheckBox *compPeaksDescPeaks1CheckBox;
    QLabel *compPeaksIterationsLabel;
    QSpinBox *compPeaksIterationsSpinBox;
    QPushButton *compPeaksDesc1InfoButton;
    QPushButton *compPeaksDesc2InfoButton;
    QPushButton *compPeaksUnInfoButton;
    QPushButton *compPeaksOvInfoButton;
    QPushButton *compPeaksANDNOTInfoButton;
    QPushButton *compPeaksANDInfoButton;
    QPushButton *compPeaksIterationsInfoButton;
    QPushButton *compPeaksRandomModeInfoButton;
    QLabel *compPeaksRandomModeLabel;
    QComboBox *compPeaksRandomModeComboBox;
    QFrame *compPeaksLine2;
    QWidget *layoutWidget_24;
    QHBoxLayout *compPeaksButtonsHorizontalLayout;
    QPushButton *runCompPeaksInverseButton;
    QPushButton *runCompPeaksClearButton;
    QPushButton *runCompPeaksButton;
    QWidget *compPeaksProgressTab;
    QTextEdit *compPeaksProgressTextEdit;
    QPushButton *stopRunCompPeaksProcessButton;
    QWidget *compGenesStackedPage;
    QVBoxLayout *verticalLayout_13;
    QTabWidget *compGenesTabWidget;
    QWidget *compGenesParametersTab;
    QLabel *compGenesTitleLabel;
    QFrame *compGenesLine1;
    QWidget *layoutWidget_25;
    QGridLayout *compGenesGridLayout;
    QLabel *compGenesSelectFile1Label;
    QLineEdit *compGenesSelectFile1LineEdit;
    QToolButton *compGenesSelectFile1ToolButton;
    QPushButton *compGenesSelectFile1InfoButton;
    QPushButton *compGenesSelectFile2InfoButton;
    QPushButton *compGenesRemoveDuplicatesInfoButton;
    QPushButton *compGenesOutputFileInfoButton;
    QLabel *compGenesSelectFile2Label;
    QLineEdit *compGenesSelectFile2LineEdit;
    QToolButton *compGenesSelectFile2ToolButton;
    QLabel *compGenesOutputFileLabel;
    QLineEdit *compGenesOutputFileLineEdit;
    QCheckBox *compGenesRemoveDuplicatesCheckBox;
    QFrame *compGenesLine2;
    QWidget *layoutWidget_26;
    QHBoxLayout *compGenesButtonsHorizontalLayout;
    QPushButton *runCompGenesClearButton;
    QPushButton *runCompGenesButton;
    QWidget *compGenesProgressTab;
    QTextEdit *compGenesProgressTextEdit;
    QPushButton *stopRunCompGenesProcessButton;
    QWidget *jaccardStackedPage;
    QVBoxLayout *verticalLayout_18;
    QTabWidget *jaccardTabWidget;
    QWidget *jaccardParametersTab;
    QLabel *jaccardTitleLabel;
    QFrame *jaccardLine1;
    QWidget *layoutWidget_27;
    QGridLayout *jaccardGridLayout;
    QLabel *jaccardSelectFileLabel;
    QLineEdit *jaccardSelectFileLineEdit;
    QToolButton *jaccardSelectFileToolButton;
    QPushButton *jaccardSelectFileInfoButton;
    QPushButton *jaccardOutputFileInfoButton;
    QLabel *jaccardOutputFileLabel;
    QLineEdit *jaccardOutputFileLineEdit;
    QFrame *jaccardLine2;
    QWidget *layoutWidget_28;
    QHBoxLayout *jaccardButtonsHorizontalLayout;
    QPushButton *runJaccardClearButton;
    QPushButton *runJaccardButton;
    QWidget *jaccardProgressTab;
    QTextEdit *jaccardProgressTextEdit;
    QPushButton *stopRunJaccardProcessButton;
    QMenuBar *menuBar;
    QMenu *menuTools;
    QMenu *menuHelp;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;
    QButtonGroup *buttonGroup_2;
    QButtonGroup *buttonGroup_3;
    QButtonGroup *buttonGroup;

    void setupUi(QMainWindow *CSDesktop)
    {
        if (CSDesktop->objectName().isEmpty())
            CSDesktop->setObjectName(QString::fromUtf8("CSDesktop"));
        CSDesktop->resize(862, 679);
        CSDesktop->setAcceptDrops(false);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/Icons/CS_logo_64.png"), QSize(), QIcon::Normal, QIcon::Off);
        CSDesktop->setWindowIcon(icon);
        CSDesktop->setUnifiedTitleAndToolBarOnMac(false);
        pathwaysAnalysisAction = new QAction(CSDesktop);
        pathwaysAnalysisAction->setObjectName(QString::fromUtf8("pathwaysAnalysisAction"));
        pathwaysAnalysisAction->setEnabled(false);
        action = new QAction(CSDesktop);
        action->setObjectName(QString::fromUtf8("action"));
        action->setEnabled(false);
        aboutAction = new QAction(CSDesktop);
        aboutAction->setObjectName(QString::fromUtf8("aboutAction"));
        comparePeaksMenu = new QAction(CSDesktop);
        comparePeaksMenu->setObjectName(QString::fromUtf8("comparePeaksMenu"));
        compareGenesMenu = new QAction(CSDesktop);
        compareGenesMenu->setObjectName(QString::fromUtf8("compareGenesMenu"));
        repMaskerAction = new QAction(CSDesktop);
        repMaskerAction->setObjectName(QString::fromUtf8("repMaskerAction"));
        cpgIslandsAction = new QAction(CSDesktop);
        cpgIslandsAction->setObjectName(QString::fromUtf8("cpgIslandsAction"));
        segDupsAction = new QAction(CSDesktop);
        segDupsAction->setObjectName(QString::fromUtf8("segDupsAction"));
        findMotifMenu = new QAction(CSDesktop);
        findMotifMenu->setObjectName(QString::fromUtf8("findMotifMenu"));
        loadDataAction = new QAction(CSDesktop);
        loadDataAction->setObjectName(QString::fromUtf8("loadDataAction"));
        peakDetectionAction_2 = new QAction(CSDesktop);
        peakDetectionAction_2->setObjectName(QString::fromUtf8("peakDetectionAction_2"));
        genesSummaryAction = new QAction(CSDesktop);
        genesSummaryAction->setObjectName(QString::fromUtf8("genesSummaryAction"));
        genomicDistributionAction = new QAction(CSDesktop);
        genomicDistributionAction->setObjectName(QString::fromUtf8("genomicDistributionAction"));
        fireMenu = new QAction(CSDesktop);
        fireMenu->setObjectName(QString::fromUtf8("fireMenu"));
        fireMenu->setEnabled(false);
        createTracksAction = new QAction(CSDesktop);
        createTracksAction->setObjectName(QString::fromUtf8("createTracksAction"));
        actionTutorial = new QAction(CSDesktop);
        actionTutorial->setObjectName(QString::fromUtf8("actionTutorial"));
        actionAbout = new QAction(CSDesktop);
        actionAbout->setObjectName(QString::fromUtf8("actionAbout"));
        centralWidget = new QWidget(CSDesktop);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        toolBox = new QToolBox(centralWidget);
        toolBox->setObjectName(QString::fromUtf8("toolBox"));
        toolBox->setEnabled(true);
        toolBox->setGeometry(QRect(0, 0, 191, 621));
        toolBox->setLayoutDirection(Qt::LeftToRight);
        toolBox->setFrameShape(QFrame::StyledPanel);
        toolBox->setFrameShadow(QFrame::Sunken);
        toolBox->setLineWidth(1);
        toolBox->setMidLineWidth(0);
        peakDetectionPage = new QWidget();
        peakDetectionPage->setObjectName(QString::fromUtf8("peakDetectionPage"));
        peakDetectionPage->setGeometry(QRect(0, 0, 189, 423));
        layoutWidget = new QWidget(peakDetectionPage);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(0, 0, 181, 131));
        peakDetectionVerticalLayout = new QVBoxLayout(layoutWidget);
        peakDetectionVerticalLayout->setSpacing(0);
        peakDetectionVerticalLayout->setContentsMargins(11, 11, 11, 11);
        peakDetectionVerticalLayout->setObjectName(QString::fromUtf8("peakDetectionVerticalLayout"));
        peakDetectionVerticalLayout->setContentsMargins(0, 0, 0, 0);
        loadRawDataButton = new QPushButton(layoutWidget);
        loadRawDataButton->setObjectName(QString::fromUtf8("loadRawDataButton"));
        QFont font;
        font.setBold(false);
        font.setItalic(false);
        font.setUnderline(false);
        font.setWeight(50);
        font.setStrikeOut(false);
        loadRawDataButton->setFont(font);
        loadRawDataButton->setDefault(false);

        peakDetectionVerticalLayout->addWidget(loadRawDataButton);

        peakDetectionButton = new QPushButton(layoutWidget);
        peakDetectionButton->setObjectName(QString::fromUtf8("peakDetectionButton"));
        peakDetectionButton->setDefault(false);

        peakDetectionVerticalLayout->addWidget(peakDetectionButton);

        createTracksButton = new QPushButton(layoutWidget);
        createTracksButton->setObjectName(QString::fromUtf8("createTracksButton"));

        peakDetectionVerticalLayout->addWidget(createTracksButton);

        toolBox->addItem(peakDetectionPage, QString::fromUtf8("Peak Detection"));
        genelevelAnnotationpage = new QWidget();
        genelevelAnnotationpage->setObjectName(QString::fromUtf8("genelevelAnnotationpage"));
        genelevelAnnotationpage->setGeometry(QRect(0, 0, 189, 423));
        layoutWidget1 = new QWidget(genelevelAnnotationpage);
        layoutWidget1->setObjectName(QString::fromUtf8("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(0, 5, 182, 111));
        genelevelAnnotationVerticalLayout = new QVBoxLayout(layoutWidget1);
        genelevelAnnotationVerticalLayout->setSpacing(6);
        genelevelAnnotationVerticalLayout->setContentsMargins(11, 11, 11, 11);
        genelevelAnnotationVerticalLayout->setObjectName(QString::fromUtf8("genelevelAnnotationVerticalLayout"));
        genelevelAnnotationVerticalLayout->setContentsMargins(0, 0, 0, 0);
        genesSummaryButton = new QPushButton(layoutWidget1);
        genesSummaryButton->setObjectName(QString::fromUtf8("genesSummaryButton"));

        genelevelAnnotationVerticalLayout->addWidget(genesSummaryButton);

        genomicDistButton = new QPushButton(layoutWidget1);
        genomicDistButton->setObjectName(QString::fromUtf8("genomicDistButton"));

        genelevelAnnotationVerticalLayout->addWidget(genomicDistButton);

        rnaGenesButton = new QPushButton(layoutWidget1);
        rnaGenesButton->setObjectName(QString::fromUtf8("rnaGenesButton"));
        rnaGenesButton->setEnabled(true);

        genelevelAnnotationVerticalLayout->addWidget(rnaGenesButton);

        toolBox->addItem(genelevelAnnotationpage, QString::fromUtf8("Gene-level annotation"));
        annotationPage = new QWidget();
        annotationPage->setObjectName(QString::fromUtf8("annotationPage"));
        annotationPage->setGeometry(QRect(0, 0, 189, 423));
        layoutWidget2 = new QWidget(annotationPage);
        layoutWidget2->setObjectName(QString::fromUtf8("layoutWidget2"));
        layoutWidget2->setGeometry(QRect(0, 0, 182, 151));
        annotationVerticalLayout = new QVBoxLayout(layoutWidget2);
        annotationVerticalLayout->setSpacing(6);
        annotationVerticalLayout->setContentsMargins(11, 11, 11, 11);
        annotationVerticalLayout->setObjectName(QString::fromUtf8("annotationVerticalLayout"));
        annotationVerticalLayout->setContentsMargins(0, 0, 0, 0);
        encodeButton = new QPushButton(layoutWidget2);
        encodeButton->setObjectName(QString::fromUtf8("encodeButton"));
        encodeButton->setEnabled(true);

        annotationVerticalLayout->addWidget(encodeButton);

        repeatsButton = new QPushButton(layoutWidget2);
        repeatsButton->setObjectName(QString::fromUtf8("repeatsButton"));
        repeatsButton->setEnabled(true);

        annotationVerticalLayout->addWidget(repeatsButton);

        cpgIslandsButton = new QPushButton(layoutWidget2);
        cpgIslandsButton->setObjectName(QString::fromUtf8("cpgIslandsButton"));
        cpgIslandsButton->setEnabled(true);

        annotationVerticalLayout->addWidget(cpgIslandsButton);

        segDuplicatesButton = new QPushButton(layoutWidget2);
        segDuplicatesButton->setObjectName(QString::fromUtf8("segDuplicatesButton"));
        segDuplicatesButton->setEnabled(true);

        annotationVerticalLayout->addWidget(segDuplicatesButton);

        toolBox->addItem(annotationPage, QString::fromUtf8("Non-genic annotation"));
        motifsAnalysisPage = new QWidget();
        motifsAnalysisPage->setObjectName(QString::fromUtf8("motifsAnalysisPage"));
        motifsAnalysisPage->setGeometry(QRect(0, 0, 189, 423));
        layoutWidget3 = new QWidget(motifsAnalysisPage);
        layoutWidget3->setObjectName(QString::fromUtf8("layoutWidget3"));
        layoutWidget3->setGeometry(QRect(0, 0, 181, 91));
        motifsAnalysisVerticalLayout = new QVBoxLayout(layoutWidget3);
        motifsAnalysisVerticalLayout->setSpacing(0);
        motifsAnalysisVerticalLayout->setContentsMargins(11, 11, 11, 11);
        motifsAnalysisVerticalLayout->setObjectName(QString::fromUtf8("motifsAnalysisVerticalLayout"));
        motifsAnalysisVerticalLayout->setContentsMargins(0, 0, 0, 0);
        findmotifButton = new QPushButton(layoutWidget3);
        findmotifButton->setObjectName(QString::fromUtf8("findmotifButton"));
        findmotifButton->setEnabled(true);

        motifsAnalysisVerticalLayout->addWidget(findmotifButton);

        fireButton = new QPushButton(layoutWidget3);
        fireButton->setObjectName(QString::fromUtf8("fireButton"));
        fireButton->setEnabled(true);

        motifsAnalysisVerticalLayout->addWidget(fireButton);

        toolBox->addItem(motifsAnalysisPage, QString::fromUtf8("Motif Analysis"));
        pathwaysAnalysisPage = new QWidget();
        pathwaysAnalysisPage->setObjectName(QString::fromUtf8("pathwaysAnalysisPage"));
        pathwaysAnalysisPage->setGeometry(QRect(0, 0, 189, 423));
        layoutWidget4 = new QWidget(pathwaysAnalysisPage);
        layoutWidget4->setObjectName(QString::fromUtf8("layoutWidget4"));
        layoutWidget4->setGeometry(QRect(0, 0, 181, 81));
        pathwaysAnalysisVerticalLayout = new QVBoxLayout(layoutWidget4);
        pathwaysAnalysisVerticalLayout->setSpacing(6);
        pathwaysAnalysisVerticalLayout->setContentsMargins(11, 11, 11, 11);
        pathwaysAnalysisVerticalLayout->setObjectName(QString::fromUtf8("pathwaysAnalysisVerticalLayout"));
        pathwaysAnalysisVerticalLayout->setContentsMargins(0, 0, 0, 0);
        findPathwayButton = new QPushButton(layoutWidget4);
        findPathwayButton->setObjectName(QString::fromUtf8("findPathwayButton"));
        findPathwayButton->setEnabled(true);

        pathwaysAnalysisVerticalLayout->addWidget(findPathwayButton);

        pageButton = new QPushButton(layoutWidget4);
        pageButton->setObjectName(QString::fromUtf8("pageButton"));
        pageButton->setEnabled(true);

        pathwaysAnalysisVerticalLayout->addWidget(pageButton);

        toolBox->addItem(pathwaysAnalysisPage, QString::fromUtf8("Pathways Analysis"));
        consAnalysisPage = new QWidget();
        consAnalysisPage->setObjectName(QString::fromUtf8("consAnalysisPage"));
        consAnalysisPage->setGeometry(QRect(0, 0, 189, 423));
        layoutWidget_3 = new QWidget(consAnalysisPage);
        layoutWidget_3->setObjectName(QString::fromUtf8("layoutWidget_3"));
        layoutWidget_3->setGeometry(QRect(0, 0, 182, 41));
        consAnalysisVerticalLayout = new QVBoxLayout(layoutWidget_3);
        consAnalysisVerticalLayout->setSpacing(0);
        consAnalysisVerticalLayout->setContentsMargins(11, 11, 11, 11);
        consAnalysisVerticalLayout->setObjectName(QString::fromUtf8("consAnalysisVerticalLayout"));
        consAnalysisVerticalLayout->setContentsMargins(0, 0, 0, 0);
        consButton = new QPushButton(layoutWidget_3);
        consButton->setObjectName(QString::fromUtf8("consButton"));
        consButton->setEnabled(true);

        consAnalysisVerticalLayout->addWidget(consButton);

        toolBox->addItem(consAnalysisPage, QString::fromUtf8("Conservation Analysis"));
        comparisonToolsPage = new QWidget();
        comparisonToolsPage->setObjectName(QString::fromUtf8("comparisonToolsPage"));
        comparisonToolsPage->setGeometry(QRect(0, 0, 189, 423));
        layoutWidget_4 = new QWidget(comparisonToolsPage);
        layoutWidget_4->setObjectName(QString::fromUtf8("layoutWidget_4"));
        layoutWidget_4->setGeometry(QRect(0, 0, 182, 131));
        comparisonToolsVerticalLayout = new QVBoxLayout(layoutWidget_4);
        comparisonToolsVerticalLayout->setSpacing(0);
        comparisonToolsVerticalLayout->setContentsMargins(11, 11, 11, 11);
        comparisonToolsVerticalLayout->setObjectName(QString::fromUtf8("comparisonToolsVerticalLayout"));
        comparisonToolsVerticalLayout->setContentsMargins(0, 0, 0, 0);
        comparePeaksButton = new QPushButton(layoutWidget_4);
        comparePeaksButton->setObjectName(QString::fromUtf8("comparePeaksButton"));

        comparisonToolsVerticalLayout->addWidget(comparePeaksButton);

        compareGenesButton = new QPushButton(layoutWidget_4);
        compareGenesButton->setObjectName(QString::fromUtf8("compareGenesButton"));

        comparisonToolsVerticalLayout->addWidget(compareGenesButton);

        jaccardButton = new QPushButton(layoutWidget_4);
        jaccardButton->setObjectName(QString::fromUtf8("jaccardButton"));

        comparisonToolsVerticalLayout->addWidget(jaccardButton);

        toolBox->addItem(comparisonToolsPage, QString::fromUtf8("Comparison tools"));
        stackedWidget = new QStackedWidget(centralWidget);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        stackedWidget->setEnabled(true);
        stackedWidget->setGeometry(QRect(190, 0, 671, 621));
        stackedWidget->setFrameShape(QFrame::StyledPanel);
        stackedWidget->setFrameShadow(QFrame::Plain);
        stackedWidget->setMidLineWidth(0);
        initialStackedPage = new QWidget();
        initialStackedPage->setObjectName(QString::fromUtf8("initialStackedPage"));
        verticalLayout_11 = new QVBoxLayout(initialStackedPage);
        verticalLayout_11->setSpacing(6);
        verticalLayout_11->setContentsMargins(11, 11, 11, 11);
        verticalLayout_11->setObjectName(QString::fromUtf8("verticalLayout_11"));
        CSlogoLabel = new QLabel(initialStackedPage);
        CSlogoLabel->setObjectName(QString::fromUtf8("CSlogoLabel"));
        CSlogoLabel->setMaximumSize(QSize(16777215, 16777215));
        CSlogoLabel->setPixmap(QPixmap(QString::fromUtf8(":/Icons/web_logo_black_white_no_bg_512.png")));
        CSlogoLabel->setScaledContents(false);
        CSlogoLabel->setAlignment(Qt::AlignBottom|Qt::AlignHCenter);

        verticalLayout_11->addWidget(CSlogoLabel);

        developedByLabel = new QLabel(initialStackedPage);
        developedByLabel->setObjectName(QString::fromUtf8("developedByLabel"));
        developedByLabel->setMaximumSize(QSize(16777215, 16777215));
        QFont font1;
        font1.setPointSize(11);
        developedByLabel->setFont(font1);
        developedByLabel->setLayoutDirection(Qt::LeftToRight);
        developedByLabel->setScaledContents(false);
        developedByLabel->setAlignment(Qt::AlignBottom|Qt::AlignHCenter);

        verticalLayout_11->addWidget(developedByLabel);

        stackedWidget->addWidget(initialStackedPage);
        loadRawDataStackedPage = new QWidget();
        loadRawDataStackedPage->setObjectName(QString::fromUtf8("loadRawDataStackedPage"));
        loadRawDataStackedPage->setLayoutDirection(Qt::LeftToRight);
        verticalLayout_6 = new QVBoxLayout(loadRawDataStackedPage);
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setContentsMargins(11, 11, 11, 11);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        loadTabWidget = new QTabWidget(loadRawDataStackedPage);
        loadTabWidget->setObjectName(QString::fromUtf8("loadTabWidget"));
        loadTabWidget->setTabsClosable(false);
        loadParametersTab = new QWidget();
        loadParametersTab->setObjectName(QString::fromUtf8("loadParametersTab"));
        loadTitleLabel = new QLabel(loadParametersTab);
        loadTitleLabel->setObjectName(QString::fromUtf8("loadTitleLabel"));
        loadTitleLabel->setGeometry(QRect(0, 8, 588, 21));
        QFont font2;
        font2.setPointSize(14);
        loadTitleLabel->setFont(font2);
        loadLine1 = new QFrame(loadParametersTab);
        loadLine1->setObjectName(QString::fromUtf8("loadLine1"));
        loadLine1->setGeometry(QRect(0, 30, 627, 3));
        loadLine1->setFrameShape(QFrame::HLine);
        loadLine1->setFrameShadow(QFrame::Sunken);
        layoutWidget5 = new QWidget(loadParametersTab);
        layoutWidget5->setObjectName(QString::fromUtf8("layoutWidget5"));
        layoutWidget5->setGeometry(QRect(0, 30, 641, 131));
        loadParametersGridLayout = new QGridLayout(layoutWidget5);
        loadParametersGridLayout->setSpacing(6);
        loadParametersGridLayout->setContentsMargins(11, 11, 11, 11);
        loadParametersGridLayout->setObjectName(QString::fromUtf8("loadParametersGridLayout"));
        loadParametersGridLayout->setContentsMargins(0, 0, 0, 0);
        selectFormatLabel = new QLabel(layoutWidget5);
        selectFormatLabel->setObjectName(QString::fromUtf8("selectFormatLabel"));
        selectFormatLabel->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        loadParametersGridLayout->addWidget(selectFormatLabel, 0, 0, 1, 1);

        loadFormatComboBox = new QComboBox(layoutWidget5);
        loadFormatComboBox->setObjectName(QString::fromUtf8("loadFormatComboBox"));
        loadFormatComboBox->setEditable(false);

        loadParametersGridLayout->addWidget(loadFormatComboBox, 0, 1, 1, 2);

        loadFormatInfoButton = new QPushButton(layoutWidget5);
        loadFormatInfoButton->setObjectName(QString::fromUtf8("loadFormatInfoButton"));
        loadFormatInfoButton->setMaximumSize(QSize(21, 21));
        loadFormatInfoButton->setMouseTracking(false);
        loadFormatInfoButton->setAcceptDrops(false);
        loadFormatInfoButton->setLayoutDirection(Qt::LeftToRight);
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/Icons/info.png"), QSize(), QIcon::Normal, QIcon::Off);
        loadFormatInfoButton->setIcon(icon1);
        loadFormatInfoButton->setAutoRepeatDelay(100);
        loadFormatInfoButton->setDefault(false);
        loadFormatInfoButton->setFlat(true);

        loadParametersGridLayout->addWidget(loadFormatInfoButton, 0, 3, 1, 1);

        selectChipRawLabel = new QLabel(layoutWidget5);
        selectChipRawLabel->setObjectName(QString::fromUtf8("selectChipRawLabel"));
        selectChipRawLabel->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        loadParametersGridLayout->addWidget(selectChipRawLabel, 1, 0, 1, 1);

        selectChipRawFolderToolButton = new QToolButton(layoutWidget5);
        selectChipRawFolderToolButton->setObjectName(QString::fromUtf8("selectChipRawFolderToolButton"));

        loadParametersGridLayout->addWidget(selectChipRawFolderToolButton, 1, 3, 1, 1);

        selectInputRawLabel = new QLabel(layoutWidget5);
        selectInputRawLabel->setObjectName(QString::fromUtf8("selectInputRawLabel"));
        selectInputRawLabel->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        loadParametersGridLayout->addWidget(selectInputRawLabel, 2, 0, 1, 1);

        selectInputRawFolderToolButton = new QToolButton(layoutWidget5);
        selectInputRawFolderToolButton->setObjectName(QString::fromUtf8("selectInputRawFolderToolButton"));

        loadParametersGridLayout->addWidget(selectInputRawFolderToolButton, 2, 3, 1, 1);

        selectChipRawFolderLineEdit = new QLineEdit(layoutWidget5);
        selectChipRawFolderLineEdit->setObjectName(QString::fromUtf8("selectChipRawFolderLineEdit"));
        selectChipRawFolderLineEdit->setReadOnly(true);

        loadParametersGridLayout->addWidget(selectChipRawFolderLineEdit, 1, 1, 1, 2);

        selectInputRawFolderLineEdit = new QLineEdit(layoutWidget5);
        selectInputRawFolderLineEdit->setObjectName(QString::fromUtf8("selectInputRawFolderLineEdit"));
        selectInputRawFolderLineEdit->setReadOnly(true);

        loadParametersGridLayout->addWidget(selectInputRawFolderLineEdit, 2, 1, 1, 2);

        loadLine2 = new QFrame(loadParametersTab);
        loadLine2->setObjectName(QString::fromUtf8("loadLine2"));
        loadLine2->setGeometry(QRect(0, 160, 629, 3));
        loadLine2->setFrameShadow(QFrame::Sunken);
        loadLine2->setFrameShape(QFrame::HLine);
        layoutWidget6 = new QWidget(loadParametersTab);
        layoutWidget6->setObjectName(QString::fromUtf8("layoutWidget6"));
        layoutWidget6->setGeometry(QRect(360, 160, 281, 41));
        loadButtonsHorizontalLayout = new QHBoxLayout(layoutWidget6);
        loadButtonsHorizontalLayout->setSpacing(6);
        loadButtonsHorizontalLayout->setContentsMargins(11, 11, 11, 11);
        loadButtonsHorizontalLayout->setObjectName(QString::fromUtf8("loadButtonsHorizontalLayout"));
        loadButtonsHorizontalLayout->setContentsMargins(0, 0, 0, 0);
        loadClearButton = new QPushButton(layoutWidget6);
        loadClearButton->setObjectName(QString::fromUtf8("loadClearButton"));

        loadButtonsHorizontalLayout->addWidget(loadClearButton);

        splitButton = new QPushButton(layoutWidget6);
        splitButton->setObjectName(QString::fromUtf8("splitButton"));

        loadButtonsHorizontalLayout->addWidget(splitButton);

        loadTabWidget->addTab(loadParametersTab, QString());
        loadProgressTab = new QWidget();
        loadProgressTab->setObjectName(QString::fromUtf8("loadProgressTab"));
        loadProgressTextEdit = new QTextEdit(loadProgressTab);
        loadProgressTextEdit->setObjectName(QString::fromUtf8("loadProgressTextEdit"));
        loadProgressTextEdit->setGeometry(QRect(0, 0, 641, 501));
        loadProgressTextEdit->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);
        stopSplitProcessButton = new QPushButton(loadProgressTab);
        stopSplitProcessButton->setObjectName(QString::fromUtf8("stopSplitProcessButton"));
        stopSplitProcessButton->setEnabled(false);
        stopSplitProcessButton->setGeometry(QRect(520, 510, 113, 32));
        loadTabWidget->addTab(loadProgressTab, QString());

        verticalLayout_6->addWidget(loadTabWidget);

        stackedWidget->addWidget(loadRawDataStackedPage);
        peakDetectionStackedPage = new QWidget();
        peakDetectionStackedPage->setObjectName(QString::fromUtf8("peakDetectionStackedPage"));
        verticalLayout_7 = new QVBoxLayout(peakDetectionStackedPage);
        verticalLayout_7->setSpacing(6);
        verticalLayout_7->setContentsMargins(11, 11, 11, 11);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        peakDetectionTabWidget = new QTabWidget(peakDetectionStackedPage);
        peakDetectionTabWidget->setObjectName(QString::fromUtf8("peakDetectionTabWidget"));
        peakDetectionParametersTab = new QWidget();
        peakDetectionParametersTab->setObjectName(QString::fromUtf8("peakDetectionParametersTab"));
        peakDetectionTitleLabel = new QLabel(peakDetectionParametersTab);
        peakDetectionTitleLabel->setObjectName(QString::fromUtf8("peakDetectionTitleLabel"));
        peakDetectionTitleLabel->setGeometry(QRect(0, 10, 588, 21));
        peakDetectionTitleLabel->setFont(font2);
        peakDetectionLine1 = new QFrame(peakDetectionParametersTab);
        peakDetectionLine1->setObjectName(QString::fromUtf8("peakDetectionLine1"));
        peakDetectionLine1->setGeometry(QRect(0, 30, 627, 3));
        peakDetectionLine1->setFrameShape(QFrame::HLine);
        peakDetectionLine1->setFrameShadow(QFrame::Sunken);
        layoutWidget7 = new QWidget(peakDetectionParametersTab);
        layoutWidget7->setObjectName(QString::fromUtf8("layoutWidget7"));
        layoutWidget7->setGeometry(QRect(0, 40, 641, 481));
        peakDetectionGridLayout = new QGridLayout(layoutWidget7);
        peakDetectionGridLayout->setSpacing(6);
        peakDetectionGridLayout->setContentsMargins(11, 11, 11, 11);
        peakDetectionGridLayout->setObjectName(QString::fromUtf8("peakDetectionGridLayout"));
        peakDetectionGridLayout->setHorizontalSpacing(-1);
        peakDetectionGridLayout->setVerticalSpacing(15);
        peakDetectionGridLayout->setContentsMargins(0, 0, 0, 0);
        selectChipFolderLabel = new QLabel(layoutWidget7);
        selectChipFolderLabel->setObjectName(QString::fromUtf8("selectChipFolderLabel"));

        peakDetectionGridLayout->addWidget(selectChipFolderLabel, 0, 0, 1, 1);

        selectChipFolderLineEdit = new QLineEdit(layoutWidget7);
        selectChipFolderLineEdit->setObjectName(QString::fromUtf8("selectChipFolderLineEdit"));
        selectChipFolderLineEdit->setReadOnly(true);

        peakDetectionGridLayout->addWidget(selectChipFolderLineEdit, 0, 1, 1, 3);

        selectChipFolderToolButton = new QToolButton(layoutWidget7);
        selectChipFolderToolButton->setObjectName(QString::fromUtf8("selectChipFolderToolButton"));

        peakDetectionGridLayout->addWidget(selectChipFolderToolButton, 0, 4, 1, 1);

        selectChipFolderInfoButton = new QPushButton(layoutWidget7);
        selectChipFolderInfoButton->setObjectName(QString::fromUtf8("selectChipFolderInfoButton"));
        selectChipFolderInfoButton->setMaximumSize(QSize(21, 21));
        selectChipFolderInfoButton->setMouseTracking(false);
        selectChipFolderInfoButton->setAcceptDrops(false);
        selectChipFolderInfoButton->setLayoutDirection(Qt::LeftToRight);
        selectChipFolderInfoButton->setIcon(icon1);
        selectChipFolderInfoButton->setAutoRepeatDelay(100);
        selectChipFolderInfoButton->setDefault(false);
        selectChipFolderInfoButton->setFlat(true);

        peakDetectionGridLayout->addWidget(selectChipFolderInfoButton, 0, 5, 1, 1);

        selectInputFolderLabel = new QLabel(layoutWidget7);
        selectInputFolderLabel->setObjectName(QString::fromUtf8("selectInputFolderLabel"));

        peakDetectionGridLayout->addWidget(selectInputFolderLabel, 1, 0, 1, 1);

        selectInputFolderLineEdit = new QLineEdit(layoutWidget7);
        selectInputFolderLineEdit->setObjectName(QString::fromUtf8("selectInputFolderLineEdit"));
        selectInputFolderLineEdit->setReadOnly(true);

        peakDetectionGridLayout->addWidget(selectInputFolderLineEdit, 1, 1, 1, 3);

        selectInputFolderToolButton = new QToolButton(layoutWidget7);
        selectInputFolderToolButton->setObjectName(QString::fromUtf8("selectInputFolderToolButton"));

        peakDetectionGridLayout->addWidget(selectInputFolderToolButton, 1, 4, 1, 1);

        selectInputFolderInfoButton = new QPushButton(layoutWidget7);
        selectInputFolderInfoButton->setObjectName(QString::fromUtf8("selectInputFolderInfoButton"));
        selectInputFolderInfoButton->setMaximumSize(QSize(21, 21));
        selectInputFolderInfoButton->setMouseTracking(false);
        selectInputFolderInfoButton->setAcceptDrops(false);
        selectInputFolderInfoButton->setLayoutDirection(Qt::LeftToRight);
        selectInputFolderInfoButton->setIcon(icon1);
        selectInputFolderInfoButton->setAutoRepeatDelay(100);
        selectInputFolderInfoButton->setDefault(false);
        selectInputFolderInfoButton->setFlat(true);

        peakDetectionGridLayout->addWidget(selectInputFolderInfoButton, 1, 5, 1, 1);

        peakDetectionOutputFileLabel = new QLabel(layoutWidget7);
        peakDetectionOutputFileLabel->setObjectName(QString::fromUtf8("peakDetectionOutputFileLabel"));

        peakDetectionGridLayout->addWidget(peakDetectionOutputFileLabel, 2, 0, 1, 1);

        peakDetectionOutputFileLineEdit = new QLineEdit(layoutWidget7);
        peakDetectionOutputFileLineEdit->setObjectName(QString::fromUtf8("peakDetectionOutputFileLineEdit"));

        peakDetectionGridLayout->addWidget(peakDetectionOutputFileLineEdit, 2, 1, 1, 3);

        peakDetectionOutputFileInfoButton = new QPushButton(layoutWidget7);
        peakDetectionOutputFileInfoButton->setObjectName(QString::fromUtf8("peakDetectionOutputFileInfoButton"));
        peakDetectionOutputFileInfoButton->setMaximumSize(QSize(21, 21));
        peakDetectionOutputFileInfoButton->setMouseTracking(false);
        peakDetectionOutputFileInfoButton->setAcceptDrops(false);
        peakDetectionOutputFileInfoButton->setLayoutDirection(Qt::LeftToRight);
        peakDetectionOutputFileInfoButton->setIcon(icon1);
        peakDetectionOutputFileInfoButton->setAutoRepeatDelay(100);
        peakDetectionOutputFileInfoButton->setDefault(false);
        peakDetectionOutputFileInfoButton->setFlat(true);

        peakDetectionGridLayout->addWidget(peakDetectionOutputFileInfoButton, 2, 5, 1, 1);

        peakDetectionFormatLabel = new QLabel(layoutWidget7);
        peakDetectionFormatLabel->setObjectName(QString::fromUtf8("peakDetectionFormatLabel"));

        peakDetectionGridLayout->addWidget(peakDetectionFormatLabel, 3, 0, 1, 1);

        peakDetectionFormatComboBox = new QComboBox(layoutWidget7);
        peakDetectionFormatComboBox->setObjectName(QString::fromUtf8("peakDetectionFormatComboBox"));
        peakDetectionFormatComboBox->setEditable(false);
        peakDetectionFormatComboBox->setModelColumn(0);

        peakDetectionGridLayout->addWidget(peakDetectionFormatComboBox, 3, 1, 1, 1);

        peakDetectionFormatInfoButton = new QPushButton(layoutWidget7);
        peakDetectionFormatInfoButton->setObjectName(QString::fromUtf8("peakDetectionFormatInfoButton"));
        peakDetectionFormatInfoButton->setMaximumSize(QSize(21, 21));
        peakDetectionFormatInfoButton->setMouseTracking(false);
        peakDetectionFormatInfoButton->setAcceptDrops(false);
        peakDetectionFormatInfoButton->setLayoutDirection(Qt::LeftToRight);
        peakDetectionFormatInfoButton->setIcon(icon1);
        peakDetectionFormatInfoButton->setAutoRepeatDelay(100);
        peakDetectionFormatInfoButton->setDefault(false);
        peakDetectionFormatInfoButton->setFlat(true);

        peakDetectionGridLayout->addWidget(peakDetectionFormatInfoButton, 3, 2, 1, 1);

        peakDetectionFoldLabel = new QLabel(layoutWidget7);
        peakDetectionFoldLabel->setObjectName(QString::fromUtf8("peakDetectionFoldLabel"));

        peakDetectionGridLayout->addWidget(peakDetectionFoldLabel, 5, 0, 1, 1);

        peakDetectionFoldSpinBox = new QDoubleSpinBox(layoutWidget7);
        peakDetectionFoldSpinBox->setObjectName(QString::fromUtf8("peakDetectionFoldSpinBox"));
        peakDetectionFoldSpinBox->setDecimals(1);
        peakDetectionFoldSpinBox->setMaximum(100);
        peakDetectionFoldSpinBox->setValue(2);

        peakDetectionGridLayout->addWidget(peakDetectionFoldSpinBox, 5, 1, 1, 1);

        peakDetectionFoldInfoButton = new QPushButton(layoutWidget7);
        peakDetectionFoldInfoButton->setObjectName(QString::fromUtf8("peakDetectionFoldInfoButton"));
        peakDetectionFoldInfoButton->setMaximumSize(QSize(21, 21));
        peakDetectionFoldInfoButton->setMouseTracking(false);
        peakDetectionFoldInfoButton->setAcceptDrops(false);
        peakDetectionFoldInfoButton->setLayoutDirection(Qt::LeftToRight);
        peakDetectionFoldInfoButton->setIcon(icon1);
        peakDetectionFoldInfoButton->setAutoRepeatDelay(100);
        peakDetectionFoldInfoButton->setDefault(false);
        peakDetectionFoldInfoButton->setFlat(true);

        peakDetectionGridLayout->addWidget(peakDetectionFoldInfoButton, 5, 2, 1, 1);

        peakDetectionThresholdLabel = new QLabel(layoutWidget7);
        peakDetectionThresholdLabel->setObjectName(QString::fromUtf8("peakDetectionThresholdLabel"));

        peakDetectionGridLayout->addWidget(peakDetectionThresholdLabel, 6, 0, 1, 1);

        peakDetectionThresholdSpinBox = new QSpinBox(layoutWidget7);
        peakDetectionThresholdSpinBox->setObjectName(QString::fromUtf8("peakDetectionThresholdSpinBox"));
        peakDetectionThresholdSpinBox->setValue(15);

        peakDetectionGridLayout->addWidget(peakDetectionThresholdSpinBox, 6, 1, 1, 1);

        peakDetectionThresholdInfoButton = new QPushButton(layoutWidget7);
        peakDetectionThresholdInfoButton->setObjectName(QString::fromUtf8("peakDetectionThresholdInfoButton"));
        peakDetectionThresholdInfoButton->setMaximumSize(QSize(21, 21));
        peakDetectionThresholdInfoButton->setMouseTracking(false);
        peakDetectionThresholdInfoButton->setAcceptDrops(false);
        peakDetectionThresholdInfoButton->setLayoutDirection(Qt::LeftToRight);
        peakDetectionThresholdInfoButton->setIcon(icon1);
        peakDetectionThresholdInfoButton->setAutoRepeatDelay(100);
        peakDetectionThresholdInfoButton->setDefault(false);
        peakDetectionThresholdInfoButton->setFlat(true);

        peakDetectionGridLayout->addWidget(peakDetectionThresholdInfoButton, 6, 2, 1, 1);

        peakDetectionFragLenLabel = new QLabel(layoutWidget7);
        peakDetectionFragLenLabel->setObjectName(QString::fromUtf8("peakDetectionFragLenLabel"));

        peakDetectionGridLayout->addWidget(peakDetectionFragLenLabel, 7, 0, 1, 1);

        peakDetectionFragLenSpinBox = new QSpinBox(layoutWidget7);
        peakDetectionFragLenSpinBox->setObjectName(QString::fromUtf8("peakDetectionFragLenSpinBox"));
        peakDetectionFragLenSpinBox->setMaximum(1000);
        peakDetectionFragLenSpinBox->setSingleStep(1);
        peakDetectionFragLenSpinBox->setValue(170);

        peakDetectionGridLayout->addWidget(peakDetectionFragLenSpinBox, 7, 1, 1, 1);

        peakDetectionFragLenInfoButton = new QPushButton(layoutWidget7);
        peakDetectionFragLenInfoButton->setObjectName(QString::fromUtf8("peakDetectionFragLenInfoButton"));
        peakDetectionFragLenInfoButton->setMaximumSize(QSize(21, 21));
        peakDetectionFragLenInfoButton->setMouseTracking(false);
        peakDetectionFragLenInfoButton->setAcceptDrops(false);
        peakDetectionFragLenInfoButton->setLayoutDirection(Qt::LeftToRight);
        peakDetectionFragLenInfoButton->setIcon(icon1);
        peakDetectionFragLenInfoButton->setAutoRepeatDelay(100);
        peakDetectionFragLenInfoButton->setDefault(false);
        peakDetectionFragLenInfoButton->setFlat(true);

        peakDetectionGridLayout->addWidget(peakDetectionFragLenInfoButton, 7, 2, 1, 1);

        peakDetectionMinLenLabel = new QLabel(layoutWidget7);
        peakDetectionMinLenLabel->setObjectName(QString::fromUtf8("peakDetectionMinLenLabel"));

        peakDetectionGridLayout->addWidget(peakDetectionMinLenLabel, 8, 0, 1, 1);

        peakDetectionMinLenSpinBox = new QSpinBox(layoutWidget7);
        peakDetectionMinLenSpinBox->setObjectName(QString::fromUtf8("peakDetectionMinLenSpinBox"));
        peakDetectionMinLenSpinBox->setMaximum(1000);
        peakDetectionMinLenSpinBox->setValue(100);

        peakDetectionGridLayout->addWidget(peakDetectionMinLenSpinBox, 8, 1, 1, 1);

        peakDetectionMinLenInfoButton = new QPushButton(layoutWidget7);
        peakDetectionMinLenInfoButton->setObjectName(QString::fromUtf8("peakDetectionMinLenInfoButton"));
        peakDetectionMinLenInfoButton->setMaximumSize(QSize(21, 21));
        peakDetectionMinLenInfoButton->setMouseTracking(false);
        peakDetectionMinLenInfoButton->setAcceptDrops(false);
        peakDetectionMinLenInfoButton->setLayoutDirection(Qt::LeftToRight);
        peakDetectionMinLenInfoButton->setIcon(icon1);
        peakDetectionMinLenInfoButton->setAutoRepeatDelay(100);
        peakDetectionMinLenInfoButton->setDefault(false);
        peakDetectionMinLenInfoButton->setFlat(true);

        peakDetectionGridLayout->addWidget(peakDetectionMinLenInfoButton, 8, 2, 1, 1);

        peakDetectionMinDistLabel = new QLabel(layoutWidget7);
        peakDetectionMinDistLabel->setObjectName(QString::fromUtf8("peakDetectionMinDistLabel"));

        peakDetectionGridLayout->addWidget(peakDetectionMinDistLabel, 9, 0, 1, 1);

        peakDetectionMinDistSpinBox = new QSpinBox(layoutWidget7);
        peakDetectionMinDistSpinBox->setObjectName(QString::fromUtf8("peakDetectionMinDistSpinBox"));
        peakDetectionMinDistSpinBox->setMaximum(10000);
        peakDetectionMinDistSpinBox->setValue(100);

        peakDetectionGridLayout->addWidget(peakDetectionMinDistSpinBox, 9, 1, 1, 1);

        peakDetectionMinDistInfoButton = new QPushButton(layoutWidget7);
        peakDetectionMinDistInfoButton->setObjectName(QString::fromUtf8("peakDetectionMinDistInfoButton"));
        peakDetectionMinDistInfoButton->setMaximumSize(QSize(21, 21));
        peakDetectionMinDistInfoButton->setMouseTracking(false);
        peakDetectionMinDistInfoButton->setAcceptDrops(false);
        peakDetectionMinDistInfoButton->setLayoutDirection(Qt::LeftToRight);
        peakDetectionMinDistInfoButton->setIcon(icon1);
        peakDetectionMinDistInfoButton->setAutoRepeatDelay(100);
        peakDetectionMinDistInfoButton->setDefault(false);
        peakDetectionMinDistInfoButton->setFlat(true);

        peakDetectionGridLayout->addWidget(peakDetectionMinDistInfoButton, 9, 2, 1, 1);

        peakDetectionUniqueCheckBox = new QCheckBox(layoutWidget7);
        peakDetectionUniqueCheckBox->setObjectName(QString::fromUtf8("peakDetectionUniqueCheckBox"));
        peakDetectionUniqueCheckBox->setLayoutDirection(Qt::LeftToRight);
        peakDetectionUniqueCheckBox->setChecked(true);

        peakDetectionGridLayout->addWidget(peakDetectionUniqueCheckBox, 11, 0, 1, 1);

        peakDetectionUniqueInfoButton = new QPushButton(layoutWidget7);
        peakDetectionUniqueInfoButton->setObjectName(QString::fromUtf8("peakDetectionUniqueInfoButton"));
        peakDetectionUniqueInfoButton->setMaximumSize(QSize(21, 21));
        peakDetectionUniqueInfoButton->setMouseTracking(false);
        peakDetectionUniqueInfoButton->setAcceptDrops(false);
        peakDetectionUniqueInfoButton->setLayoutDirection(Qt::LeftToRight);
        peakDetectionUniqueInfoButton->setIcon(icon1);
        peakDetectionUniqueInfoButton->setAutoRepeatDelay(100);
        peakDetectionUniqueInfoButton->setDefault(false);
        peakDetectionUniqueInfoButton->setFlat(true);

        peakDetectionGridLayout->addWidget(peakDetectionUniqueInfoButton, 11, 2, 1, 1);

        peakDetectionMinPeakHeightLabel = new QLabel(layoutWidget7);
        peakDetectionMinPeakHeightLabel->setObjectName(QString::fromUtf8("peakDetectionMinPeakHeightLabel"));

        peakDetectionGridLayout->addWidget(peakDetectionMinPeakHeightLabel, 10, 0, 1, 1);

        peakDetectionMinPeakHeightSpinBox = new QSpinBox(layoutWidget7);
        peakDetectionMinPeakHeightSpinBox->setObjectName(QString::fromUtf8("peakDetectionMinPeakHeightSpinBox"));
        peakDetectionMinPeakHeightSpinBox->setMaximum(10000);
        peakDetectionMinPeakHeightSpinBox->setValue(0);

        peakDetectionGridLayout->addWidget(peakDetectionMinPeakHeightSpinBox, 10, 1, 1, 1);

        peakDetectionMinPeakHeightInfoButton = new QPushButton(layoutWidget7);
        peakDetectionMinPeakHeightInfoButton->setObjectName(QString::fromUtf8("peakDetectionMinPeakHeightInfoButton"));
        peakDetectionMinPeakHeightInfoButton->setMaximumSize(QSize(21, 21));
        peakDetectionMinPeakHeightInfoButton->setMouseTracking(false);
        peakDetectionMinPeakHeightInfoButton->setAcceptDrops(false);
        peakDetectionMinPeakHeightInfoButton->setLayoutDirection(Qt::LeftToRight);
        peakDetectionMinPeakHeightInfoButton->setIcon(icon1);
        peakDetectionMinPeakHeightInfoButton->setAutoRepeatDelay(100);
        peakDetectionMinPeakHeightInfoButton->setDefault(false);
        peakDetectionMinPeakHeightInfoButton->setFlat(true);

        peakDetectionGridLayout->addWidget(peakDetectionMinPeakHeightInfoButton, 10, 2, 1, 1);

        peakDetectionSpeciesLabel = new QLabel(layoutWidget7);
        peakDetectionSpeciesLabel->setObjectName(QString::fromUtf8("peakDetectionSpeciesLabel"));

        peakDetectionGridLayout->addWidget(peakDetectionSpeciesLabel, 4, 0, 1, 1);

        peakDetectionSpeciesComboBox = new QComboBox(layoutWidget7);
        peakDetectionSpeciesComboBox->setObjectName(QString::fromUtf8("peakDetectionSpeciesComboBox"));
        peakDetectionSpeciesComboBox->setEnabled(true);
        peakDetectionSpeciesComboBox->setEditable(false);
        peakDetectionSpeciesComboBox->setModelColumn(0);

        peakDetectionGridLayout->addWidget(peakDetectionSpeciesComboBox, 4, 1, 1, 1);

        peakDetectionSpeciesInfoButton = new QPushButton(layoutWidget7);
        peakDetectionSpeciesInfoButton->setObjectName(QString::fromUtf8("peakDetectionSpeciesInfoButton"));
        peakDetectionSpeciesInfoButton->setMaximumSize(QSize(21, 21));
        peakDetectionSpeciesInfoButton->setMouseTracking(false);
        peakDetectionSpeciesInfoButton->setAcceptDrops(false);
        peakDetectionSpeciesInfoButton->setLayoutDirection(Qt::LeftToRight);
        peakDetectionSpeciesInfoButton->setIcon(icon1);
        peakDetectionSpeciesInfoButton->setAutoRepeatDelay(100);
        peakDetectionSpeciesInfoButton->setDefault(false);
        peakDetectionSpeciesInfoButton->setFlat(true);

        peakDetectionGridLayout->addWidget(peakDetectionSpeciesInfoButton, 4, 2, 1, 1);

        peakDetectionLine2 = new QFrame(peakDetectionParametersTab);
        peakDetectionLine2->setObjectName(QString::fromUtf8("peakDetectionLine2"));
        peakDetectionLine2->setGeometry(QRect(0, 510, 629, 16));
        peakDetectionLine2->setFrameShadow(QFrame::Sunken);
        peakDetectionLine2->setFrameShape(QFrame::HLine);
        layoutWidget8 = new QWidget(peakDetectionParametersTab);
        layoutWidget8->setObjectName(QString::fromUtf8("layoutWidget8"));
        layoutWidget8->setGeometry(QRect(280, 520, 361, 41));
        peakDetectionButtonsHorizontalLayout = new QHBoxLayout(layoutWidget8);
        peakDetectionButtonsHorizontalLayout->setSpacing(6);
        peakDetectionButtonsHorizontalLayout->setContentsMargins(11, 11, 11, 11);
        peakDetectionButtonsHorizontalLayout->setObjectName(QString::fromUtf8("peakDetectionButtonsHorizontalLayout"));
        peakDetectionButtonsHorizontalLayout->setContentsMargins(0, 0, 0, 0);
        runPeakDetectionInverseButton = new QPushButton(layoutWidget8);
        runPeakDetectionInverseButton->setObjectName(QString::fromUtf8("runPeakDetectionInverseButton"));

        peakDetectionButtonsHorizontalLayout->addWidget(runPeakDetectionInverseButton);

        peakDetectionClearButton = new QPushButton(layoutWidget8);
        peakDetectionClearButton->setObjectName(QString::fromUtf8("peakDetectionClearButton"));

        peakDetectionButtonsHorizontalLayout->addWidget(peakDetectionClearButton);

        runCSButton = new QPushButton(layoutWidget8);
        runCSButton->setObjectName(QString::fromUtf8("runCSButton"));

        peakDetectionButtonsHorizontalLayout->addWidget(runCSButton);

        peakDetectionTabWidget->addTab(peakDetectionParametersTab, QString());
        peakDetectionProgressTab = new QWidget();
        peakDetectionProgressTab->setObjectName(QString::fromUtf8("peakDetectionProgressTab"));
        peakDetectionProgressTextEdit = new QTextEdit(peakDetectionProgressTab);
        peakDetectionProgressTextEdit->setObjectName(QString::fromUtf8("peakDetectionProgressTextEdit"));
        peakDetectionProgressTextEdit->setGeometry(QRect(0, 0, 641, 501));
        peakDetectionProgressTextEdit->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);
        stopRunCSProcessButton = new QPushButton(peakDetectionProgressTab);
        stopRunCSProcessButton->setObjectName(QString::fromUtf8("stopRunCSProcessButton"));
        stopRunCSProcessButton->setEnabled(false);
        stopRunCSProcessButton->setGeometry(QRect(520, 510, 113, 32));
        peakDetectionTabWidget->addTab(peakDetectionProgressTab, QString());

        verticalLayout_7->addWidget(peakDetectionTabWidget);

        stackedWidget->addWidget(peakDetectionStackedPage);
        summaryStackedPage = new QWidget();
        summaryStackedPage->setObjectName(QString::fromUtf8("summaryStackedPage"));
        verticalLayout_4 = new QVBoxLayout(summaryStackedPage);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        genesSummaryTabWidget = new QTabWidget(summaryStackedPage);
        genesSummaryTabWidget->setObjectName(QString::fromUtf8("genesSummaryTabWidget"));
        genesSummaryParametersTab = new QWidget();
        genesSummaryParametersTab->setObjectName(QString::fromUtf8("genesSummaryParametersTab"));
        genesSummaryTitleLabel = new QLabel(genesSummaryParametersTab);
        genesSummaryTitleLabel->setObjectName(QString::fromUtf8("genesSummaryTitleLabel"));
        genesSummaryTitleLabel->setGeometry(QRect(0, 10, 588, 21));
        genesSummaryTitleLabel->setFont(font2);
        genesSummaryLine1 = new QFrame(genesSummaryParametersTab);
        genesSummaryLine1->setObjectName(QString::fromUtf8("genesSummaryLine1"));
        genesSummaryLine1->setGeometry(QRect(0, 30, 627, 3));
        genesSummaryLine1->setFrameShape(QFrame::HLine);
        genesSummaryLine1->setFrameShadow(QFrame::Sunken);
        layoutWidget_2 = new QWidget(genesSummaryParametersTab);
        layoutWidget_2->setObjectName(QString::fromUtf8("layoutWidget_2"));
        layoutWidget_2->setGeometry(QRect(0, 40, 641, 241));
        genesSummaryGridLayout = new QGridLayout(layoutWidget_2);
        genesSummaryGridLayout->setSpacing(6);
        genesSummaryGridLayout->setContentsMargins(11, 11, 11, 11);
        genesSummaryGridLayout->setObjectName(QString::fromUtf8("genesSummaryGridLayout"));
        genesSummaryGridLayout->setHorizontalSpacing(-1);
        genesSummaryGridLayout->setVerticalSpacing(15);
        genesSummaryGridLayout->setContentsMargins(0, 0, 0, 0);
        genesSummarySelectPeaksLabel = new QLabel(layoutWidget_2);
        genesSummarySelectPeaksLabel->setObjectName(QString::fromUtf8("genesSummarySelectPeaksLabel"));

        genesSummaryGridLayout->addWidget(genesSummarySelectPeaksLabel, 0, 0, 1, 1);

        genesSummarySelectPeaksLineEdit = new QLineEdit(layoutWidget_2);
        genesSummarySelectPeaksLineEdit->setObjectName(QString::fromUtf8("genesSummarySelectPeaksLineEdit"));
        genesSummarySelectPeaksLineEdit->setReadOnly(true);

        genesSummaryGridLayout->addWidget(genesSummarySelectPeaksLineEdit, 0, 1, 1, 3);

        genesSummarySelectPeaksToolButton = new QToolButton(layoutWidget_2);
        genesSummarySelectPeaksToolButton->setObjectName(QString::fromUtf8("genesSummarySelectPeaksToolButton"));

        genesSummaryGridLayout->addWidget(genesSummarySelectPeaksToolButton, 0, 4, 1, 1);

        genesSummarySelectPeaksInfoButton = new QPushButton(layoutWidget_2);
        genesSummarySelectPeaksInfoButton->setObjectName(QString::fromUtf8("genesSummarySelectPeaksInfoButton"));
        genesSummarySelectPeaksInfoButton->setMaximumSize(QSize(21, 21));
        genesSummarySelectPeaksInfoButton->setMouseTracking(false);
        genesSummarySelectPeaksInfoButton->setAcceptDrops(false);
        genesSummarySelectPeaksInfoButton->setLayoutDirection(Qt::LeftToRight);
        genesSummarySelectPeaksInfoButton->setIcon(icon1);
        genesSummarySelectPeaksInfoButton->setAutoRepeatDelay(100);
        genesSummarySelectPeaksInfoButton->setDefault(false);
        genesSummarySelectPeaksInfoButton->setFlat(true);

        genesSummaryGridLayout->addWidget(genesSummarySelectPeaksInfoButton, 0, 5, 1, 1);

        genesSummaryLenULabel = new QLabel(layoutWidget_2);
        genesSummaryLenULabel->setObjectName(QString::fromUtf8("genesSummaryLenULabel"));

        genesSummaryGridLayout->addWidget(genesSummaryLenULabel, 4, 0, 1, 1);

        genesSummaryLenDLabel = new QLabel(layoutWidget_2);
        genesSummaryLenDLabel->setObjectName(QString::fromUtf8("genesSummaryLenDLabel"));

        genesSummaryGridLayout->addWidget(genesSummaryLenDLabel, 5, 0, 1, 1);

        genesSummaryLenDSpinBox = new QSpinBox(layoutWidget_2);
        genesSummaryLenDSpinBox->setObjectName(QString::fromUtf8("genesSummaryLenDSpinBox"));
        genesSummaryLenDSpinBox->setMaximum(100000);
        genesSummaryLenDSpinBox->setSingleStep(100);
        genesSummaryLenDSpinBox->setValue(2000);

        genesSummaryGridLayout->addWidget(genesSummaryLenDSpinBox, 5, 1, 1, 1);

        genesSummaryLenUSpinBox = new QSpinBox(layoutWidget_2);
        genesSummaryLenUSpinBox->setObjectName(QString::fromUtf8("genesSummaryLenUSpinBox"));
        genesSummaryLenUSpinBox->setMaximum(100000);
        genesSummaryLenUSpinBox->setSingleStep(100);
        genesSummaryLenUSpinBox->setValue(2000);

        genesSummaryGridLayout->addWidget(genesSummaryLenUSpinBox, 4, 1, 1, 1);

        genesSummaryOutputFileLabel = new QLabel(layoutWidget_2);
        genesSummaryOutputFileLabel->setObjectName(QString::fromUtf8("genesSummaryOutputFileLabel"));

        genesSummaryGridLayout->addWidget(genesSummaryOutputFileLabel, 1, 0, 1, 1);

        genesSummaryOutputFileLineEdit = new QLineEdit(layoutWidget_2);
        genesSummaryOutputFileLineEdit->setObjectName(QString::fromUtf8("genesSummaryOutputFileLineEdit"));

        genesSummaryGridLayout->addWidget(genesSummaryOutputFileLineEdit, 1, 1, 1, 3);

        genesSummaryOutputFileInfoButton = new QPushButton(layoutWidget_2);
        genesSummaryOutputFileInfoButton->setObjectName(QString::fromUtf8("genesSummaryOutputFileInfoButton"));
        genesSummaryOutputFileInfoButton->setMaximumSize(QSize(21, 21));
        genesSummaryOutputFileInfoButton->setMouseTracking(false);
        genesSummaryOutputFileInfoButton->setAcceptDrops(false);
        genesSummaryOutputFileInfoButton->setLayoutDirection(Qt::LeftToRight);
        genesSummaryOutputFileInfoButton->setIcon(icon1);
        genesSummaryOutputFileInfoButton->setAutoRepeatDelay(100);
        genesSummaryOutputFileInfoButton->setDefault(false);
        genesSummaryOutputFileInfoButton->setFlat(true);

        genesSummaryGridLayout->addWidget(genesSummaryOutputFileInfoButton, 1, 5, 1, 1);

        genesSummarySuffixLabel = new QLabel(layoutWidget_2);
        genesSummarySuffixLabel->setObjectName(QString::fromUtf8("genesSummarySuffixLabel"));

        genesSummaryGridLayout->addWidget(genesSummarySuffixLabel, 1, 4, 1, 1);

        genesSummarySpeciesLabel = new QLabel(layoutWidget_2);
        genesSummarySpeciesLabel->setObjectName(QString::fromUtf8("genesSummarySpeciesLabel"));

        genesSummaryGridLayout->addWidget(genesSummarySpeciesLabel, 2, 0, 1, 1);

        genesSummarySpeciesComboBox = new QComboBox(layoutWidget_2);
        genesSummarySpeciesComboBox->setObjectName(QString::fromUtf8("genesSummarySpeciesComboBox"));
        genesSummarySpeciesComboBox->setEnabled(true);
        genesSummarySpeciesComboBox->setEditable(false);
        genesSummarySpeciesComboBox->setModelColumn(0);

        genesSummaryGridLayout->addWidget(genesSummarySpeciesComboBox, 2, 1, 1, 1);

        genesSummaryDatabaseComboBox = new QComboBox(layoutWidget_2);
        genesSummaryDatabaseComboBox->setObjectName(QString::fromUtf8("genesSummaryDatabaseComboBox"));
        genesSummaryDatabaseComboBox->setEditable(false);
        genesSummaryDatabaseComboBox->setModelColumn(0);

        genesSummaryGridLayout->addWidget(genesSummaryDatabaseComboBox, 3, 1, 1, 1);

        genesSummaryDatabaseLabel = new QLabel(layoutWidget_2);
        genesSummaryDatabaseLabel->setObjectName(QString::fromUtf8("genesSummaryDatabaseLabel"));

        genesSummaryGridLayout->addWidget(genesSummaryDatabaseLabel, 3, 0, 1, 1);

        genesSummarySpeciesInfoButton = new QPushButton(layoutWidget_2);
        genesSummarySpeciesInfoButton->setObjectName(QString::fromUtf8("genesSummarySpeciesInfoButton"));
        genesSummarySpeciesInfoButton->setMaximumSize(QSize(21, 21));
        genesSummarySpeciesInfoButton->setMouseTracking(false);
        genesSummarySpeciesInfoButton->setAcceptDrops(false);
        genesSummarySpeciesInfoButton->setLayoutDirection(Qt::LeftToRight);
        genesSummarySpeciesInfoButton->setIcon(icon1);
        genesSummarySpeciesInfoButton->setAutoRepeatDelay(100);
        genesSummarySpeciesInfoButton->setDefault(false);
        genesSummarySpeciesInfoButton->setFlat(true);

        genesSummaryGridLayout->addWidget(genesSummarySpeciesInfoButton, 2, 2, 1, 1);

        genesSummaryDatabaseInfoButton = new QPushButton(layoutWidget_2);
        genesSummaryDatabaseInfoButton->setObjectName(QString::fromUtf8("genesSummaryDatabaseInfoButton"));
        genesSummaryDatabaseInfoButton->setMaximumSize(QSize(21, 21));
        genesSummaryDatabaseInfoButton->setMouseTracking(false);
        genesSummaryDatabaseInfoButton->setAcceptDrops(false);
        genesSummaryDatabaseInfoButton->setLayoutDirection(Qt::LeftToRight);
        genesSummaryDatabaseInfoButton->setIcon(icon1);
        genesSummaryDatabaseInfoButton->setAutoRepeatDelay(100);
        genesSummaryDatabaseInfoButton->setDefault(false);
        genesSummaryDatabaseInfoButton->setFlat(true);

        genesSummaryGridLayout->addWidget(genesSummaryDatabaseInfoButton, 3, 2, 1, 1);

        genesSummaryLenUInfoButton = new QPushButton(layoutWidget_2);
        genesSummaryLenUInfoButton->setObjectName(QString::fromUtf8("genesSummaryLenUInfoButton"));
        genesSummaryLenUInfoButton->setMaximumSize(QSize(21, 21));
        genesSummaryLenUInfoButton->setMouseTracking(false);
        genesSummaryLenUInfoButton->setAcceptDrops(false);
        genesSummaryLenUInfoButton->setLayoutDirection(Qt::LeftToRight);
        genesSummaryLenUInfoButton->setIcon(icon1);
        genesSummaryLenUInfoButton->setAutoRepeatDelay(100);
        genesSummaryLenUInfoButton->setDefault(false);
        genesSummaryLenUInfoButton->setFlat(true);

        genesSummaryGridLayout->addWidget(genesSummaryLenUInfoButton, 4, 2, 1, 1);

        genesSummaryLenDInfoButton = new QPushButton(layoutWidget_2);
        genesSummaryLenDInfoButton->setObjectName(QString::fromUtf8("genesSummaryLenDInfoButton"));
        genesSummaryLenDInfoButton->setMaximumSize(QSize(21, 21));
        genesSummaryLenDInfoButton->setMouseTracking(false);
        genesSummaryLenDInfoButton->setAcceptDrops(false);
        genesSummaryLenDInfoButton->setLayoutDirection(Qt::LeftToRight);
        genesSummaryLenDInfoButton->setIcon(icon1);
        genesSummaryLenDInfoButton->setAutoRepeatDelay(100);
        genesSummaryLenDInfoButton->setDefault(false);
        genesSummaryLenDInfoButton->setFlat(true);

        genesSummaryGridLayout->addWidget(genesSummaryLenDInfoButton, 5, 2, 1, 1);

        genesSummaryLine2 = new QFrame(genesSummaryParametersTab);
        genesSummaryLine2->setObjectName(QString::fromUtf8("genesSummaryLine2"));
        genesSummaryLine2->setGeometry(QRect(0, 280, 629, 3));
        genesSummaryLine2->setFrameShadow(QFrame::Sunken);
        genesSummaryLine2->setFrameShape(QFrame::HLine);
        layoutWidget9 = new QWidget(genesSummaryParametersTab);
        layoutWidget9->setObjectName(QString::fromUtf8("layoutWidget9"));
        layoutWidget9->setGeometry(QRect(392, 280, 241, 41));
        summaryButtonsHorizontalLayout = new QHBoxLayout(layoutWidget9);
        summaryButtonsHorizontalLayout->setSpacing(6);
        summaryButtonsHorizontalLayout->setContentsMargins(11, 11, 11, 11);
        summaryButtonsHorizontalLayout->setObjectName(QString::fromUtf8("summaryButtonsHorizontalLayout"));
        summaryButtonsHorizontalLayout->setContentsMargins(0, 0, 0, 0);
        runSummaryClearButton = new QPushButton(layoutWidget9);
        runSummaryClearButton->setObjectName(QString::fromUtf8("runSummaryClearButton"));

        summaryButtonsHorizontalLayout->addWidget(runSummaryClearButton);

        runSummaryButton = new QPushButton(layoutWidget9);
        runSummaryButton->setObjectName(QString::fromUtf8("runSummaryButton"));

        summaryButtonsHorizontalLayout->addWidget(runSummaryButton);

        genesSummaryTabWidget->addTab(genesSummaryParametersTab, QString());
        genesSummaryProgressTab = new QWidget();
        genesSummaryProgressTab->setObjectName(QString::fromUtf8("genesSummaryProgressTab"));
        genesSummaryProgressTextEdit = new QTextEdit(genesSummaryProgressTab);
        genesSummaryProgressTextEdit->setObjectName(QString::fromUtf8("genesSummaryProgressTextEdit"));
        genesSummaryProgressTextEdit->setGeometry(QRect(0, 0, 641, 501));
        genesSummaryProgressTextEdit->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);
        stopRunSummaryProcessButton = new QPushButton(genesSummaryProgressTab);
        stopRunSummaryProcessButton->setObjectName(QString::fromUtf8("stopRunSummaryProcessButton"));
        stopRunSummaryProcessButton->setEnabled(false);
        stopRunSummaryProcessButton->setGeometry(QRect(520, 510, 113, 32));
        genesSummaryTabWidget->addTab(genesSummaryProgressTab, QString());

        verticalLayout_4->addWidget(genesSummaryTabWidget);

        stackedWidget->addWidget(summaryStackedPage);
        genomicDistStackedPage = new QWidget();
        genomicDistStackedPage->setObjectName(QString::fromUtf8("genomicDistStackedPage"));
        verticalLayout_2 = new QVBoxLayout(genomicDistStackedPage);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        genomicDistTabWidget = new QTabWidget(genomicDistStackedPage);
        genomicDistTabWidget->setObjectName(QString::fromUtf8("genomicDistTabWidget"));
        genomicDistParametersTab = new QWidget();
        genomicDistParametersTab->setObjectName(QString::fromUtf8("genomicDistParametersTab"));
        genomicDistTitleLabel = new QLabel(genomicDistParametersTab);
        genomicDistTitleLabel->setObjectName(QString::fromUtf8("genomicDistTitleLabel"));
        genomicDistTitleLabel->setGeometry(QRect(0, 10, 588, 21));
        genomicDistTitleLabel->setFont(font2);
        genomicDistLine1 = new QFrame(genomicDistParametersTab);
        genomicDistLine1->setObjectName(QString::fromUtf8("genomicDistLine1"));
        genomicDistLine1->setGeometry(QRect(0, 30, 627, 3));
        genomicDistLine1->setFrameShape(QFrame::HLine);
        genomicDistLine1->setFrameShadow(QFrame::Sunken);
        layoutWidget_7 = new QWidget(genomicDistParametersTab);
        layoutWidget_7->setObjectName(QString::fromUtf8("layoutWidget_7"));
        layoutWidget_7->setGeometry(QRect(1, 40, 641, 301));
        genomicDistGridLayout = new QGridLayout(layoutWidget_7);
        genomicDistGridLayout->setSpacing(6);
        genomicDistGridLayout->setContentsMargins(11, 11, 11, 11);
        genomicDistGridLayout->setObjectName(QString::fromUtf8("genomicDistGridLayout"));
        genomicDistGridLayout->setHorizontalSpacing(-1);
        genomicDistGridLayout->setVerticalSpacing(15);
        genomicDistGridLayout->setContentsMargins(0, 0, 0, 0);
        genomicDistSelectPeaksLabel = new QLabel(layoutWidget_7);
        genomicDistSelectPeaksLabel->setObjectName(QString::fromUtf8("genomicDistSelectPeaksLabel"));

        genomicDistGridLayout->addWidget(genomicDistSelectPeaksLabel, 0, 0, 1, 1);

        genomicDistSelectPeaksLineEdit = new QLineEdit(layoutWidget_7);
        genomicDistSelectPeaksLineEdit->setObjectName(QString::fromUtf8("genomicDistSelectPeaksLineEdit"));
        genomicDistSelectPeaksLineEdit->setReadOnly(true);

        genomicDistGridLayout->addWidget(genomicDistSelectPeaksLineEdit, 0, 1, 1, 2);

        genomicDistSelectPeaksToolButton = new QToolButton(layoutWidget_7);
        genomicDistSelectPeaksToolButton->setObjectName(QString::fromUtf8("genomicDistSelectPeaksToolButton"));

        genomicDistGridLayout->addWidget(genomicDistSelectPeaksToolButton, 0, 3, 1, 1);

        genomicDistSelectPeaksInfoButton = new QPushButton(layoutWidget_7);
        genomicDistSelectPeaksInfoButton->setObjectName(QString::fromUtf8("genomicDistSelectPeaksInfoButton"));
        genomicDistSelectPeaksInfoButton->setMaximumSize(QSize(21, 21));
        genomicDistSelectPeaksInfoButton->setMouseTracking(false);
        genomicDistSelectPeaksInfoButton->setAcceptDrops(false);
        genomicDistSelectPeaksInfoButton->setLayoutDirection(Qt::LeftToRight);
        genomicDistSelectPeaksInfoButton->setIcon(icon1);
        genomicDistSelectPeaksInfoButton->setAutoRepeatDelay(100);
        genomicDistSelectPeaksInfoButton->setDefault(false);
        genomicDistSelectPeaksInfoButton->setFlat(true);

        genomicDistGridLayout->addWidget(genomicDistSelectPeaksInfoButton, 0, 4, 1, 1);

        genomicDistOutputFileLabel = new QLabel(layoutWidget_7);
        genomicDistOutputFileLabel->setObjectName(QString::fromUtf8("genomicDistOutputFileLabel"));

        genomicDistGridLayout->addWidget(genomicDistOutputFileLabel, 1, 0, 1, 1);

        genomicDistOutputFileLineEdit = new QLineEdit(layoutWidget_7);
        genomicDistOutputFileLineEdit->setObjectName(QString::fromUtf8("genomicDistOutputFileLineEdit"));

        genomicDistGridLayout->addWidget(genomicDistOutputFileLineEdit, 1, 1, 1, 2);

        genomicDistSuffixLabel = new QLabel(layoutWidget_7);
        genomicDistSuffixLabel->setObjectName(QString::fromUtf8("genomicDistSuffixLabel"));

        genomicDistGridLayout->addWidget(genomicDistSuffixLabel, 1, 3, 1, 1);

        genomicDistOutputFileInfoButton = new QPushButton(layoutWidget_7);
        genomicDistOutputFileInfoButton->setObjectName(QString::fromUtf8("genomicDistOutputFileInfoButton"));
        genomicDistOutputFileInfoButton->setMaximumSize(QSize(21, 21));
        genomicDistOutputFileInfoButton->setMouseTracking(false);
        genomicDistOutputFileInfoButton->setAcceptDrops(false);
        genomicDistOutputFileInfoButton->setLayoutDirection(Qt::LeftToRight);
        genomicDistOutputFileInfoButton->setIcon(icon1);
        genomicDistOutputFileInfoButton->setAutoRepeatDelay(100);
        genomicDistOutputFileInfoButton->setDefault(false);
        genomicDistOutputFileInfoButton->setFlat(true);

        genomicDistGridLayout->addWidget(genomicDistOutputFileInfoButton, 1, 4, 1, 1);

        genomicDistLenPLabel = new QLabel(layoutWidget_7);
        genomicDistLenPLabel->setObjectName(QString::fromUtf8("genomicDistLenPLabel"));

        genomicDistGridLayout->addWidget(genomicDistLenPLabel, 4, 0, 1, 1);

        genomicDistLenPUSpinBox = new QSpinBox(layoutWidget_7);
        genomicDistLenPUSpinBox->setObjectName(QString::fromUtf8("genomicDistLenPUSpinBox"));
        genomicDistLenPUSpinBox->setMaximum(100000);
        genomicDistLenPUSpinBox->setSingleStep(100);
        genomicDistLenPUSpinBox->setValue(2000);

        genomicDistGridLayout->addWidget(genomicDistLenPUSpinBox, 4, 1, 1, 1);

        genomicDistLenDWLabel = new QLabel(layoutWidget_7);
        genomicDistLenDWLabel->setObjectName(QString::fromUtf8("genomicDistLenDWLabel"));

        genomicDistGridLayout->addWidget(genomicDistLenDWLabel, 5, 0, 1, 1);

        genomicDistLenPDSpinBox = new QSpinBox(layoutWidget_7);
        genomicDistLenPDSpinBox->setObjectName(QString::fromUtf8("genomicDistLenPDSpinBox"));
        genomicDistLenPDSpinBox->setMaximum(100000);
        genomicDistLenPDSpinBox->setSingleStep(100);
        genomicDistLenPDSpinBox->setValue(2000);

        genomicDistGridLayout->addWidget(genomicDistLenPDSpinBox, 4, 2, 1, 1);

        genomicDistLenPInfoButton = new QPushButton(layoutWidget_7);
        genomicDistLenPInfoButton->setObjectName(QString::fromUtf8("genomicDistLenPInfoButton"));
        genomicDistLenPInfoButton->setMaximumSize(QSize(21, 21));
        genomicDistLenPInfoButton->setMouseTracking(false);
        genomicDistLenPInfoButton->setAcceptDrops(false);
        genomicDistLenPInfoButton->setLayoutDirection(Qt::LeftToRight);
        genomicDistLenPInfoButton->setIcon(icon1);
        genomicDistLenPInfoButton->setAutoRepeatDelay(100);
        genomicDistLenPInfoButton->setDefault(false);
        genomicDistLenPInfoButton->setFlat(true);

        genomicDistGridLayout->addWidget(genomicDistLenPInfoButton, 4, 4, 1, 1);

        genomicDistLenDWInfoButton = new QPushButton(layoutWidget_7);
        genomicDistLenDWInfoButton->setObjectName(QString::fromUtf8("genomicDistLenDWInfoButton"));
        genomicDistLenDWInfoButton->setMaximumSize(QSize(21, 21));
        genomicDistLenDWInfoButton->setMouseTracking(false);
        genomicDistLenDWInfoButton->setAcceptDrops(false);
        genomicDistLenDWInfoButton->setLayoutDirection(Qt::LeftToRight);
        genomicDistLenDWInfoButton->setIcon(icon1);
        genomicDistLenDWInfoButton->setAutoRepeatDelay(100);
        genomicDistLenDWInfoButton->setDefault(false);
        genomicDistLenDWInfoButton->setFlat(true);

        genomicDistGridLayout->addWidget(genomicDistLenDWInfoButton, 5, 4, 1, 1);

        genomicDistLenDWUSpinBox = new QSpinBox(layoutWidget_7);
        genomicDistLenDWUSpinBox->setObjectName(QString::fromUtf8("genomicDistLenDWUSpinBox"));
        genomicDistLenDWUSpinBox->setMaximum(100000);
        genomicDistLenDWUSpinBox->setSingleStep(100);
        genomicDistLenDWUSpinBox->setValue(2000);

        genomicDistGridLayout->addWidget(genomicDistLenDWUSpinBox, 5, 1, 1, 1);

        genomicDistLenDWDSpinBox = new QSpinBox(layoutWidget_7);
        genomicDistLenDWDSpinBox->setObjectName(QString::fromUtf8("genomicDistLenDWDSpinBox"));
        genomicDistLenDWDSpinBox->setMaximum(100000);
        genomicDistLenDWDSpinBox->setSingleStep(100);
        genomicDistLenDWDSpinBox->setValue(2000);

        genomicDistGridLayout->addWidget(genomicDistLenDWDSpinBox, 5, 2, 1, 1);

        genomicDistTypeInfoButton = new QPushButton(layoutWidget_7);
        genomicDistTypeInfoButton->setObjectName(QString::fromUtf8("genomicDistTypeInfoButton"));
        genomicDistTypeInfoButton->setMaximumSize(QSize(21, 21));
        genomicDistTypeInfoButton->setMouseTracking(false);
        genomicDistTypeInfoButton->setAcceptDrops(false);
        genomicDistTypeInfoButton->setLayoutDirection(Qt::LeftToRight);
        genomicDistTypeInfoButton->setIcon(icon1);
        genomicDistTypeInfoButton->setAutoRepeatDelay(100);
        genomicDistTypeInfoButton->setDefault(false);
        genomicDistTypeInfoButton->setFlat(true);

        genomicDistGridLayout->addWidget(genomicDistTypeInfoButton, 7, 4, 1, 1);

        genomicDistTypeLabel = new QLabel(layoutWidget_7);
        genomicDistTypeLabel->setObjectName(QString::fromUtf8("genomicDistTypeLabel"));

        genomicDistGridLayout->addWidget(genomicDistTypeLabel, 7, 0, 1, 1);

        genomicDistMindistawayLabel = new QLabel(layoutWidget_7);
        genomicDistMindistawayLabel->setObjectName(QString::fromUtf8("genomicDistMindistawayLabel"));

        genomicDistGridLayout->addWidget(genomicDistMindistawayLabel, 6, 0, 1, 1);

        genomicDistMindistawaySpinBox = new QSpinBox(layoutWidget_7);
        genomicDistMindistawaySpinBox->setObjectName(QString::fromUtf8("genomicDistMindistawaySpinBox"));
        genomicDistMindistawaySpinBox->setMaximum(100000);
        genomicDistMindistawaySpinBox->setSingleStep(100);
        genomicDistMindistawaySpinBox->setValue(2000);

        genomicDistGridLayout->addWidget(genomicDistMindistawaySpinBox, 6, 1, 1, 1);

        genomicDistMindistawayInfoButton = new QPushButton(layoutWidget_7);
        genomicDistMindistawayInfoButton->setObjectName(QString::fromUtf8("genomicDistMindistawayInfoButton"));
        genomicDistMindistawayInfoButton->setMaximumSize(QSize(21, 21));
        genomicDistMindistawayInfoButton->setMouseTracking(false);
        genomicDistMindistawayInfoButton->setAcceptDrops(false);
        genomicDistMindistawayInfoButton->setLayoutDirection(Qt::LeftToRight);
        genomicDistMindistawayInfoButton->setIcon(icon1);
        genomicDistMindistawayInfoButton->setAutoRepeatDelay(100);
        genomicDistMindistawayInfoButton->setDefault(false);
        genomicDistMindistawayInfoButton->setFlat(true);

        genomicDistGridLayout->addWidget(genomicDistMindistawayInfoButton, 6, 4, 1, 1);

        genomicDistMaxdistawaySpinBox = new QSpinBox(layoutWidget_7);
        genomicDistMaxdistawaySpinBox->setObjectName(QString::fromUtf8("genomicDistMaxdistawaySpinBox"));
        genomicDistMaxdistawaySpinBox->setEnabled(true);
        genomicDistMaxdistawaySpinBox->setMaximum(100000);
        genomicDistMaxdistawaySpinBox->setSingleStep(100);
        genomicDistMaxdistawaySpinBox->setValue(50000);

        genomicDistGridLayout->addWidget(genomicDistMaxdistawaySpinBox, 6, 2, 1, 1);

        peakBasedRadioButton = new QRadioButton(layoutWidget_7);
        peakBasedRadioButton->setObjectName(QString::fromUtf8("peakBasedRadioButton"));
        peakBasedRadioButton->setChecked(false);

        genomicDistGridLayout->addWidget(peakBasedRadioButton, 7, 1, 1, 1);

        geneBasedRadioButton = new QRadioButton(layoutWidget_7);
        geneBasedRadioButton->setObjectName(QString::fromUtf8("geneBasedRadioButton"));
        geneBasedRadioButton->setChecked(false);

        genomicDistGridLayout->addWidget(geneBasedRadioButton, 7, 2, 1, 1);

        genomicDistSpeciesLabel = new QLabel(layoutWidget_7);
        genomicDistSpeciesLabel->setObjectName(QString::fromUtf8("genomicDistSpeciesLabel"));

        genomicDistGridLayout->addWidget(genomicDistSpeciesLabel, 2, 0, 1, 1);

        genomicDistSpeciesComboBox = new QComboBox(layoutWidget_7);
        genomicDistSpeciesComboBox->setObjectName(QString::fromUtf8("genomicDistSpeciesComboBox"));
        genomicDistSpeciesComboBox->setEnabled(true);
        genomicDistSpeciesComboBox->setEditable(false);
        genomicDistSpeciesComboBox->setModelColumn(0);

        genomicDistGridLayout->addWidget(genomicDistSpeciesComboBox, 2, 1, 1, 2);

        genomicDistSpeciesInfoButton = new QPushButton(layoutWidget_7);
        genomicDistSpeciesInfoButton->setObjectName(QString::fromUtf8("genomicDistSpeciesInfoButton"));
        genomicDistSpeciesInfoButton->setMaximumSize(QSize(21, 21));
        genomicDistSpeciesInfoButton->setMouseTracking(false);
        genomicDistSpeciesInfoButton->setAcceptDrops(false);
        genomicDistSpeciesInfoButton->setLayoutDirection(Qt::LeftToRight);
        genomicDistSpeciesInfoButton->setIcon(icon1);
        genomicDistSpeciesInfoButton->setAutoRepeatDelay(100);
        genomicDistSpeciesInfoButton->setDefault(false);
        genomicDistSpeciesInfoButton->setFlat(true);

        genomicDistGridLayout->addWidget(genomicDistSpeciesInfoButton, 2, 4, 1, 1);

        genomicDistDatabaseLabel = new QLabel(layoutWidget_7);
        genomicDistDatabaseLabel->setObjectName(QString::fromUtf8("genomicDistDatabaseLabel"));

        genomicDistGridLayout->addWidget(genomicDistDatabaseLabel, 3, 0, 1, 1);

        genomicDistDatabaseComboBox = new QComboBox(layoutWidget_7);
        genomicDistDatabaseComboBox->setObjectName(QString::fromUtf8("genomicDistDatabaseComboBox"));
        genomicDistDatabaseComboBox->setEditable(false);
        genomicDistDatabaseComboBox->setModelColumn(0);

        genomicDistGridLayout->addWidget(genomicDistDatabaseComboBox, 3, 1, 1, 2);

        genomicDistDatabaseInfoButton = new QPushButton(layoutWidget_7);
        genomicDistDatabaseInfoButton->setObjectName(QString::fromUtf8("genomicDistDatabaseInfoButton"));
        genomicDistDatabaseInfoButton->setMaximumSize(QSize(21, 21));
        genomicDistDatabaseInfoButton->setMouseTracking(false);
        genomicDistDatabaseInfoButton->setAcceptDrops(false);
        genomicDistDatabaseInfoButton->setLayoutDirection(Qt::LeftToRight);
        genomicDistDatabaseInfoButton->setIcon(icon1);
        genomicDistDatabaseInfoButton->setAutoRepeatDelay(100);
        genomicDistDatabaseInfoButton->setDefault(false);
        genomicDistDatabaseInfoButton->setFlat(true);

        genomicDistGridLayout->addWidget(genomicDistDatabaseInfoButton, 3, 4, 1, 1);

        genomicDistLine2 = new QFrame(genomicDistParametersTab);
        genomicDistLine2->setObjectName(QString::fromUtf8("genomicDistLine2"));
        genomicDistLine2->setGeometry(QRect(0, 340, 629, 3));
        genomicDistLine2->setFrameShadow(QFrame::Sunken);
        genomicDistLine2->setFrameShape(QFrame::HLine);
        layoutWidget_8 = new QWidget(genomicDistParametersTab);
        layoutWidget_8->setObjectName(QString::fromUtf8("layoutWidget_8"));
        layoutWidget_8->setGeometry(QRect(390, 340, 251, 41));
        genomicDistButtonsHorizontalLayout = new QHBoxLayout(layoutWidget_8);
        genomicDistButtonsHorizontalLayout->setSpacing(6);
        genomicDistButtonsHorizontalLayout->setContentsMargins(11, 11, 11, 11);
        genomicDistButtonsHorizontalLayout->setObjectName(QString::fromUtf8("genomicDistButtonsHorizontalLayout"));
        genomicDistButtonsHorizontalLayout->setContentsMargins(0, 0, 0, 0);
        runGenomicDistClearButton = new QPushButton(layoutWidget_8);
        runGenomicDistClearButton->setObjectName(QString::fromUtf8("runGenomicDistClearButton"));

        genomicDistButtonsHorizontalLayout->addWidget(runGenomicDistClearButton);

        runGenomicDistButton = new QPushButton(layoutWidget_8);
        runGenomicDistButton->setObjectName(QString::fromUtf8("runGenomicDistButton"));

        genomicDistButtonsHorizontalLayout->addWidget(runGenomicDistButton);

        genomicDistTabWidget->addTab(genomicDistParametersTab, QString());
        genomicDistProgressTab = new QWidget();
        genomicDistProgressTab->setObjectName(QString::fromUtf8("genomicDistProgressTab"));
        genomicDistProgressTextEdit = new QTextEdit(genomicDistProgressTab);
        genomicDistProgressTextEdit->setObjectName(QString::fromUtf8("genomicDistProgressTextEdit"));
        genomicDistProgressTextEdit->setGeometry(QRect(0, 0, 641, 501));
        genomicDistProgressTextEdit->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);
        stopRunGenomicDistProcessButton = new QPushButton(genomicDistProgressTab);
        stopRunGenomicDistProcessButton->setObjectName(QString::fromUtf8("stopRunGenomicDistProcessButton"));
        stopRunGenomicDistProcessButton->setEnabled(false);
        stopRunGenomicDistProcessButton->setGeometry(QRect(520, 510, 113, 32));
        genomicDistTabWidget->addTab(genomicDistProgressTab, QString());

        verticalLayout_2->addWidget(genomicDistTabWidget);

        stackedWidget->addWidget(genomicDistStackedPage);
        rnaGenesStackedPage = new QWidget();
        rnaGenesStackedPage->setObjectName(QString::fromUtf8("rnaGenesStackedPage"));
        verticalLayout_17 = new QVBoxLayout(rnaGenesStackedPage);
        verticalLayout_17->setSpacing(6);
        verticalLayout_17->setContentsMargins(11, 11, 11, 11);
        verticalLayout_17->setObjectName(QString::fromUtf8("verticalLayout_17"));
        rnaGenesTabWidget = new QTabWidget(rnaGenesStackedPage);
        rnaGenesTabWidget->setObjectName(QString::fromUtf8("rnaGenesTabWidget"));
        rnaGenesParametersTab = new QWidget();
        rnaGenesParametersTab->setObjectName(QString::fromUtf8("rnaGenesParametersTab"));
        rnaGenesTitleLabel = new QLabel(rnaGenesParametersTab);
        rnaGenesTitleLabel->setObjectName(QString::fromUtf8("rnaGenesTitleLabel"));
        rnaGenesTitleLabel->setGeometry(QRect(0, 10, 588, 21));
        rnaGenesTitleLabel->setFont(font2);
        rnaGenesLine1 = new QFrame(rnaGenesParametersTab);
        rnaGenesLine1->setObjectName(QString::fromUtf8("rnaGenesLine1"));
        rnaGenesLine1->setGeometry(QRect(0, 30, 627, 3));
        rnaGenesLine1->setFrameShape(QFrame::HLine);
        rnaGenesLine1->setFrameShadow(QFrame::Sunken);
        layoutWidget_19 = new QWidget(rnaGenesParametersTab);
        layoutWidget_19->setObjectName(QString::fromUtf8("layoutWidget_19"));
        layoutWidget_19->setGeometry(QRect(0, 40, 641, 81));
        rnaGenesGridLayout = new QGridLayout(layoutWidget_19);
        rnaGenesGridLayout->setSpacing(6);
        rnaGenesGridLayout->setContentsMargins(11, 11, 11, 11);
        rnaGenesGridLayout->setObjectName(QString::fromUtf8("rnaGenesGridLayout"));
        rnaGenesGridLayout->setHorizontalSpacing(-1);
        rnaGenesGridLayout->setVerticalSpacing(15);
        rnaGenesGridLayout->setContentsMargins(0, 0, 0, 0);
        rnaGenesSelectPeaksLabel = new QLabel(layoutWidget_19);
        rnaGenesSelectPeaksLabel->setObjectName(QString::fromUtf8("rnaGenesSelectPeaksLabel"));

        rnaGenesGridLayout->addWidget(rnaGenesSelectPeaksLabel, 0, 0, 1, 1);

        rnaGenesSelectPeaksLineEdit = new QLineEdit(layoutWidget_19);
        rnaGenesSelectPeaksLineEdit->setObjectName(QString::fromUtf8("rnaGenesSelectPeaksLineEdit"));
        rnaGenesSelectPeaksLineEdit->setReadOnly(true);

        rnaGenesGridLayout->addWidget(rnaGenesSelectPeaksLineEdit, 0, 1, 1, 2);

        rnaGenesSelectPeaksToolButton = new QToolButton(layoutWidget_19);
        rnaGenesSelectPeaksToolButton->setObjectName(QString::fromUtf8("rnaGenesSelectPeaksToolButton"));

        rnaGenesGridLayout->addWidget(rnaGenesSelectPeaksToolButton, 0, 3, 1, 1);

        rnaGenesSelectPeaksInfoButton = new QPushButton(layoutWidget_19);
        rnaGenesSelectPeaksInfoButton->setObjectName(QString::fromUtf8("rnaGenesSelectPeaksInfoButton"));
        rnaGenesSelectPeaksInfoButton->setMaximumSize(QSize(21, 21));
        rnaGenesSelectPeaksInfoButton->setMouseTracking(false);
        rnaGenesSelectPeaksInfoButton->setAcceptDrops(false);
        rnaGenesSelectPeaksInfoButton->setLayoutDirection(Qt::LeftToRight);
        rnaGenesSelectPeaksInfoButton->setIcon(icon1);
        rnaGenesSelectPeaksInfoButton->setAutoRepeatDelay(100);
        rnaGenesSelectPeaksInfoButton->setDefault(false);
        rnaGenesSelectPeaksInfoButton->setFlat(true);

        rnaGenesGridLayout->addWidget(rnaGenesSelectPeaksInfoButton, 0, 4, 1, 1);

        rnaGenesOutputFileLabel = new QLabel(layoutWidget_19);
        rnaGenesOutputFileLabel->setObjectName(QString::fromUtf8("rnaGenesOutputFileLabel"));

        rnaGenesGridLayout->addWidget(rnaGenesOutputFileLabel, 1, 0, 1, 1);

        rnaGenesOutputFileLineEdit = new QLineEdit(layoutWidget_19);
        rnaGenesOutputFileLineEdit->setObjectName(QString::fromUtf8("rnaGenesOutputFileLineEdit"));

        rnaGenesGridLayout->addWidget(rnaGenesOutputFileLineEdit, 1, 1, 1, 2);

        rnaGenesSuffixLabel = new QLabel(layoutWidget_19);
        rnaGenesSuffixLabel->setObjectName(QString::fromUtf8("rnaGenesSuffixLabel"));

        rnaGenesGridLayout->addWidget(rnaGenesSuffixLabel, 1, 3, 1, 1);

        rnaGenesOutputFileInfoButton = new QPushButton(layoutWidget_19);
        rnaGenesOutputFileInfoButton->setObjectName(QString::fromUtf8("rnaGenesOutputFileInfoButton"));
        rnaGenesOutputFileInfoButton->setMaximumSize(QSize(21, 21));
        rnaGenesOutputFileInfoButton->setMouseTracking(false);
        rnaGenesOutputFileInfoButton->setAcceptDrops(false);
        rnaGenesOutputFileInfoButton->setLayoutDirection(Qt::LeftToRight);
        rnaGenesOutputFileInfoButton->setIcon(icon1);
        rnaGenesOutputFileInfoButton->setAutoRepeatDelay(100);
        rnaGenesOutputFileInfoButton->setDefault(false);
        rnaGenesOutputFileInfoButton->setFlat(true);

        rnaGenesGridLayout->addWidget(rnaGenesOutputFileInfoButton, 1, 4, 1, 1);

        rnaGenesLine2 = new QFrame(rnaGenesParametersTab);
        rnaGenesLine2->setObjectName(QString::fromUtf8("rnaGenesLine2"));
        rnaGenesLine2->setGeometry(QRect(0, 110, 631, 21));
        rnaGenesLine2->setFrameShadow(QFrame::Sunken);
        rnaGenesLine2->setFrameShape(QFrame::HLine);
        layoutWidget_20 = new QWidget(rnaGenesParametersTab);
        layoutWidget_20->setObjectName(QString::fromUtf8("layoutWidget_20"));
        layoutWidget_20->setGeometry(QRect(390, 120, 251, 41));
        rnaGenesButtonsHorizontalLayout = new QHBoxLayout(layoutWidget_20);
        rnaGenesButtonsHorizontalLayout->setSpacing(6);
        rnaGenesButtonsHorizontalLayout->setContentsMargins(11, 11, 11, 11);
        rnaGenesButtonsHorizontalLayout->setObjectName(QString::fromUtf8("rnaGenesButtonsHorizontalLayout"));
        rnaGenesButtonsHorizontalLayout->setContentsMargins(0, 0, 0, 0);
        runFindRNAGenesClearButton = new QPushButton(layoutWidget_20);
        runFindRNAGenesClearButton->setObjectName(QString::fromUtf8("runFindRNAGenesClearButton"));

        rnaGenesButtonsHorizontalLayout->addWidget(runFindRNAGenesClearButton);

        runFindRNAGenesButton = new QPushButton(layoutWidget_20);
        runFindRNAGenesButton->setObjectName(QString::fromUtf8("runFindRNAGenesButton"));

        rnaGenesButtonsHorizontalLayout->addWidget(runFindRNAGenesButton);

        rnaGenesTabWidget->addTab(rnaGenesParametersTab, QString());
        rnaGenesProgressTab = new QWidget();
        rnaGenesProgressTab->setObjectName(QString::fromUtf8("rnaGenesProgressTab"));
        rnaGenesProgressTextEdit = new QTextEdit(rnaGenesProgressTab);
        rnaGenesProgressTextEdit->setObjectName(QString::fromUtf8("rnaGenesProgressTextEdit"));
        rnaGenesProgressTextEdit->setGeometry(QRect(0, 0, 641, 501));
        rnaGenesProgressTextEdit->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);
        stopRunFindRNAGenesProcessButton = new QPushButton(rnaGenesProgressTab);
        stopRunFindRNAGenesProcessButton->setObjectName(QString::fromUtf8("stopRunFindRNAGenesProcessButton"));
        stopRunFindRNAGenesProcessButton->setEnabled(false);
        stopRunFindRNAGenesProcessButton->setGeometry(QRect(520, 510, 113, 32));
        rnaGenesTabWidget->addTab(rnaGenesProgressTab, QString());

        verticalLayout_17->addWidget(rnaGenesTabWidget);

        stackedWidget->addWidget(rnaGenesStackedPage);
        encodeStackedPage = new QWidget();
        encodeStackedPage->setObjectName(QString::fromUtf8("encodeStackedPage"));
        verticalLayout_16 = new QVBoxLayout(encodeStackedPage);
        verticalLayout_16->setSpacing(6);
        verticalLayout_16->setContentsMargins(11, 11, 11, 11);
        verticalLayout_16->setObjectName(QString::fromUtf8("verticalLayout_16"));
        encodeTabWidget = new QTabWidget(encodeStackedPage);
        encodeTabWidget->setObjectName(QString::fromUtf8("encodeTabWidget"));
        encodeParametersTab = new QWidget();
        encodeParametersTab->setObjectName(QString::fromUtf8("encodeParametersTab"));
        encodeLine1 = new QFrame(encodeParametersTab);
        encodeLine1->setObjectName(QString::fromUtf8("encodeLine1"));
        encodeLine1->setGeometry(QRect(0, 30, 627, 3));
        encodeLine1->setFrameShape(QFrame::HLine);
        encodeLine1->setFrameShadow(QFrame::Sunken);
        layoutWidget_35 = new QWidget(encodeParametersTab);
        layoutWidget_35->setObjectName(QString::fromUtf8("layoutWidget_35"));
        layoutWidget_35->setGeometry(QRect(0, 40, 641, 281));
        encodeGridLayout = new QGridLayout(layoutWidget_35);
        encodeGridLayout->setSpacing(6);
        encodeGridLayout->setContentsMargins(11, 11, 11, 11);
        encodeGridLayout->setObjectName(QString::fromUtf8("encodeGridLayout"));
        encodeGridLayout->setHorizontalSpacing(-1);
        encodeGridLayout->setVerticalSpacing(15);
        encodeGridLayout->setContentsMargins(0, 0, 0, 0);
        encodeSpeciesComboBox = new QComboBox(layoutWidget_35);
        encodeSpeciesComboBox->setObjectName(QString::fromUtf8("encodeSpeciesComboBox"));
        encodeSpeciesComboBox->setEnabled(true);
        encodeSpeciesComboBox->setEditable(false);
        encodeSpeciesComboBox->setModelColumn(0);

        encodeGridLayout->addWidget(encodeSpeciesComboBox, 2, 1, 1, 2);

        encodeSelectPeaksLabel = new QLabel(layoutWidget_35);
        encodeSelectPeaksLabel->setObjectName(QString::fromUtf8("encodeSelectPeaksLabel"));

        encodeGridLayout->addWidget(encodeSelectPeaksLabel, 0, 0, 1, 1);

        encodeSelectPeaksLineEdit = new QLineEdit(layoutWidget_35);
        encodeSelectPeaksLineEdit->setObjectName(QString::fromUtf8("encodeSelectPeaksLineEdit"));
        encodeSelectPeaksLineEdit->setReadOnly(true);

        encodeGridLayout->addWidget(encodeSelectPeaksLineEdit, 0, 1, 1, 2);

        encodeSelectPeaksInfoButton = new QPushButton(layoutWidget_35);
        encodeSelectPeaksInfoButton->setObjectName(QString::fromUtf8("encodeSelectPeaksInfoButton"));
        encodeSelectPeaksInfoButton->setMaximumSize(QSize(21, 21));
        encodeSelectPeaksInfoButton->setMouseTracking(false);
        encodeSelectPeaksInfoButton->setAcceptDrops(false);
        encodeSelectPeaksInfoButton->setLayoutDirection(Qt::LeftToRight);
        encodeSelectPeaksInfoButton->setIcon(icon1);
        encodeSelectPeaksInfoButton->setAutoRepeatDelay(100);
        encodeSelectPeaksInfoButton->setDefault(false);
        encodeSelectPeaksInfoButton->setFlat(true);

        encodeGridLayout->addWidget(encodeSelectPeaksInfoButton, 0, 4, 1, 1);

        encodePrefixFileLabel = new QLabel(layoutWidget_35);
        encodePrefixFileLabel->setObjectName(QString::fromUtf8("encodePrefixFileLabel"));

        encodeGridLayout->addWidget(encodePrefixFileLabel, 1, 0, 1, 1);

        encodePrefixFileLineEdit = new QLineEdit(layoutWidget_35);
        encodePrefixFileLineEdit->setObjectName(QString::fromUtf8("encodePrefixFileLineEdit"));

        encodeGridLayout->addWidget(encodePrefixFileLineEdit, 1, 1, 1, 2);

        encodePrefixFileInfoButton = new QPushButton(layoutWidget_35);
        encodePrefixFileInfoButton->setObjectName(QString::fromUtf8("encodePrefixFileInfoButton"));
        encodePrefixFileInfoButton->setMaximumSize(QSize(21, 21));
        encodePrefixFileInfoButton->setMouseTracking(false);
        encodePrefixFileInfoButton->setAcceptDrops(false);
        encodePrefixFileInfoButton->setLayoutDirection(Qt::LeftToRight);
        encodePrefixFileInfoButton->setIcon(icon1);
        encodePrefixFileInfoButton->setAutoRepeatDelay(100);
        encodePrefixFileInfoButton->setDefault(false);
        encodePrefixFileInfoButton->setFlat(true);

        encodeGridLayout->addWidget(encodePrefixFileInfoButton, 1, 4, 1, 1);

        encodeSelectDatasetRadioButton = new QRadioButton(layoutWidget_35);
        encodeSelectDatasetRadioButton->setObjectName(QString::fromUtf8("encodeSelectDatasetRadioButton"));
        encodeSelectDatasetRadioButton->setChecked(false);

        encodeGridLayout->addWidget(encodeSelectDatasetRadioButton, 4, 0, 1, 1);

        encodeSelectDatasetInfoButton = new QPushButton(layoutWidget_35);
        encodeSelectDatasetInfoButton->setObjectName(QString::fromUtf8("encodeSelectDatasetInfoButton"));
        encodeSelectDatasetInfoButton->setMaximumSize(QSize(21, 21));
        encodeSelectDatasetInfoButton->setMouseTracking(false);
        encodeSelectDatasetInfoButton->setAcceptDrops(false);
        encodeSelectDatasetInfoButton->setLayoutDirection(Qt::LeftToRight);
        encodeSelectDatasetInfoButton->setIcon(icon1);
        encodeSelectDatasetInfoButton->setAutoRepeatDelay(100);
        encodeSelectDatasetInfoButton->setDefault(false);
        encodeSelectDatasetInfoButton->setFlat(true);

        encodeGridLayout->addWidget(encodeSelectDatasetInfoButton, 4, 4, 1, 1);

        encodeWriteDatasetRadioButton = new QRadioButton(layoutWidget_35);
        encodeWriteDatasetRadioButton->setObjectName(QString::fromUtf8("encodeWriteDatasetRadioButton"));
        encodeWriteDatasetRadioButton->setChecked(true);

        encodeGridLayout->addWidget(encodeWriteDatasetRadioButton, 3, 0, 1, 1);

        encodeWriteDatasetLineEdit = new QLineEdit(layoutWidget_35);
        encodeWriteDatasetLineEdit->setObjectName(QString::fromUtf8("encodeWriteDatasetLineEdit"));
        encodeWriteDatasetLineEdit->setEnabled(true);

        encodeGridLayout->addWidget(encodeWriteDatasetLineEdit, 3, 1, 1, 2);

        encodeWriteDatasetInfoButton = new QPushButton(layoutWidget_35);
        encodeWriteDatasetInfoButton->setObjectName(QString::fromUtf8("encodeWriteDatasetInfoButton"));
        encodeWriteDatasetInfoButton->setMaximumSize(QSize(21, 21));
        encodeWriteDatasetInfoButton->setMouseTracking(false);
        encodeWriteDatasetInfoButton->setAcceptDrops(false);
        encodeWriteDatasetInfoButton->setLayoutDirection(Qt::LeftToRight);
        encodeWriteDatasetInfoButton->setIcon(icon1);
        encodeWriteDatasetInfoButton->setAutoRepeatDelay(100);
        encodeWriteDatasetInfoButton->setDefault(false);
        encodeWriteDatasetInfoButton->setFlat(true);

        encodeGridLayout->addWidget(encodeWriteDatasetInfoButton, 3, 4, 1, 1);

        encodeSelectDatasetComboBox = new QComboBox(layoutWidget_35);
        encodeSelectDatasetComboBox->setObjectName(QString::fromUtf8("encodeSelectDatasetComboBox"));
        encodeSelectDatasetComboBox->setEnabled(false);
        encodeSelectDatasetComboBox->setEditable(false);
        encodeSelectDatasetComboBox->setModelColumn(0);

        encodeGridLayout->addWidget(encodeSelectDatasetComboBox, 4, 1, 1, 2);

        encodeIterationsSpinBox = new QSpinBox(layoutWidget_35);
        encodeIterationsSpinBox->setObjectName(QString::fromUtf8("encodeIterationsSpinBox"));
        encodeIterationsSpinBox->setMinimum(1);
        encodeIterationsSpinBox->setMaximum(10000);
        encodeIterationsSpinBox->setSingleStep(10);
        encodeIterationsSpinBox->setValue(100);

        encodeGridLayout->addWidget(encodeIterationsSpinBox, 5, 1, 1, 1);

        encodeSelectPeaksToolButton = new QToolButton(layoutWidget_35);
        encodeSelectPeaksToolButton->setObjectName(QString::fromUtf8("encodeSelectPeaksToolButton"));

        encodeGridLayout->addWidget(encodeSelectPeaksToolButton, 0, 3, 1, 1);

        encodeIterationsLabel = new QLabel(layoutWidget_35);
        encodeIterationsLabel->setObjectName(QString::fromUtf8("encodeIterationsLabel"));

        encodeGridLayout->addWidget(encodeIterationsLabel, 5, 0, 1, 1);

        encodeIterationsInfoButton = new QPushButton(layoutWidget_35);
        encodeIterationsInfoButton->setObjectName(QString::fromUtf8("encodeIterationsInfoButton"));
        encodeIterationsInfoButton->setMaximumSize(QSize(21, 21));
        encodeIterationsInfoButton->setMouseTracking(false);
        encodeIterationsInfoButton->setAcceptDrops(false);
        encodeIterationsInfoButton->setLayoutDirection(Qt::LeftToRight);
        encodeIterationsInfoButton->setIcon(icon1);
        encodeIterationsInfoButton->setAutoRepeatDelay(100);
        encodeIterationsInfoButton->setDefault(false);
        encodeIterationsInfoButton->setFlat(true);

        encodeGridLayout->addWidget(encodeIterationsInfoButton, 5, 4, 1, 1);

        encodeALLCheckBox = new QCheckBox(layoutWidget_35);
        encodeALLCheckBox->setObjectName(QString::fromUtf8("encodeALLCheckBox"));

        encodeGridLayout->addWidget(encodeALLCheckBox, 6, 0, 1, 1);

        encodeALLInfoButton = new QPushButton(layoutWidget_35);
        encodeALLInfoButton->setObjectName(QString::fromUtf8("encodeALLInfoButton"));
        encodeALLInfoButton->setMaximumSize(QSize(21, 21));
        encodeALLInfoButton->setMouseTracking(false);
        encodeALLInfoButton->setAcceptDrops(false);
        encodeALLInfoButton->setLayoutDirection(Qt::LeftToRight);
        encodeALLInfoButton->setIcon(icon1);
        encodeALLInfoButton->setAutoRepeatDelay(100);
        encodeALLInfoButton->setDefault(false);
        encodeALLInfoButton->setFlat(true);

        encodeGridLayout->addWidget(encodeALLInfoButton, 6, 4, 1, 1);

        encodeSpeciesLabel = new QLabel(layoutWidget_35);
        encodeSpeciesLabel->setObjectName(QString::fromUtf8("encodeSpeciesLabel"));

        encodeGridLayout->addWidget(encodeSpeciesLabel, 2, 0, 1, 1);

        encodeSpeciesInfoButton = new QPushButton(layoutWidget_35);
        encodeSpeciesInfoButton->setObjectName(QString::fromUtf8("encodeSpeciesInfoButton"));
        encodeSpeciesInfoButton->setMaximumSize(QSize(21, 21));
        encodeSpeciesInfoButton->setMouseTracking(false);
        encodeSpeciesInfoButton->setAcceptDrops(false);
        encodeSpeciesInfoButton->setLayoutDirection(Qt::LeftToRight);
        encodeSpeciesInfoButton->setIcon(icon1);
        encodeSpeciesInfoButton->setAutoRepeatDelay(100);
        encodeSpeciesInfoButton->setDefault(false);
        encodeSpeciesInfoButton->setFlat(true);

        encodeGridLayout->addWidget(encodeSpeciesInfoButton, 2, 4, 1, 1);

        encodeLine2 = new QFrame(encodeParametersTab);
        encodeLine2->setObjectName(QString::fromUtf8("encodeLine2"));
        encodeLine2->setGeometry(QRect(0, 320, 629, 3));
        encodeLine2->setFrameShadow(QFrame::Sunken);
        encodeLine2->setFrameShape(QFrame::HLine);
        layoutWidget_36 = new QWidget(encodeParametersTab);
        layoutWidget_36->setObjectName(QString::fromUtf8("layoutWidget_36"));
        layoutWidget_36->setGeometry(QRect(380, 320, 261, 41));
        encodeButtonsHorizontalLayout = new QHBoxLayout(layoutWidget_36);
        encodeButtonsHorizontalLayout->setSpacing(6);
        encodeButtonsHorizontalLayout->setContentsMargins(11, 11, 11, 11);
        encodeButtonsHorizontalLayout->setObjectName(QString::fromUtf8("encodeButtonsHorizontalLayout"));
        encodeButtonsHorizontalLayout->setContentsMargins(0, 0, 0, 0);
        runEncodeClearButton = new QPushButton(layoutWidget_36);
        runEncodeClearButton->setObjectName(QString::fromUtf8("runEncodeClearButton"));

        encodeButtonsHorizontalLayout->addWidget(runEncodeClearButton);

        runEncodeButton = new QPushButton(layoutWidget_36);
        runEncodeButton->setObjectName(QString::fromUtf8("runEncodeButton"));

        encodeButtonsHorizontalLayout->addWidget(runEncodeButton);

        encodeTitleLabel = new QLabel(encodeParametersTab);
        encodeTitleLabel->setObjectName(QString::fromUtf8("encodeTitleLabel"));
        encodeTitleLabel->setGeometry(QRect(0, 10, 588, 21));
        encodeTitleLabel->setFont(font2);
        encodeTabWidget->addTab(encodeParametersTab, QString());
        encodeProgressTab = new QWidget();
        encodeProgressTab->setObjectName(QString::fromUtf8("encodeProgressTab"));
        encodeProgressTextEdit = new QTextEdit(encodeProgressTab);
        encodeProgressTextEdit->setObjectName(QString::fromUtf8("encodeProgressTextEdit"));
        encodeProgressTextEdit->setGeometry(QRect(0, 0, 641, 501));
        encodeProgressTextEdit->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);
        stopRunEncodeProcessButton = new QPushButton(encodeProgressTab);
        stopRunEncodeProcessButton->setObjectName(QString::fromUtf8("stopRunEncodeProcessButton"));
        stopRunEncodeProcessButton->setEnabled(false);
        stopRunEncodeProcessButton->setGeometry(QRect(520, 510, 113, 32));
        encodeTabWidget->addTab(encodeProgressTab, QString());

        verticalLayout_16->addWidget(encodeTabWidget);

        stackedWidget->addWidget(encodeStackedPage);
        createTracksStackedPage = new QWidget();
        createTracksStackedPage->setObjectName(QString::fromUtf8("createTracksStackedPage"));
        verticalLayout = new QVBoxLayout(createTracksStackedPage);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        createTracksTabWidget = new QTabWidget(createTracksStackedPage);
        createTracksTabWidget->setObjectName(QString::fromUtf8("createTracksTabWidget"));
        createTracksParametersTab = new QWidget();
        createTracksParametersTab->setObjectName(QString::fromUtf8("createTracksParametersTab"));
        createTracksTitleLabel1 = new QLabel(createTracksParametersTab);
        createTracksTitleLabel1->setObjectName(QString::fromUtf8("createTracksTitleLabel1"));
        createTracksTitleLabel1->setGeometry(QRect(0, 10, 588, 21));
        createTracksTitleLabel1->setFont(font2);
        createTracksLine1 = new QFrame(createTracksParametersTab);
        createTracksLine1->setObjectName(QString::fromUtf8("createTracksLine1"));
        createTracksLine1->setGeometry(QRect(0, 30, 627, 3));
        createTracksLine1->setFrameShape(QFrame::HLine);
        createTracksLine1->setFrameShadow(QFrame::Sunken);
        layoutWidget_11 = new QWidget(createTracksParametersTab);
        layoutWidget_11->setObjectName(QString::fromUtf8("layoutWidget_11"));
        layoutWidget_11->setGeometry(QRect(0, 40, 641, 121));
        peaksTrackGridLayout1 = new QGridLayout(layoutWidget_11);
        peaksTrackGridLayout1->setSpacing(6);
        peaksTrackGridLayout1->setContentsMargins(11, 11, 11, 11);
        peaksTrackGridLayout1->setObjectName(QString::fromUtf8("peaksTrackGridLayout1"));
        peaksTrackGridLayout1->setHorizontalSpacing(-1);
        peaksTrackGridLayout1->setVerticalSpacing(15);
        peaksTrackGridLayout1->setContentsMargins(0, 0, 0, 0);
        peaksTrackSelectPeaksLabel = new QLabel(layoutWidget_11);
        peaksTrackSelectPeaksLabel->setObjectName(QString::fromUtf8("peaksTrackSelectPeaksLabel"));

        peaksTrackGridLayout1->addWidget(peaksTrackSelectPeaksLabel, 0, 0, 1, 1);

        peaksTrackSelectPeaksLineEdit = new QLineEdit(layoutWidget_11);
        peaksTrackSelectPeaksLineEdit->setObjectName(QString::fromUtf8("peaksTrackSelectPeaksLineEdit"));
        peaksTrackSelectPeaksLineEdit->setReadOnly(true);

        peaksTrackGridLayout1->addWidget(peaksTrackSelectPeaksLineEdit, 0, 1, 1, 2);

        peaksTrackSelectPeaksToolButton = new QToolButton(layoutWidget_11);
        peaksTrackSelectPeaksToolButton->setObjectName(QString::fromUtf8("peaksTrackSelectPeaksToolButton"));

        peaksTrackGridLayout1->addWidget(peaksTrackSelectPeaksToolButton, 0, 3, 1, 1);

        peaksTrackSelectPeaksInfoButton = new QPushButton(layoutWidget_11);
        peaksTrackSelectPeaksInfoButton->setObjectName(QString::fromUtf8("peaksTrackSelectPeaksInfoButton"));
        peaksTrackSelectPeaksInfoButton->setMaximumSize(QSize(21, 21));
        peaksTrackSelectPeaksInfoButton->setMouseTracking(false);
        peaksTrackSelectPeaksInfoButton->setAcceptDrops(false);
        peaksTrackSelectPeaksInfoButton->setLayoutDirection(Qt::LeftToRight);
        peaksTrackSelectPeaksInfoButton->setIcon(icon1);
        peaksTrackSelectPeaksInfoButton->setAutoRepeatDelay(100);
        peaksTrackSelectPeaksInfoButton->setDefault(false);
        peaksTrackSelectPeaksInfoButton->setFlat(true);

        peaksTrackGridLayout1->addWidget(peaksTrackSelectPeaksInfoButton, 0, 4, 1, 1);

        peaksTrackNameLabel = new QLabel(layoutWidget_11);
        peaksTrackNameLabel->setObjectName(QString::fromUtf8("peaksTrackNameLabel"));

        peaksTrackGridLayout1->addWidget(peaksTrackNameLabel, 2, 0, 1, 1);

        peaksTrackNameInfoButton = new QPushButton(layoutWidget_11);
        peaksTrackNameInfoButton->setObjectName(QString::fromUtf8("peaksTrackNameInfoButton"));
        peaksTrackNameInfoButton->setMaximumSize(QSize(21, 21));
        peaksTrackNameInfoButton->setMouseTracking(false);
        peaksTrackNameInfoButton->setAcceptDrops(false);
        peaksTrackNameInfoButton->setLayoutDirection(Qt::LeftToRight);
        peaksTrackNameInfoButton->setIcon(icon1);
        peaksTrackNameInfoButton->setAutoRepeatDelay(100);
        peaksTrackNameInfoButton->setDefault(false);
        peaksTrackNameInfoButton->setFlat(true);

        peaksTrackGridLayout1->addWidget(peaksTrackNameInfoButton, 2, 4, 1, 1);

        peaksTrackNameLineEdit = new QLineEdit(layoutWidget_11);
        peaksTrackNameLineEdit->setObjectName(QString::fromUtf8("peaksTrackNameLineEdit"));

        peaksTrackGridLayout1->addWidget(peaksTrackNameLineEdit, 2, 1, 1, 2);

        peaksTrackOutputFileLabel = new QLabel(layoutWidget_11);
        peaksTrackOutputFileLabel->setObjectName(QString::fromUtf8("peaksTrackOutputFileLabel"));

        peaksTrackGridLayout1->addWidget(peaksTrackOutputFileLabel, 1, 0, 1, 1);

        peaksTrackOutputFileLineEdit = new QLineEdit(layoutWidget_11);
        peaksTrackOutputFileLineEdit->setObjectName(QString::fromUtf8("peaksTrackOutputFileLineEdit"));

        peaksTrackGridLayout1->addWidget(peaksTrackOutputFileLineEdit, 1, 1, 1, 2);

        peaksTrackSuffixLabel = new QLabel(layoutWidget_11);
        peaksTrackSuffixLabel->setObjectName(QString::fromUtf8("peaksTrackSuffixLabel"));

        peaksTrackGridLayout1->addWidget(peaksTrackSuffixLabel, 1, 3, 1, 1);

        peaksTrackOutputFileInfoButton = new QPushButton(layoutWidget_11);
        peaksTrackOutputFileInfoButton->setObjectName(QString::fromUtf8("peaksTrackOutputFileInfoButton"));
        peaksTrackOutputFileInfoButton->setMaximumSize(QSize(21, 21));
        peaksTrackOutputFileInfoButton->setMouseTracking(false);
        peaksTrackOutputFileInfoButton->setAcceptDrops(false);
        peaksTrackOutputFileInfoButton->setLayoutDirection(Qt::LeftToRight);
        peaksTrackOutputFileInfoButton->setIcon(icon1);
        peaksTrackOutputFileInfoButton->setAutoRepeatDelay(100);
        peaksTrackOutputFileInfoButton->setDefault(false);
        peaksTrackOutputFileInfoButton->setFlat(true);

        peaksTrackGridLayout1->addWidget(peaksTrackOutputFileInfoButton, 1, 4, 1, 1);

        createTracksLine2 = new QFrame(createTracksParametersTab);
        createTracksLine2->setObjectName(QString::fromUtf8("createTracksLine2"));
        createTracksLine2->setGeometry(QRect(0, 160, 629, 3));
        createTracksLine2->setFrameShadow(QFrame::Sunken);
        createTracksLine2->setFrameShape(QFrame::HLine);
        layoutWidget_12 = new QWidget(createTracksParametersTab);
        layoutWidget_12->setObjectName(QString::fromUtf8("layoutWidget_12"));
        layoutWidget_12->setGeometry(QRect(400, 160, 241, 41));
        createTracksHorizontalLayout1 = new QHBoxLayout(layoutWidget_12);
        createTracksHorizontalLayout1->setSpacing(6);
        createTracksHorizontalLayout1->setContentsMargins(11, 11, 11, 11);
        createTracksHorizontalLayout1->setObjectName(QString::fromUtf8("createTracksHorizontalLayout1"));
        createTracksHorizontalLayout1->setContentsMargins(0, 0, 0, 0);
        peaksTrackClearButton = new QPushButton(layoutWidget_12);
        peaksTrackClearButton->setObjectName(QString::fromUtf8("peaksTrackClearButton"));

        createTracksHorizontalLayout1->addWidget(peaksTrackClearButton);

        createPeaksTrackButton = new QPushButton(layoutWidget_12);
        createPeaksTrackButton->setObjectName(QString::fromUtf8("createPeaksTrackButton"));

        createTracksHorizontalLayout1->addWidget(createPeaksTrackButton);

        createTracksLine3 = new QFrame(createTracksParametersTab);
        createTracksLine3->setObjectName(QString::fromUtf8("createTracksLine3"));
        createTracksLine3->setGeometry(QRect(1, 220, 627, 3));
        createTracksLine3->setFrameShape(QFrame::HLine);
        createTracksLine3->setFrameShadow(QFrame::Sunken);
        layoutWidget_13 = new QWidget(createTracksParametersTab);
        layoutWidget_13->setObjectName(QString::fromUtf8("layoutWidget_13"));
        layoutWidget_13->setGeometry(QRect(400, 420, 241, 41));
        createTracksHorizontalLayout2 = new QHBoxLayout(layoutWidget_13);
        createTracksHorizontalLayout2->setSpacing(6);
        createTracksHorizontalLayout2->setContentsMargins(11, 11, 11, 11);
        createTracksHorizontalLayout2->setObjectName(QString::fromUtf8("createTracksHorizontalLayout2"));
        createTracksHorizontalLayout2->setContentsMargins(0, 0, 0, 0);
        readsTrackClearButton = new QPushButton(layoutWidget_13);
        readsTrackClearButton->setObjectName(QString::fromUtf8("readsTrackClearButton"));

        createTracksHorizontalLayout2->addWidget(readsTrackClearButton);

        createReadsTrackButton = new QPushButton(layoutWidget_13);
        createReadsTrackButton->setObjectName(QString::fromUtf8("createReadsTrackButton"));

        createTracksHorizontalLayout2->addWidget(createReadsTrackButton);

        createTracksTitleLabel2 = new QLabel(createTracksParametersTab);
        createTracksTitleLabel2->setObjectName(QString::fromUtf8("createTracksTitleLabel2"));
        createTracksTitleLabel2->setGeometry(QRect(1, 200, 588, 21));
        createTracksTitleLabel2->setFont(font2);
        layoutWidget_14 = new QWidget(createTracksParametersTab);
        layoutWidget_14->setObjectName(QString::fromUtf8("layoutWidget_14"));
        layoutWidget_14->setGeometry(QRect(0, 230, 641, 191));
        peaksTrackGridLayout2 = new QGridLayout(layoutWidget_14);
        peaksTrackGridLayout2->setSpacing(6);
        peaksTrackGridLayout2->setContentsMargins(11, 11, 11, 11);
        peaksTrackGridLayout2->setObjectName(QString::fromUtf8("peaksTrackGridLayout2"));
        peaksTrackGridLayout2->setHorizontalSpacing(-1);
        peaksTrackGridLayout2->setVerticalSpacing(15);
        peaksTrackGridLayout2->setContentsMargins(0, 0, 0, 0);
        readsTrackSelectReadsLabel = new QLabel(layoutWidget_14);
        readsTrackSelectReadsLabel->setObjectName(QString::fromUtf8("readsTrackSelectReadsLabel"));

        peaksTrackGridLayout2->addWidget(readsTrackSelectReadsLabel, 0, 0, 1, 1);

        readsTrackSelectReadsLineEdit = new QLineEdit(layoutWidget_14);
        readsTrackSelectReadsLineEdit->setObjectName(QString::fromUtf8("readsTrackSelectReadsLineEdit"));
        readsTrackSelectReadsLineEdit->setReadOnly(true);

        peaksTrackGridLayout2->addWidget(readsTrackSelectReadsLineEdit, 0, 1, 1, 2);

        readsTrackSelectReadsToolButton = new QToolButton(layoutWidget_14);
        readsTrackSelectReadsToolButton->setObjectName(QString::fromUtf8("readsTrackSelectReadsToolButton"));

        peaksTrackGridLayout2->addWidget(readsTrackSelectReadsToolButton, 0, 3, 1, 1);

        readsTrackSelectReadsInfoButton = new QPushButton(layoutWidget_14);
        readsTrackSelectReadsInfoButton->setObjectName(QString::fromUtf8("readsTrackSelectReadsInfoButton"));
        readsTrackSelectReadsInfoButton->setMaximumSize(QSize(21, 21));
        readsTrackSelectReadsInfoButton->setMouseTracking(false);
        readsTrackSelectReadsInfoButton->setAcceptDrops(false);
        readsTrackSelectReadsInfoButton->setLayoutDirection(Qt::LeftToRight);
        readsTrackSelectReadsInfoButton->setIcon(icon1);
        readsTrackSelectReadsInfoButton->setAutoRepeatDelay(100);
        readsTrackSelectReadsInfoButton->setDefault(false);
        readsTrackSelectReadsInfoButton->setFlat(true);

        peaksTrackGridLayout2->addWidget(readsTrackSelectReadsInfoButton, 0, 4, 1, 1);

        readsTrackNameLabel = new QLabel(layoutWidget_14);
        readsTrackNameLabel->setObjectName(QString::fromUtf8("readsTrackNameLabel"));

        peaksTrackGridLayout2->addWidget(readsTrackNameLabel, 2, 0, 1, 1);

        readsTrackNameInfoButton = new QPushButton(layoutWidget_14);
        readsTrackNameInfoButton->setObjectName(QString::fromUtf8("readsTrackNameInfoButton"));
        readsTrackNameInfoButton->setMaximumSize(QSize(21, 21));
        readsTrackNameInfoButton->setMouseTracking(false);
        readsTrackNameInfoButton->setAcceptDrops(false);
        readsTrackNameInfoButton->setLayoutDirection(Qt::LeftToRight);
        readsTrackNameInfoButton->setIcon(icon1);
        readsTrackNameInfoButton->setAutoRepeatDelay(100);
        readsTrackNameInfoButton->setDefault(false);
        readsTrackNameInfoButton->setFlat(true);

        peaksTrackGridLayout2->addWidget(readsTrackNameInfoButton, 2, 4, 1, 1);

        readsTrackNameLineEdit = new QLineEdit(layoutWidget_14);
        readsTrackNameLineEdit->setObjectName(QString::fromUtf8("readsTrackNameLineEdit"));

        peaksTrackGridLayout2->addWidget(readsTrackNameLineEdit, 2, 1, 1, 2);

        readsTrackUniqueInfoButton = new QPushButton(layoutWidget_14);
        readsTrackUniqueInfoButton->setObjectName(QString::fromUtf8("readsTrackUniqueInfoButton"));
        readsTrackUniqueInfoButton->setMaximumSize(QSize(21, 21));
        readsTrackUniqueInfoButton->setMouseTracking(false);
        readsTrackUniqueInfoButton->setAcceptDrops(false);
        readsTrackUniqueInfoButton->setLayoutDirection(Qt::LeftToRight);
        readsTrackUniqueInfoButton->setIcon(icon1);
        readsTrackUniqueInfoButton->setAutoRepeatDelay(100);
        readsTrackUniqueInfoButton->setDefault(false);
        readsTrackUniqueInfoButton->setFlat(true);

        peaksTrackGridLayout2->addWidget(readsTrackUniqueInfoButton, 3, 4, 1, 1);

        readsTrackUniqueCheckBox = new QCheckBox(layoutWidget_14);
        readsTrackUniqueCheckBox->setObjectName(QString::fromUtf8("readsTrackUniqueCheckBox"));
        readsTrackUniqueCheckBox->setChecked(true);

        peaksTrackGridLayout2->addWidget(readsTrackUniqueCheckBox, 3, 0, 1, 1);

        readsTrackOutputFileLabel = new QLabel(layoutWidget_14);
        readsTrackOutputFileLabel->setObjectName(QString::fromUtf8("readsTrackOutputFileLabel"));

        peaksTrackGridLayout2->addWidget(readsTrackOutputFileLabel, 1, 0, 1, 1);

        readsTrackOutputFileInfoButton = new QPushButton(layoutWidget_14);
        readsTrackOutputFileInfoButton->setObjectName(QString::fromUtf8("readsTrackOutputFileInfoButton"));
        readsTrackOutputFileInfoButton->setMaximumSize(QSize(21, 21));
        readsTrackOutputFileInfoButton->setMouseTracking(false);
        readsTrackOutputFileInfoButton->setAcceptDrops(false);
        readsTrackOutputFileInfoButton->setLayoutDirection(Qt::LeftToRight);
        readsTrackOutputFileInfoButton->setIcon(icon1);
        readsTrackOutputFileInfoButton->setAutoRepeatDelay(100);
        readsTrackOutputFileInfoButton->setDefault(false);
        readsTrackOutputFileInfoButton->setFlat(true);

        peaksTrackGridLayout2->addWidget(readsTrackOutputFileInfoButton, 1, 4, 1, 1);

        readsTrackOutputFileLineEdit = new QLineEdit(layoutWidget_14);
        readsTrackOutputFileLineEdit->setObjectName(QString::fromUtf8("readsTrackOutputFileLineEdit"));

        peaksTrackGridLayout2->addWidget(readsTrackOutputFileLineEdit, 1, 1, 1, 2);

        readsTrackSuffixLabel = new QLabel(layoutWidget_14);
        readsTrackSuffixLabel->setObjectName(QString::fromUtf8("readsTrackSuffixLabel"));

        peaksTrackGridLayout2->addWidget(readsTrackSuffixLabel, 1, 3, 1, 1);

        readsTrackBigWigCheckBox = new QCheckBox(layoutWidget_14);
        readsTrackBigWigCheckBox->setObjectName(QString::fromUtf8("readsTrackBigWigCheckBox"));
        readsTrackBigWigCheckBox->setEnabled(false);
        readsTrackBigWigCheckBox->setChecked(false);

        peaksTrackGridLayout2->addWidget(readsTrackBigWigCheckBox, 4, 0, 1, 1);

        readsTrackBigWigInfoButton = new QPushButton(layoutWidget_14);
        readsTrackBigWigInfoButton->setObjectName(QString::fromUtf8("readsTrackBigWigInfoButton"));
        readsTrackBigWigInfoButton->setEnabled(false);
        readsTrackBigWigInfoButton->setMaximumSize(QSize(21, 21));
        readsTrackBigWigInfoButton->setMouseTracking(false);
        readsTrackBigWigInfoButton->setAcceptDrops(false);
        readsTrackBigWigInfoButton->setLayoutDirection(Qt::LeftToRight);
        readsTrackBigWigInfoButton->setIcon(icon1);
        readsTrackBigWigInfoButton->setAutoRepeatDelay(100);
        readsTrackBigWigInfoButton->setDefault(false);
        readsTrackBigWigInfoButton->setFlat(true);

        peaksTrackGridLayout2->addWidget(readsTrackBigWigInfoButton, 4, 4, 1, 1);

        createTracksLine4 = new QFrame(createTracksParametersTab);
        createTracksLine4->setObjectName(QString::fromUtf8("createTracksLine4"));
        createTracksLine4->setGeometry(QRect(0, 420, 629, 3));
        createTracksLine4->setFrameShadow(QFrame::Sunken);
        createTracksLine4->setFrameShape(QFrame::HLine);
        createTracksTabWidget->addTab(createTracksParametersTab, QString());
        createTracksProgressTab = new QWidget();
        createTracksProgressTab->setObjectName(QString::fromUtf8("createTracksProgressTab"));
        createTracksProgressTextEdit = new QTextEdit(createTracksProgressTab);
        createTracksProgressTextEdit->setObjectName(QString::fromUtf8("createTracksProgressTextEdit"));
        createTracksProgressTextEdit->setGeometry(QRect(0, 0, 641, 501));
        createTracksProgressTextEdit->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);
        layoutWidget10 = new QWidget(createTracksProgressTab);
        layoutWidget10->setObjectName(QString::fromUtf8("layoutWidget10"));
        layoutWidget10->setGeometry(QRect(312, 510, 321, 32));
        horizontalLayout = new QHBoxLayout(layoutWidget10);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        openGenomeBrowserButton = new QPushButton(layoutWidget10);
        openGenomeBrowserButton->setObjectName(QString::fromUtf8("openGenomeBrowserButton"));

        horizontalLayout->addWidget(openGenomeBrowserButton);

        stopCreateTracksProcessButton = new QPushButton(layoutWidget10);
        stopCreateTracksProcessButton->setObjectName(QString::fromUtf8("stopCreateTracksProcessButton"));
        stopCreateTracksProcessButton->setEnabled(false);

        horizontalLayout->addWidget(stopCreateTracksProcessButton);

        createTracksTabWidget->addTab(createTracksProgressTab, QString());

        verticalLayout->addWidget(createTracksTabWidget);

        stackedWidget->addWidget(createTracksStackedPage);
        repeatsStackedPage = new QWidget();
        repeatsStackedPage->setObjectName(QString::fromUtf8("repeatsStackedPage"));
        verticalLayout_5 = new QVBoxLayout(repeatsStackedPage);
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        repeatsTabWidget = new QTabWidget(repeatsStackedPage);
        repeatsTabWidget->setObjectName(QString::fromUtf8("repeatsTabWidget"));
        repeatsParametersTab = new QWidget();
        repeatsParametersTab->setObjectName(QString::fromUtf8("repeatsParametersTab"));
        repeatsTitleLabel = new QLabel(repeatsParametersTab);
        repeatsTitleLabel->setObjectName(QString::fromUtf8("repeatsTitleLabel"));
        repeatsTitleLabel->setGeometry(QRect(0, 10, 588, 21));
        repeatsTitleLabel->setFont(font2);
        repeatsLine1 = new QFrame(repeatsParametersTab);
        repeatsLine1->setObjectName(QString::fromUtf8("repeatsLine1"));
        repeatsLine1->setGeometry(QRect(0, 30, 627, 3));
        repeatsLine1->setFrameShape(QFrame::HLine);
        repeatsLine1->setFrameShadow(QFrame::Sunken);
        layoutWidget_9 = new QWidget(repeatsParametersTab);
        layoutWidget_9->setObjectName(QString::fromUtf8("layoutWidget_9"));
        layoutWidget_9->setGeometry(QRect(0, 40, 641, 111));
        repeatsGridLayout = new QGridLayout(layoutWidget_9);
        repeatsGridLayout->setSpacing(6);
        repeatsGridLayout->setContentsMargins(11, 11, 11, 11);
        repeatsGridLayout->setObjectName(QString::fromUtf8("repeatsGridLayout"));
        repeatsGridLayout->setHorizontalSpacing(-1);
        repeatsGridLayout->setVerticalSpacing(15);
        repeatsGridLayout->setContentsMargins(0, 0, 0, 0);
        repeatsSpeciesComboBox = new QComboBox(layoutWidget_9);
        repeatsSpeciesComboBox->setObjectName(QString::fromUtf8("repeatsSpeciesComboBox"));
        repeatsSpeciesComboBox->setEnabled(true);
        repeatsSpeciesComboBox->setEditable(false);
        repeatsSpeciesComboBox->setModelColumn(0);

        repeatsGridLayout->addWidget(repeatsSpeciesComboBox, 1, 1, 1, 2);

        repeatsSpeciesInfoButton = new QPushButton(layoutWidget_9);
        repeatsSpeciesInfoButton->setObjectName(QString::fromUtf8("repeatsSpeciesInfoButton"));
        repeatsSpeciesInfoButton->setMaximumSize(QSize(21, 21));
        repeatsSpeciesInfoButton->setMouseTracking(false);
        repeatsSpeciesInfoButton->setAcceptDrops(false);
        repeatsSpeciesInfoButton->setLayoutDirection(Qt::LeftToRight);
        repeatsSpeciesInfoButton->setIcon(icon1);
        repeatsSpeciesInfoButton->setAutoRepeatDelay(100);
        repeatsSpeciesInfoButton->setDefault(false);
        repeatsSpeciesInfoButton->setFlat(true);

        repeatsGridLayout->addWidget(repeatsSpeciesInfoButton, 1, 4, 1, 1);

        repeatsSelectPeaksLabel = new QLabel(layoutWidget_9);
        repeatsSelectPeaksLabel->setObjectName(QString::fromUtf8("repeatsSelectPeaksLabel"));

        repeatsGridLayout->addWidget(repeatsSelectPeaksLabel, 0, 0, 1, 1);

        repeatsSelectPeaksLineEdit = new QLineEdit(layoutWidget_9);
        repeatsSelectPeaksLineEdit->setObjectName(QString::fromUtf8("repeatsSelectPeaksLineEdit"));
        repeatsSelectPeaksLineEdit->setReadOnly(true);

        repeatsGridLayout->addWidget(repeatsSelectPeaksLineEdit, 0, 1, 1, 2);

        repeatsSelectPeaksToolButton = new QToolButton(layoutWidget_9);
        repeatsSelectPeaksToolButton->setObjectName(QString::fromUtf8("repeatsSelectPeaksToolButton"));

        repeatsGridLayout->addWidget(repeatsSelectPeaksToolButton, 0, 3, 1, 1);

        repeatsSelectPeaksInfoButton = new QPushButton(layoutWidget_9);
        repeatsSelectPeaksInfoButton->setObjectName(QString::fromUtf8("repeatsSelectPeaksInfoButton"));
        repeatsSelectPeaksInfoButton->setMaximumSize(QSize(21, 21));
        repeatsSelectPeaksInfoButton->setMouseTracking(false);
        repeatsSelectPeaksInfoButton->setAcceptDrops(false);
        repeatsSelectPeaksInfoButton->setLayoutDirection(Qt::LeftToRight);
        repeatsSelectPeaksInfoButton->setIcon(icon1);
        repeatsSelectPeaksInfoButton->setAutoRepeatDelay(100);
        repeatsSelectPeaksInfoButton->setDefault(false);
        repeatsSelectPeaksInfoButton->setFlat(true);

        repeatsGridLayout->addWidget(repeatsSelectPeaksInfoButton, 0, 4, 1, 1);

        repeatsOutputFileLabel = new QLabel(layoutWidget_9);
        repeatsOutputFileLabel->setObjectName(QString::fromUtf8("repeatsOutputFileLabel"));

        repeatsGridLayout->addWidget(repeatsOutputFileLabel, 2, 0, 1, 1);

        repeatsOutputFileLineEdit = new QLineEdit(layoutWidget_9);
        repeatsOutputFileLineEdit->setObjectName(QString::fromUtf8("repeatsOutputFileLineEdit"));

        repeatsGridLayout->addWidget(repeatsOutputFileLineEdit, 2, 1, 1, 2);

        repeatsSuffixLabel = new QLabel(layoutWidget_9);
        repeatsSuffixLabel->setObjectName(QString::fromUtf8("repeatsSuffixLabel"));

        repeatsGridLayout->addWidget(repeatsSuffixLabel, 2, 3, 1, 1);

        repeatsOutputFileInfoButton = new QPushButton(layoutWidget_9);
        repeatsOutputFileInfoButton->setObjectName(QString::fromUtf8("repeatsOutputFileInfoButton"));
        repeatsOutputFileInfoButton->setMaximumSize(QSize(21, 21));
        repeatsOutputFileInfoButton->setMouseTracking(false);
        repeatsOutputFileInfoButton->setAcceptDrops(false);
        repeatsOutputFileInfoButton->setLayoutDirection(Qt::LeftToRight);
        repeatsOutputFileInfoButton->setIcon(icon1);
        repeatsOutputFileInfoButton->setAutoRepeatDelay(100);
        repeatsOutputFileInfoButton->setDefault(false);
        repeatsOutputFileInfoButton->setFlat(true);

        repeatsGridLayout->addWidget(repeatsOutputFileInfoButton, 2, 4, 1, 1);

        repeatsSpeciesLabel = new QLabel(layoutWidget_9);
        repeatsSpeciesLabel->setObjectName(QString::fromUtf8("repeatsSpeciesLabel"));

        repeatsGridLayout->addWidget(repeatsSpeciesLabel, 1, 0, 1, 1);

        repeatsLine2 = new QFrame(repeatsParametersTab);
        repeatsLine2->setObjectName(QString::fromUtf8("repeatsLine2"));
        repeatsLine2->setGeometry(QRect(0, 150, 629, 3));
        repeatsLine2->setFrameShadow(QFrame::Sunken);
        repeatsLine2->setFrameShape(QFrame::HLine);
        layoutWidget_10 = new QWidget(repeatsParametersTab);
        layoutWidget_10->setObjectName(QString::fromUtf8("layoutWidget_10"));
        layoutWidget_10->setGeometry(QRect(390, 150, 251, 41));
        repeatsButtonsHorizontalLayout = new QHBoxLayout(layoutWidget_10);
        repeatsButtonsHorizontalLayout->setSpacing(6);
        repeatsButtonsHorizontalLayout->setContentsMargins(11, 11, 11, 11);
        repeatsButtonsHorizontalLayout->setObjectName(QString::fromUtf8("repeatsButtonsHorizontalLayout"));
        repeatsButtonsHorizontalLayout->setContentsMargins(0, 0, 0, 0);
        runFindRepeatsClearButton = new QPushButton(layoutWidget_10);
        runFindRepeatsClearButton->setObjectName(QString::fromUtf8("runFindRepeatsClearButton"));

        repeatsButtonsHorizontalLayout->addWidget(runFindRepeatsClearButton);

        runFindRepeatsButton = new QPushButton(layoutWidget_10);
        runFindRepeatsButton->setObjectName(QString::fromUtf8("runFindRepeatsButton"));

        repeatsButtonsHorizontalLayout->addWidget(runFindRepeatsButton);

        repeatsTabWidget->addTab(repeatsParametersTab, QString());
        repeatsProgressTab = new QWidget();
        repeatsProgressTab->setObjectName(QString::fromUtf8("repeatsProgressTab"));
        repeatsProgressTextEdit = new QTextEdit(repeatsProgressTab);
        repeatsProgressTextEdit->setObjectName(QString::fromUtf8("repeatsProgressTextEdit"));
        repeatsProgressTextEdit->setGeometry(QRect(0, 0, 641, 501));
        repeatsProgressTextEdit->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);
        stopRunFindRepeatsProcessButton = new QPushButton(repeatsProgressTab);
        stopRunFindRepeatsProcessButton->setObjectName(QString::fromUtf8("stopRunFindRepeatsProcessButton"));
        stopRunFindRepeatsProcessButton->setEnabled(false);
        stopRunFindRepeatsProcessButton->setGeometry(QRect(520, 510, 113, 32));
        repeatsTabWidget->addTab(repeatsProgressTab, QString());

        verticalLayout_5->addWidget(repeatsTabWidget);

        stackedWidget->addWidget(repeatsStackedPage);
        cpgIslandsStackedPage = new QWidget();
        cpgIslandsStackedPage->setObjectName(QString::fromUtf8("cpgIslandsStackedPage"));
        verticalLayout_8 = new QVBoxLayout(cpgIslandsStackedPage);
        verticalLayout_8->setSpacing(6);
        verticalLayout_8->setContentsMargins(11, 11, 11, 11);
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        cpgIslandsTabWidget = new QTabWidget(cpgIslandsStackedPage);
        cpgIslandsTabWidget->setObjectName(QString::fromUtf8("cpgIslandsTabWidget"));
        cpgIslandsParametersTab = new QWidget();
        cpgIslandsParametersTab->setObjectName(QString::fromUtf8("cpgIslandsParametersTab"));
        cpgIslandsTitleLabel = new QLabel(cpgIslandsParametersTab);
        cpgIslandsTitleLabel->setObjectName(QString::fromUtf8("cpgIslandsTitleLabel"));
        cpgIslandsTitleLabel->setGeometry(QRect(0, 10, 588, 21));
        cpgIslandsTitleLabel->setFont(font2);
        cpgIslandsLine1 = new QFrame(cpgIslandsParametersTab);
        cpgIslandsLine1->setObjectName(QString::fromUtf8("cpgIslandsLine1"));
        cpgIslandsLine1->setGeometry(QRect(0, 30, 627, 3));
        cpgIslandsLine1->setFrameShape(QFrame::HLine);
        cpgIslandsLine1->setFrameShadow(QFrame::Sunken);
        layoutWidget_15 = new QWidget(cpgIslandsParametersTab);
        layoutWidget_15->setObjectName(QString::fromUtf8("layoutWidget_15"));
        layoutWidget_15->setGeometry(QRect(0, 40, 641, 111));
        cpgIslandsGridLayout = new QGridLayout(layoutWidget_15);
        cpgIslandsGridLayout->setSpacing(6);
        cpgIslandsGridLayout->setContentsMargins(11, 11, 11, 11);
        cpgIslandsGridLayout->setObjectName(QString::fromUtf8("cpgIslandsGridLayout"));
        cpgIslandsGridLayout->setHorizontalSpacing(-1);
        cpgIslandsGridLayout->setVerticalSpacing(15);
        cpgIslandsGridLayout->setContentsMargins(0, 0, 0, 0);
        cpgIslandsSpeciesComboBox = new QComboBox(layoutWidget_15);
        cpgIslandsSpeciesComboBox->setObjectName(QString::fromUtf8("cpgIslandsSpeciesComboBox"));
        cpgIslandsSpeciesComboBox->setEnabled(true);
        cpgIslandsSpeciesComboBox->setEditable(false);
        cpgIslandsSpeciesComboBox->setModelColumn(0);

        cpgIslandsGridLayout->addWidget(cpgIslandsSpeciesComboBox, 1, 1, 1, 2);

        cpgIslandsSpeciesInfoButton = new QPushButton(layoutWidget_15);
        cpgIslandsSpeciesInfoButton->setObjectName(QString::fromUtf8("cpgIslandsSpeciesInfoButton"));
        cpgIslandsSpeciesInfoButton->setMaximumSize(QSize(21, 21));
        cpgIslandsSpeciesInfoButton->setMouseTracking(false);
        cpgIslandsSpeciesInfoButton->setAcceptDrops(false);
        cpgIslandsSpeciesInfoButton->setLayoutDirection(Qt::LeftToRight);
        cpgIslandsSpeciesInfoButton->setIcon(icon1);
        cpgIslandsSpeciesInfoButton->setAutoRepeatDelay(100);
        cpgIslandsSpeciesInfoButton->setDefault(false);
        cpgIslandsSpeciesInfoButton->setFlat(true);

        cpgIslandsGridLayout->addWidget(cpgIslandsSpeciesInfoButton, 1, 4, 1, 1);

        cpgIslandsSpeciesLabel = new QLabel(layoutWidget_15);
        cpgIslandsSpeciesLabel->setObjectName(QString::fromUtf8("cpgIslandsSpeciesLabel"));

        cpgIslandsGridLayout->addWidget(cpgIslandsSpeciesLabel, 1, 0, 1, 1);

        cpgIslandsSelectPeaksLabel = new QLabel(layoutWidget_15);
        cpgIslandsSelectPeaksLabel->setObjectName(QString::fromUtf8("cpgIslandsSelectPeaksLabel"));

        cpgIslandsGridLayout->addWidget(cpgIslandsSelectPeaksLabel, 0, 0, 1, 1);

        cpgIslandsSelectPeaksLineEdit = new QLineEdit(layoutWidget_15);
        cpgIslandsSelectPeaksLineEdit->setObjectName(QString::fromUtf8("cpgIslandsSelectPeaksLineEdit"));
        cpgIslandsSelectPeaksLineEdit->setReadOnly(true);

        cpgIslandsGridLayout->addWidget(cpgIslandsSelectPeaksLineEdit, 0, 1, 1, 2);

        cpgIslandsSelectPeaksToolButton = new QToolButton(layoutWidget_15);
        cpgIslandsSelectPeaksToolButton->setObjectName(QString::fromUtf8("cpgIslandsSelectPeaksToolButton"));

        cpgIslandsGridLayout->addWidget(cpgIslandsSelectPeaksToolButton, 0, 3, 1, 1);

        cpgIslandsSelectPeaksInfoButton = new QPushButton(layoutWidget_15);
        cpgIslandsSelectPeaksInfoButton->setObjectName(QString::fromUtf8("cpgIslandsSelectPeaksInfoButton"));
        cpgIslandsSelectPeaksInfoButton->setMaximumSize(QSize(21, 21));
        cpgIslandsSelectPeaksInfoButton->setMouseTracking(false);
        cpgIslandsSelectPeaksInfoButton->setAcceptDrops(false);
        cpgIslandsSelectPeaksInfoButton->setLayoutDirection(Qt::LeftToRight);
        cpgIslandsSelectPeaksInfoButton->setIcon(icon1);
        cpgIslandsSelectPeaksInfoButton->setAutoRepeatDelay(100);
        cpgIslandsSelectPeaksInfoButton->setDefault(false);
        cpgIslandsSelectPeaksInfoButton->setFlat(true);

        cpgIslandsGridLayout->addWidget(cpgIslandsSelectPeaksInfoButton, 0, 4, 1, 1);

        cpgIslandsOutputFileLabel = new QLabel(layoutWidget_15);
        cpgIslandsOutputFileLabel->setObjectName(QString::fromUtf8("cpgIslandsOutputFileLabel"));

        cpgIslandsGridLayout->addWidget(cpgIslandsOutputFileLabel, 2, 0, 1, 1);

        cpgIslandsOutputFileLineEdit = new QLineEdit(layoutWidget_15);
        cpgIslandsOutputFileLineEdit->setObjectName(QString::fromUtf8("cpgIslandsOutputFileLineEdit"));

        cpgIslandsGridLayout->addWidget(cpgIslandsOutputFileLineEdit, 2, 1, 1, 2);

        cpgIslandsSuffixLabel = new QLabel(layoutWidget_15);
        cpgIslandsSuffixLabel->setObjectName(QString::fromUtf8("cpgIslandsSuffixLabel"));

        cpgIslandsGridLayout->addWidget(cpgIslandsSuffixLabel, 2, 3, 1, 1);

        cpgIslandsOutputFileInfoButton = new QPushButton(layoutWidget_15);
        cpgIslandsOutputFileInfoButton->setObjectName(QString::fromUtf8("cpgIslandsOutputFileInfoButton"));
        cpgIslandsOutputFileInfoButton->setMaximumSize(QSize(21, 21));
        cpgIslandsOutputFileInfoButton->setMouseTracking(false);
        cpgIslandsOutputFileInfoButton->setAcceptDrops(false);
        cpgIslandsOutputFileInfoButton->setLayoutDirection(Qt::LeftToRight);
        cpgIslandsOutputFileInfoButton->setIcon(icon1);
        cpgIslandsOutputFileInfoButton->setAutoRepeatDelay(100);
        cpgIslandsOutputFileInfoButton->setDefault(false);
        cpgIslandsOutputFileInfoButton->setFlat(true);

        cpgIslandsGridLayout->addWidget(cpgIslandsOutputFileInfoButton, 2, 4, 1, 1);

        cpgIslandsLine2 = new QFrame(cpgIslandsParametersTab);
        cpgIslandsLine2->setObjectName(QString::fromUtf8("cpgIslandsLine2"));
        cpgIslandsLine2->setGeometry(QRect(0, 150, 629, 3));
        cpgIslandsLine2->setFrameShadow(QFrame::Sunken);
        cpgIslandsLine2->setFrameShape(QFrame::HLine);
        layoutWidget_16 = new QWidget(cpgIslandsParametersTab);
        layoutWidget_16->setObjectName(QString::fromUtf8("layoutWidget_16"));
        layoutWidget_16->setGeometry(QRect(390, 150, 251, 41));
        cpgIslandsButtonsHorizontalLayout = new QHBoxLayout(layoutWidget_16);
        cpgIslandsButtonsHorizontalLayout->setSpacing(6);
        cpgIslandsButtonsHorizontalLayout->setContentsMargins(11, 11, 11, 11);
        cpgIslandsButtonsHorizontalLayout->setObjectName(QString::fromUtf8("cpgIslandsButtonsHorizontalLayout"));
        cpgIslandsButtonsHorizontalLayout->setContentsMargins(0, 0, 0, 0);
        runFindCpgIslandsClearButton = new QPushButton(layoutWidget_16);
        runFindCpgIslandsClearButton->setObjectName(QString::fromUtf8("runFindCpgIslandsClearButton"));

        cpgIslandsButtonsHorizontalLayout->addWidget(runFindCpgIslandsClearButton);

        runFindCpgIslandsButton = new QPushButton(layoutWidget_16);
        runFindCpgIslandsButton->setObjectName(QString::fromUtf8("runFindCpgIslandsButton"));

        cpgIslandsButtonsHorizontalLayout->addWidget(runFindCpgIslandsButton);

        cpgIslandsTabWidget->addTab(cpgIslandsParametersTab, QString());
        cpgIslandsProgressTab = new QWidget();
        cpgIslandsProgressTab->setObjectName(QString::fromUtf8("cpgIslandsProgressTab"));
        cpgIslandsProgressTextEdit = new QTextEdit(cpgIslandsProgressTab);
        cpgIslandsProgressTextEdit->setObjectName(QString::fromUtf8("cpgIslandsProgressTextEdit"));
        cpgIslandsProgressTextEdit->setGeometry(QRect(0, 0, 641, 501));
        cpgIslandsProgressTextEdit->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);
        stopRunFindCpgIslandsProcessButton = new QPushButton(cpgIslandsProgressTab);
        stopRunFindCpgIslandsProcessButton->setObjectName(QString::fromUtf8("stopRunFindCpgIslandsProcessButton"));
        stopRunFindCpgIslandsProcessButton->setEnabled(false);
        stopRunFindCpgIslandsProcessButton->setGeometry(QRect(520, 510, 113, 32));
        cpgIslandsTabWidget->addTab(cpgIslandsProgressTab, QString());

        verticalLayout_8->addWidget(cpgIslandsTabWidget);

        stackedWidget->addWidget(cpgIslandsStackedPage);
        segDuplicatesStackedPage = new QWidget();
        segDuplicatesStackedPage->setObjectName(QString::fromUtf8("segDuplicatesStackedPage"));
        verticalLayout_9 = new QVBoxLayout(segDuplicatesStackedPage);
        verticalLayout_9->setSpacing(6);
        verticalLayout_9->setContentsMargins(11, 11, 11, 11);
        verticalLayout_9->setObjectName(QString::fromUtf8("verticalLayout_9"));
        segDuplicatesTabWidget = new QTabWidget(segDuplicatesStackedPage);
        segDuplicatesTabWidget->setObjectName(QString::fromUtf8("segDuplicatesTabWidget"));
        segDuplicatesParametersTab = new QWidget();
        segDuplicatesParametersTab->setObjectName(QString::fromUtf8("segDuplicatesParametersTab"));
        segDuplicatesTitleLabel = new QLabel(segDuplicatesParametersTab);
        segDuplicatesTitleLabel->setObjectName(QString::fromUtf8("segDuplicatesTitleLabel"));
        segDuplicatesTitleLabel->setGeometry(QRect(0, 10, 588, 21));
        segDuplicatesTitleLabel->setFont(font2);
        segDuplicatesLine1 = new QFrame(segDuplicatesParametersTab);
        segDuplicatesLine1->setObjectName(QString::fromUtf8("segDuplicatesLine1"));
        segDuplicatesLine1->setGeometry(QRect(0, 30, 627, 3));
        segDuplicatesLine1->setFrameShape(QFrame::HLine);
        segDuplicatesLine1->setFrameShadow(QFrame::Sunken);
        layoutWidget_17 = new QWidget(segDuplicatesParametersTab);
        layoutWidget_17->setObjectName(QString::fromUtf8("layoutWidget_17"));
        layoutWidget_17->setGeometry(QRect(0, 40, 641, 111));
        segDuplicatesGridLayout = new QGridLayout(layoutWidget_17);
        segDuplicatesGridLayout->setSpacing(6);
        segDuplicatesGridLayout->setContentsMargins(11, 11, 11, 11);
        segDuplicatesGridLayout->setObjectName(QString::fromUtf8("segDuplicatesGridLayout"));
        segDuplicatesGridLayout->setHorizontalSpacing(-1);
        segDuplicatesGridLayout->setVerticalSpacing(15);
        segDuplicatesGridLayout->setContentsMargins(0, 0, 0, 0);
        segDuplicatesSelectPeaksLabel = new QLabel(layoutWidget_17);
        segDuplicatesSelectPeaksLabel->setObjectName(QString::fromUtf8("segDuplicatesSelectPeaksLabel"));

        segDuplicatesGridLayout->addWidget(segDuplicatesSelectPeaksLabel, 0, 0, 1, 1);

        segDuplicatesSelectPeaksLineEdit = new QLineEdit(layoutWidget_17);
        segDuplicatesSelectPeaksLineEdit->setObjectName(QString::fromUtf8("segDuplicatesSelectPeaksLineEdit"));
        segDuplicatesSelectPeaksLineEdit->setReadOnly(true);

        segDuplicatesGridLayout->addWidget(segDuplicatesSelectPeaksLineEdit, 0, 1, 1, 2);

        segDuplicatesSelectPeaksToolButton = new QToolButton(layoutWidget_17);
        segDuplicatesSelectPeaksToolButton->setObjectName(QString::fromUtf8("segDuplicatesSelectPeaksToolButton"));

        segDuplicatesGridLayout->addWidget(segDuplicatesSelectPeaksToolButton, 0, 3, 1, 1);

        segDuplicatesSelectPeaksInfoButton = new QPushButton(layoutWidget_17);
        segDuplicatesSelectPeaksInfoButton->setObjectName(QString::fromUtf8("segDuplicatesSelectPeaksInfoButton"));
        segDuplicatesSelectPeaksInfoButton->setMaximumSize(QSize(21, 21));
        segDuplicatesSelectPeaksInfoButton->setMouseTracking(false);
        segDuplicatesSelectPeaksInfoButton->setAcceptDrops(false);
        segDuplicatesSelectPeaksInfoButton->setLayoutDirection(Qt::LeftToRight);
        segDuplicatesSelectPeaksInfoButton->setIcon(icon1);
        segDuplicatesSelectPeaksInfoButton->setAutoRepeatDelay(100);
        segDuplicatesSelectPeaksInfoButton->setDefault(false);
        segDuplicatesSelectPeaksInfoButton->setFlat(true);

        segDuplicatesGridLayout->addWidget(segDuplicatesSelectPeaksInfoButton, 0, 4, 1, 1);

        segDuplicatesOutputFileLabel = new QLabel(layoutWidget_17);
        segDuplicatesOutputFileLabel->setObjectName(QString::fromUtf8("segDuplicatesOutputFileLabel"));

        segDuplicatesGridLayout->addWidget(segDuplicatesOutputFileLabel, 2, 0, 1, 1);

        segDuplicatesOutputFileLineEdit = new QLineEdit(layoutWidget_17);
        segDuplicatesOutputFileLineEdit->setObjectName(QString::fromUtf8("segDuplicatesOutputFileLineEdit"));

        segDuplicatesGridLayout->addWidget(segDuplicatesOutputFileLineEdit, 2, 1, 1, 2);

        segDuplicatesSuffixLabel = new QLabel(layoutWidget_17);
        segDuplicatesSuffixLabel->setObjectName(QString::fromUtf8("segDuplicatesSuffixLabel"));

        segDuplicatesGridLayout->addWidget(segDuplicatesSuffixLabel, 2, 3, 1, 1);

        segDuplicatesOutputFileInfoButton = new QPushButton(layoutWidget_17);
        segDuplicatesOutputFileInfoButton->setObjectName(QString::fromUtf8("segDuplicatesOutputFileInfoButton"));
        segDuplicatesOutputFileInfoButton->setMaximumSize(QSize(21, 21));
        segDuplicatesOutputFileInfoButton->setMouseTracking(false);
        segDuplicatesOutputFileInfoButton->setAcceptDrops(false);
        segDuplicatesOutputFileInfoButton->setLayoutDirection(Qt::LeftToRight);
        segDuplicatesOutputFileInfoButton->setIcon(icon1);
        segDuplicatesOutputFileInfoButton->setAutoRepeatDelay(100);
        segDuplicatesOutputFileInfoButton->setDefault(false);
        segDuplicatesOutputFileInfoButton->setFlat(true);

        segDuplicatesGridLayout->addWidget(segDuplicatesOutputFileInfoButton, 2, 4, 1, 1);

        segDuplicatesSpeciesComboBox = new QComboBox(layoutWidget_17);
        segDuplicatesSpeciesComboBox->setObjectName(QString::fromUtf8("segDuplicatesSpeciesComboBox"));
        segDuplicatesSpeciesComboBox->setEnabled(true);
        segDuplicatesSpeciesComboBox->setEditable(false);
        segDuplicatesSpeciesComboBox->setModelColumn(0);

        segDuplicatesGridLayout->addWidget(segDuplicatesSpeciesComboBox, 1, 1, 1, 2);

        segDuplicatesSpeciesInfoButton = new QPushButton(layoutWidget_17);
        segDuplicatesSpeciesInfoButton->setObjectName(QString::fromUtf8("segDuplicatesSpeciesInfoButton"));
        segDuplicatesSpeciesInfoButton->setMaximumSize(QSize(21, 21));
        segDuplicatesSpeciesInfoButton->setMouseTracking(false);
        segDuplicatesSpeciesInfoButton->setAcceptDrops(false);
        segDuplicatesSpeciesInfoButton->setLayoutDirection(Qt::LeftToRight);
        segDuplicatesSpeciesInfoButton->setIcon(icon1);
        segDuplicatesSpeciesInfoButton->setAutoRepeatDelay(100);
        segDuplicatesSpeciesInfoButton->setDefault(false);
        segDuplicatesSpeciesInfoButton->setFlat(true);

        segDuplicatesGridLayout->addWidget(segDuplicatesSpeciesInfoButton, 1, 4, 1, 1);

        segDuplicatesSpeciesLabel = new QLabel(layoutWidget_17);
        segDuplicatesSpeciesLabel->setObjectName(QString::fromUtf8("segDuplicatesSpeciesLabel"));

        segDuplicatesGridLayout->addWidget(segDuplicatesSpeciesLabel, 1, 0, 1, 1);

        segDuplicatesLine2 = new QFrame(segDuplicatesParametersTab);
        segDuplicatesLine2->setObjectName(QString::fromUtf8("segDuplicatesLine2"));
        segDuplicatesLine2->setGeometry(QRect(0, 150, 629, 3));
        segDuplicatesLine2->setFrameShadow(QFrame::Sunken);
        segDuplicatesLine2->setFrameShape(QFrame::HLine);
        layoutWidget_18 = new QWidget(segDuplicatesParametersTab);
        layoutWidget_18->setObjectName(QString::fromUtf8("layoutWidget_18"));
        layoutWidget_18->setGeometry(QRect(380, 150, 261, 41));
        segDuplicatesButtonsHorizontalLayout = new QHBoxLayout(layoutWidget_18);
        segDuplicatesButtonsHorizontalLayout->setSpacing(6);
        segDuplicatesButtonsHorizontalLayout->setContentsMargins(11, 11, 11, 11);
        segDuplicatesButtonsHorizontalLayout->setObjectName(QString::fromUtf8("segDuplicatesButtonsHorizontalLayout"));
        segDuplicatesButtonsHorizontalLayout->setContentsMargins(0, 0, 0, 0);
        runFindSegDuplicatesClearButton = new QPushButton(layoutWidget_18);
        runFindSegDuplicatesClearButton->setObjectName(QString::fromUtf8("runFindSegDuplicatesClearButton"));

        segDuplicatesButtonsHorizontalLayout->addWidget(runFindSegDuplicatesClearButton);

        runFindSegDuplicatesButton = new QPushButton(layoutWidget_18);
        runFindSegDuplicatesButton->setObjectName(QString::fromUtf8("runFindSegDuplicatesButton"));

        segDuplicatesButtonsHorizontalLayout->addWidget(runFindSegDuplicatesButton);

        segDuplicatesTabWidget->addTab(segDuplicatesParametersTab, QString());
        segDuplicatesProgressTab = new QWidget();
        segDuplicatesProgressTab->setObjectName(QString::fromUtf8("segDuplicatesProgressTab"));
        segDuplicatesProgressTextEdit = new QTextEdit(segDuplicatesProgressTab);
        segDuplicatesProgressTextEdit->setObjectName(QString::fromUtf8("segDuplicatesProgressTextEdit"));
        segDuplicatesProgressTextEdit->setGeometry(QRect(0, 0, 641, 501));
        segDuplicatesProgressTextEdit->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);
        stopRunFindSegDuplicatesProcessButton = new QPushButton(segDuplicatesProgressTab);
        stopRunFindSegDuplicatesProcessButton->setObjectName(QString::fromUtf8("stopRunFindSegDuplicatesProcessButton"));
        stopRunFindSegDuplicatesProcessButton->setEnabled(false);
        stopRunFindSegDuplicatesProcessButton->setGeometry(QRect(520, 510, 113, 32));
        segDuplicatesTabWidget->addTab(segDuplicatesProgressTab, QString());

        verticalLayout_9->addWidget(segDuplicatesTabWidget);

        stackedWidget->addWidget(segDuplicatesStackedPage);
        findmotifStackedPage = new QWidget();
        findmotifStackedPage->setObjectName(QString::fromUtf8("findmotifStackedPage"));
        verticalLayout_10 = new QVBoxLayout(findmotifStackedPage);
        verticalLayout_10->setSpacing(6);
        verticalLayout_10->setContentsMargins(11, 11, 11, 11);
        verticalLayout_10->setObjectName(QString::fromUtf8("verticalLayout_10"));
        findmotifTabWidget = new QTabWidget(findmotifStackedPage);
        findmotifTabWidget->setObjectName(QString::fromUtf8("findmotifTabWidget"));
        findmotifParametersTab = new QWidget();
        findmotifParametersTab->setObjectName(QString::fromUtf8("findmotifParametersTab"));
        findmotifTitleLabel = new QLabel(findmotifParametersTab);
        findmotifTitleLabel->setObjectName(QString::fromUtf8("findmotifTitleLabel"));
        findmotifTitleLabel->setGeometry(QRect(0, 10, 588, 21));
        findmotifTitleLabel->setFont(font2);
        findmotifLine1 = new QFrame(findmotifParametersTab);
        findmotifLine1->setObjectName(QString::fromUtf8("findmotifLine1"));
        findmotifLine1->setGeometry(QRect(0, 30, 627, 3));
        findmotifLine1->setFrameShape(QFrame::HLine);
        findmotifLine1->setFrameShadow(QFrame::Sunken);
        layoutWidget_21 = new QWidget(findmotifParametersTab);
        layoutWidget_21->setObjectName(QString::fromUtf8("layoutWidget_21"));
        layoutWidget_21->setGeometry(QRect(0, 40, 641, 281));
        findmotifGridLayout = new QGridLayout(layoutWidget_21);
        findmotifGridLayout->setSpacing(6);
        findmotifGridLayout->setContentsMargins(11, 11, 11, 11);
        findmotifGridLayout->setObjectName(QString::fromUtf8("findmotifGridLayout"));
        findmotifGridLayout->setHorizontalSpacing(-1);
        findmotifGridLayout->setVerticalSpacing(15);
        findmotifGridLayout->setContentsMargins(0, 0, 0, 0);
        findmotifSelectPeaksLabel = new QLabel(layoutWidget_21);
        findmotifSelectPeaksLabel->setObjectName(QString::fromUtf8("findmotifSelectPeaksLabel"));

        findmotifGridLayout->addWidget(findmotifSelectPeaksLabel, 0, 0, 1, 1);

        findmotifSelectPeaksLineEdit = new QLineEdit(layoutWidget_21);
        findmotifSelectPeaksLineEdit->setObjectName(QString::fromUtf8("findmotifSelectPeaksLineEdit"));
        findmotifSelectPeaksLineEdit->setReadOnly(true);

        findmotifGridLayout->addWidget(findmotifSelectPeaksLineEdit, 0, 1, 1, 2);

        findmotifSelectPeaksToolButton = new QToolButton(layoutWidget_21);
        findmotifSelectPeaksToolButton->setObjectName(QString::fromUtf8("findmotifSelectPeaksToolButton"));

        findmotifGridLayout->addWidget(findmotifSelectPeaksToolButton, 0, 3, 1, 1);

        findmotifSelectPeaksInfoButton = new QPushButton(layoutWidget_21);
        findmotifSelectPeaksInfoButton->setObjectName(QString::fromUtf8("findmotifSelectPeaksInfoButton"));
        findmotifSelectPeaksInfoButton->setMaximumSize(QSize(21, 21));
        findmotifSelectPeaksInfoButton->setMouseTracking(false);
        findmotifSelectPeaksInfoButton->setAcceptDrops(false);
        findmotifSelectPeaksInfoButton->setLayoutDirection(Qt::LeftToRight);
        findmotifSelectPeaksInfoButton->setIcon(icon1);
        findmotifSelectPeaksInfoButton->setAutoRepeatDelay(100);
        findmotifSelectPeaksInfoButton->setDefault(false);
        findmotifSelectPeaksInfoButton->setFlat(true);

        findmotifGridLayout->addWidget(findmotifSelectPeaksInfoButton, 0, 4, 1, 1);

        findmotifScoreLabel = new QLabel(layoutWidget_21);
        findmotifScoreLabel->setObjectName(QString::fromUtf8("findmotifScoreLabel"));

        findmotifGridLayout->addWidget(findmotifScoreLabel, 6, 0, 1, 1);

        findmotifScoreSpinBox = new QDoubleSpinBox(layoutWidget_21);
        findmotifScoreSpinBox->setObjectName(QString::fromUtf8("findmotifScoreSpinBox"));
        findmotifScoreSpinBox->setDecimals(1);
        findmotifScoreSpinBox->setMinimum(-100);
        findmotifScoreSpinBox->setMaximum(100);
        findmotifScoreSpinBox->setSingleStep(0.1);
        findmotifScoreSpinBox->setValue(1);

        findmotifGridLayout->addWidget(findmotifScoreSpinBox, 6, 1, 1, 1);

        findmotifOutputFileLabel = new QLabel(layoutWidget_21);
        findmotifOutputFileLabel->setObjectName(QString::fromUtf8("findmotifOutputFileLabel"));

        findmotifGridLayout->addWidget(findmotifOutputFileLabel, 1, 0, 1, 1);

        findmotifOutputFileLineEdit = new QLineEdit(layoutWidget_21);
        findmotifOutputFileLineEdit->setObjectName(QString::fromUtf8("findmotifOutputFileLineEdit"));

        findmotifGridLayout->addWidget(findmotifOutputFileLineEdit, 1, 1, 1, 2);

        findmotifSuffixLabel = new QLabel(layoutWidget_21);
        findmotifSuffixLabel->setObjectName(QString::fromUtf8("findmotifSuffixLabel"));

        findmotifGridLayout->addWidget(findmotifSuffixLabel, 1, 3, 1, 1);

        findmotifOutputFileInfoButton = new QPushButton(layoutWidget_21);
        findmotifOutputFileInfoButton->setObjectName(QString::fromUtf8("findmotifOutputFileInfoButton"));
        findmotifOutputFileInfoButton->setMaximumSize(QSize(21, 21));
        findmotifOutputFileInfoButton->setMouseTracking(false);
        findmotifOutputFileInfoButton->setAcceptDrops(false);
        findmotifOutputFileInfoButton->setLayoutDirection(Qt::LeftToRight);
        findmotifOutputFileInfoButton->setIcon(icon1);
        findmotifOutputFileInfoButton->setAutoRepeatDelay(100);
        findmotifOutputFileInfoButton->setDefault(false);
        findmotifOutputFileInfoButton->setFlat(true);

        findmotifGridLayout->addWidget(findmotifOutputFileInfoButton, 1, 4, 1, 1);

        findmotifScoreInfoButton = new QPushButton(layoutWidget_21);
        findmotifScoreInfoButton->setObjectName(QString::fromUtf8("findmotifScoreInfoButton"));
        findmotifScoreInfoButton->setMaximumSize(QSize(21, 21));
        findmotifScoreInfoButton->setMouseTracking(false);
        findmotifScoreInfoButton->setAcceptDrops(false);
        findmotifScoreInfoButton->setLayoutDirection(Qt::LeftToRight);
        findmotifScoreInfoButton->setIcon(icon1);
        findmotifScoreInfoButton->setAutoRepeatDelay(100);
        findmotifScoreInfoButton->setDefault(false);
        findmotifScoreInfoButton->setFlat(true);

        findmotifGridLayout->addWidget(findmotifScoreInfoButton, 6, 4, 1, 1);

        findmotifGenomeLineEdit = new QLineEdit(layoutWidget_21);
        findmotifGenomeLineEdit->setObjectName(QString::fromUtf8("findmotifGenomeLineEdit"));
        findmotifGenomeLineEdit->setReadOnly(true);

        findmotifGridLayout->addWidget(findmotifGenomeLineEdit, 2, 1, 1, 2);

        findmotifGenomeLabel = new QLabel(layoutWidget_21);
        findmotifGenomeLabel->setObjectName(QString::fromUtf8("findmotifGenomeLabel"));

        findmotifGridLayout->addWidget(findmotifGenomeLabel, 2, 0, 1, 1);

        findmotifGenomeInfoButton = new QPushButton(layoutWidget_21);
        findmotifGenomeInfoButton->setObjectName(QString::fromUtf8("findmotifGenomeInfoButton"));
        findmotifGenomeInfoButton->setMaximumSize(QSize(21, 21));
        findmotifGenomeInfoButton->setMouseTracking(false);
        findmotifGenomeInfoButton->setAcceptDrops(false);
        findmotifGenomeInfoButton->setLayoutDirection(Qt::LeftToRight);
        findmotifGenomeInfoButton->setIcon(icon1);
        findmotifGenomeInfoButton->setAutoRepeatDelay(100);
        findmotifGenomeInfoButton->setDefault(false);
        findmotifGenomeInfoButton->setFlat(true);

        findmotifGridLayout->addWidget(findmotifGenomeInfoButton, 2, 4, 1, 1);

        findmotifGenomeToolButton = new QToolButton(layoutWidget_21);
        findmotifGenomeToolButton->setObjectName(QString::fromUtf8("findmotifGenomeToolButton"));

        findmotifGridLayout->addWidget(findmotifGenomeToolButton, 2, 3, 1, 1);

        findmotifNameRadioButton = new QRadioButton(layoutWidget_21);
        buttonGroup_2 = new QButtonGroup(CSDesktop);
        buttonGroup_2->setObjectName(QString::fromUtf8("buttonGroup_2"));
        buttonGroup_2->addButton(findmotifNameRadioButton);
        findmotifNameRadioButton->setObjectName(QString::fromUtf8("findmotifNameRadioButton"));
        findmotifNameRadioButton->setChecked(false);

        findmotifGridLayout->addWidget(findmotifNameRadioButton, 4, 0, 1, 1);

        findmotifNameInfoButton = new QPushButton(layoutWidget_21);
        findmotifNameInfoButton->setObjectName(QString::fromUtf8("findmotifNameInfoButton"));
        findmotifNameInfoButton->setMaximumSize(QSize(21, 21));
        findmotifNameInfoButton->setMouseTracking(false);
        findmotifNameInfoButton->setAcceptDrops(false);
        findmotifNameInfoButton->setLayoutDirection(Qt::LeftToRight);
        findmotifNameInfoButton->setIcon(icon1);
        findmotifNameInfoButton->setAutoRepeatDelay(100);
        findmotifNameInfoButton->setDefault(false);
        findmotifNameInfoButton->setFlat(true);

        findmotifGridLayout->addWidget(findmotifNameInfoButton, 4, 4, 1, 1);

        findmotifJasparRadioButton = new QRadioButton(layoutWidget_21);
        buttonGroup_3 = new QButtonGroup(CSDesktop);
        buttonGroup_3->setObjectName(QString::fromUtf8("buttonGroup_3"));
        buttonGroup_3->addButton(findmotifJasparRadioButton);
        findmotifJasparRadioButton->setObjectName(QString::fromUtf8("findmotifJasparRadioButton"));
        findmotifJasparRadioButton->setEnabled(false);
        findmotifJasparRadioButton->setChecked(false);

        findmotifGridLayout->addWidget(findmotifJasparRadioButton, 4, 2, 1, 1);

        findmotifJasparMotifComboBox = new QComboBox(layoutWidget_21);
        findmotifJasparMotifComboBox->setObjectName(QString::fromUtf8("findmotifJasparMotifComboBox"));
        findmotifJasparMotifComboBox->setEnabled(false);
        findmotifJasparMotifComboBox->setEditable(false);
        findmotifJasparMotifComboBox->setModelColumn(0);

        findmotifGridLayout->addWidget(findmotifJasparMotifComboBox, 5, 2, 1, 1);

        findmotifBulykRadioButton = new QRadioButton(layoutWidget_21);
        buttonGroup_3->addButton(findmotifBulykRadioButton);
        findmotifBulykRadioButton->setObjectName(QString::fromUtf8("findmotifBulykRadioButton"));
        findmotifBulykRadioButton->setEnabled(false);
        findmotifBulykRadioButton->setChecked(false);

        findmotifGridLayout->addWidget(findmotifBulykRadioButton, 4, 1, 1, 1);

        findmotifBulykMotifComboBox = new QComboBox(layoutWidget_21);
        findmotifBulykMotifComboBox->setObjectName(QString::fromUtf8("findmotifBulykMotifComboBox"));
        findmotifBulykMotifComboBox->setEnabled(false);
        findmotifBulykMotifComboBox->setEditable(false);
        findmotifBulykMotifComboBox->setModelColumn(0);

        findmotifGridLayout->addWidget(findmotifBulykMotifComboBox, 5, 1, 1, 1);

        findmotifSequenceRadioButton = new QRadioButton(layoutWidget_21);
        buttonGroup_2->addButton(findmotifSequenceRadioButton);
        findmotifSequenceRadioButton->setObjectName(QString::fromUtf8("findmotifSequenceRadioButton"));
        findmotifSequenceRadioButton->setChecked(true);

        findmotifGridLayout->addWidget(findmotifSequenceRadioButton, 3, 0, 1, 1);

        findmotifSequenceLineEdit = new QLineEdit(layoutWidget_21);
        findmotifSequenceLineEdit->setObjectName(QString::fromUtf8("findmotifSequenceLineEdit"));
        findmotifSequenceLineEdit->setEnabled(true);

        findmotifGridLayout->addWidget(findmotifSequenceLineEdit, 3, 1, 1, 2);

        findmotifSequenceInfoButton = new QPushButton(layoutWidget_21);
        findmotifSequenceInfoButton->setObjectName(QString::fromUtf8("findmotifSequenceInfoButton"));
        findmotifSequenceInfoButton->setMaximumSize(QSize(21, 21));
        findmotifSequenceInfoButton->setMouseTracking(false);
        findmotifSequenceInfoButton->setAcceptDrops(false);
        findmotifSequenceInfoButton->setLayoutDirection(Qt::LeftToRight);
        findmotifSequenceInfoButton->setIcon(icon1);
        findmotifSequenceInfoButton->setAutoRepeatDelay(100);
        findmotifSequenceInfoButton->setDefault(false);
        findmotifSequenceInfoButton->setFlat(true);

        findmotifGridLayout->addWidget(findmotifSequenceInfoButton, 3, 4, 1, 1);

        findmotifLine2 = new QFrame(findmotifParametersTab);
        findmotifLine2->setObjectName(QString::fromUtf8("findmotifLine2"));
        findmotifLine2->setGeometry(QRect(0, 320, 629, 3));
        findmotifLine2->setFrameShadow(QFrame::Sunken);
        findmotifLine2->setFrameShape(QFrame::HLine);
        layoutWidget_22 = new QWidget(findmotifParametersTab);
        layoutWidget_22->setObjectName(QString::fromUtf8("layoutWidget_22"));
        layoutWidget_22->setGeometry(QRect(400, 320, 241, 41));
        findmotifButtonsHorizontalLayout = new QHBoxLayout(layoutWidget_22);
        findmotifButtonsHorizontalLayout->setSpacing(6);
        findmotifButtonsHorizontalLayout->setContentsMargins(11, 11, 11, 11);
        findmotifButtonsHorizontalLayout->setObjectName(QString::fromUtf8("findmotifButtonsHorizontalLayout"));
        findmotifButtonsHorizontalLayout->setContentsMargins(0, 0, 0, 0);
        runFindmotifClearButton = new QPushButton(layoutWidget_22);
        runFindmotifClearButton->setObjectName(QString::fromUtf8("runFindmotifClearButton"));

        findmotifButtonsHorizontalLayout->addWidget(runFindmotifClearButton);

        runFindmotifButton = new QPushButton(layoutWidget_22);
        runFindmotifButton->setObjectName(QString::fromUtf8("runFindmotifButton"));

        findmotifButtonsHorizontalLayout->addWidget(runFindmotifButton);

        findmotifTabWidget->addTab(findmotifParametersTab, QString());
        findmotifProgressTab = new QWidget();
        findmotifProgressTab->setObjectName(QString::fromUtf8("findmotifProgressTab"));
        findmotifProgressTextEdit = new QTextEdit(findmotifProgressTab);
        findmotifProgressTextEdit->setObjectName(QString::fromUtf8("findmotifProgressTextEdit"));
        findmotifProgressTextEdit->setGeometry(QRect(0, 0, 641, 501));
        findmotifProgressTextEdit->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);
        stopRunFindmotifProcessButton = new QPushButton(findmotifProgressTab);
        stopRunFindmotifProcessButton->setObjectName(QString::fromUtf8("stopRunFindmotifProcessButton"));
        stopRunFindmotifProcessButton->setEnabled(false);
        stopRunFindmotifProcessButton->setGeometry(QRect(520, 510, 113, 32));
        findmotifTabWidget->addTab(findmotifProgressTab, QString());

        verticalLayout_10->addWidget(findmotifTabWidget);

        stackedWidget->addWidget(findmotifStackedPage);
        fireStackedPage = new QWidget();
        fireStackedPage->setObjectName(QString::fromUtf8("fireStackedPage"));
        verticalLayout_14 = new QVBoxLayout(fireStackedPage);
        verticalLayout_14->setSpacing(6);
        verticalLayout_14->setContentsMargins(11, 11, 11, 11);
        verticalLayout_14->setObjectName(QString::fromUtf8("verticalLayout_14"));
        fireTabWidget = new QTabWidget(fireStackedPage);
        fireTabWidget->setObjectName(QString::fromUtf8("fireTabWidget"));
        fireParametersTab = new QWidget();
        fireParametersTab->setObjectName(QString::fromUtf8("fireParametersTab"));
        fireTitleLabel = new QLabel(fireParametersTab);
        fireTitleLabel->setObjectName(QString::fromUtf8("fireTitleLabel"));
        fireTitleLabel->setGeometry(QRect(0, 10, 588, 21));
        fireTitleLabel->setFont(font2);
        fireLine1 = new QFrame(fireParametersTab);
        fireLine1->setObjectName(QString::fromUtf8("fireLine1"));
        fireLine1->setGeometry(QRect(0, 30, 627, 3));
        fireLine1->setFrameShape(QFrame::HLine);
        fireLine1->setFrameShadow(QFrame::Sunken);
        layoutWidget_29 = new QWidget(fireParametersTab);
        layoutWidget_29->setObjectName(QString::fromUtf8("layoutWidget_29"));
        layoutWidget_29->setGeometry(QRect(0, 40, 641, 251));
        fireGridLayout = new QGridLayout(layoutWidget_29);
        fireGridLayout->setSpacing(6);
        fireGridLayout->setContentsMargins(11, 11, 11, 11);
        fireGridLayout->setObjectName(QString::fromUtf8("fireGridLayout"));
        fireGridLayout->setHorizontalSpacing(-1);
        fireGridLayout->setVerticalSpacing(15);
        fireGridLayout->setContentsMargins(0, 0, 0, 0);
        fireSelectPeaksLabel = new QLabel(layoutWidget_29);
        fireSelectPeaksLabel->setObjectName(QString::fromUtf8("fireSelectPeaksLabel"));

        fireGridLayout->addWidget(fireSelectPeaksLabel, 0, 0, 1, 1);

        fireSelectPeaksLineEdit = new QLineEdit(layoutWidget_29);
        fireSelectPeaksLineEdit->setObjectName(QString::fromUtf8("fireSelectPeaksLineEdit"));
        fireSelectPeaksLineEdit->setReadOnly(true);

        fireGridLayout->addWidget(fireSelectPeaksLineEdit, 0, 1, 1, 2);

        fireSelectPeaksToolButton = new QToolButton(layoutWidget_29);
        fireSelectPeaksToolButton->setObjectName(QString::fromUtf8("fireSelectPeaksToolButton"));

        fireGridLayout->addWidget(fireSelectPeaksToolButton, 0, 3, 1, 1);

        fireSelectPeaksInfoButton = new QPushButton(layoutWidget_29);
        fireSelectPeaksInfoButton->setObjectName(QString::fromUtf8("fireSelectPeaksInfoButton"));
        fireSelectPeaksInfoButton->setMaximumSize(QSize(21, 21));
        fireSelectPeaksInfoButton->setMouseTracking(false);
        fireSelectPeaksInfoButton->setAcceptDrops(false);
        fireSelectPeaksInfoButton->setLayoutDirection(Qt::LeftToRight);
        fireSelectPeaksInfoButton->setIcon(icon1);
        fireSelectPeaksInfoButton->setAutoRepeatDelay(100);
        fireSelectPeaksInfoButton->setDefault(false);
        fireSelectPeaksInfoButton->setFlat(true);

        fireGridLayout->addWidget(fireSelectPeaksInfoButton, 0, 4, 1, 1);

        fireGenomeLineEdit = new QLineEdit(layoutWidget_29);
        fireGenomeLineEdit->setObjectName(QString::fromUtf8("fireGenomeLineEdit"));
        fireGenomeLineEdit->setReadOnly(true);

        fireGridLayout->addWidget(fireGenomeLineEdit, 2, 1, 1, 2);

        fireGenomeLabel = new QLabel(layoutWidget_29);
        fireGenomeLabel->setObjectName(QString::fromUtf8("fireGenomeLabel"));

        fireGridLayout->addWidget(fireGenomeLabel, 2, 0, 1, 1);

        fireGenomeInfoButton = new QPushButton(layoutWidget_29);
        fireGenomeInfoButton->setObjectName(QString::fromUtf8("fireGenomeInfoButton"));
        fireGenomeInfoButton->setMaximumSize(QSize(21, 21));
        fireGenomeInfoButton->setMouseTracking(false);
        fireGenomeInfoButton->setAcceptDrops(false);
        fireGenomeInfoButton->setLayoutDirection(Qt::LeftToRight);
        fireGenomeInfoButton->setIcon(icon1);
        fireGenomeInfoButton->setAutoRepeatDelay(100);
        fireGenomeInfoButton->setDefault(false);
        fireGenomeInfoButton->setFlat(true);

        fireGridLayout->addWidget(fireGenomeInfoButton, 2, 4, 1, 1);

        fireGenomeToolButton = new QToolButton(layoutWidget_29);
        fireGenomeToolButton->setObjectName(QString::fromUtf8("fireGenomeToolButton"));

        fireGridLayout->addWidget(fireGenomeToolButton, 2, 3, 1, 1);

        fireSpeciesLabel = new QLabel(layoutWidget_29);
        fireSpeciesLabel->setObjectName(QString::fromUtf8("fireSpeciesLabel"));

        fireGridLayout->addWidget(fireSpeciesLabel, 3, 0, 1, 1);

        fireSpeciesComboBox = new QComboBox(layoutWidget_29);
        fireSpeciesComboBox->setObjectName(QString::fromUtf8("fireSpeciesComboBox"));
        fireSpeciesComboBox->setEnabled(true);
        fireSpeciesComboBox->setEditable(false);
        fireSpeciesComboBox->setModelColumn(0);

        fireGridLayout->addWidget(fireSpeciesComboBox, 3, 1, 1, 2);

        fireSpeciesInfoButton = new QPushButton(layoutWidget_29);
        fireSpeciesInfoButton->setObjectName(QString::fromUtf8("fireSpeciesInfoButton"));
        fireSpeciesInfoButton->setMaximumSize(QSize(21, 21));
        fireSpeciesInfoButton->setMouseTracking(false);
        fireSpeciesInfoButton->setAcceptDrops(false);
        fireSpeciesInfoButton->setLayoutDirection(Qt::LeftToRight);
        fireSpeciesInfoButton->setIcon(icon1);
        fireSpeciesInfoButton->setAutoRepeatDelay(100);
        fireSpeciesInfoButton->setDefault(false);
        fireSpeciesInfoButton->setFlat(true);

        fireGridLayout->addWidget(fireSpeciesInfoButton, 3, 4, 1, 1);

        fireSelectPeaksFolderInfoButton = new QPushButton(layoutWidget_29);
        fireSelectPeaksFolderInfoButton->setObjectName(QString::fromUtf8("fireSelectPeaksFolderInfoButton"));
        fireSelectPeaksFolderInfoButton->setMaximumSize(QSize(21, 21));
        fireSelectPeaksFolderInfoButton->setMouseTracking(false);
        fireSelectPeaksFolderInfoButton->setAcceptDrops(false);
        fireSelectPeaksFolderInfoButton->setLayoutDirection(Qt::LeftToRight);
        fireSelectPeaksFolderInfoButton->setIcon(icon1);
        fireSelectPeaksFolderInfoButton->setAutoRepeatDelay(100);
        fireSelectPeaksFolderInfoButton->setDefault(false);
        fireSelectPeaksFolderInfoButton->setFlat(true);

        fireGridLayout->addWidget(fireSelectPeaksFolderInfoButton, 1, 4, 1, 1);

        fireSelectPeaksFolderToolButton = new QToolButton(layoutWidget_29);
        fireSelectPeaksFolderToolButton->setObjectName(QString::fromUtf8("fireSelectPeaksFolderToolButton"));

        fireGridLayout->addWidget(fireSelectPeaksFolderToolButton, 1, 3, 1, 1);

        fireSelectPeaksFolderLabel = new QLabel(layoutWidget_29);
        fireSelectPeaksFolderLabel->setObjectName(QString::fromUtf8("fireSelectPeaksFolderLabel"));

        fireGridLayout->addWidget(fireSelectPeaksFolderLabel, 1, 0, 1, 1);

        fireSelectPeaksFolderLineEdit = new QLineEdit(layoutWidget_29);
        fireSelectPeaksFolderLineEdit->setObjectName(QString::fromUtf8("fireSelectPeaksFolderLineEdit"));
        fireSelectPeaksFolderLineEdit->setReadOnly(true);

        fireGridLayout->addWidget(fireSelectPeaksFolderLineEdit, 1, 1, 1, 2);

        fireRandomeModeLabel = new QLabel(layoutWidget_29);
        fireRandomeModeLabel->setObjectName(QString::fromUtf8("fireRandomeModeLabel"));

        fireGridLayout->addWidget(fireRandomeModeLabel, 4, 0, 1, 1);

        fireRandomModeComboBox = new QComboBox(layoutWidget_29);
        fireRandomModeComboBox->setObjectName(QString::fromUtf8("fireRandomModeComboBox"));
        fireRandomModeComboBox->setEnabled(true);
        fireRandomModeComboBox->setEditable(false);
        fireRandomModeComboBox->setModelColumn(0);

        fireGridLayout->addWidget(fireRandomModeComboBox, 4, 1, 1, 2);

        fireRandmodeInfoButton = new QPushButton(layoutWidget_29);
        fireRandmodeInfoButton->setObjectName(QString::fromUtf8("fireRandmodeInfoButton"));
        fireRandmodeInfoButton->setMaximumSize(QSize(21, 21));
        fireRandmodeInfoButton->setMouseTracking(false);
        fireRandmodeInfoButton->setAcceptDrops(false);
        fireRandmodeInfoButton->setLayoutDirection(Qt::LeftToRight);
        fireRandmodeInfoButton->setIcon(icon1);
        fireRandmodeInfoButton->setAutoRepeatDelay(100);
        fireRandmodeInfoButton->setDefault(false);
        fireRandmodeInfoButton->setFlat(true);

        fireGridLayout->addWidget(fireRandmodeInfoButton, 4, 4, 1, 1);

        fireSeedCheckBox = new QCheckBox(layoutWidget_29);
        fireSeedCheckBox->setObjectName(QString::fromUtf8("fireSeedCheckBox"));
        fireSeedCheckBox->setLayoutDirection(Qt::LeftToRight);
        fireSeedCheckBox->setChecked(true);

        fireGridLayout->addWidget(fireSeedCheckBox, 5, 0, 1, 2);

        fireSeedInfoButton = new QPushButton(layoutWidget_29);
        fireSeedInfoButton->setObjectName(QString::fromUtf8("fireSeedInfoButton"));
        fireSeedInfoButton->setMaximumSize(QSize(21, 21));
        fireSeedInfoButton->setMouseTracking(false);
        fireSeedInfoButton->setAcceptDrops(false);
        fireSeedInfoButton->setLayoutDirection(Qt::LeftToRight);
        fireSeedInfoButton->setIcon(icon1);
        fireSeedInfoButton->setAutoRepeatDelay(100);
        fireSeedInfoButton->setDefault(false);
        fireSeedInfoButton->setFlat(true);

        fireGridLayout->addWidget(fireSeedInfoButton, 5, 4, 1, 1);

        fireLine2 = new QFrame(fireParametersTab);
        fireLine2->setObjectName(QString::fromUtf8("fireLine2"));
        fireLine2->setGeometry(QRect(0, 290, 629, 3));
        fireLine2->setFrameShadow(QFrame::Sunken);
        fireLine2->setFrameShape(QFrame::HLine);
        layoutWidget_30 = new QWidget(fireParametersTab);
        layoutWidget_30->setObjectName(QString::fromUtf8("layoutWidget_30"));
        layoutWidget_30->setGeometry(QRect(400, 290, 241, 41));
        fireButtonsHorizontalLayout = new QHBoxLayout(layoutWidget_30);
        fireButtonsHorizontalLayout->setSpacing(6);
        fireButtonsHorizontalLayout->setContentsMargins(11, 11, 11, 11);
        fireButtonsHorizontalLayout->setObjectName(QString::fromUtf8("fireButtonsHorizontalLayout"));
        fireButtonsHorizontalLayout->setContentsMargins(0, 0, 0, 0);
        runFIREClearButton = new QPushButton(layoutWidget_30);
        runFIREClearButton->setObjectName(QString::fromUtf8("runFIREClearButton"));

        fireButtonsHorizontalLayout->addWidget(runFIREClearButton);

        runFIREButton = new QPushButton(layoutWidget_30);
        runFIREButton->setObjectName(QString::fromUtf8("runFIREButton"));

        fireButtonsHorizontalLayout->addWidget(runFIREButton);

        fireTabWidget->addTab(fireParametersTab, QString());
        fireProgressTab = new QWidget();
        fireProgressTab->setObjectName(QString::fromUtf8("fireProgressTab"));
        fireProgressTextEdit = new QTextEdit(fireProgressTab);
        fireProgressTextEdit->setObjectName(QString::fromUtf8("fireProgressTextEdit"));
        fireProgressTextEdit->setGeometry(QRect(0, 0, 641, 501));
        fireProgressTextEdit->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);
        stopRunFIREProcessButton = new QPushButton(fireProgressTab);
        stopRunFIREProcessButton->setObjectName(QString::fromUtf8("stopRunFIREProcessButton"));
        stopRunFIREProcessButton->setEnabled(false);
        stopRunFIREProcessButton->setGeometry(QRect(520, 510, 113, 32));
        fireTabWidget->addTab(fireProgressTab, QString());

        verticalLayout_14->addWidget(fireTabWidget);

        stackedWidget->addWidget(fireStackedPage);
        findPathwayStackedPage = new QWidget();
        findPathwayStackedPage->setObjectName(QString::fromUtf8("findPathwayStackedPage"));
        verticalLayout_3 = new QVBoxLayout(findPathwayStackedPage);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        findPathwayTabWidget = new QTabWidget(findPathwayStackedPage);
        findPathwayTabWidget->setObjectName(QString::fromUtf8("findPathwayTabWidget"));
        findPathwayParametersTab = new QWidget();
        findPathwayParametersTab->setObjectName(QString::fromUtf8("findPathwayParametersTab"));
        findPathwayTitleLabel = new QLabel(findPathwayParametersTab);
        findPathwayTitleLabel->setObjectName(QString::fromUtf8("findPathwayTitleLabel"));
        findPathwayTitleLabel->setGeometry(QRect(0, 10, 588, 21));
        findPathwayTitleLabel->setFont(font2);
        findPathwayLine1 = new QFrame(findPathwayParametersTab);
        findPathwayLine1->setObjectName(QString::fromUtf8("findPathwayLine1"));
        findPathwayLine1->setGeometry(QRect(0, 30, 627, 3));
        findPathwayLine1->setFrameShape(QFrame::HLine);
        findPathwayLine1->setFrameShadow(QFrame::Sunken);
        layoutWidget_31 = new QWidget(findPathwayParametersTab);
        layoutWidget_31->setObjectName(QString::fromUtf8("layoutWidget_31"));
        layoutWidget_31->setGeometry(QRect(0, 40, 641, 431));
        findPathwayGridLayout = new QGridLayout(layoutWidget_31);
        findPathwayGridLayout->setSpacing(6);
        findPathwayGridLayout->setContentsMargins(11, 11, 11, 11);
        findPathwayGridLayout->setObjectName(QString::fromUtf8("findPathwayGridLayout"));
        findPathwayGridLayout->setHorizontalSpacing(-1);
        findPathwayGridLayout->setVerticalSpacing(15);
        findPathwayGridLayout->setContentsMargins(0, 0, 0, 0);
        findPathwaySelectPeaksLabel = new QLabel(layoutWidget_31);
        findPathwaySelectPeaksLabel->setObjectName(QString::fromUtf8("findPathwaySelectPeaksLabel"));

        findPathwayGridLayout->addWidget(findPathwaySelectPeaksLabel, 0, 0, 1, 2);

        findPathwaySelectPeaksLineEdit = new QLineEdit(layoutWidget_31);
        findPathwaySelectPeaksLineEdit->setObjectName(QString::fromUtf8("findPathwaySelectPeaksLineEdit"));
        findPathwaySelectPeaksLineEdit->setReadOnly(true);

        findPathwayGridLayout->addWidget(findPathwaySelectPeaksLineEdit, 0, 2, 1, 4);

        findPathwaySelectPeaksInfoButton = new QPushButton(layoutWidget_31);
        findPathwaySelectPeaksInfoButton->setObjectName(QString::fromUtf8("findPathwaySelectPeaksInfoButton"));
        findPathwaySelectPeaksInfoButton->setMaximumSize(QSize(21, 21));
        findPathwaySelectPeaksInfoButton->setMouseTracking(false);
        findPathwaySelectPeaksInfoButton->setAcceptDrops(false);
        findPathwaySelectPeaksInfoButton->setLayoutDirection(Qt::LeftToRight);
        findPathwaySelectPeaksInfoButton->setIcon(icon1);
        findPathwaySelectPeaksInfoButton->setAutoRepeatDelay(100);
        findPathwaySelectPeaksInfoButton->setDefault(false);
        findPathwaySelectPeaksInfoButton->setFlat(true);

        findPathwayGridLayout->addWidget(findPathwaySelectPeaksInfoButton, 0, 7, 1, 1);

        findPathwayOutputFileLabel = new QLabel(layoutWidget_31);
        findPathwayOutputFileLabel->setObjectName(QString::fromUtf8("findPathwayOutputFileLabel"));

        findPathwayGridLayout->addWidget(findPathwayOutputFileLabel, 1, 0, 1, 2);

        findPathwayOutputFileLineEdit = new QLineEdit(layoutWidget_31);
        findPathwayOutputFileLineEdit->setObjectName(QString::fromUtf8("findPathwayOutputFileLineEdit"));

        findPathwayGridLayout->addWidget(findPathwayOutputFileLineEdit, 1, 2, 1, 3);

        findPathwayOutputFileInfoButton = new QPushButton(layoutWidget_31);
        findPathwayOutputFileInfoButton->setObjectName(QString::fromUtf8("findPathwayOutputFileInfoButton"));
        findPathwayOutputFileInfoButton->setMaximumSize(QSize(21, 21));
        findPathwayOutputFileInfoButton->setMouseTracking(false);
        findPathwayOutputFileInfoButton->setAcceptDrops(false);
        findPathwayOutputFileInfoButton->setLayoutDirection(Qt::LeftToRight);
        findPathwayOutputFileInfoButton->setIcon(icon1);
        findPathwayOutputFileInfoButton->setAutoRepeatDelay(100);
        findPathwayOutputFileInfoButton->setDefault(false);
        findPathwayOutputFileInfoButton->setFlat(true);

        findPathwayGridLayout->addWidget(findPathwayOutputFileInfoButton, 1, 7, 1, 1);

        findPathwayWritePathwayLineEdit = new QLineEdit(layoutWidget_31);
        findPathwayWritePathwayLineEdit->setObjectName(QString::fromUtf8("findPathwayWritePathwayLineEdit"));
        findPathwayWritePathwayLineEdit->setEnabled(true);

        findPathwayGridLayout->addWidget(findPathwayWritePathwayLineEdit, 9, 2, 1, 4);

        findPathwayWritePathwayInfoButton = new QPushButton(layoutWidget_31);
        findPathwayWritePathwayInfoButton->setObjectName(QString::fromUtf8("findPathwayWritePathwayInfoButton"));
        findPathwayWritePathwayInfoButton->setMaximumSize(QSize(21, 21));
        findPathwayWritePathwayInfoButton->setMouseTracking(false);
        findPathwayWritePathwayInfoButton->setAcceptDrops(false);
        findPathwayWritePathwayInfoButton->setLayoutDirection(Qt::LeftToRight);
        findPathwayWritePathwayInfoButton->setIcon(icon1);
        findPathwayWritePathwayInfoButton->setAutoRepeatDelay(100);
        findPathwayWritePathwayInfoButton->setDefault(false);
        findPathwayWritePathwayInfoButton->setFlat(true);

        findPathwayGridLayout->addWidget(findPathwayWritePathwayInfoButton, 9, 7, 1, 1);

        findPathwaySelectDatabaseLabel = new QLabel(layoutWidget_31);
        findPathwaySelectDatabaseLabel->setObjectName(QString::fromUtf8("findPathwaySelectDatabaseLabel"));

        findPathwayGridLayout->addWidget(findPathwaySelectDatabaseLabel, 8, 0, 1, 2);

        findPathwaySelectDatabaseInfoButton = new QPushButton(layoutWidget_31);
        findPathwaySelectDatabaseInfoButton->setObjectName(QString::fromUtf8("findPathwaySelectDatabaseInfoButton"));
        findPathwaySelectDatabaseInfoButton->setMaximumSize(QSize(21, 21));
        findPathwaySelectDatabaseInfoButton->setMouseTracking(false);
        findPathwaySelectDatabaseInfoButton->setAcceptDrops(false);
        findPathwaySelectDatabaseInfoButton->setLayoutDirection(Qt::LeftToRight);
        findPathwaySelectDatabaseInfoButton->setIcon(icon1);
        findPathwaySelectDatabaseInfoButton->setAutoRepeatDelay(100);
        findPathwaySelectDatabaseInfoButton->setDefault(false);
        findPathwaySelectDatabaseInfoButton->setFlat(true);

        findPathwayGridLayout->addWidget(findPathwaySelectDatabaseInfoButton, 8, 7, 1, 1);

        findPathwaySelectDatabaseComboBox = new QComboBox(layoutWidget_31);
        findPathwaySelectDatabaseComboBox->setObjectName(QString::fromUtf8("findPathwaySelectDatabaseComboBox"));
        findPathwaySelectDatabaseComboBox->setEnabled(true);
        findPathwaySelectDatabaseComboBox->setEditable(false);
        findPathwaySelectDatabaseComboBox->setModelColumn(0);

        findPathwayGridLayout->addWidget(findPathwaySelectDatabaseComboBox, 8, 2, 1, 4);

        findPathwaySpeciesLabel = new QLabel(layoutWidget_31);
        findPathwaySpeciesLabel->setObjectName(QString::fromUtf8("findPathwaySpeciesLabel"));

        findPathwayGridLayout->addWidget(findPathwaySpeciesLabel, 2, 0, 1, 2);

        findPathwaySpeciesComboBox = new QComboBox(layoutWidget_31);
        findPathwaySpeciesComboBox->setObjectName(QString::fromUtf8("findPathwaySpeciesComboBox"));
        findPathwaySpeciesComboBox->setEnabled(true);
        findPathwaySpeciesComboBox->setEditable(false);
        findPathwaySpeciesComboBox->setModelColumn(0);

        findPathwayGridLayout->addWidget(findPathwaySpeciesComboBox, 2, 2, 1, 4);

        findPathwaySpeciesInfoButton = new QPushButton(layoutWidget_31);
        findPathwaySpeciesInfoButton->setObjectName(QString::fromUtf8("findPathwaySpeciesInfoButton"));
        findPathwaySpeciesInfoButton->setMaximumSize(QSize(21, 21));
        findPathwaySpeciesInfoButton->setMouseTracking(false);
        findPathwaySpeciesInfoButton->setAcceptDrops(false);
        findPathwaySpeciesInfoButton->setLayoutDirection(Qt::LeftToRight);
        findPathwaySpeciesInfoButton->setIcon(icon1);
        findPathwaySpeciesInfoButton->setAutoRepeatDelay(100);
        findPathwaySpeciesInfoButton->setDefault(false);
        findPathwaySpeciesInfoButton->setFlat(true);

        findPathwayGridLayout->addWidget(findPathwaySpeciesInfoButton, 2, 7, 1, 1);

        findPathwaySelectPathwayComboBox = new QComboBox(layoutWidget_31);
        findPathwaySelectPathwayComboBox->setObjectName(QString::fromUtf8("findPathwaySelectPathwayComboBox"));
        findPathwaySelectPathwayComboBox->setEnabled(false);
        findPathwaySelectPathwayComboBox->setEditable(false);
        findPathwaySelectPathwayComboBox->setModelColumn(0);

        findPathwayGridLayout->addWidget(findPathwaySelectPathwayComboBox, 10, 2, 1, 4);

        findPathwaySelectPathwayInfoButton = new QPushButton(layoutWidget_31);
        findPathwaySelectPathwayInfoButton->setObjectName(QString::fromUtf8("findPathwaySelectPathwayInfoButton"));
        findPathwaySelectPathwayInfoButton->setMaximumSize(QSize(21, 21));
        findPathwaySelectPathwayInfoButton->setMouseTracking(false);
        findPathwaySelectPathwayInfoButton->setAcceptDrops(false);
        findPathwaySelectPathwayInfoButton->setLayoutDirection(Qt::LeftToRight);
        findPathwaySelectPathwayInfoButton->setIcon(icon1);
        findPathwaySelectPathwayInfoButton->setAutoRepeatDelay(100);
        findPathwaySelectPathwayInfoButton->setDefault(false);
        findPathwaySelectPathwayInfoButton->setFlat(true);

        findPathwayGridLayout->addWidget(findPathwaySelectPathwayInfoButton, 10, 7, 1, 1);

        findPathwayWritePathwayRadioButton = new QRadioButton(layoutWidget_31);
        buttonGroup = new QButtonGroup(CSDesktop);
        buttonGroup->setObjectName(QString::fromUtf8("buttonGroup"));
        buttonGroup->addButton(findPathwayWritePathwayRadioButton);
        findPathwayWritePathwayRadioButton->setObjectName(QString::fromUtf8("findPathwayWritePathwayRadioButton"));
        findPathwayWritePathwayRadioButton->setChecked(true);

        findPathwayGridLayout->addWidget(findPathwayWritePathwayRadioButton, 9, 0, 1, 2);

        findPathwaySelectPathwayRadioButton = new QRadioButton(layoutWidget_31);
        buttonGroup->addButton(findPathwaySelectPathwayRadioButton);
        findPathwaySelectPathwayRadioButton->setObjectName(QString::fromUtf8("findPathwaySelectPathwayRadioButton"));

        findPathwayGridLayout->addWidget(findPathwaySelectPathwayRadioButton, 10, 0, 1, 2);

        findPathwaysPromotersCheckBox = new QCheckBox(layoutWidget_31);
        findPathwaysPromotersCheckBox->setObjectName(QString::fromUtf8("findPathwaysPromotersCheckBox"));
        findPathwaysPromotersCheckBox->setChecked(true);

        findPathwayGridLayout->addWidget(findPathwaysPromotersCheckBox, 4, 2, 1, 2);

        findPathwaySelectGenepartsLabel = new QLabel(layoutWidget_31);
        findPathwaySelectGenepartsLabel->setObjectName(QString::fromUtf8("findPathwaySelectGenepartsLabel"));

        findPathwayGridLayout->addWidget(findPathwaySelectGenepartsLabel, 4, 0, 1, 2);

        findPathwaysSelectGenepartsInfoButton = new QPushButton(layoutWidget_31);
        findPathwaysSelectGenepartsInfoButton->setObjectName(QString::fromUtf8("findPathwaysSelectGenepartsInfoButton"));
        findPathwaysSelectGenepartsInfoButton->setMaximumSize(QSize(21, 21));
        findPathwaysSelectGenepartsInfoButton->setMouseTracking(false);
        findPathwaysSelectGenepartsInfoButton->setAcceptDrops(false);
        findPathwaysSelectGenepartsInfoButton->setLayoutDirection(Qt::LeftToRight);
        findPathwaysSelectGenepartsInfoButton->setIcon(icon1);
        findPathwaysSelectGenepartsInfoButton->setAutoRepeatDelay(100);
        findPathwaysSelectGenepartsInfoButton->setDefault(false);
        findPathwaysSelectGenepartsInfoButton->setFlat(true);

        findPathwayGridLayout->addWidget(findPathwaysSelectGenepartsInfoButton, 4, 7, 1, 1);

        findPathwaySelectPeaksToolButton = new QToolButton(layoutWidget_31);
        findPathwaySelectPeaksToolButton->setObjectName(QString::fromUtf8("findPathwaySelectPeaksToolButton"));

        findPathwayGridLayout->addWidget(findPathwaySelectPeaksToolButton, 0, 6, 1, 1);

        findPathwaysIntronsCheckBox = new QCheckBox(layoutWidget_31);
        findPathwaysIntronsCheckBox->setObjectName(QString::fromUtf8("findPathwaysIntronsCheckBox"));

        findPathwayGridLayout->addWidget(findPathwaysIntronsCheckBox, 4, 4, 1, 1);

        findPathwaysIntron1CheckBox = new QCheckBox(layoutWidget_31);
        findPathwaysIntron1CheckBox->setObjectName(QString::fromUtf8("findPathwaysIntron1CheckBox"));

        findPathwayGridLayout->addWidget(findPathwaysIntron1CheckBox, 5, 4, 1, 1);

        findPathwaysIntron2CheckBox = new QCheckBox(layoutWidget_31);
        findPathwaysIntron2CheckBox->setObjectName(QString::fromUtf8("findPathwaysIntron2CheckBox"));

        findPathwayGridLayout->addWidget(findPathwaysIntron2CheckBox, 6, 4, 1, 1);

        findPathwaySuffixLabel = new QLabel(layoutWidget_31);
        findPathwaySuffixLabel->setObjectName(QString::fromUtf8("findPathwaySuffixLabel"));
        QFont font3;
        font3.setPointSize(13);
        findPathwaySuffixLabel->setFont(font3);

        findPathwayGridLayout->addWidget(findPathwaySuffixLabel, 1, 5, 1, 2);

        findPathwaysDatabaseLabel = new QLabel(layoutWidget_31);
        findPathwaysDatabaseLabel->setObjectName(QString::fromUtf8("findPathwaysDatabaseLabel"));

        findPathwayGridLayout->addWidget(findPathwaysDatabaseLabel, 3, 0, 1, 2);

        findPathwaysDatabaseComboBox = new QComboBox(layoutWidget_31);
        findPathwaysDatabaseComboBox->setObjectName(QString::fromUtf8("findPathwaysDatabaseComboBox"));
        findPathwaysDatabaseComboBox->setEditable(false);
        findPathwaysDatabaseComboBox->setModelColumn(0);

        findPathwayGridLayout->addWidget(findPathwaysDatabaseComboBox, 3, 2, 1, 4);

        findPathwaysDatabaseInfoButton = new QPushButton(layoutWidget_31);
        findPathwaysDatabaseInfoButton->setObjectName(QString::fromUtf8("findPathwaysDatabaseInfoButton"));
        findPathwaysDatabaseInfoButton->setMaximumSize(QSize(21, 21));
        findPathwaysDatabaseInfoButton->setMouseTracking(false);
        findPathwaysDatabaseInfoButton->setAcceptDrops(false);
        findPathwaysDatabaseInfoButton->setLayoutDirection(Qt::LeftToRight);
        findPathwaysDatabaseInfoButton->setIcon(icon1);
        findPathwaysDatabaseInfoButton->setAutoRepeatDelay(100);
        findPathwaysDatabaseInfoButton->setDefault(false);
        findPathwaysDatabaseInfoButton->setFlat(true);

        findPathwayGridLayout->addWidget(findPathwaysDatabaseInfoButton, 3, 7, 1, 1);

        findPathwaysExonsCheckBox = new QCheckBox(layoutWidget_31);
        findPathwaysExonsCheckBox->setObjectName(QString::fromUtf8("findPathwaysExonsCheckBox"));

        findPathwayGridLayout->addWidget(findPathwaysExonsCheckBox, 7, 4, 1, 1);

        findPathwaysIntergenicCheckBox = new QCheckBox(layoutWidget_31);
        findPathwaysIntergenicCheckBox->setObjectName(QString::fromUtf8("findPathwaysIntergenicCheckBox"));

        findPathwayGridLayout->addWidget(findPathwaysIntergenicCheckBox, 7, 2, 1, 2);

        findPathwaysDownstreamCheckBox = new QCheckBox(layoutWidget_31);
        findPathwaysDownstreamCheckBox->setObjectName(QString::fromUtf8("findPathwaysDownstreamCheckBox"));

        findPathwayGridLayout->addWidget(findPathwaysDownstreamCheckBox, 5, 2, 1, 2);

        findPathwaysDistalCheckBox = new QCheckBox(layoutWidget_31);
        findPathwaysDistalCheckBox->setObjectName(QString::fromUtf8("findPathwaysDistalCheckBox"));

        findPathwayGridLayout->addWidget(findPathwaysDistalCheckBox, 6, 2, 1, 2);

        findPathwayLine2 = new QFrame(findPathwayParametersTab);
        findPathwayLine2->setObjectName(QString::fromUtf8("findPathwayLine2"));
        findPathwayLine2->setGeometry(QRect(0, 470, 629, 3));
        findPathwayLine2->setFrameShadow(QFrame::Sunken);
        findPathwayLine2->setFrameShape(QFrame::HLine);
        layoutWidget_32 = new QWidget(findPathwayParametersTab);
        layoutWidget_32->setObjectName(QString::fromUtf8("layoutWidget_32"));
        layoutWidget_32->setGeometry(QRect(410, 470, 231, 41));
        findPathwayButtonsHorizontalLayout = new QHBoxLayout(layoutWidget_32);
        findPathwayButtonsHorizontalLayout->setSpacing(6);
        findPathwayButtonsHorizontalLayout->setContentsMargins(11, 11, 11, 11);
        findPathwayButtonsHorizontalLayout->setObjectName(QString::fromUtf8("findPathwayButtonsHorizontalLayout"));
        findPathwayButtonsHorizontalLayout->setContentsMargins(0, 0, 0, 0);
        runFindPathwayClearButton = new QPushButton(layoutWidget_32);
        runFindPathwayClearButton->setObjectName(QString::fromUtf8("runFindPathwayClearButton"));

        findPathwayButtonsHorizontalLayout->addWidget(runFindPathwayClearButton);

        runFindPathwayButton = new QPushButton(layoutWidget_32);
        runFindPathwayButton->setObjectName(QString::fromUtf8("runFindPathwayButton"));

        findPathwayButtonsHorizontalLayout->addWidget(runFindPathwayButton);

        findPathwayTabWidget->addTab(findPathwayParametersTab, QString());
        findPathwayProgressTab = new QWidget();
        findPathwayProgressTab->setObjectName(QString::fromUtf8("findPathwayProgressTab"));
        findPathwayProgressTextEdit = new QTextEdit(findPathwayProgressTab);
        findPathwayProgressTextEdit->setObjectName(QString::fromUtf8("findPathwayProgressTextEdit"));
        findPathwayProgressTextEdit->setGeometry(QRect(0, 0, 641, 501));
        findPathwayProgressTextEdit->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);
        stopRunFindPathwayProcessButton = new QPushButton(findPathwayProgressTab);
        stopRunFindPathwayProcessButton->setObjectName(QString::fromUtf8("stopRunFindPathwayProcessButton"));
        stopRunFindPathwayProcessButton->setEnabled(false);
        stopRunFindPathwayProcessButton->setGeometry(QRect(520, 510, 113, 32));
        findPathwayTabWidget->addTab(findPathwayProgressTab, QString());

        verticalLayout_3->addWidget(findPathwayTabWidget);

        stackedWidget->addWidget(findPathwayStackedPage);
        pageStackedPage = new QWidget();
        pageStackedPage->setObjectName(QString::fromUtf8("pageStackedPage"));
        verticalLayout_15 = new QVBoxLayout(pageStackedPage);
        verticalLayout_15->setSpacing(6);
        verticalLayout_15->setContentsMargins(11, 11, 11, 11);
        verticalLayout_15->setObjectName(QString::fromUtf8("verticalLayout_15"));
        pageTabWidget = new QTabWidget(pageStackedPage);
        pageTabWidget->setObjectName(QString::fromUtf8("pageTabWidget"));
        pageParametersTab = new QWidget();
        pageParametersTab->setObjectName(QString::fromUtf8("pageParametersTab"));
        pageTitleLabel = new QLabel(pageParametersTab);
        pageTitleLabel->setObjectName(QString::fromUtf8("pageTitleLabel"));
        pageTitleLabel->setGeometry(QRect(0, 10, 588, 21));
        pageTitleLabel->setFont(font2);
        pageLine1 = new QFrame(pageParametersTab);
        pageLine1->setObjectName(QString::fromUtf8("pageLine1"));
        pageLine1->setGeometry(QRect(0, 30, 627, 3));
        pageLine1->setFrameShape(QFrame::HLine);
        pageLine1->setFrameShadow(QFrame::Sunken);
        layoutWidget_33 = new QWidget(pageParametersTab);
        layoutWidget_33->setObjectName(QString::fromUtf8("layoutWidget_33"));
        layoutWidget_33->setGeometry(QRect(0, 40, 641, 341));
        pageGridLayout = new QGridLayout(layoutWidget_33);
        pageGridLayout->setSpacing(6);
        pageGridLayout->setContentsMargins(11, 11, 11, 11);
        pageGridLayout->setObjectName(QString::fromUtf8("pageGridLayout"));
        pageGridLayout->setHorizontalSpacing(-1);
        pageGridLayout->setVerticalSpacing(15);
        pageGridLayout->setContentsMargins(0, 0, 0, 0);
        pageSelectPeaksLabel = new QLabel(layoutWidget_33);
        pageSelectPeaksLabel->setObjectName(QString::fromUtf8("pageSelectPeaksLabel"));

        pageGridLayout->addWidget(pageSelectPeaksLabel, 0, 0, 1, 1);

        pageSelectPeaksLineEdit = new QLineEdit(layoutWidget_33);
        pageSelectPeaksLineEdit->setObjectName(QString::fromUtf8("pageSelectPeaksLineEdit"));
        pageSelectPeaksLineEdit->setReadOnly(true);

        pageGridLayout->addWidget(pageSelectPeaksLineEdit, 0, 1, 1, 5);

        pageSelectPeaksInfoButton = new QPushButton(layoutWidget_33);
        pageSelectPeaksInfoButton->setObjectName(QString::fromUtf8("pageSelectPeaksInfoButton"));
        pageSelectPeaksInfoButton->setMaximumSize(QSize(21, 21));
        pageSelectPeaksInfoButton->setMouseTracking(false);
        pageSelectPeaksInfoButton->setAcceptDrops(false);
        pageSelectPeaksInfoButton->setLayoutDirection(Qt::LeftToRight);
        pageSelectPeaksInfoButton->setIcon(icon1);
        pageSelectPeaksInfoButton->setAutoRepeatDelay(100);
        pageSelectPeaksInfoButton->setDefault(false);
        pageSelectPeaksInfoButton->setFlat(true);

        pageGridLayout->addWidget(pageSelectPeaksInfoButton, 0, 7, 1, 1);

        pageOutputFileLabel = new QLabel(layoutWidget_33);
        pageOutputFileLabel->setObjectName(QString::fromUtf8("pageOutputFileLabel"));

        pageGridLayout->addWidget(pageOutputFileLabel, 1, 0, 1, 1);

        pageOutputFileLineEdit = new QLineEdit(layoutWidget_33);
        pageOutputFileLineEdit->setObjectName(QString::fromUtf8("pageOutputFileLineEdit"));

        pageGridLayout->addWidget(pageOutputFileLineEdit, 1, 1, 1, 4);

        pageOutputFileInfoButton = new QPushButton(layoutWidget_33);
        pageOutputFileInfoButton->setObjectName(QString::fromUtf8("pageOutputFileInfoButton"));
        pageOutputFileInfoButton->setMaximumSize(QSize(21, 21));
        pageOutputFileInfoButton->setMouseTracking(false);
        pageOutputFileInfoButton->setAcceptDrops(false);
        pageOutputFileInfoButton->setLayoutDirection(Qt::LeftToRight);
        pageOutputFileInfoButton->setIcon(icon1);
        pageOutputFileInfoButton->setAutoRepeatDelay(100);
        pageOutputFileInfoButton->setDefault(false);
        pageOutputFileInfoButton->setFlat(true);

        pageGridLayout->addWidget(pageOutputFileInfoButton, 1, 7, 1, 1);

        pageSpeciesLabel = new QLabel(layoutWidget_33);
        pageSpeciesLabel->setObjectName(QString::fromUtf8("pageSpeciesLabel"));

        pageGridLayout->addWidget(pageSpeciesLabel, 2, 0, 1, 1);

        pageSpeciesComboBox = new QComboBox(layoutWidget_33);
        pageSpeciesComboBox->setObjectName(QString::fromUtf8("pageSpeciesComboBox"));
        pageSpeciesComboBox->setEnabled(true);
        pageSpeciesComboBox->setEditable(false);
        pageSpeciesComboBox->setModelColumn(0);

        pageGridLayout->addWidget(pageSpeciesComboBox, 2, 1, 1, 5);

        pageDatabaseLabel = new QLabel(layoutWidget_33);
        pageDatabaseLabel->setObjectName(QString::fromUtf8("pageDatabaseLabel"));

        pageGridLayout->addWidget(pageDatabaseLabel, 3, 0, 1, 1);

        pageGenesDBComboBox = new QComboBox(layoutWidget_33);
        pageGenesDBComboBox->setObjectName(QString::fromUtf8("pageGenesDBComboBox"));
        pageGenesDBComboBox->setEditable(false);
        pageGenesDBComboBox->setModelColumn(0);

        pageGridLayout->addWidget(pageGenesDBComboBox, 3, 1, 1, 5);

        pageSelectDatabaseLabel = new QLabel(layoutWidget_33);
        pageSelectDatabaseLabel->setObjectName(QString::fromUtf8("pageSelectDatabaseLabel"));

        pageGridLayout->addWidget(pageSelectDatabaseLabel, 8, 0, 1, 1);

        pagePathwaysDBComboBox = new QComboBox(layoutWidget_33);
        pagePathwaysDBComboBox->setObjectName(QString::fromUtf8("pagePathwaysDBComboBox"));
        pagePathwaysDBComboBox->setEnabled(true);
        pagePathwaysDBComboBox->setEditable(false);
        pagePathwaysDBComboBox->setModelColumn(0);

        pageGridLayout->addWidget(pagePathwaysDBComboBox, 8, 1, 1, 5);

        pageSelectGenepartsLabel = new QLabel(layoutWidget_33);
        pageSelectGenepartsLabel->setObjectName(QString::fromUtf8("pageSelectGenepartsLabel"));

        pageGridLayout->addWidget(pageSelectGenepartsLabel, 4, 0, 1, 1);

        pageSelectGenepartsInfoButton = new QPushButton(layoutWidget_33);
        pageSelectGenepartsInfoButton->setObjectName(QString::fromUtf8("pageSelectGenepartsInfoButton"));
        pageSelectGenepartsInfoButton->setMaximumSize(QSize(21, 21));
        pageSelectGenepartsInfoButton->setMouseTracking(false);
        pageSelectGenepartsInfoButton->setAcceptDrops(false);
        pageSelectGenepartsInfoButton->setLayoutDirection(Qt::LeftToRight);
        pageSelectGenepartsInfoButton->setIcon(icon1);
        pageSelectGenepartsInfoButton->setAutoRepeatDelay(100);
        pageSelectGenepartsInfoButton->setDefault(false);
        pageSelectGenepartsInfoButton->setFlat(true);

        pageGridLayout->addWidget(pageSelectGenepartsInfoButton, 4, 7, 1, 1);

        pageSelectDatabaseInfoButton = new QPushButton(layoutWidget_33);
        pageSelectDatabaseInfoButton->setObjectName(QString::fromUtf8("pageSelectDatabaseInfoButton"));
        pageSelectDatabaseInfoButton->setMaximumSize(QSize(21, 21));
        pageSelectDatabaseInfoButton->setMouseTracking(false);
        pageSelectDatabaseInfoButton->setAcceptDrops(false);
        pageSelectDatabaseInfoButton->setLayoutDirection(Qt::LeftToRight);
        pageSelectDatabaseInfoButton->setIcon(icon1);
        pageSelectDatabaseInfoButton->setAutoRepeatDelay(100);
        pageSelectDatabaseInfoButton->setDefault(false);
        pageSelectDatabaseInfoButton->setFlat(true);

        pageGridLayout->addWidget(pageSelectDatabaseInfoButton, 8, 7, 1, 1);

        pageSpeciesInfoButton = new QPushButton(layoutWidget_33);
        pageSpeciesInfoButton->setObjectName(QString::fromUtf8("pageSpeciesInfoButton"));
        pageSpeciesInfoButton->setMaximumSize(QSize(21, 21));
        pageSpeciesInfoButton->setMouseTracking(false);
        pageSpeciesInfoButton->setAcceptDrops(false);
        pageSpeciesInfoButton->setLayoutDirection(Qt::LeftToRight);
        pageSpeciesInfoButton->setIcon(icon1);
        pageSpeciesInfoButton->setAutoRepeatDelay(100);
        pageSpeciesInfoButton->setDefault(false);
        pageSpeciesInfoButton->setFlat(true);

        pageGridLayout->addWidget(pageSpeciesInfoButton, 2, 7, 1, 1);

        pageDatabaseInfoButton = new QPushButton(layoutWidget_33);
        pageDatabaseInfoButton->setObjectName(QString::fromUtf8("pageDatabaseInfoButton"));
        pageDatabaseInfoButton->setMaximumSize(QSize(21, 21));
        pageDatabaseInfoButton->setMouseTracking(false);
        pageDatabaseInfoButton->setAcceptDrops(false);
        pageDatabaseInfoButton->setLayoutDirection(Qt::LeftToRight);
        pageDatabaseInfoButton->setIcon(icon1);
        pageDatabaseInfoButton->setAutoRepeatDelay(100);
        pageDatabaseInfoButton->setDefault(false);
        pageDatabaseInfoButton->setFlat(true);

        pageGridLayout->addWidget(pageDatabaseInfoButton, 3, 7, 1, 1);

        pagePromotersCheckBox = new QCheckBox(layoutWidget_33);
        pagePromotersCheckBox->setObjectName(QString::fromUtf8("pagePromotersCheckBox"));
        pagePromotersCheckBox->setChecked(true);

        pageGridLayout->addWidget(pagePromotersCheckBox, 4, 1, 1, 3);

        pageSuffixLabel = new QLabel(layoutWidget_33);
        pageSuffixLabel->setObjectName(QString::fromUtf8("pageSuffixLabel"));
        pageSuffixLabel->setFont(font3);

        pageGridLayout->addWidget(pageSuffixLabel, 1, 5, 1, 1);

        pageSelectPeaksToolButton = new QToolButton(layoutWidget_33);
        pageSelectPeaksToolButton->setObjectName(QString::fromUtf8("pageSelectPeaksToolButton"));

        pageGridLayout->addWidget(pageSelectPeaksToolButton, 0, 6, 1, 1);

        pageIntronsCheckBox = new QCheckBox(layoutWidget_33);
        pageIntronsCheckBox->setObjectName(QString::fromUtf8("pageIntronsCheckBox"));

        pageGridLayout->addWidget(pageIntronsCheckBox, 4, 4, 1, 1);

        pageIntron1CheckBox = new QCheckBox(layoutWidget_33);
        pageIntron1CheckBox->setObjectName(QString::fromUtf8("pageIntron1CheckBox"));

        pageGridLayout->addWidget(pageIntron1CheckBox, 5, 4, 1, 1);

        pageIntron2CheckBox = new QCheckBox(layoutWidget_33);
        pageIntron2CheckBox->setObjectName(QString::fromUtf8("pageIntron2CheckBox"));

        pageGridLayout->addWidget(pageIntron2CheckBox, 6, 4, 1, 1);

        pageExonsCheckBox = new QCheckBox(layoutWidget_33);
        pageExonsCheckBox->setObjectName(QString::fromUtf8("pageExonsCheckBox"));

        pageGridLayout->addWidget(pageExonsCheckBox, 7, 4, 1, 1);

        pageIntergenicCheckBox = new QCheckBox(layoutWidget_33);
        pageIntergenicCheckBox->setObjectName(QString::fromUtf8("pageIntergenicCheckBox"));

        pageGridLayout->addWidget(pageIntergenicCheckBox, 7, 1, 1, 3);

        pageDownstreamCheckBox = new QCheckBox(layoutWidget_33);
        pageDownstreamCheckBox->setObjectName(QString::fromUtf8("pageDownstreamCheckBox"));

        pageGridLayout->addWidget(pageDownstreamCheckBox, 5, 1, 1, 3);

        pageDistalCheckBox = new QCheckBox(layoutWidget_33);
        pageDistalCheckBox->setObjectName(QString::fromUtf8("pageDistalCheckBox"));

        pageGridLayout->addWidget(pageDistalCheckBox, 6, 1, 1, 3);

        pageLine2 = new QFrame(pageParametersTab);
        pageLine2->setObjectName(QString::fromUtf8("pageLine2"));
        pageLine2->setGeometry(QRect(0, 380, 629, 3));
        pageLine2->setFrameShadow(QFrame::Sunken);
        pageLine2->setFrameShape(QFrame::HLine);
        layoutWidget_34 = new QWidget(pageParametersTab);
        layoutWidget_34->setObjectName(QString::fromUtf8("layoutWidget_34"));
        layoutWidget_34->setGeometry(QRect(410, 380, 231, 41));
        pageButtonsHorizontalLayout = new QHBoxLayout(layoutWidget_34);
        pageButtonsHorizontalLayout->setSpacing(6);
        pageButtonsHorizontalLayout->setContentsMargins(11, 11, 11, 11);
        pageButtonsHorizontalLayout->setObjectName(QString::fromUtf8("pageButtonsHorizontalLayout"));
        pageButtonsHorizontalLayout->setContentsMargins(0, 0, 0, 0);
        runPAGEClearButton = new QPushButton(layoutWidget_34);
        runPAGEClearButton->setObjectName(QString::fromUtf8("runPAGEClearButton"));

        pageButtonsHorizontalLayout->addWidget(runPAGEClearButton);

        runPAGEButton = new QPushButton(layoutWidget_34);
        runPAGEButton->setObjectName(QString::fromUtf8("runPAGEButton"));

        pageButtonsHorizontalLayout->addWidget(runPAGEButton);

        pageTabWidget->addTab(pageParametersTab, QString());
        pageProgressTab = new QWidget();
        pageProgressTab->setObjectName(QString::fromUtf8("pageProgressTab"));
        pageProgressTextEdit = new QTextEdit(pageProgressTab);
        pageProgressTextEdit->setObjectName(QString::fromUtf8("pageProgressTextEdit"));
        pageProgressTextEdit->setGeometry(QRect(0, 0, 641, 501));
        pageProgressTextEdit->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);
        stopRunPAGEProcessButton = new QPushButton(pageProgressTab);
        stopRunPAGEProcessButton->setObjectName(QString::fromUtf8("stopRunPAGEProcessButton"));
        stopRunPAGEProcessButton->setEnabled(false);
        stopRunPAGEProcessButton->setGeometry(QRect(520, 510, 113, 32));
        pageTabWidget->addTab(pageProgressTab, QString());

        verticalLayout_15->addWidget(pageTabWidget);

        stackedWidget->addWidget(pageStackedPage);
        consStackedPage = new QWidget();
        consStackedPage->setObjectName(QString::fromUtf8("consStackedPage"));
        verticalLayout_20 = new QVBoxLayout(consStackedPage);
        verticalLayout_20->setSpacing(6);
        verticalLayout_20->setContentsMargins(11, 11, 11, 11);
        verticalLayout_20->setObjectName(QString::fromUtf8("verticalLayout_20"));
        consTabWidget = new QTabWidget(consStackedPage);
        consTabWidget->setObjectName(QString::fromUtf8("consTabWidget"));
        consParametersTab = new QWidget();
        consParametersTab->setObjectName(QString::fromUtf8("consParametersTab"));
        consTitleLabel = new QLabel(consParametersTab);
        consTitleLabel->setObjectName(QString::fromUtf8("consTitleLabel"));
        consTitleLabel->setGeometry(QRect(0, 10, 588, 21));
        consTitleLabel->setFont(font2);
        consLine1 = new QFrame(consParametersTab);
        consLine1->setObjectName(QString::fromUtf8("consLine1"));
        consLine1->setGeometry(QRect(0, 30, 627, 3));
        consLine1->setFrameShape(QFrame::HLine);
        consLine1->setFrameShadow(QFrame::Sunken);
        layoutWidget_39 = new QWidget(consParametersTab);
        layoutWidget_39->setObjectName(QString::fromUtf8("layoutWidget_39"));
        layoutWidget_39->setGeometry(QRect(0, 40, 641, 480));
        consGridLayout = new QGridLayout(layoutWidget_39);
        consGridLayout->setSpacing(6);
        consGridLayout->setContentsMargins(11, 11, 11, 11);
        consGridLayout->setObjectName(QString::fromUtf8("consGridLayout"));
        consGridLayout->setHorizontalSpacing(-1);
        consGridLayout->setVerticalSpacing(15);
        consGridLayout->setContentsMargins(0, 0, 0, 0);
        consSelectPeaksLabel = new QLabel(layoutWidget_39);
        consSelectPeaksLabel->setObjectName(QString::fromUtf8("consSelectPeaksLabel"));

        consGridLayout->addWidget(consSelectPeaksLabel, 0, 0, 1, 1);

        consSelectPeaksLineEdit = new QLineEdit(layoutWidget_39);
        consSelectPeaksLineEdit->setObjectName(QString::fromUtf8("consSelectPeaksLineEdit"));
        consSelectPeaksLineEdit->setReadOnly(true);

        consGridLayout->addWidget(consSelectPeaksLineEdit, 0, 1, 1, 2);

        consSelectPeaksToolButton = new QToolButton(layoutWidget_39);
        consSelectPeaksToolButton->setObjectName(QString::fromUtf8("consSelectPeaksToolButton"));

        consGridLayout->addWidget(consSelectPeaksToolButton, 0, 3, 1, 1);

        consSelectPeaksInfoButton = new QPushButton(layoutWidget_39);
        consSelectPeaksInfoButton->setObjectName(QString::fromUtf8("consSelectPeaksInfoButton"));
        consSelectPeaksInfoButton->setMaximumSize(QSize(21, 21));
        consSelectPeaksInfoButton->setMouseTracking(false);
        consSelectPeaksInfoButton->setAcceptDrops(false);
        consSelectPeaksInfoButton->setLayoutDirection(Qt::LeftToRight);
        consSelectPeaksInfoButton->setIcon(icon1);
        consSelectPeaksInfoButton->setAutoRepeatDelay(100);
        consSelectPeaksInfoButton->setDefault(false);
        consSelectPeaksInfoButton->setFlat(true);

        consGridLayout->addWidget(consSelectPeaksInfoButton, 0, 4, 1, 1);

        consOutputFileLabel = new QLabel(layoutWidget_39);
        consOutputFileLabel->setObjectName(QString::fromUtf8("consOutputFileLabel"));

        consGridLayout->addWidget(consOutputFileLabel, 1, 0, 1, 1);

        consOutputFileLineEdit = new QLineEdit(layoutWidget_39);
        consOutputFileLineEdit->setObjectName(QString::fromUtf8("consOutputFileLineEdit"));

        consGridLayout->addWidget(consOutputFileLineEdit, 1, 1, 1, 2);

        consOutputFileInfoButton = new QPushButton(layoutWidget_39);
        consOutputFileInfoButton->setObjectName(QString::fromUtf8("consOutputFileInfoButton"));
        consOutputFileInfoButton->setMaximumSize(QSize(21, 21));
        consOutputFileInfoButton->setMouseTracking(false);
        consOutputFileInfoButton->setAcceptDrops(false);
        consOutputFileInfoButton->setLayoutDirection(Qt::LeftToRight);
        consOutputFileInfoButton->setIcon(icon1);
        consOutputFileInfoButton->setAutoRepeatDelay(100);
        consOutputFileInfoButton->setDefault(false);
        consOutputFileInfoButton->setFlat(true);

        consGridLayout->addWidget(consOutputFileInfoButton, 1, 4, 1, 1);

        consMethodLabel = new QLabel(layoutWidget_39);
        consMethodLabel->setObjectName(QString::fromUtf8("consMethodLabel"));

        consGridLayout->addWidget(consMethodLabel, 3, 0, 1, 1);

        consMethodComboBox = new QComboBox(layoutWidget_39);
        consMethodComboBox->setObjectName(QString::fromUtf8("consMethodComboBox"));
        consMethodComboBox->setEditable(false);
        consMethodComboBox->setModelColumn(0);

        consGridLayout->addWidget(consMethodComboBox, 3, 1, 1, 2);

        consMethodInfoButton = new QPushButton(layoutWidget_39);
        consMethodInfoButton->setObjectName(QString::fromUtf8("consMethodInfoButton"));
        consMethodInfoButton->setMaximumSize(QSize(21, 21));
        consMethodInfoButton->setMouseTracking(false);
        consMethodInfoButton->setAcceptDrops(false);
        consMethodInfoButton->setLayoutDirection(Qt::LeftToRight);
        consMethodInfoButton->setIcon(icon1);
        consMethodInfoButton->setAutoRepeatDelay(100);
        consMethodInfoButton->setDefault(false);
        consMethodInfoButton->setFlat(true);

        consGridLayout->addWidget(consMethodInfoButton, 3, 4, 1, 1);

        consCategoryInfoButton = new QPushButton(layoutWidget_39);
        consCategoryInfoButton->setObjectName(QString::fromUtf8("consCategoryInfoButton"));
        consCategoryInfoButton->setMaximumSize(QSize(21, 21));
        consCategoryInfoButton->setMouseTracking(false);
        consCategoryInfoButton->setAcceptDrops(false);
        consCategoryInfoButton->setLayoutDirection(Qt::LeftToRight);
        consCategoryInfoButton->setIcon(icon1);
        consCategoryInfoButton->setAutoRepeatDelay(100);
        consCategoryInfoButton->setDefault(false);
        consCategoryInfoButton->setFlat(true);

        consGridLayout->addWidget(consCategoryInfoButton, 4, 4, 1, 1);

        consConsDirLabel = new QLabel(layoutWidget_39);
        consConsDirLabel->setObjectName(QString::fromUtf8("consConsDirLabel"));

        consGridLayout->addWidget(consConsDirLabel, 5, 0, 1, 1);

        consConsDirLineEdit = new QLineEdit(layoutWidget_39);
        consConsDirLineEdit->setObjectName(QString::fromUtf8("consConsDirLineEdit"));
        consConsDirLineEdit->setReadOnly(true);

        consGridLayout->addWidget(consConsDirLineEdit, 5, 1, 1, 2);

        consConsDirToolButton = new QToolButton(layoutWidget_39);
        consConsDirToolButton->setObjectName(QString::fromUtf8("consConsDirToolButton"));

        consGridLayout->addWidget(consConsDirToolButton, 5, 3, 1, 1);

        consConsDirInfoButton = new QPushButton(layoutWidget_39);
        consConsDirInfoButton->setObjectName(QString::fromUtf8("consConsDirInfoButton"));
        consConsDirInfoButton->setMaximumSize(QSize(21, 21));
        consConsDirInfoButton->setMouseTracking(false);
        consConsDirInfoButton->setAcceptDrops(false);
        consConsDirInfoButton->setLayoutDirection(Qt::LeftToRight);
        consConsDirInfoButton->setIcon(icon1);
        consConsDirInfoButton->setAutoRepeatDelay(100);
        consConsDirInfoButton->setDefault(false);
        consConsDirInfoButton->setFlat(true);

        consGridLayout->addWidget(consConsDirInfoButton, 5, 4, 1, 1);

        consFormatLabel = new QLabel(layoutWidget_39);
        consFormatLabel->setObjectName(QString::fromUtf8("consFormatLabel"));

        consGridLayout->addWidget(consFormatLabel, 6, 0, 1, 1);

        consFormatComboBox = new QComboBox(layoutWidget_39);
        consFormatComboBox->setObjectName(QString::fromUtf8("consFormatComboBox"));
        consFormatComboBox->setEditable(false);
        consFormatComboBox->setModelColumn(0);

        consGridLayout->addWidget(consFormatComboBox, 6, 1, 1, 2);

        consFormatInfoButton = new QPushButton(layoutWidget_39);
        consFormatInfoButton->setObjectName(QString::fromUtf8("consFormatInfoButton"));
        consFormatInfoButton->setMaximumSize(QSize(21, 21));
        consFormatInfoButton->setMouseTracking(false);
        consFormatInfoButton->setAcceptDrops(false);
        consFormatInfoButton->setLayoutDirection(Qt::LeftToRight);
        consFormatInfoButton->setIcon(icon1);
        consFormatInfoButton->setAutoRepeatDelay(100);
        consFormatInfoButton->setDefault(false);
        consFormatInfoButton->setFlat(true);

        consGridLayout->addWidget(consFormatInfoButton, 6, 4, 1, 1);

        consOutputRandomLabel = new QLabel(layoutWidget_39);
        consOutputRandomLabel->setObjectName(QString::fromUtf8("consOutputRandomLabel"));

        consGridLayout->addWidget(consOutputRandomLabel, 9, 0, 1, 1);

        consOutputRandomLineEdit = new QLineEdit(layoutWidget_39);
        consOutputRandomLineEdit->setObjectName(QString::fromUtf8("consOutputRandomLineEdit"));

        consGridLayout->addWidget(consOutputRandomLineEdit, 9, 1, 1, 2);

        consOutputRandomInfoButton = new QPushButton(layoutWidget_39);
        consOutputRandomInfoButton->setObjectName(QString::fromUtf8("consOutputRandomInfoButton"));
        consOutputRandomInfoButton->setMaximumSize(QSize(21, 21));
        consOutputRandomInfoButton->setMouseTracking(false);
        consOutputRandomInfoButton->setAcceptDrops(false);
        consOutputRandomInfoButton->setLayoutDirection(Qt::LeftToRight);
        consOutputRandomInfoButton->setIcon(icon1);
        consOutputRandomInfoButton->setAutoRepeatDelay(100);
        consOutputRandomInfoButton->setDefault(false);
        consOutputRandomInfoButton->setFlat(true);

        consGridLayout->addWidget(consOutputRandomInfoButton, 9, 4, 1, 1);

        consShowProfilesCheckBox = new QCheckBox(layoutWidget_39);
        consShowProfilesCheckBox->setObjectName(QString::fromUtf8("consShowProfilesCheckBox"));
        consShowProfilesCheckBox->setEnabled(true);
        consShowProfilesCheckBox->setChecked(false);

        consGridLayout->addWidget(consShowProfilesCheckBox, 11, 0, 1, 1);

        consShowProfilesInfoButton = new QPushButton(layoutWidget_39);
        consShowProfilesInfoButton->setObjectName(QString::fromUtf8("consShowProfilesInfoButton"));
        consShowProfilesInfoButton->setMaximumSize(QSize(21, 21));
        consShowProfilesInfoButton->setMouseTracking(false);
        consShowProfilesInfoButton->setAcceptDrops(false);
        consShowProfilesInfoButton->setLayoutDirection(Qt::LeftToRight);
        consShowProfilesInfoButton->setIcon(icon1);
        consShowProfilesInfoButton->setAutoRepeatDelay(100);
        consShowProfilesInfoButton->setDefault(false);
        consShowProfilesInfoButton->setFlat(true);

        consGridLayout->addWidget(consShowProfilesInfoButton, 11, 4, 1, 1);

        consRandistLabel = new QLabel(layoutWidget_39);
        consRandistLabel->setObjectName(QString::fromUtf8("consRandistLabel"));

        consGridLayout->addWidget(consRandistLabel, 10, 0, 1, 1);

        consRandistSpinBox = new QSpinBox(layoutWidget_39);
        consRandistSpinBox->setObjectName(QString::fromUtf8("consRandistSpinBox"));
        consRandistSpinBox->setMaximum(1000000);
        consRandistSpinBox->setSingleStep(500);
        consRandistSpinBox->setValue(50000);

        consGridLayout->addWidget(consRandistSpinBox, 10, 1, 1, 1);

        consRandistInfoButton = new QPushButton(layoutWidget_39);
        consRandistInfoButton->setObjectName(QString::fromUtf8("consRandistInfoButton"));
        consRandistInfoButton->setMaximumSize(QSize(21, 21));
        consRandistInfoButton->setMouseTracking(false);
        consRandistInfoButton->setAcceptDrops(false);
        consRandistInfoButton->setLayoutDirection(Qt::LeftToRight);
        consRandistInfoButton->setIcon(icon1);
        consRandistInfoButton->setAutoRepeatDelay(100);
        consRandistInfoButton->setDefault(false);
        consRandistInfoButton->setFlat(true);

        consGridLayout->addWidget(consRandistInfoButton, 10, 4, 1, 1);

        consWindowSizeInfoButton = new QPushButton(layoutWidget_39);
        consWindowSizeInfoButton->setObjectName(QString::fromUtf8("consWindowSizeInfoButton"));
        consWindowSizeInfoButton->setMaximumSize(QSize(21, 21));
        consWindowSizeInfoButton->setMouseTracking(false);
        consWindowSizeInfoButton->setAcceptDrops(false);
        consWindowSizeInfoButton->setLayoutDirection(Qt::LeftToRight);
        consWindowSizeInfoButton->setIcon(icon1);
        consWindowSizeInfoButton->setAutoRepeatDelay(100);
        consWindowSizeInfoButton->setDefault(false);
        consWindowSizeInfoButton->setFlat(true);

        consGridLayout->addWidget(consWindowSizeInfoButton, 12, 4, 1, 1);

        consWindowSizeLabel = new QLabel(layoutWidget_39);
        consWindowSizeLabel->setObjectName(QString::fromUtf8("consWindowSizeLabel"));
        consWindowSizeLabel->setEnabled(false);

        consGridLayout->addWidget(consWindowSizeLabel, 11, 1, 1, 1);

        consWindowSizeSpinBox = new QSpinBox(layoutWidget_39);
        consWindowSizeSpinBox->setObjectName(QString::fromUtf8("consWindowSizeSpinBox"));
        consWindowSizeSpinBox->setEnabled(false);
        consWindowSizeSpinBox->setMaximum(500000);
        consWindowSizeSpinBox->setSingleStep(10);
        consWindowSizeSpinBox->setValue(10);

        consGridLayout->addWidget(consWindowSizeSpinBox, 11, 2, 1, 1);

        consAroundSummitCheckBox = new QCheckBox(layoutWidget_39);
        consAroundSummitCheckBox->setObjectName(QString::fromUtf8("consAroundSummitCheckBox"));
        consAroundSummitCheckBox->setEnabled(false);
        consAroundSummitCheckBox->setChecked(false);

        consGridLayout->addWidget(consAroundSummitCheckBox, 12, 0, 1, 1);

        consDistanceSpinBox = new QSpinBox(layoutWidget_39);
        consDistanceSpinBox->setObjectName(QString::fromUtf8("consDistanceSpinBox"));
        consDistanceSpinBox->setEnabled(false);
        consDistanceSpinBox->setMaximum(500000);
        consDistanceSpinBox->setSingleStep(500);
        consDistanceSpinBox->setValue(2000);

        consGridLayout->addWidget(consDistanceSpinBox, 12, 2, 1, 1);

        consDistanceLabel = new QLabel(layoutWidget_39);
        consDistanceLabel->setObjectName(QString::fromUtf8("consDistanceLabel"));
        consDistanceLabel->setEnabled(false);

        consGridLayout->addWidget(consDistanceLabel, 12, 1, 1, 1);

        consThresholdInfoButton = new QPushButton(layoutWidget_39);
        consThresholdInfoButton->setObjectName(QString::fromUtf8("consThresholdInfoButton"));
        consThresholdInfoButton->setMaximumSize(QSize(21, 21));
        consThresholdInfoButton->setMouseTracking(false);
        consThresholdInfoButton->setAcceptDrops(false);
        consThresholdInfoButton->setLayoutDirection(Qt::LeftToRight);
        consThresholdInfoButton->setIcon(icon1);
        consThresholdInfoButton->setAutoRepeatDelay(100);
        consThresholdInfoButton->setDefault(false);
        consThresholdInfoButton->setFlat(true);

        consGridLayout->addWidget(consThresholdInfoButton, 7, 4, 1, 1);

        consThresholdSpinBox = new QDoubleSpinBox(layoutWidget_39);
        consThresholdSpinBox->setObjectName(QString::fromUtf8("consThresholdSpinBox"));
        consThresholdSpinBox->setDecimals(1);
        consThresholdSpinBox->setMinimum(0);
        consThresholdSpinBox->setMaximum(1);
        consThresholdSpinBox->setSingleStep(0.1);
        consThresholdSpinBox->setValue(0.5);

        consGridLayout->addWidget(consThresholdSpinBox, 7, 1, 1, 1);

        consThresholdLabel = new QLabel(layoutWidget_39);
        consThresholdLabel->setObjectName(QString::fromUtf8("consThresholdLabel"));

        consGridLayout->addWidget(consThresholdLabel, 7, 0, 1, 1);

        consSpeciesComboBox = new QComboBox(layoutWidget_39);
        consSpeciesComboBox->setObjectName(QString::fromUtf8("consSpeciesComboBox"));
        consSpeciesComboBox->setEnabled(true);
        consSpeciesComboBox->setEditable(false);
        consSpeciesComboBox->setModelColumn(0);

        consGridLayout->addWidget(consSpeciesComboBox, 2, 1, 1, 2);

        consSpeciesLabel = new QLabel(layoutWidget_39);
        consSpeciesLabel->setObjectName(QString::fromUtf8("consSpeciesLabel"));

        consGridLayout->addWidget(consSpeciesLabel, 2, 0, 1, 1);

        consSpeciesInfoButton = new QPushButton(layoutWidget_39);
        consSpeciesInfoButton->setObjectName(QString::fromUtf8("consSpeciesInfoButton"));
        consSpeciesInfoButton->setMaximumSize(QSize(21, 21));
        consSpeciesInfoButton->setMouseTracking(false);
        consSpeciesInfoButton->setAcceptDrops(false);
        consSpeciesInfoButton->setLayoutDirection(Qt::LeftToRight);
        consSpeciesInfoButton->setIcon(icon1);
        consSpeciesInfoButton->setAutoRepeatDelay(100);
        consSpeciesInfoButton->setDefault(false);
        consSpeciesInfoButton->setFlat(true);

        consGridLayout->addWidget(consSpeciesInfoButton, 2, 4, 1, 1);

        consMakeRandInfoButton = new QPushButton(layoutWidget_39);
        consMakeRandInfoButton->setObjectName(QString::fromUtf8("consMakeRandInfoButton"));
        consMakeRandInfoButton->setMaximumSize(QSize(21, 21));
        consMakeRandInfoButton->setMouseTracking(false);
        consMakeRandInfoButton->setAcceptDrops(false);
        consMakeRandInfoButton->setLayoutDirection(Qt::LeftToRight);
        consMakeRandInfoButton->setIcon(icon1);
        consMakeRandInfoButton->setAutoRepeatDelay(100);
        consMakeRandInfoButton->setDefault(false);
        consMakeRandInfoButton->setFlat(true);

        consGridLayout->addWidget(consMakeRandInfoButton, 8, 4, 1, 1);

        consMakeRandCheckBox = new QCheckBox(layoutWidget_39);
        consMakeRandCheckBox->setObjectName(QString::fromUtf8("consMakeRandCheckBox"));
        consMakeRandCheckBox->setChecked(true);

        consGridLayout->addWidget(consMakeRandCheckBox, 8, 0, 1, 1);

        consCategoryComboBox = new QComboBox(layoutWidget_39);
        consCategoryComboBox->setObjectName(QString::fromUtf8("consCategoryComboBox"));
        consCategoryComboBox->setEditable(false);
        consCategoryComboBox->setModelColumn(0);

        consGridLayout->addWidget(consCategoryComboBox, 4, 1, 1, 2);

        consCategoryLabel = new QLabel(layoutWidget_39);
        consCategoryLabel->setObjectName(QString::fromUtf8("consCategoryLabel"));

        consGridLayout->addWidget(consCategoryLabel, 4, 0, 1, 1);

        consLine2 = new QFrame(consParametersTab);
        consLine2->setObjectName(QString::fromUtf8("consLine2"));
        consLine2->setGeometry(QRect(0, 500, 629, 20));
        consLine2->setFrameShadow(QFrame::Sunken);
        consLine2->setFrameShape(QFrame::HLine);
        layoutWidget_40 = new QWidget(consParametersTab);
        layoutWidget_40->setObjectName(QString::fromUtf8("layoutWidget_40"));
        layoutWidget_40->setGeometry(QRect(410, 510, 231, 41));
        consButtonsHorizontalLayout = new QHBoxLayout(layoutWidget_40);
        consButtonsHorizontalLayout->setSpacing(6);
        consButtonsHorizontalLayout->setContentsMargins(11, 11, 11, 11);
        consButtonsHorizontalLayout->setObjectName(QString::fromUtf8("consButtonsHorizontalLayout"));
        consButtonsHorizontalLayout->setContentsMargins(0, 0, 0, 0);
        runConsClearButton = new QPushButton(layoutWidget_40);
        runConsClearButton->setObjectName(QString::fromUtf8("runConsClearButton"));

        consButtonsHorizontalLayout->addWidget(runConsClearButton);

        runConsButton = new QPushButton(layoutWidget_40);
        runConsButton->setObjectName(QString::fromUtf8("runConsButton"));

        consButtonsHorizontalLayout->addWidget(runConsButton);

        consTabWidget->addTab(consParametersTab, QString());
        consProgressTab = new QWidget();
        consProgressTab->setObjectName(QString::fromUtf8("consProgressTab"));
        consProgressTextEdit = new QTextEdit(consProgressTab);
        consProgressTextEdit->setObjectName(QString::fromUtf8("consProgressTextEdit"));
        consProgressTextEdit->setGeometry(QRect(0, 0, 641, 501));
        consProgressTextEdit->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);
        stopRunConsProcessButton = new QPushButton(consProgressTab);
        stopRunConsProcessButton->setObjectName(QString::fromUtf8("stopRunConsProcessButton"));
        stopRunConsProcessButton->setEnabled(false);
        stopRunConsProcessButton->setGeometry(QRect(520, 510, 113, 32));
        consTabWidget->addTab(consProgressTab, QString());

        verticalLayout_20->addWidget(consTabWidget);

        stackedWidget->addWidget(consStackedPage);
        compPeaksStackedPage = new QWidget();
        compPeaksStackedPage->setObjectName(QString::fromUtf8("compPeaksStackedPage"));
        verticalLayout_12 = new QVBoxLayout(compPeaksStackedPage);
        verticalLayout_12->setSpacing(6);
        verticalLayout_12->setContentsMargins(11, 11, 11, 11);
        verticalLayout_12->setObjectName(QString::fromUtf8("verticalLayout_12"));
        compPeaksTabWidget = new QTabWidget(compPeaksStackedPage);
        compPeaksTabWidget->setObjectName(QString::fromUtf8("compPeaksTabWidget"));
        compPeaksParametersTab = new QWidget();
        compPeaksParametersTab->setObjectName(QString::fromUtf8("compPeaksParametersTab"));
        compPeaksTitleLabel = new QLabel(compPeaksParametersTab);
        compPeaksTitleLabel->setObjectName(QString::fromUtf8("compPeaksTitleLabel"));
        compPeaksTitleLabel->setGeometry(QRect(0, 10, 588, 21));
        compPeaksTitleLabel->setFont(font2);
        compPeaksLine1 = new QFrame(compPeaksParametersTab);
        compPeaksLine1->setObjectName(QString::fromUtf8("compPeaksLine1"));
        compPeaksLine1->setGeometry(QRect(0, 30, 627, 3));
        compPeaksLine1->setFrameShape(QFrame::HLine);
        compPeaksLine1->setFrameShadow(QFrame::Sunken);
        layoutWidget_23 = new QWidget(compPeaksParametersTab);
        layoutWidget_23->setObjectName(QString::fromUtf8("layoutWidget_23"));
        layoutWidget_23->setGeometry(QRect(0, 40, 641, 471));
        compPeaksGridLayout = new QGridLayout(layoutWidget_23);
        compPeaksGridLayout->setSpacing(6);
        compPeaksGridLayout->setContentsMargins(11, 11, 11, 11);
        compPeaksGridLayout->setObjectName(QString::fromUtf8("compPeaksGridLayout"));
        compPeaksGridLayout->setHorizontalSpacing(-1);
        compPeaksGridLayout->setVerticalSpacing(15);
        compPeaksGridLayout->setContentsMargins(0, 0, 0, 0);
        compPeaksSelectPeaks1Label = new QLabel(layoutWidget_23);
        compPeaksSelectPeaks1Label->setObjectName(QString::fromUtf8("compPeaksSelectPeaks1Label"));

        compPeaksGridLayout->addWidget(compPeaksSelectPeaks1Label, 0, 0, 1, 1);

        compPeaksSelectPeaks1LineEdit = new QLineEdit(layoutWidget_23);
        compPeaksSelectPeaks1LineEdit->setObjectName(QString::fromUtf8("compPeaksSelectPeaks1LineEdit"));
        compPeaksSelectPeaks1LineEdit->setReadOnly(true);

        compPeaksGridLayout->addWidget(compPeaksSelectPeaks1LineEdit, 0, 1, 1, 3);

        compPeaksSelectPeaks1InfoButton = new QPushButton(layoutWidget_23);
        compPeaksSelectPeaks1InfoButton->setObjectName(QString::fromUtf8("compPeaksSelectPeaks1InfoButton"));
        compPeaksSelectPeaks1InfoButton->setMaximumSize(QSize(21, 21));
        compPeaksSelectPeaks1InfoButton->setMouseTracking(false);
        compPeaksSelectPeaks1InfoButton->setAcceptDrops(false);
        compPeaksSelectPeaks1InfoButton->setLayoutDirection(Qt::LeftToRight);
        compPeaksSelectPeaks1InfoButton->setIcon(icon1);
        compPeaksSelectPeaks1InfoButton->setAutoRepeatDelay(100);
        compPeaksSelectPeaks1InfoButton->setDefault(false);
        compPeaksSelectPeaks1InfoButton->setFlat(true);

        compPeaksGridLayout->addWidget(compPeaksSelectPeaks1InfoButton, 0, 5, 1, 1);

        compPeaksSelectPeaks2Label = new QLabel(layoutWidget_23);
        compPeaksSelectPeaks2Label->setObjectName(QString::fromUtf8("compPeaksSelectPeaks2Label"));

        compPeaksGridLayout->addWidget(compPeaksSelectPeaks2Label, 2, 0, 1, 1);

        compPeaksSelectPeaks2LineEdit = new QLineEdit(layoutWidget_23);
        compPeaksSelectPeaks2LineEdit->setObjectName(QString::fromUtf8("compPeaksSelectPeaks2LineEdit"));
        compPeaksSelectPeaks2LineEdit->setReadOnly(true);

        compPeaksGridLayout->addWidget(compPeaksSelectPeaks2LineEdit, 2, 1, 1, 3);

        compPeaksPrefixFileLabel = new QLabel(layoutWidget_23);
        compPeaksPrefixFileLabel->setObjectName(QString::fromUtf8("compPeaksPrefixFileLabel"));

        compPeaksGridLayout->addWidget(compPeaksPrefixFileLabel, 4, 0, 1, 1);

        compPeaksPrefixFileLineEdit = new QLineEdit(layoutWidget_23);
        compPeaksPrefixFileLineEdit->setObjectName(QString::fromUtf8("compPeaksPrefixFileLineEdit"));

        compPeaksGridLayout->addWidget(compPeaksPrefixFileLineEdit, 4, 1, 1, 3);

        compPeaksCenter1CheckBox = new QCheckBox(layoutWidget_23);
        compPeaksCenter1CheckBox->setObjectName(QString::fromUtf8("compPeaksCenter1CheckBox"));

        compPeaksGridLayout->addWidget(compPeaksCenter1CheckBox, 1, 3, 1, 1);

        compPeaksHeader1CheckBox = new QCheckBox(layoutWidget_23);
        compPeaksHeader1CheckBox->setObjectName(QString::fromUtf8("compPeaksHeader1CheckBox"));

        compPeaksGridLayout->addWidget(compPeaksHeader1CheckBox, 1, 2, 1, 1);

        compPeaksID1CheckBox = new QCheckBox(layoutWidget_23);
        compPeaksID1CheckBox->setObjectName(QString::fromUtf8("compPeaksID1CheckBox"));

        compPeaksGridLayout->addWidget(compPeaksID1CheckBox, 1, 1, 1, 1);

        compPeaksSelectPeaks1ToolButton = new QToolButton(layoutWidget_23);
        compPeaksSelectPeaks1ToolButton->setObjectName(QString::fromUtf8("compPeaksSelectPeaks1ToolButton"));

        compPeaksGridLayout->addWidget(compPeaksSelectPeaks1ToolButton, 0, 4, 1, 1);

        compPeaksSelectPeaks2ToolButton = new QToolButton(layoutWidget_23);
        compPeaksSelectPeaks2ToolButton->setObjectName(QString::fromUtf8("compPeaksSelectPeaks2ToolButton"));

        compPeaksGridLayout->addWidget(compPeaksSelectPeaks2ToolButton, 2, 4, 1, 1);

        compPeaksCenter2CheckBox = new QCheckBox(layoutWidget_23);
        compPeaksCenter2CheckBox->setObjectName(QString::fromUtf8("compPeaksCenter2CheckBox"));

        compPeaksGridLayout->addWidget(compPeaksCenter2CheckBox, 3, 3, 1, 1);

        compPeaksHeader2CheckBox = new QCheckBox(layoutWidget_23);
        compPeaksHeader2CheckBox->setObjectName(QString::fromUtf8("compPeaksHeader2CheckBox"));

        compPeaksGridLayout->addWidget(compPeaksHeader2CheckBox, 3, 2, 1, 1);

        compPeaksID2CheckBox = new QCheckBox(layoutWidget_23);
        compPeaksID2CheckBox->setObjectName(QString::fromUtf8("compPeaksID2CheckBox"));

        compPeaksGridLayout->addWidget(compPeaksID2CheckBox, 3, 1, 1, 1);

        compPeaksColumnHeaderCenter1InfoButton = new QPushButton(layoutWidget_23);
        compPeaksColumnHeaderCenter1InfoButton->setObjectName(QString::fromUtf8("compPeaksColumnHeaderCenter1InfoButton"));
        compPeaksColumnHeaderCenter1InfoButton->setMaximumSize(QSize(21, 21));
        compPeaksColumnHeaderCenter1InfoButton->setMouseTracking(false);
        compPeaksColumnHeaderCenter1InfoButton->setAcceptDrops(false);
        compPeaksColumnHeaderCenter1InfoButton->setLayoutDirection(Qt::LeftToRight);
        compPeaksColumnHeaderCenter1InfoButton->setIcon(icon1);
        compPeaksColumnHeaderCenter1InfoButton->setAutoRepeatDelay(100);
        compPeaksColumnHeaderCenter1InfoButton->setDefault(false);
        compPeaksColumnHeaderCenter1InfoButton->setFlat(true);

        compPeaksGridLayout->addWidget(compPeaksColumnHeaderCenter1InfoButton, 1, 5, 1, 1);

        compPeaksSelectPeaks2InfoButton = new QPushButton(layoutWidget_23);
        compPeaksSelectPeaks2InfoButton->setObjectName(QString::fromUtf8("compPeaksSelectPeaks2InfoButton"));
        compPeaksSelectPeaks2InfoButton->setMaximumSize(QSize(21, 21));
        compPeaksSelectPeaks2InfoButton->setMouseTracking(false);
        compPeaksSelectPeaks2InfoButton->setAcceptDrops(false);
        compPeaksSelectPeaks2InfoButton->setLayoutDirection(Qt::LeftToRight);
        compPeaksSelectPeaks2InfoButton->setIcon(icon1);
        compPeaksSelectPeaks2InfoButton->setAutoRepeatDelay(100);
        compPeaksSelectPeaks2InfoButton->setDefault(false);
        compPeaksSelectPeaks2InfoButton->setFlat(true);

        compPeaksGridLayout->addWidget(compPeaksSelectPeaks2InfoButton, 2, 5, 1, 1);

        compPeaksColumnHeaderCenter2InfoButton = new QPushButton(layoutWidget_23);
        compPeaksColumnHeaderCenter2InfoButton->setObjectName(QString::fromUtf8("compPeaksColumnHeaderCenter2InfoButton"));
        compPeaksColumnHeaderCenter2InfoButton->setMaximumSize(QSize(21, 21));
        compPeaksColumnHeaderCenter2InfoButton->setMouseTracking(false);
        compPeaksColumnHeaderCenter2InfoButton->setAcceptDrops(false);
        compPeaksColumnHeaderCenter2InfoButton->setLayoutDirection(Qt::LeftToRight);
        compPeaksColumnHeaderCenter2InfoButton->setIcon(icon1);
        compPeaksColumnHeaderCenter2InfoButton->setAutoRepeatDelay(100);
        compPeaksColumnHeaderCenter2InfoButton->setDefault(false);
        compPeaksColumnHeaderCenter2InfoButton->setFlat(true);

        compPeaksGridLayout->addWidget(compPeaksColumnHeaderCenter2InfoButton, 3, 5, 1, 1);

        compPeaksPrefixInfoButton = new QPushButton(layoutWidget_23);
        compPeaksPrefixInfoButton->setObjectName(QString::fromUtf8("compPeaksPrefixInfoButton"));
        compPeaksPrefixInfoButton->setMaximumSize(QSize(21, 21));
        compPeaksPrefixInfoButton->setMouseTracking(false);
        compPeaksPrefixInfoButton->setAcceptDrops(false);
        compPeaksPrefixInfoButton->setLayoutDirection(Qt::LeftToRight);
        compPeaksPrefixInfoButton->setIcon(icon1);
        compPeaksPrefixInfoButton->setAutoRepeatDelay(100);
        compPeaksPrefixInfoButton->setDefault(false);
        compPeaksPrefixInfoButton->setFlat(true);

        compPeaksGridLayout->addWidget(compPeaksPrefixInfoButton, 4, 5, 1, 1);

        compPeaksOvPeaksCheckBox = new QCheckBox(layoutWidget_23);
        compPeaksOvPeaksCheckBox->setObjectName(QString::fromUtf8("compPeaksOvPeaksCheckBox"));

        compPeaksGridLayout->addWidget(compPeaksOvPeaksCheckBox, 7, 1, 1, 2);

        compPeaksUnPeaksCheckBox = new QCheckBox(layoutWidget_23);
        compPeaksUnPeaksCheckBox->setObjectName(QString::fromUtf8("compPeaksUnPeaksCheckBox"));

        compPeaksGridLayout->addWidget(compPeaksUnPeaksCheckBox, 8, 1, 1, 2);

        compPeaksDescPeaks2CheckBox = new QCheckBox(layoutWidget_23);
        compPeaksDescPeaks2CheckBox->setObjectName(QString::fromUtf8("compPeaksDescPeaks2CheckBox"));
        compPeaksDescPeaks2CheckBox->setEnabled(false);

        compPeaksGridLayout->addWidget(compPeaksDescPeaks2CheckBox, 10, 1, 1, 2);

        compPeaksANDRadioButton = new QRadioButton(layoutWidget_23);
        buttonGroup_3->addButton(compPeaksANDRadioButton);
        compPeaksANDRadioButton->setObjectName(QString::fromUtf8("compPeaksANDRadioButton"));
        compPeaksANDRadioButton->setLayoutDirection(Qt::RightToLeft);
        compPeaksANDRadioButton->setChecked(true);

        compPeaksGridLayout->addWidget(compPeaksANDRadioButton, 5, 0, 1, 1);

        compPeaksANDNOTRadioButton = new QRadioButton(layoutWidget_23);
        buttonGroup_3->addButton(compPeaksANDNOTRadioButton);
        compPeaksANDNOTRadioButton->setObjectName(QString::fromUtf8("compPeaksANDNOTRadioButton"));
        compPeaksANDNOTRadioButton->setLayoutDirection(Qt::RightToLeft);

        compPeaksGridLayout->addWidget(compPeaksANDNOTRadioButton, 9, 0, 1, 1);

        compPeaksANDLabel = new QLabel(layoutWidget_23);
        compPeaksANDLabel->setObjectName(QString::fromUtf8("compPeaksANDLabel"));

        compPeaksGridLayout->addWidget(compPeaksANDLabel, 5, 1, 1, 2);

        compPeaksANDNOTLabel = new QLabel(layoutWidget_23);
        compPeaksANDNOTLabel->setObjectName(QString::fromUtf8("compPeaksANDNOTLabel"));

        compPeaksGridLayout->addWidget(compPeaksANDNOTLabel, 9, 1, 1, 2);

        compPeaksDescPeaks1CheckBox = new QCheckBox(layoutWidget_23);
        compPeaksDescPeaks1CheckBox->setObjectName(QString::fromUtf8("compPeaksDescPeaks1CheckBox"));

        compPeaksGridLayout->addWidget(compPeaksDescPeaks1CheckBox, 6, 1, 1, 2);

        compPeaksIterationsLabel = new QLabel(layoutWidget_23);
        compPeaksIterationsLabel->setObjectName(QString::fromUtf8("compPeaksIterationsLabel"));

        compPeaksGridLayout->addWidget(compPeaksIterationsLabel, 11, 0, 1, 1);

        compPeaksIterationsSpinBox = new QSpinBox(layoutWidget_23);
        compPeaksIterationsSpinBox->setObjectName(QString::fromUtf8("compPeaksIterationsSpinBox"));
        compPeaksIterationsSpinBox->setMinimum(1);
        compPeaksIterationsSpinBox->setMaximum(10000);
        compPeaksIterationsSpinBox->setSingleStep(10);
        compPeaksIterationsSpinBox->setValue(100);

        compPeaksGridLayout->addWidget(compPeaksIterationsSpinBox, 11, 1, 1, 2);

        compPeaksDesc1InfoButton = new QPushButton(layoutWidget_23);
        compPeaksDesc1InfoButton->setObjectName(QString::fromUtf8("compPeaksDesc1InfoButton"));
        compPeaksDesc1InfoButton->setMaximumSize(QSize(21, 21));
        compPeaksDesc1InfoButton->setMouseTracking(false);
        compPeaksDesc1InfoButton->setAcceptDrops(false);
        compPeaksDesc1InfoButton->setLayoutDirection(Qt::LeftToRight);
        compPeaksDesc1InfoButton->setIcon(icon1);
        compPeaksDesc1InfoButton->setAutoRepeatDelay(100);
        compPeaksDesc1InfoButton->setDefault(false);
        compPeaksDesc1InfoButton->setFlat(true);

        compPeaksGridLayout->addWidget(compPeaksDesc1InfoButton, 6, 5, 1, 1);

        compPeaksDesc2InfoButton = new QPushButton(layoutWidget_23);
        compPeaksDesc2InfoButton->setObjectName(QString::fromUtf8("compPeaksDesc2InfoButton"));
        compPeaksDesc2InfoButton->setMaximumSize(QSize(21, 21));
        compPeaksDesc2InfoButton->setMouseTracking(false);
        compPeaksDesc2InfoButton->setAcceptDrops(false);
        compPeaksDesc2InfoButton->setLayoutDirection(Qt::LeftToRight);
        compPeaksDesc2InfoButton->setIcon(icon1);
        compPeaksDesc2InfoButton->setAutoRepeatDelay(100);
        compPeaksDesc2InfoButton->setDefault(false);
        compPeaksDesc2InfoButton->setFlat(true);

        compPeaksGridLayout->addWidget(compPeaksDesc2InfoButton, 10, 5, 1, 1);

        compPeaksUnInfoButton = new QPushButton(layoutWidget_23);
        compPeaksUnInfoButton->setObjectName(QString::fromUtf8("compPeaksUnInfoButton"));
        compPeaksUnInfoButton->setMaximumSize(QSize(21, 21));
        compPeaksUnInfoButton->setMouseTracking(false);
        compPeaksUnInfoButton->setAcceptDrops(false);
        compPeaksUnInfoButton->setLayoutDirection(Qt::LeftToRight);
        compPeaksUnInfoButton->setIcon(icon1);
        compPeaksUnInfoButton->setAutoRepeatDelay(100);
        compPeaksUnInfoButton->setDefault(false);
        compPeaksUnInfoButton->setFlat(true);

        compPeaksGridLayout->addWidget(compPeaksUnInfoButton, 8, 5, 1, 1);

        compPeaksOvInfoButton = new QPushButton(layoutWidget_23);
        compPeaksOvInfoButton->setObjectName(QString::fromUtf8("compPeaksOvInfoButton"));
        compPeaksOvInfoButton->setMaximumSize(QSize(21, 21));
        compPeaksOvInfoButton->setMouseTracking(false);
        compPeaksOvInfoButton->setAcceptDrops(false);
        compPeaksOvInfoButton->setLayoutDirection(Qt::LeftToRight);
        compPeaksOvInfoButton->setIcon(icon1);
        compPeaksOvInfoButton->setAutoRepeatDelay(100);
        compPeaksOvInfoButton->setDefault(false);
        compPeaksOvInfoButton->setFlat(true);

        compPeaksGridLayout->addWidget(compPeaksOvInfoButton, 7, 5, 1, 1);

        compPeaksANDNOTInfoButton = new QPushButton(layoutWidget_23);
        compPeaksANDNOTInfoButton->setObjectName(QString::fromUtf8("compPeaksANDNOTInfoButton"));
        compPeaksANDNOTInfoButton->setMaximumSize(QSize(21, 21));
        compPeaksANDNOTInfoButton->setMouseTracking(false);
        compPeaksANDNOTInfoButton->setAcceptDrops(false);
        compPeaksANDNOTInfoButton->setLayoutDirection(Qt::LeftToRight);
        compPeaksANDNOTInfoButton->setIcon(icon1);
        compPeaksANDNOTInfoButton->setAutoRepeatDelay(100);
        compPeaksANDNOTInfoButton->setDefault(false);
        compPeaksANDNOTInfoButton->setFlat(true);

        compPeaksGridLayout->addWidget(compPeaksANDNOTInfoButton, 9, 5, 1, 1);

        compPeaksANDInfoButton = new QPushButton(layoutWidget_23);
        compPeaksANDInfoButton->setObjectName(QString::fromUtf8("compPeaksANDInfoButton"));
        compPeaksANDInfoButton->setMaximumSize(QSize(21, 21));
        compPeaksANDInfoButton->setMouseTracking(false);
        compPeaksANDInfoButton->setAcceptDrops(false);
        compPeaksANDInfoButton->setLayoutDirection(Qt::LeftToRight);
        compPeaksANDInfoButton->setIcon(icon1);
        compPeaksANDInfoButton->setAutoRepeatDelay(100);
        compPeaksANDInfoButton->setDefault(false);
        compPeaksANDInfoButton->setFlat(true);

        compPeaksGridLayout->addWidget(compPeaksANDInfoButton, 5, 5, 1, 1);

        compPeaksIterationsInfoButton = new QPushButton(layoutWidget_23);
        compPeaksIterationsInfoButton->setObjectName(QString::fromUtf8("compPeaksIterationsInfoButton"));
        compPeaksIterationsInfoButton->setMaximumSize(QSize(21, 21));
        compPeaksIterationsInfoButton->setMouseTracking(false);
        compPeaksIterationsInfoButton->setAcceptDrops(false);
        compPeaksIterationsInfoButton->setLayoutDirection(Qt::LeftToRight);
        compPeaksIterationsInfoButton->setIcon(icon1);
        compPeaksIterationsInfoButton->setAutoRepeatDelay(100);
        compPeaksIterationsInfoButton->setDefault(false);
        compPeaksIterationsInfoButton->setFlat(true);

        compPeaksGridLayout->addWidget(compPeaksIterationsInfoButton, 11, 5, 1, 1);

        compPeaksRandomModeInfoButton = new QPushButton(layoutWidget_23);
        compPeaksRandomModeInfoButton->setObjectName(QString::fromUtf8("compPeaksRandomModeInfoButton"));
        compPeaksRandomModeInfoButton->setMaximumSize(QSize(21, 21));
        compPeaksRandomModeInfoButton->setMouseTracking(false);
        compPeaksRandomModeInfoButton->setAcceptDrops(false);
        compPeaksRandomModeInfoButton->setLayoutDirection(Qt::LeftToRight);
        compPeaksRandomModeInfoButton->setIcon(icon1);
        compPeaksRandomModeInfoButton->setAutoRepeatDelay(100);
        compPeaksRandomModeInfoButton->setDefault(false);
        compPeaksRandomModeInfoButton->setFlat(true);

        compPeaksGridLayout->addWidget(compPeaksRandomModeInfoButton, 12, 5, 1, 1);

        compPeaksRandomModeLabel = new QLabel(layoutWidget_23);
        compPeaksRandomModeLabel->setObjectName(QString::fromUtf8("compPeaksRandomModeLabel"));

        compPeaksGridLayout->addWidget(compPeaksRandomModeLabel, 12, 0, 1, 1);

        compPeaksRandomModeComboBox = new QComboBox(layoutWidget_23);
        compPeaksRandomModeComboBox->setObjectName(QString::fromUtf8("compPeaksRandomModeComboBox"));
        compPeaksRandomModeComboBox->setEnabled(true);
        compPeaksRandomModeComboBox->setEditable(false);
        compPeaksRandomModeComboBox->setModelColumn(0);

        compPeaksGridLayout->addWidget(compPeaksRandomModeComboBox, 12, 1, 1, 3);

        compPeaksLine2 = new QFrame(compPeaksParametersTab);
        compPeaksLine2->setObjectName(QString::fromUtf8("compPeaksLine2"));
        compPeaksLine2->setGeometry(QRect(0, 510, 629, 3));
        compPeaksLine2->setFrameShadow(QFrame::Sunken);
        compPeaksLine2->setFrameShape(QFrame::HLine);
        layoutWidget_24 = new QWidget(compPeaksParametersTab);
        layoutWidget_24->setObjectName(QString::fromUtf8("layoutWidget_24"));
        layoutWidget_24->setGeometry(QRect(260, 510, 381, 41));
        compPeaksButtonsHorizontalLayout = new QHBoxLayout(layoutWidget_24);
        compPeaksButtonsHorizontalLayout->setSpacing(6);
        compPeaksButtonsHorizontalLayout->setContentsMargins(11, 11, 11, 11);
        compPeaksButtonsHorizontalLayout->setObjectName(QString::fromUtf8("compPeaksButtonsHorizontalLayout"));
        compPeaksButtonsHorizontalLayout->setContentsMargins(0, 0, 0, 0);
        runCompPeaksInverseButton = new QPushButton(layoutWidget_24);
        runCompPeaksInverseButton->setObjectName(QString::fromUtf8("runCompPeaksInverseButton"));

        compPeaksButtonsHorizontalLayout->addWidget(runCompPeaksInverseButton);

        runCompPeaksClearButton = new QPushButton(layoutWidget_24);
        runCompPeaksClearButton->setObjectName(QString::fromUtf8("runCompPeaksClearButton"));

        compPeaksButtonsHorizontalLayout->addWidget(runCompPeaksClearButton);

        runCompPeaksButton = new QPushButton(layoutWidget_24);
        runCompPeaksButton->setObjectName(QString::fromUtf8("runCompPeaksButton"));

        compPeaksButtonsHorizontalLayout->addWidget(runCompPeaksButton);

        compPeaksTabWidget->addTab(compPeaksParametersTab, QString());
        compPeaksProgressTab = new QWidget();
        compPeaksProgressTab->setObjectName(QString::fromUtf8("compPeaksProgressTab"));
        compPeaksProgressTextEdit = new QTextEdit(compPeaksProgressTab);
        compPeaksProgressTextEdit->setObjectName(QString::fromUtf8("compPeaksProgressTextEdit"));
        compPeaksProgressTextEdit->setGeometry(QRect(0, 0, 641, 501));
        compPeaksProgressTextEdit->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);
        stopRunCompPeaksProcessButton = new QPushButton(compPeaksProgressTab);
        stopRunCompPeaksProcessButton->setObjectName(QString::fromUtf8("stopRunCompPeaksProcessButton"));
        stopRunCompPeaksProcessButton->setEnabled(false);
        stopRunCompPeaksProcessButton->setGeometry(QRect(520, 510, 113, 32));
        compPeaksTabWidget->addTab(compPeaksProgressTab, QString());

        verticalLayout_12->addWidget(compPeaksTabWidget);

        stackedWidget->addWidget(compPeaksStackedPage);
        compGenesStackedPage = new QWidget();
        compGenesStackedPage->setObjectName(QString::fromUtf8("compGenesStackedPage"));
        verticalLayout_13 = new QVBoxLayout(compGenesStackedPage);
        verticalLayout_13->setSpacing(6);
        verticalLayout_13->setContentsMargins(11, 11, 11, 11);
        verticalLayout_13->setObjectName(QString::fromUtf8("verticalLayout_13"));
        compGenesTabWidget = new QTabWidget(compGenesStackedPage);
        compGenesTabWidget->setObjectName(QString::fromUtf8("compGenesTabWidget"));
        compGenesParametersTab = new QWidget();
        compGenesParametersTab->setObjectName(QString::fromUtf8("compGenesParametersTab"));
        compGenesTitleLabel = new QLabel(compGenesParametersTab);
        compGenesTitleLabel->setObjectName(QString::fromUtf8("compGenesTitleLabel"));
        compGenesTitleLabel->setGeometry(QRect(0, 10, 588, 21));
        compGenesTitleLabel->setFont(font2);
        compGenesLine1 = new QFrame(compGenesParametersTab);
        compGenesLine1->setObjectName(QString::fromUtf8("compGenesLine1"));
        compGenesLine1->setGeometry(QRect(0, 30, 627, 3));
        compGenesLine1->setFrameShape(QFrame::HLine);
        compGenesLine1->setFrameShadow(QFrame::Sunken);
        layoutWidget_25 = new QWidget(compGenesParametersTab);
        layoutWidget_25->setObjectName(QString::fromUtf8("layoutWidget_25"));
        layoutWidget_25->setGeometry(QRect(0, 40, 641, 161));
        compGenesGridLayout = new QGridLayout(layoutWidget_25);
        compGenesGridLayout->setSpacing(6);
        compGenesGridLayout->setContentsMargins(11, 11, 11, 11);
        compGenesGridLayout->setObjectName(QString::fromUtf8("compGenesGridLayout"));
        compGenesGridLayout->setHorizontalSpacing(-1);
        compGenesGridLayout->setVerticalSpacing(15);
        compGenesGridLayout->setContentsMargins(0, 0, 0, 0);
        compGenesSelectFile1Label = new QLabel(layoutWidget_25);
        compGenesSelectFile1Label->setObjectName(QString::fromUtf8("compGenesSelectFile1Label"));

        compGenesGridLayout->addWidget(compGenesSelectFile1Label, 0, 0, 1, 1);

        compGenesSelectFile1LineEdit = new QLineEdit(layoutWidget_25);
        compGenesSelectFile1LineEdit->setObjectName(QString::fromUtf8("compGenesSelectFile1LineEdit"));
        compGenesSelectFile1LineEdit->setReadOnly(true);

        compGenesGridLayout->addWidget(compGenesSelectFile1LineEdit, 0, 1, 1, 2);

        compGenesSelectFile1ToolButton = new QToolButton(layoutWidget_25);
        compGenesSelectFile1ToolButton->setObjectName(QString::fromUtf8("compGenesSelectFile1ToolButton"));

        compGenesGridLayout->addWidget(compGenesSelectFile1ToolButton, 0, 3, 1, 1);

        compGenesSelectFile1InfoButton = new QPushButton(layoutWidget_25);
        compGenesSelectFile1InfoButton->setObjectName(QString::fromUtf8("compGenesSelectFile1InfoButton"));
        compGenesSelectFile1InfoButton->setMaximumSize(QSize(21, 21));
        compGenesSelectFile1InfoButton->setMouseTracking(false);
        compGenesSelectFile1InfoButton->setAcceptDrops(false);
        compGenesSelectFile1InfoButton->setLayoutDirection(Qt::LeftToRight);
        compGenesSelectFile1InfoButton->setIcon(icon1);
        compGenesSelectFile1InfoButton->setAutoRepeatDelay(100);
        compGenesSelectFile1InfoButton->setDefault(false);
        compGenesSelectFile1InfoButton->setFlat(true);

        compGenesGridLayout->addWidget(compGenesSelectFile1InfoButton, 0, 4, 1, 1);

        compGenesSelectFile2InfoButton = new QPushButton(layoutWidget_25);
        compGenesSelectFile2InfoButton->setObjectName(QString::fromUtf8("compGenesSelectFile2InfoButton"));
        compGenesSelectFile2InfoButton->setMaximumSize(QSize(21, 21));
        compGenesSelectFile2InfoButton->setMouseTracking(false);
        compGenesSelectFile2InfoButton->setAcceptDrops(false);
        compGenesSelectFile2InfoButton->setLayoutDirection(Qt::LeftToRight);
        compGenesSelectFile2InfoButton->setIcon(icon1);
        compGenesSelectFile2InfoButton->setAutoRepeatDelay(100);
        compGenesSelectFile2InfoButton->setDefault(false);
        compGenesSelectFile2InfoButton->setFlat(true);

        compGenesGridLayout->addWidget(compGenesSelectFile2InfoButton, 1, 4, 1, 1);

        compGenesRemoveDuplicatesInfoButton = new QPushButton(layoutWidget_25);
        compGenesRemoveDuplicatesInfoButton->setObjectName(QString::fromUtf8("compGenesRemoveDuplicatesInfoButton"));
        compGenesRemoveDuplicatesInfoButton->setMaximumSize(QSize(21, 21));
        compGenesRemoveDuplicatesInfoButton->setMouseTracking(false);
        compGenesRemoveDuplicatesInfoButton->setAcceptDrops(false);
        compGenesRemoveDuplicatesInfoButton->setLayoutDirection(Qt::LeftToRight);
        compGenesRemoveDuplicatesInfoButton->setIcon(icon1);
        compGenesRemoveDuplicatesInfoButton->setAutoRepeatDelay(100);
        compGenesRemoveDuplicatesInfoButton->setDefault(false);
        compGenesRemoveDuplicatesInfoButton->setFlat(true);

        compGenesGridLayout->addWidget(compGenesRemoveDuplicatesInfoButton, 3, 4, 1, 1);

        compGenesOutputFileInfoButton = new QPushButton(layoutWidget_25);
        compGenesOutputFileInfoButton->setObjectName(QString::fromUtf8("compGenesOutputFileInfoButton"));
        compGenesOutputFileInfoButton->setMaximumSize(QSize(21, 21));
        compGenesOutputFileInfoButton->setMouseTracking(false);
        compGenesOutputFileInfoButton->setAcceptDrops(false);
        compGenesOutputFileInfoButton->setLayoutDirection(Qt::LeftToRight);
        compGenesOutputFileInfoButton->setIcon(icon1);
        compGenesOutputFileInfoButton->setAutoRepeatDelay(100);
        compGenesOutputFileInfoButton->setDefault(false);
        compGenesOutputFileInfoButton->setFlat(true);

        compGenesGridLayout->addWidget(compGenesOutputFileInfoButton, 2, 4, 1, 1);

        compGenesSelectFile2Label = new QLabel(layoutWidget_25);
        compGenesSelectFile2Label->setObjectName(QString::fromUtf8("compGenesSelectFile2Label"));

        compGenesGridLayout->addWidget(compGenesSelectFile2Label, 1, 0, 1, 1);

        compGenesSelectFile2LineEdit = new QLineEdit(layoutWidget_25);
        compGenesSelectFile2LineEdit->setObjectName(QString::fromUtf8("compGenesSelectFile2LineEdit"));
        compGenesSelectFile2LineEdit->setReadOnly(true);

        compGenesGridLayout->addWidget(compGenesSelectFile2LineEdit, 1, 1, 1, 2);

        compGenesSelectFile2ToolButton = new QToolButton(layoutWidget_25);
        compGenesSelectFile2ToolButton->setObjectName(QString::fromUtf8("compGenesSelectFile2ToolButton"));

        compGenesGridLayout->addWidget(compGenesSelectFile2ToolButton, 1, 3, 1, 1);

        compGenesOutputFileLabel = new QLabel(layoutWidget_25);
        compGenesOutputFileLabel->setObjectName(QString::fromUtf8("compGenesOutputFileLabel"));

        compGenesGridLayout->addWidget(compGenesOutputFileLabel, 2, 0, 1, 1);

        compGenesOutputFileLineEdit = new QLineEdit(layoutWidget_25);
        compGenesOutputFileLineEdit->setObjectName(QString::fromUtf8("compGenesOutputFileLineEdit"));

        compGenesGridLayout->addWidget(compGenesOutputFileLineEdit, 2, 1, 1, 2);

        compGenesRemoveDuplicatesCheckBox = new QCheckBox(layoutWidget_25);
        compGenesRemoveDuplicatesCheckBox->setObjectName(QString::fromUtf8("compGenesRemoveDuplicatesCheckBox"));

        compGenesGridLayout->addWidget(compGenesRemoveDuplicatesCheckBox, 3, 0, 1, 1);

        compGenesLine2 = new QFrame(compGenesParametersTab);
        compGenesLine2->setObjectName(QString::fromUtf8("compGenesLine2"));
        compGenesLine2->setGeometry(QRect(0, 200, 629, 3));
        compGenesLine2->setFrameShadow(QFrame::Sunken);
        compGenesLine2->setFrameShape(QFrame::HLine);
        layoutWidget_26 = new QWidget(compGenesParametersTab);
        layoutWidget_26->setObjectName(QString::fromUtf8("layoutWidget_26"));
        layoutWidget_26->setGeometry(QRect(400, 200, 241, 41));
        compGenesButtonsHorizontalLayout = new QHBoxLayout(layoutWidget_26);
        compGenesButtonsHorizontalLayout->setSpacing(6);
        compGenesButtonsHorizontalLayout->setContentsMargins(11, 11, 11, 11);
        compGenesButtonsHorizontalLayout->setObjectName(QString::fromUtf8("compGenesButtonsHorizontalLayout"));
        compGenesButtonsHorizontalLayout->setContentsMargins(0, 0, 0, 0);
        runCompGenesClearButton = new QPushButton(layoutWidget_26);
        runCompGenesClearButton->setObjectName(QString::fromUtf8("runCompGenesClearButton"));

        compGenesButtonsHorizontalLayout->addWidget(runCompGenesClearButton);

        runCompGenesButton = new QPushButton(layoutWidget_26);
        runCompGenesButton->setObjectName(QString::fromUtf8("runCompGenesButton"));

        compGenesButtonsHorizontalLayout->addWidget(runCompGenesButton);

        compGenesTabWidget->addTab(compGenesParametersTab, QString());
        compGenesProgressTab = new QWidget();
        compGenesProgressTab->setObjectName(QString::fromUtf8("compGenesProgressTab"));
        compGenesProgressTextEdit = new QTextEdit(compGenesProgressTab);
        compGenesProgressTextEdit->setObjectName(QString::fromUtf8("compGenesProgressTextEdit"));
        compGenesProgressTextEdit->setGeometry(QRect(0, 0, 641, 501));
        compGenesProgressTextEdit->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);
        stopRunCompGenesProcessButton = new QPushButton(compGenesProgressTab);
        stopRunCompGenesProcessButton->setObjectName(QString::fromUtf8("stopRunCompGenesProcessButton"));
        stopRunCompGenesProcessButton->setEnabled(false);
        stopRunCompGenesProcessButton->setGeometry(QRect(520, 510, 113, 32));
        compGenesTabWidget->addTab(compGenesProgressTab, QString());

        verticalLayout_13->addWidget(compGenesTabWidget);

        stackedWidget->addWidget(compGenesStackedPage);
        jaccardStackedPage = new QWidget();
        jaccardStackedPage->setObjectName(QString::fromUtf8("jaccardStackedPage"));
        verticalLayout_18 = new QVBoxLayout(jaccardStackedPage);
        verticalLayout_18->setSpacing(6);
        verticalLayout_18->setContentsMargins(11, 11, 11, 11);
        verticalLayout_18->setObjectName(QString::fromUtf8("verticalLayout_18"));
        jaccardTabWidget = new QTabWidget(jaccardStackedPage);
        jaccardTabWidget->setObjectName(QString::fromUtf8("jaccardTabWidget"));
        jaccardParametersTab = new QWidget();
        jaccardParametersTab->setObjectName(QString::fromUtf8("jaccardParametersTab"));
        jaccardTitleLabel = new QLabel(jaccardParametersTab);
        jaccardTitleLabel->setObjectName(QString::fromUtf8("jaccardTitleLabel"));
        jaccardTitleLabel->setGeometry(QRect(0, 10, 588, 21));
        jaccardTitleLabel->setFont(font2);
        jaccardLine1 = new QFrame(jaccardParametersTab);
        jaccardLine1->setObjectName(QString::fromUtf8("jaccardLine1"));
        jaccardLine1->setGeometry(QRect(0, 30, 627, 3));
        jaccardLine1->setFrameShape(QFrame::HLine);
        jaccardLine1->setFrameShadow(QFrame::Sunken);
        layoutWidget_27 = new QWidget(jaccardParametersTab);
        layoutWidget_27->setObjectName(QString::fromUtf8("layoutWidget_27"));
        layoutWidget_27->setGeometry(QRect(0, 40, 641, 81));
        jaccardGridLayout = new QGridLayout(layoutWidget_27);
        jaccardGridLayout->setSpacing(6);
        jaccardGridLayout->setContentsMargins(11, 11, 11, 11);
        jaccardGridLayout->setObjectName(QString::fromUtf8("jaccardGridLayout"));
        jaccardGridLayout->setHorizontalSpacing(-1);
        jaccardGridLayout->setVerticalSpacing(15);
        jaccardGridLayout->setContentsMargins(0, 0, 0, 0);
        jaccardSelectFileLabel = new QLabel(layoutWidget_27);
        jaccardSelectFileLabel->setObjectName(QString::fromUtf8("jaccardSelectFileLabel"));

        jaccardGridLayout->addWidget(jaccardSelectFileLabel, 0, 0, 1, 1);

        jaccardSelectFileLineEdit = new QLineEdit(layoutWidget_27);
        jaccardSelectFileLineEdit->setObjectName(QString::fromUtf8("jaccardSelectFileLineEdit"));
        jaccardSelectFileLineEdit->setReadOnly(true);

        jaccardGridLayout->addWidget(jaccardSelectFileLineEdit, 0, 1, 1, 2);

        jaccardSelectFileToolButton = new QToolButton(layoutWidget_27);
        jaccardSelectFileToolButton->setObjectName(QString::fromUtf8("jaccardSelectFileToolButton"));

        jaccardGridLayout->addWidget(jaccardSelectFileToolButton, 0, 3, 1, 1);

        jaccardSelectFileInfoButton = new QPushButton(layoutWidget_27);
        jaccardSelectFileInfoButton->setObjectName(QString::fromUtf8("jaccardSelectFileInfoButton"));
        jaccardSelectFileInfoButton->setMaximumSize(QSize(21, 21));
        jaccardSelectFileInfoButton->setMouseTracking(false);
        jaccardSelectFileInfoButton->setAcceptDrops(false);
        jaccardSelectFileInfoButton->setLayoutDirection(Qt::LeftToRight);
        jaccardSelectFileInfoButton->setIcon(icon1);
        jaccardSelectFileInfoButton->setAutoRepeatDelay(100);
        jaccardSelectFileInfoButton->setDefault(false);
        jaccardSelectFileInfoButton->setFlat(true);

        jaccardGridLayout->addWidget(jaccardSelectFileInfoButton, 0, 4, 1, 1);

        jaccardOutputFileInfoButton = new QPushButton(layoutWidget_27);
        jaccardOutputFileInfoButton->setObjectName(QString::fromUtf8("jaccardOutputFileInfoButton"));
        jaccardOutputFileInfoButton->setMaximumSize(QSize(21, 21));
        jaccardOutputFileInfoButton->setMouseTracking(false);
        jaccardOutputFileInfoButton->setAcceptDrops(false);
        jaccardOutputFileInfoButton->setLayoutDirection(Qt::LeftToRight);
        jaccardOutputFileInfoButton->setIcon(icon1);
        jaccardOutputFileInfoButton->setAutoRepeatDelay(100);
        jaccardOutputFileInfoButton->setDefault(false);
        jaccardOutputFileInfoButton->setFlat(true);

        jaccardGridLayout->addWidget(jaccardOutputFileInfoButton, 1, 4, 1, 1);

        jaccardOutputFileLabel = new QLabel(layoutWidget_27);
        jaccardOutputFileLabel->setObjectName(QString::fromUtf8("jaccardOutputFileLabel"));

        jaccardGridLayout->addWidget(jaccardOutputFileLabel, 1, 0, 1, 1);

        jaccardOutputFileLineEdit = new QLineEdit(layoutWidget_27);
        jaccardOutputFileLineEdit->setObjectName(QString::fromUtf8("jaccardOutputFileLineEdit"));

        jaccardGridLayout->addWidget(jaccardOutputFileLineEdit, 1, 1, 1, 2);

        jaccardLine2 = new QFrame(jaccardParametersTab);
        jaccardLine2->setObjectName(QString::fromUtf8("jaccardLine2"));
        jaccardLine2->setGeometry(QRect(0, 120, 629, 3));
        jaccardLine2->setFrameShadow(QFrame::Sunken);
        jaccardLine2->setFrameShape(QFrame::HLine);
        layoutWidget_28 = new QWidget(jaccardParametersTab);
        layoutWidget_28->setObjectName(QString::fromUtf8("layoutWidget_28"));
        layoutWidget_28->setGeometry(QRect(410, 120, 231, 41));
        jaccardButtonsHorizontalLayout = new QHBoxLayout(layoutWidget_28);
        jaccardButtonsHorizontalLayout->setSpacing(6);
        jaccardButtonsHorizontalLayout->setContentsMargins(11, 11, 11, 11);
        jaccardButtonsHorizontalLayout->setObjectName(QString::fromUtf8("jaccardButtonsHorizontalLayout"));
        jaccardButtonsHorizontalLayout->setContentsMargins(0, 0, 0, 0);
        runJaccardClearButton = new QPushButton(layoutWidget_28);
        runJaccardClearButton->setObjectName(QString::fromUtf8("runJaccardClearButton"));

        jaccardButtonsHorizontalLayout->addWidget(runJaccardClearButton);

        runJaccardButton = new QPushButton(layoutWidget_28);
        runJaccardButton->setObjectName(QString::fromUtf8("runJaccardButton"));

        jaccardButtonsHorizontalLayout->addWidget(runJaccardButton);

        jaccardTabWidget->addTab(jaccardParametersTab, QString());
        jaccardProgressTab = new QWidget();
        jaccardProgressTab->setObjectName(QString::fromUtf8("jaccardProgressTab"));
        jaccardProgressTextEdit = new QTextEdit(jaccardProgressTab);
        jaccardProgressTextEdit->setObjectName(QString::fromUtf8("jaccardProgressTextEdit"));
        jaccardProgressTextEdit->setGeometry(QRect(0, 0, 641, 501));
        jaccardProgressTextEdit->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);
        stopRunJaccardProcessButton = new QPushButton(jaccardProgressTab);
        stopRunJaccardProcessButton->setObjectName(QString::fromUtf8("stopRunJaccardProcessButton"));
        stopRunJaccardProcessButton->setEnabled(false);
        stopRunJaccardProcessButton->setGeometry(QRect(520, 510, 113, 32));
        jaccardTabWidget->addTab(jaccardProgressTab, QString());

        verticalLayout_18->addWidget(jaccardTabWidget);

        stackedWidget->addWidget(jaccardStackedPage);
        CSDesktop->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(CSDesktop);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setEnabled(true);
        menuBar->setGeometry(QRect(0, 0, 862, 22));
        menuTools = new QMenu(menuBar);
        menuTools->setObjectName(QString::fromUtf8("menuTools"));
        menuHelp = new QMenu(menuBar);
        menuHelp->setObjectName(QString::fromUtf8("menuHelp"));
        CSDesktop->setMenuBar(menuBar);
        mainToolBar = new QToolBar(CSDesktop);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        CSDesktop->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(CSDesktop);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        QFont font4;
        font4.setPointSize(12);
        font4.setItalic(true);
        statusBar->setFont(font4);
        CSDesktop->setStatusBar(statusBar);

        menuBar->addAction(menuTools->menuAction());
        menuBar->addAction(menuHelp->menuAction());
        menuTools->addAction(aboutAction);
        menuHelp->addAction(actionTutorial);

        retranslateUi(CSDesktop);

        toolBox->layout()->setSpacing(4);
        stackedWidget->setCurrentIndex(6);
        loadTabWidget->setCurrentIndex(0);
        loadFormatComboBox->setCurrentIndex(0);
        peakDetectionTabWidget->setCurrentIndex(0);
        peakDetectionFormatComboBox->setCurrentIndex(0);
        peakDetectionSpeciesComboBox->setCurrentIndex(0);
        genesSummaryTabWidget->setCurrentIndex(0);
        genesSummarySpeciesComboBox->setCurrentIndex(0);
        genesSummaryDatabaseComboBox->setCurrentIndex(0);
        genomicDistTabWidget->setCurrentIndex(0);
        genomicDistSpeciesComboBox->setCurrentIndex(0);
        genomicDistDatabaseComboBox->setCurrentIndex(0);
        rnaGenesTabWidget->setCurrentIndex(0);
        encodeTabWidget->setCurrentIndex(0);
        encodeSpeciesComboBox->setCurrentIndex(0);
        encodeSelectDatasetComboBox->setCurrentIndex(-1);
        createTracksTabWidget->setCurrentIndex(0);
        repeatsTabWidget->setCurrentIndex(0);
        repeatsSpeciesComboBox->setCurrentIndex(0);
        cpgIslandsTabWidget->setCurrentIndex(0);
        cpgIslandsSpeciesComboBox->setCurrentIndex(0);
        segDuplicatesTabWidget->setCurrentIndex(0);
        segDuplicatesSpeciesComboBox->setCurrentIndex(0);
        findmotifTabWidget->setCurrentIndex(0);
        findmotifJasparMotifComboBox->setCurrentIndex(-1);
        findmotifBulykMotifComboBox->setCurrentIndex(-1);
        fireTabWidget->setCurrentIndex(0);
        fireSpeciesComboBox->setCurrentIndex(0);
        fireRandomModeComboBox->setCurrentIndex(0);
        findPathwayTabWidget->setCurrentIndex(0);
        findPathwaySelectDatabaseComboBox->setCurrentIndex(0);
        findPathwaySpeciesComboBox->setCurrentIndex(0);
        findPathwaySelectPathwayComboBox->setCurrentIndex(-1);
        findPathwaysDatabaseComboBox->setCurrentIndex(0);
        pageTabWidget->setCurrentIndex(0);
        pageSpeciesComboBox->setCurrentIndex(0);
        pageGenesDBComboBox->setCurrentIndex(0);
        pagePathwaysDBComboBox->setCurrentIndex(0);
        consTabWidget->setCurrentIndex(0);
        consMethodComboBox->setCurrentIndex(0);
        consFormatComboBox->setCurrentIndex(0);
        consSpeciesComboBox->setCurrentIndex(0);
        consCategoryComboBox->setCurrentIndex(0);
        compPeaksTabWidget->setCurrentIndex(0);
        compPeaksRandomModeComboBox->setCurrentIndex(0);
        compGenesTabWidget->setCurrentIndex(0);
        jaccardTabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(CSDesktop);
    } // setupUi

    void retranslateUi(QMainWindow *CSDesktop)
    {
        CSDesktop->setWindowTitle(QString());
#ifndef QT_NO_STATUSTIP
        CSDesktop->setStatusTip(QApplication::translate("CSDesktop", "Welcome to ChIPseeqer", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        pathwaysAnalysisAction->setText(QApplication::translate("CSDesktop", "Pathways Analysis", 0, QApplication::UnicodeUTF8));
        action->setText(QApplication::translate("CSDesktop", "Conservation Analysis", 0, QApplication::UnicodeUTF8));
        aboutAction->setText(QApplication::translate("CSDesktop", "About ChIPseeqer", 0, QApplication::UnicodeUTF8));
        comparePeaksMenu->setText(QApplication::translate("CSDesktop", "Compare Peaks", 0, QApplication::UnicodeUTF8));
        compareGenesMenu->setText(QApplication::translate("CSDesktop", "Compare Genes", 0, QApplication::UnicodeUTF8));
        repMaskerAction->setText(QApplication::translate("CSDesktop", "Repeats (RepMasker)", 0, QApplication::UnicodeUTF8));
        cpgIslandsAction->setText(QApplication::translate("CSDesktop", "CpG islands", 0, QApplication::UnicodeUTF8));
        segDupsAction->setText(QApplication::translate("CSDesktop", "Segmental Duplicates", 0, QApplication::UnicodeUTF8));
        findMotifMenu->setText(QApplication::translate("CSDesktop", "Find specific motif", 0, QApplication::UnicodeUTF8));
        loadDataAction->setText(QApplication::translate("CSDesktop", "Load data", 0, QApplication::UnicodeUTF8));
        peakDetectionAction_2->setText(QApplication::translate("CSDesktop", "Peak detection", 0, QApplication::UnicodeUTF8));
        genesSummaryAction->setText(QApplication::translate("CSDesktop", "Genes summary", 0, QApplication::UnicodeUTF8));
        genomicDistributionAction->setText(QApplication::translate("CSDesktop", "Genomic distribution of peaks", 0, QApplication::UnicodeUTF8));
        fireMenu->setText(QApplication::translate("CSDesktop", "De novo motif discovery", 0, QApplication::UnicodeUTF8));
        createTracksAction->setText(QApplication::translate("CSDesktop", "Create UCSC tracks", 0, QApplication::UnicodeUTF8));
        actionTutorial->setText(QApplication::translate("CSDesktop", "Tutorial", 0, QApplication::UnicodeUTF8));
        actionAbout->setText(QApplication::translate("CSDesktop", "About", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        loadRawDataButton->setStatusTip(QApplication::translate("CSDesktop", "Load and split read files into one read file per chromosome", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        loadRawDataButton->setText(QApplication::translate("CSDesktop", "Load raw data", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        peakDetectionButton->setStatusTip(QApplication::translate("CSDesktop", "Run peak detection algorithm", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        peakDetectionButton->setText(QApplication::translate("CSDesktop", "Peak Detection", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        createTracksButton->setStatusTip(QApplication::translate("CSDesktop", "Create tracks for the UCSC Genome Browser", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        createTracksButton->setText(QApplication::translate("CSDesktop", "Create UCSC Tracks", 0, QApplication::UnicodeUTF8));
        toolBox->setItemText(toolBox->indexOf(peakDetectionPage), QApplication::translate("CSDesktop", "Peak Detection", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        genesSummaryButton->setStatusTip(QApplication::translate("CSDesktop", "Quick summary of promoters with peaks", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        genesSummaryButton->setText(QApplication::translate("CSDesktop", "Promoters Summary", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        genomicDistButton->setStatusTip(QApplication::translate("CSDesktop", "Find peaks overlap with gene parts (e.g., promoters, exons, introns)", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        genomicDistButton->setText(QApplication::translate("CSDesktop", "Genomic Distribution", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        rnaGenesButton->setStatusTip(QApplication::translate("CSDesktop", "Find peaks overlap with RNA genes (e.g., snRNA, tRNA, miRNA)", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        rnaGenesButton->setText(QApplication::translate("CSDesktop", "RNA Genes", 0, QApplication::UnicodeUTF8));
        toolBox->setItemText(toolBox->indexOf(genelevelAnnotationpage), QApplication::translate("CSDesktop", "Gene-level annotation", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        encodeButton->setStatusTip(QApplication::translate("CSDesktop", "Find peaks overlap with ENCODE ChIP-seq datasets", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        encodeButton->setText(QApplication::translate("CSDesktop", "ENCODE ChIP-seq", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        repeatsButton->setStatusTip(QApplication::translate("CSDesktop", "Find peaks overlap with Repeats (using UCSC RepeatMasker track)", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        repeatsButton->setText(QApplication::translate("CSDesktop", "Find Repeats", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        cpgIslandsButton->setStatusTip(QApplication::translate("CSDesktop", "Find peaks overlap with CpG islands (using UCSC track)", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        cpgIslandsButton->setText(QApplication::translate("CSDesktop", "Find CpG islands", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        segDuplicatesButton->setStatusTip(QApplication::translate("CSDesktop", "Find peaks overlap with Segmental Duplicates (using UCSC track)", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        segDuplicatesButton->setText(QApplication::translate("CSDesktop", "Find Segm. Duplicates", 0, QApplication::UnicodeUTF8));
        toolBox->setItemText(toolBox->indexOf(annotationPage), QApplication::translate("CSDesktop", "Non-genic annotation", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        findmotifButton->setStatusTip(QApplication::translate("CSDesktop", "Find a specific motif (JASPAR, UniPROBE) within the detected peaks", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        findmotifButton->setText(QApplication::translate("CSDesktop", "Find motif in peaks", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        fireButton->setStatusTip(QApplication::translate("CSDesktop", "Perform de novo motif discovery (FIRE) within the detected peaks", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        fireButton->setText(QApplication::translate("CSDesktop", "FIRE", 0, QApplication::UnicodeUTF8));
        toolBox->setItemText(toolBox->indexOf(motifsAnalysisPage), QApplication::translate("CSDesktop", "Motif Analysis", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        findPathwayButton->setStatusTip(QApplication::translate("CSDesktop", "Find a specific pathway (apoptosis, GO:0006915) within the detected peaks", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        findPathwayButton->setText(QApplication::translate("CSDesktop", "Find pathway", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        pageButton->setStatusTip(QApplication::translate("CSDesktop", "Perform pathways analysis (iPAGE) for the genes found with peaks", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        pageButton->setText(QApplication::translate("CSDesktop", "iPAGE", 0, QApplication::UnicodeUTF8));
        toolBox->setItemText(toolBox->indexOf(pathwaysAnalysisPage), QApplication::translate("CSDesktop", "Pathways Analysis", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        consButton->setStatusTip(QApplication::translate("CSDesktop", "Evaluate conservation of detected peaks", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        consButton->setText(QApplication::translate("CSDesktop", "Compute cons. scores", 0, QApplication::UnicodeUTF8));
        toolBox->setItemText(toolBox->indexOf(consAnalysisPage), QApplication::translate("CSDesktop", "Conservation Analysis", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        comparePeaksButton->setStatusTip(QApplication::translate("CSDesktop", "Compare two lists of peaks", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        comparePeaksButton->setText(QApplication::translate("CSDesktop", "Compare peaks", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        compareGenesButton->setStatusTip(QApplication::translate("CSDesktop", "Compare two lists of genes", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        compareGenesButton->setText(QApplication::translate("CSDesktop", "Compare genes", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        jaccardButton->setStatusTip(QApplication::translate("CSDesktop", "Compute Jaccard similarity coefficient for multiple peak files", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        jaccardButton->setText(QApplication::translate("CSDesktop", "Similarity coefficient", 0, QApplication::UnicodeUTF8));
        toolBox->setItemText(toolBox->indexOf(comparisonToolsPage), QApplication::translate("CSDesktop", "Comparison tools", 0, QApplication::UnicodeUTF8));
        CSlogoLabel->setText(QString());
        developedByLabel->setText(QApplication::translate("CSDesktop", "Developed by Eugenia Giannopoulou\n"
" Elemento lab\n"
"Last update 2012", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        loadTitleLabel->setStatusTip(QApplication::translate("CSDesktop", "Load and split read files into one read file per chromosome.", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        loadTitleLabel->setText(QApplication::translate("CSDesktop", "Load raw data", 0, QApplication::UnicodeUTF8));
        selectFormatLabel->setText(QApplication::translate("CSDesktop", "Select data format", 0, QApplication::UnicodeUTF8));
        loadFormatComboBox->clear();
        loadFormatComboBox->insertItems(0, QStringList()
         << QApplication::translate("CSDesktop", "bam", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "sam", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "bowtiesam", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "eland", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "exteland", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "export", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "bed", 0, QApplication::UnicodeUTF8)
        );
        loadFormatInfoButton->setText(QString());
        selectChipRawLabel->setText(QApplication::translate("CSDesktop", "Select folder with ChIP data:", 0, QApplication::UnicodeUTF8));
        selectChipRawFolderToolButton->setText(QApplication::translate("CSDesktop", "...", 0, QApplication::UnicodeUTF8));
        selectInputRawLabel->setText(QApplication::translate("CSDesktop", "Select folder for INPUT data:", 0, QApplication::UnicodeUTF8));
        selectInputRawFolderToolButton->setText(QApplication::translate("CSDesktop", "...", 0, QApplication::UnicodeUTF8));
        loadClearButton->setText(QApplication::translate("CSDesktop", "Clear all", 0, QApplication::UnicodeUTF8));
        splitButton->setText(QApplication::translate("CSDesktop", "Split in chromosomes", 0, QApplication::UnicodeUTF8));
        loadTabWidget->setTabText(loadTabWidget->indexOf(loadParametersTab), QApplication::translate("CSDesktop", "Parameters", 0, QApplication::UnicodeUTF8));
        stopSplitProcessButton->setText(QApplication::translate("CSDesktop", "Stop", 0, QApplication::UnicodeUTF8));
        loadTabWidget->setTabText(loadTabWidget->indexOf(loadProgressTab), QApplication::translate("CSDesktop", "Progress", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        peakDetectionTitleLabel->setStatusTip(QApplication::translate("CSDesktop", "Run peak detection algorithm", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        peakDetectionTitleLabel->setText(QApplication::translate("CSDesktop", "Peak detection", 0, QApplication::UnicodeUTF8));
        selectChipFolderLabel->setText(QApplication::translate("CSDesktop", "Select ChIP folder:", 0, QApplication::UnicodeUTF8));
        selectChipFolderToolButton->setText(QApplication::translate("CSDesktop", "...", 0, QApplication::UnicodeUTF8));
        selectChipFolderInfoButton->setText(QString());
        selectInputFolderLabel->setText(QApplication::translate("CSDesktop", "Select INPUT folder:", 0, QApplication::UnicodeUTF8));
        selectInputFolderToolButton->setText(QApplication::translate("CSDesktop", "...", 0, QApplication::UnicodeUTF8));
        selectInputFolderInfoButton->setText(QString());
        peakDetectionOutputFileLabel->setText(QApplication::translate("CSDesktop", "Output file", 0, QApplication::UnicodeUTF8));
        peakDetectionOutputFileInfoButton->setText(QString());
        peakDetectionFormatLabel->setText(QApplication::translate("CSDesktop", "Format", 0, QApplication::UnicodeUTF8));
        peakDetectionFormatComboBox->clear();
        peakDetectionFormatComboBox->insertItems(0, QStringList()
         << QApplication::translate("CSDesktop", "bam", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "sam", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "bowtiesam", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "eland", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "exteland", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "export", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "bed", 0, QApplication::UnicodeUTF8)
        );
        peakDetectionFormatInfoButton->setText(QString());
        peakDetectionFoldLabel->setText(QApplication::translate("CSDesktop", "Fold change", 0, QApplication::UnicodeUTF8));
        peakDetectionFoldInfoButton->setText(QString());
        peakDetectionThresholdLabel->setText(QApplication::translate("CSDesktop", "Threshold", 0, QApplication::UnicodeUTF8));
        peakDetectionThresholdInfoButton->setText(QString());
        peakDetectionFragLenLabel->setText(QApplication::translate("CSDesktop", "Fragment length", 0, QApplication::UnicodeUTF8));
        peakDetectionFragLenInfoButton->setText(QString());
        peakDetectionMinLenLabel->setText(QApplication::translate("CSDesktop", "Minimum length", 0, QApplication::UnicodeUTF8));
        peakDetectionMinLenInfoButton->setText(QString());
        peakDetectionMinDistLabel->setText(QApplication::translate("CSDesktop", "Minimum distance", 0, QApplication::UnicodeUTF8));
        peakDetectionMinDistInfoButton->setText(QString());
        peakDetectionUniqueCheckBox->setText(QApplication::translate("CSDesktop", "Remove clonal reads", 0, QApplication::UnicodeUTF8));
        peakDetectionUniqueInfoButton->setText(QString());
        peakDetectionMinPeakHeightLabel->setText(QApplication::translate("CSDesktop", "Minimum peak height", 0, QApplication::UnicodeUTF8));
        peakDetectionMinPeakHeightInfoButton->setText(QString());
        peakDetectionSpeciesLabel->setText(QApplication::translate("CSDesktop", "Species", 0, QApplication::UnicodeUTF8));
        peakDetectionSpeciesComboBox->clear();
        peakDetectionSpeciesComboBox->insertItems(0, QStringList()
         << QApplication::translate("CSDesktop", "Homo sapiens (hg19)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Homo sapiens (hg18)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Mus musculus (mm10)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Mus musculus (mm9)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Rattus norvegicus (rn4)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Saccharomyces cerevisiae (sacCer2)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Drosophila melanogaster (dm3)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Zebrafish (zv9)", 0, QApplication::UnicodeUTF8)
        );
        peakDetectionSpeciesInfoButton->setText(QString());
        runPeakDetectionInverseButton->setText(QApplication::translate("CSDesktop", "Inverse folders", 0, QApplication::UnicodeUTF8));
        peakDetectionClearButton->setText(QApplication::translate("CSDesktop", "Clear all", 0, QApplication::UnicodeUTF8));
        runCSButton->setText(QApplication::translate("CSDesktop", "Run", 0, QApplication::UnicodeUTF8));
        peakDetectionTabWidget->setTabText(peakDetectionTabWidget->indexOf(peakDetectionParametersTab), QApplication::translate("CSDesktop", "Parameters", 0, QApplication::UnicodeUTF8));
        stopRunCSProcessButton->setText(QApplication::translate("CSDesktop", "Stop", 0, QApplication::UnicodeUTF8));
        peakDetectionTabWidget->setTabText(peakDetectionTabWidget->indexOf(peakDetectionProgressTab), QApplication::translate("CSDesktop", "Progress", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        genesSummaryTitleLabel->setStatusTip(QApplication::translate("CSDesktop", "Find genes close to the detected peaks", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        genesSummaryTitleLabel->setText(QApplication::translate("CSDesktop", "Promoters Summary", 0, QApplication::UnicodeUTF8));
        genesSummarySelectPeaksLabel->setText(QApplication::translate("CSDesktop", "Select peak file:", 0, QApplication::UnicodeUTF8));
        genesSummarySelectPeaksToolButton->setText(QApplication::translate("CSDesktop", "...", 0, QApplication::UnicodeUTF8));
        genesSummarySelectPeaksInfoButton->setText(QString());
        genesSummaryLenULabel->setText(QApplication::translate("CSDesktop", "Length upstream of TSS (bp)", 0, QApplication::UnicodeUTF8));
        genesSummaryLenDLabel->setText(QApplication::translate("CSDesktop", "Length downstream of TSS (bp)", 0, QApplication::UnicodeUTF8));
        genesSummaryOutputFileLabel->setText(QApplication::translate("CSDesktop", "Output files", 0, QApplication::UnicodeUTF8));
        genesSummaryOutputFileInfoButton->setText(QString());
#ifndef QT_NO_STATUSTIP
        genesSummarySuffixLabel->setStatusTip(QApplication::translate("CSDesktop", "Output files end in: .NM.txt and .SUM.txt", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        genesSummarySuffixLabel->setText(QApplication::translate("CSDesktop", "*.txt", 0, QApplication::UnicodeUTF8));
        genesSummarySpeciesLabel->setText(QApplication::translate("CSDesktop", "Species", 0, QApplication::UnicodeUTF8));
        genesSummarySpeciesComboBox->clear();
        genesSummarySpeciesComboBox->insertItems(0, QStringList()
         << QApplication::translate("CSDesktop", "Homo sapiens (hg19)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Homo sapiens (hg18)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Mus musculus (mm10)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Mus musculus (mm9)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Rattus norvegicus (rn4)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Saccharomyces cerevisiae (sacCer2)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Drosophila melanogaster (dm3)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Zebrafish (zv9)", 0, QApplication::UnicodeUTF8)
        );
        genesSummaryDatabaseComboBox->clear();
        genesSummaryDatabaseComboBox->insertItems(0, QStringList()
         << QApplication::translate("CSDesktop", "refSeq", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Ensembl", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "UCSCGenes", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "AceView", 0, QApplication::UnicodeUTF8)
        );
        genesSummaryDatabaseLabel->setText(QApplication::translate("CSDesktop", "Database", 0, QApplication::UnicodeUTF8));
        genesSummarySpeciesInfoButton->setText(QString());
        genesSummaryDatabaseInfoButton->setText(QString());
        genesSummaryLenUInfoButton->setText(QString());
        genesSummaryLenDInfoButton->setText(QString());
        runSummaryClearButton->setText(QApplication::translate("CSDesktop", "Clear all", 0, QApplication::UnicodeUTF8));
        runSummaryButton->setText(QApplication::translate("CSDesktop", "Run", 0, QApplication::UnicodeUTF8));
        genesSummaryTabWidget->setTabText(genesSummaryTabWidget->indexOf(genesSummaryParametersTab), QApplication::translate("CSDesktop", "Parameters", 0, QApplication::UnicodeUTF8));
        stopRunSummaryProcessButton->setText(QApplication::translate("CSDesktop", "Stop", 0, QApplication::UnicodeUTF8));
        genesSummaryTabWidget->setTabText(genesSummaryTabWidget->indexOf(genesSummaryProgressTab), QApplication::translate("CSDesktop", "Progress", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        genomicDistTitleLabel->setStatusTip(QApplication::translate("CSDesktop", "Find peaks overlap with gene parts (e.g., promoters, exons, introns)", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        genomicDistTitleLabel->setText(QApplication::translate("CSDesktop", "Genomic Distribution", 0, QApplication::UnicodeUTF8));
        genomicDistSelectPeaksLabel->setText(QApplication::translate("CSDesktop", "Select peak file:", 0, QApplication::UnicodeUTF8));
        genomicDistSelectPeaksToolButton->setText(QApplication::translate("CSDesktop", "...", 0, QApplication::UnicodeUTF8));
        genomicDistSelectPeaksInfoButton->setText(QString());
        genomicDistOutputFileLabel->setText(QApplication::translate("CSDesktop", "Output files", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        genomicDistSuffixLabel->setStatusTip(QApplication::translate("CSDesktop", "Output files end in: .NM.txt and .SUM.txt", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        genomicDistSuffixLabel->setText(QApplication::translate("CSDesktop", "*.txt", 0, QApplication::UnicodeUTF8));
        genomicDistOutputFileInfoButton->setText(QString());
        genomicDistLenPLabel->setText(QApplication::translate("CSDesktop", "Length up/downstream of TSS (bp)", 0, QApplication::UnicodeUTF8));
        genomicDistLenDWLabel->setText(QApplication::translate("CSDesktop", "Length up/downstream of TES (bp)", 0, QApplication::UnicodeUTF8));
        genomicDistLenPInfoButton->setText(QString());
        genomicDistLenDWInfoButton->setText(QString());
        genomicDistTypeInfoButton->setText(QString());
        genomicDistTypeLabel->setText(QApplication::translate("CSDesktop", "Type of annotation", 0, QApplication::UnicodeUTF8));
        genomicDistMindistawayLabel->setText(QApplication::translate("CSDesktop", "Min/max distance away (distal peaks)", 0, QApplication::UnicodeUTF8));
        genomicDistMindistawayInfoButton->setText(QString());
        peakBasedRadioButton->setText(QApplication::translate("CSDesktop", "Peak-based", 0, QApplication::UnicodeUTF8));
        geneBasedRadioButton->setText(QApplication::translate("CSDesktop", "Gene-based", 0, QApplication::UnicodeUTF8));
        genomicDistSpeciesLabel->setText(QApplication::translate("CSDesktop", "Species", 0, QApplication::UnicodeUTF8));
        genomicDistSpeciesComboBox->clear();
        genomicDistSpeciesComboBox->insertItems(0, QStringList()
         << QApplication::translate("CSDesktop", "Homo sapiens (hg19)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Homo sapiens (hg18)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Mus musculus (mm10)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Mus musculus (mm9)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Rattus norvegicus (rn4)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Saccharomyces cerevisiae (sacCer2)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Drosophila melanogaster (dm3)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Zebrafish (Zv9)", 0, QApplication::UnicodeUTF8)
        );
        genomicDistSpeciesInfoButton->setText(QString());
        genomicDistDatabaseLabel->setText(QApplication::translate("CSDesktop", "Database", 0, QApplication::UnicodeUTF8));
        genomicDistDatabaseComboBox->clear();
        genomicDistDatabaseComboBox->insertItems(0, QStringList()
         << QApplication::translate("CSDesktop", "refSeq", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Ensembl", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "UCSCGenes", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "AceView", 0, QApplication::UnicodeUTF8)
        );
        genomicDistDatabaseInfoButton->setText(QString());
        runGenomicDistClearButton->setText(QApplication::translate("CSDesktop", "Clear all", 0, QApplication::UnicodeUTF8));
        runGenomicDistButton->setText(QApplication::translate("CSDesktop", "Run", 0, QApplication::UnicodeUTF8));
        genomicDistTabWidget->setTabText(genomicDistTabWidget->indexOf(genomicDistParametersTab), QApplication::translate("CSDesktop", "Parameters", 0, QApplication::UnicodeUTF8));
        stopRunGenomicDistProcessButton->setText(QApplication::translate("CSDesktop", "Stop", 0, QApplication::UnicodeUTF8));
        genomicDistTabWidget->setTabText(genomicDistTabWidget->indexOf(genomicDistProgressTab), QApplication::translate("CSDesktop", "Progress", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        rnaGenesTitleLabel->setStatusTip(QApplication::translate("CSDesktop", "Find peaks overlap with RNA genes (e.g., snRNA, tRNA, miRNA)", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        rnaGenesTitleLabel->setText(QApplication::translate("CSDesktop", "<html><head/><body><p>Find RNA genes <span style=\" color:#ff0000;\">(hg18 only)</span></p></body></html>", 0, QApplication::UnicodeUTF8));
        rnaGenesSelectPeaksLabel->setText(QApplication::translate("CSDesktop", "Select peak file:", 0, QApplication::UnicodeUTF8));
        rnaGenesSelectPeaksToolButton->setText(QApplication::translate("CSDesktop", "...", 0, QApplication::UnicodeUTF8));
        rnaGenesSelectPeaksInfoButton->setText(QString());
        rnaGenesOutputFileLabel->setText(QApplication::translate("CSDesktop", "Output files", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        rnaGenesSuffixLabel->setStatusTip(QApplication::translate("CSDesktop", "Output files end in: .NM.txt and .SUM.txt", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        rnaGenesSuffixLabel->setText(QApplication::translate("CSDesktop", "*.txt", 0, QApplication::UnicodeUTF8));
        rnaGenesOutputFileInfoButton->setText(QString());
        runFindRNAGenesClearButton->setText(QApplication::translate("CSDesktop", "Clear all", 0, QApplication::UnicodeUTF8));
        runFindRNAGenesButton->setText(QApplication::translate("CSDesktop", "Run", 0, QApplication::UnicodeUTF8));
        rnaGenesTabWidget->setTabText(rnaGenesTabWidget->indexOf(rnaGenesParametersTab), QApplication::translate("CSDesktop", "Parameters", 0, QApplication::UnicodeUTF8));
        stopRunFindRNAGenesProcessButton->setText(QApplication::translate("CSDesktop", "Stop", 0, QApplication::UnicodeUTF8));
        rnaGenesTabWidget->setTabText(rnaGenesTabWidget->indexOf(rnaGenesProgressTab), QApplication::translate("CSDesktop", "Progress", 0, QApplication::UnicodeUTF8));
        encodeSpeciesComboBox->clear();
        encodeSpeciesComboBox->insertItems(0, QStringList()
         << QApplication::translate("CSDesktop", "Homo sapiens (hg19)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Homo sapiens (hg18)", 0, QApplication::UnicodeUTF8)
        );
#ifndef QT_NO_STATUSTIP
        encodeSelectPeaksLabel->setStatusTip(QString());
#endif // QT_NO_STATUSTIP
        encodeSelectPeaksLabel->setText(QApplication::translate("CSDesktop", "Select peak file:", 0, QApplication::UnicodeUTF8));
        encodeSelectPeaksInfoButton->setText(QString());
        encodePrefixFileLabel->setText(QApplication::translate("CSDesktop", "Prefix", 0, QApplication::UnicodeUTF8));
        encodePrefixFileInfoButton->setText(QString());
        encodeSelectDatasetRadioButton->setText(QApplication::translate("CSDesktop", "Select dataset", 0, QApplication::UnicodeUTF8));
        encodeSelectDatasetInfoButton->setText(QString());
        encodeWriteDatasetRadioButton->setText(QApplication::translate("CSDesktop", "Write dataset name", 0, QApplication::UnicodeUTF8));
        encodeWriteDatasetInfoButton->setText(QString());
        encodeSelectPeaksToolButton->setText(QApplication::translate("CSDesktop", "...", 0, QApplication::UnicodeUTF8));
        encodeIterationsLabel->setText(QApplication::translate("CSDesktop", "Iterations", 0, QApplication::UnicodeUTF8));
        encodeIterationsInfoButton->setText(QString());
        encodeALLCheckBox->setText(QApplication::translate("CSDesktop", "Run against all ENCODE datasets", 0, QApplication::UnicodeUTF8));
        encodeALLInfoButton->setText(QString());
        encodeSpeciesLabel->setText(QApplication::translate("CSDesktop", "Species", 0, QApplication::UnicodeUTF8));
        encodeSpeciesInfoButton->setText(QString());
        runEncodeClearButton->setText(QApplication::translate("CSDesktop", "Clear all", 0, QApplication::UnicodeUTF8));
        runEncodeButton->setText(QApplication::translate("CSDesktop", "Run", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        encodeTitleLabel->setStatusTip(QApplication::translate("CSDesktop", "Find peaks overlap with Repeats (using UCSC RepeatMasker track)", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        encodeTitleLabel->setText(QApplication::translate("CSDesktop", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Lucida Grande'; font-size:14pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Find overlap with ENCODE <span style=\" color:#ff0000;\">(hg18/hg19 only)</span></p></body></html>", 0, QApplication::UnicodeUTF8));
        encodeTabWidget->setTabText(encodeTabWidget->indexOf(encodeParametersTab), QApplication::translate("CSDesktop", "Parameters", 0, QApplication::UnicodeUTF8));
        stopRunEncodeProcessButton->setText(QApplication::translate("CSDesktop", "Stop", 0, QApplication::UnicodeUTF8));
        encodeTabWidget->setTabText(encodeTabWidget->indexOf(encodeProgressTab), QApplication::translate("CSDesktop", "Progress", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        createTracksTitleLabel1->setToolTip(QString());
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        createTracksTitleLabel1->setStatusTip(QApplication::translate("CSDesktop", "Create a peak locations track for the UCSC Genome Browser", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        createTracksTitleLabel1->setText(QApplication::translate("CSDesktop", "Detected peaks Track", 0, QApplication::UnicodeUTF8));
        peaksTrackSelectPeaksLabel->setText(QApplication::translate("CSDesktop", "Select peak file:", 0, QApplication::UnicodeUTF8));
        peaksTrackSelectPeaksToolButton->setText(QApplication::translate("CSDesktop", "...", 0, QApplication::UnicodeUTF8));
        peaksTrackSelectPeaksInfoButton->setText(QString());
        peaksTrackNameLabel->setText(QApplication::translate("CSDesktop", "Track name", 0, QApplication::UnicodeUTF8));
        peaksTrackNameInfoButton->setText(QString());
        peaksTrackOutputFileLabel->setText(QApplication::translate("CSDesktop", "Output file", 0, QApplication::UnicodeUTF8));
        peaksTrackSuffixLabel->setText(QApplication::translate("CSDesktop", ".wgl.gz", 0, QApplication::UnicodeUTF8));
        peaksTrackOutputFileInfoButton->setText(QString());
        peaksTrackClearButton->setText(QApplication::translate("CSDesktop", "Clear all", 0, QApplication::UnicodeUTF8));
        createPeaksTrackButton->setText(QApplication::translate("CSDesktop", "Run", 0, QApplication::UnicodeUTF8));
        readsTrackClearButton->setText(QApplication::translate("CSDesktop", "Clear all", 0, QApplication::UnicodeUTF8));
        createReadsTrackButton->setText(QApplication::translate("CSDesktop", "Run", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        createTracksTitleLabel2->setToolTip(QString());
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        createTracksTitleLabel2->setStatusTip(QApplication::translate("CSDesktop", "Smooth read counts across the whole genome and create a read density track for the UCSC Genome Browser", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        createTracksTitleLabel2->setText(QApplication::translate("CSDesktop", "Smoothed reads Track", 0, QApplication::UnicodeUTF8));
        readsTrackSelectReadsLabel->setText(QApplication::translate("CSDesktop", "Select reads folder:", 0, QApplication::UnicodeUTF8));
        readsTrackSelectReadsToolButton->setText(QApplication::translate("CSDesktop", "...", 0, QApplication::UnicodeUTF8));
        readsTrackSelectReadsInfoButton->setText(QString());
        readsTrackNameLabel->setText(QApplication::translate("CSDesktop", "Track name", 0, QApplication::UnicodeUTF8));
        readsTrackNameInfoButton->setText(QString());
        readsTrackUniqueInfoButton->setText(QString());
        readsTrackUniqueCheckBox->setText(QApplication::translate("CSDesktop", "Remove clonal reads", 0, QApplication::UnicodeUTF8));
        readsTrackOutputFileLabel->setText(QApplication::translate("CSDesktop", "Output file", 0, QApplication::UnicodeUTF8));
        readsTrackOutputFileInfoButton->setText(QString());
        readsTrackSuffixLabel->setText(QApplication::translate("CSDesktop", ".wgl.gz", 0, QApplication::UnicodeUTF8));
        readsTrackBigWigCheckBox->setText(QApplication::translate("CSDesktop", "bigWig format", 0, QApplication::UnicodeUTF8));
        readsTrackBigWigInfoButton->setText(QString());
        createTracksTabWidget->setTabText(createTracksTabWidget->indexOf(createTracksParametersTab), QApplication::translate("CSDesktop", "Parameters", 0, QApplication::UnicodeUTF8));
        openGenomeBrowserButton->setText(QApplication::translate("CSDesktop", "Open UCSC Genome Browser", 0, QApplication::UnicodeUTF8));
        stopCreateTracksProcessButton->setText(QApplication::translate("CSDesktop", "Stop", 0, QApplication::UnicodeUTF8));
        createTracksTabWidget->setTabText(createTracksTabWidget->indexOf(createTracksProgressTab), QApplication::translate("CSDesktop", "Progress", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        repeatsTitleLabel->setStatusTip(QApplication::translate("CSDesktop", "Find peaks overlap with Repeats (using UCSC RepeatMasker track)", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        repeatsTitleLabel->setText(QApplication::translate("CSDesktop", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Lucida Grande'; font-size:14pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Find overlap with Repeats</p></body></html>", 0, QApplication::UnicodeUTF8));
        repeatsSpeciesComboBox->clear();
        repeatsSpeciesComboBox->insertItems(0, QStringList()
         << QApplication::translate("CSDesktop", "Homo sapiens (hg19)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Homo sapiens (hg18)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Mus musculus (mm10)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Mus musculus (mm9)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Rattus norvegicus (rn4)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Zebrafish (Zv9)", 0, QApplication::UnicodeUTF8)
        );
        repeatsSpeciesInfoButton->setText(QString());
        repeatsSelectPeaksLabel->setText(QApplication::translate("CSDesktop", "Select peak file:", 0, QApplication::UnicodeUTF8));
        repeatsSelectPeaksToolButton->setText(QApplication::translate("CSDesktop", "...", 0, QApplication::UnicodeUTF8));
        repeatsSelectPeaksInfoButton->setText(QString());
        repeatsOutputFileLabel->setText(QApplication::translate("CSDesktop", "Output files", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        repeatsSuffixLabel->setStatusTip(QApplication::translate("CSDesktop", "Output files end in: .NM.txt and .SUM.txt", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        repeatsSuffixLabel->setText(QApplication::translate("CSDesktop", "*.txt", 0, QApplication::UnicodeUTF8));
        repeatsOutputFileInfoButton->setText(QString());
        repeatsSpeciesLabel->setText(QApplication::translate("CSDesktop", "Species", 0, QApplication::UnicodeUTF8));
        runFindRepeatsClearButton->setText(QApplication::translate("CSDesktop", "Clear all", 0, QApplication::UnicodeUTF8));
        runFindRepeatsButton->setText(QApplication::translate("CSDesktop", "Run", 0, QApplication::UnicodeUTF8));
        repeatsTabWidget->setTabText(repeatsTabWidget->indexOf(repeatsParametersTab), QApplication::translate("CSDesktop", "Parameters", 0, QApplication::UnicodeUTF8));
        stopRunFindRepeatsProcessButton->setText(QApplication::translate("CSDesktop", "Stop", 0, QApplication::UnicodeUTF8));
        repeatsTabWidget->setTabText(repeatsTabWidget->indexOf(repeatsProgressTab), QApplication::translate("CSDesktop", "Progress", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        cpgIslandsStackedPage->setStatusTip(QApplication::translate("CSDesktop", "Find peaks overlap with CpG islands", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_STATUSTIP
        cpgIslandsTitleLabel->setStatusTip(QApplication::translate("CSDesktop", "Find peaks overlap with CpG islands (using UCSC track)", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        cpgIslandsTitleLabel->setText(QApplication::translate("CSDesktop", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Lucida Grande'; font-size:14pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Find overlap with CpG islands</p></body></html>", 0, QApplication::UnicodeUTF8));
        cpgIslandsSpeciesComboBox->clear();
        cpgIslandsSpeciesComboBox->insertItems(0, QStringList()
         << QApplication::translate("CSDesktop", "Homo sapiens (hg19)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Homo sapiens (hg18)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Mus musculus (mm10)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Mus musculus (mm9)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Rattus norvegicus (rn4)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Zebrafish (Zv9)", 0, QApplication::UnicodeUTF8)
        );
        cpgIslandsSpeciesInfoButton->setText(QString());
        cpgIslandsSpeciesLabel->setText(QApplication::translate("CSDesktop", "Species", 0, QApplication::UnicodeUTF8));
        cpgIslandsSelectPeaksLabel->setText(QApplication::translate("CSDesktop", "Select peak file:", 0, QApplication::UnicodeUTF8));
        cpgIslandsSelectPeaksToolButton->setText(QApplication::translate("CSDesktop", "...", 0, QApplication::UnicodeUTF8));
        cpgIslandsSelectPeaksInfoButton->setText(QString());
        cpgIslandsOutputFileLabel->setText(QApplication::translate("CSDesktop", "Output files", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        cpgIslandsSuffixLabel->setStatusTip(QApplication::translate("CSDesktop", "Output files end in: .NM.txt and .SUM.txt", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        cpgIslandsSuffixLabel->setText(QApplication::translate("CSDesktop", "*.txt", 0, QApplication::UnicodeUTF8));
        cpgIslandsOutputFileInfoButton->setText(QString());
        runFindCpgIslandsClearButton->setText(QApplication::translate("CSDesktop", "Clear all", 0, QApplication::UnicodeUTF8));
        runFindCpgIslandsButton->setText(QApplication::translate("CSDesktop", "Run", 0, QApplication::UnicodeUTF8));
        cpgIslandsTabWidget->setTabText(cpgIslandsTabWidget->indexOf(cpgIslandsParametersTab), QApplication::translate("CSDesktop", "Parameters", 0, QApplication::UnicodeUTF8));
        stopRunFindCpgIslandsProcessButton->setText(QApplication::translate("CSDesktop", "Stop", 0, QApplication::UnicodeUTF8));
        cpgIslandsTabWidget->setTabText(cpgIslandsTabWidget->indexOf(cpgIslandsProgressTab), QApplication::translate("CSDesktop", "Progress", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        segDuplicatesStackedPage->setStatusTip(QApplication::translate("CSDesktop", "Find peaks overlap with segmental duplicates", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_STATUSTIP
        segDuplicatesTitleLabel->setStatusTip(QApplication::translate("CSDesktop", "Find peaks overlap with promoters, exons and introns", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        segDuplicatesTitleLabel->setText(QApplication::translate("CSDesktop", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Lucida Grande'; font-size:14pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Find overlap with Segmental Duplicates</p></body></html>", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        segDuplicatesSelectPeaksLabel->setStatusTip(QString());
#endif // QT_NO_STATUSTIP
        segDuplicatesSelectPeaksLabel->setText(QApplication::translate("CSDesktop", "Select peak file:", 0, QApplication::UnicodeUTF8));
        segDuplicatesSelectPeaksToolButton->setText(QApplication::translate("CSDesktop", "...", 0, QApplication::UnicodeUTF8));
        segDuplicatesSelectPeaksInfoButton->setText(QString());
        segDuplicatesOutputFileLabel->setText(QApplication::translate("CSDesktop", "Output files", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        segDuplicatesSuffixLabel->setStatusTip(QApplication::translate("CSDesktop", "Output files end in: .NM.txt and .SUM.txt", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        segDuplicatesSuffixLabel->setText(QApplication::translate("CSDesktop", "*.txt", 0, QApplication::UnicodeUTF8));
        segDuplicatesOutputFileInfoButton->setText(QString());
        segDuplicatesSpeciesComboBox->clear();
        segDuplicatesSpeciesComboBox->insertItems(0, QStringList()
         << QApplication::translate("CSDesktop", "Homo sapiens (hg19)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Homo sapiens (hg18)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Mus musculus (mm9)", 0, QApplication::UnicodeUTF8)
        );
        segDuplicatesSpeciesInfoButton->setText(QString());
        segDuplicatesSpeciesLabel->setText(QApplication::translate("CSDesktop", "Species", 0, QApplication::UnicodeUTF8));
        runFindSegDuplicatesClearButton->setText(QApplication::translate("CSDesktop", "Clear all", 0, QApplication::UnicodeUTF8));
        runFindSegDuplicatesButton->setText(QApplication::translate("CSDesktop", "Run", 0, QApplication::UnicodeUTF8));
        segDuplicatesTabWidget->setTabText(segDuplicatesTabWidget->indexOf(segDuplicatesParametersTab), QApplication::translate("CSDesktop", "Parameters", 0, QApplication::UnicodeUTF8));
        stopRunFindSegDuplicatesProcessButton->setText(QApplication::translate("CSDesktop", "Stop", 0, QApplication::UnicodeUTF8));
        segDuplicatesTabWidget->setTabText(segDuplicatesTabWidget->indexOf(segDuplicatesProgressTab), QApplication::translate("CSDesktop", "Progress", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        findmotifTitleLabel->setStatusTip(QApplication::translate("CSDesktop", "Find a specific motif within the detected peaks", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        findmotifTitleLabel->setText(QApplication::translate("CSDesktop", "Find motif in peaks", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        findmotifSelectPeaksLabel->setStatusTip(QString());
#endif // QT_NO_STATUSTIP
        findmotifSelectPeaksLabel->setText(QApplication::translate("CSDesktop", "Select peak file:", 0, QApplication::UnicodeUTF8));
        findmotifSelectPeaksToolButton->setText(QApplication::translate("CSDesktop", "...", 0, QApplication::UnicodeUTF8));
        findmotifSelectPeaksInfoButton->setText(QString());
        findmotifScoreLabel->setText(QApplication::translate("CSDesktop", "Score threshold", 0, QApplication::UnicodeUTF8));
        findmotifOutputFileLabel->setText(QApplication::translate("CSDesktop", "Output file", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        findmotifSuffixLabel->setStatusTip(QApplication::translate("CSDesktop", "Output files end in: .NM.txt and .SUM.txt", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        findmotifSuffixLabel->setText(QApplication::translate("CSDesktop", ".txt", 0, QApplication::UnicodeUTF8));
        findmotifOutputFileInfoButton->setText(QString());
        findmotifScoreInfoButton->setText(QString());
        findmotifGenomeLabel->setText(QApplication::translate("CSDesktop", "Genome:", 0, QApplication::UnicodeUTF8));
        findmotifGenomeInfoButton->setText(QString());
        findmotifGenomeToolButton->setText(QApplication::translate("CSDesktop", "...", 0, QApplication::UnicodeUTF8));
        findmotifNameRadioButton->setText(QApplication::translate("CSDesktop", "Select motif name", 0, QApplication::UnicodeUTF8));
        findmotifNameInfoButton->setText(QString());
        findmotifJasparRadioButton->setText(QApplication::translate("CSDesktop", "JASPAR", 0, QApplication::UnicodeUTF8));
        findmotifBulykRadioButton->setText(QApplication::translate("CSDesktop", "UniPROBE", 0, QApplication::UnicodeUTF8));
        findmotifSequenceRadioButton->setText(QApplication::translate("CSDesktop", "Write motif sequence", 0, QApplication::UnicodeUTF8));
        findmotifSequenceInfoButton->setText(QString());
        runFindmotifClearButton->setText(QApplication::translate("CSDesktop", "Clear all", 0, QApplication::UnicodeUTF8));
        runFindmotifButton->setText(QApplication::translate("CSDesktop", "Run", 0, QApplication::UnicodeUTF8));
        findmotifTabWidget->setTabText(findmotifTabWidget->indexOf(findmotifParametersTab), QApplication::translate("CSDesktop", "Parameters", 0, QApplication::UnicodeUTF8));
        stopRunFindmotifProcessButton->setText(QApplication::translate("CSDesktop", "Stop", 0, QApplication::UnicodeUTF8));
        findmotifTabWidget->setTabText(findmotifTabWidget->indexOf(findmotifProgressTab), QApplication::translate("CSDesktop", "Progress", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        fireTitleLabel->setStatusTip(QApplication::translate("CSDesktop", "Perform de novo motif discovery (FIRE) within the detected peaks", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        fireTitleLabel->setText(QApplication::translate("CSDesktop", "FIRE", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        fireSelectPeaksLabel->setStatusTip(QString());
#endif // QT_NO_STATUSTIP
        fireSelectPeaksLabel->setText(QApplication::translate("CSDesktop", "Select peak file:", 0, QApplication::UnicodeUTF8));
        fireSelectPeaksToolButton->setText(QApplication::translate("CSDesktop", "...", 0, QApplication::UnicodeUTF8));
        fireSelectPeaksInfoButton->setText(QString());
        fireGenomeLabel->setText(QApplication::translate("CSDesktop", "Genome:", 0, QApplication::UnicodeUTF8));
        fireGenomeInfoButton->setText(QString());
        fireGenomeToolButton->setText(QApplication::translate("CSDesktop", "...", 0, QApplication::UnicodeUTF8));
        fireSpeciesLabel->setText(QApplication::translate("CSDesktop", "Species", 0, QApplication::UnicodeUTF8));
        fireSpeciesComboBox->clear();
        fireSpeciesComboBox->insertItems(0, QStringList()
         << QApplication::translate("CSDesktop", "human", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "mouse", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "rat", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "drosophila", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "yeast", 0, QApplication::UnicodeUTF8)
        );
        fireSpeciesInfoButton->setText(QString());
        fireSelectPeaksFolderInfoButton->setText(QString());
        fireSelectPeaksFolderToolButton->setText(QApplication::translate("CSDesktop", "...", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        fireSelectPeaksFolderLabel->setStatusTip(QString());
#endif // QT_NO_STATUSTIP
        fireSelectPeaksFolderLabel->setText(QApplication::translate("CSDesktop", "          OR folder:", 0, QApplication::UnicodeUTF8));
        fireRandomeModeLabel->setText(QApplication::translate("CSDesktop", "Random mode", 0, QApplication::UnicodeUTF8));
        fireRandomModeComboBox->clear();
        fireRandomModeComboBox->insertItems(0, QStringList()
         << QApplication::translate("CSDesktop", "random sequences with the same GC content", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "adjacent sequences to the peaks", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "1st order Markov model sequences learnt on peaks", 0, QApplication::UnicodeUTF8)
        );
        fireRandmodeInfoButton->setText(QString());
        fireSeedCheckBox->setText(QApplication::translate("CSDesktop", "Use the default seed for random regions ", 0, QApplication::UnicodeUTF8));
        fireSeedInfoButton->setText(QString());
        runFIREClearButton->setText(QApplication::translate("CSDesktop", "Clear all", 0, QApplication::UnicodeUTF8));
        runFIREButton->setText(QApplication::translate("CSDesktop", "Run", 0, QApplication::UnicodeUTF8));
        fireTabWidget->setTabText(fireTabWidget->indexOf(fireParametersTab), QApplication::translate("CSDesktop", "Parameters", 0, QApplication::UnicodeUTF8));
        stopRunFIREProcessButton->setText(QApplication::translate("CSDesktop", "Stop", 0, QApplication::UnicodeUTF8));
        fireTabWidget->setTabText(fireTabWidget->indexOf(fireProgressTab), QApplication::translate("CSDesktop", "Progress", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        findPathwayTitleLabel->setStatusTip(QApplication::translate("CSDesktop", "Find a specific pathway (apoptosis, GO:0006915) within the detected peaks", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        findPathwayTitleLabel->setText(QApplication::translate("CSDesktop", "Find pathway in peaks", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        findPathwaySelectPeaksLabel->setStatusTip(QString());
#endif // QT_NO_STATUSTIP
        findPathwaySelectPeaksLabel->setText(QApplication::translate("CSDesktop", "Select peak file:", 0, QApplication::UnicodeUTF8));
        findPathwaySelectPeaksInfoButton->setText(QString());
        findPathwayOutputFileLabel->setText(QApplication::translate("CSDesktop", "Output file", 0, QApplication::UnicodeUTF8));
        findPathwayOutputFileInfoButton->setText(QString());
        findPathwayWritePathwayInfoButton->setText(QString());
        findPathwaySelectDatabaseLabel->setText(QApplication::translate("CSDesktop", "Select pathways database", 0, QApplication::UnicodeUTF8));
        findPathwaySelectDatabaseInfoButton->setText(QString());
        findPathwaySelectDatabaseComboBox->clear();
        findPathwaySelectDatabaseComboBox->insertItems(0, QStringList()
         << QApplication::translate("CSDesktop", "Gene Ontology (GO)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "BioCarta", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "KEGG", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Human Protein Reference Database", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "SignatureDB", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Reactome", 0, QApplication::UnicodeUTF8)
        );
        findPathwaySpeciesLabel->setText(QApplication::translate("CSDesktop", "Species", 0, QApplication::UnicodeUTF8));
        findPathwaySpeciesComboBox->clear();
        findPathwaySpeciesComboBox->insertItems(0, QStringList()
         << QApplication::translate("CSDesktop", "Homo sapiens (hg19)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Homo sapiens (hg18)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Mus musculus (mm10)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Mus musculus (mm9)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Rattus norvegicus (rn4)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Saccharomyces cerevisiae (sacCer2)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Drosophila melanogaster (dm3)", 0, QApplication::UnicodeUTF8)
        );
        findPathwaySpeciesInfoButton->setText(QString());
        findPathwaySelectPathwayInfoButton->setText(QString());
        findPathwayWritePathwayRadioButton->setText(QApplication::translate("CSDesktop", "Write pathway", 0, QApplication::UnicodeUTF8));
        findPathwaySelectPathwayRadioButton->setText(QApplication::translate("CSDesktop", "Select pathway", 0, QApplication::UnicodeUTF8));
        findPathwaysPromotersCheckBox->setText(QApplication::translate("CSDesktop", "Promoters (TSS)", 0, QApplication::UnicodeUTF8));
        findPathwaySelectGenepartsLabel->setText(QApplication::translate("CSDesktop", "Select peaks in:", 0, QApplication::UnicodeUTF8));
        findPathwaysSelectGenepartsInfoButton->setText(QString());
        findPathwaySelectPeaksToolButton->setText(QApplication::translate("CSDesktop", "...", 0, QApplication::UnicodeUTF8));
        findPathwaysIntronsCheckBox->setText(QApplication::translate("CSDesktop", "Introns", 0, QApplication::UnicodeUTF8));
        findPathwaysIntron1CheckBox->setText(QApplication::translate("CSDesktop", "Intron 1", 0, QApplication::UnicodeUTF8));
        findPathwaysIntron2CheckBox->setText(QApplication::translate("CSDesktop", "Intron 2", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        findPathwaySuffixLabel->setStatusTip(QApplication::translate("CSDesktop", "Output files end in: .NM.txt and .SUM.txt", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        findPathwaySuffixLabel->setText(QApplication::translate("CSDesktop", "*.pathway.txt", 0, QApplication::UnicodeUTF8));
        findPathwaysDatabaseLabel->setText(QApplication::translate("CSDesktop", "Database", 0, QApplication::UnicodeUTF8));
        findPathwaysDatabaseComboBox->clear();
        findPathwaysDatabaseComboBox->insertItems(0, QStringList()
         << QApplication::translate("CSDesktop", "refSeq", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Ensembl", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "UCSCGenes", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "AceView", 0, QApplication::UnicodeUTF8)
        );
        findPathwaysDatabaseInfoButton->setText(QString());
        findPathwaysExonsCheckBox->setText(QApplication::translate("CSDesktop", "Exons", 0, QApplication::UnicodeUTF8));
        findPathwaysIntergenicCheckBox->setText(QApplication::translate("CSDesktop", "Intergenic (>50kb)", 0, QApplication::UnicodeUTF8));
        findPathwaysDownstreamCheckBox->setText(QApplication::translate("CSDesktop", "Downstream (TES)", 0, QApplication::UnicodeUTF8));
        findPathwaysDistalCheckBox->setText(QApplication::translate("CSDesktop", "Distal (>2kb <50kb)", 0, QApplication::UnicodeUTF8));
        runFindPathwayClearButton->setText(QApplication::translate("CSDesktop", "Clear all", 0, QApplication::UnicodeUTF8));
        runFindPathwayButton->setText(QApplication::translate("CSDesktop", "Run", 0, QApplication::UnicodeUTF8));
        findPathwayTabWidget->setTabText(findPathwayTabWidget->indexOf(findPathwayParametersTab), QApplication::translate("CSDesktop", "Parameters", 0, QApplication::UnicodeUTF8));
        stopRunFindPathwayProcessButton->setText(QApplication::translate("CSDesktop", "Stop", 0, QApplication::UnicodeUTF8));
        findPathwayTabWidget->setTabText(findPathwayTabWidget->indexOf(findPathwayProgressTab), QApplication::translate("CSDesktop", "Progress", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        pageTitleLabel->setStatusTip(QApplication::translate("CSDesktop", "Perform pathways analysis (iPAGE) for the genes found with peaks", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        pageTitleLabel->setText(QApplication::translate("CSDesktop", "iPAGE", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        pageSelectPeaksLabel->setStatusTip(QString());
#endif // QT_NO_STATUSTIP
        pageSelectPeaksLabel->setText(QApplication::translate("CSDesktop", "Select peak file:", 0, QApplication::UnicodeUTF8));
        pageSelectPeaksInfoButton->setText(QString());
        pageOutputFileLabel->setText(QApplication::translate("CSDesktop", "Output file", 0, QApplication::UnicodeUTF8));
        pageOutputFileInfoButton->setText(QString());
        pageSpeciesLabel->setText(QApplication::translate("CSDesktop", "Species", 0, QApplication::UnicodeUTF8));
        pageSpeciesComboBox->clear();
        pageSpeciesComboBox->insertItems(0, QStringList()
         << QApplication::translate("CSDesktop", "Homo sapiens (hg19)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Homo sapiens (hg18)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Mus musculus (mm10)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Mus musculus (mm9)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Rattus norvegicus (rn4)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Saccharomyces cerevisiae (sacCer2)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Drosophila melanogaster (dm3)", 0, QApplication::UnicodeUTF8)
        );
        pageDatabaseLabel->setText(QApplication::translate("CSDesktop", "Database", 0, QApplication::UnicodeUTF8));
        pageGenesDBComboBox->clear();
        pageGenesDBComboBox->insertItems(0, QStringList()
         << QApplication::translate("CSDesktop", "refSeq", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Ensembl", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "UCSCGenes", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "AceView", 0, QApplication::UnicodeUTF8)
        );
        pageSelectDatabaseLabel->setText(QApplication::translate("CSDesktop", "Select pathways database", 0, QApplication::UnicodeUTF8));
        pagePathwaysDBComboBox->clear();
        pagePathwaysDBComboBox->insertItems(0, QStringList()
         << QApplication::translate("CSDesktop", "Gene Ontology (GO)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "BioCarta", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "KEGG", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Human Protein Reference Database", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "SignatureDB", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "Reactome", 0, QApplication::UnicodeUTF8)
        );
        pageSelectGenepartsLabel->setText(QApplication::translate("CSDesktop", "Select peaks in:", 0, QApplication::UnicodeUTF8));
        pageSelectGenepartsInfoButton->setText(QString());
        pageSelectDatabaseInfoButton->setText(QString());
        pageSpeciesInfoButton->setText(QString());
        pageDatabaseInfoButton->setText(QString());
        pagePromotersCheckBox->setText(QApplication::translate("CSDesktop", "Promoters (TSS)", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        pageSuffixLabel->setStatusTip(QApplication::translate("CSDesktop", "Output files end in: .NM.txt and .SUM.txt", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        pageSuffixLabel->setText(QApplication::translate("CSDesktop", "_PAGE", 0, QApplication::UnicodeUTF8));
        pageSelectPeaksToolButton->setText(QApplication::translate("CSDesktop", "...", 0, QApplication::UnicodeUTF8));
        pageIntronsCheckBox->setText(QApplication::translate("CSDesktop", "Introns", 0, QApplication::UnicodeUTF8));
        pageIntron1CheckBox->setText(QApplication::translate("CSDesktop", "Intron 1", 0, QApplication::UnicodeUTF8));
        pageIntron2CheckBox->setText(QApplication::translate("CSDesktop", "Intron 2", 0, QApplication::UnicodeUTF8));
        pageExonsCheckBox->setText(QApplication::translate("CSDesktop", "Exons", 0, QApplication::UnicodeUTF8));
        pageIntergenicCheckBox->setText(QApplication::translate("CSDesktop", "Intergenic (>50kb)", 0, QApplication::UnicodeUTF8));
        pageDownstreamCheckBox->setText(QApplication::translate("CSDesktop", "Downstream (TES)", 0, QApplication::UnicodeUTF8));
        pageDistalCheckBox->setText(QApplication::translate("CSDesktop", "Distal (>2kb <50kb)", 0, QApplication::UnicodeUTF8));
        runPAGEClearButton->setText(QApplication::translate("CSDesktop", "Clear all", 0, QApplication::UnicodeUTF8));
        runPAGEButton->setText(QApplication::translate("CSDesktop", "Run", 0, QApplication::UnicodeUTF8));
        pageTabWidget->setTabText(pageTabWidget->indexOf(pageParametersTab), QApplication::translate("CSDesktop", "Parameters", 0, QApplication::UnicodeUTF8));
        stopRunPAGEProcessButton->setText(QApplication::translate("CSDesktop", "Stop", 0, QApplication::UnicodeUTF8));
        pageTabWidget->setTabText(pageTabWidget->indexOf(pageProgressTab), QApplication::translate("CSDesktop", "Progress", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        consTitleLabel->setStatusTip(QApplication::translate("CSDesktop", "Evaluate conservation of detected peaks", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        consTitleLabel->setText(QApplication::translate("CSDesktop", "Compute conservation scores", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        consSelectPeaksLabel->setStatusTip(QString());
#endif // QT_NO_STATUSTIP
        consSelectPeaksLabel->setText(QApplication::translate("CSDesktop", "Select peak file:", 0, QApplication::UnicodeUTF8));
        consSelectPeaksToolButton->setText(QApplication::translate("CSDesktop", "...", 0, QApplication::UnicodeUTF8));
        consSelectPeaksInfoButton->setText(QString());
        consOutputFileLabel->setText(QApplication::translate("CSDesktop", "Output file", 0, QApplication::UnicodeUTF8));
        consOutputFileInfoButton->setText(QString());
        consMethodLabel->setText(QApplication::translate("CSDesktop", "Method", 0, QApplication::UnicodeUTF8));
        consMethodComboBox->clear();
        consMethodComboBox->insertItems(0, QStringList()
         << QApplication::translate("CSDesktop", "phastCons", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "phyloP", 0, QApplication::UnicodeUTF8)
        );
        consMethodInfoButton->setText(QString());
        consCategoryInfoButton->setText(QString());
        consConsDirLabel->setText(QApplication::translate("CSDesktop", "Select cons. scores directory:", 0, QApplication::UnicodeUTF8));
        consConsDirToolButton->setText(QApplication::translate("CSDesktop", "...", 0, QApplication::UnicodeUTF8));
        consConsDirInfoButton->setText(QString());
        consFormatLabel->setText(QApplication::translate("CSDesktop", "Format", 0, QApplication::UnicodeUTF8));
        consFormatComboBox->clear();
        consFormatComboBox->insertItems(0, QStringList()
         << QApplication::translate("CSDesktop", "gzscores", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "scores", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "nucleosomes", 0, QApplication::UnicodeUTF8)
        );
        consFormatInfoButton->setText(QString());
        consOutputRandomLabel->setText(QApplication::translate("CSDesktop", "Output file (random regions)", 0, QApplication::UnicodeUTF8));
        consOutputRandomInfoButton->setText(QString());
        consShowProfilesCheckBox->setText(QApplication::translate("CSDesktop", "Show conservation profiles", 0, QApplication::UnicodeUTF8));
        consShowProfilesInfoButton->setText(QString());
        consRandistLabel->setText(QApplication::translate("CSDesktop", "Random distance", 0, QApplication::UnicodeUTF8));
        consRandistInfoButton->setText(QString());
        consWindowSizeInfoButton->setText(QString());
        consWindowSizeLabel->setText(QApplication::translate("CSDesktop", "Window size (bp)", 0, QApplication::UnicodeUTF8));
        consAroundSummitCheckBox->setText(QApplication::translate("CSDesktop", "Extract regions around peak summit ", 0, QApplication::UnicodeUTF8));
        consDistanceLabel->setText(QApplication::translate("CSDesktop", "Distance (bp)", 0, QApplication::UnicodeUTF8));
        consThresholdInfoButton->setText(QString());
        consThresholdLabel->setText(QApplication::translate("CSDesktop", "Score threshold", 0, QApplication::UnicodeUTF8));
        consSpeciesComboBox->clear();
        consSpeciesComboBox->insertItems(0, QStringList()
         << QApplication::translate("CSDesktop", "human (hg18)", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "mouse (mm9)", 0, QApplication::UnicodeUTF8)
        );
        consSpeciesLabel->setText(QApplication::translate("CSDesktop", "Species", 0, QApplication::UnicodeUTF8));
        consSpeciesInfoButton->setText(QString());
        consMakeRandInfoButton->setText(QString());
        consMakeRandCheckBox->setText(QApplication::translate("CSDesktop", "Make random regions", 0, QApplication::UnicodeUTF8));
        consCategoryComboBox->clear();
        consCategoryComboBox->insertItems(0, QStringList()
         << QApplication::translate("CSDesktop", "placental", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "primates", 0, QApplication::UnicodeUTF8)
        );
        consCategoryLabel->setText(QApplication::translate("CSDesktop", "Category", 0, QApplication::UnicodeUTF8));
        runConsClearButton->setText(QApplication::translate("CSDesktop", "Clear all", 0, QApplication::UnicodeUTF8));
        runConsButton->setText(QApplication::translate("CSDesktop", "Run", 0, QApplication::UnicodeUTF8));
        consTabWidget->setTabText(consTabWidget->indexOf(consParametersTab), QApplication::translate("CSDesktop", "Parameters", 0, QApplication::UnicodeUTF8));
        stopRunConsProcessButton->setText(QApplication::translate("CSDesktop", "Stop", 0, QApplication::UnicodeUTF8));
        consTabWidget->setTabText(consTabWidget->indexOf(consProgressTab), QApplication::translate("CSDesktop", "Progress", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        compPeaksTitleLabel->setStatusTip(QApplication::translate("CSDesktop", "Compare two lists of peaks", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        compPeaksTitleLabel->setText(QApplication::translate("CSDesktop", "Compare peaks", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        compPeaksSelectPeaks1Label->setStatusTip(QString());
#endif // QT_NO_STATUSTIP
        compPeaksSelectPeaks1Label->setText(QApplication::translate("CSDesktop", "Select peak file1:", 0, QApplication::UnicodeUTF8));
        compPeaksSelectPeaks1InfoButton->setText(QString());
#ifndef QT_NO_STATUSTIP
        compPeaksSelectPeaks2Label->setStatusTip(QString());
#endif // QT_NO_STATUSTIP
        compPeaksSelectPeaks2Label->setText(QApplication::translate("CSDesktop", "Select peak file2:", 0, QApplication::UnicodeUTF8));
        compPeaksPrefixFileLabel->setText(QApplication::translate("CSDesktop", "Prefix", 0, QApplication::UnicodeUTF8));
        compPeaksCenter1CheckBox->setText(QApplication::translate("CSDesktop", "Use peak center", 0, QApplication::UnicodeUTF8));
        compPeaksHeader1CheckBox->setText(QApplication::translate("CSDesktop", "Header", 0, QApplication::UnicodeUTF8));
        compPeaksID1CheckBox->setText(QApplication::translate("CSDesktop", "Column ID", 0, QApplication::UnicodeUTF8));
        compPeaksSelectPeaks1ToolButton->setText(QApplication::translate("CSDesktop", "...", 0, QApplication::UnicodeUTF8));
        compPeaksSelectPeaks2ToolButton->setText(QApplication::translate("CSDesktop", "...", 0, QApplication::UnicodeUTF8));
        compPeaksCenter2CheckBox->setText(QApplication::translate("CSDesktop", "Use peak center", 0, QApplication::UnicodeUTF8));
        compPeaksHeader2CheckBox->setText(QApplication::translate("CSDesktop", "Header", 0, QApplication::UnicodeUTF8));
        compPeaksID2CheckBox->setText(QApplication::translate("CSDesktop", "Column ID", 0, QApplication::UnicodeUTF8));
        compPeaksColumnHeaderCenter1InfoButton->setText(QString());
        compPeaksSelectPeaks2InfoButton->setText(QString());
        compPeaksColumnHeaderCenter2InfoButton->setText(QString());
        compPeaksPrefixInfoButton->setText(QString());
        compPeaksOvPeaksCheckBox->setText(QApplication::translate("CSDesktop", "Show overlapping peaks from file2", 0, QApplication::UnicodeUTF8));
        compPeaksUnPeaksCheckBox->setText(QApplication::translate("CSDesktop", "Show union of peaks from file1 and file2", 0, QApplication::UnicodeUTF8));
        compPeaksDescPeaks2CheckBox->setText(QApplication::translate("CSDesktop", "Show peaks description", 0, QApplication::UnicodeUTF8));
        compPeaksANDRadioButton->setText(QString());
        compPeaksANDNOTRadioButton->setText(QString());
#ifndef QT_NO_STATUSTIP
        compPeaksANDLabel->setStatusTip(QString());
#endif // QT_NO_STATUSTIP
        compPeaksANDLabel->setText(QApplication::translate("CSDesktop", "Peaks from file1 AND file2", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        compPeaksANDNOTLabel->setStatusTip(QString());
#endif // QT_NO_STATUSTIP
        compPeaksANDNOTLabel->setText(QApplication::translate("CSDesktop", "Peaks from file1 AND NOT file2", 0, QApplication::UnicodeUTF8));
        compPeaksDescPeaks1CheckBox->setText(QApplication::translate("CSDesktop", "Show peaks description", 0, QApplication::UnicodeUTF8));
        compPeaksIterationsLabel->setText(QApplication::translate("CSDesktop", "Iterations", 0, QApplication::UnicodeUTF8));
        compPeaksDesc1InfoButton->setText(QString());
        compPeaksDesc2InfoButton->setText(QString());
        compPeaksUnInfoButton->setText(QString());
        compPeaksOvInfoButton->setText(QString());
        compPeaksANDNOTInfoButton->setText(QString());
        compPeaksANDInfoButton->setText(QString());
        compPeaksIterationsInfoButton->setText(QString());
        compPeaksRandomModeInfoButton->setText(QString());
        compPeaksRandomModeLabel->setText(QApplication::translate("CSDesktop", "Random mode", 0, QApplication::UnicodeUTF8));
        compPeaksRandomModeComboBox->clear();
        compPeaksRandomModeComboBox->insertItems(0, QStringList()
         << QApplication::translate("CSDesktop", "random regions", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CSDesktop", "same genomic distribution with peaks", 0, QApplication::UnicodeUTF8)
        );
        runCompPeaksInverseButton->setText(QApplication::translate("CSDesktop", "Inverse peak files", 0, QApplication::UnicodeUTF8));
        runCompPeaksClearButton->setText(QApplication::translate("CSDesktop", "Clear all", 0, QApplication::UnicodeUTF8));
        runCompPeaksButton->setText(QApplication::translate("CSDesktop", "Run", 0, QApplication::UnicodeUTF8));
        compPeaksTabWidget->setTabText(compPeaksTabWidget->indexOf(compPeaksParametersTab), QApplication::translate("CSDesktop", "Parameters", 0, QApplication::UnicodeUTF8));
        stopRunCompPeaksProcessButton->setText(QApplication::translate("CSDesktop", "Stop", 0, QApplication::UnicodeUTF8));
        compPeaksTabWidget->setTabText(compPeaksTabWidget->indexOf(compPeaksProgressTab), QApplication::translate("CSDesktop", "Progress", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        compGenesTitleLabel->setStatusTip(QApplication::translate("CSDesktop", "Compare two lists of genes", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        compGenesTitleLabel->setText(QApplication::translate("CSDesktop", "Compare genes", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        compGenesSelectFile1Label->setStatusTip(QString());
#endif // QT_NO_STATUSTIP
        compGenesSelectFile1Label->setText(QApplication::translate("CSDesktop", "Select gene file1:", 0, QApplication::UnicodeUTF8));
        compGenesSelectFile1ToolButton->setText(QApplication::translate("CSDesktop", "...", 0, QApplication::UnicodeUTF8));
        compGenesSelectFile1InfoButton->setText(QString());
        compGenesSelectFile2InfoButton->setText(QString());
        compGenesRemoveDuplicatesInfoButton->setText(QString());
        compGenesOutputFileInfoButton->setText(QString());
        compGenesSelectFile2Label->setText(QApplication::translate("CSDesktop", "Select gene file2:", 0, QApplication::UnicodeUTF8));
        compGenesSelectFile2ToolButton->setText(QApplication::translate("CSDesktop", "...", 0, QApplication::UnicodeUTF8));
        compGenesOutputFileLabel->setText(QApplication::translate("CSDesktop", "Output directory", 0, QApplication::UnicodeUTF8));
        compGenesRemoveDuplicatesCheckBox->setText(QApplication::translate("CSDesktop", "Remove Duplicates", 0, QApplication::UnicodeUTF8));
        runCompGenesClearButton->setText(QApplication::translate("CSDesktop", "Clear all", 0, QApplication::UnicodeUTF8));
        runCompGenesButton->setText(QApplication::translate("CSDesktop", "Run", 0, QApplication::UnicodeUTF8));
        compGenesTabWidget->setTabText(compGenesTabWidget->indexOf(compGenesParametersTab), QApplication::translate("CSDesktop", "Parameters", 0, QApplication::UnicodeUTF8));
        stopRunCompGenesProcessButton->setText(QApplication::translate("CSDesktop", "Stop", 0, QApplication::UnicodeUTF8));
        compGenesTabWidget->setTabText(compGenesTabWidget->indexOf(compGenesProgressTab), QApplication::translate("CSDesktop", "Progress", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        jaccardTitleLabel->setToolTip(QString());
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        jaccardTitleLabel->setStatusTip(QApplication::translate("CSDesktop", "Compute Jaccard similarity coefficient for multiple peak files", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        jaccardTitleLabel->setWhatsThis(QString());
#endif // QT_NO_WHATSTHIS
        jaccardTitleLabel->setText(QApplication::translate("CSDesktop", "Compute similarity coefficient (Jaccard Index)", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_STATUSTIP
        jaccardSelectFileLabel->setStatusTip(QString());
#endif // QT_NO_STATUSTIP
        jaccardSelectFileLabel->setText(QApplication::translate("CSDesktop", "Select peaks file:", 0, QApplication::UnicodeUTF8));
        jaccardSelectFileToolButton->setText(QApplication::translate("CSDesktop", "...", 0, QApplication::UnicodeUTF8));
        jaccardSelectFileInfoButton->setText(QString());
        jaccardOutputFileInfoButton->setText(QString());
        jaccardOutputFileLabel->setText(QApplication::translate("CSDesktop", "Output directory", 0, QApplication::UnicodeUTF8));
        runJaccardClearButton->setText(QApplication::translate("CSDesktop", "Clear all", 0, QApplication::UnicodeUTF8));
        runJaccardButton->setText(QApplication::translate("CSDesktop", "Run", 0, QApplication::UnicodeUTF8));
        jaccardTabWidget->setTabText(jaccardTabWidget->indexOf(jaccardParametersTab), QApplication::translate("CSDesktop", "Parameters", 0, QApplication::UnicodeUTF8));
        stopRunJaccardProcessButton->setText(QApplication::translate("CSDesktop", "Stop", 0, QApplication::UnicodeUTF8));
        jaccardTabWidget->setTabText(jaccardTabWidget->indexOf(jaccardProgressTab), QApplication::translate("CSDesktop", "Progress", 0, QApplication::UnicodeUTF8));
        menuTools->setTitle(QApplication::translate("CSDesktop", "Tools", 0, QApplication::UnicodeUTF8));
        menuHelp->setTitle(QApplication::translate("CSDesktop", "Help", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class CSDesktop: public Ui_CSDesktop {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CSDESKTOP_H
