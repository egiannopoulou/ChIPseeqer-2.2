#ifndef COMMONPATHS_H
#define COMMONPATHS_H

#define ANNOTATE_PROGRAM        "../../../../dist/ChIPseeqerAnnotate"
#define CHIPSEEQER_PROGRAM      "../../../../dist/ChIPseeqer.bin"
#define COMPAREGENES_PROGRAM    "../../../../dist/CompareGenes.pl"
#define COMPAREINTERVALS_PROGRAM    "../../../../dist/ChIPseeqerCompareIntervals"
#define CONSPROCESS_PROGRAM     "../../../../dist/ChIPseeqerCons"
#define FINDMOTIF_PROGRAM       "../../../../dist/ChIPseeqerMotifMatch"
#define FINDPATHWAY_PROGRAM     "../../../../dist/ChIPseeqerPathwayMatch"
#define FIRE_PROGRAM            "../../../../dist/ChIPseeqerFIRE"
#define JACCARD_PROGRAM         "../../../../dist/ChIPseeqerComputeJaccardIndex"
#define NONGENICANNOTATE_PROGRAM    "../../../../dist/ChIPseeqerNongenicAnnotate"
#define RNAGENESANNOTATE_PROGRAM    "../../../../dist/ChIPseeqerRNAGenes"
#define PAGE_PROGRAM            "../../../../dist/ChIPseeqeriPAGE"
#define PEAKSTRACK_PROGRAM      "../../../../dist/ChIPseeqerPeaksTrack"
#define READSTRACK_PROGRAM      "../../../../dist/ChIPseeqerMakeReadDensityTrack"
#define SPLIT_SCRIPT            "../../../../dist/ChIPseeqerSplitReadFiles"
#define SUMMARY_PROGRAM         "../../../../dist/ChIPseeqerSummaryPromoters"

#define BULYK_MATRICES          "../../../../dist/DATA/BULYK_MATRICES"
#define JASPAR_MATRICES         "../../../../dist/DATA/JASPAR_MATRICES"

#define HG18_ENCODE_DATASETS         "../../../../dist/DATA/NONGENIC_ANNOTATION/Encode_hg18"
#define HG19_ENCODE_DATASETS         "../../../../dist/DATA/NONGENIC_ANNOTATION/Encode_hg19"

#define HUMAN_GO_PATHWAYS       "../../../../tools/PAGE/PAGE_DATA/ANNOTATIONS/human_go_orf/human_go_orf_names.txt"
#define HUMAN_BIOCARTA_PATHWAYS "../../../../tools/PAGE/PAGE_DATA/ANNOTATIONS/biocarta/biocarta_names.txt"
#define HUMAN_KEGG_PATHWAYS     "../../../../tools/PAGE/PAGE_DATA/ANNOTATIONS/kegg/kegg_names.txt"
#define HUMAN_HPRD_PATHWAYS     "../../../../tools/PAGE/PAGE_DATA/ANNOTATIONS/HPRD_interactions/HPRD_interactions_names.txt"
#define HUMAN_STAUDT_PATHWAYS   "../../../../tools/PAGE/PAGE_DATA/ANNOTATIONS/staudt_genesets/staudt_genesets_names.txt"
#define HUMAN_REACTOME_PATHWAYS "../../../../tools/PAGE/PAGE_DATA/ANNOTATIONS/reactome/reactome_names.txt"

#define DROSOPH_GO_PATHWAYS     "../../../../tools/PAGE/PAGE_DATA/ANNOTATIONS/human_go_orf/human_go_orf_names.txt"
#define MOUSE_GO_PATHWAYS       "../../../../tools/PAGE/PAGE_DATA/ANNOTATIONS/mouse_go/mouse_go_names.txt"
#define SACCER_GO_PATHWAYS      "../../../../tools/PAGE/PAGE_DATA/ANNOTATIONS/yeast_go/yeast_go_names.txt"

#define HG18_CHRDATA            "../../../../dist/DATA/hg18.chrdata"
#define HG19_CHRDATA            "../../../../dist/DATA/hg19.chrdata"
#define DM3_CHRDATA             "../../../../dist/DATA/dm3.chrdata"
#define MM9_CHRDATA             "../../../../dist/DATA/mm9.chrdata"
#define MM10_CHRDATA             "../../../../dist/DATA/mm10.chrdata"
#define RN4_CHRDATA             "../../../../dist/DATA/rn4.chrdata"
#define ZV9_CHRDATA             "../../../../dist/DATA/zv9.chrdata"
#define SACSER_CHRDATA          "../../../../dist/DATA/sacser.chrdata"

// #define HOME_ENV                "HOME=../../../"
// #define CHIPSEEQER_ENV          "CHIPSEEQERDIR=../../../../dist"
// #define MYSCANACE_ENV           "MYSCANACEDIR=../../../../tools/MYSCANACE"
// #define FIRE_ENV                "FIREDIR=../../../../tools/FIRE"
// #define PAGE_ENV                "PAGEDIR=../../../../tools/PAGE"
// #define PERLMODULES_ENV         "PERLMODULESDIR=../../../../tools/PERL_MODULES"
// #define PERLSCRIPTS_ENV         "PERLSCRIPTSDIR=../../../../tools/PERL_SCRIPTS"

#define PERL            "perl"

#endif // COMMONPATHS_H
