# -------------------------------------------------
# Project created by QtCreator 2010-03-17T17:59:49
# -------------------------------------------------
TARGET = ChIPseeqerGUI
TEMPLATE = app
SOURCES += main.cpp \
    csDesktop.cpp \
    formatsDialog.cpp \
    splitProcess.cpp \
    chipseeqerProcess.cpp \
    summaryProcess.cpp \
    peaksTrackProcess.cpp \
    readsTrackProcess.cpp \
    annotateProcess.cpp \
    findmotifProcess.cpp \
    consProcess.cpp \
    pieview.cpp \
    compareIntervalsProcess.cpp \
    compareGenesProcess.cpp \
    aboutDialog.cpp \
    fireProcess.cpp \
    computeJaccardProcess.cpp \
    findPathwayProcess.cpp \
    pageProcess.cpp \
    documentWidget.cpp
HEADERS += csDesktop.h \
    formatsDialog.h \
    splitProcess.h \
    chipseeqerProcess.h \
    summaryProcess.h \
    peaksTrackProcess.h \
    readsTrackProcess.h \
    annotateProcess.h \
    findmotifProcess.h \
    consProcess.h \
    pieview.h \
    compareIntervalsProcess.h \
    compareGenesProcess.h \
    aboutDialog.h \
    commonPaths.h \
    fireProcess.h \
    computeJaccardProcess.h \
    findPathwayProcess.h \
    pageProcess.h \
    documentWidget.h
FORMS += csDesktop.ui \
    formatsDialog.ui \
    aboutDialog.ui
RESOURCES += ChIPseeqerGUI.qrc
ICON = ChIPseeqerGUI.icns
# jenny -- addons 
INCLUDEPATH += /opt/local/include/poppler/qt4
INCLUDEPATH += /usr/include/poppler/qt4
LIBS += -L/opt/local/lib -lpoppler-qt4
CONFIG += x86_64
