/*
***************************************************************
*
*   Filename    :   findmotifProcess.cpp
*   Description :   This is the class that wraps the
*                   CSFindPeaksWithMotifsMatch process.
*   Version     :   1.0
*   Created     :   04/02/2010
*
***************************************************************
*
*   Compiler    :   gcc
*   IDE         :   Qt Creator 1.3.1
*
***************************************************************
*
*   Author      :   Eugenia G. Giannopoulou
*   Email       :   eug2002@med.cornell.edu
*
***************************************************************
*/

#include <QString>
#include "findPathwayProcess.h"
#include "commonPaths.h"

FindPathwayProcess::FindPathwayProcess()
{
}

QString FindPathwayProcess::getPeaksFile() {
    return this->peaksFile;
}

void FindPathwayProcess::setPeaksFile(QString peaks) {
    this->peaksFile = peaks;
}

QString FindPathwayProcess::getOutputFile() {
    return this->outputFile;
}

void FindPathwayProcess::setOutputFile(QString oFile) {
    this->outputFile = oFile;
}

QString FindPathwayProcess::getSpecies() {
    return this->species;
}

void FindPathwayProcess::setSpecies(QString sp) {
    this->species = sp;
}

QString FindPathwayProcess::getPathwaysDB() {
    return this->pathwaysDB;
}

void FindPathwayProcess::setPathwaysDB(QString pdb) {
    this->pathwaysDB = pdb;
}

QString FindPathwayProcess::getDatabase() {
    return this->database;
}

void FindPathwayProcess::setDatabase(QString db) {
    this->database = db;
}

QString FindPathwayProcess::getPathwayName() {
    return this->pathwayName;
}

void FindPathwayProcess::setPathwayName(QString pathway) {
    this->pathwayName = pathway;
}

QString FindPathwayProcess::getGeneparts() {
    return this->geneparts;
}

void FindPathwayProcess::setGeneparts(QString gp) {
    this->geneparts = gp;
}

bool FindPathwayProcess::getFromError() {
    return this->fromError;
}

void FindPathwayProcess::setFromError(bool err) {
    this->fromError = err;
}
