/********************************************************************************
** Form generated from reading UI file 'formatsDialog.ui'
**
** Created by: Qt User Interface Compiler version 4.8.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORMATSDIALOG_H
#define UI_FORMATSDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QTextEdit>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_FormatsDialog
{
public:
    QPushButton *pushButton;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QTextEdit *textEdit;

    void setupUi(QDialog *FormatsDialog)
    {
        if (FormatsDialog->objectName().isEmpty())
            FormatsDialog->setObjectName(QString::fromUtf8("FormatsDialog"));
        FormatsDialog->resize(629, 415);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/Icons/CS_logo_64.png"), QSize(), QIcon::Normal, QIcon::Off);
        FormatsDialog->setWindowIcon(icon);
        FormatsDialog->setSizeGripEnabled(false);
        pushButton = new QPushButton(FormatsDialog);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(510, 380, 113, 32));
        layoutWidget = new QWidget(FormatsDialog);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 10, 611, 361));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(layoutWidget);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setPointSize(13);
        font.setBold(false);
        font.setWeight(50);
        label->setFont(font);
        label->setScaledContents(true);
        label->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        verticalLayout->addWidget(label);

        textEdit = new QTextEdit(layoutWidget);
        textEdit->setObjectName(QString::fromUtf8("textEdit"));

        verticalLayout->addWidget(textEdit);


        retranslateUi(FormatsDialog);

        QMetaObject::connectSlotsByName(FormatsDialog);
    } // setupUi

    void retranslateUi(QDialog *FormatsDialog)
    {
        FormatsDialog->setWindowTitle(QApplication::translate("FormatsDialog", "ChIPseeqer Supported Formats", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("FormatsDialog", "OK", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("FormatsDialog", "Examples of the supported formats (bed, eland, extended eland, mit, sidow, sam, export):", 0, QApplication::UnicodeUTF8));
        textEdit->setHtml(QApplication::translate("FormatsDialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Lucida Grande'; font-size:13pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:9pt; font-weight:600;\">BED FORMAT (bed)</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">chr10    95452398    95452422    U0    0    +</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">chr10    88011870    88011894    U0    0    -</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-"
                        "left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">chr10    106079691   106079715   U0    0    -</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">chr10    43505655    43505679    U0    0    -</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:9pt; font-weight:600;\">ELAND FORMAT (eland)</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">73     AGAGACGATCACAAACATTAAAATCCAAAACAAAAT    U0    1    0    0    chr10    34054332    R</span></p>\n"
"<p style=\" marg"
                        "in-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">96     GCTAAAAAAATTAGTACGAAATAGTAGGAAGGTATT    U0    1    0    0    chr10    60810246    R</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">132    GCCCTAGTAGGGGAAAGAGAAACCAGTCGAGTTTTT    U0    1    0    0    chr10    16182814    F</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">135    GGCCAAACTCACGTTTTCTTTTTTTCTTTTCCAACA    U0    1    0    0    chr10    116275567   F</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-in"
                        "dent:0; text-indent:0px;\"><span style=\" font-size:9pt; font-weight:600;\">EXTENDED ELAND FORMAT (exteland)</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">&gt;HWUSI-EAS107:8:1:230:334#0/1	ACCAAAAACAAACAACTACATCAAATGTCTAAAGAA	1:0:0	chr12.fa:56030696F36</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">&gt;HWUSI-EAS107:8:1:230:257#0/1	AATTTGTCAAAATAGTGTTACTGAAAAAATATATAC	NM	-</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">&gt;HWUSI-EAS107:8:1:230:1610#0/1	ACATTGGGGGTCATCTTGGGGTCTCATTTAACAGAT	NM	-</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt"
                        ";\">&gt;HWUSI-EAS107:8:1:230:751#0/1	ATGCTACATTTATTGCCCATTCTAATCCAGTCTGAT	1:0:0	chr3.fa:175417739F36</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:9pt; font-weight:600;\">MIT FORMAT (mit)</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">chr1    57042343        57042379        +       203P4.5.23      0</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">chr1    27285992        27286028        -       203P4.5.31      0</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margi"
                        "n-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">chr1    162710401       162710437       -       203P4.5.33      0</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">chr1    54300993        54301029        -       203P4.5.39      0</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:9pt; font-weight:600;\">SIDOW FORMAT (sidow)</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">CACCGGAAACGGAAATTCACCGTCC 0 0 chr10:75160261 F</span></p>\n"
"<p style=\" margin-top:0px; margin-"
                        "bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">GCTCCACGCGAGGGTCCTGCCGGAA 0 0 chr10:16519152 F</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">CATTATTGTGACCATAAATTGTATT 0 0 chr10:81942940 R</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">GGTTACCCCGGGACAAATCCGGGGG 0 0 chr10:99195862 F</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:9pt; font-weight:600;\">SAM FORMAT (sam)</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0"
                        "px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">SRR030735.399990        16      Pf3D7_09        1163673 37      36M     =       1163673 0       GTATATATATATAATATATTTATTTTATATATGATA    @:IFI&gt;IIIIICIIIIIIIIIIIIIIIIIIIIIIHI    XT:A:U  NM:i:0  X0:i:1  X1:i:0  XM:i:0  XO:i:0  XG:i:0  MD:Z:36</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">SRR030735.399961        16      Pf3D7_09        12052   0       36M     =       12052   0       AACATAGGTCTTAACTTGACTAACATAGCTCTTAGT    IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII    XT:A:R  NM:i:0  X0:i:2  X1:i:1  XM:i:0  XO:i:0  XG:i:0  MD:Z:36</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">SRR030735.399937        16      Pf3D7_09        994204  37      36M     =       994204  0       "
                        "GTTATAAGTTAGCTTTCTTATATGTTGATGATAATA    &quot;%$&gt;)/3&quot;'%5,,&amp;/+-.88(?*3/HII8CIIII4I    XT:A:U  NM:i:1  X0:i:1  X1:i:0  XM:i:1  XO:i:0  XG:i:0  MD:Z:7T28</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">SRR030735.399934        16      Pf3D7_09        376817  37      36M     =       376817  0       TCTTTTATGTATATATTATACCAAAAGTAGTTATAT    CIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII    XT:A:U  NM:i:0  X0:i:1  X1:i:0  XM:i:0  XO:i:0  XG:i:0  MD:Z:36</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:9pt; font-weight:600;\">EXPORT FORMAT (export)</span><span style=\" font-size:8pt; font-weight:600;\"> </span></p>\n"
"<p s"
                        "tyle=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">&gt;HWI-EAS159	20A9RAAXX	7	1	15	247     AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA	WW\\WWWZLWWZZZZ^UZZ^Wa^W\\WWO\\\\W\\ZaZZZ	255:255:255	Y</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">&gt;HWI-EAS159	20A9RAAXX	7	1	16	103	AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGAAA	ZWZZa\\WWW\\WWZWZWWWZWZWWZLZWZWWaQHQWZ	255:255:255	Y</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">&gt;HWI-EAS159	20A9RAAXX	7	1	16	755	AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGAAAAA	ZWWLWZWW^aLWZW\\WWZLZWLWL\\ZWWOOLW^W\\L	255:255:255	Y</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" fon"
                        "t-size:8pt;\">&gt;HWI-EAS159	20A9RAAXX	7	1	17	953	AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGA    WWWWWZWZ^^^ZWaWZ\\WZZWWWZZ\\LZZWZWWZZW	255:255:255	Y</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p></body></html>", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class FormatsDialog: public Ui_FormatsDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORMATSDIALOG_H
