/*
***************************************************************
*
*   Filename    :   readsTrackProcess.h
*   Description :   This is the header of the class that wraps
*                   the ChIPsmoothedReads2Track process.
*   Version     :   1.0
*   Created     :   04/02/2010
*
***************************************************************
*
*   Compiler    :   gcc
*   IDE         :   Qt Creator 1.3.1
*
***************************************************************
*
*   Author      :   Eugenia G. Giannopoulou
*   Email       :   eug2002@med.cornell.edu
*
***************************************************************
*/

#ifndef READSTRACKPROCESS_H
#define READSTRACKPROCESS_H

#include <QProcess>
#include <QString>

namespace Ui {
    class ReadsTrackProcess;
}

class ReadsTrackProcess : public QProcess
{
public:
    ReadsTrackProcess();
    QString getReadsDir();
    void setReadsDir(QString reaks);
    QString getOutputFile();
    void setOutputFile(QString oFile);
    QString getTrackName();
    void setTrackName(QString name);
    QString getFormat();
    void setFormat(QString format);
    bool getUniqueReads();
    void setUniqueReads(bool t);
    bool getFromError();
    void setFromError(bool err);

private:
    QString readsDir;
    QString outputFile;
    QString trackName;
    QString format;
    bool    uniqueReads;
    bool    fromError;
};

#endif // READSTRACKPROCESS_H
