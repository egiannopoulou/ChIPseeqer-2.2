/*
***************************************************************
*
*   Filename    :   csDesktop.h
*   Description :   This is the header file for the central
*                   class for the ChIPseeqer GUI application.
*   Version     :   1.0
*   Created     :   04/02/2010
*
***************************************************************
*
*   Compiler    :   gcc
*   IDE         :   Qt Creator 1.3.1
*
***************************************************************
*
*   Author      :   Eugenia G. Giannopoulou
*   Email       :   eug2002@med.cornell.edu
*
***************************************************************
*/

#ifndef CSDESKTOP_H
#define CSDESKTOP_H

#include <QAbstractItemModel>
#include <QAbstractItemView>
#include <QEvent>
#include <QFile>
#include <QLabel>
#include <QMainWindow>
#include <QProcess>
#include <QProgressBar>
#include <QPushButton>
#include <QScrollArea>
#include <QSplitter>
#include <QTableWidget>
#include <QTextEdit>
#include <QWidget>

#include "annotateProcess.h"
#include "chipseeqerProcess.h"
#include "compareIntervalsProcess.h"
#include "compareGenesProcess.h"
#include "computeJaccardProcess.h"
#include "consProcess.h"
#include "documentWidget.h"
#include "findmotifProcess.h"
#include "findPathwayProcess.h"
#include "fireProcess.h"
#include "pageProcess.h"
#include "peaksTrackProcess.h"
#include "readsTrackProcess.h"
#include "splitProcess.h"
#include "summaryProcess.h"

#define WINDOW_TITLE    "ChIPseeqer v2.1"

namespace Ui {
    class CSDesktop;
}

//using namespace Ui;

class CSDesktop : public QMainWindow {
    Q_OBJECT
public:
    CSDesktop(QWidget *parent = 0); //creates a top-level window, i.e., no parent
    ~CSDesktop();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::CSDesktop           *ui;
    SplitProcess            splitChipProcess;
    SplitProcess            splitInputProcess;
    ChipseeqerProcess       chipseeqerProcess;
    SummaryProcess          summaryProcess;
    PeaksTrackProcess       peaksTrackProcess;
    ReadsTrackProcess       readsTrackProcess;
    AnnotateProcess         genomicDistProcess;
    AnnotateProcess         rnaGenesProcess;
    AnnotateProcess         encodeProcess;
    AnnotateProcess         repeatsProcess;
    AnnotateProcess         cpgIslandsProcess;
    AnnotateProcess         segDuplicatesProcess;
    FindmotifProcess        findmotifProcess;
    FireProcess             fireProcess;
    FindPathwayProcess      findPathwayProcess;
    PageProcess             pageProcess;
    ConsProcess             consProcess;
    CompareIntervalsProcess compPeaksProcess;
    CompareGenesProcess     compGenesProcess;
    ComputeJaccardProcess   computeJaccardProcess;

    //show peakDetResults tab and contents
    QWidget             *showPeakDetTab;
    QLabel              *peakDetStatsLabel;
    QTextEdit           *showPeakDetStatsTextEdit;
    QTableWidget        *peakDetectionTable;
    QPushButton         *peakDetectionCopyButton;

    //show genDistResults tab and contents
    QWidget             *showGenDistTab;
    QLabel              *genDistStatsLabel;
    QTextEdit           *showGenDistStatsTextEdit;
    QWidget             *showPieChartTab;
    QTableWidget        *genomicDistTable;
    QPushButton         *genomicDistCopyButton;

    //show genesSummaryResults tab and contents
    QWidget             *showGenSummTab;
    QLabel              *genSummStatsLabel;
    QTextEdit           *showGenSummStatsTextEdit;
    QTableWidget        *summaryTable;
    QPushButton         *summaryCopyButton;

    //show genesSummaryResults tab and contents
    QWidget             *showRnaGenesTab;
    QLabel              *rnaGenesStatsLabel;
    QTextEdit           *showRnaGenesStatsTextEdit;
    QTableWidget        *rnaGenesTable;
    QPushButton         *rnaGenesCopyButton;

    //show repeatsResults tab and contents
    QWidget             *showEncodeTab;
    QLabel              *encodeStatsLabel;
    QTextEdit           *showEncodeStatsTextEdit;
    QTableWidget        *encodeTable;
    QPushButton         *encodeCopyButton;

    //show repeatsResults tab and contents
    QWidget             *showRepeatsTab;
    QLabel              *repeatsStatsLabel;
    QTextEdit           *showRepeatsStatsTextEdit;
    QTableWidget        *repeatsTable;
    QPushButton         *repeatsCopyButton;

    //show cpgIslandsResults tab and contents
    QWidget             *showCpgIslandsTab;
    QLabel              *cpgIslandsStatsLabel;
    QTextEdit           *showCpgIslandsStatsTextEdit;
    QTableWidget        *cpgIslandsTable;
    QPushButton         *cpgIslandsCopyButton;

    //show segDupsResults tab and contents
    QWidget             *showSegDupsTab;
    QLabel              *segDupsStatsLabel;
    QTextEdit           *showSegDupsStatsTextEdit;
    QTableWidget        *segDuplicatesTable;
    QPushButton         *segDuplicatesCopyButton;

    //show FindMotifsResults tab and contents
    QWidget             *showFindMotifsTab;
    QLabel              *findMotifsStatsLabel;
    QTextEdit           *showFindMotifsStatsTextEdit;
    QTableWidget        *findMotifsTable;
    QPushButton         *findMotifsCopyButton;

    //show FireResults tab and contents
    QWidget             *showFireTab;
    QLabel              *fireStatsLabel;
    QTextEdit           *showFireStatsTextEdit;
    QTableWidget        *fireTable;
    QPushButton         *fireCopyButton;
    QScrollArea         *fireScrollArea;
    QLabel              *fireImageLabel;

    //show FindPathwaysResults tab and contents
    QWidget             *showFindPathwaysTab;
    QLabel              *findPathwaysStatsLabel;
    QTextEdit           *showFindPathwaysStatsTextEdit;
    QTableWidget        *findPathwaysTable;
    QPushButton         *findPathwaysCopyButton;

    //show PageResults tab and contents
    QWidget             *showPageTab;
    QLabel              *pageStatsLabel;
    QTextEdit           *showPageStatsTextEdit;
    QTableWidget        *pageTable;
    QPushButton         *pageCopyButton;
    QScrollArea         *pageScrollArea;
    QLabel              *pageImageLabel;

    //show ConservationResults tab and contents
    QWidget             *showConservationTab;
    QLabel              *conservationStatsLabel;
    QTextEdit           *showConservationStatsTextEdit;
    QTableWidget        *conservationTable;
    QPushButton         *conservationCopyButton;

    //show CompPeaksResults tab and contents
    QWidget             *showCompPeaksTab;
    QLabel              *compPeaksStatsLabel;
    QTextEdit           *showCompPeaksStatsTextEdit;
    QTableWidget        *compPeaksTable;
    QPushButton         *compPeaksCopyButton;

    //show CompGenesResults tab and contents
    QWidget             *showCompGenesTab;
    QLabel              *compGenesStatsLabel;
    QTextEdit           *showCompGenesStatsTextEdit;
    QTableWidget        *compGenesTable;
    QPushButton         *compGenesCopyButton;

    //show CompGenesResults tab and contents
    QWidget             *showCompJaccardTab;
    QLabel              *compJaccardStatsLabel;
    QTextEdit           *showCompJaccardStatsTextEdit;
    QTableWidget        *compJaccardTable;
    QPushButton         *compJaccardCopyButton;

    //progress bar
    QProgressBar        *progressBar;

    //test for the piechart
    QAbstractItemModel  *pieChartModel;
    QAbstractItemView   *pieChart;
    QItemSelectionModel *pieChartSelectionModel;
    QSplitter           *splitter;

    int i;

private slots:

    //related to encode
    void on_compPeaksIterationsInfoButton_clicked();
    void on_encodeALLInfoButton_clicked();
    void on_encodeALLCheckBox_clicked();
    void on_encodeIterationsInfoButton_clicked();
    void on_encodeWriteDatasetRadioButton_clicked();
    void on_encodeSelectDatasetRadioButton_clicked();
    void on_encodeSelectPeaksToolButton_clicked();
    void on_stopRunEncodeProcessButton_clicked();
    void on_runEncodeButton_clicked();
    void on_runEncodeClearButton_clicked();
    void on_encodeSelectDatasetInfoButton_clicked();
    void on_encodeWriteDatasetInfoButton_clicked();
    void on_encodePrefixFileInfoButton_clicked();
    void on_encodeSelectPeaksInfoButton_clicked();
    void on_encodeButton_clicked();
    void encodeTableOpenBrowser(int row);

    //related to page
    void on_stopRunPAGEProcessButton_clicked();
    void on_pageSelectGenepartsInfoButton_clicked();
    void on_pageSelectPeaksToolButton_clicked();
    void on_runPAGEClearButton_clicked();
    void on_pageSelectDatabaseInfoButton_clicked();
    void on_pageDatabaseInfoButton_clicked();
    void on_pageSpeciesInfoButton_clicked();
    void on_pageOutputFileInfoButton_clicked();
    void on_pageSelectPeaksInfoButton_clicked();
    void on_pageButton_clicked();
    void on_runPAGEButton_clicked();

    //related to findpathway
    void on_findPathwaysSelectGenepartsInfoButton_clicked();
    void on_findPathwaySelectPeaksToolButton_clicked();
    void on_stopRunFindPathwayProcessButton_clicked();
    void on_runFindPathwayButton_clicked();
    void on_runFindPathwayClearButton_clicked();
    void on_findPathwaySelectPathwayRadioButton_clicked();
    void on_findPathwayWritePathwayRadioButton_clicked();
    void on_findPathwaySelectPathwayInfoButton_clicked();
    void on_findPathwayWritePathwayInfoButton_clicked();
    void on_findPathwaySelectDatabaseInfoButton_clicked();
    void on_findPathwaySpeciesInfoButton_clicked();
    void on_findPathwayOutputFileInfoButton_clicked();
    void on_findPathwaySelectPeaksInfoButton_clicked();
    void on_findPathwayButton_clicked();
    void findPathwaysTableOpenBrowser(int row);

    //related to Jaccard Similarity Coefficient
    void on_jaccardSelectFileToolButton_clicked();
    void on_stopRunJaccardProcessButton_clicked();
    void on_runJaccardButton_clicked();
    void on_runJaccardClearButton_clicked();
    void on_jaccardSelectFileInfoButton_clicked();
    void on_jaccardOutputFileInfoButton_clicked();
    void on_jaccardButton_clicked();

    //related to rnaGenes
    void on_rnaGenesSelectPeaksToolButton_clicked();
    void on_rnaGenesSelectPeaksInfoButton_clicked();
    void on_rnaGenesOutputFileInfoButton_clicked();
    void on_stopRunFindRNAGenesProcessButton_clicked();
    void on_runFindRNAGenesButton_clicked();
    void on_runFindRNAGenesClearButton_clicked();
    void on_rnaGenesButton_clicked();
    void rnaGenesTableOpenBrowser(int row);

    //related to FIRE
    void on_stopRunFIREProcessButton_clicked();
    void on_runFIREButton_clicked();
    void on_fireRandmodeInfoButton_clicked();
    void on_fireSeedInfoButton_clicked();
    void on_fireSpeciesInfoButton_clicked();
    void on_fireGenomeInfoButton_clicked();
    void on_fireSelectPeaksFolderInfoButton_clicked();
    void on_fireSelectPeaksInfoButton_clicked();
    void on_runFIREClearButton_clicked();
    void on_fireGenomeToolButton_clicked();
    void on_fireSelectPeaksFolderToolButton_clicked();
    void on_fireSelectPeaksToolButton_clicked();

    //related to compare genes    
    void on_runCompGenesClearButton_clicked();
    void on_stopRunCompGenesProcessButton_clicked();
    void on_runCompGenesButton_clicked();
    void on_compGenesRemoveDuplicatesInfoButton_clicked();
    void on_compGenesOutputFileInfoButton_clicked();
    void on_compGenesSelectFile2InfoButton_clicked();
    void on_compGenesSelectFile1InfoButton_clicked();
    void on_compGenesSelectFile2ToolButton_clicked();
    void on_compGenesSelectFile1ToolButton_clicked();
    void compGenesTableOpenBrowser(int row);

    //related to compare peaks
    void on_runCompPeaksInverseButton_clicked();
    void on_compPeaksDesc2InfoButton_clicked();
    void on_compPeaksANDNOTInfoButton_clicked();
    void on_compPeaksDesc1InfoButton_clicked();
    void on_compPeaksUnInfoButton_clicked();
    void on_compPeaksOvInfoButton_clicked();
    void on_compPeaksANDInfoButton_clicked();
    void on_compPeaksPrefixInfoButton_clicked();
    void on_compPeaksColumnHeaderCenter2InfoButton_clicked();
    void on_compPeaksSelectPeaks2InfoButton_clicked();
    void on_compPeaksColumnHeaderCenter1InfoButton_clicked();
    void on_compPeaksSelectPeaks1InfoButton_clicked();
    void on_compPeaksCenter2CheckBox_clicked();
    void on_compPeaksCenter1CheckBox_clicked();
    void on_compPeaksHeader2CheckBox_clicked();
    void on_compPeaksHeader1CheckBox_clicked();
    void on_compPeaksID2CheckBox_clicked();
    void on_compPeaksID1CheckBox_clicked();
    void on_compPeaksDescPeaks2CheckBox_clicked();
    void on_compPeaksDescPeaks1CheckBox_clicked();
    void on_compPeaksUnPeaksCheckBox_clicked();
    void on_compPeaksOvPeaksCheckBox_clicked();
    void on_compPeaksANDNOTRadioButton_clicked();
    void on_compPeaksANDRadioButton_clicked();
    void on_runCompPeaksClearButton_clicked();
    void on_compPeaksSelectPeaks2ToolButton_clicked();
    void on_compPeaksSelectPeaks1ToolButton_clicked();
    void on_stopRunCompPeaksProcessButton_clicked();
    void on_runCompPeaksButton_clicked();
    void compPeaksTableOpenBrowser(int row);

    //related to cons
    void on_consThresholdInfoButton_clicked();
    void on_consAroundSummitCheckBox_clicked();
    void on_consWindowSizeInfoButton_clicked();
    void on_consShowProfilesCheckBox_clicked();
    void on_consRandistInfoButton_clicked();
    void on_consShowProfilesInfoButton_clicked();
    void on_consMakeRandInfoButton_clicked();
    void on_consMethodInfoButton_clicked();
    void on_consOutputRandomInfoButton_clicked();
    void on_consMakeRandCheckBox_clicked();
    void on_consCategoryInfoButton_clicked();
    void on_stopRunConsProcessButton_clicked();
    void on_runConsClearButton_clicked();
    void on_consFormatInfoButton_clicked();
    void on_consConsDirInfoButton_clicked();
    void on_consOutputFileInfoButton_clicked();
    void on_consSelectPeaksInfoButton_clicked();
    void on_consConsDirToolButton_clicked();
    void on_consSelectPeaksToolButton_clicked();
    void on_runConsButton_clicked();
    void conservationTableOpenBrowser(int row);

    //related to findmotif
    void on_findmotifSequenceInfoButton_clicked();
    void on_findmotifNameInfoButton_clicked();
    void on_findmotifSequenceRadioButton_clicked();
    void on_findmotifNameRadioButton_clicked();
    void on_stopRunFindmotifProcessButton_clicked();
    void on_findmotifScoreInfoButton_clicked();
    void on_findmotifGenomeInfoButton_clicked();
    void on_findmotifOutputFileInfoButton_clicked();
    void on_findmotifSelectPeaksInfoButton_clicked();
    void on_findmotifJasparRadioButton_clicked();
    void on_findmotifBulykRadioButton_clicked();
    void on_findmotifGenomeToolButton_clicked();
    void on_runFindmotifButton_clicked();
    void on_findmotifSelectPeaksToolButton_clicked();
    void on_runFindmotifClearButton_clicked();
    void findMotifsTableOpenBrowser(int row);

    //related to seg. duplicates process
    void on_segDuplicatesOutputFileInfoButton_clicked();
    void on_segDuplicatesSelectPeaksInfoButton_clicked();
    void on_segDuplicatesSelectPeaksToolButton_clicked();
    void on_runFindSegDuplicatesButton_clicked();
    void on_runFindSegDuplicatesClearButton_clicked();
    void on_stopRunFindSegDuplicatesProcessButton_clicked();
    void segDuplicatesTableOpenBrowser(int row);

    //related to cpg islands process
    void on_cpgIslandsOutputFileInfoButton_clicked();
    void on_cpgIslandsSelectPeaksInfoButton_clicked();
    void on_cpgIslandsSelectPeaksToolButton_clicked();
    void on_runFindCpgIslandsButton_clicked();
    void on_runFindCpgIslandsClearButton_clicked();
    void on_stopRunFindCpgIslandsProcessButton_clicked();
    void cpgIslandsTableOpenBrowser(int row);

    //related to repeats process
    void on_repeatsOutputFileInfoButton_clicked();
    void on_repeatsSelectPeaksInfoButton_clicked();    
    void on_repeatsSelectPeaksToolButton_clicked();
    void on_runFindRepeatsButton_clicked();
    void on_runFindRepeatsClearButton_clicked();
    void on_stopRunFindRepeatsProcessButton_clicked();
    void repeatsTableOpenBrowser(int row);

    //related to genomic distribution process
    void on_genomicDistSpeciesInfoButton_clicked();
    void on_genomicDistTypeInfoButton_clicked();
    void on_genomicDistLenDWInfoButton_clicked();
    void on_genomicDistLenPInfoButton_clicked();
    void on_genomicDistDatabaseInfoButton_clicked();
    void on_genomicDistOutputFileInfoButton_clicked();
    void on_genomicDistSelectPeaksInfoButton_clicked();
    void on_genomicDistSelectPeaksToolButton_clicked();
    void on_genomicDistMindistawayInfoButton_clicked();
    void on_runGenomicDistClearButton_clicked();
    void on_runGenomicDistButton_clicked();
    void on_stopRunGenomicDistProcessButton_clicked();
    void genomicDistTableOpenBrowser(int row);

    //related to create tracks process
    void on_readsTrackSelectReadsToolButton_clicked();
    void on_createReadsTrackButton_clicked();
    void on_readsTrackUniqueInfoButton_clicked();
    void on_readsTrackOutputFileInfoButton_clicked();
    void on_peaksTrackOutputFileInfoButton_clicked();
    void on_readsTrackNameInfoButton_clicked();
    void on_peaksTrackNameInfoButton_clicked();
    void on_readsTrackSelectReadsInfoButton_clicked();
    void on_peaksTrackSelectPeaksInfoButton_clicked();
    void on_readsTrackClearButton_clicked();
    void on_openGenomeBrowserButton_clicked();
    void on_createPeaksTrackButton_clicked();
    void on_peaksTrackClearButton_clicked();
    void on_peaksTrackSelectPeaksToolButton_clicked();
    void on_stopCreateTracksProcessButton_clicked();

    //related to genes summary process
    void on_genesSummarySpeciesInfoButton_clicked();
    void on_stopRunSummaryProcessButton_clicked();
    void on_runSummaryClearButton_clicked();
    void on_peakDetectionClearButton_clicked();
    void on_genesSummarySelectPeaksToolButton_clicked();
    void on_runSummaryButton_clicked();
    void on_genesSummaryOutputFileInfoButton_clicked();
    void on_genesSummarySelectPeaksInfoButton_clicked();
    void on_genesSummaryLenDInfoButton_clicked();
    void on_genesSummaryLenUInfoButton_clicked();
    void on_genesSummaryDatabaseInfoButton_clicked();
    void summaryTableOpenBrowser(int row);

    //related to run CS process
    void on_runPeakDetectionInverseButton_clicked();
    void on_peakDetectionMinPeakHeightInfoButton_clicked();
    void on_peakDetectionSpeciesInfoButton_clicked();
    void on_stopRunCSProcessButton_clicked();
    void on_peakDetectionOutputFileInfoButton_clicked();
    void on_peakDetectionUniqueInfoButton_clicked();
    void on_peakDetectionMinDistInfoButton_clicked();
    void on_peakDetectionMinLenInfoButton_clicked();
    void on_peakDetectionFragLenInfoButton_clicked();
    void on_peakDetectionThresholdInfoButton_clicked();
    void on_peakDetectionFoldInfoButton_clicked();
    void on_peakDetectionFormatInfoButton_clicked();
    void on_runCSButton_clicked();
    void on_selectInputFolderToolButton_clicked();
    void on_selectChipFolderToolButton_clicked();
    void on_selectInputFolderInfoButton_clicked();
    void on_selectChipFolderInfoButton_clicked();
    void peakDetectionTableOpenBrowser(int row);

    //related to split process
    void on_loadClearButton_clicked();
    void on_loadFormatInfoButton_clicked();
    void on_selectInputRawFolderToolButton_clicked();
    void on_selectChipRawFolderToolButton_clicked();
    void on_stopSplitProcessButton_clicked();
    void on_splitButton_clicked();

    void on_loadRawDataButton_clicked();
    void on_genesSummaryButton_clicked();
    void on_peakDetectionButton_clicked();
    void on_createTracksButton_clicked();
    void on_genomicDistButton_clicked();
    void on_segDuplicatesButton_clicked();
    void on_cpgIslandsButton_clicked();
    void on_repeatsButton_clicked();
    void on_findmotifButton_clicked();
    void on_fireButton_clicked();
    void on_consButton_clicked();
    void on_compareGenesButton_clicked();
    void on_comparePeaksButton_clicked();

    void setLoadRawButtonBold(bool bold);
    void setPeakDetectionButtonBold(bool bold);
    void setGenesSummaryButtonBold(bool bold);
    void setCreateTracksButtonBold(bool bold);
    void setGenomicDistButtonBold(bool bold);
    void setRNAGenesButtonBold(bool bold);
    void setEncodeButtonBold(bool bold);
    void setRepeatsButtonBold(bool bold);
    void setCpgIslandsButtonBold(bool bold);
    void setSegDuplicatesButtonBold(bool bold);
    void setFindMotifButtonBold(bool bold);
    void setFIREButtonBold(bool bold);
    void setFindPathwayButtonBold(bool bold);
    void setPAGEButtonBold(bool bold);
    void setConsButtonBold(bool bold);
    void setCompPeaksButtonBold(bool bold);
    void setCompGenesButtonBold(bool bold);
    void setJaccardButtonBold(bool bold);

    void peakDetectionTableCopy();
    void genomicDistTableCopy();
    void summaryTableCopy();
    void rnaGenesTableCopy();
    void encodeTableCopy();
    void repeatsTableCopy();
    void cpgIslandsTableCopy();
    void segDuplicatesTableCopy();
    void findMotifsTableCopy();
    void fireTableCopy();
    void findPathwaysTableCopy();
    void pageTableCopy();
    void conservationTableCopy();
    void compPeaksTableCopy();
    void compGenesTableCopy();
    void compJaccardTableCopy();

    void setDefaultIndices();
    void makePeakDetResultsTab();
    void makeGenDistResultsTab();
    void makeGenSummResultsTab();
    void makeRnaGenesResultsTab();
    void makeEncodeResultsTab();
    void makeRepeatsResultsTab();
    void makeCpgIslandsResultsTab();
    void makeSegDuplicatesResultsTab();
    void makeFindMotifsResultsTab();
    void makeFireResultsTab();
    void makeFindPathwaysResultsTab();
    void makePageResultsTab();
    void makeConsResultsTab();
    void makeCompPeaksResultsTab();
    void makeCompGenesResultsTab();
    void makeCompJaccardResultsTab();
    void findAvailableMotifs();
    void findAvailablePathways();
    void findAvailableEncodeDatasets(QString gnm);

    void readOutputFromSplitChipProcess();
    void readErrorFromSplitChipProcess();
    void readOutputFromSplitInputProcess();
    void readErrorFromSplitInputProcess();
    void readOutputFromChipseeqerProcess();
    void readErrorFromChipseeqerProcess();
    void readOutputFromSummaryProcess();
    void readErrorFromSummaryProcess();
    void readOutputFromPeaksTrackProcess();
    void readErrorFromPeaksTrackProcess();
    void readOutputFromReadsTrackProcess();
    void readErrorFromReadsTrackProcess();
    void readOutputFromGenomicDistProcess();
    void readErrorFromGenomicDistProcess();
    void readOutputFromFindRnaGenesProcess();
    void readErrorFromFindRnaGenesProcess();
    void readOutputFromEncodeProcess();
    void readErrorFromEncodeProcess();
    void readOutputFromRepeatsProcess();
    void readErrorFromRepeatsProcess();
    void readOutputFromCpgIslandsProcess();
    void readErrorFromCpgIslandsProcess();
    void readOutputFromSegDuplicatesProcess();
    void readErrorFromSegDuplicatesProcess();
    void readOutputFromFindMotifProcess();
    void readErrorFromFindMotifProcess();
    void readOutputFromFIREProcess();
    void readErrorFromFIREProcess();
    void readOutputFromFindPathwayProcess();
    void readErrorFromFindPathwayProcess();
    void readOutputFromPAGEProcess();
    void readErrorFromPAGEProcess();
    void readOutputFromConsProcess();
    void readErrorFromConsProcess();
    void readOutputFromCompPeaksProcess();
    void readErrorFromCompPeaksProcess();
    void readOutputFromCompGenesProcess();
    void readErrorFromCompGenesProcess();
    void readOutputFromJaccardProcess();
    void readErrorFromJaccardProcess();

    void onSplitChipProcessFinished();
    void onSplitInputProcessFinished();
    void onChipseeqerProcessFinished();
    void onSummaryProcessFinished();
    void onPeaksTrackProcessFinished();
    void onReadsTrackProcessFinished();
    void onGenomicDistProcessFinished();
    void onRepeatsProcessFinished();
    void onRnaGenesProcessFinished();
    void onEncodeProcessFinished();
    void onCpgIslandsProcessFinished();
    void onSegDuplicatesProcessFinished();
    void onFindMotifProcessFinished();
    void onFIREProcessFinished();
    void onFindPathwayProcessFinished();
    void onPAGEProcessFinished();
    void onConsProcessFinished();
    void onCompPeaksProcessFinished();
    void onCompGenesProcessFinished();
    void onJaccardProcessFinished();

    void on_aboutAction_triggered();
    void on_loadDataAction_triggered();
    void on_peakDetectionAction_2_triggered();
    void on_genesSummaryAction_triggered();
    void on_genomicDistributionAction_triggered();
    void on_createTracksAction_triggered();
    void on_repMaskerAction_triggered();
    void on_cpgIslandsAction_triggered();
    void on_segDupsAction_triggered();
    void on_findMotifMenu_triggered();
    void on_comparePeaksMenu_triggered();
    void on_compareGenesMenu_triggered();

    void checkGenesSummarySpeciesComboBox();
    void checkGenomDistSpeciesComboBox();
    void checkEncodeSpeciesComboBox();
    void changeFindMotifSuffixBComboBox();
    void changeFindMotifSuffixJComboBox();
    void checkFindPathwaySpeciesComboBox();
    void fillInPathwaysComboBox();
    void checkPageSpeciesComboBox();
    void on_compPeaksRandomModeInfoButton_clicked();
    void on_consSpeciesInfoButton_clicked();
};

#endif // CSDESKTOP_H
