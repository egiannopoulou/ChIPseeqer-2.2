/*
***************************************************************
*
*   Filename    :   documentWidget.cpp
*   Description :   This is the class describes a widget
*                   used to handle pdf documents
*   Version     :   1.0
*   Created     :   02/11/2010
*
***************************************************************
*
*   Compiler    :   gcc
*   IDE         :   Qt Creator 1.3.1
*
***************************************************************
*
*   Author      :   Eugenia G. Giannopoulou
*   Email       :   eug2002@med.cornell.edu
*
***************************************************************
*/

#include "documentWidget.h"

DocumentWidget::DocumentWidget(QWidget *parent): QWidget(parent)

{
}
