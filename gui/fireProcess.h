/*
***************************************************************
*
*   Filename    :   fireProcess.h
*   Description :   This is the header file for the class that
*                   wraps the CSFIRE process.
*   Version     :   1.0
*   Created     :   10/14/2010
*
***************************************************************
*
*   Compiler    :   gcc
*   IDE         :   Qt Creator 1.3.1
*
***************************************************************
*
*   Author      :   Eugenia G. Giannopoulou
*   Email       :   eug2002@med.cornell.edu
*
***************************************************************
*/

#ifndef FIREPROCESS_H
#define FIREPROCESS_H

#include <QProcess>
#include <QString>

namespace Ui {
    class FireProcess;
}

class FireProcess : public QProcess
{
public:
    FireProcess();

    QString getPeaksFile();
    void setPeaksFile(QString peaks);
    QString getPeaksFolder();
    void setPeaksFolder(QString folder);
    QString getOutputFolder();
    void setOutputFolder(QString oFolder);
    QString getGenome();
    void setGenome(QString g);
    QString getSpecies();
    void setSpecies(QString sp);
    QString getRandmode();
    void setRandmode(QString rm);
    bool getSeed();
    void setSeed(bool s);
    bool getIsFile();
    void setIsFile(bool isf);
    bool getFromError();
    void setFromError(bool err);

private:
    QString peaksFile;
    QString peaksFolder;
    QString oFolder;
    QString genome;
    QString species;
    QString randmode;
    bool    seed;
    bool    isFile;
    bool    fromError;
};

#endif // FIREPROCESS_H
