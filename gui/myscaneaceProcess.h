#ifndef MYSCANEACEPROCESS_H
#define MYSCANEACEPROCESS_H
#define MYSCANACE_PROGRAM "../../../ChIPseeqer/ChIPseeqerFindPeaksWithMotifMatch"


class MyscaneaceProcess
{
public:
    MyscaneaceProcess();
};

#endif // MYSCANEACEPROCESS_H



#ifndef SUMMARYPROCESS_H
#define SUMMARYPROCESS_H
#define SUMMARY_PROGRAM "../../../ChIPseeqer/ChIPseeqerSummary"

#include <QProcess>
#include <QString>

namespace Ui {
    class SummaryProcess;
}

class SummaryProcess : public QProcess
{
public:
    SummaryProcess();
    QString getPeaksFile();
    void setPeaksFile(QString peaks);
    QString getOutputFile();
    void setOutputFile(QString oFile);
    QString getDatabase();
    void setDatabase(QString db);
    int getLenU();
    void setLenU(int lu);
    int getLenD();
    void setLenD(int ld);
    bool getFromError();
    void setFromError(bool err);

private:
    QString peaksFile;
    QString outputFile;
    QString database;
    int     lenU;
    int     lenD;
    bool    fromError;
};

#endif // SUMMARYPROCESS_H

