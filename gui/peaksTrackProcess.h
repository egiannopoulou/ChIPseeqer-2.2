/*
***************************************************************
*
*   Filename    :   peaksTrackProcess.h
*   Description :   This is the header of the class that wraps
*                   the CS2Track process.
*   Version     :   1.0
*   Created     :   04/02/2010
*
***************************************************************
*
*   Compiler    :   gcc
*   IDE         :   Qt Creator 1.3.1
*
***************************************************************
*
*   Author      :   Eugenia G. Giannopoulou
*   Email       :   eug2002@med.cornell.edu
*
***************************************************************
*/

#ifndef PEAKSTRACKPROCESS_H
#define PEAKSTRACKPROCESS_H

#include <QProcess>
#include <QString>

namespace Ui {
    class PeaksTrackProcess;
}

class PeaksTrackProcess : public QProcess
{
public:
    PeaksTrackProcess();
    QString getPeaksFile();
    void setPeaksFile(QString peaks);
    QString getOutputFile();
    void setOutputFile(QString oFile);
    QString getTrackName();
    void setTrackName(QString name);
    bool getFromError();
    void setFromError(bool err);

private:
    QString peaksFile;
    QString outputFile;
    QString trackName;
    bool    fromError;
};

#endif // PEAKSTRACKPROCESS_H
