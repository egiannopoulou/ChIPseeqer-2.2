/*
***************************************************************
*
*   Filename    :   main.cpp
*   Description :   This is the main function of the project.
*   Version     :   1.0
*   Created     :   04/02/2010
*
***************************************************************
*
*   Compiler    :   gcc
*   IDE         :   Qt Creator 1.3.1
*
***************************************************************
*
*   Author      :   Eugenia G. Giannopoulou
*   Email       :   eug2002@med.cornell.edu
*
***************************************************************
*/

#include <QApplication>
#include "csDesktop.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    CSDesktop w;
    w.show();
    return a.exec();
}
