/*
***************************************************************
*
*   Filename    :   csDesktop.cpp
*   Description :   This is the central class for the ChIPseeqer
                    GUI application.
*   Version     :   1.0
*   Created     :   04/02/2010
*
***************************************************************
*
*   Compiler    :   gcc
*   IDE         :   Qt Creator 1.3.1
*
***************************************************************
*
*   Author      :   Eugenia G. Giannopoulou
*   Email       :   eug2002@med.cornell.edu
*
***************************************************************
*/

#include <QClipboard>
#include <QColorGroup>
#include <QDesktopServices>
#include <QEvent>
#include <QFile>
#include <QFileDialog>
#include <QLayoutIterator>
#include <QMainWindow>
#include <QMessageBox>
#include <QPalette>
#include <QProgressBar>
#include <QStandardItemModel>
#include <QStringList>
#include <QSplitter>
#include <QTextIStream>
#include <QUrl>
#include <QWhatsThis>
#include <QWidget>

#include "aboutDialog.h"
#include "commonPaths.h"
#include "csDesktop.h"
#include "formatsDialog.h"
#include "pieview.h"
#include "ui_csDesktop.h"

#include <stdio.h>
#include <string>
#include <sstream>
#include <iostream>

#include <dirent.h>
#include <stddef.h>
#include <sys/types.h>
#include <poppler-qt4.h>

using namespace std;

CSDesktop::CSDesktop(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::CSDesktop),
    splitChipProcess(),
    splitInputProcess(),
    chipseeqerProcess(),
    summaryProcess(),
    peaksTrackProcess(),
    readsTrackProcess(),
    genomicDistProcess(),
    rnaGenesProcess(),
    encodeProcess(),
    repeatsProcess(),
    cpgIslandsProcess(),
    segDuplicatesProcess(),
    findmotifProcess(),
    fireProcess(),
    findPathwayProcess(),
    pageProcess(),
    consProcess(),
    compPeaksProcess(),
    compGenesProcess(),
    computeJaccardProcess()
{
    ui->setupUi(this);
    this->setWindowTitle(WINDOW_TITLE);
    this->setFixedSize(size());

    setDefaultIndices();
    makePeakDetResultsTab();
    makeGenDistResultsTab();
    makeGenSummResultsTab();
    makeRnaGenesResultsTab();
    makeEncodeResultsTab();
    makeRepeatsResultsTab();
    makeCpgIslandsResultsTab();
    makeSegDuplicatesResultsTab();
    makeFindMotifsResultsTab();
    makeFireResultsTab();
    makeFindPathwaysResultsTab();
    makePageResultsTab();
    makeConsResultsTab();
    makeCompPeaksResultsTab();
    makeCompGenesResultsTab();
    makeCompJaccardResultsTab();

    findAvailableMotifs();
    findAvailablePathways();
    //findAvailableEncodeDatasets();

    //i=0;

    //set the default radio buttons
    //ui->findmotifBulykRadioButton->setChecked(false);
    //ui->findmotifNameRadioButton->setChecked(false);
    ui->peakBasedRadioButton->setChecked(true);
    ui->compPeaksANDRadioButton->setChecked(true);
    ui->findPathwaysPromotersCheckBox->setChecked(true);
    ui->pagePromotersCheckBox->setChecked(true);

    //create and set the progressBar
    progressBar = new QProgressBar();
    progressBar->setTextVisible(false);
    progressBar->setRange(0, 0);
    progressBar->setVisible(0);
    progressBar->setFixedWidth(120);
    this->statusBar()->addPermanentWidget(progressBar, -1);
    QFont newFont("Lucida Grande", 12, QFont::StyleItalic, true);
    this->statusBar()->setFont(newFont);

    //
    //connect Processes with slots to print output and handle the finish SIGNAL
    //

    //splitChipProcess
    connect(&splitChipProcess, SIGNAL(readyReadStandardOutput()), this,
            SLOT(readOutputFromSplitChipProcess()));
    connect(&splitChipProcess, SIGNAL(readyReadStandardError()), this,
            SLOT(readErrorFromSplitChipProcess()));
    connect(&splitChipProcess, SIGNAL(finished(int,QProcess::ExitStatus)), this,
            SLOT(onSplitChipProcessFinished()));

    //splitInputProcess
    connect(&splitInputProcess, SIGNAL(readyReadStandardOutput()), this,
            SLOT(readOutputFromSplitInputProcess()));
    connect(&splitInputProcess, SIGNAL(readyReadStandardError()), this,
            SLOT(readErrorFromSplitInputProcess()));
    connect(&splitInputProcess, SIGNAL(finished(int,QProcess::ExitStatus)), this,
            SLOT(onSplitInputProcessFinished()));

    //chipseeqerProcess
    connect(&chipseeqerProcess, SIGNAL(readyReadStandardOutput()), this,
            SLOT(readOutputFromChipseeqerProcess()));
    connect(&chipseeqerProcess, SIGNAL(readyReadStandardError()), this,
            SLOT(readErrorFromChipseeqerProcess()));
    connect(&chipseeqerProcess, SIGNAL(finished(int,QProcess::ExitStatus)), this,
            SLOT(onChipseeqerProcessFinished()));

    //summaryProcess
    connect(&summaryProcess, SIGNAL(readyReadStandardOutput()), this,
            SLOT(readOutputFromSummaryProcess()));
    connect(&summaryProcess, SIGNAL(readyReadStandardError()), this,
            SLOT(readErrorFromSummaryProcess()));
    connect(&summaryProcess, SIGNAL(finished(int,QProcess::ExitStatus)), this,
            SLOT(onSummaryProcessFinished()));

    //peaksTrackProcess
    connect(&peaksTrackProcess, SIGNAL(readyReadStandardOutput()), this,
            SLOT(readOutputFromPeaksTrackProcess()));
    connect(&peaksTrackProcess, SIGNAL(readyReadStandardError()), this,
            SLOT(readErrorFromPeaksTrackProcess()));
    connect(&peaksTrackProcess, SIGNAL(finished(int,QProcess::ExitStatus)), this,
            SLOT(onPeaksTrackProcessFinished()));

    //readsTrackProcess
    connect(&readsTrackProcess, SIGNAL(readyReadStandardOutput()), this,
            SLOT(readOutputFromReadsTrackProcess()));
    connect(&readsTrackProcess, SIGNAL(readyReadStandardError()), this,
            SLOT(readErrorFromReadsTrackProcess()));
    connect(&readsTrackProcess, SIGNAL(finished(int,QProcess::ExitStatus)), this,
            SLOT(onReadsTrackProcessFinished()));

    //genomicDistProcess
    connect(&genomicDistProcess, SIGNAL(readyReadStandardOutput()), this,
            SLOT(readOutputFromGenomicDistProcess()));
    connect(&genomicDistProcess, SIGNAL(readyReadStandardError()), this,
            SLOT(readErrorFromGenomicDistProcess()));
    connect(&genomicDistProcess, SIGNAL(finished(int,QProcess::ExitStatus)), this,
            SLOT(onGenomicDistProcessFinished()));

    //rnaGenesProcess
    connect(&rnaGenesProcess, SIGNAL(readyReadStandardOutput()), this,
            SLOT(readOutputFromFindRnaGenesProcess()));
    connect(&rnaGenesProcess, SIGNAL(readyReadStandardError()), this,
            SLOT(readErrorFromFindRnaGenesProcess()));
    connect(&rnaGenesProcess, SIGNAL(finished(int,QProcess::ExitStatus)), this,
            SLOT(onRnaGenesProcessFinished()));

    //rnaGenesProcess
    connect(&encodeProcess, SIGNAL(readyReadStandardOutput()), this,
            SLOT(readOutputFromEncodeProcess()));
    connect(&encodeProcess, SIGNAL(readyReadStandardError()), this,
            SLOT(readErrorFromEncodeProcess()));
    connect(&encodeProcess, SIGNAL(finished(int,QProcess::ExitStatus)), this,
            SLOT(onEncodeProcessFinished()));

    //repeatsProcess
    connect(&repeatsProcess, SIGNAL(readyReadStandardOutput()), this,
            SLOT(readOutputFromRepeatsProcess()));
    connect(&repeatsProcess, SIGNAL(readyReadStandardError()), this,
            SLOT(readErrorFromRepeatsProcess()));
    connect(&repeatsProcess, SIGNAL(finished(int,QProcess::ExitStatus)), this,
            SLOT(onRepeatsProcessFinished()));

    //cpgIslandsProcess
    connect(&cpgIslandsProcess, SIGNAL(readyReadStandardOutput()), this,
            SLOT(readOutputFromCpgIslandsProcess()));
    connect(&cpgIslandsProcess, SIGNAL(readyReadStandardError()), this,
            SLOT(readErrorFromCpgIslandsProcess()));
    connect(&cpgIslandsProcess, SIGNAL(finished(int,QProcess::ExitStatus)), this,
            SLOT(onCpgIslandsProcessFinished()));

    //segDuplicatesProcess
    connect(&segDuplicatesProcess, SIGNAL(readyReadStandardOutput()), this,
            SLOT(readOutputFromSegDuplicatesProcess()));
    connect(&segDuplicatesProcess, SIGNAL(readyReadStandardError()), this,
            SLOT(readErrorFromSegDuplicatesProcess()));
    connect(&segDuplicatesProcess, SIGNAL(finished(int,QProcess::ExitStatus)), this,
            SLOT(onSegDuplicatesProcessFinished()));

    //findmotifProcess
    connect(&findmotifProcess, SIGNAL(readyReadStandardOutput()), this,
            SLOT(readOutputFromFindMotifProcess()));
    connect(&findmotifProcess, SIGNAL(readyReadStandardError()), this,
            SLOT(readErrorFromFindMotifProcess()));
    connect(&findmotifProcess, SIGNAL(finished(int,QProcess::ExitStatus)), this,
            SLOT(onFindMotifProcessFinished()));

    //fireProcess
    connect(&fireProcess, SIGNAL(readyReadStandardOutput()), this,
            SLOT(readOutputFromFIREProcess()));
    connect(&fireProcess, SIGNAL(readyReadStandardError()), this,
            SLOT(readErrorFromFIREProcess()));
    connect(&fireProcess, SIGNAL(finished(int,QProcess::ExitStatus)), this,
            SLOT(onFIREProcessFinished()));

    //findPathwayProcess
    connect(&findPathwayProcess, SIGNAL(readyReadStandardOutput()), this,
            SLOT(readOutputFromFindPathwayProcess()));
    connect(&findPathwayProcess, SIGNAL(readyReadStandardError()), this,
            SLOT(readErrorFromFindPathwayProcess()));
    connect(&findPathwayProcess, SIGNAL(finished(int,QProcess::ExitStatus)), this,
            SLOT(onFindPathwayProcessFinished()));

    //pageProcess
    connect(&pageProcess, SIGNAL(readyReadStandardOutput()), this,
            SLOT(readOutputFromPAGEProcess()));
    connect(&pageProcess, SIGNAL(readyReadStandardError()), this,
            SLOT(readErrorFromPAGEProcess()));
    connect(&pageProcess, SIGNAL(finished(int,QProcess::ExitStatus)), this,
            SLOT(onPAGEProcessFinished()));

    //consProcess
    connect(&consProcess, SIGNAL(readyReadStandardOutput()), this,
            SLOT(readOutputFromConsProcess()));
    connect(&consProcess, SIGNAL(readyReadStandardError()), this,
            SLOT(readErrorFromConsProcess()));
    connect(&consProcess, SIGNAL(finished(int,QProcess::ExitStatus)), this,
            SLOT(onConsProcessFinished()));

    //compPeaksProcess
    connect(&compPeaksProcess, SIGNAL(readyReadStandardOutput()), this,
            SLOT(readOutputFromCompPeaksProcess()));
    connect(&compPeaksProcess, SIGNAL(readyReadStandardError()), this,
            SLOT(readErrorFromCompPeaksProcess()));
    connect(&compPeaksProcess, SIGNAL(finished(int,QProcess::ExitStatus)), this,
            SLOT(onCompPeaksProcessFinished()));

    //compGenesProcess
    connect(&compGenesProcess, SIGNAL(readyReadStandardOutput()), this,
            SLOT(readOutputFromCompGenesProcess()));
    connect(&compGenesProcess, SIGNAL(readyReadStandardError()), this,
            SLOT(readErrorFromCompGenesProcess()));
    connect(&compGenesProcess, SIGNAL(finished(int,QProcess::ExitStatus)), this,
            SLOT(onCompGenesProcessFinished()));

    //computeJaccardProcess
    connect(&computeJaccardProcess, SIGNAL(readyReadStandardOutput()), this,
            SLOT(readOutputFromJaccardProcess()));
    connect(&computeJaccardProcess, SIGNAL(readyReadStandardError()), this,
            SLOT(readErrorFromJaccardProcess()));
    connect(&computeJaccardProcess, SIGNAL(finished(int,QProcess::ExitStatus)), this,
            SLOT(onJaccardProcessFinished()));

    ui->genesSummarySpeciesComboBox->connect(ui->genesSummarySpeciesComboBox, SIGNAL(activated(int)), this, SLOT(checkGenesSummarySpeciesComboBox()) );
    ui->genomicDistSpeciesComboBox->connect(ui->genomicDistSpeciesComboBox, SIGNAL(activated(int)), this, SLOT(checkGenomDistSpeciesComboBox()) );
    ui->encodeSpeciesComboBox->connect(ui->encodeSpeciesComboBox, SIGNAL(activated(int)), this, SLOT(checkEncodeSpeciesComboBox()) );
    ui->findmotifBulykMotifComboBox->connect(ui->findmotifBulykMotifComboBox, SIGNAL(activated(int)), this, SLOT(changeFindMotifSuffixBComboBox()) );
    ui->findmotifJasparMotifComboBox->connect(ui->findmotifJasparMotifComboBox, SIGNAL(activated(int)), this, SLOT(changeFindMotifSuffixJComboBox()) );
    ui->findPathwaySpeciesComboBox->connect(ui->findPathwaySpeciesComboBox, SIGNAL(activated(int)), this, SLOT(checkFindPathwaySpeciesComboBox()) );
    ui->findPathwaySelectDatabaseComboBox->connect(ui->findPathwaySelectDatabaseComboBox, SIGNAL(activated(int)), this, SLOT(fillInPathwaysComboBox()) );
    ui->pageSpeciesComboBox->connect(ui->pageSpeciesComboBox, SIGNAL(activated(int)), this, SLOT(checkPageSpeciesComboBox()) );
}

CSDesktop::~CSDesktop()
{
    delete ui;
}

void CSDesktop::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type())
    {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

//
// Handling clicks on the ToolBox buttons
//

void CSDesktop::on_loadRawDataButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(1);
    ui->loadTabWidget->setCurrentIndex(0);

    setLoadRawButtonBold(true);
    setPeakDetectionButtonBold(false);
    setGenesSummaryButtonBold(false);
    setCreateTracksButtonBold(false);
    setGenomicDistButtonBold(false);
    setRNAGenesButtonBold(false);
    setEncodeButtonBold(false);
    setRepeatsButtonBold(false);
    setCpgIslandsButtonBold(false);
    setSegDuplicatesButtonBold(false);
    setFindMotifButtonBold(false);
    setFIREButtonBold(false);
    setFindPathwayButtonBold(false);
    setPAGEButtonBold(false);
    setConsButtonBold(false);
    setCompPeaksButtonBold(false);
    setCompGenesButtonBold(false);
    setJaccardButtonBold(false);
}

void CSDesktop::on_peakDetectionButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(2);
    ui->peakDetectionTabWidget->setCurrentIndex(0);

    setLoadRawButtonBold(false);
    setPeakDetectionButtonBold(true);
    setGenesSummaryButtonBold(false);
    setCreateTracksButtonBold(false);
    setGenomicDistButtonBold(false);
    setRNAGenesButtonBold(false);
    setEncodeButtonBold(false);
    setRepeatsButtonBold(false);
    setCpgIslandsButtonBold(false);
    setSegDuplicatesButtonBold(true);
    setFindMotifButtonBold(false);
    setFIREButtonBold(false);
    setFindPathwayButtonBold(false);
    setPAGEButtonBold(false);
    setConsButtonBold(false);
    setCompPeaksButtonBold(false);
    setCompGenesButtonBold(false);
    setJaccardButtonBold(false);
}

void CSDesktop::on_genesSummaryButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(3);
    ui->genesSummaryTabWidget->setCurrentIndex(0);

    setLoadRawButtonBold(false);
    setPeakDetectionButtonBold(false);
    setGenesSummaryButtonBold(true);
    setCreateTracksButtonBold(false);
    setGenomicDistButtonBold(false);
    setRNAGenesButtonBold(false);
    setEncodeButtonBold(false);
    setRepeatsButtonBold(false);
    setCpgIslandsButtonBold(false);
    setSegDuplicatesButtonBold(false);
    setFindMotifButtonBold(false);
    setFIREButtonBold(false);
    setFindPathwayButtonBold(false);
    setPAGEButtonBold(false);
    setConsButtonBold(false);
    setCompPeaksButtonBold(false);
    setCompGenesButtonBold(false);
    setJaccardButtonBold(false);
}

void CSDesktop::on_createTracksButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(7);
    ui->createTracksTabWidget->setCurrentIndex(0);

    setLoadRawButtonBold(false);
    setPeakDetectionButtonBold(false);
    setGenesSummaryButtonBold(false);
    setCreateTracksButtonBold(true);
    setGenomicDistButtonBold(false);
    setRNAGenesButtonBold(false);
    setEncodeButtonBold(false);
    setRepeatsButtonBold(false);
    setCpgIslandsButtonBold(false);
    setSegDuplicatesButtonBold(false);
    setFindMotifButtonBold(false);
    setFIREButtonBold(false);
    setFindPathwayButtonBold(false);
    setPAGEButtonBold(false);
    setConsButtonBold(false);
    setCompPeaksButtonBold(false);
    setCompGenesButtonBold(false);
    setJaccardButtonBold(false);

    ui->peaksTrackNameLineEdit->setText("ChIPseeqer peaks");
    ui->readsTrackNameLineEdit->setText("Smoothed reads");
}

void CSDesktop::on_genomicDistButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(4);
    ui->genomicDistTabWidget->setCurrentIndex(0);

    setLoadRawButtonBold(false);
    setPeakDetectionButtonBold(false);
    setGenesSummaryButtonBold(false);
    setCreateTracksButtonBold(false);
    setGenomicDistButtonBold(true);
    setRNAGenesButtonBold(false);
    setEncodeButtonBold(false);
    setRepeatsButtonBold(false);
    setCpgIslandsButtonBold(false);
    setSegDuplicatesButtonBold(false);
    setFindMotifButtonBold(false);
    setFIREButtonBold(false);
    setFindPathwayButtonBold(false);
    setPAGEButtonBold(false);
    setConsButtonBold(false);
    setCompPeaksButtonBold(false);
    setCompGenesButtonBold(false);
    setJaccardButtonBold(false);
}

void CSDesktop::on_rnaGenesButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(5);
    ui->rnaGenesTabWidget->setCurrentIndex(0);

    setLoadRawButtonBold(false);
    setPeakDetectionButtonBold(false);
    setGenesSummaryButtonBold(false);
    setCreateTracksButtonBold(false);
    setGenomicDistButtonBold(false);
    setRNAGenesButtonBold(true);
    setEncodeButtonBold(false);
    setRepeatsButtonBold(false);
    setCpgIslandsButtonBold(false);
    setSegDuplicatesButtonBold(false);
    setFindMotifButtonBold(false);
    setFIREButtonBold(false);
    setFindPathwayButtonBold(false);
    setPAGEButtonBold(false);
    setConsButtonBold(false);
    setCompPeaksButtonBold(false);
    setCompGenesButtonBold(false);
    setJaccardButtonBold(false);
}

void CSDesktop::on_encodeButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(6);
    ui->repeatsTabWidget->setCurrentIndex(0);

    setLoadRawButtonBold(false);
    setPeakDetectionButtonBold(false);
    setGenesSummaryButtonBold(false);
    setCreateTracksButtonBold(false);
    setGenomicDistButtonBold(false);
    setRNAGenesButtonBold(false);
    setEncodeButtonBold(true);
    setRepeatsButtonBold(false);
    setCpgIslandsButtonBold(false);
    setSegDuplicatesButtonBold(false);
    setFindMotifButtonBold(false);
    setFIREButtonBold(false);
    setFindPathwayButtonBold(false);
    setPAGEButtonBold(false);
    setConsButtonBold(false);
    setCompPeaksButtonBold(false);
    setCompGenesButtonBold(false);
    setJaccardButtonBold(false);

    findAvailableEncodeDatasets("hg19");
}


void CSDesktop::on_repeatsButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(8);
    ui->repeatsTabWidget->setCurrentIndex(0);

    setLoadRawButtonBold(false);
    setPeakDetectionButtonBold(false);
    setGenesSummaryButtonBold(false);
    setCreateTracksButtonBold(false);
    setGenomicDistButtonBold(false);
    setRNAGenesButtonBold(false);
    setEncodeButtonBold(false);
    setRepeatsButtonBold(true);
    setCpgIslandsButtonBold(false);
    setSegDuplicatesButtonBold(false);
    setFindMotifButtonBold(false);
    setFIREButtonBold(false);
    setFindPathwayButtonBold(false);
    setPAGEButtonBold(false);
    setConsButtonBold(false);
    setCompPeaksButtonBold(false);
    setCompGenesButtonBold(false);
    setJaccardButtonBold(false);
}

void CSDesktop::on_cpgIslandsButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(9);
    ui->cpgIslandsTabWidget->setCurrentIndex(0);

    setLoadRawButtonBold(false);
    setPeakDetectionButtonBold(false);
    setGenesSummaryButtonBold(false);
    setCreateTracksButtonBold(false);
    setGenomicDistButtonBold(false);
    setRNAGenesButtonBold(false);
    setEncodeButtonBold(false);
    setRepeatsButtonBold(false);
    setCpgIslandsButtonBold(true);
    setSegDuplicatesButtonBold(false);
    setFindMotifButtonBold(false);
    setFIREButtonBold(false);
    setFindPathwayButtonBold(false);
    setPAGEButtonBold(false);
    setConsButtonBold(false);
    setCompPeaksButtonBold(false);
    setCompGenesButtonBold(false);
    setJaccardButtonBold(false);
}

void CSDesktop::on_segDuplicatesButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(10);
    ui->segDuplicatesTabWidget->setCurrentIndex(0);

    setLoadRawButtonBold(false);
    setPeakDetectionButtonBold(false);
    setGenesSummaryButtonBold(false);
    setCreateTracksButtonBold(false);
    setGenomicDistButtonBold(false);
    setRNAGenesButtonBold(false);
    setEncodeButtonBold(false);
    setRepeatsButtonBold(false);
    setCpgIslandsButtonBold(false);
    setSegDuplicatesButtonBold(true);
    setFindMotifButtonBold(false);
    setFIREButtonBold(false);
    setFindPathwayButtonBold(false);
    setPAGEButtonBold(false);
    setConsButtonBold(false);
    setCompPeaksButtonBold(false);
    setCompGenesButtonBold(false);
    setJaccardButtonBold(false);
}

void CSDesktop::on_findmotifButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(11);
    ui->findmotifTabWidget->setCurrentIndex(0);

    setLoadRawButtonBold(false);
    setPeakDetectionButtonBold(false);
    setGenesSummaryButtonBold(false);
    setCreateTracksButtonBold(false);
    setGenomicDistButtonBold(false);
    setRNAGenesButtonBold(false);
    setEncodeButtonBold(false);
    setRepeatsButtonBold(false);
    setCpgIslandsButtonBold(false);
    setSegDuplicatesButtonBold(false);
    setFindMotifButtonBold(true);
    setFIREButtonBold(false);
    setFindPathwayButtonBold(false);
    setPAGEButtonBold(false);
    setConsButtonBold(false);
    setCompPeaksButtonBold(false);
    setCompGenesButtonBold(false);
    setJaccardButtonBold(false);
}

void CSDesktop::on_fireButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(12);
    ui->fireTabWidget->setCurrentIndex(0);

    setLoadRawButtonBold(false);
    setPeakDetectionButtonBold(false);
    setGenesSummaryButtonBold(false);
    setCreateTracksButtonBold(false);
    setGenomicDistButtonBold(false);
    setRNAGenesButtonBold(false);
    setEncodeButtonBold(false);
    setRepeatsButtonBold(false);
    setCpgIslandsButtonBold(false);
    setSegDuplicatesButtonBold(false);
    setFindMotifButtonBold(false);
    setFIREButtonBold(true);
    setFindPathwayButtonBold(false);
    setPAGEButtonBold(false);
    setConsButtonBold(false);
    setCompPeaksButtonBold(false);
    setCompGenesButtonBold(false);
    setJaccardButtonBold(false);
}


void CSDesktop::on_findPathwayButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(13);
    ui->findPathwayTabWidget->setCurrentIndex(0);

    setLoadRawButtonBold(false);
    setPeakDetectionButtonBold(false);
    setGenesSummaryButtonBold(false);
    setCreateTracksButtonBold(false);
    setGenomicDistButtonBold(false);
    setRNAGenesButtonBold(false);
    setEncodeButtonBold(false);
    setRepeatsButtonBold(false);
    setCpgIslandsButtonBold(false);
    setSegDuplicatesButtonBold(false);
    setFindMotifButtonBold(false);
    setFIREButtonBold(false);
    setFindPathwayButtonBold(true);
    setPAGEButtonBold(false);
    setConsButtonBold(false);
    setCompPeaksButtonBold(false);
    setCompGenesButtonBold(false);
    setJaccardButtonBold(false);
}

void CSDesktop::on_pageButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(14);
    ui->pageTabWidget->setCurrentIndex(0);

    setLoadRawButtonBold(false);
    setPeakDetectionButtonBold(false);
    setGenesSummaryButtonBold(false);
    setCreateTracksButtonBold(false);
    setGenomicDistButtonBold(false);
    setRNAGenesButtonBold(false);
    setEncodeButtonBold(false);
    setRepeatsButtonBold(false);
    setCpgIslandsButtonBold(false);
    setSegDuplicatesButtonBold(false);
    setFindMotifButtonBold(false);
    setFIREButtonBold(false);
    setFindPathwayButtonBold(false);
    setPAGEButtonBold(true);
    setConsButtonBold(false);
    setCompPeaksButtonBold(false);
    setCompGenesButtonBold(false);
    setJaccardButtonBold(false);
}

void CSDesktop::on_consButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(15);
    ui->consTabWidget->setCurrentIndex(0);

    setLoadRawButtonBold(false);
    setPeakDetectionButtonBold(false);
    setGenesSummaryButtonBold(false);
    setCreateTracksButtonBold(false);
    setGenomicDistButtonBold(false);
    setRNAGenesButtonBold(false);
    setEncodeButtonBold(false);
    setRepeatsButtonBold(false);
    setCpgIslandsButtonBold(false);
    setSegDuplicatesButtonBold(false);
    setFindMotifButtonBold(false);
    setFIREButtonBold(false);
    setFindPathwayButtonBold(false);
    setPAGEButtonBold(false);
    setConsButtonBold(true);
    setCompPeaksButtonBold(false);
    setCompGenesButtonBold(false);
    setJaccardButtonBold(false);
}

void CSDesktop::on_comparePeaksButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(16);
    ui->compPeaksTabWidget->setCurrentIndex(0);

    setLoadRawButtonBold(false);
    setPeakDetectionButtonBold(false);
    setGenesSummaryButtonBold(false);
    setCreateTracksButtonBold(false);
    setGenomicDistButtonBold(false);
    setRNAGenesButtonBold(false);
    setEncodeButtonBold(false);
    setRepeatsButtonBold(false);
    setCpgIslandsButtonBold(false);
    setSegDuplicatesButtonBold(false);
    setFindMotifButtonBold(false);
    setFIREButtonBold(false);
    setFindPathwayButtonBold(false);
    setPAGEButtonBold(false);
    setConsButtonBold(false);
    setCompPeaksButtonBold(true);
    setCompGenesButtonBold(false);
    setJaccardButtonBold(false);
}

void CSDesktop::on_compareGenesButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(17);
    ui->compGenesTabWidget->setCurrentIndex(0);

    setLoadRawButtonBold(false);
    setPeakDetectionButtonBold(false);
    setGenesSummaryButtonBold(false);
    setCreateTracksButtonBold(false);
    setGenomicDistButtonBold(false);
    setRNAGenesButtonBold(false);
    setEncodeButtonBold(false);
    setRepeatsButtonBold(false);
    setCpgIslandsButtonBold(false);
    setSegDuplicatesButtonBold(false);
    setFindMotifButtonBold(false);
    setFIREButtonBold(false);
    setFindPathwayButtonBold(false);
    setPAGEButtonBold(false);
    setConsButtonBold(false);
    setCompPeaksButtonBold(false);
    setCompGenesButtonBold(true);
    setJaccardButtonBold(false);
}

void CSDesktop::on_jaccardButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(18);
    ui->jaccardTabWidget->setCurrentIndex(0);

    setLoadRawButtonBold(false);
    setPeakDetectionButtonBold(false);
    setGenesSummaryButtonBold(false);
    setCreateTracksButtonBold(false);
    setGenomicDistButtonBold(false);
    setRNAGenesButtonBold(false);
    setEncodeButtonBold(false);
    setRepeatsButtonBold(false);
    setCpgIslandsButtonBold(false);
    setSegDuplicatesButtonBold(false);
    setFindMotifButtonBold(false);
    setFIREButtonBold(false);
    setFindPathwayButtonBold(false);
    setPAGEButtonBold(false);
    setConsButtonBold(false);
    setCompPeaksButtonBold(false);
    setCompGenesButtonBold(false);
    setJaccardButtonBold(true);
}

//
// Functions that make bold the ToolBox buttons font
//

void CSDesktop::setLoadRawButtonBold(bool bold) {
    QFont f = ui->loadRawDataButton->font();
    f.setBold(bold);
    ui->loadRawDataButton->setFont(f);
}

void CSDesktop::setPeakDetectionButtonBold(bool bold) {
    QFont f = ui->peakDetectionButton->font();
    f.setBold(bold);
    ui->peakDetectionButton->setFont(f);
}

void CSDesktop::setGenesSummaryButtonBold(bool bold) {
    QFont f = ui->genesSummaryButton->font();
    f.setBold(bold);
    ui->genesSummaryButton->setFont(f);
}

void CSDesktop::setCreateTracksButtonBold(bool bold) {
    QFont f = ui->createTracksButton->font();
    f.setBold(bold);
    ui->createTracksButton->setFont(f);
}

void CSDesktop::setGenomicDistButtonBold(bool bold) {
    QFont f = ui->genomicDistButton->font();
    f.setBold(bold);
    ui->genomicDistButton->setFont(f);
}

void CSDesktop::setRNAGenesButtonBold(bool bold) {
    QFont f = ui->rnaGenesButton->font();
    f.setBold(bold);
    ui->rnaGenesButton->setFont(f);
}

void CSDesktop::setEncodeButtonBold(bool bold) {
    QFont f = ui->encodeButton->font();
    f.setBold(bold);
    ui->encodeButton->setFont(f);
}

void CSDesktop::setRepeatsButtonBold(bool bold) {
    QFont f = ui->repeatsButton->font();
    f.setBold(bold);
    ui->repeatsButton->setFont(f);
}

void CSDesktop::setCpgIslandsButtonBold(bool bold) {
    QFont f = ui->cpgIslandsButton->font();
    f.setBold(bold);
    ui->cpgIslandsButton->setFont(f);
}

void CSDesktop::setSegDuplicatesButtonBold(bool bold) {
    QFont f = ui->segDuplicatesButton->font();
    f.setBold(bold);
    ui->segDuplicatesButton->setFont(f);
}

void CSDesktop::setFindMotifButtonBold(bool bold) {
    QFont f = ui->findmotifButton->font();
    f.setBold(bold);
    ui->findmotifButton->setFont(f);
}

void CSDesktop::setFIREButtonBold(bool bold) {
    QFont f = ui->fireButton->font();
    f.setBold(bold);
    ui->fireButton->setFont(f);
}

void CSDesktop::setFindPathwayButtonBold(bool bold) {
    QFont f = ui->findPathwayButton->font();
    f.setBold(bold);
    ui->findPathwayButton->setFont(f);
}

void CSDesktop::setPAGEButtonBold(bool bold) {
    QFont f = ui->pageButton->font();
    f.setBold(bold);
    ui->pageButton->setFont(f);
}

void CSDesktop::setConsButtonBold(bool bold) {
    QFont f = ui->consButton->font();
    f.setBold(bold);
    ui->consButton->setFont(f);
}

void CSDesktop::setCompPeaksButtonBold(bool bold) {
    QFont f = ui->comparePeaksButton->font();
    f.setBold(bold);
    ui->comparePeaksButton->setFont(f);
}

void CSDesktop::setCompGenesButtonBold(bool bold) {
    QFont f = ui->compareGenesButton->font();
    f.setBold(bold);
    ui->compareGenesButton->setFont(f);
}

void CSDesktop::setJaccardButtonBold(bool bold) {
    QFont f = ui->jaccardButton->font();
    f.setBold(bold);
    ui->jaccardButton->setFont(f);
}

//
// Function that sets the default index in all tabs
//

void CSDesktop::setDefaultIndices()
{
    ui->stackedWidget->setCurrentIndex(0);
    ui->loadTabWidget->setCurrentIndex(0);
    ui->peakDetectionTabWidget->setCurrentIndex(0);
    ui->genesSummaryTabWidget->setCurrentIndex(0);
    ui->genomicDistTabWidget->setCurrentIndex(0);
    ui->createTracksTabWidget->setCurrentIndex(0);
    ui->repeatsTabWidget->setCurrentIndex(0);
    ui->cpgIslandsTabWidget->setCurrentIndex(0);
    ui->segDuplicatesTabWidget->setCurrentIndex(0);
    ui->findmotifTabWidget->setCurrentIndex(0);
    ui->consTabWidget->setCurrentIndex(0);
}

//
// Functions that copy the results tables
//

void CSDesktop::peakDetectionTableCopy() {

    QString tableStr;

    for (int r = 0; r < this->peakDetectionTable->rowCount(); r++) {

        for (int c = 0; c < this->peakDetectionTable->columnCount(); c++) {
            QTableWidgetItem* pWidget = this->peakDetectionTable->item(r, c);

            tableStr += pWidget->text();
            tableStr += "\t";
        }
        tableStr += "\n";
    }
    QApplication::clipboard()->setText(tableStr);
}

void CSDesktop::genomicDistTableCopy() {

    QString tableStr;

    for (int r = 0; r < this->genomicDistTable->rowCount(); r++) {

        for (int c = 0; c < this->genomicDistTable->columnCount(); c++) {
            QTableWidgetItem* pWidget = this->genomicDistTable->item(r, c);

            tableStr += pWidget->text();
            tableStr += "\t";
        }
        tableStr += "\n";
    }
    QApplication::clipboard()->setText(tableStr);
}

void CSDesktop::summaryTableCopy() {

    QString tableStr;

    for (int r = 0; r < this->summaryTable->rowCount(); r++) {

        for (int c = 0; c < this->summaryTable->columnCount(); c++) {
            QTableWidgetItem* pWidget = this->summaryTable->item(r, c);

            tableStr += pWidget->text();
            tableStr += "\t";
        }
        tableStr += "\n";
    }
    QApplication::clipboard()->setText(tableStr);
}

void CSDesktop::rnaGenesTableCopy() {

    QString tableStr;

    for (int r = 0; r < this->rnaGenesTable->rowCount(); r++) {

        for (int c = 0; c < this->rnaGenesTable->columnCount(); c++) {
            QTableWidgetItem* pWidget = this->rnaGenesTable->item(r, c);

            tableStr += pWidget->text();
            tableStr += "\t";
        }
        tableStr += "\n";
    }
    QApplication::clipboard()->setText(tableStr);
}

void CSDesktop::encodeTableCopy() {

    QString tableStr;

    for (int r = 0; r < this->encodeTable->rowCount(); r++) {

        for (int c = 0; c < this->encodeTable->columnCount(); c++) {
            QTableWidgetItem* pWidget = this->encodeTable->item(r, c);

            tableStr += pWidget->text();
            tableStr += "\t";
        }
        tableStr += "\n";
    }
    QApplication::clipboard()->setText(tableStr);
}

void CSDesktop::repeatsTableCopy() {

    QString tableStr;

    for (int r = 0; r < this->repeatsTable->rowCount(); r++) {

        for (int c = 0; c < this->repeatsTable->columnCount(); c++) {
            QTableWidgetItem* pWidget = this->repeatsTable->item(r, c);

            tableStr += pWidget->text();
            tableStr += "\t";
        }
        tableStr += "\n";
    }
    QApplication::clipboard()->setText(tableStr);
}

void CSDesktop::cpgIslandsTableCopy() {

    QString tableStr;

    for (int r = 0; r < this->cpgIslandsTable->rowCount(); r++) {

        for (int c = 0; c < this->cpgIslandsTable->columnCount(); c++) {
            QTableWidgetItem* pWidget = this->cpgIslandsTable->item(r, c);

            tableStr += pWidget->text();
            tableStr += "\t";
        }
        tableStr += "\n";
    }
    QApplication::clipboard()->setText(tableStr);
}

void CSDesktop::segDuplicatesTableCopy() {

    QString tableStr;

    for (int r = 0; r < this->segDuplicatesTable->rowCount(); r++) {

        for (int c = 0; c < this->segDuplicatesTable->columnCount(); c++) {
            QTableWidgetItem* pWidget = this->segDuplicatesTable->item(r, c);

            tableStr += pWidget->text();
            tableStr += "\t";
        }
        tableStr += "\n";
    }
    QApplication::clipboard()->setText(tableStr);
}

void CSDesktop::findMotifsTableCopy() {

    QString tableStr;

    for (int r = 0; r < this->findMotifsTable->rowCount(); r++) {

        for (int c = 0; c < this->findMotifsTable->columnCount(); c++) {
            QTableWidgetItem* pWidget = this->findMotifsTable->item(r, c);

            tableStr += pWidget->text();
            tableStr += "\t";
        }
        tableStr += "\n";
    }
    QApplication::clipboard()->setText(tableStr);
}

void CSDesktop::fireTableCopy() {

    QString tableStr;

    for (int r = 0; r < this->fireTable->rowCount(); r++) {

        for (int c = 0; c < this->fireTable->columnCount(); c++) {
            QTableWidgetItem* pWidget = this->fireTable->item(r, c);

            tableStr += pWidget->text();
            tableStr += "\t";
        }
        tableStr += "\n";
    }
    QApplication::clipboard()->setText(tableStr);
}

void CSDesktop::findPathwaysTableCopy() {

    QString tableStr;

    for (int r = 0; r < this->findPathwaysTable->rowCount(); r++) {

        for (int c = 0; c < this->findPathwaysTable->columnCount(); c++) {
            QTableWidgetItem* pWidget = this->findPathwaysTable->item(r, c);

            tableStr += pWidget->text();
            tableStr += "\t";
        }
        tableStr += "\n";
    }
    QApplication::clipboard()->setText(tableStr);
}

void CSDesktop::pageTableCopy() {

    QString tableStr;

    for (int r = 0; r < this->pageTable->rowCount(); r++) {

        for (int c = 0; c < this->pageTable->columnCount(); c++) {
            QTableWidgetItem* pWidget = this->pageTable->item(r, c);

            tableStr += pWidget->text();
            tableStr += "\t";
        }
        tableStr += "\n";
    }
    QApplication::clipboard()->setText(tableStr);
}

void CSDesktop::conservationTableCopy() {

    QString tableStr;

    for (int r = 0; r < this->conservationTable->rowCount(); r++) {

        for (int c = 0; c < this->conservationTable->columnCount(); c++) {
            QTableWidgetItem* pWidget = this->conservationTable->item(r, c);

            tableStr += pWidget->text();
            tableStr += "\t";
        }
        tableStr += "\n";
    }
    QApplication::clipboard()->setText(tableStr);
}

void CSDesktop::compPeaksTableCopy()
{
    QString tableStr;

    for (int r = 0; r < this->compPeaksTable->rowCount(); r++) {

        for (int c = 0; c < this->compPeaksTable->columnCount(); c++) {
            QTableWidgetItem* pWidget = this->compPeaksTable->item(r, c);

            tableStr += pWidget->text();
            tableStr += "\t";
        }
        tableStr += "\n";
    }
    QApplication::clipboard()->setText(tableStr);
}

void CSDesktop::compGenesTableCopy() {

    QString tableStr;

    for (int r = 0; r < this->compGenesTable->rowCount(); r++) {

        for (int c = 0; c < this->compGenesTable->columnCount(); c++) {
            QTableWidgetItem* pWidget = this->compGenesTable->item(r, c);

            tableStr += pWidget->text();
            tableStr += "\t";
        }
        tableStr += "\n";
    }
    QApplication::clipboard()->setText(tableStr);
}

void CSDesktop::compJaccardTableCopy() {

    QString tableStr;

    for (int r = 0; r < this->compJaccardTable->rowCount(); r++) {

        for (int c = 0; c < this->compJaccardTable->columnCount(); c++) {
            QTableWidgetItem* pWidget = this->compJaccardTable->item(r, c);

            tableStr += pWidget->text();
            tableStr += "\t";
        }
        tableStr += "\n";
    }
    QApplication::clipboard()->setText(tableStr);
}


//
// Function that creates the results tab for the Peak Detection
//
void CSDesktop::makePeakDetResultsTab()
{
    showPeakDetTab           = new QWidget(ui->peakDetectionTabWidget);
    peakDetStatsLabel        = new QLabel(showPeakDetTab);
    showPeakDetStatsTextEdit = new QTextEdit(showPeakDetTab);
    peakDetectionTable       = new QTableWidget(showPeakDetTab);
    peakDetectionCopyButton  = new QPushButton(showPeakDetTab);

    peakDetectionTable->setStatusTip("Double-click on a table row to see the peak (first 3 columns: chr-start-end) in the Genome Browser.");

    showPeakDetTab->setVisible(false);
    peakDetStatsLabel->setVisible(false);
    showPeakDetStatsTextEdit->setVisible(false);
    peakDetectionTable->setVisible(false);
    peakDetectionCopyButton->setVisible(false);
    peakDetectionTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
}

//
// Function that creates the results tab for the Genomic Distribution
//
void CSDesktop::makeGenDistResultsTab()
{
    showGenDistTab              = new QWidget(ui->genomicDistTabWidget);
    genDistStatsLabel           = new QLabel(showGenDistTab);
    showGenDistStatsTextEdit    = new QTextEdit(showGenDistTab);
    genomicDistTable            = new QTableWidget(showGenDistTab);
    genomicDistCopyButton       = new QPushButton(showGenDistTab);

    genomicDistTable->setStatusTip("Double-click on a table row to see the peak (first 3 columns: chr-start-end) in the Genome Browser.");

    showPieChartTab             = new QWidget(ui->genomicDistTabWidget);
    showPieChartTab->setVisible(false);

    showGenDistTab->setVisible(false);
    genDistStatsLabel->setVisible(false);
    showGenDistStatsTextEdit->setVisible(false);
    genomicDistTable->setVisible(false);
    genomicDistCopyButton->setVisible(false);
    genomicDistTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
}

//
// Function that creates the results tab for the Summary Distribution
//
void CSDesktop::makeGenSummResultsTab()
{
    showGenSummTab              = new QWidget(ui->genesSummaryTabWidget);
    genSummStatsLabel           = new QLabel(showGenSummTab);
    showGenSummStatsTextEdit    = new QTextEdit(showGenSummTab);
    summaryTable                = new QTableWidget(showGenSummTab);
    summaryCopyButton           = new QPushButton(showGenSummTab);

    summaryTable->setStatusTip("Double-click on a table row to see the peak (first 3 columns: chr-start-end) in the Genome Browser.");

    showGenSummTab->setVisible(false);
    genSummStatsLabel->setVisible(false);
    showGenSummStatsTextEdit->setVisible(false);
    summaryTable->setVisible(false);
    summaryCopyButton->setVisible(false);
    summaryTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
}

//
// Function that creates the results tab for the RNA Genes
//
void CSDesktop::makeRnaGenesResultsTab()
{
    showRnaGenesTab              = new QWidget(ui->rnaGenesTabWidget);
    rnaGenesStatsLabel           = new QLabel(showRnaGenesTab);
    showRnaGenesStatsTextEdit    = new QTextEdit(showRnaGenesTab);
    rnaGenesTable                = new QTableWidget(showRnaGenesTab);
    rnaGenesCopyButton           = new QPushButton(showRnaGenesTab);

    rnaGenesTable->setStatusTip("Double-click on a table row to see the peak (first 3 columns: chr-start-end) in the Genome Browser.");

    showRnaGenesTab->setVisible(false);
    rnaGenesStatsLabel->setVisible(false);
    showRnaGenesStatsTextEdit->setVisible(false);
    rnaGenesTable->setVisible(false);
    rnaGenesCopyButton->setVisible(false);
    rnaGenesTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
}

//
// Function that creates the results tab for the RNA Genes
//
void CSDesktop::makeEncodeResultsTab()
{
    showEncodeTab              = new QWidget(ui->encodeTabWidget);
    encodeStatsLabel           = new QLabel(showEncodeTab);
    showEncodeStatsTextEdit    = new QTextEdit(showEncodeTab);
    encodeTable                = new QTableWidget(showEncodeTab);
    encodeCopyButton           = new QPushButton(showEncodeTab);

    encodeTable->setStatusTip("Double-click on a table row to see the peak (first 3 columns: chr-start-end) in the Genome Browser.");

    showEncodeTab->setVisible(false);
    encodeStatsLabel->setVisible(false);
    showEncodeStatsTextEdit->setVisible(false);
    encodeTable->setVisible(false);
    encodeCopyButton->setVisible(false);
    encodeTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
}

//
// Function that creates the results tab for the Repeats
//
void CSDesktop::makeRepeatsResultsTab()
{
    showRepeatsTab              = new QWidget(ui->repeatsTabWidget);
    repeatsStatsLabel           = new QLabel(showRepeatsTab);
    showRepeatsStatsTextEdit    = new QTextEdit(showRepeatsTab);
    repeatsTable                = new QTableWidget(showRepeatsTab);
    repeatsCopyButton           = new QPushButton(showRepeatsTab);

    repeatsTable->setStatusTip("Double-click on a table row to see the peak (first 3 columns: chr-start-end) in the Genome Browser.");

    showRepeatsTab->setVisible(false);
    repeatsStatsLabel->setVisible(false);
    showRepeatsStatsTextEdit->setVisible(false);
    repeatsTable->setVisible(false);
    repeatsCopyButton->setVisible(false);
    repeatsTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
}

//
// Function that creates the results tab for the CpG Islands
//
void CSDesktop::makeCpgIslandsResultsTab()
{
    showCpgIslandsTab               = new QWidget(ui->cpgIslandsTabWidget);
    cpgIslandsStatsLabel            = new QLabel(showCpgIslandsTab);
    showCpgIslandsStatsTextEdit     = new QTextEdit(showCpgIslandsTab);
    cpgIslandsTable                 = new QTableWidget(showCpgIslandsTab);
    cpgIslandsCopyButton            = new QPushButton(showCpgIslandsTab);

    cpgIslandsTable->setStatusTip("Double-click on a table row to see the peak (first 3 columns: chr-start-end) in the Genome Browser.");

    showCpgIslandsTab->setVisible(false);
    cpgIslandsStatsLabel->setVisible(false);
    showCpgIslandsStatsTextEdit->setVisible(false);
    cpgIslandsTable->setVisible(false);
    cpgIslandsCopyButton->setVisible(false);
    cpgIslandsTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
}

//
// Function that creates the results tab for the Segmental Duplicates
//
void CSDesktop::makeSegDuplicatesResultsTab()
{
    showSegDupsTab               = new QWidget(ui->segDuplicatesTabWidget);
    segDupsStatsLabel            = new QLabel(showSegDupsTab);
    showSegDupsStatsTextEdit     = new QTextEdit(showSegDupsTab);
    segDuplicatesTable           = new QTableWidget(showSegDupsTab);
    segDuplicatesCopyButton      = new QPushButton(showSegDupsTab);

    segDuplicatesTable->setStatusTip("Double-click on a table row to see the peak (first 3 columns: chr-start-end) in the Genome Browser.");

    showSegDupsTab->setVisible(false);
    segDupsStatsLabel->setVisible(false);
    showSegDupsStatsTextEdit->setVisible(false);
    segDuplicatesTable->setVisible(false);
    segDuplicatesCopyButton->setVisible(false);
    segDuplicatesTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
}

//
// Function that creates the results tab for the FindMotifs
//
void CSDesktop::makeFindMotifsResultsTab()
{
    showFindMotifsTab               = new QWidget(ui->findmotifTabWidget);
    findMotifsStatsLabel            = new QLabel(showFindMotifsTab);
    showFindMotifsStatsTextEdit     = new QTextEdit(showFindMotifsTab);
    findMotifsTable                 = new QTableWidget(showFindMotifsTab);
    findMotifsCopyButton            = new QPushButton(showFindMotifsTab);

    findMotifsTable->setStatusTip("Double-click on a table row to see the peak (first 3 columns: chr-start-end) in the Genome Browser.");

    showFindMotifsTab->setVisible(false);
    findMotifsStatsLabel->setVisible(false);
    showFindMotifsStatsTextEdit->setVisible(false);
    findMotifsTable->setVisible(false);
    findMotifsCopyButton->setVisible(false);
    findMotifsTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
}

//
// Function that creates the results tab for the FIRE
//
void CSDesktop::makeFireResultsTab()
{
    showFireTab               = new QWidget(ui->fireTabWidget);
    fireStatsLabel            = new QLabel(showFireTab);
    showFireStatsTextEdit     = new QTextEdit(showFireTab);
    fireTable                 = new QTableWidget(showFireTab);
    fireCopyButton            = new QPushButton(showFireTab);
    fireScrollArea            = new QScrollArea(showFireTab);
    fireImageLabel            = new QLabel;

    fireScrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    fireScrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);

    //fireTable->setStatusTip("Double-click on a table row to see the peak (first 3 columns: chr-start-end) in the Genome Browser.");

    showFireTab->setVisible(false);
    fireStatsLabel->setVisible(false);
    showFireStatsTextEdit->setVisible(false);
    fireTable->setVisible(false);
    fireCopyButton->setVisible(false);
    fireScrollArea->setVisible(false);
    fireImageLabel->setVisible(false);
    fireTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
}

//
// Function that creates the results tab for the FindPathways
//
void CSDesktop::makeFindPathwaysResultsTab()
{
    showFindPathwaysTab               = new QWidget(ui->findPathwayTabWidget);
    findPathwaysStatsLabel            = new QLabel(showFindPathwaysTab);
    showFindPathwaysStatsTextEdit     = new QTextEdit(showFindPathwaysTab);
    findPathwaysTable                 = new QTableWidget(showFindPathwaysTab);
    findPathwaysCopyButton            = new QPushButton(showFindPathwaysTab);

    //findPathwaysTable->setStatusTip("Double-click on a table row to see the peak (first 3 columns: chr-start-end) in the Genome Browser.");

    showFindPathwaysTab->setVisible(false);
    findPathwaysStatsLabel->setVisible(false);
    showFindPathwaysStatsTextEdit->setVisible(false);
    findPathwaysTable->setVisible(false);
    findPathwaysCopyButton->setVisible(false);
    findPathwaysTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
}

//
// Function that creates the results tab for the FindPathways
//
void CSDesktop::makePageResultsTab()
{
    showPageTab               = new QWidget(ui->pageTabWidget);
    pageStatsLabel            = new QLabel(showPageTab);
    showPageStatsTextEdit     = new QTextEdit(showPageTab);
    pageTable                 = new QTableWidget(showPageTab);
    pageCopyButton            = new QPushButton(showPageTab);
    pageScrollArea            = new QScrollArea(showPageTab);
    pageImageLabel            = new QLabel;

    pageScrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    pageScrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);

    //pageTable->setStatusTip("Double-click on a table row to see the peak (first 3 columns: chr-start-end) in the Genome Browser.");

    showPageTab->setVisible(false);
    pageStatsLabel->setVisible(false);
    showPageStatsTextEdit->setVisible(false);
    pageTable->setVisible(false);
    pageCopyButton->setVisible(false);
    pageScrollArea->setVisible(false);
    pageImageLabel->setVisible(false);
    pageTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
}

//
// Function that creates the results tab for the Conservation
//
void CSDesktop::makeConsResultsTab()
{
    showConservationTab               = new QWidget(ui->consTabWidget);
    conservationStatsLabel            = new QLabel(showConservationTab);
    showConservationStatsTextEdit     = new QTextEdit(showConservationTab);
    conservationTable                 = new QTableWidget(showConservationTab);
    conservationCopyButton            = new QPushButton(showConservationTab);

    conservationTable->setStatusTip("Double-click on a table row to see the peak (first 3 columns: chr-start-end) in the Genome Browser.");

    showConservationTab->setVisible(false);
    conservationStatsLabel->setVisible(false);
    showConservationStatsTextEdit->setVisible(false);
    conservationTable->setVisible(false);
    conservationCopyButton->setVisible(false);
    conservationTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
}

//
// Function that creates the results tab for the ComparePeaks
//
void CSDesktop::makeCompPeaksResultsTab()
{
    showCompPeaksTab               = new QWidget(ui->compPeaksTabWidget);
    compPeaksStatsLabel            = new QLabel(showCompPeaksTab);
    showCompPeaksStatsTextEdit     = new QTextEdit(showCompPeaksTab);
    compPeaksTable                 = new QTableWidget(showCompPeaksTab);
    compPeaksCopyButton            = new QPushButton(showCompPeaksTab);

    compPeaksTable->setStatusTip("Double-click on a table row to see the peak (first 3 columns: chr-start-end) in the Genome Browser.");

    showCompPeaksTab->setVisible(false);
    compPeaksStatsLabel->setVisible(false);
    showCompPeaksStatsTextEdit->setVisible(false);
    compPeaksTable->setVisible(false);
    compPeaksCopyButton->setVisible(false);
    compPeaksTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
}

//
// Function that creates the results tab for the ComparePeaks
//
void CSDesktop::makeCompGenesResultsTab()
{
    showCompGenesTab               = new QWidget(ui->compGenesTabWidget);
    compGenesStatsLabel            = new QLabel(showCompGenesTab);
    showCompGenesStatsTextEdit     = new QTextEdit(showCompGenesTab);
    compGenesTable                 = new QTableWidget(showCompGenesTab);
    compGenesCopyButton            = new QPushButton(showCompGenesTab);

    compGenesTable->setStatusTip("Double-click on a table row to see the gene in the Genome Browser.");

    showCompGenesTab->setVisible(false);
    compGenesStatsLabel->setVisible(false);
    showCompGenesStatsTextEdit->setVisible(false);
    compGenesTable->setVisible(false);
    compGenesCopyButton->setVisible(false);
    compGenesTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
}

//
// Function that creates the results tab for the ComparePeaks
//
void CSDesktop::makeCompJaccardResultsTab()
{
    showCompJaccardTab               = new QWidget(ui->jaccardTabWidget);
    compJaccardStatsLabel            = new QLabel(showCompJaccardTab);
    showCompJaccardStatsTextEdit     = new QTextEdit(showCompJaccardTab);
    compJaccardTable                 = new QTableWidget(showCompJaccardTab);
    compJaccardCopyButton            = new QPushButton(showCompJaccardTab);

    showCompJaccardTab->setVisible(false);
    compJaccardStatsLabel->setVisible(false);
    showCompJaccardStatsTextEdit->setVisible(false);
    compJaccardTable->setVisible(false);
    compJaccardCopyButton->setVisible(false);
    compJaccardTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
}

//
// Function that looks for the available motifs
//
void CSDesktop::findAvailableMotifs()
{
    DIR* bulyk_dir  = opendir (BULYK_MATRICES);
    DIR* jaspar_dir = opendir (JASPAR_MATRICES);

    struct dirent *ep;
    string tempMotif;

    if (bulyk_dir != NULL)
    {
        while ((ep = readdir (bulyk_dir))) {
            if(ep->d_name[0] != '.') {
                tempMotif = ep->d_name;
                QString motifName = tempMotif.substr(0, tempMotif.find_first_of("_")).c_str();

                if(ui->findmotifBulykMotifComboBox->findText(motifName) == -1) {
                    ui->findmotifBulykMotifComboBox->addItem(motifName);
                }
            }
        }
        closedir (bulyk_dir);
    }
    else {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                              | Qt::WindowMinimizeButtonHint);
        msgBox.setText(tr("Cannot open the BULYK_MATRICES directory."));
        msgBox.exec();
    }

    if (jaspar_dir != NULL)
    {
        while ((ep = readdir (jaspar_dir))) {
            if(ep->d_name[0] != '.') {
                tempMotif = ep->d_name;
                QString motifName = tempMotif.substr(0, tempMotif.find_first_of(".")).c_str();

                if(ui->findmotifJasparMotifComboBox->findText(motifName) == -1) {
                    ui->findmotifJasparMotifComboBox->addItem(motifName);
                }
            }
        }
        closedir (jaspar_dir);
    }
    else {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                              | Qt::WindowMinimizeButtonHint);
        msgBox.setText(tr("Cannot not open the JASPAR_MATRICES directory."));
        msgBox.exec();
    }
}

//
// Function that looks for the available pathways
//
void CSDesktop::findAvailablePathways()
{
    QString tmpSpecies = ui->findPathwaySpeciesComboBox->currentText();

    QString tmpDatabase = ui->findPathwaySelectDatabaseComboBox->currentText();

    QFile gefile;

    if(tmpSpecies == "Homo sapiens (hg18)") {
        if(tmpDatabase == "Gene Ontology (GO)") {
            gefile.setFileName(HUMAN_GO_PATHWAYS);
        }
        else if(tmpDatabase == "BioCarta") {
            gefile.setFileName(HUMAN_BIOCARTA_PATHWAYS);
        }
        else if(tmpDatabase == "KEGG") {
            gefile.setFileName(HUMAN_KEGG_PATHWAYS);
        }
        else if(tmpDatabase == "Human Protein Reference Database") {
            gefile.setFileName(HUMAN_HPRD_PATHWAYS);
        }
        else if(tmpDatabase == "SignatureDB") {
            gefile.setFileName(HUMAN_STAUDT_PATHWAYS);
        }
        else if(tmpDatabase == "Reactome") {
            gefile.setFileName(HUMAN_REACTOME_PATHWAYS);
        }
    }
    else if(tmpSpecies == "Mus musculus (mm9)") {
        if(tmpDatabase == "Gene Ontology (GO)") {
            gefile.setFileName(MOUSE_GO_PATHWAYS);
        }
    }
    else if(tmpSpecies == "Drosophila melanogaster (dm3)") {
        if(tmpDatabase == "Gene Ontology (GO)") {
            gefile.setFileName(DROSOPH_GO_PATHWAYS);
        }
    }
    else if(tmpSpecies == "Saccharomyces cerevisiae (sacCer2)") {
        if(tmpDatabase == "Gene Ontology (GO)") {
            gefile.setFileName(SACCER_GO_PATHWAYS);
        }
    }

    if (!gefile.open(QIODevice::ReadOnly | QIODevice::Text))
        return;

    QTextStream in(&gefile);

    while (!in.atEnd())
    {
        QString gline           = in.readLine();

        QStringList lineList    = gline.split("\t", QString::SkipEmptyParts);

        ui->findPathwaySelectPathwayComboBox->addItem(lineList.value(0)+" "+lineList.value(1));
    }

    gefile.close();
}

//
// Function that looks for the available ENCODE datasets
//
void CSDesktop::findAvailableEncodeDatasets(QString gnm)
{

    DIR* encode_dir;

    if(gnm == "hg18") {
        encode_dir  = opendir (HG18_ENCODE_DATASETS);
    }
    else if (gnm == "hg19") {
        encode_dir  = opendir (HG19_ENCODE_DATASETS);
    }

    struct dirent *ep;
    string tmpName;

    if (encode_dir != NULL)
    {
        while ((ep = readdir (encode_dir))) {
            if(ep->d_name[0] != '.'){
                tmpName = ep->d_name;
                QString name = tmpName.substr(0, tmpName.find_first_of(".")).c_str();

                if(!(name.compare("ENCODE_TERMS") == 0)) {
                    if(ui->encodeSelectDatasetComboBox->findText(name) == -1) {
                        ui->encodeSelectDatasetComboBox->addItem(name);
                    }
                }
            }
        }
        closedir (encode_dir);
    }
    else {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                              | Qt::WindowMinimizeButtonHint);
        msgBox.setText(tr("Cannot open the ENCODE_DATASETS directory. \nIf you want to use this module you must download the ENCODE datasets (read INSTALL file)."));
        msgBox.exec();
    }
}

//
// Functions for the info buttons
//
void CSDesktop::on_selectChipFolderInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the folder that contains the ChIP reads per chromosome files."));
    msgBox.exec();
}

void CSDesktop::on_selectInputFolderInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the folder that contains the INPUT reads per chromosome files."));
    msgBox.exec();
}

void CSDesktop::on_peakDetectionFormatInfoButton_clicked()
{
    FormatsDialog *f = new FormatsDialog(this);
    f->show();
}

void CSDesktop::on_peakDetectionSpeciesInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("This option indicates the species. The available species are: Homo sapiens, Mus musculus, Drosophila melanogaster and Saccharomyces cerevisiae."));
    msgBox.exec();
}

void CSDesktop::on_peakDetectionFoldInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("This option indicates the fold ratio: how much higher ChIP peaks should be compared to input DNA peaks. Float values allowed (e.g., 2, 2.5, 5)."));
    msgBox.exec();
}

void CSDesktop::on_peakDetectionThresholdInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("This option indicates the negative log p-value (ratio) threshold for peaks. Threshold set to 15 means 10^-15. Integer values allowed (e.g., 5, 10, 15)."));
    msgBox.exec();
}

void CSDesktop::on_peakDetectionFragLenInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("This option indicates the length of the fragments whose extremities have been sequenced. Integer values allowed (e.g., 170, 250)."));
    msgBox.exec();
}

void CSDesktop::on_peakDetectionMinLenInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("This option indicates the minimum peak width. Integer values allowed (e.g., 50, 100)."));
    msgBox.exec();
}

void CSDesktop::on_peakDetectionMinDistInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("This option indicates the minimum distance between peaks (otherwises merges subpeaks). Integer values allowed (e.g., 50, 100)."));
    msgBox.exec();
}

void CSDesktop::on_peakDetectionMinPeakHeightInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("This option sets the threshold for the maximum peak height, estimated by the reads count at the peak summit. Integer values allowed (e.g., 5, 10, 20)."));
    msgBox.exec();
}

void CSDesktop::on_peakDetectionUniqueInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("This option indicates removing clonal reads (when several identical reads map to the same exact position in the genome. \n Binary values allowed (i.e., 0 or 1)."));
    msgBox.exec();
}

void CSDesktop::on_loadFormatInfoButton_clicked()
{
    FormatsDialog *f = new FormatsDialog(this);
    f->show();
}

void CSDesktop::on_peakDetectionOutputFileInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Write the name for the output file that will contain the detected peaks."));
    msgBox.exec();
}

void CSDesktop::on_genesSummaryDatabaseInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the genes annotation database.\n refSeq, Ensembl, AceView or UCSCGenes."));
    msgBox.exec();
}

void CSDesktop::on_genesSummarySpeciesInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("This option indicates the species. The available species are: Homo sapiens, Mus musculus, Drosophila melanogaster and Saccharomyces cerevisiae."));
    msgBox.exec();
}

void CSDesktop::on_genesSummaryLenUInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the upstream length from the TSS."));
    msgBox.exec();
}

void CSDesktop::on_genesSummaryLenDInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the downstream length from the TSS."));
    msgBox.exec();
}

void CSDesktop::on_genesSummarySelectPeaksInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the file that contains the peaks."));
    msgBox.exec();
}

void CSDesktop::on_genesSummaryOutputFileInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Write the name for the output file."));
    msgBox.exec();
}

void CSDesktop::on_rnaGenesSelectPeaksInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the file that contains the peaks."));
    msgBox.exec();
}

void CSDesktop::on_rnaGenesOutputFileInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Write the name for the output file."));
    msgBox.exec();
}

void CSDesktop::on_encodeSelectPeaksInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the file that contains the peaks."));
    msgBox.exec();
}

void CSDesktop::on_encodePrefixFileInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Write the prefix for the output file."));
    msgBox.exec();
}

void CSDesktop::on_encodeWriteDatasetInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Write the dataset name or part of it. For example, K562, NRSF."));
    msgBox.exec();
}

void CSDesktop::on_encodeSelectDatasetInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the desired ChIP-seq dataset."));
    msgBox.exec();
}

void CSDesktop::on_encodeIterationsInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the number of random background files, used to estimate the z-score."));
    msgBox.exec();
}

void CSDesktop::on_encodeALLInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Check this option, if you want to compare your peaks against all ENCODE datasets."));
    msgBox.exec();
}

void CSDesktop::on_peaksTrackSelectPeaksInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the file that contains the peaks."));
    msgBox.exec();
}

void CSDesktop::on_readsTrackSelectReadsInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the folder that contains the reads."));
    msgBox.exec();
}

void CSDesktop::on_peaksTrackNameInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Write the name of the track \n (it will appear in the Genome Browser)."));
    msgBox.exec();
}

void CSDesktop::on_readsTrackNameInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Write the name of the track \n (it will appear in the Genome Browser)."));
    msgBox.exec();
}

void CSDesktop::on_readsTrackOutputFileInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Write the name for the output file."));
    msgBox.exec();
}

void CSDesktop::on_peaksTrackOutputFileInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Write the name for the output file."));
    msgBox.exec();
}

void CSDesktop::on_readsTrackUniqueInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("This option indicates removing clonal reads (when several identical reads map to the same exact position in the genome. \n Binary values allowed (i.e., 0 or 1)."));
    msgBox.exec();
}

void CSDesktop::on_genomicDistSelectPeaksInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the file that contains the peaks."));
    msgBox.exec();
}

void CSDesktop::on_genomicDistOutputFileInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Write the name for the output file."));
    msgBox.exec();
}

void CSDesktop::on_genomicDistDatabaseInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the genes annotation database.\n refSeq, Ensembl, AceView or UCSCGenes."));
    msgBox.exec();
}

void CSDesktop::on_genomicDistSpeciesInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("This option indicates the species. The available species are: Homo sapiens, Mus musculus, Drosophila melanogaster and Saccharomyces cerevisiae."));
    msgBox.exec();
}

void CSDesktop::on_genomicDistLenPInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the upstream and downstream length from the TSS."));
    msgBox.exec();
}

void CSDesktop::on_genomicDistLenDWInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the upstream and downstream length from the TES."));
    msgBox.exec();
}

void CSDesktop::on_genomicDistMindistawayInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the minimum/maxiumum distance away from known genes that the distal (intergenic) peaks should be."));
    msgBox.exec();
}

void CSDesktop::on_genomicDistTypeInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select between a gene-based or a peak-based genomic distribution."));
    msgBox.exec();
}

void CSDesktop::on_repeatsSelectPeaksInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the file that contains the peaks."));
    msgBox.exec();
}

void CSDesktop::on_repeatsOutputFileInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Write the name for the output file."));
    msgBox.exec();
}

void CSDesktop::on_cpgIslandsSelectPeaksInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the file that contains the peaks."));
    msgBox.exec();
}

void CSDesktop::on_cpgIslandsOutputFileInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Write the name for the output file."));
    msgBox.exec();
}

void CSDesktop::on_segDuplicatesSelectPeaksInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the file that contains the peaks."));
    msgBox.exec();
}

void CSDesktop::on_segDuplicatesOutputFileInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Write the name for the output file."));
    msgBox.exec();
}

void CSDesktop::on_findmotifSelectPeaksInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the file that contains the peaks."));
    msgBox.exec();
}

void CSDesktop::on_findmotifOutputFileInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Write the name for the output file."));
    msgBox.exec();
}

void CSDesktop::on_findmotifGenomeInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the genome file."));
    msgBox.exec();
}

void CSDesktop::on_findmotifNameInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the desired BULYK or JASPAR motif."));
    msgBox.exec();
}

void CSDesktop::on_findmotifSequenceInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Examples of allowed sequences are: TCCTAGA, TC[AT]CCGG, [CG][TA]TTTCGAT."));
    msgBox.exec();
}

void CSDesktop::on_findmotifScoreInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("This option indicates the motif score threshold, in standard deviations below the average. Threshold set to 1 is the default. Threshold set to 0 can be used for higher affinity matches."));
    msgBox.exec();
}

void CSDesktop::on_fireSelectPeaksInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the file that contains the peaks."));
    msgBox.exec();
}

void CSDesktop::on_fireSelectPeaksFolderInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the folder that contains the peaks files. Use this in order to run FIRE on multiple categories of peaks."));
    msgBox.exec();
}

void CSDesktop::on_fireGenomeInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the genome file."));
    msgBox.exec();
}

void CSDesktop::on_fireSpeciesInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the species."));
    msgBox.exec();
}

void CSDesktop::on_fireRandmodeInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the way to create random peaks. (1) Completely random sequences on the same chromosome for each peak. (2) Adjacent sequences to each peak. (3) Using 1st order Markov model sequences learnt on peaks"));
    msgBox.exec();
}

void CSDesktop::on_fireSeedInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Check this option to use the default seed for random regions."));
    msgBox.exec();
}

void CSDesktop::on_consSelectPeaksInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the file that contains the peaks."));
    msgBox.exec();
}

void CSDesktop::on_consOutputFileInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Write the name for the output file."));
    msgBox.exec();
}

void CSDesktop::on_consConsDirInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the conservation scores folder. Can be either mammalsPlacental or primates."));
    msgBox.exec();
}

void CSDesktop::on_consFormatInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the conservation scores format."));
    msgBox.exec();
}

void CSDesktop::on_consThresholdInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Set the threshold for the conservation score. Peaks with score greater than this are printed in the .filter file."));
    msgBox.exec();
}

void CSDesktop::on_consCategoryInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the species category (placental or primates)."));
    msgBox.exec();
}

void CSDesktop::on_consOutputRandomInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Write the name for the random regions output file."));
    msgBox.exec();
}

void CSDesktop::on_consMethodInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the method (phastCons or phyloP)."));
    msgBox.exec();
}

void CSDesktop::on_consMakeRandInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select to extract random regions and estimate their conservation scores."));
    msgBox.exec();
}

void CSDesktop::on_consShowProfilesInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select to estimate the average conservation profile for a peak. Set the window size for estimating the average conservation profile. Default is 10bp. If not selected it estimates the average/min/max conservation for each peak."));
    msgBox.exec();
}

void CSDesktop::on_consRandistInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the distance of random regions from the peaks. Default is 50000."));
    msgBox.exec();
}

void CSDesktop::on_consWindowSizeInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select if you want to estimate the conservation on regions around the peak summit. Set the distance around the peak summit. Default is 2kb at each direction."));
    msgBox.exec();
}

void CSDesktop::on_consSpeciesInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the species."));
    msgBox.exec();
}

void CSDesktop::on_compPeaksSelectPeaks1InfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the first peaks file to compare."));
    msgBox.exec();
}

void CSDesktop::on_compPeaksColumnHeaderCenter1InfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select if the file has the first column as ID, has header, or if you want to compare based on the peak center."));
    msgBox.exec();
}

void CSDesktop::on_compPeaksSelectPeaks2InfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the second peaks file to compare."));
    msgBox.exec();
}

void CSDesktop::on_compPeaksColumnHeaderCenter2InfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select if the file has the first column as ID, has header, or if you want to compare based on the peak center."));
    msgBox.exec();
}

void CSDesktop::on_compPeaksPrefixInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Write the prefix for the output file."));
    msgBox.exec();
}

void CSDesktop::on_compPeaksANDInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select if you want to find the peaks of file1 that overlap with the peaks of file2."));
    msgBox.exec();
}

void CSDesktop::on_compPeaksOvInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select if you want to show the overlapping peaks from file2."));
    msgBox.exec();
}

void CSDesktop::on_compPeaksUnInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select if you want to show the union of overlapping peaks."));
    msgBox.exec();
}

void CSDesktop::on_compPeaksDesc1InfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select if you want to show all columns of file1."));
    msgBox.exec();
}

void CSDesktop::on_compPeaksANDNOTInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select if you want to find the peaks of file1 that do not overlap with the peaks of file2."));
    msgBox.exec();
}

void CSDesktop::on_compPeaksDesc2InfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select if you want to show all columns of file2."));
    msgBox.exec();
}

void CSDesktop::on_compPeaksIterationsInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the number of random background files, used to estimate the z-score."));
    msgBox.exec();
}

void CSDesktop::on_compPeaksRandomModeInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the way to create random peaks. (1) Completely random sequences on the same chromosome for each peak. (2) Adjacent sequences to each peak. (3) Using 1st order Markov model sequences learnt on peaks"));
    msgBox.exec();
}

void CSDesktop::on_compGenesSelectFile1InfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the first genes file to compare."));
    msgBox.exec();
}

void CSDesktop::on_compGenesSelectFile2InfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the second genes file to compare."));
    msgBox.exec();
}

void CSDesktop::on_compGenesOutputFileInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Write the name for the output file."));
    msgBox.exec();
}

void CSDesktop::on_compGenesRemoveDuplicatesInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select if you don't want to keep duplicate genes."));
    msgBox.exec();
}

void CSDesktop::on_findPathwaySelectPeaksInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the file that contains the peaks."));
    msgBox.exec();
}

void CSDesktop::on_findPathwayOutputFileInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Write the name for the output file."));
    msgBox.exec();
}

void CSDesktop::on_findPathwaySpeciesInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("This option indicates the species. The available species are: Homo sapiens, Mus musculus, Drosophila melanogaster and Saccharomyces cerevisiae."));
    msgBox.exec();
}

void CSDesktop::on_findPathwaySelectDatabaseInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the pathways database.\n Gene Ontology, Biocarta, KEGG, Human Reference Protein Database, SignatureDB, or Reactome."));
    msgBox.exec();
}

void CSDesktop::on_findPathwayWritePathwayInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Write the desired pathway name or id. Examples: GO:0006915, apoptosis, glykolysis."));
    msgBox.exec();
}

void CSDesktop::on_findPathwaySelectPathwayInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the desired pathway."));
    msgBox.exec();
}

void CSDesktop::on_pageSelectPeaksInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the file that contains the peaks."));
    msgBox.exec();
}

void CSDesktop::on_pageOutputFileInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Write the name for the output file(s)."));
    msgBox.exec();
}

void CSDesktop::on_pageSpeciesInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("This option indicates the species. The available species are: \nHomo sapiens, Mus musculus, Drosophila melanogaster and Saccharomyces cerevisiae."));
    msgBox.exec();
}

void CSDesktop::on_pageDatabaseInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the genes annotation database.\nrefSeq, Ensembl, AceView or UCSCGenes. \nThis is needed in order to find the promoters of genes that contain at least one of the peaks."));
    msgBox.exec();
}


void CSDesktop::on_pageSelectGenepartsInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select which genes you want to include in the analysis. \nFor example: genes with peaks in their PROMOTERS, EXONS, INTRONS, etc."));
    msgBox.exec();
}

void CSDesktop::on_pageSelectDatabaseInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the pathways database.\nGene Ontology, Biocarta, KEGG, Human Reference Protein Database, SignatureDB, or Reactome."));
    msgBox.exec();
}

void CSDesktop::on_jaccardSelectFileInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select the file that contains the names and labels of the peak files. For example: \nBcl6Ly1_peaks.txt	Bcl6Ly1 \nBCOR_peaks.txt BCOR \nh3k4me3_peaks.txt	h3k4me3."));
    msgBox.exec();
}

void CSDesktop::on_jaccardOutputFileInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Write the name for the output file."));
    msgBox.exec();
}

void CSDesktop::on_findPathwaysSelectGenepartsInfoButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                          | Qt::WindowMinimizeButtonHint);
    msgBox.setText(tr("Select which genes you want to include in the analysis. \nFor example: genes with peaks in their PROMOTERS, EXONS, INTRONS, etc."));
    msgBox.exec();
}

//
// Functions for the MenuActions
//
void CSDesktop::on_aboutAction_triggered()
{
    AboutDialog *a = new AboutDialog(this);
    a->show();
}

void CSDesktop::on_createTracksAction_triggered()
{
    ui->toolBox->setCurrentIndex(0);

    ui->stackedWidget->setCurrentIndex(5);
    ui->createTracksTabWidget->setCurrentIndex(0);

    setLoadRawButtonBold(false);
    setPeakDetectionButtonBold(false);
    setGenesSummaryButtonBold(false);
    setCreateTracksButtonBold(true);
    setGenomicDistButtonBold(false);
    setRepeatsButtonBold(false);
    setCpgIslandsButtonBold(false);
    setSegDuplicatesButtonBold(false);
    setFindMotifButtonBold(false);
    setConsButtonBold(false);
    setCompPeaksButtonBold(false);
    setCompGenesButtonBold(false);

    ui->peaksTrackNameLineEdit->setText("ChIPseeqer peaks");
    ui->readsTrackNameLineEdit->setText("Smoothed reads");
}

void CSDesktop::on_loadDataAction_triggered()
{
    ui->toolBox->setCurrentIndex(0);

    ui->stackedWidget->setCurrentIndex(1);
    ui->loadTabWidget->setCurrentIndex(0);

    setLoadRawButtonBold(true);
    setPeakDetectionButtonBold(false);
    setGenesSummaryButtonBold(false);
    setCreateTracksButtonBold(false);
    setGenomicDistButtonBold(false);
    setRepeatsButtonBold(false);
    setCpgIslandsButtonBold(false);
    setSegDuplicatesButtonBold(false);
    setFindMotifButtonBold(false);
    setConsButtonBold(false);
    setCompPeaksButtonBold(false);
    setCompGenesButtonBold(false);
}

void CSDesktop::on_peakDetectionAction_2_triggered()
{
    ui->toolBox->setCurrentIndex(0);

    ui->stackedWidget->setCurrentIndex(2);
    ui->peakDetectionTabWidget->setCurrentIndex(0);

    setLoadRawButtonBold(false);
    setPeakDetectionButtonBold(true);
    setGenesSummaryButtonBold(false);
    setCreateTracksButtonBold(false);
    setGenomicDistButtonBold(false);
    setRepeatsButtonBold(false);
    setCpgIslandsButtonBold(false);
    setSegDuplicatesButtonBold(true);
    setFindMotifButtonBold(false);
    setConsButtonBold(false);
    setCompPeaksButtonBold(false);
    setCompGenesButtonBold(false);
}

void CSDesktop::on_genesSummaryAction_triggered()
{
    ui->toolBox->setCurrentIndex(0);

    ui->stackedWidget->setCurrentIndex(3);
    ui->genesSummaryTabWidget->setCurrentIndex(0);

    setLoadRawButtonBold(false);
    setPeakDetectionButtonBold(false);
    setGenesSummaryButtonBold(true);
    setCreateTracksButtonBold(false);
    setGenomicDistButtonBold(false);
    setRepeatsButtonBold(false);
    setCpgIslandsButtonBold(false);
    setSegDuplicatesButtonBold(false);
    setFindMotifButtonBold(false);
    setConsButtonBold(false);
    setCompPeaksButtonBold(false);
    setCompGenesButtonBold(false);
}

void CSDesktop::on_genomicDistributionAction_triggered()
{
    ui->toolBox->setCurrentIndex(0);

    ui->stackedWidget->setCurrentIndex(4);
    ui->genomicDistTabWidget->setCurrentIndex(0);

    setLoadRawButtonBold(false);
    setPeakDetectionButtonBold(false);
    setGenesSummaryButtonBold(false);
    setCreateTracksButtonBold(false);
    setGenomicDistButtonBold(true);
    setRepeatsButtonBold(false);
    setCpgIslandsButtonBold(false);
    setSegDuplicatesButtonBold(false);
    setFindMotifButtonBold(false);
    setConsButtonBold(false);
    setCompPeaksButtonBold(false);
    setCompGenesButtonBold(false);
}

void CSDesktop::on_repMaskerAction_triggered()
{
    ui->toolBox->setCurrentIndex(1);

    ui->stackedWidget->setCurrentIndex(6);
    ui->repeatsTabWidget->setCurrentIndex(0);

    setLoadRawButtonBold(false);
    setPeakDetectionButtonBold(false);
    setGenesSummaryButtonBold(false);
    setCreateTracksButtonBold(false);
    setGenomicDistButtonBold(false);
    setRepeatsButtonBold(true);
    setCpgIslandsButtonBold(false);
    setSegDuplicatesButtonBold(false);
    setFindMotifButtonBold(false);
    setConsButtonBold(false);
    setCompPeaksButtonBold(false);
    setCompGenesButtonBold(false);
}

void CSDesktop::on_cpgIslandsAction_triggered()
{
    ui->toolBox->setCurrentIndex(1);

    ui->stackedWidget->setCurrentIndex(7);
    ui->cpgIslandsTabWidget->setCurrentIndex(0);

    setLoadRawButtonBold(false);
    setPeakDetectionButtonBold(false);
    setGenesSummaryButtonBold(false);
    setCreateTracksButtonBold(false);
    setGenomicDistButtonBold(false);
    setRepeatsButtonBold(false);
    setCpgIslandsButtonBold(true);
    setSegDuplicatesButtonBold(false);
    setFindMotifButtonBold(false);
    setConsButtonBold(false);
    setCompPeaksButtonBold(false);
    setCompGenesButtonBold(false);
}

void CSDesktop::on_segDupsAction_triggered()
{
    ui->toolBox->setCurrentIndex(1);

    ui->stackedWidget->setCurrentIndex(8);
    ui->segDuplicatesTabWidget->setCurrentIndex(0);

    setLoadRawButtonBold(false);
    setPeakDetectionButtonBold(false);
    setGenesSummaryButtonBold(false);
    setCreateTracksButtonBold(false);
    setGenomicDistButtonBold(false);
    setRepeatsButtonBold(false);
    setCpgIslandsButtonBold(false);
    setSegDuplicatesButtonBold(true);
    setFindMotifButtonBold(false);
    setConsButtonBold(false);
    setCompPeaksButtonBold(false);
    setCompGenesButtonBold(false);
}

void CSDesktop::on_findMotifMenu_triggered()
{
    ui->toolBox->setCurrentIndex(2);

    ui->stackedWidget->setCurrentIndex(9);
    ui->repeatsTabWidget->setCurrentIndex(0);

    setLoadRawButtonBold(false);
    setPeakDetectionButtonBold(false);
    setGenesSummaryButtonBold(false);
    setCreateTracksButtonBold(false);
    setGenomicDistButtonBold(false);
    setRepeatsButtonBold(false);
    setCpgIslandsButtonBold(false);
    setSegDuplicatesButtonBold(false);
    setFindMotifButtonBold(true);
    setConsButtonBold(false);
    setCompPeaksButtonBold(false);
    setCompGenesButtonBold(false);
}

void CSDesktop::on_comparePeaksMenu_triggered()
{
    ui->toolBox->setCurrentIndex(5);

    ui->stackedWidget->setCurrentIndex(15);
    ui->compPeaksTabWidget->setCurrentIndex(0);

    setLoadRawButtonBold(false);
    setPeakDetectionButtonBold(false);
    setGenesSummaryButtonBold(false);
    setCreateTracksButtonBold(false);
    setGenomicDistButtonBold(false);
    setRepeatsButtonBold(false);
    setCpgIslandsButtonBold(false);
    setSegDuplicatesButtonBold(false);
    setFindMotifButtonBold(false);
    setConsButtonBold(false);
    setCompPeaksButtonBold(true);
    setCompGenesButtonBold(false);
}

void CSDesktop::on_compareGenesMenu_triggered()
{
    ui->toolBox->setCurrentIndex(5);

    ui->stackedWidget->setCurrentIndex(16);
    ui->compGenesTabWidget->setCurrentIndex(0);

    setLoadRawButtonBold(false);
    setPeakDetectionButtonBold(false);
    setGenesSummaryButtonBold(false);
    setCreateTracksButtonBold(false);
    setGenomicDistButtonBold(false);
    setRepeatsButtonBold(false);
    setCpgIslandsButtonBold(false);
    setSegDuplicatesButtonBold(false);
    setFindMotifButtonBold(false);
    setConsButtonBold(false);
    setCompPeaksButtonBold(false);
    setCompGenesButtonBold(true);
}

//
// Functions for the ToolButtons
//
void CSDesktop::on_selectChipRawFolderToolButton_clicked()
{
    QFileDialog *fd = new QFileDialog(this);
    fd->setWindowTitle(tr("Select ChIP folder"));
    fd->setParent(this);
    fd->setFileMode(QFileDialog::DirectoryOnly);
    fd->setViewMode(QFileDialog::List);
    fd->show();
    QStringList fileNames;
    if (fd->exec()) {
        fileNames = fd->selectedFiles();
        for(int i = 0; i < fileNames.size(); ++i)
        {
            ui->selectChipRawFolderLineEdit->setText(fileNames.at(i));
        }
    }
}

void CSDesktop::on_selectInputRawFolderToolButton_clicked()
{
    QFileDialog *fd = new QFileDialog(this);
    fd->setWindowTitle(tr("Select INPUT folder"));
    fd->setParent(this);
    fd->setFileMode(QFileDialog::DirectoryOnly);
    fd->setViewMode(QFileDialog::List);
    fd->show();
    QStringList fileNames;
    if (fd->exec()) {
        fileNames = fd->selectedFiles();
        for(int i = 0; i < fileNames.size(); ++i)
        {
            ui->selectInputRawFolderLineEdit->setText(fileNames.at(i));
        }
    }
}

void CSDesktop::on_selectChipFolderToolButton_clicked()
{
    QFileDialog *fd = new QFileDialog(this);
    fd->setWindowTitle(tr("Select ChIP folder"));
    fd->setParent(this);
    fd->setFileMode(QFileDialog::DirectoryOnly);
    fd->setViewMode(QFileDialog::List);
    fd->show();
    QStringList fileNames;
    if (fd->exec()) {
        fileNames = fd->selectedFiles();
        for(int i = 0; i < fileNames.size(); ++i)
        {
            ui->selectChipFolderLineEdit->setText(fileNames.at(i));
        }
        ui->peakDetectionOutputFileLineEdit->setText(fileNames.at(0)+"/TF_targets.txt");
    }
}

void CSDesktop::on_selectInputFolderToolButton_clicked()
{
    QFileDialog *fd = new QFileDialog(this);
    fd->setWindowTitle(tr("Select INPUT folder"));
    fd->setParent(this);
    fd->setFileMode(QFileDialog::DirectoryOnly);
    fd->setViewMode(QFileDialog::List);
    fd->show();
    QStringList fileNames;
    if (fd->exec()) {
        fileNames = fd->selectedFiles();
        for(int i = 0; i < fileNames.size(); ++i)
        {
            ui->selectInputFolderLineEdit->setText(fileNames.at(i));
        }
    }
}

void CSDesktop::on_genesSummarySelectPeaksToolButton_clicked()
{
    QFileDialog *fd = new QFileDialog(this);
    fd->setWindowTitle(tr("Select peaks file"));
    fd->setParent(this);
    fd->setFileMode(QFileDialog::ExistingFile);
    fd->setViewMode(QFileDialog::List);
    fd->show();
    QStringList fileNames;
    if (fd->exec()) {
        fileNames = fd->selectedFiles();
        for(int i = 0; i < fileNames.size(); ++i)
        {
            ui->genesSummarySelectPeaksLineEdit->setText(fileNames.at(i));
        }
        ui->genesSummaryOutputFileLineEdit->setText(fileNames.at(0));
    }
}

void CSDesktop::on_peaksTrackSelectPeaksToolButton_clicked()
{
    QFileDialog *fd = new QFileDialog(this);
    fd->setWindowTitle(tr("Select peaks file"));
    fd->setParent(this);
    fd->setFileMode(QFileDialog::ExistingFile);
    fd->setViewMode(QFileDialog::List);
    fd->show();
    QStringList fileNames;
    if (fd->exec()) {
        fileNames = fd->selectedFiles();
        for(int i = 0; i < fileNames.size(); ++i)
        {
            ui->peaksTrackSelectPeaksLineEdit->setText(fileNames.at(i));
        }
        ui->peaksTrackOutputFileLineEdit->setText(fileNames.at(0));
    }
}

void CSDesktop::on_readsTrackSelectReadsToolButton_clicked()
{
    QFileDialog *fd = new QFileDialog(this);
    fd->setWindowTitle(tr("Select reads folder"));
    fd->setParent(this);
    fd->setFileMode(QFileDialog::DirectoryOnly);
    fd->setViewMode(QFileDialog::List);
    fd->show();
    QStringList fileNames;
    if (fd->exec()) {
        fileNames = fd->selectedFiles();
        for(int i = 0; i < fileNames.size(); ++i)
        {
            ui->readsTrackSelectReadsLineEdit->setText(fileNames.at(i));
        }
        ui->readsTrackOutputFileLineEdit->setText(fileNames.at(0)+"/Smoothed_reads");
    }
}

void CSDesktop::on_genomicDistSelectPeaksToolButton_clicked()
{
    QFileDialog *fd = new QFileDialog(this);
    fd->setWindowTitle(tr("Select peaks file"));
    fd->setParent(this);
    fd->setFileMode(QFileDialog::ExistingFile);
    fd->setViewMode(QFileDialog::List);
    fd->show();
    QStringList fileNames;
    if (fd->exec()) {
        fileNames = fd->selectedFiles();
        for(int i = 0; i < fileNames.size(); ++i)
        {
            ui->genomicDistSelectPeaksLineEdit->setText(fileNames.at(i));
        }
        ui->genomicDistOutputFileLineEdit->setText(fileNames.at(0));
    }
}

void CSDesktop::on_rnaGenesSelectPeaksToolButton_clicked()
{
    QFileDialog *fd = new QFileDialog(this);
    fd->setWindowTitle(tr("Select peaks file"));
    fd->setParent(this);
    fd->setFileMode(QFileDialog::ExistingFile);
    fd->setViewMode(QFileDialog::List);
    fd->show();
    QStringList fileNames;
    if (fd->exec()) {
        fileNames = fd->selectedFiles();
        for(int i = 0; i < fileNames.size(); ++i)
        {
            ui->rnaGenesSelectPeaksLineEdit->setText(fileNames.at(i));
        }
        ui->rnaGenesOutputFileLineEdit->setText(fileNames.at(0));
    }
}

void CSDesktop::on_encodeSelectPeaksToolButton_clicked()
{
    QFileDialog *fd = new QFileDialog(this);
    fd->setWindowTitle(tr("Select peaks file"));
    fd->setParent(this);
    fd->setFileMode(QFileDialog::ExistingFile);
    fd->setViewMode(QFileDialog::List);
    fd->show();
    QStringList fileNames;
    if (fd->exec()) {
        fileNames = fd->selectedFiles();
        for(int i = 0; i < fileNames.size(); ++i)
        {
            ui->encodeSelectPeaksLineEdit->setText(fileNames.at(i));
        }
        //ui->encodeOutputFileLineEdit->setText(fileNames.at(0)+".ENCODE");
    }
}

void CSDesktop::on_repeatsSelectPeaksToolButton_clicked()
{
    QFileDialog *fd = new QFileDialog(this);
    fd->setWindowTitle(tr("Select peaks file"));
    fd->setParent(this);
    fd->setFileMode(QFileDialog::ExistingFile);
    fd->setViewMode(QFileDialog::List);
    fd->show();
    QStringList fileNames;
    if (fd->exec()) {
        fileNames = fd->selectedFiles();
        for(int i = 0; i < fileNames.size(); ++i)
        {
            ui->repeatsSelectPeaksLineEdit->setText(fileNames.at(i));
        }
        ui->repeatsOutputFileLineEdit->setText(fileNames.at(0));
    }
}

void CSDesktop::on_cpgIslandsSelectPeaksToolButton_clicked()
{
    QFileDialog *fd = new QFileDialog(this);
    fd->setWindowTitle(tr("Select peaks file"));
    fd->setParent(this);
    fd->setFileMode(QFileDialog::ExistingFile);
    fd->setViewMode(QFileDialog::List);
    fd->show();
    QStringList fileNames;
    if (fd->exec()) {
        fileNames = fd->selectedFiles();
        for(int i = 0; i < fileNames.size(); ++i)
        {
            ui->cpgIslandsSelectPeaksLineEdit->setText(fileNames.at(i));
        }
        ui->cpgIslandsOutputFileLineEdit->setText(fileNames.at(0));
    }
}

void CSDesktop::on_segDuplicatesSelectPeaksToolButton_clicked()
{
    QFileDialog *fd = new QFileDialog(this);
    fd->setWindowTitle(tr("Select peaks file"));
    fd->setParent(this);
    fd->setFileMode(QFileDialog::ExistingFile);
    fd->setViewMode(QFileDialog::List);
    fd->show();
    QStringList fileNames;
    if (fd->exec()) {
        fileNames = fd->selectedFiles();
        for(int i = 0; i < fileNames.size(); ++i)
        {
            ui->segDuplicatesSelectPeaksLineEdit->setText(fileNames.at(i));
        }
        ui->segDuplicatesOutputFileLineEdit->setText(fileNames.at(0));
    }
}

void CSDesktop::on_findmotifSelectPeaksToolButton_clicked()
{
    QFileDialog *fd = new QFileDialog(this);
    fd->setWindowTitle(tr("Select peaks file"));
    fd->setParent(this);
    fd->setFileMode(QFileDialog::ExistingFile);
    fd->setViewMode(QFileDialog::List);
    fd->show();
    QStringList fileNames;
    if (fd->exec()) {
        fileNames = fd->selectedFiles();
        for(int i = 0; i < fileNames.size(); ++i)
        {
            ui->findmotifSelectPeaksLineEdit->setText(fileNames.at(i));
        }
        ui->findmotifOutputFileLineEdit->setText(fileNames.at(0)+"_motif");
    }
}

void CSDesktop::on_findmotifGenomeToolButton_clicked()
{
    QFileDialog *fd = new QFileDialog(this);
    fd->setWindowTitle(tr("Select genome file"));
    fd->setParent(this);
    fd->setFileMode(QFileDialog::ExistingFile);
    fd->setViewMode(QFileDialog::List);
    fd->show();
    QStringList fileNames;
    if (fd->exec()) {
        fileNames = fd->selectedFiles();
        for(int i = 0; i < fileNames.size(); ++i)
        {
            ui->findmotifGenomeLineEdit->setText(fileNames.at(i));
        }
    }
}

void CSDesktop::on_fireSelectPeaksToolButton_clicked()
{
    QFileDialog *fd = new QFileDialog(this);
    fd->setWindowTitle(tr("Select peaks file"));
    fd->setParent(this);
    fd->setFileMode(QFileDialog::ExistingFile);
    fd->setViewMode(QFileDialog::List);
    fd->show();
    QStringList fileNames;
    if (fd->exec()) {
        fileNames = fd->selectedFiles();
        for(int i = 0; i < fileNames.size(); ++i)
        {
            ui->fireSelectPeaksLineEdit->setText(fileNames.at(i));
        }
    }

    ui->fireSelectPeaksFolderLineEdit->clear();
}

void CSDesktop::on_fireSelectPeaksFolderToolButton_clicked()
{
    QFileDialog *fd = new QFileDialog(this);
    fd->setWindowTitle(tr("Select peaks folder"));
    fd->setParent(this);
    fd->setFileMode(QFileDialog::DirectoryOnly);
    fd->setViewMode(QFileDialog::List);
    fd->show();
    QStringList fileNames;
    if (fd->exec()) {
        fileNames = fd->selectedFiles();
        for(int i = 0; i < fileNames.size(); ++i)
        {
            ui->fireSelectPeaksFolderLineEdit->setText(fileNames.at(i));
        }
    }

    ui->fireSelectPeaksLineEdit->clear();
}

void CSDesktop::on_fireGenomeToolButton_clicked()
{
    QFileDialog *fd = new QFileDialog(this);
    fd->setWindowTitle(tr("Select genome file"));
    fd->setParent(this);
    fd->setFileMode(QFileDialog::ExistingFile);
    fd->setViewMode(QFileDialog::List);
    fd->show();
    QStringList fileNames;
    if (fd->exec()) {
        fileNames = fd->selectedFiles();
        for(int i = 0; i < fileNames.size(); ++i)
        {
            ui->fireGenomeLineEdit->setText(fileNames.at(i));
        }
    }
}

void CSDesktop::on_consSelectPeaksToolButton_clicked()
{
    QFileDialog *fd = new QFileDialog(this);
    fd->setWindowTitle(tr("Select peaks file"));
    fd->setParent(this);
    fd->setFileMode(QFileDialog::ExistingFile);
    fd->setViewMode(QFileDialog::List);
    fd->show();
    QStringList fileNames;
    if (fd->exec()) {
        fileNames = fd->selectedFiles();
        for(int i = 0; i < fileNames.size(); ++i)
        {
            ui->consSelectPeaksLineEdit->setText(fileNames.at(i));
        }
        ui->consOutputFileLineEdit->setText(fileNames.at(0)+"_cons.txt");
        ui->consOutputRandomLineEdit->setText(fileNames.at(0)+"_randcons.txt");
    }
}

void CSDesktop::on_consConsDirToolButton_clicked()
{
    QFileDialog *fd = new QFileDialog(this);
    fd->setWindowTitle(tr("Select conservation scores folder"));
    fd->setParent(this);
    fd->setFileMode(QFileDialog::DirectoryOnly);
    fd->setViewMode(QFileDialog::List);
    fd->show();
    QStringList fileNames;
    if (fd->exec()) {
        fileNames = fd->selectedFiles();
        for(int i = 0; i < fileNames.size(); ++i)
        {
            ui->consConsDirLineEdit->setText(fileNames.at(i));
        }
    }
}

void CSDesktop::on_compPeaksSelectPeaks1ToolButton_clicked()
{
    QFileDialog *fd = new QFileDialog(this);
    fd->setWindowTitle(tr("Select peaks file"));
    fd->setParent(this);
    fd->setFileMode(QFileDialog::ExistingFile);
    fd->setViewMode(QFileDialog::List);
    fd->show();
    QStringList fileNames;
    if (fd->exec()) {
        fileNames = fd->selectedFiles();
        for(int i = 0; i < fileNames.size(); ++i)
        {
            ui->compPeaksSelectPeaks1LineEdit->setText(fileNames.at(i));
        }
        //ui->compPeaksOutputFileLineEdit->setText(fileNames.at(0)+"_comp.txt");
    }
}

void CSDesktop::on_compPeaksSelectPeaks2ToolButton_clicked()
{
    QFileDialog *fd = new QFileDialog(this);
    fd->setWindowTitle(tr("Select peaks file"));
    fd->setParent(this);
    fd->setFileMode(QFileDialog::ExistingFile);
    fd->setViewMode(QFileDialog::List);
    fd->show();
    QStringList fileNames;
    if (fd->exec()) {
        fileNames = fd->selectedFiles();
        for(int i = 0; i < fileNames.size(); ++i)
        {
            ui->compPeaksSelectPeaks2LineEdit->setText(fileNames.at(i));
        }
    }
}

void CSDesktop::on_compGenesSelectFile1ToolButton_clicked()
{
    QFileDialog *fd = new QFileDialog(this);
    fd->setWindowTitle(tr("Select genes file"));
    fd->setParent(this);
    fd->setFileMode(QFileDialog::ExistingFile);
    fd->setViewMode(QFileDialog::List);
    fd->show();
    QStringList fileNames;
    if (fd->exec()) {
        fileNames = fd->selectedFiles();
        for(int i = 0; i < fileNames.size(); ++i)
        {
            ui->compGenesSelectFile1LineEdit->setText(fileNames.at(i));
        }
        QString tmp = fileNames.at(0);
        ui->compGenesOutputFileLineEdit->setText(tmp.left(tmp.lastIndexOf("/")+1));
    }
}

void CSDesktop::on_compGenesSelectFile2ToolButton_clicked()
{
    QFileDialog *fd = new QFileDialog(this);
    fd->setWindowTitle(tr("Select genes file"));
    fd->setParent(this);
    fd->setFileMode(QFileDialog::ExistingFile);
    fd->setViewMode(QFileDialog::List);
    fd->show();
    QStringList fileNames;
    if (fd->exec()) {
        fileNames = fd->selectedFiles();
        for(int i = 0; i < fileNames.size(); ++i)
        {
            ui->compGenesSelectFile2LineEdit->setText(fileNames.at(i));
        }
    }
}

void CSDesktop::on_findPathwaySelectPeaksToolButton_clicked()
{
    QFileDialog *fd = new QFileDialog(this);
    fd->setWindowTitle(tr("Select peaks file"));
    fd->setParent(this);
    fd->setFileMode(QFileDialog::ExistingFile);
    fd->setViewMode(QFileDialog::List);
    fd->show();
    QStringList fileNames;
    if (fd->exec()) {
        fileNames = fd->selectedFiles();
        for(int i = 0; i < fileNames.size(); ++i)
        {
            ui->findPathwaySelectPeaksLineEdit->setText(fileNames.at(i));
        }
        ui->findPathwayOutputFileLineEdit->setText(fileNames.at(0));
    }
}

void CSDesktop::on_jaccardSelectFileToolButton_clicked()
{
    QFileDialog *fd = new QFileDialog(this);
    fd->setWindowTitle(tr("Select the file containing the names and labels of the peak files you want to compare."));
    fd->setParent(this);
    fd->setFileMode(QFileDialog::ExistingFile);
    fd->setViewMode(QFileDialog::List);
    fd->show();
    QStringList fileNames;
    if (fd->exec()) {
        fileNames = fd->selectedFiles();
        for(int i = 0; i < fileNames.size(); ++i)
        {
            ui->jaccardSelectFileLineEdit->setText(fileNames.at(i));
        }
        ui->jaccardOutputFileLineEdit->setText(fileNames.at(0)+"_Jaccard.txt");
    }
}

void CSDesktop::on_pageSelectPeaksToolButton_clicked()
{
    QFileDialog *fd = new QFileDialog(this);
    fd->setWindowTitle(tr("Select peaks file"));
    fd->setParent(this);
    fd->setFileMode(QFileDialog::ExistingFile);
    fd->setViewMode(QFileDialog::List);
    fd->show();
    QStringList fileNames;
    if (fd->exec()) {
        fileNames = fd->selectedFiles();
        for(int i = 0; i < fileNames.size(); ++i)
        {
            ui->pageSelectPeaksLineEdit->setText(fileNames.at(i));
        }
        ui->pageOutputFileLineEdit->setText(fileNames.at(0));
    }
}

//
// Functions that handle the radio buttons
//

void CSDesktop::on_findmotifBulykRadioButton_clicked()
{
    ui->findmotifJasparMotifComboBox->setEnabled(false);
    ui->findmotifBulykMotifComboBox->setEnabled(true);
}

void CSDesktop::on_findmotifJasparRadioButton_clicked()
{
    ui->findmotifJasparMotifComboBox->setEnabled(true);
    ui->findmotifBulykMotifComboBox->setEnabled(false);
}

void CSDesktop::on_findmotifNameRadioButton_clicked()
{
    ui->findmotifJasparMotifComboBox->setEnabled(false);
    ui->findmotifJasparRadioButton->setEnabled(true);
    ui->findmotifBulykMotifComboBox->setEnabled(true);
    ui->findmotifBulykRadioButton->setChecked(true);
    ui->findmotifBulykRadioButton->setEnabled(true);

    ui->findmotifSequenceLineEdit->setEnabled(false);
}

void CSDesktop::on_findmotifSequenceRadioButton_clicked()
{
    ui->findmotifJasparMotifComboBox->setEnabled(false);
    ui->findmotifJasparRadioButton->setEnabled(false);
    ui->findmotifBulykMotifComboBox->setEnabled(false);
    ui->findmotifBulykRadioButton->setEnabled(false);

    ui->findmotifSequenceLineEdit->setEnabled(true);
}

void CSDesktop::on_compPeaksANDRadioButton_clicked()
{
    ui->compPeaksOvPeaksCheckBox->setEnabled(true);
    ui->compPeaksUnPeaksCheckBox->setEnabled(true);
    ui->compPeaksDescPeaks1CheckBox->setEnabled(true);
    ui->compPeaksDescPeaks2CheckBox->setEnabled(false);
    compPeaksProcess.setShowOvPeaks(false);
    compPeaksProcess.setShowUnPeaks(false);
    compPeaksProcess.setShowDesc(false);
    compPeaksProcess.setCompType("and");
}

void CSDesktop::on_compPeaksANDNOTRadioButton_clicked()
{
    ui->compPeaksOvPeaksCheckBox->setEnabled(false);
    ui->compPeaksOvPeaksCheckBox->setChecked(false);
    ui->compPeaksUnPeaksCheckBox->setEnabled(false);
    ui->compPeaksUnPeaksCheckBox->setChecked(false);
    ui->compPeaksDescPeaks1CheckBox->setEnabled(false);
    ui->compPeaksDescPeaks1CheckBox->setChecked(false);
    ui->compPeaksDescPeaks2CheckBox->setEnabled(true);
    compPeaksProcess.setShowOvPeaks(false);
    compPeaksProcess.setShowUnPeaks(false);
    compPeaksProcess.setShowDesc(false);
    compPeaksProcess.setCompType("andnot");
}

void CSDesktop::on_findPathwayWritePathwayRadioButton_clicked()
{
    ui->findPathwaySelectPathwayComboBox->setEnabled(false);

    ui->findPathwayWritePathwayLineEdit->setEnabled(true);
}

void CSDesktop::on_findPathwaySelectPathwayRadioButton_clicked()
{    
    ui->findPathwayWritePathwayLineEdit->setEnabled(false);

    ui->findPathwaySelectPathwayComboBox->setEnabled(true);
}

void CSDesktop::on_encodeWriteDatasetRadioButton_clicked()
{
    ui->encodeSelectDatasetComboBox->setEnabled(false);

    ui->encodeWriteDatasetLineEdit->setEnabled(true);
}

void CSDesktop::on_encodeSelectDatasetRadioButton_clicked()
{
    ui->encodeSelectDatasetComboBox->setEnabled(true);

    ui->encodeWriteDatasetLineEdit->setEnabled(false);
}

//
// Functions that handle the check boxes
//

void CSDesktop::on_compPeaksOvPeaksCheckBox_clicked()
{
    if(ui->compPeaksOvPeaksCheckBox->isChecked()) {
        compPeaksProcess.setShowOvPeaks(true);
    }
    else {
        compPeaksProcess.setShowOvPeaks(false);
    }
}

void CSDesktop::on_compPeaksUnPeaksCheckBox_clicked()
{
    if(ui->compPeaksUnPeaksCheckBox->isChecked()) {
        compPeaksProcess.setShowUnPeaks(true);
    }
    else {
        compPeaksProcess.setShowUnPeaks(false);
    }
}

void CSDesktop::on_compPeaksDescPeaks1CheckBox_clicked()
{
    if(ui->compPeaksDescPeaks1CheckBox->isChecked()) {
        compPeaksProcess.setShowDesc(true);
    }
    else {
        compPeaksProcess.setShowDesc(false);
    }
}

void CSDesktop::on_compPeaksDescPeaks2CheckBox_clicked()
{
    if(ui->compPeaksDescPeaks2CheckBox->isChecked()) {
        compPeaksProcess.setShowDesc(true);
    }
    else {
        compPeaksProcess.setShowDesc(false);
    }
}

void CSDesktop::on_compPeaksID1CheckBox_clicked()
{
    if(ui->compPeaksID1CheckBox->isChecked()) {
        compPeaksProcess.setHasID1(true);
    }
    else {
        compPeaksProcess.setHasID1(false);
    }
}

void CSDesktop::on_compPeaksID2CheckBox_clicked()
{
    if(ui->compPeaksID2CheckBox->isChecked()) {
        compPeaksProcess.setHasID2(true);
    }
    else {
        compPeaksProcess.setHasID2(false);
    }
}

void CSDesktop::on_compPeaksHeader1CheckBox_clicked()
{
    if(ui->compPeaksHeader1CheckBox->isChecked()) {
        compPeaksProcess.setHasHeader1(true);
    }
    else {
        compPeaksProcess.setHasHeader1(false);
    }
}

void CSDesktop::on_compPeaksHeader2CheckBox_clicked()
{
    if(ui->compPeaksHeader2CheckBox->isChecked()) {
        compPeaksProcess.setHasHeader2(true);
    }
    else {
        compPeaksProcess.setHasHeader2(false);
    }
}

void CSDesktop::on_compPeaksCenter1CheckBox_clicked()
{
    if(ui->compPeaksCenter1CheckBox->isChecked()) {
        compPeaksProcess.setUseCenter1(true);
    }
    else {
        compPeaksProcess.setUseCenter1(false);
    }
}

void CSDesktop::on_compPeaksCenter2CheckBox_clicked()
{
    if(ui->compPeaksCenter2CheckBox->isChecked()) {
        compPeaksProcess.setUseCenter2(true);
    }
    else {
        compPeaksProcess.setUseCenter2(false);
    }
}

void CSDesktop::on_consMakeRandCheckBox_clicked()
{
    if(ui->consMakeRandCheckBox->isChecked()) {
        consProcess.setMakeRandom(true);
        ui->consOutputRandomLabel->setEnabled(true);
        ui->consOutputRandomLineEdit->setEnabled(true);
        ui->consRandistLabel->setEnabled(true);
        ui->consRandistSpinBox->setEnabled(true);
    }
    else {
        consProcess.setMakeRandom(false);
        ui->consOutputRandomLabel->setEnabled(false);
        ui->consOutputRandomLineEdit->setEnabled(false);
        ui->consRandistLabel->setEnabled(false);
        ui->consRandistSpinBox->setEnabled(false);
    }
}

void CSDesktop::on_consShowProfilesCheckBox_clicked()
{
    if(ui->consShowProfilesCheckBox->isChecked()) {
        consProcess.setShowProfiles(true);
        ui->consWindowSizeLabel->setEnabled(true);
        ui->consWindowSizeSpinBox->setEnabled(true);
        ui->consAroundSummitCheckBox->setEnabled(true);
        ui->consDistanceLabel->setEnabled(true);
        ui->consDistanceSpinBox->setEnabled(true);
    }
    else {
        consProcess.setShowProfiles(false);
        ui->consWindowSizeLabel->setEnabled(false);
        ui->consWindowSizeSpinBox->setEnabled(false);
        ui->consAroundSummitCheckBox->setEnabled(false);
        ui->consDistanceLabel->setEnabled(false);
        ui->consDistanceSpinBox->setEnabled(false);
    }
}

void CSDesktop::on_consAroundSummitCheckBox_clicked()
{
    if(ui->consAroundSummitCheckBox->isChecked()) {
        consProcess.setAroundSummit(true);
    }
    else {
        consProcess.setAroundSummit(false);
    }
}

//
// Functions that handle the clear buttons
//

void CSDesktop::on_loadClearButton_clicked()
{
    ui->selectChipRawFolderLineEdit->clear();
    ui->selectInputRawFolderLineEdit->clear();
}

void CSDesktop::on_peakDetectionClearButton_clicked()
{
    ui->selectChipFolderLineEdit->clear();
    ui->selectInputFolderLineEdit->clear();
    ui->peakDetectionOutputFileLineEdit->clear();
}

void CSDesktop::on_runSummaryClearButton_clicked()
{
    ui->genesSummarySelectPeaksLineEdit->clear();
    ui->genesSummaryOutputFileLineEdit->clear();
}

void CSDesktop::on_peaksTrackClearButton_clicked()
{
    ui->peaksTrackSelectPeaksLineEdit->clear();
    ui->peaksTrackNameLineEdit->clear();
}

void CSDesktop::on_readsTrackClearButton_clicked()
{
    ui->readsTrackNameLineEdit->clear();
    ui->readsTrackOutputFileLineEdit->clear();
    ui->readsTrackSelectReadsLineEdit->clear();
}

void CSDesktop::on_runGenomicDistClearButton_clicked()
{
    ui->genomicDistSelectPeaksLineEdit->clear();
    ui->genomicDistOutputFileLineEdit->clear();
}

void CSDesktop::on_runFindRNAGenesClearButton_clicked()
{
    ui->rnaGenesSelectPeaksLineEdit->clear();
    ui->rnaGenesOutputFileLineEdit->clear();
}

void CSDesktop::on_runEncodeClearButton_clicked()
{
    ui->encodeSelectPeaksLineEdit->clear();
    ui->encodePrefixFileLineEdit->clear();
    ui->encodeWriteDatasetLineEdit->clear();
}

void CSDesktop::on_runFindSegDuplicatesClearButton_clicked()
{
    ui->segDuplicatesSelectPeaksLineEdit->clear();
    ui->segDuplicatesOutputFileLineEdit->clear();
}

void CSDesktop::on_runFindCpgIslandsClearButton_clicked()
{
    ui->cpgIslandsSelectPeaksLineEdit->clear();
    ui->cpgIslandsOutputFileLineEdit->clear();
}

void CSDesktop::on_runFindRepeatsClearButton_clicked()
{
    ui->repeatsSelectPeaksLineEdit->clear();
    ui->repeatsOutputFileLineEdit->clear();
}

void CSDesktop::on_runFindmotifClearButton_clicked()
{
    ui->findmotifSelectPeaksLineEdit->clear();
    ui->findmotifOutputFileLineEdit->clear();
    ui->findmotifGenomeLineEdit->clear();
    ui->findmotifSequenceLineEdit->clear();
}

void CSDesktop::on_runFIREClearButton_clicked()
{
    ui->fireSelectPeaksLineEdit->clear();
    ui->fireSelectPeaksFolderLineEdit->clear();
    ui->fireGenomeLineEdit->clear();
}

void CSDesktop::on_runFindPathwayClearButton_clicked()
{
    ui->findPathwaySelectPeaksLineEdit->clear();
    ui->findPathwayOutputFileLineEdit->clear();
    ui->findPathwayWritePathwayLineEdit->clear();
    ui->findPathwaysDownstreamCheckBox->setChecked(false);
    ui->findPathwaysExonsCheckBox->setChecked(false);
    ui->findPathwaysIntergenicCheckBox->setChecked(false);
    ui->findPathwaysIntronsCheckBox->setChecked(false);
    ui->findPathwaysIntron1CheckBox->setChecked(false);
    ui->findPathwaysIntron2CheckBox->setChecked(false);
    ui->findPathwaysPromotersCheckBox->setChecked(true);
}

void CSDesktop::on_runPAGEClearButton_clicked()
{
    ui->pageSelectPeaksLineEdit->clear();
    ui->pageOutputFileLineEdit->clear();
    ui->pageDownstreamCheckBox->setChecked(false);
    ui->pageExonsCheckBox->setChecked(false);
    ui->pageIntergenicCheckBox->setChecked(false);
    ui->pageIntronsCheckBox->setChecked(false);
    ui->pageIntron1CheckBox->setChecked(false);
    ui->pageIntron2CheckBox->setChecked(false);
    ui->pagePromotersCheckBox->setChecked(true);
}

void CSDesktop::on_runConsClearButton_clicked()
{
    ui->consSelectPeaksLineEdit->clear();
    ui->consOutputFileLineEdit->clear();
    ui->consConsDirLineEdit->clear();
    ui->consOutputRandomLineEdit->clear();
}

void CSDesktop::on_runCompPeaksClearButton_clicked()
{
    ui->compPeaksSelectPeaks1LineEdit->clear();
    ui->compPeaksSelectPeaks2LineEdit->clear();
    ui->compPeaksPrefixFileLineEdit->clear();
    ui->compPeaksOvPeaksCheckBox->setChecked(false);
    ui->compPeaksUnPeaksCheckBox->setChecked(false);
    ui->compPeaksDescPeaks1CheckBox->setChecked(false);
    ui->compPeaksDescPeaks2CheckBox->setChecked(false);
    ui->compPeaksCenter1CheckBox->setChecked(false);
    ui->compPeaksCenter2CheckBox->setChecked(false);
    ui->compPeaksHeader1CheckBox->setChecked(false);
    ui->compPeaksHeader2CheckBox->setChecked(false);
    ui->compPeaksID1CheckBox->setChecked(false);
    ui->compPeaksID2CheckBox->setChecked(false);
}

void CSDesktop::on_runCompGenesClearButton_clicked()
{
    ui->compGenesSelectFile1LineEdit->clear();
    ui->compGenesSelectFile2LineEdit->clear();
    ui->compGenesOutputFileLineEdit->clear();
}

void CSDesktop::on_runJaccardClearButton_clicked()
{
    ui->jaccardSelectFileLineEdit->clear();
    ui->jaccardOutputFileLineEdit->clear();
}

//
// Functions that handle other buttons
//

void CSDesktop::on_openGenomeBrowserButton_clicked()
{
    QDesktopServices::openUrl(QUrl(tr("http://genome.ucsc.edu/cgi-bin/hgGateway")));
}

void CSDesktop::peakDetectionTableOpenBrowser(int row) {
    QTableWidgetItem *chr   = peakDetectionTable->item(row, 0);
    QTableWidgetItem *start = peakDetectionTable->item(row, 1);
    QTableWidgetItem *end   = peakDetectionTable->item(row, 2);
    // launch web browser here with the clicked coordinates
    QString address = "http://genome.ucsc.edu/cgi-bin/hgTracks?position="+chr->text()+"+"+start->text()+"+"+end->text()+"&Submit=submit";
    QDesktopServices::openUrl(QUrl(address));
}

void CSDesktop::summaryTableOpenBrowser(int row) {
    QTableWidgetItem *chr;
    QTableWidgetItem *start;
    QTableWidgetItem *end;

    if(summaryProcess.getDatabase() == "refSeq") {
        chr   = summaryTable->item(row, 2);
        start = summaryTable->item(row, 3);
        end   = summaryTable->item(row, 4);
    }
    else {
        chr   = summaryTable->item(row, 1);
        start = summaryTable->item(row, 2);
        end   = summaryTable->item(row, 3);
    }
    // launch web browser here with the clicked coordinates
    QString address = "http://genome.ucsc.edu/cgi-bin/hgTracks?position="+chr->text()+"+"+start->text()+"+"+end->text()+"&Submit=submit";
    QDesktopServices::openUrl(QUrl(address));
}

void CSDesktop::genomicDistTableOpenBrowser(int row) {
    QTableWidgetItem *chr   = genomicDistTable->item(row, 0);
    QTableWidgetItem *start = genomicDistTable->item(row, 1);
    QTableWidgetItem *end   = genomicDistTable->item(row, 2);
    // launch web browser here with the clicked coordinates
    QString address = "http://genome.ucsc.edu/cgi-bin/hgTracks?position="+chr->text()+"+"+start->text()+"+"+end->text()+"&Submit=submit";
    QDesktopServices::openUrl(QUrl(address));
}

void CSDesktop::rnaGenesTableOpenBrowser(int row) {
    QTableWidgetItem *chr   = rnaGenesTable->item(row, 0);
    QTableWidgetItem *start = rnaGenesTable->item(row, 1);
    QTableWidgetItem *end   = rnaGenesTable->item(row, 2);
    // launch web browser here with the clicked coordinates
    QString address = "http://genome.ucsc.edu/cgi-bin/hgTracks?position="+chr->text()+"+"+start->text()+"+"+end->text()+"&Submit=submit";
    QDesktopServices::openUrl(QUrl(address));
}

void CSDesktop::encodeTableOpenBrowser(int row) {
    QTableWidgetItem *chr   = encodeTable->item(row, 0);
    QTableWidgetItem *start = encodeTable->item(row, 1);
    QTableWidgetItem *end   = encodeTable->item(row, 2);
    // launch web browser here with the clicked coordinates
    QString address = "http://genome.ucsc.edu/cgi-bin/hgTracks?position="+chr->text()+"+"+start->text()+"+"+end->text()+"&Submit=submit";
    QDesktopServices::openUrl(QUrl(address));
}

void CSDesktop::repeatsTableOpenBrowser(int row) {
    QTableWidgetItem *chr   = repeatsTable->item(row, 0);
    QTableWidgetItem *start = repeatsTable->item(row, 1);
    QTableWidgetItem *end   = repeatsTable->item(row, 2);
    // launch web browser here with the clicked coordinates
    QString address = "http://genome.ucsc.edu/cgi-bin/hgTracks?position="+chr->text()+"+"+start->text()+"+"+end->text()+"&Submit=submit";
    QDesktopServices::openUrl(QUrl(address));
}

void CSDesktop::cpgIslandsTableOpenBrowser(int row) {
    QTableWidgetItem *chr   = cpgIslandsTable->item(row, 0);
    QTableWidgetItem *start = cpgIslandsTable->item(row, 1);
    QTableWidgetItem *end   = cpgIslandsTable->item(row, 2);
    // launch web browser here with the clicked coordinates
    QString address = "http://genome.ucsc.edu/cgi-bin/hgTracks?position="+chr->text()+"+"+start->text()+"+"+end->text()+"&Submit=submit";
    QDesktopServices::openUrl(QUrl(address));
}

void CSDesktop::segDuplicatesTableOpenBrowser(int row) {
    QTableWidgetItem *chr   = segDuplicatesTable->item(row, 0);
    QTableWidgetItem *start = segDuplicatesTable->item(row, 1);
    QTableWidgetItem *end   = segDuplicatesTable->item(row, 2);
    // launch web browser here with the clicked coordinates
    QString address = "http://genome.ucsc.edu/cgi-bin/hgTracks?position="+chr->text()+"+"+start->text()+"+"+end->text()+"&Submit=submit";
    QDesktopServices::openUrl(QUrl(address));
}

void CSDesktop::findMotifsTableOpenBrowser(int row) {
    QTableWidgetItem *chr   = findMotifsTable->item(row, 0);
    QTableWidgetItem *start = findMotifsTable->item(row, 1);
    QTableWidgetItem *end   = findMotifsTable->item(row, 2);
    // launch web browser here with the clicked coordinates
    QString address = "http://genome.ucsc.edu/cgi-bin/hgTracks?position="+chr->text()+"+"+start->text()+"+"+end->text()+"&Submit=submit";
    QDesktopServices::openUrl(QUrl(address));
}

void CSDesktop::findPathwaysTableOpenBrowser(int row) {
    QTableWidgetItem *chr   = findPathwaysTable->item(row, 0);
    QTableWidgetItem *start = findPathwaysTable->item(row, 1);
    QTableWidgetItem *end   = findPathwaysTable->item(row, 2);
    // launch web browser here with the clicked coordinates
    QString address = "http://genome.ucsc.edu/cgi-bin/hgTracks?position="+chr->text()+"+"+start->text()+"+"+end->text()+"&Submit=submit";
    QDesktopServices::openUrl(QUrl(address));
}

void CSDesktop::conservationTableOpenBrowser(int row) {
    QTableWidgetItem *chr   = conservationTable->item(row, 0);
    QTableWidgetItem *start = conservationTable->item(row, 1);
    QTableWidgetItem *end   = conservationTable->item(row, 2);
    // launch web browser here with the clicked coordinates
    QString address = "http://genome.ucsc.edu/cgi-bin/hgTracks?position="+chr->text()+"+"+start->text()+"+"+end->text()+"&Submit=submit";
    QDesktopServices::openUrl(QUrl(address));
}

void CSDesktop::compPeaksTableOpenBrowser(int row) {
    QTableWidgetItem *chr   = compPeaksTable->item(row, 0);
    QTableWidgetItem *start = compPeaksTable->item(row, 1);
    QTableWidgetItem *end   = compPeaksTable->item(row, 2);
    // launch web browser here with the clicked coordinates
    QString address = "http://genome.ucsc.edu/cgi-bin/hgTracks?position="+chr->text()+"+"+start->text()+"+"+end->text()+"&Submit=submit";
    QDesktopServices::openUrl(QUrl(address));
}

void CSDesktop::compGenesTableOpenBrowser(int row) {
    QTableWidgetItem *id   = compGenesTable->item(row, 0);
    // launch web browser here with the clicked coordinates
    QString address = "http://genome.ucsc.edu/cgi-bin/hgTracks?position="+id->text()+"&Submit=submit";
    QDesktopServices::openUrl(QUrl(address));
}

void CSDesktop::checkGenesSummarySpeciesComboBox() {
    QString tmpSpecies = ui->genesSummarySpeciesComboBox->currentText();

    //cout << tmpSpecies.toStdString();

    ui->genesSummaryDatabaseComboBox->clear();

    if(tmpSpecies == "Homo sapiens (hg18)" || tmpSpecies == "Mus musculus (mm9)") {
        ui->genesSummaryDatabaseComboBox->insertItems(0, QStringList()
                                                      << QApplication::translate("CSDesktop", "refSeq", 0, QApplication::UnicodeUTF8)
                                                      << QApplication::translate("CSDesktop", "Ensembl", 0, QApplication::UnicodeUTF8)
                                                      << QApplication::translate("CSDesktop", "UCSCGenes", 0, QApplication::UnicodeUTF8)
                                                      << QApplication::translate("CSDesktop", "AceView", 0, QApplication::UnicodeUTF8)
                                                      );
    }
    else if(tmpSpecies == "Drosophila melanogaster (dm3)") {
        ui->genesSummaryDatabaseComboBox->insertItems(0, QStringList()
                                                      << QApplication::translate("CSDesktop", "refSeq", 0, QApplication::UnicodeUTF8)
                                                      << QApplication::translate("CSDesktop", "Ensembl", 0, QApplication::UnicodeUTF8)
                                                      );
    }
    else if(tmpSpecies == "Saccharomyces cerevisiae (sacCer2)") {
        ui->genesSummaryDatabaseComboBox->insertItems(0, QStringList()
                                                      << QApplication::translate("CSDesktop", "SGD", 0, QApplication::UnicodeUTF8)
                                                      );
    }
}

void CSDesktop::checkGenomDistSpeciesComboBox() {
    QString tmpSpecies = ui->genomicDistSpeciesComboBox->currentText();

    ui->genomicDistDatabaseComboBox->clear();

    if(tmpSpecies == "Homo sapiens (hg18)" || tmpSpecies == "Mus musculus (mm9)") {
        ui->genomicDistDatabaseComboBox->insertItems(0, QStringList()
                                                     << QApplication::translate("CSDesktop", "refSeq", 0, QApplication::UnicodeUTF8)
                                                     << QApplication::translate("CSDesktop", "Ensembl", 0, QApplication::UnicodeUTF8)
                                                     << QApplication::translate("CSDesktop", "UCSCGenes", 0, QApplication::UnicodeUTF8)
                                                     << QApplication::translate("CSDesktop", "AceView", 0, QApplication::UnicodeUTF8)
                                                     );
    }
    else if(tmpSpecies == "Drosophila melanogaster (dm3)") {
        ui->genomicDistDatabaseComboBox->insertItems(0, QStringList()
                                                     << QApplication::translate("CSDesktop", "refSeq", 0, QApplication::UnicodeUTF8)
                                                     << QApplication::translate("CSDesktop", "Ensembl", 0, QApplication::UnicodeUTF8)
                                                     );
    }
    else if(tmpSpecies == "Saccharomyces cerevisiae (sacCer2)") {
        ui->genomicDistDatabaseComboBox->insertItems(0, QStringList()
                                                     << QApplication::translate("CSDesktop", "SGD", 0, QApplication::UnicodeUTF8)
                                                     );
    }
}

void CSDesktop::checkEncodeSpeciesComboBox() {
    QString tmpSpecies = ui->encodeSpeciesComboBox->currentText();
    QString genome;

    ui->encodeSelectDatasetComboBox->clear();

    if(tmpSpecies == "Homo sapiens (hg18)") {
        genome = "hg18";
    }
    else if(tmpSpecies == "Homo sapiens (hg19)") {
        genome = "hg19";
    }

    findAvailableEncodeDatasets(genome);
}

void CSDesktop::changeFindMotifSuffixBComboBox() {
    QString tmpMotifName = ui->findmotifBulykMotifComboBox->currentText();

    ui->findmotifOutputFileLineEdit->setText(ui->findmotifSelectPeaksLineEdit->text()+"_"+tmpMotifName);
}

void CSDesktop::changeFindMotifSuffixJComboBox() {
    QString tmpMotifName = ui->findmotifJasparMotifComboBox->currentText();

    ui->findmotifOutputFileLineEdit->setText(ui->findmotifSelectPeaksLineEdit->text()+"_"+tmpMotifName);
}

void CSDesktop::checkFindPathwaySpeciesComboBox() {
    QString tmpSpecies = ui->findPathwaySpeciesComboBox->currentText();

    ui->findPathwaySelectDatabaseComboBox->clear();
    ui->findPathwaysDatabaseComboBox->clear();


    if(tmpSpecies == "Homo sapiens (hg18)") {
        ui->findPathwaySelectDatabaseComboBox->insertItems(0, QStringList()
                                                           << QApplication::translate("CSDesktop", "Gene Ontology (GO)", 0, QApplication::UnicodeUTF8)
                                                           << QApplication::translate("CSDesktop", "BioCarta", 0, QApplication::UnicodeUTF8)
                                                           << QApplication::translate("CSDesktop", "KEGG", 0, QApplication::UnicodeUTF8)
                                                           << QApplication::translate("CSDesktop", "Human Protein Reference Database", 0, QApplication::UnicodeUTF8)
                                                           << QApplication::translate("CSDesktop", "SignatureDB", 0, QApplication::UnicodeUTF8)
                                                           << QApplication::translate("CSDesktop", "Reactome", 0, QApplication::UnicodeUTF8)
                                                           );

        ui->findPathwaysDatabaseComboBox->insertItems(0, QStringList()
                                                      << QApplication::translate("CSDesktop", "refSeq", 0, QApplication::UnicodeUTF8)
                                                      << QApplication::translate("CSDesktop", "Ensembl", 0, QApplication::UnicodeUTF8)
                                                      << QApplication::translate("CSDesktop", "UCSCGenes", 0, QApplication::UnicodeUTF8)
                                                      << QApplication::translate("CSDesktop", "AceView", 0, QApplication::UnicodeUTF8)
                                                      );
    }
    if(tmpSpecies == "Mus musculus (mm9)") {
        ui->findPathwaySelectDatabaseComboBox->insertItems(0, QStringList()
                                                           << QApplication::translate("CSDesktop", "Gene Ontology (GO)", 0, QApplication::UnicodeUTF8)
                                                           );

        ui->findPathwaysDatabaseComboBox->insertItems(0, QStringList()
                                                      << QApplication::translate("CSDesktop", "refSeq", 0, QApplication::UnicodeUTF8)
                                                      << QApplication::translate("CSDesktop", "Ensembl", 0, QApplication::UnicodeUTF8)
                                                      << QApplication::translate("CSDesktop", "UCSCGenes", 0, QApplication::UnicodeUTF8)
                                                      << QApplication::translate("CSDesktop", "AceView", 0, QApplication::UnicodeUTF8)
                                                      );
    }
    else if(tmpSpecies == "Drosophila melanogaster (dm3)") {
        ui->findPathwaySelectDatabaseComboBox->insertItems(0, QStringList()
                                                           << QApplication::translate("CSDesktop", "Gene Ontology (GO)", 0, QApplication::UnicodeUTF8)
                                                           );

        ui->findPathwaysDatabaseComboBox->insertItems(0, QStringList()
                                                      << QApplication::translate("CSDesktop", "refSeq", 0, QApplication::UnicodeUTF8)
                                                      << QApplication::translate("CSDesktop", "Ensembl", 0, QApplication::UnicodeUTF8)
                                                      );
    }
    else if(tmpSpecies == "Saccharomyces cerevisiae (sacCer2)") {
        ui->findPathwaySelectDatabaseComboBox->insertItems(0, QStringList()
                                                           << QApplication::translate("CSDesktop", "Gene Ontology (GO)", 0, QApplication::UnicodeUTF8)
                                                           );

        ui->findPathwaysDatabaseComboBox->insertItems(0, QStringList()
                                                      << QApplication::translate("CSDesktop", "SGD", 0, QApplication::UnicodeUTF8)
                                                      );
    }

    fillInPathwaysComboBox();
}

void CSDesktop::fillInPathwaysComboBox() {

    ui->findPathwaySelectPathwayComboBox->clear();

    findAvailablePathways();
}

void CSDesktop::checkPageSpeciesComboBox() {
    QString tmpSpecies = ui->pageSpeciesComboBox->currentText();

    ui->pageGenesDBComboBox->clear();
    ui->pagePathwaysDBComboBox->clear();

    if(tmpSpecies == "Homo sapiens (hg18)") {
        ui->pageGenesDBComboBox->insertItems(0, QStringList()
                                             << QApplication::translate("CSDesktop", "refSeq", 0, QApplication::UnicodeUTF8)
                                             << QApplication::translate("CSDesktop", "Ensembl", 0, QApplication::UnicodeUTF8)
                                             << QApplication::translate("CSDesktop", "UCSCGenes", 0, QApplication::UnicodeUTF8)
                                             << QApplication::translate("CSDesktop", "AceView", 0, QApplication::UnicodeUTF8)
                                             );

        ui->pagePathwaysDBComboBox->insertItems(0, QStringList()
                                                << QApplication::translate("CSDesktop", "Gene Ontology (GO)", 0, QApplication::UnicodeUTF8)
                                                << QApplication::translate("CSDesktop", "BioCarta", 0, QApplication::UnicodeUTF8)
                                                << QApplication::translate("CSDesktop", "KEGG", 0, QApplication::UnicodeUTF8)
                                                << QApplication::translate("CSDesktop", "Human Protein Reference Database", 0, QApplication::UnicodeUTF8)
                                                << QApplication::translate("CSDesktop", "SignatureDB", 0, QApplication::UnicodeUTF8)
                                                << QApplication::translate("CSDesktop", "Reactome", 0, QApplication::UnicodeUTF8)
                                                );
    }
    else if(tmpSpecies == "Mus musculus (mm9)") {
        ui->pageGenesDBComboBox->insertItems(0, QStringList()
                                             << QApplication::translate("CSDesktop", "refSeq", 0, QApplication::UnicodeUTF8)
                                             << QApplication::translate("CSDesktop", "Ensembl", 0, QApplication::UnicodeUTF8)
                                             << QApplication::translate("CSDesktop", "UCSCGenes", 0, QApplication::UnicodeUTF8)
                                             << QApplication::translate("CSDesktop", "AceView", 0, QApplication::UnicodeUTF8)
                                             );

        ui->pagePathwaysDBComboBox->insertItems(0, QStringList()
                                                << QApplication::translate("CSDesktop", "Gene Ontology (GO)", 0, QApplication::UnicodeUTF8)
                                                );
    }
    else if(tmpSpecies == "Drosophila melanogaster (dm3)") {
        ui->pageGenesDBComboBox->insertItems(0, QStringList()
                                             << QApplication::translate("CSDesktop", "refSeq", 0, QApplication::UnicodeUTF8)
                                             << QApplication::translate("CSDesktop", "Ensembl", 0, QApplication::UnicodeUTF8)
                                             );

        ui->pagePathwaysDBComboBox->insertItems(0, QStringList()
                                                << QApplication::translate("CSDesktop", "Gene Ontology (GO)", 0, QApplication::UnicodeUTF8)
                                                );
    }
    else if(tmpSpecies == "Saccharomyces cerevisiae (sacCer2)") {
        ui->pageGenesDBComboBox->insertItems(0, QStringList()
                                             << QApplication::translate("CSDesktop", "SGD", 0, QApplication::UnicodeUTF8)
                                             );

        ui->pagePathwaysDBComboBox->insertItems(0, QStringList()
                                                << QApplication::translate("CSDesktop", "Gene Ontology (GO)", 0, QApplication::UnicodeUTF8)
                                                );
    }
}

void CSDesktop::on_runCompPeaksInverseButton_clicked()
{
    if(!(ui->compPeaksSelectPeaks1LineEdit->text().isEmpty()) && !(ui->compPeaksSelectPeaks1LineEdit->text().isNull())) {
        if(!(ui->compPeaksSelectPeaks2LineEdit->text().isEmpty()) && !(ui->compPeaksSelectPeaks2LineEdit->text().isNull())) {
            QString file1tmp = ui->compPeaksSelectPeaks1LineEdit->text();
            ui->compPeaksSelectPeaks1LineEdit->setText(ui->compPeaksSelectPeaks2LineEdit->text());
            ui->compPeaksSelectPeaks2LineEdit->setText(file1tmp);
        }
        else {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Information);
            msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                                  | Qt::WindowMinimizeButtonHint);
            msgBox.setText(tr("Cannot inverse files. Peak file 2 does not exist."));
            msgBox.exec();
        }
    }
    else {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Information);
        msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                              | Qt::WindowMinimizeButtonHint);
        msgBox.setText(tr("Cannot inverse files. Peak file 1 does not exist."));
        msgBox.exec();
    }
}

void CSDesktop::on_runPeakDetectionInverseButton_clicked()
{
    if(!(ui->selectChipFolderLineEdit->text().isEmpty()) && !(ui->selectChipFolderLineEdit->text().isNull())) {
        if(!(ui->selectInputFolderLineEdit->text().isEmpty()) && !(ui->selectInputFolderLineEdit->text().isNull())) {
            QString folder1tmp = ui->selectChipFolderLineEdit->text();
            ui->selectChipFolderLineEdit->setText(ui->selectInputFolderLineEdit->text());
            ui->selectInputFolderLineEdit->setText(folder1tmp);
        }
        else {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Information);
            msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                                  | Qt::WindowMinimizeButtonHint);
            msgBox.setText(tr("Cannot inverse folders. INPUT folder does not exist."));
            msgBox.exec();
        }
    }
    else {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Information);
        msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                              | Qt::WindowMinimizeButtonHint);
        msgBox.setText(tr("Cannot inverse folders. ChIP folder does not exist."));
        msgBox.exec();
    }
}

//
// Functions that handle the split processes
//

void CSDesktop::on_splitButton_clicked()
{
    //clear the fromError variable
    this->splitChipProcess.setFromError(false);

    //clear the text edit area
    ui->loadProgressTextEdit->clear();

    //get the data format
    QString tmpFormat = ui->loadFormatComboBox->currentText();
    if(tmpFormat == "bowtiesam") {
        tmpFormat = "sam";
    }

    this->splitChipProcess.setDataFormat(tmpFormat);
    this->splitInputProcess.setDataFormat(tmpFormat);

    //get the ChIP folder
    QString tmpChipFolder = ui->selectChipRawFolderLineEdit->text();
    if(tmpChipFolder.isNull() || tmpChipFolder.isEmpty()) {
        //bring progress tab to front
        ui->loadTabWidget->setCurrentIndex(1);
        //throw message
        ui->loadProgressTextEdit->append("<font color=red> No ChIP folder was selected. Please try again. </font>");
        return;
    }
    else {
        //prepare the next tab's ChIP field
        ui->selectChipFolderLineEdit->setText(tmpChipFolder);

        //prepare the next tab's output field
        ui->peakDetectionOutputFileLineEdit->setText(tmpChipFolder+"/TF_targets.txt");

        //prepare the next tab's format combo box
        ui->peakDetectionFormatComboBox->setCurrentIndex(ui->loadFormatComboBox->currentIndex());

        //prepare next tab's reads folder
        ui->readsTrackSelectReadsLineEdit->setText(tmpChipFolder);

        //prepare the next tab's output field
        ui->readsTrackOutputFileLineEdit->setText(tmpChipFolder+"/Smoothed_Reads_Track");

        //set the data and output folders
        this->splitChipProcess.setDataFolder(tmpChipFolder);
        this->splitChipProcess.setOutputFolder(tmpChipFolder);

        //set the process flag
        this->splitChipProcess.setChipFlag(true);

        //set the arguments
        QStringList chipArguments;
        chipArguments << SPLIT_SCRIPT << "--format" << this->splitChipProcess.getDataFormat()
                      << "--datafolder" << this->splitChipProcess.getDataFolder()
                      << "--outputfolder" << this->splitChipProcess.getDataFolder();

        //cout<<SPLIT_SCRIPT<<"\n";
        //cout<<this->splitChipProcess.getDataFormat().toStdString()<<"\n";
        //cout<<this->splitChipProcess.getDataFolder().toStdString()<<"\n";

        //start process for ChIP data
        this->splitChipProcess.start(PERL, chipArguments);

        //start the progress bar
        progressBar->setVisible(1);

        //throw start message
        ui->loadProgressTextEdit->append("<font color=green> Split process for ChIP data has started. </font>");
    }

    //get the INPUT folder
    QString tmpInputFolder = ui->selectInputRawFolderLineEdit->text();
    if(tmpInputFolder.isNull() || tmpInputFolder.isEmpty()) {
        //bring progress tab to front
        ui->loadTabWidget->setCurrentIndex(1);
        //throw message
        ui->loadProgressTextEdit->append("<font color=red> No INPUT folder was selected. </font>");
    }
    else {
        //prepare the next tab's INPUT field
        ui->selectInputFolderLineEdit->setText(tmpInputFolder);

        //set the data and output folders
        this->splitInputProcess.setDataFolder(tmpInputFolder);
        this->splitInputProcess.setOutputFolder(tmpInputFolder);

        //set the process flag
        this->splitInputProcess.setChipFlag(false);

        //set the arguments
        QStringList inputArguments;
        inputArguments << SPLIT_SCRIPT << "--format" << this->splitInputProcess.getDataFormat()
                       << "--datafolder" << this->splitInputProcess.getDataFolder()
                       << "--outputfolder" << this->splitInputProcess.getDataFolder();

        //start process for INPUT data
        this->splitInputProcess.start(PERL, inputArguments);

        //start the progress bar
        progressBar->setVisible(1);

        //throw start message
        ui->loadProgressTextEdit->append("<font color=green> Split process for INPUT data has started. </font>");
    }
    //bring progress tab to front
    ui->loadTabWidget->setCurrentIndex(1);
    //enable stop button
    ui->stopSplitProcessButton->setEnabled(true);
    //disable run button
    ui->splitButton->setEnabled(false);
    //disable clear button
    ui->loadClearButton->setEnabled(false);
}

void CSDesktop::readOutputFromSplitChipProcess()
{
    ui->loadProgressTextEdit->append(this->splitChipProcess.readAllStandardOutput());
}

void CSDesktop::readErrorFromSplitChipProcess()
{
    ui->loadProgressTextEdit->append("<font color=red>" +
                                     this->splitChipProcess.readAllStandardError() + "</font>");
    if(this->splitChipProcess.state() == QProcess::Running)
    {
        this->splitChipProcess.kill();
        //disable stop button
        ui->stopSplitProcessButton->setEnabled(false);
        //enable run button
        ui->splitButton->setEnabled(true);
        //enable clear button
        ui->loadClearButton->setEnabled(true);
        //throw end message
        ui->loadProgressTextEdit->append("<font color=red> Split process for ChIP terminated.</font>");
        this->splitChipProcess.setFromError(true);

        if(this->splitInputProcess.state() == QProcess::NotRunning) {
            //stop the progress bar
            progressBar->setVisible(0);
        }
    }
}

void CSDesktop::readOutputFromSplitInputProcess()
{
    ui->loadProgressTextEdit->append(this->splitInputProcess.readAllStandardOutput());
}

void CSDesktop::readErrorFromSplitInputProcess()
{
    ui->loadProgressTextEdit->append("<font color=red>" +
                                     this->splitInputProcess.readAllStandardError() + "</font>");
    if(this->splitInputProcess.state() == QProcess::Running)
    {
        this->splitInputProcess.kill();
        //disable stop button
        ui->stopSplitProcessButton->setEnabled(false);
        //enable run button
        ui->splitButton->setEnabled(true);
        //enable clear button
        ui->loadClearButton->setEnabled(true);
        //throw end message
        ui->loadProgressTextEdit->append("<font color=red> Split process for INPUT terminated.</font>");
        this->splitInputProcess.setFromError(true);

        if(this->splitChipProcess.state() == QProcess::NotRunning) {
            //stop the progress bar
            progressBar->setVisible(0);
        }
    }
}

void CSDesktop::on_stopSplitProcessButton_clicked()
{
    if(this->splitChipProcess.state() == QProcess::Running)
    {
        this->splitChipProcess.kill();
        ui->loadProgressTextEdit->append("<font color=red> Split process was terminated by the user.</font>");
    }

    if(this->splitInputProcess.state() == QProcess::Running)
    {
        this->splitInputProcess.kill();
        ui->loadProgressTextEdit->append("<font color=red> INPUT process was terminated by the user.</font>");
    }
    //disable stop button
    ui->stopSplitProcessButton->setEnabled(false);
    //enable run button
    ui->splitButton->setEnabled(true);
    //enable clear button
    ui->loadClearButton->setEnabled(true);

    this->splitChipProcess.setFromError(true);
    this->splitInputProcess.setFromError(true);

    //stop the progress bar
    progressBar->setVisible(0);

    return;
}

void CSDesktop::onSplitChipProcessFinished()
{
    if(!(this->splitChipProcess.getFromError()))
    {
        //throw message
        ui->loadProgressTextEdit->append("<font color=green> Split process for ChIP data finished successfully. The reads per chromosome files have been created in "
                                         + this->splitChipProcess.getOutputFolder() + "</font>");
    }

    if(this->splitInputProcess.state() == QProcess::NotRunning) {
        //enable split button
        ui->splitButton->setEnabled(true);
        //enable clear button
        ui->loadClearButton->setEnabled(true);
        //disable stop button
        ui->stopSplitProcessButton->setEnabled(false);
        //stop the progress bar
        progressBar->setVisible(0);
    }
}

void CSDesktop::onSplitInputProcessFinished()
{
    if(!(this->splitInputProcess.getFromError()))
    {
        //throw message
        ui->loadProgressTextEdit->append("<font color=green> Split process for INPUT data finished successfully. The reads per chromosome files have been created in "
                                         + this->splitInputProcess.getOutputFolder() + "</font>");
    }


    if(this->splitChipProcess.state() == QProcess::NotRunning) {
        //enable run button
        ui->splitButton->setEnabled(true);
        //enable clear button
        ui->loadClearButton->setEnabled(true);
        //disable stop button
        ui->stopSplitProcessButton->setEnabled(false);
        //stop the progress bar
        progressBar->setVisible(0);
    }
}

//
// Functions that handle the run CS process
//

void CSDesktop::on_runCSButton_clicked()
{
    //clear the fromError variable
    this->chipseeqerProcess.setFromError(false);

    //clear the text edit area
    ui->peakDetectionProgressTextEdit->clear();

    //clear the targets results tab fields
    this->peakDetectionTable->clear();
    this->showPeakDetStatsTextEdit->clear();

    //get the data format
    QString tmpFormat = ui->peakDetectionFormatComboBox->currentText();
    if(tmpFormat == "exteland") {
        tmpFormat = "eland";
    }
    if(tmpFormat == "export") {
        tmpFormat = "eland";
    }
    this->chipseeqerProcess.setDataFormat(tmpFormat);

    //get the output name
    this->chipseeqerProcess.setOutputFile(ui->peakDetectionOutputFileLineEdit->text());

    if(this->chipseeqerProcess.getOutputFile().isNull() || this->chipseeqerProcess.getOutputFile().isEmpty()) {
        //bring progress tab to front
        ui->peakDetectionTabWidget->setCurrentIndex(1);
        //throw message
        ui->peakDetectionProgressTextEdit->append("<font color=red> No output name was selected. Please try again. </font>");
        return;
    }

    //get the fold value
    this->chipseeqerProcess.setFold(ui->peakDetectionFoldSpinBox->text().toDouble());

    //get the threshold
    this->chipseeqerProcess.setThreshold(ui->peakDetectionThresholdSpinBox->text().toInt());

    //get the fragment length
    this->chipseeqerProcess.setFragmentLength(ui->peakDetectionFragLenSpinBox->text().toInt());

    //get the minimum length
    this->chipseeqerProcess.setMinimumLength(ui->peakDetectionMinLenSpinBox->text().toInt());

    //get the minimum distance
    this->chipseeqerProcess.setMinimumDistance(ui->peakDetectionMinDistSpinBox->text().toInt());

    //get the max peak height
    this->chipseeqerProcess.setMinPeakHeight(ui->peakDetectionMinPeakHeightSpinBox->text().toInt());

    if(ui->peakDetectionUniqueCheckBox->isChecked())
        this->chipseeqerProcess.setUniqueReads(true);
    else
        this->chipseeqerProcess.setUniqueReads(false);

    //get the species
    QString tmpSpecies = ui->peakDetectionSpeciesComboBox->currentText();
    QString chrdata;

    if(tmpSpecies == "Homo sapiens (hg18)") {
        chrdata = HG18_CHRDATA;
    }
    else if(tmpSpecies == "Homo sapiens (hg19)") {
        chrdata = HG19_CHRDATA;
    }
    else if(tmpSpecies == "Mus musculus (mm10)") {
        chrdata = MM10_CHRDATA;
    }
    else if(tmpSpecies == "Mus musculus (mm9)") {
        chrdata = MM9_CHRDATA;
    }
    else if(tmpSpecies == "Rattus norvegicus (rn4)") {
        chrdata = RN4_CHRDATA;
    }
    else if(tmpSpecies == "Drosophila melanogaster (dm3)") {
        chrdata = DM3_CHRDATA;
    }
    else if(tmpSpecies == "Saccharomyces cerevisiae (sacCer2)") {
        chrdata = SACSER_CHRDATA;
    }
    else if(tmpSpecies == "Zebrafish (zv9)") {
        chrdata = ZV9_CHRDATA;
    }

    this->chipseeqerProcess.setChrdata(chrdata);

    //get the ChIP folder
    QString tmpChIPFolder = ui->selectChipFolderLineEdit->text();
    if(tmpChIPFolder.isNull() || tmpChIPFolder.isEmpty()) {
        //bring progress tab to front
        ui->peakDetectionTabWidget->setCurrentIndex(1);
        //throw message
        ui->peakDetectionProgressTextEdit->append("<font color=red> No ChIP folder was selected. Please try again. </font>");
        return;
    }
    else {
        //set the ChIP folder
        this->chipseeqerProcess.setChipFolder(tmpChIPFolder);

        //prepare the next tab's output field
        ui->genesSummarySelectPeaksLineEdit->setText(chipseeqerProcess.getOutputFile());
        ui->genesSummaryOutputFileLineEdit->setText(chipseeqerProcess.getOutputFile());
        ui->peaksTrackSelectPeaksLineEdit->setText(chipseeqerProcess.getOutputFile());
        ui->peaksTrackOutputFileLineEdit->setText(chipseeqerProcess.getOutputFile());
        ui->genomicDistSelectPeaksLineEdit->setText(chipseeqerProcess.getOutputFile());
        ui->genomicDistOutputFileLineEdit->setText(chipseeqerProcess.getOutputFile());
        ui->rnaGenesSelectPeaksLineEdit->setText(chipseeqerProcess.getOutputFile());
        ui->rnaGenesOutputFileLineEdit->setText(chipseeqerProcess.getOutputFile());
        ui->repeatsSelectPeaksLineEdit->setText(chipseeqerProcess.getOutputFile());
        ui->repeatsOutputFileLineEdit->setText(chipseeqerProcess.getOutputFile());
        ui->cpgIslandsSelectPeaksLineEdit->setText(chipseeqerProcess.getOutputFile());
        ui->cpgIslandsOutputFileLineEdit->setText(chipseeqerProcess.getOutputFile());
        ui->segDuplicatesSelectPeaksLineEdit->setText(chipseeqerProcess.getOutputFile());
        ui->segDuplicatesOutputFileLineEdit->setText(chipseeqerProcess.getOutputFile());
        ui->findmotifSelectPeaksLineEdit->setText(chipseeqerProcess.getOutputFile());
        ui->findmotifOutputFileLineEdit->setText(chipseeqerProcess.getOutputFile()+"_motif");
        ui->fireSelectPeaksLineEdit->setText(chipseeqerProcess.getOutputFile());
        ui->consSelectPeaksLineEdit->setText(chipseeqerProcess.getOutputFile());
        ui->consOutputFileLineEdit->setText(chipseeqerProcess.getOutputFile()+"_cons");
        ui->consOutputRandomLineEdit->setText(chipseeqerProcess.getOutputFile()+"_randcons");
        ui->findPathwaySelectPeaksLineEdit->setText(chipseeqerProcess.getOutputFile());
        ui->findPathwayOutputFileLineEdit->setText(chipseeqerProcess.getOutputFile());
        ui->findPathwaySelectPeaksLineEdit->setText(chipseeqerProcess.getOutputFile());
        ui->findPathwayOutputFileLineEdit->setText(chipseeqerProcess.getOutputFile());
        ui->pageSelectPeaksLineEdit->setText(chipseeqerProcess.getOutputFile());
        ui->pageOutputFileLineEdit->setText(chipseeqerProcess.getOutputFile());
        ui->encodeSelectPeaksLineEdit->setText(chipseeqerProcess.getOutputFile());
        //ui->encodeOutputFileLineEdit->setText(chipseeqerProcess.getOutputFile()+".ENCODE");

        QString fold;
        QString threshold;
        QString fraglen;
        QString mindist;
        QString minlen;
        QString minheight;
        QString ureads;

        fold.setNum(this->chipseeqerProcess.getFold());
        threshold.setNum(this->chipseeqerProcess.getThreshold());
        fraglen.setNum(this->chipseeqerProcess.getFragmentLength());
        mindist.setNum(this->chipseeqerProcess.getMinimumDistance());
        minlen.setNum(this->chipseeqerProcess.getMinimumLength());
        minheight.setNum(this->chipseeqerProcess.getMinPeakHeight());

        //cout << "MINHEIGHT:" << minheight.toStdString() << "\n";

        if(chipseeqerProcess.getUniqueReads()) {
            ureads = "1";
        }
        else {
            ureads = "0";
        }

        //set the arguments
        QStringList csArguments;
        csArguments << "-chipdir" << this->chipseeqerProcess.getChipFolder()
                    << "-format" << this->chipseeqerProcess.getDataFormat()
                    << "-fold" << fold
                    << "-t" << threshold
                    << "-fraglen" << fraglen
                    << "-mindist" << mindist
                    << "-minlen" << minlen
                    << "-minpeakheight" << minheight
                    << "-uniquereads" << ureads
                    << "-chrdata" << chrdata
                    << "-outfile" << this->chipseeqerProcess.getOutputFile();

        //see if the INPUT data is given
        QString tmpInputFolder = ui->selectInputFolderLineEdit->text();
        if(!tmpChIPFolder.isNull() && !tmpChIPFolder.isEmpty()) {
            this->chipseeqerProcess.setInputFolder(tmpInputFolder);
            csArguments << "-inputdir" << this->chipseeqerProcess.getInputFolder();
        }

        //start process for ChIP data
        this->chipseeqerProcess.start(CHIPSEEQER_PROGRAM, csArguments);

        //start the progress bar
        progressBar->setVisible(1);

        //bring progress tab to front
        ui->peakDetectionTabWidget->setCurrentIndex(1);

        //enable stop button
        ui->stopRunCSProcessButton->setEnabled(true);
        //disable run button
        ui->runCSButton->setEnabled(false);
        //disable clear button
        ui->peakDetectionClearButton->setEnabled(false);
    }
}

void CSDesktop::onChipseeqerProcessFinished()
{
    if(this->chipseeqerProcess.state() == QProcess::Running)
    {
        this->chipseeqerProcess.kill();
    }
    if(!(this->chipseeqerProcess.getFromError()))
    {
        //throw message
        ui->peakDetectionProgressTextEdit->append("<font color=green> ChIPseeqer process finished successfully. The detected peaks are in "
                                                  + this->chipseeqerProcess.getOutputFile() + "</font>");

        //show results in new tab
        ui->peakDetectionTabWidget->addTab(showPeakDetTab, "Results");
        ui->peakDetectionTabWidget->setCurrentIndex(ui->peakDetectionTabWidget->indexOf(showPeakDetTab));

        peakDetectionTable->setGeometry(QRect(0, 23, 641, 381));
        peakDetectionTable->show();

        peakDetStatsLabel->setObjectName(QString::fromUtf8("targetsStatsLabel"));
        peakDetStatsLabel->setText("Information");
        peakDetStatsLabel->setGeometry(QRect(0, 406, 641, 21));
        peakDetStatsLabel->show();

        showPeakDetStatsTextEdit->setGeometry(QRect(0, 429, 641, 101));
        showPeakDetStatsTextEdit->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);
        showPeakDetStatsTextEdit->show();

        peakDetectionCopyButton->setGeometry(QRect(0, 540, 100, 21));
        peakDetectionCopyButton->setText("Copy table");
        peakDetectionCopyButton->show();
        peakDetectionCopyButton->connect(peakDetectionCopyButton, SIGNAL(clicked()), this, SLOT(peakDetectionTableCopy()));
        peakDetectionCopyButton->setStatusTip("Copy table contents to clipboard");
        peakDetectionCopyButton->setToolTip("Copy table contents to clipboard");

        // open the targets file to count its lines
        QFile file(this->chipseeqerProcess.getOutputFile());
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

        QTextStream inL(&file);

        int lines = 0;
        //count lines in the file
        while (!inL.atEnd())
        {
            QString gline = inL.readLine();
            lines++;
        }

        file.close();

        //set the table's rows and columns
        peakDetectionTable->setRowCount(lines);
        peakDetectionTable->setColumnCount(11);
        QStringList headerNames;
        headerNames << "Chromosome" << "Start" << "End" << "Avg p_value" << "Score" << "Max peak height position" << "Max peak height"
                    << "% Max peak height position" << "Peak size" << "Mid point" << "Summit dist. from midpoint";
        peakDetectionTable->setHorizontalHeaderLabels(headerNames);

        //open the file to add lines in the table
        QFile gefile(this->chipseeqerProcess.getOutputFile());
        if (!gefile.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

        QTextStream in(&gefile);

        //fill in the table with each row
        while (!in.atEnd())
        {
            for( int r = 0; r < peakDetectionTable->rowCount(); r++ ){

                QString gline           = in.readLine();
                QStringList lineList    = gline.split("\t", QString::SkipEmptyParts);

                for( int c = 0; c < peakDetectionTable->columnCount(); ++c ){

                    QTableWidgetItem *temp = new QTableWidgetItem(lineList.value(c), 0);
                    temp->setText(lineList.value(c));
                    peakDetectionTable->setItem(r, c, temp);
                }
            }
        }

        gefile.close();

        peakDetectionTable->connect(peakDetectionTable, SIGNAL(cellDoubleClicked(int,int)), this, SLOT(peakDetectionTableOpenBrowser(int)) );

        //print the statistics
        //stringstream sstr;
        //sstr << lines;
        //QString tmpLines = sstr.str().c_str();
        //showPeakDetStatsTextEdit->append("Total number of peaks: " + tmpLines);

        QString output          = ui->peakDetectionProgressTextEdit->toPlainText();
        QStringList outputList  = output.split("\n", QString::SkipEmptyParts);

        for (int j=outputList.size()-5; j<outputList.size()-1; j++) {
            //cout << outputList.value(j).toStdString() << "\n";
            showPeakDetStatsTextEdit->append(outputList.value(j));
        }

    }
    //disable stop button
    ui->stopRunCSProcessButton->setEnabled(false);
    //enable run button
    ui->runCSButton->setEnabled(true);
    //enable clear button
    ui->peakDetectionClearButton->setEnabled(true);
    //stop the progress bar
    progressBar->setVisible(0);

    return;
}

void CSDesktop::readOutputFromChipseeqerProcess()
{
    ui->peakDetectionProgressTextEdit->append(this->chipseeqerProcess.readAllStandardOutput());
}

void CSDesktop::readErrorFromChipseeqerProcess()
{
    ui->peakDetectionProgressTextEdit->append("<font color=red>" +
                                              this->chipseeqerProcess.readAllStandardError() + "</font>");
    if(this->chipseeqerProcess.state() == QProcess::Running)
    {
        this->chipseeqerProcess.kill();
        //disable stop button
        ui->stopRunCSProcessButton->setEnabled(false);
        //enable run button
        ui->runCSButton->setEnabled(true);
        //enable clear button
        ui->peakDetectionClearButton->setEnabled(true);
        //throw end message
        ui->peakDetectionProgressTextEdit->append("<font color=red> ChIPseeqer process terminated.</font>");
        this->chipseeqerProcess.setFromError(true);
        //stop the progress bar
        progressBar->setVisible(0);
    }
}

void CSDesktop::on_stopRunCSProcessButton_clicked()
{
    if(this->chipseeqerProcess.state() == QProcess::Running)
    {
        this->chipseeqerProcess.kill();
        ui->peakDetectionProgressTextEdit->append("<font color=red> ChIPseeqer process was terminated by the user.</font>");
    }
    //disable stop button
    ui->stopRunCSProcessButton->setEnabled(false);
    //enable run button
    ui->runCSButton->setEnabled(true);
    //enable clear button
    ui->peakDetectionClearButton->setEnabled(true);
    this->chipseeqerProcess.setFromError(true);
    //stop the progress bar
    progressBar->setVisible(0);

    return;
}

//
// Functions that handle the run summary process
//

void CSDesktop::on_runSummaryButton_clicked()
{
    //clear the fromError variable
    this->summaryProcess.setFromError(false);

    //clear the text edit area
    ui->genesSummaryProgressTextEdit->clear();

    //clear the targets results tab fields
    this->summaryTable->clear();
    this->showGenSummStatsTextEdit->clear();

    //get the lenD value
    this->summaryProcess.setLenD(ui->genesSummaryLenDSpinBox->text().toDouble());

    //get the lenU value
    this->summaryProcess.setLenU(ui->genesSummaryLenUSpinBox->text().toInt());

    //get the peaks file
    QString tmpPeaksFile = ui->genesSummarySelectPeaksLineEdit->text();
    if(tmpPeaksFile.isNull() || tmpPeaksFile.isEmpty()) {
        //bring progress tab to front
        ui->genesSummaryTabWidget->setCurrentIndex(1);
        //throw message
        ui->genesSummaryProgressTextEdit->append("<font color=red> No peaks file was selected. Please try again. </font>");
        return;
    }
    else {

        //set the peaks file
        this->summaryProcess.setPeaksFile(tmpPeaksFile);

        //set the outfile name
        this->summaryProcess.setOutputFile(ui->genesSummaryOutputFileLineEdit->text());

        if(this->summaryProcess.getOutputFile().isNull() || this->summaryProcess.getOutputFile().isEmpty()) {
            //bring progress tab to front
            ui->genesSummaryTabWidget->setCurrentIndex(1);
            //throw message
            ui->genesSummaryProgressTextEdit->append("<font color=red> No output name was selected. Please try again. </font>");
            return;
        }

        //get the species
        QString tmpSpecies = ui->genesSummarySpeciesComboBox->currentText();
        QString genome;

        if(tmpSpecies == "Homo sapiens (hg18)") {
            genome = "hg18";
        }
        else if(tmpSpecies == "Homo sapiens (hg19)") {
            genome = "hg19";
        }
        else if(tmpSpecies == "Mus musculus (mm9)") {
            genome = "mm9";
        }
        else if(tmpSpecies == "Mus musculus (mm10)") {
            genome = "mm10";
        }
        else if(tmpSpecies == "Drosophila melanogaster (dm3)") {
            genome = "dm3";
        }
        else if(tmpSpecies == "Rattus norvegicus (rn4)") {
            genome = "rn4";
        }
        else if(tmpSpecies == "Zebrafish (zv9)") {
            genome = "zv9";
        }
        else if(tmpSpecies == "Saccharomyces cerevisiae (sacCer2)") {
            genome = "sacser";
        }

        this->summaryProcess.setSpecies(genome);

        //get the database
        this->summaryProcess.setDatabase(ui->genesSummaryDatabaseComboBox->currentText());

        QString lenu;
        QString lend;

        lenu.setNum(this->summaryProcess.getLenU());
        lend.setNum(this->summaryProcess.getLenD());

        //set the arguments
        QStringList inputArguments;
        inputArguments << SUMMARY_PROGRAM << "--targets" << this->summaryProcess.getPeaksFile()
                       << "--db" << this->summaryProcess.getDatabase()
                       << "--prefix" << this->summaryProcess.getPeaksFile()
                       << "--lenu" << lenu
                       << "--lend" << lend
                       << "--genome" << genome;

        //start process for INPUT data
        this->summaryProcess.start(PERL, inputArguments);

        //start the progress bar
        progressBar->setVisible(1);

        //throw start message
        ui->genesSummaryProgressTextEdit->append("<font color=green> Gene Summary process for peaks has started. </font>");
    }
    //bring progress tab to front
    ui->genesSummaryTabWidget->setCurrentIndex(1);
    //enable stop button
    ui->stopRunSummaryProcessButton->setEnabled(true);
    //disable run button
    ui->runSummaryButton->setEnabled(false);
    //disable clear button
    ui->runSummaryClearButton->setEnabled(false);
}

void CSDesktop::readOutputFromSummaryProcess()
{
    ui->genesSummaryProgressTextEdit->append(this->summaryProcess.readAllStandardOutput());
}

void CSDesktop::readErrorFromSummaryProcess()
{
    ui->genesSummaryProgressTextEdit->append("<font color=red>" +
                                             this->summaryProcess.readAllStandardError() + "</font>");
    if(this->summaryProcess.state() == QProcess::Running)
    {
        this->summaryProcess.kill();
        //disable stop button
        ui->stopRunSummaryProcessButton->setEnabled(false);
        //enable run button
        ui->runSummaryButton->setEnabled(true);
        //enable clear button
        ui->runSummaryClearButton->setEnabled(true);
        //throw end message
        ui->peakDetectionProgressTextEdit->append("<font color=red> Gene Summary process terminated.</font>");
        this->summaryProcess.setFromError(true);
        //start the progress bar
        progressBar->setVisible(0);
    }
}

void CSDesktop::onSummaryProcessFinished()
{
    if(this->summaryProcess.state() == QProcess::Running)
    {
        this->summaryProcess.kill();
    }
    if(!(this->summaryProcess.getFromError()))
    {
        //throw message
        ui->genesSummaryProgressTextEdit->append("<font color=green> Gene Summary process finished successfully.</font>");
        ui->genesSummaryProgressTextEdit->append("<font color=green> The extracted transcripts are in " + this->summaryProcess.getOutputFile()
                                                 + "."+this->summaryProcess.getDatabase()+".NM.txt" + "</font>");
        ui->genesSummaryProgressTextEdit->append("<font color=green> The extracted genes are in " + this->summaryProcess.getOutputFile()
                                                 + "."+this->summaryProcess.getDatabase()+".SUM.txt" + "</font>");

        //show results in new tab
        ui->genesSummaryTabWidget->addTab(showGenSummTab, "Results");
        ui->genesSummaryTabWidget->setCurrentIndex(ui->genesSummaryTabWidget->indexOf(showGenSummTab));

        summaryTable->setGeometry(QRect(0, 23, 641, 381));
        summaryTable->show();

        genSummStatsLabel->setObjectName(QString::fromUtf8("genesStatsLabel"));
        genSummStatsLabel->setText("Information");
        genSummStatsLabel->setGeometry(QRect(0, 406, 641, 21));
        genSummStatsLabel->show();

        showGenSummStatsTextEdit->setGeometry(QRect(0, 429, 641, 101));
        showGenSummStatsTextEdit->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);
        showGenSummStatsTextEdit->show();

        summaryCopyButton->setGeometry(QRect(0, 535, 100, 21));
        summaryCopyButton->setText("Copy table");
        summaryCopyButton->show();
        summaryCopyButton->connect(summaryCopyButton, SIGNAL(clicked()), this, SLOT(summaryTableCopy()));
        summaryCopyButton->setStatusTip("Copy table contents to clipboard");
        summaryCopyButton->setToolTip("Copy table contents to clipboard");

        QString suffix;
        QStringList headerNames;

        if(summaryProcess.getDatabase() == "refSeq") {
            suffix = ".SUM.txt";
            headerNames << "Gene Name" << "Description" << "Chromosome" << "Start" << "End" << "Num. of overlap peaks" << "Overlap peaks";
            summaryTable->setColumnCount(7);
        }
        else {
            suffix = ".NM.txt";
            headerNames << "Transcript Name" << "Chromosome" << "Start" << "End" << "Num. of overlap peaks" << "Overlap peaks";
            summaryTable->setColumnCount(6);
        }

        //open the SUM.txt file to count its lines
        QFile gfile(this->summaryProcess.getOutputFile()+ "."+this->summaryProcess.getDatabase()+suffix);
        if (!gfile.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

        QTextStream inL(&gfile);

        int glines = 0;
        //count lines in the file
        while (!inL.atEnd())
        {
            QString gline = inL.readLine();
            glines++;
        }

        gfile.close();

        //set the table's rows and columns
        summaryTable->setRowCount(glines);
        summaryTable->setHorizontalHeaderLabels(headerNames);

        //open the file to add lines in the table
        QFile gefile(this->summaryProcess.getOutputFile()+ "."+this->summaryProcess.getDatabase()+suffix);
        if (!gefile.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

        QTextStream in(&gefile);

        //fill in the table with each row
        while (!in.atEnd())
        {
            for( int r = 0; r < summaryTable->rowCount(); r++ ){

                QString gline           = in.readLine();
                QStringList lineList    = gline.split("\t", QString::SkipEmptyParts);

                for( int c = 0; c < summaryTable->columnCount(); ++c ){

                    QTableWidgetItem *temp = new QTableWidgetItem(lineList.value(c), 0);
                    temp->setText(lineList.value(c));
                    summaryTable->setItem(r, c, temp);
                }
            }
        }

        gefile.close();

        summaryTable->connect(summaryTable, SIGNAL(cellDoubleClicked(int,int)), this, SLOT(summaryTableOpenBrowser(int)) );

        //print the number of genes
        if(summaryProcess.getDatabase() == "refSeq") {

            stringstream sstr;
            sstr << glines;
            QString tmpLines = sstr.str().c_str();
            showGenSummStatsTextEdit->append("Total number of genes: " + tmpLines);

        }
        QFile tfile(this->summaryProcess.getOutputFile()+ "."+this->summaryProcess.getDatabase()+".NM.txt");
        if (!tfile.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

        int tlines = 0;
        QTextStream inT(&tfile);
        while (!inT.atEnd())
        {
            QString tline = inT.readLine();
            tlines++;
        }

        //print the number of transcripts
        stringstream sstr2;
        sstr2 << tlines;
        QString tmpLines2 = sstr2.str().c_str();
        showGenSummStatsTextEdit->append("Total number of transcripts: " + tmpLines2);

        tfile.close();
    }
    //disable stop button
    ui->stopRunSummaryProcessButton->setEnabled(false);
    //enable run button
    ui->runSummaryButton->setEnabled(true);
    //enable clear button
    ui->runSummaryClearButton->setEnabled(true);
    //start the progress bar
    progressBar->setVisible(0);

    return;
}

void CSDesktop::on_stopRunSummaryProcessButton_clicked()
{
    if(this->summaryProcess.state() == QProcess::Running)
    {
        this->summaryProcess.kill();
        ui->genesSummaryProgressTextEdit->append("<font color=red> Gene summary process was terminated by the user.</font>");
    }
    //disable stop button
    ui->stopRunSummaryProcessButton->setEnabled(false);
    //enable run button
    ui->runSummaryButton->setEnabled(true);
    //enable clear button
    ui->runSummaryClearButton->setEnabled(true);
    this->summaryProcess.setFromError(true);
    //stop the progress bar
    progressBar->setVisible(0);

    return;
}

//
// Functions that handle the create peaks track process
//

void CSDesktop::on_createPeaksTrackButton_clicked()
{
    //clear the fromError variable
    this->peaksTrackProcess.setFromError(false);

    //clear the text edit area
    ui->createTracksProgressTextEdit->clear();

    //get the peaks file
    QString tmpPeaksFile = ui->peaksTrackSelectPeaksLineEdit->text();
    if(tmpPeaksFile.isNull() || tmpPeaksFile.isEmpty()) {
        //bring progress tab to front
        ui->createTracksTabWidget->setCurrentIndex(1);
        //throw message
        ui->createTracksProgressTextEdit->append("<font color=red> No peaks file was selected. Please try again. </font>");
        return;
    }
    else {

        //set the peaks file
        this->peaksTrackProcess.setPeaksFile(tmpPeaksFile);

        //get the track name
        QString tmpTrackName = ui->peaksTrackNameLineEdit->text();
        if(tmpTrackName.isNull() || tmpTrackName.isEmpty()) {
            //bring progress tab to front
            ui->createTracksTabWidget->setCurrentIndex(1);
            //throw message
            ui->createTracksProgressTextEdit->append("<font color=red> No track name was selected. Please try again. </font>");
            return;
        }
        else
        {
            //set the track name
            this->peaksTrackProcess.setTrackName(tmpTrackName);

            //set the outfile name
            this->peaksTrackProcess.setOutputFile(this->peaksTrackProcess.getPeaksFile());

            //set the arguments
            QStringList inputArguments;
            inputArguments << PEAKSTRACK_PROGRAM << "--targets" << this->peaksTrackProcess.getPeaksFile()
                           << "--trackname" << this->peaksTrackProcess.getTrackName()
                           << "--outfile" << this->peaksTrackProcess.getOutputFile();

            //start process for INPUT data
            this->peaksTrackProcess.start(PERL, inputArguments);

            //start the progress bar
            progressBar->setVisible(1);

            //throw start message
            ui->createTracksProgressTextEdit->append("<font color=green> Creating peaks track process for peaks has started. </font>");
        }
    }

    //bring progress tab to front
    ui->createTracksTabWidget->setCurrentIndex(1);
    //enable stop button
    ui->stopCreateTracksProcessButton->setEnabled(true);
    //disable run button
    ui->createPeaksTrackButton->setEnabled(false);
    //disable clear button
    ui->peakDetectionClearButton->setEnabled(false);
}

void CSDesktop::readOutputFromPeaksTrackProcess()
{
    ui->createTracksProgressTextEdit->append(this->peaksTrackProcess.readAllStandardOutput());
}

void CSDesktop::readErrorFromPeaksTrackProcess()
{
    ui->createTracksProgressTextEdit->append("<font color=red>" +
                                             this->peaksTrackProcess.readAllStandardError() + "</font>");
    if(this->peaksTrackProcess.state() == QProcess::Running)
    {
        this->peaksTrackProcess.kill();
        //disable stop button
        ui->stopCreateTracksProcessButton->setEnabled(false);
        //enable run button
        ui->createPeaksTrackButton->setEnabled(true);
        //enable clear button
        ui->peaksTrackClearButton->setEnabled(true);
        //throw end message
        ui->createTracksProgressTextEdit->append("<font color=red> Create peaks track process terminated.</font>");

        this->peaksTrackProcess.setFromError(true);
        //start the progress bar
        progressBar->setVisible(0);
    }
}

void CSDesktop::onPeaksTrackProcessFinished()
{
    if(this->peaksTrackProcess.state() == QProcess::Running)
    {
        this->peaksTrackProcess.kill();
    }

    if(!(this->peaksTrackProcess.getFromError()))
    {
        //throw message
        ui->createTracksProgressTextEdit->append("<font color=green> The process for creating the peaks track finished successfully. You can now load the file in the UCSC Genome Browser. </font>");
    }

    //disable stop button
    ui->stopCreateTracksProcessButton->setEnabled(false);
    //enable run button
    ui->createPeaksTrackButton->setEnabled(true);
    //enable clear button
    ui->peaksTrackClearButton->setEnabled(true);
    //start the progress bar
    progressBar->setVisible(0);

    return;
}

//
// Functions that handle the create reads track process
//

void CSDesktop::on_createReadsTrackButton_clicked()
{
    //clear the fromError variable
    this->readsTrackProcess.setFromError(false);

    //clear the text edit area
    ui->createTracksProgressTextEdit->clear();

    //get the reads folder
    QString tmpReadsFolder = ui->readsTrackSelectReadsLineEdit->text();
    if(tmpReadsFolder.isNull() || tmpReadsFolder.isEmpty()) {
        //bring progress tab to front
        ui->createTracksTabWidget->setCurrentIndex(1);
        //throw message
        ui->createTracksProgressTextEdit->append("<font color=red> No reads folder was selected. Please try again. </font>");
        return;
    }
    else {

        //set the reads folder
        this->readsTrackProcess.setReadsDir(tmpReadsFolder);

        //get the track name
        QString tmpTrackName = ui->readsTrackNameLineEdit->text();
        if(tmpTrackName.isNull() || tmpTrackName.isEmpty()) {
            //bring progress tab to front
            ui->createTracksTabWidget->setCurrentIndex(1);
            //throw message
            ui->createTracksProgressTextEdit->append("<font color=red> No track name was selected. Please try again. </font>");
            return;
        }
        else
        {
            //set the track name
            this->readsTrackProcess.setTrackName(tmpTrackName);

            //set the outfile name
            this->readsTrackProcess.setOutputFile(ui->readsTrackOutputFileLineEdit->text());

            if(this->readsTrackProcess.getOutputFile().isNull() || this->readsTrackProcess.getOutputFile().isEmpty()) {
                //bring progress tab to front
                ui->createTracksTabWidget->setCurrentIndex(1);
                //throw message
                ui->createTracksProgressTextEdit->append("<font color=red> No output name was selected. Please try again. </font>");
                return;
            }


            //set the arguments
            QStringList inputArguments;
            inputArguments << READSTRACK_PROGRAM << "--readdir" << this->readsTrackProcess.getReadsDir()
                           << "--trackname" << this->readsTrackProcess.getTrackName()
                           << "--format" << this->readsTrackProcess.getFormat()
                           << "--outfile" << this->readsTrackProcess.getOutputFile();

            //start process for INPUT data
            this->readsTrackProcess.start(PERL, inputArguments);

            //start the progress bar
            progressBar->setVisible(1);

            //throw start message
            ui->createTracksProgressTextEdit->append("<font color=green> Creating reads track process has started. </font>");
        }
    }

    //bring progress tab to front
    ui->createTracksTabWidget->setCurrentIndex(1);
    //enable stop button
    ui->stopCreateTracksProcessButton->setEnabled(true);
    //disable run button
    ui->createReadsTrackButton->setEnabled(false);
    //disable clear button
    ui->peakDetectionClearButton->setEnabled(false);
}

void CSDesktop::readOutputFromReadsTrackProcess()
{
    ui->createTracksProgressTextEdit->append(this->readsTrackProcess.readAllStandardOutput());
}

void CSDesktop::readErrorFromReadsTrackProcess()
{
    ui->createTracksProgressTextEdit->append("<font color=red>" +
                                             this->readsTrackProcess.readAllStandardError() + "</font>");
    if(this->readsTrackProcess.state() == QProcess::Running)
    {
        this->readsTrackProcess.kill();
        //disable stop button
        ui->stopCreateTracksProcessButton->setEnabled(false);
        //enable run button
        ui->createReadsTrackButton->setEnabled(true);
        //enable clear button
        ui->readsTrackClearButton->setEnabled(true);
        //throw end message
        ui->createTracksProgressTextEdit->append("<font color=red> Create peaks track process terminated.</font>");

        this->readsTrackProcess.setFromError(true);

        //start the progress bar
        progressBar->setVisible(0);
    }
}

void CSDesktop::onReadsTrackProcessFinished()
{
    if(this->readsTrackProcess.state() == QProcess::Running)
    {
        this->readsTrackProcess.kill();
    }

    if(!(this->readsTrackProcess.getFromError()))
    {
        //throw message
        ui->createTracksProgressTextEdit->append("<font color=green> The process for creating the reads track finished successfully. You can now load the file in the UBSC Genome Browser. </font>");
    }

    //disable stop button
    ui->stopCreateTracksProcessButton->setEnabled(false);
    //enable run button
    ui->createReadsTrackButton->setEnabled(true);
    //enable clear button
    ui->readsTrackClearButton->setEnabled(true);

    //start the progress bar
    progressBar->setVisible(0);

    return;
}

void CSDesktop::on_stopCreateTracksProcessButton_clicked()
{
    if(this->readsTrackProcess.state() == QProcess::Running)
    {
        this->readsTrackProcess.kill();
        ui->createTracksProgressTextEdit->append("<font color=red> Create tracks process was terminated by the user.</font>");
        this->readsTrackProcess.setFromError(true);
    }
    if(this->peaksTrackProcess.state() == QProcess::Running)
    {
        this->peaksTrackProcess.kill();
        ui->createTracksProgressTextEdit->append("<font color=red> Create tracks process was terminated by the user.</font>");
        this->peaksTrackProcess.setFromError(true);
    }
    //disable stop button
    ui->stopCreateTracksProcessButton->setEnabled(false);
    //enable run buttons
    ui->createReadsTrackButton->setEnabled(true);
    ui->createPeaksTrackButton->setEnabled(true);
    //enable clear buttons
    ui->readsTrackClearButton->setEnabled(true);
    ui->peaksTrackClearButton->setEnabled(true);

    //start the progress bar
    progressBar->setVisible(0);

    return;
}

//
// Functions that handle the genomic distribution process
//

void CSDesktop::on_runGenomicDistButton_clicked()
{
    //clear the fromError variable
    this->genomicDistProcess.setFromError(false);

    //clear the text edit area
    ui->genomicDistProgressTextEdit->clear();

    //clear the targets results tab fields
    this->genomicDistTable->clear();
    this->showGenDistStatsTextEdit->clear();

    //get the database
    this->genomicDistProcess.setDatabase(ui->genomicDistDatabaseComboBox->currentText());

    //get the lenPD value
    this->genomicDistProcess.setLenPD(ui->genomicDistLenPDSpinBox->text().toDouble());

    //get the lenPU value
    this->genomicDistProcess.setLenPU(ui->genomicDistLenPUSpinBox->text().toDouble());

    //get the lenDWD value
    this->genomicDistProcess.setLenDWD(ui->genomicDistLenDWDSpinBox->text().toDouble());

    //get the lenDWU value
    this->genomicDistProcess.setLenDWU(ui->genomicDistLenDWUSpinBox->text().toDouble());

    //get the minDistAway value
    this->genomicDistProcess.setMinDistAway(ui->genomicDistMindistawaySpinBox->text().toDouble());

    //get the maxDistAway value
    this->genomicDistProcess.setMaxDistAway(ui->genomicDistMaxdistawaySpinBox->text().toDouble());

    //get the type
    if(ui->geneBasedRadioButton->isChecked())
        this->genomicDistProcess.setType(true);
    else if(ui->peakBasedRadioButton->isChecked())
        this->genomicDistProcess.setType(false);

    //get the peaks file
    QString tmpPeaksFile = ui->genomicDistSelectPeaksLineEdit->text();
    if(tmpPeaksFile.isNull() || tmpPeaksFile.isEmpty()) {
        //bring progress tab to front
        ui->genomicDistTabWidget->setCurrentIndex(1);
        //throw message
        ui->genomicDistProgressTextEdit->append("<font color=red> No peaks file was selected. Please try again. </font>");
        return;
    }
    else {

        //set the peaks file
        this->genomicDistProcess.setPeaksFile(tmpPeaksFile);

        //set the outfile name
        this->genomicDistProcess.setOutputFile(ui->genomicDistOutputFileLineEdit->text());

        if(this->genomicDistProcess.getOutputFile().isNull() || this->genomicDistProcess.getOutputFile().isEmpty()) {
            //bring progress tab to front
            ui->genomicDistTabWidget->setCurrentIndex(1);
            //throw message
            ui->genomicDistProgressTextEdit->append("<font color=red> No output name was selected. Please try again. </font>");
            return;
        }

        QString lenpu;
        QString lenpd;
        QString lendwu;
        QString lendwd;
        QString mindistaway;
        QString maxdistaway;
        QString type;

        lenpu.setNum(this->genomicDistProcess.getLenPU());
        lenpd.setNum(this->genomicDistProcess.getLenPD());
        lendwu.setNum(this->genomicDistProcess.getLenDWU());
        lendwd.setNum(this->genomicDistProcess.getLenDWD());
        mindistaway.setNum(this->genomicDistProcess.getMinDistAway());
        maxdistaway.setNum(this->genomicDistProcess.getMaxDistAway());

        if(genomicDistProcess.getType()) {
            type = "1";
        }
        else {
            type = "0";
        }

        //get the species
        QString tmpSpecies = ui->genomicDistSpeciesComboBox->currentText();
        QString genome;

        if(tmpSpecies == "Homo sapiens (hg18)") {
            genome = "hg18";
        }
        else if(tmpSpecies == "Homo sapiens (hg19)") {
            genome = "hg19";
        }
        else if(tmpSpecies == "Mus musculus (mm9)") {
            genome = "mm9";
        }
        else if(tmpSpecies == "Mus musculus (mm10)") {
            genome = "mm10";
        }
        else if(tmpSpecies == "Drosophila melanogaster (dm3)") {
            genome = "dm3";
        }
        else if(tmpSpecies == "Rattus norvegicus (rn4)") {
            genome = "rn4";
        }
        else if(tmpSpecies == "Zebrafish (zv9)") {
            genome = "zv9";
        }
        else if(tmpSpecies == "Saccharomyces cerevisiae (sacCer2)") {
            genome = "sacser";
        }

        //set the arguments
        QStringList inputArguments;
        inputArguments << ANNOTATE_PROGRAM << "--type" << "GeneParts"
                       << "--targets" << this->genomicDistProcess.getPeaksFile()
                       << "--db" << this->genomicDistProcess.getDatabase()
                       << "--prefix" << this->genomicDistProcess.getOutputFile()
                       << "--lenuP" << lenpu
                       << "--lendP" << lenpd
                       << "--lenuDW" << lendwu
                       << "--lendDW" << lendwd
                       << "--mindistaway" << mindistaway
                       << "--maxdistal" << maxdistaway
                       << "--gp_type" << type
                       << "--genome" << genome
                       << "--fromgui" << "1";

        //start process for INPUT data
        this->genomicDistProcess.start(PERL, inputArguments);

        //start the progress bar
        progressBar->setVisible(1);

        //throw start message
        ui->genomicDistProgressTextEdit->append("<font color=green> Genomic distribution process for peaks has started. </font>");
    }
    //bring progress tab to front
    ui->genomicDistTabWidget->setCurrentIndex(1);
    //enable stop button
    ui->stopRunGenomicDistProcessButton->setEnabled(true);
    //disable run button
    ui->runGenomicDistButton->setEnabled(false);
    //disable clear button
    ui->runGenomicDistClearButton->setEnabled(false);
}

void CSDesktop::readOutputFromGenomicDistProcess()
{
    ui->genomicDistProgressTextEdit->append(this->genomicDistProcess.readAllStandardOutput());
}

void CSDesktop::readErrorFromGenomicDistProcess()
{
    ui->genomicDistProgressTextEdit->append("<font color=red>" +
                                            this->genomicDistProcess.readAllStandardError() + "</font>");
    if(this->genomicDistProcess.state() == QProcess::Running)
    {
        this->genomicDistProcess.kill();
        //disable stop button
        ui->stopRunGenomicDistProcessButton->setEnabled(false);
        //enable run button
        ui->runGenomicDistButton->setEnabled(true);
        //enable clear button
        ui->runGenomicDistClearButton->setEnabled(true);
        //throw end message
        ui->genomicDistProgressTextEdit->append("<font color=red> Genomic distribution process terminated.</font>");

        this->genomicDistProcess.setFromError(true);

        //start the progress bar
        progressBar->setVisible(0);
    }
}

void CSDesktop::onGenomicDistProcessFinished()
{
    if(this->genomicDistProcess.state() == QProcess::Running)
    {
        this->genomicDistProcess.kill();
    }

    if(!(this->genomicDistProcess.getFromError()))
    {
        //throw message
        ui->genomicDistProgressTextEdit->append("<font color=green> Genomic distribution process finished successfully.</font>");
        ui->genomicDistProgressTextEdit->append("<font color=green> The results are in " + this->genomicDistProcess.getOutputFile()
                                                + "."+this->genomicDistProcess.getDatabase()+".GP" + "</font>");
        ui->genomicDistProgressTextEdit->append("<font color=green> The statistics are in " + this->genomicDistProcess.getOutputFile()
                                                + "."+this->genomicDistProcess.getDatabase()+".GP.stats" + "</font>");
        //show results in new tab
        ui->genomicDistTabWidget->addTab(showGenDistTab, "Results");
        ui->genomicDistTabWidget->setCurrentIndex(ui->genomicDistTabWidget->indexOf(showGenDistTab));

        genomicDistTable->setGeometry(QRect(0, 23, 641, 381));
        genomicDistTable->show();

        genDistStatsLabel->setObjectName(QString::fromUtf8("genesStatsLabel"));
        genDistStatsLabel->setText("Information");
        genDistStatsLabel->setGeometry(QRect(0, 406, 641, 21));
        genDistStatsLabel->show();

        showGenDistStatsTextEdit->setGeometry(QRect(0, 429, 641, 101));
        showGenDistStatsTextEdit->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);
        showGenDistStatsTextEdit->show();

        genomicDistCopyButton->setGeometry(QRect(0, 535, 100, 21));
        genomicDistCopyButton->setText("Copy table");
        genomicDistCopyButton->show();
        genomicDistCopyButton->connect(genomicDistCopyButton, SIGNAL(clicked()), this, SLOT(genomicDistTableCopy()));
        genomicDistCopyButton->setStatusTip("Copy table contents to clipboard");
        genomicDistCopyButton->setToolTip("Copy table contents to clipboard");

        // create the Pie chart tab
        ui->genomicDistTabWidget->addTab(showPieChartTab, "Pie Chart");
        ui->genomicDistTabWidget->setCurrentIndex(ui->genomicDistTabWidget->indexOf(showPieChartTab));

        // open the file to count its lines
        QFile gfile(this->genomicDistProcess.getOutputFile()+"."+this->genomicDistProcess.getDatabase()+".GP");
        if (!gfile.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

        QTextStream inL(&gfile);

        int lines = 0;
        //count lines in the file
        while (!inL.atEnd())
        {
            QString gline = inL.readLine();
            lines++;
        }

        gfile.close();

        //set the table's rows and columns
        genomicDistTable->setRowCount(lines);
        genomicDistTable->setColumnCount(5);
        QStringList headerNames;
        headerNames << "Chromosome" << "Start" << "End" <<"# of overlaps" <<"Overlaps";
        genomicDistTable->setHorizontalHeaderLabels(headerNames);

        //open the file to add lines in the table
        QFile gefile(this->genomicDistProcess.getOutputFile()+"."+this->genomicDistProcess.getDatabase()+".GP");
        if (!gefile.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

        QTextStream in(&gefile);

        //fill in the table with each row
        while (!in.atEnd())
        {
            for( int r = 0; r < genomicDistTable->rowCount(); r++ ){

                QString gline           = in.readLine();
                QStringList lineList    = gline.split("\t", QString::SkipEmptyParts);

                for( int c = 0; c < genomicDistTable->columnCount(); ++c ){

                    QTableWidgetItem *temp = new QTableWidgetItem(lineList.value(c), 0);
                    temp->setText(lineList.value(c));
                    genomicDistTable->setItem(r, c, temp);
                }
            }
        }

        gefile.close();

        genomicDistTable->connect(genomicDistTable, SIGNAL(cellDoubleClicked(int,int)), this, SLOT(genomicDistTableOpenBrowser(int)) );

        //open stats file and print in text edit
        QFile tfile(this->genomicDistProcess.getOutputFile()+"."+this->genomicDistProcess.getDatabase()+".GP.stats");
        if (!tfile.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

        QTextStream inT(&tfile);
        while (!inT.atEnd())
        {
            QString tline = inT.readLine();
            showGenDistStatsTextEdit->append(tline);
        }

        tfile.close();

        //make layout and model for the pieChart
        QGridLayout *layout = new QGridLayout(showPieChartTab);
        pieChartModel       = new QStandardItemModel(6, 2, this);

        //first remove previous widget
        if (showPieChartTab->layout()->indexOf(pieChart) != -1) {
            showPieChartTab->layout()->removeWidget(pieChart);
        }

        //make new pieChart
        pieChart            = new PieView();

        //add pieChart to layout and set its model
        showPieChartTab->layout()->addWidget(pieChart);
        pieChart->setModel(pieChartModel);

        //set the model for the view's selected items
        QItemSelectionModel *pieChartSelectionModel = new QItemSelectionModel(pieChartModel);
        pieChart->setSelectionModel(pieChartSelectionModel);

        //open stats file again
        QFile pfile(this->genomicDistProcess.getOutputFile()+"."+this->genomicDistProcess.getDatabase()+".GP.stats");
        if (!pfile.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

        //customize the pie chart
        int row             = 0;
        int modelRow        = 0;
        QString promsLabel  = "Promoters [" + QString::number(this->genomicDistProcess.getLenPU()) + "bp, "
                + QString::number(this->genomicDistProcess.getLenPD()) + "bp]";
        QString dExtrLabel  = "Downstream Extremities [" + QString::number(this->genomicDistProcess.getLenDWU()) + "bp, "
                + QString::number(this->genomicDistProcess.getLenDWD()) + "bp]";
        QString distLabel  = "Distal (>" + QString::number(this->genomicDistProcess.getMinDistAway()) + "bp <" + QString::number(this->genomicDistProcess.getMaxDistAway()) + "bp)";
        QString interLabel  = "Intergenic (>" + QString::number(this->genomicDistProcess.getMaxDistAway()) + "bp)";
        QString names[6]    = {promsLabel, dExtrLabel, "Exons", "Introns", distLabel, interLabel};
        QColor colors[6]    = {"Salmon", "LemonChiffon", "SkyBlue", "OliveDrab", "Gold", "CadetBlue"};

        QString values[6];
        QString prct[6];

        QTextStream inP(&pfile);

        while (!inP.atEnd())
        {
            pieChartModel->removeRows(0, pieChartModel->rowCount(QModelIndex()), QModelIndex());

            QString line = inP.readLine();

            //print only some of the lines in the file
            if(row >= 1 && row <= 6) {
                if (!line.isEmpty()) {
                    QStringList lineList    = line.split("\t", QString::SkipEmptyParts);
                    values[modelRow]        = lineList.value(1);
                    prct[modelRow]          = lineList.value(2);
                    modelRow++;
                }
            }
            row++;
        }

        pfile.close();


        for(int j=0; j<modelRow; j++) {

            QString tmp = prct[j] + ": " + names[j];

            //add new row in model
            pieChartModel->insertRows(j, 1, QModelIndex());
            //set name
            pieChartModel->setData(pieChartModel->index(j, 0, QModelIndex()), tmp);
            //set name
            pieChartModel->setData(pieChartModel->index(j, 1, QModelIndex()), values[j]);
            //set name
            pieChartModel->setData(pieChartModel->index(j, 0, QModelIndex()), colors[j], Qt::DecorationRole);
        }
    }

    //disable stop button
    ui->stopRunGenomicDistProcessButton->setEnabled(false);
    //enable run button
    ui->runGenomicDistButton->setEnabled(true);
    //enable clear button
    ui->runGenomicDistClearButton->setEnabled(true);

    //start the progress bar
    progressBar->setVisible(0);

    return;
}

void CSDesktop::on_stopRunGenomicDistProcessButton_clicked()
{
    if(this->genomicDistProcess.state() == QProcess::Running)
    {
        this->genomicDistProcess.kill();
        ui->genomicDistProgressTextEdit->append("<font color=red> Genomic distribution process was terminated by the user.</font>");
    }
    //disable stop button
    ui->stopRunGenomicDistProcessButton->setEnabled(false);
    //enable run button
    ui->runGenomicDistButton->setEnabled(true);
    //enable clear button
    ui->runGenomicDistClearButton->setEnabled(true);

    this->genomicDistProcess.setFromError(true);

    //start the progress bar
    progressBar->setVisible(0);

    return;
}

//
// Functions that handle the find RNA genes process
//

void CSDesktop::on_runFindRNAGenesButton_clicked()
{
    //clear the fromError variable
    this->rnaGenesProcess.setFromError(false);

    //clear the text edit area
    ui->rnaGenesProgressTextEdit->clear();

    //clear the targets results tab fields
    this->rnaGenesTable->clear();
    this->showRnaGenesStatsTextEdit->clear();

    //get the peaks file
    QString tmpPeaksFile = ui->rnaGenesSelectPeaksLineEdit->text();
    if(tmpPeaksFile.isNull() || tmpPeaksFile.isEmpty()) {
        //bring progress tab to front
        ui->rnaGenesTabWidget->setCurrentIndex(1);
        //throw message
        ui->rnaGenesProgressTextEdit->append("<font color=red> No peaks file was selected. Please try again. </font>");
        return;
    }
    else {
        //set the peaks file
        this->rnaGenesProcess.setPeaksFile(tmpPeaksFile);

        //set the outfile name
        this->rnaGenesProcess.setOutputFile(ui->rnaGenesOutputFileLineEdit->text());

        if(this->rnaGenesProcess.getOutputFile().isNull() || this->rnaGenesProcess.getOutputFile().isEmpty()) {
            //bring progress tab to front
            ui->rnaGenesTabWidget->setCurrentIndex(1);
            //throw message
            ui->rnaGenesProgressTextEdit->append("<font color=red> No output name was selected. Please try again. </font>");
            return;
        }

        //set the arguments
        QStringList inputArguments;
        inputArguments << RNAGENESANNOTATE_PROGRAM
                       << "--targets" << this->rnaGenesProcess.getPeaksFile()
                       << "--outfile" << this->rnaGenesProcess.getOutputFile();

        //start process for INPUT data
        this->rnaGenesProcess.start(PERL, inputArguments);

        //start the progress bar
        progressBar->setVisible(1);

        //throw start message
        ui->rnaGenesProgressTextEdit->append("<font color=green> Find RNAGenes process for peaks has started. </font>");
    }
    //bring progress tab to front
    ui->rnaGenesTabWidget->setCurrentIndex(1);
    //enable stop button
    ui->stopRunFindRNAGenesProcessButton->setEnabled(true);
    //disable run button
    ui->runFindRNAGenesButton->setEnabled(false);
    //disable clear button
    ui->runFindRNAGenesClearButton->setEnabled(false);
}

void CSDesktop::readOutputFromFindRnaGenesProcess()
{
    ui->rnaGenesProgressTextEdit->append(this->rnaGenesProcess.readAllStandardOutput());
}

void CSDesktop::readErrorFromFindRnaGenesProcess()
{
    ui->rnaGenesProgressTextEdit->append("<font color=red>" +
                                         this->rnaGenesProcess.readAllStandardError() + "</font>");
    if(this->rnaGenesProcess.state() == QProcess::Running)
    {
        this->rnaGenesProcess.kill();
        //disable stop button
        ui->stopRunFindRNAGenesProcessButton->setEnabled(false);
        //enable run button
        ui->runFindRNAGenesButton->setEnabled(true);
        //enable clear button
        ui->runFindRNAGenesClearButton->setEnabled(true);
        //throw end message
        ui->rnaGenesProgressTextEdit->append("<font color=red> Find RNAGenes process terminated.</font>");

        this->rnaGenesProcess.setFromError(true);

        //start the progress bar
        progressBar->setVisible(0);
    }
}

void CSDesktop::onRnaGenesProcessFinished()
{
    if(this->rnaGenesProcess.state() == QProcess::Running)
    {
        this->rnaGenesProcess.kill();
    }

    if(!(this->rnaGenesProcess.getFromError()))
    {
        //throw message
        ui->rnaGenesProgressTextEdit->append("<font color=green> Find RNA genes process finished successfully.</font>");
        ui->rnaGenesProgressTextEdit->append("<font color=green> The results are in " + this->rnaGenesProcess.getOutputFile()
                                             + ".RNAG" + "</font>");
        ui->rnaGenesProgressTextEdit->append("<font color=green> The statistics are in " + this->rnaGenesProcess.getOutputFile()
                                             + ".RNAG.stats" + "</font>");

        //show results in new tab
        ui->rnaGenesTabWidget->addTab(showRnaGenesTab, "Results");
        ui->rnaGenesTabWidget->setCurrentIndex(ui->rnaGenesTabWidget->indexOf(showRnaGenesTab));

        rnaGenesTable->setGeometry(QRect(0, 23, 641, 381));
        rnaGenesTable->show();

        rnaGenesStatsLabel->setObjectName(QString::fromUtf8("genesStatsLabel"));
        rnaGenesStatsLabel->setText("Statistics");
        rnaGenesStatsLabel->setGeometry(QRect(0, 406, 641, 21));
        rnaGenesStatsLabel->show();

        showRnaGenesStatsTextEdit->setGeometry(QRect(0, 429, 641, 101));
        showRnaGenesStatsTextEdit->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);
        showRnaGenesStatsTextEdit->show();

        rnaGenesCopyButton->setGeometry(QRect(0, 540, 100, 21));
        rnaGenesCopyButton->setText("Copy table");
        rnaGenesCopyButton->show();
        rnaGenesCopyButton->connect(rnaGenesCopyButton, SIGNAL(clicked()), this, SLOT(rnaGenesTableCopy()));
        rnaGenesCopyButton->setStatusTip("Copy table contents to clipboard");
        rnaGenesCopyButton->setToolTip("Copy table contents to clipboard");

        QFile gfile(this->rnaGenesProcess.getOutputFile()+".RNAG");
        if (!gfile.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

        QTextStream inL(&gfile);

        int lines = 0;
        //count lines in the file
        while (!inL.atEnd())
        {
            QString gline = inL.readLine();
            lines++;
        }

        gfile.close();

        //set the table's rows and columns
        rnaGenesTable->setRowCount(lines);
        rnaGenesTable->setColumnCount(5);
        QStringList headerNames;
        headerNames << "Chromosome" << "Start" << "End" << "Num. of overlap RNA genes" << "Overlap RNA genes";
        rnaGenesTable->setHorizontalHeaderLabels(headerNames);

        //open the file to add lines in the table
        QFile gefile(this->rnaGenesProcess.getOutputFile()+".RNAG");
        if (!gefile.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

        QTextStream in(&gefile);

        //fill in the table with each row
        while (!in.atEnd())
        {
            for( int r = 0; r < rnaGenesTable->rowCount(); r++ ){

                QString gline           = in.readLine();
                QStringList lineList    = gline.split("\t", QString::SkipEmptyParts);

                for( int c = 0; c < rnaGenesTable->columnCount(); ++c ){

                    QTableWidgetItem *temp = new QTableWidgetItem(lineList.value(c), 0);
                    temp->setText(lineList.value(c));
                    rnaGenesTable->setItem(r, c, temp);
                }
            }
        }

        gefile.close();

        rnaGenesTable->connect(rnaGenesTable, SIGNAL(cellDoubleClicked(int,int)), this, SLOT(rnaGenesTableOpenBrowser(int)) );

        //open stats file and print in text edit
        QFile tfile(this->rnaGenesProcess.getOutputFile()+".RNAG.stats");
        if (!tfile.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

        QTextStream inT(&tfile);
        while (!inT.atEnd())
        {
            QString tline = inT.readLine();
            showRnaGenesStatsTextEdit->append(tline);
        }

        tfile.close();
    }

    //disable stop button
    ui->stopRunFindRNAGenesProcessButton->setEnabled(false);
    //enable run button
    ui->runFindRNAGenesButton->setEnabled(true);
    //enable clear button
    ui->runFindRNAGenesClearButton->setEnabled(true);

    //start the progress bar
    progressBar->setVisible(0);

    return;
}

void CSDesktop::on_stopRunFindRNAGenesProcessButton_clicked()
{
    if(this->rnaGenesProcess.state() == QProcess::Running)
    {
        this->rnaGenesProcess.kill();
        ui->rnaGenesProgressTextEdit->append("<font color=red> Find RNAGenes process was terminated by the user.</font>");
    }
    //disable stop button
    ui->stopRunFindRNAGenesProcessButton->setEnabled(false);
    //enable run button
    ui->runFindRNAGenesButton->setEnabled(true);
    //enable clear button
    ui->runFindRNAGenesClearButton->setEnabled(true);

    this->rnaGenesProcess.setFromError(true);

    //start the progress bar
    progressBar->setVisible(0);

    return;
}


//
// Functions that handle the find repeats process
//

void CSDesktop::on_runFindRepeatsButton_clicked()
{
    //clear the fromError variable
    this->repeatsProcess.setFromError(false);

    //clear the text edit area
    ui->repeatsProgressTextEdit->clear();

    //clear the targets results tab fields
    this->repeatsTable->clear();
    this->showRepeatsStatsTextEdit->clear();

    QString tmpSpecies = ui->repeatsSpeciesComboBox->currentText();
    QString genome;

    if(tmpSpecies == "Homo sapiens (hg18)") {
        genome = "hg18";
    }
    else if(tmpSpecies == "Homo sapiens (hg19)") {
        genome = "hg19";
    }
    else if(tmpSpecies == "Mus musculus (mm9)") {
        genome = "mm9";
    }
    else if(tmpSpecies == "Mus musculus (mm10)") {
        genome = "mm10";
    }
    else if(tmpSpecies == "Drosophila melanogaster (dm3)") {
        genome = "dm3";
    }
    else if(tmpSpecies == "Rattus norvegicus (rn4)") {
        genome = "rn4";
    }
    else if(tmpSpecies == "Zebrafish (zv9)") {
        genome = "zv9";
    }
    else if(tmpSpecies == "Saccharomyces cerevisiae (sacCer2)") {
        genome = "sacser";
    }

    //get the peaks file
    QString tmpPeaksFile = ui->repeatsSelectPeaksLineEdit->text();
    if(tmpPeaksFile.isNull() || tmpPeaksFile.isEmpty()) {
        //bring progress tab to front
        ui->repeatsTabWidget->setCurrentIndex(1);
        //throw message
        ui->repeatsProgressTextEdit->append("<font color=red> No peaks file was selected. Please try again. </font>");
        return;
    }
    else {

        //set the peaks file
        this->repeatsProcess.setPeaksFile(tmpPeaksFile);

        //set the outfile name
        this->repeatsProcess.setOutputFile(ui->repeatsOutputFileLineEdit->text());

        if(this->repeatsProcess.getOutputFile().isNull() || this->repeatsProcess.getOutputFile().isEmpty()) {
            //bring progress tab to front
            ui->repeatsTabWidget->setCurrentIndex(1);
            //throw message
            ui->repeatsProgressTextEdit->append("<font color=red> No output name was selected. Please try again. </font>");
            return;
        }

        //set the arguments
        QStringList inputArguments;
        inputArguments << NONGENICANNOTATE_PROGRAM << "--type" << "RepMasker"
                       << "--targets" << this->repeatsProcess.getPeaksFile()
                       << "--prefix" << this->repeatsProcess.getOutputFile()
                       << "--genome" << genome;

        //start process for INPUT data
        this->repeatsProcess.start(PERL, inputArguments);

        //start the progress bar
        progressBar->setVisible(1);

        //throw start message
        ui->repeatsProgressTextEdit->append("<font color=green> Find repeats process for peaks has started. </font>");
    }
    //bring progress tab to front
    ui->repeatsTabWidget->setCurrentIndex(1);
    //enable stop button
    ui->stopRunFindRepeatsProcessButton->setEnabled(true);
    //disable run button
    ui->runFindRepeatsButton->setEnabled(false);
    //disable clear button
    ui->runFindRepeatsClearButton->setEnabled(false);
}

void CSDesktop::readOutputFromRepeatsProcess()
{
    ui->repeatsProgressTextEdit->append(this->repeatsProcess.readAllStandardOutput());
}

void CSDesktop::readErrorFromRepeatsProcess()
{
    ui->repeatsProgressTextEdit->append("<font color=red>" +
                                        this->repeatsProcess.readAllStandardError() + "</font>");
    if(this->repeatsProcess.state() == QProcess::Running)
    {
        this->repeatsProcess.kill();
        //disable stop button
        ui->stopRunFindRepeatsProcessButton->setEnabled(false);
        //enable run button
        ui->runFindRepeatsButton->setEnabled(true);
        //enable clear button
        ui->runFindRepeatsClearButton->setEnabled(true);
        //throw end message
        ui->repeatsProgressTextEdit->append("<font color=red> Find repeats process terminated.</font>");

        this->repeatsProcess.setFromError(true);

        //start the progress bar
        progressBar->setVisible(0);
    }
}

void CSDesktop::onRepeatsProcessFinished()
{
    if(this->repeatsProcess.state() == QProcess::Running)
    {
        this->repeatsProcess.kill();
    }

    if(!(this->repeatsProcess.getFromError()))
    {
        //throw message
        ui->repeatsProgressTextEdit->append("<font color=green> Find repeats process finished successfully.</font>");
        ui->repeatsProgressTextEdit->append("<font color=green> The results are in " + this->repeatsProcess.getOutputFile()
                                            + ".RM" + "</font>");
        ui->repeatsProgressTextEdit->append("<font color=green> The statistics are in " + this->repeatsProcess.getOutputFile()
                                            + ".RM.stats" + "</font>");

        //show results in new tab
        ui->repeatsTabWidget->addTab(showRepeatsTab, "Results");
        ui->repeatsTabWidget->setCurrentIndex(ui->repeatsTabWidget->indexOf(showRepeatsTab));

        repeatsTable->setGeometry(QRect(0, 23, 641, 381));
        repeatsTable->show();

        repeatsStatsLabel->setObjectName(QString::fromUtf8("genesStatsLabel"));
        repeatsStatsLabel->setText("Statistics");
        repeatsStatsLabel->setGeometry(QRect(0, 406, 641, 21));
        repeatsStatsLabel->show();

        showRepeatsStatsTextEdit->setGeometry(QRect(0, 429, 641, 101));
        showRepeatsStatsTextEdit->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);
        showRepeatsStatsTextEdit->show();

        repeatsCopyButton->setGeometry(QRect(0, 540, 100, 21));
        repeatsCopyButton->setText("Copy table");
        repeatsCopyButton->show();
        repeatsCopyButton->connect(repeatsCopyButton, SIGNAL(clicked()), this, SLOT(repeatsTableCopy()));
        repeatsCopyButton->setStatusTip("Copy table contents to clipboard");
        repeatsCopyButton->setToolTip("Copy table contents to clipboard");

        QFile gfile(this->repeatsProcess.getOutputFile()+".RM");
        if (!gfile.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

        QTextStream inL(&gfile);

        int lines = 0;
        //count lines in the file
        while (!inL.atEnd())
        {
            QString gline = inL.readLine();
            lines++;
        }


        gfile.close();

        //set the table's rows and columns
        repeatsTable->setRowCount(lines);
        repeatsTable->setColumnCount(5);
        QStringList headerNames;
        headerNames << "Chromosome" << "Start" << "End" << "Num. of overlap repeats" << "Overlap repeats";
        repeatsTable->setHorizontalHeaderLabels(headerNames);

        //open the file to add lines in the table
        QFile gefile(this->repeatsProcess.getOutputFile()+".RM");
        if (!gefile.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

        QTextStream in(&gefile);

        //fill in the table with each row
        while (!in.atEnd())
        {
            for( int r = 0; r < repeatsTable->rowCount(); r++ ){

                QString gline           = in.readLine();
                QStringList lineList    = gline.split("\t", QString::SkipEmptyParts);

                for( int c = 0; c < repeatsTable->columnCount(); ++c ){

                    QTableWidgetItem *temp = new QTableWidgetItem(lineList.value(c), 0);
                    temp->setText(lineList.value(c));
                    repeatsTable->setItem(r, c, temp);
                }
            }
        }

        gefile.close();

        repeatsTable->connect(repeatsTable, SIGNAL(cellDoubleClicked(int,int)), this, SLOT(repeatsTableOpenBrowser(int)) );

        //open stats file and print in text edit
        QFile tfile(this->repeatsProcess.getOutputFile()+".RM.stats");
        if (!tfile.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

        QTextStream inT(&tfile);
        while (!inT.atEnd())
        {
            QString tline = inT.readLine();
            showRepeatsStatsTextEdit->append(tline);
        }

        tfile.close();
    }

    //disable stop button
    ui->stopRunFindRepeatsProcessButton->setEnabled(false);
    //enable run button
    ui->runFindRepeatsButton->setEnabled(true);
    //enable clear button
    ui->runFindRepeatsClearButton->setEnabled(true);

    //start the progress bar
    progressBar->setVisible(0);

    return;
}

void CSDesktop::on_stopRunFindRepeatsProcessButton_clicked()
{
    if(this->repeatsProcess.state() == QProcess::Running)
    {
        this->repeatsProcess.kill();
        ui->repeatsProgressTextEdit->append("<font color=red> Find repeats process was terminated by the user.</font>");
    }
    //disable stop button
    ui->stopRunFindRepeatsProcessButton->setEnabled(false);
    //enable run button
    ui->runFindRepeatsButton->setEnabled(true);
    //enable clear button
    ui->runFindRepeatsClearButton->setEnabled(true);

    this->repeatsProcess.setFromError(true);

    //start the progress bar
    progressBar->setVisible(0);

    return;
}

//
// Functions that handle the find CpG Islands process
//

void CSDesktop::on_runFindCpgIslandsButton_clicked()
{
    //clear the fromError variable
    this->cpgIslandsProcess.setFromError(false);

    //clear the text edit area
    ui->cpgIslandsProgressTextEdit->clear();

    //clear the targets results tab fields
    this->cpgIslandsTable->clear();
    this->showCpgIslandsStatsTextEdit->clear();

    QString tmpSpecies = ui->repeatsSpeciesComboBox->currentText();
    QString genome;

    if(tmpSpecies == "Homo sapiens (hg18)") {
        genome = "hg18";
    }
    else if(tmpSpecies == "Homo sapiens (hg19)") {
        genome = "hg19";
    }
    else if(tmpSpecies == "Mus musculus (mm9)") {
        genome = "mm9";
    }
    else if(tmpSpecies == "Mus musculus (mm10)") {
        genome = "mm10";
    }
    else if(tmpSpecies == "Drosophila melanogaster (dm3)") {
        genome = "dm3";
    }
    else if(tmpSpecies == "Rattus norvegicus (rn4)") {
        genome = "rn4";
    }
    else if(tmpSpecies == "Zebrafish (zv9)") {
        genome = "zv9";
    }
    else if(tmpSpecies == "Saccharomyces cerevisiae (sacCer2)") {
        genome = "sacser";
    }

    //get the peaks file
    QString tmpPeaksFile = ui->cpgIslandsSelectPeaksLineEdit->text();
    if(tmpPeaksFile.isNull() || tmpPeaksFile.isEmpty()) {
        //bring progress tab to front
        ui->cpgIslandsTabWidget->setCurrentIndex(1);
        //throw message
        ui->cpgIslandsProgressTextEdit->append("<font color=red> No peaks file was selected. Please try again. </font>");
        return;
    }
    else {

        //set the peaks file
        this->cpgIslandsProcess.setPeaksFile(tmpPeaksFile);

        //set the outfile name
        this->cpgIslandsProcess.setOutputFile(ui->cpgIslandsOutputFileLineEdit->text());

        if(this->cpgIslandsProcess.getOutputFile().isNull() || this->cpgIslandsProcess.getOutputFile().isEmpty()) {
            //bring progress tab to front
            ui->cpgIslandsTabWidget->setCurrentIndex(1);
            //throw message
            ui->cpgIslandsProgressTextEdit->append("<font color=red> No output name was selected. Please try again. </font>");
            return;
        }

        //set the arguments
        QStringList inputArguments;
        inputArguments << NONGENICANNOTATE_PROGRAM << "--type" << "CpGislands"
                       << "--targets" << this->cpgIslandsProcess.getPeaksFile()
                       << "--prefix" << this->cpgIslandsProcess.getOutputFile()
                       << "--genome" << genome;

        //start process for INPUT data
        this->cpgIslandsProcess.start(PERL, inputArguments);

        //start the progress bar
        progressBar->setVisible(1);

        //throw start message
        ui->cpgIslandsProgressTextEdit->append("<font color=green> Find CpG islands process for peaks has started. </font>");
    }
    //bring progress tab to front
    ui->cpgIslandsTabWidget->setCurrentIndex(1);
    //enable stop button
    ui->stopRunFindCpgIslandsProcessButton->setEnabled(true);
    //disable run button
    ui->runFindCpgIslandsButton->setEnabled(false);
    //disable clear button
    ui->runFindCpgIslandsClearButton->setEnabled(false);
}

void CSDesktop::readOutputFromCpgIslandsProcess()
{
    ui->cpgIslandsProgressTextEdit->append(this->cpgIslandsProcess.readAllStandardOutput());
}

void CSDesktop::readErrorFromCpgIslandsProcess()
{
    ui->cpgIslandsProgressTextEdit->append("<font color=red>" +
                                           this->cpgIslandsProcess.readAllStandardError() + "</font>");
    if(this->cpgIslandsProcess.state() == QProcess::Running)
    {
        this->cpgIslandsProcess.kill();
        //disable stop button
        ui->stopRunFindCpgIslandsProcessButton->setEnabled(false);
        //enable run button
        ui->runFindCpgIslandsButton->setEnabled(true);
        //enable clear button
        ui->runFindCpgIslandsClearButton->setEnabled(true);
        //throw end message
        ui->cpgIslandsProgressTextEdit->append("<font color=red> Find cpg islands process terminated.</font>");

        this->cpgIslandsProcess.setFromError(true);

        //start the progress bar
        progressBar->setVisible(0);
    }
}

void CSDesktop::onCpgIslandsProcessFinished()
{
    if(this->cpgIslandsProcess.state() == QProcess::Running)
    {
        this->cpgIslandsProcess.kill();
    }

    if(!(this->cpgIslandsProcess.getFromError()))
    {
        //throw message
        ui->cpgIslandsProgressTextEdit->append("<font color=green> Find CpG islands process finished successfully.</font>");
        ui->cpgIslandsProgressTextEdit->append("<font color=green> The results are in " + this->cpgIslandsProcess.getOutputFile()
                                               + ".CpG" + "</font>");
        ui->cpgIslandsProgressTextEdit->append("<font color=green> The statistics are in " + this->cpgIslandsProcess.getOutputFile()
                                               + ".CpG.stats" + "</font>");

        //show results in new tab
        ui->cpgIslandsTabWidget->addTab(showCpgIslandsTab, "Results");
        ui->cpgIslandsTabWidget->setCurrentIndex(ui->cpgIslandsTabWidget->indexOf(showCpgIslandsTab));

        cpgIslandsTable->setGeometry(QRect(0, 23, 641, 381));
        cpgIslandsTable->show();

        cpgIslandsStatsLabel->setObjectName(QString::fromUtf8("genesStatsLabel"));
        cpgIslandsStatsLabel->setText("Statistics");
        cpgIslandsStatsLabel->setGeometry(QRect(0, 406, 641, 21));
        cpgIslandsStatsLabel->show();

        showCpgIslandsStatsTextEdit->setGeometry(QRect(0, 429, 641, 101));
        showCpgIslandsStatsTextEdit->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);
        showCpgIslandsStatsTextEdit->show();

        cpgIslandsCopyButton->setGeometry(QRect(0, 540, 100, 21));
        cpgIslandsCopyButton->setText("Copy table");
        cpgIslandsCopyButton->show();
        cpgIslandsCopyButton->connect(cpgIslandsCopyButton, SIGNAL(clicked()), this, SLOT(cpgIslandsTableCopy()));
        cpgIslandsCopyButton->setStatusTip("Copy table contents to clipboard");
        cpgIslandsCopyButton->setToolTip("Copy table contents to clipboard");

        QFile gfile(this->cpgIslandsProcess.getOutputFile()+".CpG");
        if (!gfile.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

        QTextStream inL(&gfile);

        int lines = 0;
        //count lines in the file
        while (!inL.atEnd())
        {
            QString gline = inL.readLine();
            lines++;
        }

        gfile.close();

        //set the table's rows and columns
        cpgIslandsTable->setRowCount(lines);
        cpgIslandsTable->setColumnCount(5);
        QStringList headerNames;
        headerNames << "Chromosome" << "Start" << "End" << "Num. of overlap CpG islands" << "Overlap CpG islands";
        cpgIslandsTable->setHorizontalHeaderLabels(headerNames);

        //open the file to add lines in the table
        QFile gefile(this->cpgIslandsProcess.getOutputFile()+".CpG");
        if (!gefile.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

        QTextStream in(&gefile);

        //fill in the table with each row
        while (!in.atEnd())
        {
            for( int r = 0; r < cpgIslandsTable->rowCount(); r++ ){

                QString gline           = in.readLine();
                QStringList lineList    = gline.split("\t", QString::SkipEmptyParts);

                for( int c = 0; c < cpgIslandsTable->columnCount(); ++c ){

                    QTableWidgetItem *temp = new QTableWidgetItem(lineList.value(c), 0);
                    temp->setText(lineList.value(c));
                    cpgIslandsTable->setItem(r, c, temp);
                }
            }
        }

        gefile.close();

        cpgIslandsTable->connect(cpgIslandsTable, SIGNAL(cellDoubleClicked(int,int)), this, SLOT(cpgIslandsTableOpenBrowser(int)) );

        //open stats file and print in text edit
        QFile tfile(this->cpgIslandsProcess.getOutputFile()+".CpG.stats");
        if (!tfile.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

        QTextStream inT(&tfile);
        while (!inT.atEnd())
        {
            QString tline = inT.readLine();
            showCpgIslandsStatsTextEdit->append(tline);
        }

        tfile.close();
    }

    //disable stop button
    ui->stopRunFindCpgIslandsProcessButton->setEnabled(false);
    //enable run button
    ui->runFindCpgIslandsButton->setEnabled(true);
    //enable clear button
    ui->runFindCpgIslandsClearButton->setEnabled(true);

    //start the progress bar
    progressBar->setVisible(0);

    return;
}

void CSDesktop::on_stopRunFindCpgIslandsProcessButton_clicked()
{
    if(this->cpgIslandsProcess.state() == QProcess::Running)
    {
        this->cpgIslandsProcess.kill();
        ui->cpgIslandsProgressTextEdit->append("<font color=red> Find CpG islands process was terminated by the user.</font>");
    }
    //disable stop button
    ui->stopRunFindCpgIslandsProcessButton->setEnabled(false);
    //enable run button
    ui->runFindCpgIslandsButton->setEnabled(true);
    //enable clear button
    ui->runFindCpgIslandsClearButton->setEnabled(true);

    this->cpgIslandsProcess.setFromError(true);

    //start the progress bar
    progressBar->setVisible(0);

    return;
}

//
// Functions that handle the find seg. duplicates process
//

void CSDesktop::on_runFindSegDuplicatesButton_clicked()
{
    //clear the fromError variable
    this->segDuplicatesProcess.setFromError(false);

    //clear the text edit area
    ui->segDuplicatesProgressTextEdit->clear();

    //clear the targets results tab fields
    this->segDuplicatesTable->clear();
    this->showSegDupsStatsTextEdit->clear();

    QString tmpSpecies = ui->repeatsSpeciesComboBox->currentText();
    QString genome;

    if(tmpSpecies == "Homo sapiens (hg18)") {
        genome = "hg18";
    }
    else if(tmpSpecies == "Homo sapiens (hg19)") {
        genome = "hg19";
    }
    else if(tmpSpecies == "Mus musculus (mm9)") {
        genome = "mm9";
    }
    else if(tmpSpecies == "Mus musculus (mm10)") {
        genome = "mm10";
    }
    else if(tmpSpecies == "Drosophila melanogaster (dm3)") {
        genome = "dm3";
    }
    else if(tmpSpecies == "Rattus norvegicus (rn4)") {
        genome = "rn4";
    }
    else if(tmpSpecies == "Zebrafish (zv9)") {
        genome = "zv9";
    }
    else if(tmpSpecies == "Saccharomyces cerevisiae (sacCer2)") {
        genome = "sacser";
    }

    //get the peaks file
    QString tmpPeaksFile = ui->segDuplicatesSelectPeaksLineEdit->text();
    if(tmpPeaksFile.isNull() || tmpPeaksFile.isEmpty()) {
        //bring progress tab to front
        ui->segDuplicatesTabWidget->setCurrentIndex(1);
        //throw message
        ui->segDuplicatesProgressTextEdit->append("<font color=red> No peaks file was selected. Please try again. </font>");
        return;
    }
    else {
        //set the peaks file
        this->segDuplicatesProcess.setPeaksFile(tmpPeaksFile);

        //set the outfile name
        this->segDuplicatesProcess.setOutputFile(ui->segDuplicatesOutputFileLineEdit->text());

        if(this->segDuplicatesProcess.getOutputFile().isNull() || this->segDuplicatesProcess.getOutputFile().isEmpty()) {
            //bring progress tab to front
            ui->segDuplicatesTabWidget->setCurrentIndex(1);
            //throw message
            ui->segDuplicatesProgressTextEdit->append("<font color=red> No output name was selected. Please try again. </font>");
            return;
        }

        //set the arguments
        QStringList inputArguments;
        inputArguments << NONGENICANNOTATE_PROGRAM << "--type" << "SegmentalDups"
                       << "--targets" << this->segDuplicatesProcess.getPeaksFile()
                       << "--prefix" << this->segDuplicatesProcess.getOutputFile()
                       << "--genome" << genome;

        //start process for INPUT data
        this->segDuplicatesProcess.start(PERL, inputArguments);

        //start the progress bar
        progressBar->setVisible(1);

        //throw start message
        ui->segDuplicatesProgressTextEdit->append("<font color=green> Find segmental duplicates process for peaks has started. </font>");
    }
    //bring progress tab to front
    ui->segDuplicatesTabWidget->setCurrentIndex(1);
    //enable stop button
    ui->stopRunFindSegDuplicatesProcessButton->setEnabled(true);
    //disable run button
    ui->runFindSegDuplicatesButton->setEnabled(false);
    //disable clear button
    ui->runFindSegDuplicatesClearButton->setEnabled(false);
}

void CSDesktop::readOutputFromSegDuplicatesProcess()
{
    ui->segDuplicatesProgressTextEdit->append(this->segDuplicatesProcess.readAllStandardOutput());
}

void CSDesktop::readErrorFromSegDuplicatesProcess()
{
    ui->segDuplicatesProgressTextEdit->append("<font color=red>" +
                                              this->segDuplicatesProcess.readAllStandardError() + "</font>");
    if(this->segDuplicatesProcess.state() == QProcess::Running)
    {
        this->segDuplicatesProcess.kill();
        //disable stop button
        ui->stopRunFindSegDuplicatesProcessButton->setEnabled(false);
        //enable run button
        ui->runFindSegDuplicatesButton->setEnabled(true);
        //enable clear button
        ui->runFindSegDuplicatesClearButton->setEnabled(true);
        //throw end message
        ui->segDuplicatesProgressTextEdit->append("<font color=red> Find segmental duplicates process terminated.</font>");

        this->segDuplicatesProcess.setFromError(true);

        //start the progress bar
        progressBar->setVisible(0);
    }
}

void CSDesktop::onSegDuplicatesProcessFinished()
{
    if(this->segDuplicatesProcess.state() == QProcess::Running)
    {
        this->segDuplicatesProcess.kill();
    }

    if(!(this->segDuplicatesProcess.getFromError()))
    {
        //throw message
        ui->segDuplicatesProgressTextEdit->append("<font color=green> Find segmental duplicates process finished successfully.</font>");
        ui->segDuplicatesProgressTextEdit->append("<font color=green> The results are in " + this->segDuplicatesProcess.getOutputFile()
                                                  + ".DUP" + "</font>");
        ui->segDuplicatesProgressTextEdit->append("<font color=green> The statistics are in " + this->segDuplicatesProcess.getOutputFile()
                                                  + ".DUP.stats" + "</font>");

        //show results in new tab
        ui->segDuplicatesTabWidget->addTab(showSegDupsTab, "Results");
        ui->segDuplicatesTabWidget->setCurrentIndex(ui->segDuplicatesTabWidget->indexOf(showSegDupsTab));

        segDuplicatesTable->setGeometry(QRect(0, 23, 641, 381));
        segDuplicatesTable->show();

        segDupsStatsLabel->setObjectName(QString::fromUtf8("genesStatsLabel"));
        segDupsStatsLabel->setText("Statistics");
        segDupsStatsLabel->setGeometry(QRect(0, 406, 641, 21));
        segDupsStatsLabel->show();

        showSegDupsStatsTextEdit->setGeometry(QRect(0, 429, 641, 101));
        showSegDupsStatsTextEdit->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);
        showSegDupsStatsTextEdit->show();

        segDuplicatesCopyButton->setGeometry(QRect(0, 540, 100, 21));
        segDuplicatesCopyButton->setText("Copy table");
        segDuplicatesCopyButton->show();
        segDuplicatesCopyButton->connect(segDuplicatesCopyButton, SIGNAL(clicked()), this, SLOT(segDuplicatesTableCopy()));
        segDuplicatesCopyButton->setStatusTip("Copy table contents to clipboard");
        segDuplicatesCopyButton->setToolTip("Copy table contents to clipboard");

        QFile gfile(this->segDuplicatesProcess.getOutputFile()+".DUP");
        if (!gfile.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

        QTextStream inL(&gfile);

        int lines = 0;
        //count lines in the file
        while (!inL.atEnd())
        {
            QString gline = inL.readLine();
            lines++;
        }

        gfile.close();

        //set the table's rows and columns
        segDuplicatesTable->setRowCount(lines);
        segDuplicatesTable->setColumnCount(5);
        QStringList headerNames;
        headerNames << "Chromosome" << "Start" << "End" << "Num. of overlap seg. duplicates" << "Overlap seg. duplicates";
        segDuplicatesTable->setHorizontalHeaderLabels(headerNames);

        //open the file to add lines in the table
        QFile gefile(this->segDuplicatesProcess.getOutputFile()+".DUP");
        if (!gefile.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

        QTextStream in(&gefile);

        //fill in the table with each row
        while (!in.atEnd())
        {
            for( int r = 0; r < segDuplicatesTable->rowCount(); r++ ){

                QString gline           = in.readLine();
                QStringList lineList    = gline.split("\t", QString::SkipEmptyParts);

                for( int c = 0; c < segDuplicatesTable->columnCount(); ++c ){

                    QTableWidgetItem *temp = new QTableWidgetItem(lineList.value(c), 0);
                    temp->setText(lineList.value(c));
                    segDuplicatesTable->setItem(r, c, temp);
                }
            }
        }

        gefile.close();

        segDuplicatesTable->connect(segDuplicatesTable, SIGNAL(cellDoubleClicked(int,int)), this, SLOT(segDuplicatesTableOpenBrowser(int)) );

        //open stats file and print in text edit
        QFile tfile(this->segDuplicatesProcess.getOutputFile()+".DUP.stats");
        if (!tfile.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

        QTextStream inT(&tfile);
        while (!inT.atEnd())
        {
            QString tline = inT.readLine();
            showSegDupsStatsTextEdit->append(tline);
        }

        tfile.close();
    }

    //disable stop button
    ui->stopRunFindSegDuplicatesProcessButton->setEnabled(false);
    //enable run button
    ui->runFindSegDuplicatesButton->setEnabled(true);
    //enable clear button
    ui->runFindSegDuplicatesClearButton->setEnabled(true);

    //start the progress bar
    progressBar->setVisible(0);

    return;
}

void CSDesktop::on_stopRunFindSegDuplicatesProcessButton_clicked()
{
    if(this->segDuplicatesProcess.state() == QProcess::Running)
    {
        this->segDuplicatesProcess.kill();
        ui->segDuplicatesProgressTextEdit->append("<font color=red> Find segmental duplicates process was terminated by the user.</font>");
    }
    //disable stop button
    ui->stopRunFindSegDuplicatesProcessButton->setEnabled(false);
    //enable run button
    ui->runFindSegDuplicatesButton->setEnabled(true);
    //enable clear button
    ui->runFindSegDuplicatesClearButton->setEnabled(true);

    this->segDuplicatesProcess.setFromError(true);

    //start the progress bar
    progressBar->setVisible(0);

    return;
}

//
// Functions that handle the find motif (myscanace) process
//

void CSDesktop::on_runFindmotifButton_clicked()
{
    //clear the fromError variable
    this->findmotifProcess.setFromError(false);

    //clear the text edit area
    ui->findmotifProgressTextEdit->clear();

    //clear the targets results tab fields
    this->findMotifsTable->clear();
    this->showFindMotifsStatsTextEdit->clear();

    //get the peaks file
    QString tmpPeaksFile = ui->findmotifSelectPeaksLineEdit->text();
    if(tmpPeaksFile.isNull() || tmpPeaksFile.isEmpty()) {
        //bring progress tab to front
        ui->findmotifTabWidget->setCurrentIndex(1);
        //throw message
        ui->findmotifProgressTextEdit->append("<font color=red> No peaks file was selected. Please try again. </font>");
        return;
    }
    else {
        //set the peaks file
        this->findmotifProcess.setPeaksFile(tmpPeaksFile);

        //set the outfile name
        if(ui->findmotifOutputFileLineEdit->text().isNull() || ui->findmotifOutputFileLineEdit->text().isEmpty()) {
            //bring progress tab to front
            ui->findmotifTabWidget->setCurrentIndex(1);
            //throw message
            ui->findmotifProgressTextEdit->append("<font color=red> No output name was selected. Please try again. </font>");
            return;
        }

        this->findmotifProcess.setOutputFile(ui->findmotifOutputFileLineEdit->text() + ".txt");

        //set the genome file name
        this->findmotifProcess.setGenomeFile(ui->findmotifGenomeLineEdit->text());

        if(this->findmotifProcess.getGenomeFile().isNull() || this->findmotifProcess.getGenomeFile().isEmpty()) {
            //bring progress tab to front
            ui->findmotifTabWidget->setCurrentIndex(1);
            //throw message
            ui->findmotifProgressTextEdit->append("<font color=red> No genome file was selected. Please try again. </font>");
            return;
        }

        //get the threshold
        this->findmotifProcess.setThreshold(ui->findmotifScoreSpinBox->text().toFloat());

        QString threshold;
        threshold.setNum(this->findmotifProcess.getThreshold());

        //get the motif
        if(ui->findmotifNameRadioButton->isChecked()) {
            //name
            if(ui->findmotifBulykMotifComboBox->isEnabled()) {
                findmotifProcess.setMotifName(ui->findmotifBulykMotifComboBox->currentText());
            }
            else if(ui->findmotifJasparMotifComboBox->isEnabled()) {
                findmotifProcess.setMotifName(ui->findmotifJasparMotifComboBox->currentText()+".jaspar");
            }
        }
        else {
            //sequence
            QRegExp rx("[^ATCG\\]\\[]");

            if(ui->findmotifSequenceLineEdit->text().contains(rx) || ui->findmotifSequenceLineEdit->text().isEmpty()) {
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                                      | Qt::WindowMinimizeButtonHint);
                msgBox.setText(tr("Examples of allowed sequences are: TCCTAGA, TC[AT]CCGG, [CG][TA]TTTCGAT."));
                msgBox.exec();
                return;
            }
            else {
                findmotifProcess.setMotifSequence(ui->findmotifSequenceLineEdit->text());
            }
        }

        //set the arguments
        QStringList inputArguments;
        inputArguments << FINDMOTIF_PROGRAM << "--targets" << this->findmotifProcess.getPeaksFile()
                       << "--c" << threshold
                       << "--genome" << this->findmotifProcess.getGenomeFile()
                       << "--suffix" << this->findmotifProcess.getOutputFile();

        if(ui->findmotifNameRadioButton->isChecked()) {
            inputArguments << "--motifname" << this->findmotifProcess.getMotifName();
        }
        else {
            inputArguments << "--motifseq" << this->findmotifProcess.getMotifSequence();
        }

        //start process for INPUT data
        this->findmotifProcess.start(PERL, inputArguments);

        //start the progress bar
        progressBar->setVisible(1);

        //throw start message
        ui->findmotifProgressTextEdit->append("<font color=green> Find motif process for peaks has started. </font>");
    }

    //bring progress tab to front
    ui->findmotifTabWidget->setCurrentIndex(1);
    //enable stop button
    ui->stopRunFindmotifProcessButton->setEnabled(true);
    //disable run button
    ui->runFindmotifButton->setEnabled(false);
    //disable clear button
    ui->runFindmotifClearButton->setEnabled(false);
}

void CSDesktop::readOutputFromFindMotifProcess()
{
    ui->findmotifProgressTextEdit->append(this->findmotifProcess.readAllStandardOutput());
}

void CSDesktop::readErrorFromFindMotifProcess()
{
    ui->findmotifProgressTextEdit->append("<font color=red>" +
                                          this->findmotifProcess.readAllStandardError() + "</font>");

    if(this->findmotifProcess.state() == QProcess::Running)
    {
        this->findmotifProcess.kill();
        //disable stop button
        ui->stopRunFindmotifProcessButton->setEnabled(false);
        //enable run button
        ui->runFindmotifButton->setEnabled(true);
        //enable clear button
        ui->runFindmotifClearButton->setEnabled(true);
        //throw end message
        ui->findmotifProgressTextEdit->append("<font color=red> Find motif process terminated.</font>");

        this->findmotifProcess.setFromError(true);

        //start the progress bar
        progressBar->setVisible(0);
    }
}

void CSDesktop::onFindMotifProcessFinished()
{
    if(this->findmotifProcess.state() == QProcess::Running)
    {
        this->findmotifProcess.kill();
    }

    if(!(this->findmotifProcess.getFromError()))
    {
        //throw message
        ui->findmotifProgressTextEdit->append("<font color=green> Find motif process finished successfully.</font>");
        ui->findmotifProgressTextEdit->append("<font color=green> The results are in " + this->findmotifProcess.getOutputFile() + "</font>");

        //show results in new tab
        ui->findmotifTabWidget->addTab(showFindMotifsTab, "Results");
        ui->findmotifTabWidget->setCurrentIndex(ui->findmotifTabWidget->indexOf(showFindMotifsTab));

        findMotifsTable->setGeometry(QRect(0, 23, 641, 381));
        findMotifsTable->show();

        findMotifsStatsLabel->setObjectName(QString::fromUtf8("genesStatsLabel"));
        findMotifsStatsLabel->setText("Information");
        findMotifsStatsLabel->setGeometry(QRect(0, 406, 641, 21));
        findMotifsStatsLabel->show();

        showFindMotifsStatsTextEdit->setGeometry(QRect(0, 429, 641, 101));
        showFindMotifsStatsTextEdit->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);
        showFindMotifsStatsTextEdit->show();

        findMotifsCopyButton->setGeometry(QRect(0, 540, 100, 21));
        findMotifsCopyButton->setText("Copy table");
        findMotifsCopyButton->show();
        findMotifsCopyButton->connect(findMotifsCopyButton, SIGNAL(clicked()), this, SLOT(findMotifsTableCopy()));
        findMotifsCopyButton->setStatusTip("Copy table contents to clipboard");
        findMotifsCopyButton->setToolTip("Copy table contents to clipboard");

        QFile gfile(this->findmotifProcess.getOutputFile());
        if (!gfile.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

        QTextStream inL(&gfile);

        int lines = 0;
        //count lines in the file
        while (!inL.atEnd())
        {
            QString gline = inL.readLine();
            lines++;
        }

        gfile.close();

        //set the table's rows and columns
        findMotifsTable->setRowCount(lines);
        findMotifsTable->setColumnCount(8);
        QStringList headerNames;
        headerNames << "Chromosome" << "Start" << "End" << "Avg p_value" << "Score" << "Max peak height position" << "Max peak height" << "% Max peak height position";
        findMotifsTable->setHorizontalHeaderLabels(headerNames);

        //open the file to add lines in the table
        QFile gefile(this->findmotifProcess.getOutputFile());
        if (!gefile.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

        QTextStream in(&gefile);

        //fill in the table with each row
        while (!in.atEnd())
        {
            for( int r = 0; r < findMotifsTable->rowCount(); r++ ){

                QString gline           = in.readLine();
                QStringList lineList    = gline.split("\t", QString::SkipEmptyParts);

                for( int c = 0; c < findMotifsTable->columnCount(); ++c ){

                    QTableWidgetItem *temp = new QTableWidgetItem(lineList.value(c), 0);
                    temp->setText(lineList.value(c));
                    findMotifsTable->setItem(r, c, temp);
                }
            }
        }

        gefile.close();

        findMotifsTable->removeRow(lines-1);
        findMotifsTable->connect(findMotifsTable, SIGNAL(cellDoubleClicked(int,int)), this, SLOT(findMotifsTableOpenBrowser(int)) );

        //open stats file and print in text edit
        QFile tfile(this->findmotifProcess.getOutputFile());
        if (!tfile.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

        QTextStream inT(&tfile);
        while (!inT.atEnd())
        {
            QString tline = inT.readLine();
            if(tline.startsWith("#")) {
                showFindMotifsStatsTextEdit->append(tline);
            }
        }
        tfile.close();
    }

    //disable stop button
    ui->stopRunFindmotifProcessButton->setEnabled(false);
    //enable run button
    ui->runFindmotifButton->setEnabled(true);
    //enable clear button
    ui->runFindmotifClearButton->setEnabled(true);

    //start the progress bar
    progressBar->setVisible(0);

    return;
}

void CSDesktop::on_stopRunFindmotifProcessButton_clicked()
{
    if(this->findmotifProcess.state() == QProcess::Running)
    {
        this->findmotifProcess.kill();
        ui->findmotifProgressTextEdit->append("<font color=red> Find motif process was terminated by the user.</font>");
    }
    //disable stop button
    ui->stopRunFindmotifProcessButton->setEnabled(false);
    //enable run button
    ui->runFindmotifButton->setEnabled(true);
    //enable clear button
    ui->runFindmotifClearButton->setEnabled(true);

    this->findmotifProcess.setFromError(true);

    //start the progress bar
    progressBar->setVisible(0);

    return;
}

//
// Functions that handle the conservation process
//

void CSDesktop::on_runConsButton_clicked()
{
    //clear the fromError variable
    this->consProcess.setFromError(false);

    //clear the text edit area
    ui->consProgressTextEdit->clear();

    //clear the targets results tab fields
    this->conservationTable->clear();
    this->showConservationStatsTextEdit->clear();

    //get the peaks file
    QString tmpPeaksFile = ui->consSelectPeaksLineEdit->text();
    if(tmpPeaksFile.isNull() || tmpPeaksFile.isEmpty()) {
        //bring progress tab to front
        ui->consTabWidget->setCurrentIndex(1);
        //throw message
        ui->consProgressTextEdit->append("<font color=red> No peaks file was selected. Please try again. </font>");
        return;
    }
    else {
        //set the peaks file
        this->consProcess.setPeaksFile(tmpPeaksFile);

        //set the outfile name
        this->consProcess.setOutputFile(ui->consOutputFileLineEdit->text());

        if(this->consProcess.getOutputFile().isNull() || this->consProcess.getOutputFile().isEmpty()) {
            //bring progress tab to front
            ui->consTabWidget->setCurrentIndex(1);
            //throw message
            ui->consProgressTextEdit->append("<font color=red> No output name was selected. Please try again. </font>");
            return;
        }

        //get the species
        QString species = ui->consSpeciesComboBox->currentText();
        if(species == "human (hg18)")
            this->consProcess.setGenome("hg18");
        else if(species == "mouse (mm9)")
            this->consProcess.setGenome("mm9");

        //set the cons. scores folder
        this->consProcess.setConsFolder(ui->consConsDirLineEdit->text());

        if(this->consProcess.getConsFolder().isNull() || this->consProcess.getConsFolder().isEmpty()) {
            //bring progress tab to front
            ui->consTabWidget->setCurrentIndex(1);
            //throw message
            ui->consProgressTextEdit->append("<font color=red> No folder for conservation scores was selected. Please try again. </font>");
            return;
        }

        //get the format
        this->consProcess.setFormat(ui->consFormatComboBox->currentText());

        //get the category
        this->consProcess.setCategory(ui->consCategoryComboBox->currentText());

        //get the method
        this->consProcess.setMethod(ui->consMethodComboBox->currentText());

        //get the threshold
        this->consProcess.setThreshold(ui->consThresholdSpinBox->text().toFloat());

        QString threshold;
        threshold.setNum(this->consProcess.getThreshold());

        //get the random distance
        this->consProcess.setRanDistance(ui->consRandistSpinBox->text().toInt());
        QString randist;
        randist.setNum(this->consProcess.getRanDistance());

        //get the show random bool variable
        QString makeRand;
        if(ui->consMakeRandCheckBox->isChecked()) {
            this->consProcess.setMakeRandom(true);
            makeRand = "1";
        }
        else {
            this->consProcess.setMakeRandom(false);
            makeRand = "0";
        }

        //set the outrand name
        this->consProcess.setOutputRandomFile(ui->consOutputRandomLineEdit->text());

        //get the show profiles bool variable
        QString showProfiles;
        if(ui->consShowProfilesCheckBox->isChecked()) {
            this->consProcess.setShowProfiles(true);
            showProfiles = "1";
        }
        else {
            this->consProcess.setShowProfiles(false);
            showProfiles = "0";
        }

        //get the window size
        this->consProcess.setWindowSize(ui->consWindowSizeSpinBox->text().toInt());
        QString windowSize;
        windowSize.setNum(this->consProcess.getWindowSize());


        //set the arguments
        QStringList inputArguments;
        inputArguments << CONSPROCESS_PROGRAM << "--targets" << this->consProcess.getPeaksFile()
                       << "--consdir" << this->consProcess.getConsFolder()
                       << "--format" << this->consProcess.getFormat()
                       << "--category" << this->consProcess.getCategory()
                       << "--method" << this->consProcess.getMethod()
                       << "--outfile" << this->consProcess.getOutputFile()
                       << "--make_rand" << makeRand
                       << "--randist" << randist
                       << "--show_profiles" << showProfiles
                       << "--outrandom" << this->consProcess.getOutputRandomFile()
                       << "--window_size" << windowSize
                       << "--score_thres" << threshold
                       << "--genome" << this->consProcess.getGenome();
        //<< "--chr" << "chr19";

        //start process for INPUT data
        this->consProcess.start(PERL, inputArguments);

        //start the progress bar
        progressBar->setVisible(1);

        //throw start message
        ui->consProgressTextEdit->append("<font color=green> Conservation process for peaks has started. </font>");
    }

    //bring progress tab to front
    ui->consTabWidget->setCurrentIndex(1);
    //enable stop button
    ui->stopRunConsProcessButton->setEnabled(true);
    //disable run button
    ui->runConsButton->setEnabled(false);
    //disable clear button
    ui->runConsClearButton->setEnabled(false);
}

void CSDesktop::readOutputFromConsProcess()
{
    ui->consProgressTextEdit->append(this->consProcess.readAllStandardOutput());
}

void CSDesktop::readErrorFromConsProcess()
{
    ui->consProgressTextEdit->append("<font color=red>" +
                                     this->consProcess.readAllStandardError() + "</font>");

    if(this->consProcess.state() == QProcess::Running)
    {
        this->consProcess.kill();
        //disable stop button
        ui->stopRunConsProcessButton->setEnabled(false);
        //enable run button
        ui->runConsButton->setEnabled(true);
        //enable clear button
        ui->runConsClearButton->setEnabled(true);
        //throw end message
        ui->consProgressTextEdit->append("<font color=red> Conservation process terminated.</font>");

        this->consProcess.setFromError(true);

        //start the progress bar
        progressBar->setVisible(0);
    }
}

void CSDesktop::onConsProcessFinished()
{
    if(this->consProcess.state() == QProcess::Running)
    {
        this->consProcess.kill();
    }

    if(!(this->consProcess.getFromError()))
    {
        //throw message
        ui->consProgressTextEdit->append("<font color=green> Conservation process finished successfully.</font>");
        ui->consProgressTextEdit->append("<font color=green> The cons. results for the peaks are in " + this->consProcess.getOutputFile() + "</font>");

        if(consProcess.getMakeRandom()) {
            ui->consProgressTextEdit->append("<font color=green> The cons. results for the random regions are in " + this->consProcess.getOutputRandomFile() + "</font>");
        }

        //show results in new tab
        ui->consTabWidget->addTab(showConservationTab, "Results");
        ui->consTabWidget->setCurrentIndex(ui->consTabWidget->indexOf(showConservationTab));

        conservationStatsLabel->setObjectName(QString::fromUtf8("genesStatsLabel"));
        conservationStatsLabel->setText("Information");
        conservationStatsLabel->setGeometry(QRect(0, 406, 641, 21));
        conservationStatsLabel->show();

        showConservationStatsTextEdit->setGeometry(QRect(0, 429, 641, 101));
        showConservationStatsTextEdit->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);
        showConservationStatsTextEdit->show();
        showConservationStatsTextEdit->setText("The cons. results for the peaks are in" + this->consProcess.getOutputFile());

        if(consProcess.getMakeRandom()) {
            showConservationStatsTextEdit->append("The cons. results for the random regions are in" + this->consProcess.getOutputRandomFile());
        }

        if(!consProcess.getShowProfiles()) {

            conservationTable->setGeometry(QRect(0, 23, 641, 381));
            conservationTable->show();

            conservationCopyButton->setGeometry(QRect(0, 540, 100, 21));
            conservationCopyButton->setText("Copy table");
            conservationCopyButton->show();
            conservationCopyButton->connect(conservationCopyButton, SIGNAL(clicked()), this, SLOT(conservationTableCopy()));
            conservationCopyButton->setStatusTip("Copy table contents to clipboard");
            conservationCopyButton->setToolTip("Copy table contents to clipboard");

            QFile gfile(this->consProcess.getOutputFile());
            if (!gfile.open(QIODevice::ReadOnly | QIODevice::Text))
                return;

            QTextStream inL(&gfile);

            int lines = 0;
            //count lines in the file
            while (!inL.atEnd())
            {
                QString gline = inL.readLine();
                lines++;
            }

            gfile.close();

            //set the table's rows and columns
            conservationTable->setRowCount(lines);
            conservationTable->setColumnCount(6);
            QStringList headerNames;
            headerNames << "Chromosome" << "Start" << "End" << "Avg. cons" << "Min. cons" << "Max. cons";
            conservationTable->setHorizontalHeaderLabels(headerNames);

            //open the file to add lines in the table
            QFile gefile(this->consProcess.getOutputFile());
            if (!gefile.open(QIODevice::ReadOnly | QIODevice::Text))
                return;

            QTextStream in(&gefile);

            //fill in the table with each row
            while (!in.atEnd())
            {
                for( int r = 0; r < conservationTable->rowCount(); r++ ){

                    QString gline           = in.readLine();
                    QStringList lineList    = gline.split("\t", QString::SkipEmptyParts);

                    for( int c = 0; c < conservationTable->columnCount(); ++c ){

                        QTableWidgetItem *temp = new QTableWidgetItem(lineList.value(c), 0);
                        temp->setText(lineList.value(c));
                        conservationTable->setItem(r, c, temp);
                    }
                }
            }

            gefile.close();

            conservationTable->connect(conservationTable, SIGNAL(cellDoubleClicked(int,int)), this, SLOT(conservationTableOpenBrowser(int)) );
        }
        else {
            conservationTable->hide();
        }
    }

    //disable stop button
    ui->stopRunConsProcessButton->setEnabled(false);
    //enable run button
    ui->runConsButton->setEnabled(true);
    //enable clear button
    ui->runConsClearButton->setEnabled(true);

    //start the progress bar
    progressBar->setVisible(0);

    return;
}

void CSDesktop::on_stopRunConsProcessButton_clicked()
{
    if(this->consProcess.state() == QProcess::Running)
    {
        this->consProcess.kill();
        ui->consProgressTextEdit->append("<font color=red> Conservation process was terminated by the user.</font>");
    }
    //disable stop button
    ui->stopRunConsProcessButton->setEnabled(false);
    //enable run button
    ui->runConsButton->setEnabled(true);
    //enable clear button
    ui->runConsClearButton->setEnabled(true);

    this->consProcess.setFromError(true);

    //start the progress bar
    progressBar->setVisible(0);

    return;
}

//
// Functions that handle the compare peaks process
//

void CSDesktop::on_runCompPeaksButton_clicked()
{
    //clear the fromError variable
    this->compPeaksProcess.setFromError(false);

    //init bool values
    compPeaksProcess.setShowOvPeaks(false);
    compPeaksProcess.setShowUnPeaks(false);
    compPeaksProcess.setShowDesc(false);
    compPeaksProcess.setHasID1(false);
    compPeaksProcess.setHasID2(false);

    //clear the text edit area
    ui->compPeaksProgressTextEdit->clear();

    //clear the targets results tab fields
    this->compPeaksTable->clear();
    this->compPeaksTable->setColumnCount(0);
    this->showCompPeaksStatsTextEdit->clear();

    //get the peaks files
    QString tmpPeaksFile1 = ui->compPeaksSelectPeaks1LineEdit->text();
    QString tmpPeaksFile2 = ui->compPeaksSelectPeaks2LineEdit->text();

    if(tmpPeaksFile1.isNull() || tmpPeaksFile1.isEmpty()) {
        //bring progress tab to front
        ui->compPeaksTabWidget->setCurrentIndex(1);
        //throw message
        ui->compPeaksProgressTextEdit->append("<font color=red> No peaks file1 was selected. Please try again. </font>");
        return;
    }
    else if(tmpPeaksFile2.isNull() || tmpPeaksFile2.isEmpty()) {
        //bring progress tab to front
        ui->compPeaksTabWidget->setCurrentIndex(1);
        //throw message
        ui->compPeaksProgressTextEdit->append("<font color=red> No peaks file2 was selected. Please try again. </font>");
        return;
    }
    else {

        //set the peaks files
        this->compPeaksProcess.setPeaksFile1(tmpPeaksFile1);
        this->compPeaksProcess.setPeaksFile2(tmpPeaksFile2);

        //set the outfile name
        this->compPeaksProcess.setPrefix(ui->compPeaksPrefixFileLineEdit->text());
        this->compPeaksProcess.setOutputFile(this->compPeaksProcess.getPeaksFile1().toStdString().substr(0, this->compPeaksProcess.getPeaksFile1().toStdString().find_last_of("/")+1).c_str());

        if(this->compPeaksProcess.getPrefix().isNull() || this->compPeaksProcess.getPrefix().isEmpty()) {
            //bring progress tab to front
            ui->compPeaksTabWidget->setCurrentIndex(1);
            //throw message
            ui->compPeaksProgressTextEdit->append("<font color=red> No prefix name was selected. Please try again. </font>");
            return;
        }

        //get the overlapping type
        if(ui->compPeaksANDRadioButton->isChecked()) {
            compPeaksProcess.setCompType("and");
        }
        else if(ui->compPeaksANDNOTRadioButton->isChecked()) {
            compPeaksProcess.setCompType("andnot");
        }

        //get the number of iterations
        QString iter;
        this->compPeaksProcess.setIterations(ui->compPeaksIterationsSpinBox->text().toInt());
        iter.setNum(this->compPeaksProcess.getIterations());


        //get the random mode
        QString tmpMode = ui->compPeaksRandomModeComboBox->currentText();

        if(tmpMode == "random regions") {
            this->compPeaksProcess.setRandmode("default");
        }
        else if(tmpMode == "same genomic distribution with peaks") {
            this->compPeaksProcess.setRandmode("gendist");
        }

        //set the arguments
        QStringList inputArguments;
        inputArguments << "--peakfile1" << this->compPeaksProcess.getPeaksFile1()
                       << "--peakfile2" << this->compPeaksProcess.getPeaksFile2()
                          //<< "--outdir" << this->compPeaksProcess.getOutputFile()
                       << "--showovpeaks" <<"1" //because of this I have the ovtype (and, andnot) option
                       << "--ovtype" << this->compPeaksProcess.getCompType()
                       << "--prefix" << this->compPeaksProcess.getPrefix()
                       << "--iterations" << iter
                       << "--randmode" << this->compPeaksProcess.getRandmode();

        //get the hasid options
        if(ui->compPeaksID1CheckBox->isChecked()) {
            compPeaksProcess.setHasID1(true);
            inputArguments << "--hasid1" <<"1";
        }

        if(ui->compPeaksID2CheckBox->isChecked()) {
            compPeaksProcess.setHasID2(true);
            inputArguments << "--hasid2" <<"1";
        }

        //get the hasheader options
        if(ui->compPeaksHeader1CheckBox->isChecked()) {
            compPeaksProcess.setHasHeader1(true);
            inputArguments << "--hasheader1" <<"1";
        }

        if(ui->compPeaksHeader2CheckBox->isChecked()) {
            compPeaksProcess.setHasHeader2(true);
            inputArguments << "--hasheader2" <<"1";
        }

        //get the usepeakcenter option
        if(ui->compPeaksCenter1CheckBox->isChecked()) {
            compPeaksProcess.setUseCenter1(true);
            inputArguments << "--usepeak1center" <<"1";
        }

        if(ui->compPeaksCenter2CheckBox->isChecked()) {
            compPeaksProcess.setUseCenter2(true);
            inputArguments << "--usepeak2center" <<"1";
        }

        //get the show_ov_int option
        if(ui->compPeaksOvPeaksCheckBox->isChecked()) {
            compPeaksProcess.setShowOvPeaks(true);
            inputArguments << "--show_ov_int" <<"1";
        }

        //get the showunion option
        if(ui->compPeaksUnPeaksCheckBox->isChecked()) {
            compPeaksProcess.setShowUnPeaks(true);
            inputArguments << "--showunion" <<"1";
        }

        //get the showpeakdesc option
        if(ui->compPeaksDescPeaks1CheckBox->isChecked() || ui->compPeaksDescPeaks2CheckBox->isChecked()) {
            compPeaksProcess.setShowDesc(true);
            inputArguments << "--showpeakdesc" <<"1";
        }

        //start process
        this->compPeaksProcess.start(COMPAREINTERVALS_PROGRAM, inputArguments);

        //start the progress bar
        progressBar->setVisible(1);

        //throw start message
        ui->compPeaksProgressTextEdit->append("<font color=green> Compare peaks process has started. </font>");
    }
    //bring progress tab to front
    ui->compPeaksTabWidget->setCurrentIndex(1);
    //enable stop button
    ui->stopRunCompPeaksProcessButton->setEnabled(true);
    //disable run button
    ui->runCompPeaksButton->setEnabled(false);
    //disable clear button
    ui->runCompPeaksClearButton->setEnabled(false);
}

void CSDesktop::readOutputFromCompPeaksProcess()
{
    ui->compPeaksProgressTextEdit->append(this->compPeaksProcess.readAllStandardOutput());
}

void CSDesktop::readErrorFromCompPeaksProcess()
{
    ui->compPeaksProgressTextEdit->append("<font color=red>" +
                                          this->compPeaksProcess.readAllStandardError() + "</font>");

    if(this->compPeaksProcess.state() == QProcess::Running)
    {
        this->compPeaksProcess.kill();
        //disable stop button
        ui->stopRunCompPeaksProcessButton->setEnabled(false);
        //enable run button
        ui->runCompPeaksButton->setEnabled(true);
        //enable clear button
        ui->runCompPeaksClearButton->setEnabled(true);
        //throw end message
        ui->compPeaksProgressTextEdit->append("<font color=red> Compare peaks process terminated.</font>");

        this->compPeaksProcess.setFromError(true);

        //start the progress bar
        progressBar->setVisible(0);
    }
}

void CSDesktop::onCompPeaksProcessFinished()
{

    if(this->compPeaksProcess.state() == QProcess::Running)
    {
        this->compPeaksProcess.kill();
    }

    if(!(this->compPeaksProcess.getFromError()))
    {
        //throw message
        ui->compPeaksProgressTextEdit->append("<font color=green> Compare peaks process finished successfully.</font>");
        ui->compPeaksProgressTextEdit->append("<font color=green> The results are in " + this->compPeaksProcess.getOutputFile() + "OUT." + this->compPeaksProcess.getPrefix() + ".txt" + "</font>");

        //show results in new tab
        ui->compPeaksTabWidget->addTab(showCompPeaksTab, "Results");
        ui->compPeaksTabWidget->setCurrentIndex(ui->compPeaksTabWidget->indexOf(showCompPeaksTab));

        compPeaksTable->setGeometry(QRect(0, 23, 641, 381));
        compPeaksTable->show();

        compPeaksStatsLabel->setObjectName(QString::fromUtf8("genesStatsLabel"));
        compPeaksStatsLabel->setText("Information");
        compPeaksStatsLabel->setGeometry(QRect(0, 406, 641, 21));
        compPeaksStatsLabel->show();

        showCompPeaksStatsTextEdit->setGeometry(QRect(0, 429, 641, 101));
        showCompPeaksStatsTextEdit->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);
        showCompPeaksStatsTextEdit->show();

        compPeaksCopyButton->setGeometry(QRect(0, 540, 100, 21));
        compPeaksCopyButton->setText("Copy table");
        compPeaksCopyButton->show();
        compPeaksCopyButton->connect(compPeaksCopyButton, SIGNAL(clicked()), this, SLOT(compPeaksTableCopy()));
        compPeaksCopyButton->setStatusTip("Copy table contents to clipboard");
        compPeaksCopyButton->setToolTip("Copy table contents to clipboard");

        QFile gfile(this->compPeaksProcess.getOutputFile() + "OUT." + this->compPeaksProcess.getPrefix() + ".txt");

        if (!gfile.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

        QTextStream inL(&gfile);

        int lines = 0;
        //count lines in the file
        while (!inL.atEnd())
        {
            QString gline = inL.readLine();
            lines++;
        }

        gfile.close();

        //set the table's rows and columns
        compPeaksTable->setRowCount(lines);
        QStringList headerNames;

        if(compPeaksProcess.getHasID1()) {
            compPeaksTable->setColumnCount(1);
            headerNames << "ID";
        }

        compPeaksTable->setColumnCount(compPeaksTable->columnCount()+3);
        headerNames << "Chromosome" << "Start" << "End";

        if(compPeaksProcess.getShowDesc()) {
            compPeaksTable->setColumnCount(compPeaksTable->columnCount()+8);
            headerNames <<"Avg p-value" << "Score" << "Max peak height position" << "Max peak height" << "% Max peak height position" << "Peak size" << " Mid point" << "Summit dist from mid";
        }

        /*if(compPeaksProcess.getCompType() != "andnot") {
            compPeaksTable->setColumnCount(compPeaksTable->columnCount()+1);
            headerNames << "Count";
        }*/

        if(compPeaksProcess.getShowOvPeaks()) {
            compPeaksTable->setColumnCount(compPeaksTable->columnCount()+1);
            headerNames <<"Overlapping peaks";
        }

        if(compPeaksProcess.getShowUnPeaks()) {
            compPeaksTable->setColumnCount(compPeaksTable->columnCount()+1);
            headerNames <<"Union of overlapping peaks";
        }

        compPeaksTable->setHorizontalHeaderLabels(headerNames);

        //open the file to add lines in the table
        QFile gefile(this->compPeaksProcess.getOutputFile() + "OUT." + this->compPeaksProcess.getPrefix() + ".txt");

        if (!gefile.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

        QTextStream in(&gefile);

        //fill in the table with each row
        while (!in.atEnd())
        {
            for( int r = 0; r < compPeaksTable->rowCount(); r++ ){

                QString gline           = in.readLine();
                QStringList lineList    = gline.split("\t", QString::SkipEmptyParts);

                for( int c = 0; c < compPeaksTable->columnCount(); ++c ){

                    //QTableWidgetItem *temp = new QTableWidgetItem(lineList.value(c), 0);

                    QTableWidgetItem *temp = new QTableWidgetItem();

                    temp->setData(Qt::DisplayRole, lineList.value(c));
                    //temp->setText(lineList.value(c));
                    compPeaksTable->setItem(r, c, temp);

                    compPeaksTable->setSortingEnabled(true);
                }
            }
        }

        gefile.close();

        compPeaksTable->connect(compPeaksTable, SIGNAL(cellDoubleClicked(int,int)), this, SLOT(compPeaksTableOpenBrowser(int)));

        //print the number of peaks
        stringstream sstr;
        sstr << lines;
        QString tmpLines = sstr.str().c_str();

        if(compPeaksProcess.getCompType() == "and") {
            showCompPeaksStatsTextEdit->append("Total number of peaks in file1 that DO overlap with peaks in file2: " + tmpLines);
        }
        else if(compPeaksProcess.getCompType() == "andnot") {
            showCompPeaksStatsTextEdit->append("Total number of peaks in file1 that DO NOT overlap with peaks in file2: " + tmpLines);
        }

        //open stats file and print in text edit
        QFile tfile(this->compPeaksProcess.getOutputFile() + "STATS." + this->compPeaksProcess.getPrefix() + ".txt");

        if (!tfile.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

        QTextStream inT(&tfile);

        while (!inT.atEnd())
        {
            QString tline = inT.readLine();
            showCompPeaksStatsTextEdit->append(tline);
        }

        tfile.close();
    }

    //disable stop button
    ui->stopRunCompPeaksProcessButton->setEnabled(false);
    //enable run button
    ui->runCompPeaksButton->setEnabled(true);
    //enable clear button
    ui->runCompPeaksClearButton->setEnabled(true);

    //start the progress bar
    progressBar->setVisible(0);

    return;
}

void CSDesktop::on_stopRunCompPeaksProcessButton_clicked()
{
    if(this->compPeaksProcess.state() == QProcess::Running)
    {
        this->compPeaksProcess.kill();
        ui->compPeaksProgressTextEdit->append("<font color=red> Compare peaks process was terminated by the user.</font>");
    }
    //disable stop button
    ui->stopRunCompPeaksProcessButton->setEnabled(false);
    //enable run button
    ui->runCompPeaksButton->setEnabled(true);
    //enable clear button
    ui->runCompPeaksClearButton->setEnabled(true);

    this->compPeaksProcess.setFromError(true);

    //start the progress bar
    progressBar->setVisible(0);

    return;
}

//
// Functions that handle the compare genes process
//

void CSDesktop::on_runCompGenesButton_clicked()
{
    //clear the fromError variable
    this->compGenesProcess.setFromError(false);

    //clear the text edit area
    ui->compGenesProgressTextEdit->clear();

    //clear the targets results tab fields
    this->compGenesTable->clear();
    this->showCompGenesStatsTextEdit->clear();

    //get the peaks files
    QString tmpGenesFile1 = ui->compGenesSelectFile1LineEdit->text();
    QString tmpGenesFile2 = ui->compGenesSelectFile2LineEdit->text();

    if(tmpGenesFile1.isNull() || tmpGenesFile1.isEmpty()) {
        //bring progress tab to front
        ui->compGenesTabWidget->setCurrentIndex(1);
        //throw message
        ui->compGenesProgressTextEdit->append("<font color=red> No genes file1 was selected. Please try again. </font>");
        return;
    }
    else if(tmpGenesFile2.isNull() || tmpGenesFile2.isEmpty()) {
        //bring progress tab to front
        ui->compGenesTabWidget->setCurrentIndex(1);
        //throw message
        ui->compGenesProgressTextEdit->append("<font color=red> No genes file2 was selected. Please try again. </font>");
        return;
    }
    else {

        //set the peaks files
        this->compGenesProcess.setGenesFile1(tmpGenesFile1);
        this->compGenesProcess.setGenesFile2(tmpGenesFile2);

        //set the outfile name
        this->compGenesProcess.setOutputFile(ui->compGenesOutputFileLineEdit->text());

        if(this->compGenesProcess.getOutputFile().isNull() || this->compGenesProcess.getOutputFile().isEmpty()) {
            //bring progress tab to front
            ui->compGenesTabWidget->setCurrentIndex(1);
            //throw message
            ui->compGenesProgressTextEdit->append("<font color=red> No output name was selected. Please try again. </font>");
            return;
        }

        //set the arguments
        QStringList inputArguments;
        inputArguments << COMPAREGENES_PROGRAM
                       << "-input1" << this->compGenesProcess.getGenesFile1()
                       << "-input2" << this->compGenesProcess.getGenesFile2()
                       << "-output" << this->compGenesProcess.getOutputFile()
                       << "-guiVersion" << "1";

        // get the keepUniqRec option
        if(ui->compGenesRemoveDuplicatesCheckBox->isChecked()) {
            this->compGenesProcess.setKeepUniqRec(true);
            inputArguments << "-keepUniqRec" <<"1";
        }

        //start process
        this->compGenesProcess.start(PERL, inputArguments);

        //start the progress bar
        progressBar->setVisible(1);

        //throw start message
        ui->compGenesProgressTextEdit->append("<font color=green> Compare genes process has started. </font>");
    }

    //bring progress tab to front
    ui->compGenesTabWidget->setCurrentIndex(1);
    //enable stop button
    ui->stopRunCompGenesProcessButton->setEnabled(true);
    //disable run button
    ui->runCompGenesButton->setEnabled(false);
    //disable clear button
    ui->runCompGenesClearButton->setEnabled(false);
}

void CSDesktop::readOutputFromCompGenesProcess()
{
    ui->compGenesProgressTextEdit->append(this->compGenesProcess.readAllStandardOutput());
}

void CSDesktop::readErrorFromCompGenesProcess()
{
    ui->compGenesProgressTextEdit->append("<font color=red>" +
                                          this->compGenesProcess.readAllStandardError() + "</font>");

    if(this->compGenesProcess.state() == QProcess::Running)
    {
        this->compGenesProcess.kill();
        //disable stop button
        ui->stopRunCompGenesProcessButton->setEnabled(false);
        //enable run button
        ui->runCompGenesButton->setEnabled(true);
        //enable clear button
        ui->runCompGenesClearButton->setEnabled(true);
        //throw end message
        ui->compGenesProgressTextEdit->append("<font color=red> Compare genes process terminated.</font>");

        this->compGenesProcess.setFromError(true);

        //start the progress bar
        progressBar->setVisible(0);
    }
}

void CSDesktop::onCompGenesProcessFinished()
{
    if(this->compGenesProcess.state() == QProcess::Running)
    {
        this->compGenesProcess.kill();
    }

    if(!(this->compGenesProcess.getFromError()))
    {
        //throw message
        ui->compGenesProgressTextEdit->append("<font color=green> Compare genes process finished successfully.</font>");
        ui->compGenesProgressTextEdit->append("<font color=green> The results are in " + this->compGenesProcess.getOutputFile() + "</font>");

        //show results in new tab
        ui->compGenesTabWidget->addTab(showCompGenesTab, "Results");
        ui->compGenesTabWidget->setCurrentIndex(ui->compGenesTabWidget->indexOf(showCompGenesTab));

        compGenesTable->setGeometry(QRect(0, 23, 641, 381));
        compGenesTable->show();

        compGenesStatsLabel->setObjectName(QString::fromUtf8("genesStatsLabel"));
        compGenesStatsLabel->setText("Information");
        compGenesStatsLabel->setGeometry(QRect(0, 406, 641, 21));
        compGenesStatsLabel->show();

        showCompGenesStatsTextEdit->setGeometry(QRect(0, 429, 641, 101));
        showCompGenesStatsTextEdit->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);
        showCompGenesStatsTextEdit->show();

        compGenesCopyButton->setGeometry(QRect(0, 540, 100, 21));
        compGenesCopyButton->setText("Copy table");
        compGenesCopyButton->show();
        compGenesCopyButton->connect(compGenesCopyButton, SIGNAL(clicked()), this, SLOT(compGenesTableCopy()));
        compGenesCopyButton->setStatusTip("Copy table contents to clipboard");
        compGenesCopyButton->setToolTip("Copy table contents to clipboard");

        QFile cfile(this->compGenesProcess.getOutputFile() + "Commons.txt");
        if (!cfile.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

        QTextStream inL(&cfile);

        int commonsLines = 0;
        //count lines in the file
        while (!inL.atEnd())
        {
            QString cline = inL.readLine();
            commonsLines++;
        }

        cfile.close();

        //set the table's rows and columns
        compGenesTable->setRowCount(commonsLines);
        QStringList headerNames;
        compGenesTable->setColumnCount(1);
        headerNames << "Gene ID";
        compGenesTable->setHorizontalHeaderLabels(headerNames);

        //open the file to add lines in the table
        QFile cgefile(this->compGenesProcess.getOutputFile() + "Commons.txt");
        if (!cgefile.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

        QTextStream in(&cgefile);

        //fill in the table with each row
        while (!in.atEnd())
        {
            for( int r = 0; r < compGenesTable->rowCount(); r++ ){

                QString gline           = in.readLine();
                QStringList lineList    = gline.split("\t", QString::SkipEmptyParts);

                for( int c = 0; c < compGenesTable->columnCount(); ++c ){

                    QTableWidgetItem *temp = new QTableWidgetItem(lineList.value(c), 0);
                    temp->setText(lineList.value(c));
                    compGenesTable->setItem(r, c, temp);
                }
            }
        }

        cgefile.close();

        compGenesTable->connect(compGenesTable, SIGNAL(cellDoubleClicked(int,int)), this, SLOT(compGenesTableOpenBrowser(int)) );

        //open output files and count lines
        QFile u1file(this->compGenesProcess.getOutputFile() + "Unique1.txt");
        if (!u1file.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

        QTextStream inU1(&u1file);

        int unique1Lines = 0;
        //count lines in the file
        while (!inU1.atEnd())
        {
            QString u1line = inU1.readLine();
            unique1Lines++;
        }

        u1file.close();

        QFile u2file(this->compGenesProcess.getOutputFile() + "Unique2.txt");
        if (!u2file.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

        QTextStream inU2(&u2file);

        int unique2Lines = 0;
        //count lines in the file
        while (!inU2.atEnd())
        {
            QString u2line = inU2.readLine();
            unique2Lines++;
        }

        u2file.close();

        //print the number of genes
        stringstream sstr;
        sstr << commonsLines;
        QString tmpCommonsLines = sstr.str().c_str();
        showCompGenesStatsTextEdit->append("Number of common genes: " + tmpCommonsLines);

        stringstream sstr1;
        sstr1 << unique1Lines;
        QString tmpUnique1Lines = sstr1.str().c_str();
        showCompGenesStatsTextEdit->append("Number of unique genes in file 1: " + tmpUnique1Lines);

        stringstream sstr2;
        sstr2 << unique2Lines;
        QString tmpUnique2Lines = sstr2.str().c_str();
        showCompGenesStatsTextEdit->append("Number of unique genes in file 2: " + tmpUnique2Lines);

        int allUniqueLines = unique1Lines + unique2Lines;
        stringstream sstr3;
        sstr3 << allUniqueLines;
        QString tmpAllUniqueLines = sstr3.str().c_str();
        showCompGenesStatsTextEdit->append("Number of unique genes in both files: " + tmpAllUniqueLines);
    }

    //disable stop button
    ui->stopRunCompGenesProcessButton->setEnabled(false);
    //enable run button
    ui->runCompGenesButton->setEnabled(true);
    //enable clear button
    ui->runCompGenesClearButton->setEnabled(true);

    //start the progress bar
    progressBar->setVisible(0);

    return;
}

void CSDesktop::on_stopRunCompGenesProcessButton_clicked()
{
    if(this->compGenesProcess.state() == QProcess::Running)
    {
        this->compGenesProcess.kill();
        ui->compGenesProgressTextEdit->append("<font color=red> Compare genes process was terminated by the user.</font>");
    }
    //disable stop button
    ui->stopRunCompGenesProcessButton->setEnabled(false);
    //enable run button
    ui->runCompGenesButton->setEnabled(true);
    //enable clear button
    ui->runCompGenesClearButton->setEnabled(true);

    this->compGenesProcess.setFromError(true);

    //start the progress bar
    progressBar->setVisible(0);

    return;
}

//
// Functions that handle the FIRE process
//

void CSDesktop::on_runFIREButton_clicked()
{
    //clear the fromError variable
    this->fireProcess.setFromError(false);

    //clear the text edit area
    ui->fireProgressTextEdit->clear();

    //clear the targets results tab fields
    this->fireTable->clear();
    this->showFireStatsTextEdit->clear();

    //get the peaks file or folder
    QString tmpPeaksFile    = ui->fireSelectPeaksLineEdit->text();
    QString tmpPeaksFolder  = ui->fireSelectPeaksFolderLineEdit->text();

    if((tmpPeaksFile.isNull() || tmpPeaksFile.isEmpty()) && (tmpPeaksFolder.isNull() || tmpPeaksFolder.isEmpty())) {
        //bring progress tab to front
        ui->fireTabWidget->setCurrentIndex(1);
        //throw message
        ui->fireProgressTextEdit->append("<font color=red> No peaks file or peaks folder was selected. Please try again. </font>");
        return;
    }

    /*if(!(tmpPeaksFolder.isNull()) && !(tmpPeaksFolder.isEmpty())) {
        //bring progress tab to front
        ui->fireTabWidget->setCurrentIndex(1);
        //throw message
        ui->fireProgressTextEdit->append("<font color=red> Please select a peaks file or a peaks folder. Use the clear button to clean fields and try again. </font>");
        return;
    }
    else {*/
    //set the genome file
    this->fireProcess.setGenome(ui->fireGenomeLineEdit->text());

    if(this->fireProcess.getGenome().isNull() || this->fireProcess.getGenome().isEmpty()) {
        //bring progress tab to front
        ui->fireTabWidget->setCurrentIndex(1);
        //throw message
        ui->fireProgressTextEdit->append("<font color=red> No genome file was selected. Please try again. </font>");
        return;
    }

    //get the species
    this->fireProcess.setSpecies(ui->fireSpeciesComboBox->currentText());

    //get the random mode
    QString tmpMode = ui->fireRandomModeComboBox->currentText();
    if(tmpMode == "random sequences with the same GC content") {
        this->fireProcess.setRandmode("CGI");
    }
    else if(tmpMode == "adjacent sequences to the peaks") {
        this->fireProcess.setRandmode("adjacent");
    }
    else if(tmpMode == "1st order Markov model sequences learnt on peaks") {
        this->fireProcess.setRandmode("1MM");
    }

    //get the use same seed
    QString seed;
    if(ui->consMakeRandCheckBox->isChecked()) {
        this->fireProcess.setSeed(true);
        seed = "1";
    }
    else {
        this->fireProcess.setSeed(false);
        seed = "0";
    }

    //set the arguments
    QStringList inputArguments;
    inputArguments << FIRE_PROGRAM << "--fastafile" << this->fireProcess.getGenome()
                   << "--randmode" << this->fireProcess.getRandmode()
                   << "--seed" << seed
                   << "--species" << this->fireProcess.getSpecies();

    //get the peaks file/folder options
    if(!(tmpPeaksFile.isNull()) && !(tmpPeaksFile.isEmpty())) {
        this->fireProcess.setPeaksFile(tmpPeaksFile);
        inputArguments << "--targets" << this->fireProcess.getPeaksFile();
        this->fireProcess.setOutputFolder(tmpPeaksFile+".FIRE.txt_FIRE/DNA");
        this->fireProcess.setIsFile(true);
    }

    if(!(tmpPeaksFolder.isNull()) && !(tmpPeaksFolder.isEmpty())) {
        this->fireProcess.setPeaksFolder(tmpPeaksFolder);
        inputArguments << "--peaksfolder" << this->fireProcess.getPeaksFolder();
        this->fireProcess.setOutputFolder(tmpPeaksFolder+"/allpeakfiles.txt.FIRE.txt_FIRE/DNA");
        this->fireProcess.setIsFile(false);
    }

    //start process for INPUT data
    this->fireProcess.start(PERL, inputArguments);

    //start the progress bar
    progressBar->setVisible(1);

    //throw start message
    ui->fireProgressTextEdit->append("<font color=green> FIRE process for peaks has started. </font>");
    //}

    //bring progress tab to front
    ui->fireTabWidget->setCurrentIndex(1);
    //enable stop button
    ui->stopRunFIREProcessButton->setEnabled(true);
    //disable run button
    ui->runFIREButton->setEnabled(false);
    //disable clear button
    ui->runFIREClearButton->setEnabled(false);
}

void CSDesktop::readOutputFromFIREProcess()
{
    ui->fireProgressTextEdit->append(this->fireProcess.readAllStandardOutput());
}

void CSDesktop::readErrorFromFIREProcess()
{
    ui->fireProgressTextEdit->append("<font color=red>" +
                                     this->fireProcess.readAllStandardError() + "</font>");

    if(this->fireProcess.state() == QProcess::Running)
    {
        this->fireProcess.kill();
        //disable stop button
        ui->stopRunFIREProcessButton->setEnabled(false);
        //enable run button
        ui->runFIREButton->setEnabled(true);
        //enable clear button
        ui->runFIREClearButton->setEnabled(true);
        //throw end message
        ui->fireProgressTextEdit->append("<font color=red> FIRE process terminated.</font>");

        this->fireProcess.setFromError(true);

        //start the progress bar
        progressBar->setVisible(0);
    }
}

void CSDesktop::onFIREProcessFinished()
{

    if(this->fireProcess.state() == QProcess::Running)
    {
        this->fireProcess.kill();
    }

    if(!(this->fireProcess.getFromError()))
    {
        //throw message
        ui->fireProgressTextEdit->append("<font color=green> FIRE process finished successfully.</font>");
        ui->fireProgressTextEdit->append("<font color=green> The results are in " + this->fireProcess.getOutputFolder() + "</font>");

        //show results in new tab
        ui->fireTabWidget->addTab(showFireTab, "Results");
        ui->fireTabWidget->setCurrentIndex(ui->fireTabWidget->indexOf(showFireTab));

        //get the summary file
        QString summaryFile;
        if(this->fireProcess.getIsFile()) {
            QString fileName = this->fireProcess.getPeaksFile().toStdString().substr(this->fireProcess.getPeaksFile().toStdString().find_last_of("/"),this->fireProcess.getPeaksFile().toStdString().length()).c_str();
            summaryFile = this->fireProcess.getOutputFolder() + fileName + ".FIRE.txt.summary";
        }
        else {
            summaryFile = this->fireProcess.getOutputFolder() + "/allpeakfiles.txt.FIRE.txt.summary";
        }

        summaryFile.append(".pdf");

        Poppler::Document* document = Poppler::Document::load(summaryFile);

        if (!document || document->isLocked()) {

            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Information);
            msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                                  | Qt::WindowMinimizeButtonHint);
            msgBox.setText(tr("The document does not exist or it is locked."));
            msgBox.exec();

            delete document;
        }
        else {

            // Access page of the PDF file
            Poppler::Page* pdfPage = document->page(0);  // Document starts at page 0
            if (pdfPage == 0) {

                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Information);
                msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                                      | Qt::WindowMinimizeButtonHint);
                msgBox.setText(tr("Failed to open pdf page."));
                msgBox.exec();
            }
            else {

                // Generate a QImage of the rendered page
                QImage image = pdfPage->renderToImage(200.0, 200.0, -1, -1, -1, -1);
                if (image.isNull()) {

                    QMessageBox msgBox;
                    msgBox.setIcon(QMessageBox::Information);
                    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                                          | Qt::WindowMinimizeButtonHint);
                    msgBox.setText(tr("Failed to render pdf page to image."));
                    msgBox.exec();
                }
                else {

                    fireScrollArea->setGeometry(QRect(0, 13, 641, 571));
                    fireScrollArea->show();

                    fireScrollArea->setWidget(fireImageLabel);

                    fireImageLabel->setPixmap(QPixmap::fromImage(image, Qt::AutoColor));
                    fireImageLabel->resize(0.5*fireImageLabel->pixmap()->size());
                    fireImageLabel->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
                    fireImageLabel->setScaledContents(true);
                    fireImageLabel->show();

                    // after the usage, the page must be deleted
                    delete pdfPage;
                    delete document;
                }
            }
        }
    }

    //disable stop button
    ui->stopRunFIREProcessButton->setEnabled(false);
    //enable run button
    ui->runFIREButton->setEnabled(true);
    //enable clear button
    ui->runFIREClearButton->setEnabled(true);

    //start the progress bar
    progressBar->setVisible(0);
    return;
}

void CSDesktop::on_stopRunFIREProcessButton_clicked()
{
    if(this->fireProcess.state() == QProcess::Running)
    {
        this->fireProcess.kill();
        ui->fireProgressTextEdit->append("<font color=red> FIRE process was terminated by the user.</font>");
    }
    //disable stop button
    ui->stopRunFIREProcessButton->setEnabled(false);
    //enable run button
    ui->runFIREButton->setEnabled(true);
    //enable clear button
    ui->runFIREClearButton->setEnabled(true);

    this->fireProcess.setFromError(true);

    //start the progress bar
    progressBar->setVisible(0);

    return;
}

//
// Functions that handle the ComputeJaccard process
//

void CSDesktop::on_runJaccardButton_clicked()
{
    //clear the fromError variable
    this->computeJaccardProcess.setFromError(false);

    //clear the text edit area
    ui->jaccardProgressTextEdit->clear();

    //clear the targets results tab fields
    this->compJaccardTable->clear();
    this->showCompJaccardStatsTextEdit->clear();

    //get the peaks file
    QString tmpPeaksFile = ui->jaccardSelectFileLineEdit->text();
    if(tmpPeaksFile.isNull() || tmpPeaksFile.isEmpty()) {
        //bring progress tab to front
        ui->jaccardTabWidget->setCurrentIndex(1);
        //throw message
        ui->jaccardProgressTextEdit->append("<font color=red> No peaks file was selected. Please try again. </font>");
        return;
    }
    else {

        //set the peaks file
        this->computeJaccardProcess.setPeaksFile(tmpPeaksFile);

        //set the outfile name
        this->computeJaccardProcess.setOutputFile(ui->jaccardOutputFileLineEdit->text());

        if(this->computeJaccardProcess.getOutputFile().isNull() || this->computeJaccardProcess.getOutputFile().isEmpty()) {
            //bring progress tab to front
            ui->jaccardTabWidget->setCurrentIndex(1);
            //throw message
            ui->jaccardProgressTextEdit->append("<font color=red> No output name was selected. Please try again. </font>");
            return;
        }

        //set the arguments
        QStringList inputArguments;
        inputArguments << JACCARD_PROGRAM << "--peaksfile" << this->computeJaccardProcess.getPeaksFile()
                       << "--outfile" << this->computeJaccardProcess.getOutputFile();

        //start process for INPUT data
        this->computeJaccardProcess.start(PERL, inputArguments);

        //start the progress bar
        progressBar->setVisible(1);

        //throw start message
        ui->jaccardProgressTextEdit->append("<font color=green> Compute Jaccard index process for peak files has started. </font>");
    }

    //bring progress tab to front
    ui->jaccardTabWidget->setCurrentIndex(1);
    //enable stop button
    ui->stopRunJaccardProcessButton->setEnabled(true);
    //disable run button
    ui->runJaccardButton->setEnabled(false);
    //disable clear button
    ui->runJaccardClearButton->setEnabled(false);
}

void CSDesktop::readOutputFromJaccardProcess()
{
    ui->jaccardProgressTextEdit->append(this->computeJaccardProcess.readAllStandardOutput());
}

void CSDesktop::readErrorFromJaccardProcess()
{
    ui->jaccardProgressTextEdit->append("<font color=red>" +
                                        this->computeJaccardProcess.readAllStandardError() + "</font>");

    if(this->computeJaccardProcess.state() == QProcess::Running)
    {
        this->computeJaccardProcess.kill();
        //disable stop button
        ui->stopRunJaccardProcessButton->setEnabled(false);
        //enable run button
        ui->runJaccardButton->setEnabled(true);
        //enable clear button
        ui->runJaccardClearButton->setEnabled(true);
        //throw end message
        ui->jaccardProgressTextEdit->append("<font color=red> Compute Jaccard index process terminated.</font>");

        this->computeJaccardProcess.setFromError(true);

        //start the progress bar
        progressBar->setVisible(0);
    }
}

void CSDesktop::onJaccardProcessFinished()
{
    if(this->computeJaccardProcess.state() == QProcess::Running)
    {
        this->computeJaccardProcess.kill();
    }

    if(!(this->computeJaccardProcess.getFromError()))
    {
        //throw message
        ui->jaccardProgressTextEdit->append("<font color=green> Compute Jaccard index process finished successfully.</font>");
        ui->jaccardProgressTextEdit->append("<font color=green> The results are in " + this->computeJaccardProcess.getOutputFile() + "</font>");

        //show results in new tab
        ui->jaccardTabWidget->addTab(showCompJaccardTab, "Results");
        ui->jaccardTabWidget->setCurrentIndex(ui->jaccardTabWidget->indexOf(showCompJaccardTab));

        compJaccardTable->setGeometry(QRect(0, 23, 641, 381));
        compJaccardTable->show();

        compJaccardStatsLabel->setObjectName(QString::fromUtf8("genesStatsLabel"));
        compJaccardStatsLabel->setText("Information");
        compJaccardStatsLabel->setGeometry(QRect(0, 406, 641, 21));
        compJaccardStatsLabel->show();

        showCompJaccardStatsTextEdit->setGeometry(QRect(0, 429, 641, 101));
        showCompJaccardStatsTextEdit->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);
        showCompJaccardStatsTextEdit->show();
        showCompJaccardStatsTextEdit->setText("The results are in" + this->computeJaccardProcess.getOutputFile());

        compJaccardCopyButton->setGeometry(QRect(0, 540, 100, 21));
        compJaccardCopyButton->setText("Copy table");
        compJaccardCopyButton->show();
        compJaccardCopyButton->connect(compJaccardCopyButton, SIGNAL(clicked()), this, SLOT(compJaccardTableCopy()));
        compJaccardCopyButton->setStatusTip("Copy table contents to clipboard");
        compJaccardCopyButton->setToolTip("Copy table contents to clipboard");

        QFile gfile(this->computeJaccardProcess.getOutputFile());
        if (!gfile.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

        QTextStream inL(&gfile);

        int lines = 0;
        //count lines in the file
        while (!inL.atEnd())
        {
            QString gline = inL.readLine();
            lines++;
        }

        gfile.close();

        //set the table's rows and columns
        compJaccardTable->setRowCount(lines);
        compJaccardTable->setColumnCount(lines);

        //open the file to add lines in the table
        QFile gefile(this->computeJaccardProcess.getOutputFile());
        if (!gefile.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

        QTextStream in(&gefile);

        //fill in the table with each row
        while (!in.atEnd())
        {
            for( int r = 0; r < compJaccardTable->rowCount(); r++ ){

                QString gline           = in.readLine();
                QStringList lineList    = gline.split("\t", QString::SkipEmptyParts);

                for( int c = 0; c < compJaccardTable->columnCount(); ++c ){

                    QTableWidgetItem *temp = new QTableWidgetItem(lineList.value(c), 0);
                    temp->setText(lineList.value(c));
                    compJaccardTable->setItem(r, c, temp);
                }
            }
        }

        gefile.close();

        for( int r = 1; r < compJaccardTable->rowCount(); r++ ){

            for( int c = 1; c < compJaccardTable->columnCount(); ++c ){

                QTableWidgetItem *tempCell = new QTableWidgetItem;
                tempCell = compJaccardTable->item(r,c);

                QString tempText = tempCell->text();

                double tempNum = tempText.toDouble();

                if(tempText == "0") {
                    tempCell->setBackground(QBrush(QColor(QColor::fromRgbF(0.83, 1.0, 0.83))));
                }
                else {

                    tempCell->setBackground(QBrush(QColor(QColor::fromRgbF(1.0, 1-tempNum, 1-tempNum))));
                }
            }
        }
    }

    //disable stop button
    ui->stopRunJaccardProcessButton->setEnabled(false);
    //enable run button
    ui->runJaccardButton->setEnabled(true);
    //enable clear button
    ui->runJaccardClearButton->setEnabled(true);

    //start the progress bar
    progressBar->setVisible(0);

    return;
}

void CSDesktop::on_stopRunJaccardProcessButton_clicked()
{
    if(this->computeJaccardProcess.state() == QProcess::Running)
    {
        this->computeJaccardProcess.kill();
        ui->jaccardProgressTextEdit->append("<font color=red> Compute Jaccard index process was terminated by the user.</font>");
    }
    //disable stop button
    ui->stopRunJaccardProcessButton->setEnabled(false);
    //enable run button
    ui->runJaccardButton->setEnabled(true);
    //enable clear button
    ui->runJaccardClearButton->setEnabled(true);
    this->computeJaccardProcess.setFromError(true);
    //stop the progress bar
    progressBar->setVisible(0);

    return;
}

//
// Functions that handle the FindPathway process
//

void CSDesktop::on_runFindPathwayButton_clicked()
{
    //clear the fromError variable
    this->findPathwayProcess.setFromError(false);

    //clear the text edit area
    ui->findPathwayProgressTextEdit->clear();

    //clear the targets results tab fields
    this->findPathwaysTable->clear();
    this->showFindPathwaysStatsTextEdit->clear();

    //get the peaks file
    QString tmpPeaksFile = ui->findPathwaySelectPeaksLineEdit->text();
    if(tmpPeaksFile.isNull() || tmpPeaksFile.isEmpty()) {
        //bring progress tab to front
        ui->findPathwayTabWidget->setCurrentIndex(1);
        //throw message
        ui->findPathwayProgressTextEdit->append("<font color=red> No peaks file was selected. Please try again. </font>");
        return;
    }
    else {
        //set the peaks file
        this->findPathwayProcess.setPeaksFile(tmpPeaksFile);


        //set the outfile name
        if(ui->findPathwayOutputFileLineEdit->text().isNull() || ui->findPathwayOutputFileLineEdit->text().isEmpty()) {
            //bring progress tab to front
            ui->findPathwayTabWidget->setCurrentIndex(1);
            //throw message
            ui->findPathwayProgressTextEdit->append("<font color=red> No output name was selected. Please try again. </font>");
            return;
        }

        this->findPathwayProcess.setOutputFile(ui->findPathwayOutputFileLineEdit->text());

        //get the species and the pathways database
        QString tmpSpecies = ui->findPathwaySpeciesComboBox->currentText();
        QString genome;

        QString tmpDatabase = ui->findPathwaySelectDatabaseComboBox->currentText();
        QString db;

        if(tmpSpecies == "Homo sapiens (hg18)") {
            genome = "hg18";

            if(tmpDatabase == "Gene Ontology (GO)") {
                db = "human_go_orf";
            }
            else if(tmpDatabase == "BioCarta") {
                db = "biocarta";
            }
            else if(tmpDatabase == "KEGG") {
                db = "kegg";
            }
            else if(tmpDatabase == "Human Protein Reference Database") {
                db = "HPRD_interactions";
            }
            else if(tmpDatabase == "SignatureDB") {
                db = "staudt_genesets";
            }
            else if(tmpDatabase == "Reactome") {
                db = "reactome";
            }
        }
        else if(tmpSpecies == "Mus musculus (mm9)") {
            genome  = "mm9";
            db      = "mouse_go_orf";
        }
        else if(tmpSpecies == "Drosophila melanogaster (dm3)") {
            genome  = "dm3";
            db      = "";
        }
        else if(tmpSpecies == "Saccharomyces cerevisiae (sacCer2)") {
            genome  = "sacser";
            db      = "yeast_go";
        }

        this->findPathwayProcess.setSpecies(genome);
        this->findPathwayProcess.setPathwaysDB(db);

        //get the annotations database
        this->findPathwayProcess.setDatabase(ui->findPathwaysDatabaseComboBox->currentText());

        //get the pathway
        if(ui->findPathwaySelectPathwayRadioButton->isChecked()) {
            //select pathway
            QStringList lineList    = ui->findPathwaySelectPathwayComboBox->currentText().split(" ", QString::SkipEmptyParts);
            findPathwayProcess.setPathwayName(lineList.value(0));
        }
        //write pathway
        else {
            if(ui->findPathwayWritePathwayLineEdit->text().isEmpty()) {
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                                      | Qt::WindowMinimizeButtonHint);
                msgBox.setText(tr("Pathway name is empty. Please write or select a pathway name or id."));
                msgBox.exec();
                return;
            }
            else {
                findPathwayProcess.setPathwayName(ui->findPathwayWritePathwayLineEdit->text());
            }
        }

        //get the geneparts

        QString geneparts;

        if(ui->findPathwaysPromotersCheckBox->isChecked()) {
            if(geneparts.isEmpty()) {
                geneparts = "P";
            }
            else {
                geneparts.append(",P");
            }
        }

        if(ui->findPathwaysExonsCheckBox->isChecked()) {
            if(geneparts.isEmpty()) {
                geneparts = "E";
            }
            else {
                geneparts.append(",E");
            }
        }

        if(ui->findPathwaysDownstreamCheckBox->isChecked()) {
            if(geneparts.isEmpty()) {
                geneparts = "DW";
            }
            else {
                geneparts.append(",DW");
            }
        }

        if(ui->findPathwaysIntergenicCheckBox->isChecked()) {
            if(geneparts.isEmpty()) {
                geneparts = "IG";
            }
            else {
                geneparts.append(",IG");
            }
        }

        if(ui->findPathwaysDistalCheckBox->isChecked()) {
            if(geneparts.isEmpty()) {
                geneparts = "D";
            }
            else {
                geneparts.append(",D");
            }
        }

        if(ui->findPathwaysIntronsCheckBox->isChecked()) {
            if(geneparts.isEmpty()) {
                geneparts = "I";
            }
            else {
                geneparts.append(",I");
            }
        }

        if(ui->findPathwaysIntron1CheckBox->isChecked()) {
            if(geneparts.isEmpty()) {
                geneparts = "I1";
            }
            else {
                geneparts.append(",I1");
            }
        }

        if(ui->findPathwaysIntron2CheckBox->isChecked()) {
            if(geneparts.isEmpty()) {
                geneparts = "I2";
            }
            else {
                geneparts.append(",I2");
            }
        }

        findPathwayProcess.setGeneparts(geneparts);

        //set the arguments
        QStringList inputArguments;
        inputArguments << FINDPATHWAY_PROGRAM << "--peakfile" << this->findPathwayProcess.getPeaksFile()
                       << "--genome" << this->findPathwayProcess.getSpecies()
                       << "--pathways" << this->findPathwayProcess.getPathwaysDB()
                       << "--db" << this->findPathwayProcess.getDatabase()
                       << "--pathway" << this->findPathwayProcess.getPathwayName()
                       << "--geneparts" << this->findPathwayProcess.getGeneparts()
                       << "--prefix" << this->findPathwayProcess.getOutputFile();

        //start process for INPUT data
        this->findPathwayProcess.start(PERL, inputArguments);

        //start the progress bar
        progressBar->setVisible(1);

        //throw start message
        ui->findPathwayProgressTextEdit->append("<font color=green> Find pathway process for peaks has started. </font>");
    }

    //bring progress tab to front
    ui->findPathwayTabWidget->setCurrentIndex(1);
    //enable stop button
    ui->stopRunFindPathwayProcessButton->setEnabled(true);
    //disable run button
    ui->runFindPathwayButton->setEnabled(false);
    //disable clear button
    ui->runFindPathwayClearButton->setEnabled(false);
}

void CSDesktop::readOutputFromFindPathwayProcess()
{
    ui->findPathwayProgressTextEdit->append(this->findPathwayProcess.readAllStandardOutput());
}

void CSDesktop::readErrorFromFindPathwayProcess()
{
    ui->findPathwayProgressTextEdit->append("<font color=red>" +
                                            this->findPathwayProcess.readAllStandardError() + "</font>");

    if(this->findPathwayProcess.state() == QProcess::Running)
    {
        this->findPathwayProcess.kill();
        //disable stop button
        ui->stopRunFindPathwayProcessButton->setEnabled(false);
        //enable run button
        ui->runFindPathwayButton->setEnabled(true);
        //enable clear button
        ui->runFindPathwayClearButton->setEnabled(true);
        //throw end message
        ui->findPathwayProgressTextEdit->append("<font color=red> Find pathway process terminated.</font>");

        this->findPathwayProcess.setFromError(true);

        //start the progress bar
        progressBar->setVisible(0);
    }
}

void CSDesktop::onFindPathwayProcessFinished()
{

    if(this->findPathwayProcess.state() == QProcess::Running)
    {
        this->findPathwayProcess.kill();
    }

    if(!(this->findPathwayProcess.getFromError()))
    {
        //throw message
        ui->findPathwayProgressTextEdit->append("<font color=green> Find pathway process finished successfully.</font>");
        ui->findPathwayProgressTextEdit->append("<font color=green> The results are in " + this->findPathwayProcess.getOutputFile() + "</font>");

        //show results in new tab
        ui->findPathwayTabWidget->addTab(showFindPathwaysTab, "Results");
        ui->findPathwayTabWidget->setCurrentIndex(ui->findPathwayTabWidget->indexOf(showFindPathwaysTab));

        findPathwaysTable->setGeometry(QRect(0, 23, 641, 381));
        findPathwaysTable->show();

        findPathwaysStatsLabel->setObjectName(QString::fromUtf8("genesStatsLabel"));
        findPathwaysStatsLabel->setText("Information");
        findPathwaysStatsLabel->setGeometry(QRect(0, 406, 641, 21));
        findPathwaysStatsLabel->show();

        showFindPathwaysStatsTextEdit->setGeometry(QRect(0, 429, 641, 101));
        showFindPathwaysStatsTextEdit->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);
        showFindPathwaysStatsTextEdit->show();

        findPathwaysCopyButton->setGeometry(QRect(0, 540, 100, 21));
        findPathwaysCopyButton->setText("Copy table");
        findPathwaysCopyButton->show();
        findPathwaysCopyButton->connect(findPathwaysCopyButton, SIGNAL(clicked()), this, SLOT(findPathwaysTableCopy()));
        findPathwaysCopyButton->setStatusTip("Copy table contents to clipboard");
        findPathwaysCopyButton->setToolTip("Copy table contents to clipboard");

        QFile gfile(this->findPathwayProcess.getOutputFile()+".peaks."+this->findPathwayProcess.getPathwayName()+".txt");
        if (!gfile.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

        QTextStream inL(&gfile);

        int lines = 0;
        //count lines in the file
        while (!inL.atEnd())
        {
            QString gline = inL.readLine();
            lines++;
        }

        gfile.close();

        //set the table's rows and columns
        findPathwaysTable->setRowCount(lines);
        findPathwaysTable->setColumnCount(12);
        QStringList headerNames;
        headerNames << "Chromosome" << "Start" << "End" << "Avg p_value" << "Score" << "Max peak height position" << "Max peak height"
                    << "% Max peak height position" << "Peak size" << "Mid point" << "Summit dist. from midpoint" << "Transcript Name";

        findPathwaysTable->setHorizontalHeaderLabels(headerNames);

        //open the file to add lines in the table
        QFile gefile(this->findPathwayProcess.getOutputFile()+".peaks."+this->findPathwayProcess.getPathwayName()+".txt");
        if (!gefile.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

        QTextStream in(&gefile);

        //fill in the table with each row
        while (!in.atEnd())
        {
            for( int r = 0; r < findPathwaysTable->rowCount(); r++ ){

                QString gline           = in.readLine();
                QStringList lineList    = gline.split("\t", QString::SkipEmptyParts);

                for( int c = 0; c < findPathwaysTable->columnCount(); ++c ){

                    QTableWidgetItem *temp = new QTableWidgetItem(lineList.value(c), 0);
                    temp->setText(lineList.value(c));
                    findPathwaysTable->setItem(r, c, temp);
                }
            }
        }

        gefile.close();

        findPathwaysTable->connect(findPathwaysTable, SIGNAL(cellDoubleClicked(int,int)), this, SLOT(findPathwaysTableOpenBrowser(int)) );
        //open stats file and print in text edit
        QFile tfile(this->findPathwayProcess.getOutputFile()+".genes."+this->findPathwayProcess.getPathwayName()+".txt");
        if (!tfile.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

        int tlines = 0;

        QTextStream inT(&tfile);
        while (!inT.atEnd())
        {
            QString tline = inT.readLine();
            tlines++;
        }
        tfile.close();

        //print the statistics
        stringstream sstr1;
        sstr1 << lines;
        QString tmpLines = sstr1.str().c_str();
        stringstream sstr2;
        sstr2 << tlines;
        QString tmpTLines = sstr2.str().c_str();

        showFindPathwaysStatsTextEdit->append("Pathway: " + findPathwayProcess.getPathwayName());
        showFindPathwaysStatsTextEdit->append("Number of peaks with the pathway: " + tmpLines);
        showFindPathwaysStatsTextEdit->append("Number of genes with the pathway: " + tmpTLines);

        QFile pvfile(this->findPathwayProcess.getOutputFile()+".pvalue."+this->findPathwayProcess.getPathwayName()+".txt");
        if (!pvfile.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

        QTextStream inPV(&pvfile);

        showFindPathwaysStatsTextEdit->append(inPV.readLine());

        pvfile.close();
    }

    //disable stop button
    ui->stopRunFindPathwayProcessButton->setEnabled(false);
    //enable run button
    ui->runFindPathwayButton->setEnabled(true);
    //enable clear button
    ui->runFindPathwayClearButton->setEnabled(true);

    //start the progress bar
    progressBar->setVisible(0);

    return;
}

void CSDesktop::on_stopRunFindPathwayProcessButton_clicked()
{
    if(this->findPathwayProcess.state() == QProcess::Running)
    {
        this->findPathwayProcess.kill();
        ui->findPathwayProgressTextEdit->append("<font color=red> Find pathway process was terminated by the user.</font>");
    }
    //disable stop button
    ui->stopRunFindPathwayProcessButton->setEnabled(false);
    //enable run button
    ui->runFindPathwayButton->setEnabled(true);
    //enable clear button
    ui->runFindPathwayClearButton->setEnabled(true);

    this->findPathwayProcess.setFromError(true);

    //start the progress bar
    progressBar->setVisible(0);

    return;
}

//
// Functions that handle the FindPathway process
//

void CSDesktop::on_runPAGEButton_clicked()
{
    //clear the fromError variable
    this->pageProcess.setFromError(false);

    //clear the text edit area
    ui->pageProgressTextEdit->clear();

    //clear the targets results tab fields
    this->pageTable->clear();
    this->showPageStatsTextEdit->clear();


    //get the peaks file
    QString tmpPeaksFile = ui->pageSelectPeaksLineEdit->text();
    if(tmpPeaksFile.isNull() || tmpPeaksFile.isEmpty()) {
        //bring progress tab to front
        ui->pageTabWidget->setCurrentIndex(1);
        //throw message
        ui->pageProgressTextEdit->append("<font color=red> No peaks file was selected. Please try again. </font>");
        return;
    }
    else {
        //set the peaks file
        this->pageProcess.setPeaksFile(tmpPeaksFile);

        //set the outfile name
        if(ui->pageOutputFileLineEdit->text().isNull() || ui->pageOutputFileLineEdit->text().isEmpty()) {
            //bring progress tab to front
            ui->pageTabWidget->setCurrentIndex(1);
            //throw message
            ui->pageProgressTextEdit->append("<font color=red> No output name was selected. Please try again. </font>");
            return;
        }

        this->pageProcess.setOutputFile(ui->pageOutputFileLineEdit->text());

        //get the species and the pathways database
        QString tmpSpecies = ui->pageSpeciesComboBox->currentText();
        QString genome;

        QString tmpDatabase = ui->pagePathwaysDBComboBox->currentText();
        QString db;

        if(tmpSpecies == "Homo sapiens (hg18)") {
            genome = "hg18";

            if(tmpDatabase == "Gene Ontology (GO)") {
                db = "human_go_orf";
            }
            else if(tmpDatabase == "BioCarta") {
                db = "biocarta";
            }
            else if(tmpDatabase == "KEGG") {
                db = "kegg";
            }
            else if(tmpDatabase == "Human Protein Reference Database") {
                db = "HPRD_interactions";
            }
            else if(tmpDatabase == "SignatureDB") {
                db = "staudt_genesets";
            }
            else if(tmpDatabase == "Reactome") {
                db = "reactome";
            }
        }
        else if(tmpSpecies == "Mus musculus (mm9)") {
            genome  = "mm9";
            db      = "mouse_go_orf";
        }
        else if(tmpSpecies == "Drosophila melanogaster (dm3)") {
            genome  = "dm3";
            db      = "";
        }
        else if(tmpSpecies == "Saccharomyces cerevisiae (sacCer2)") {
            genome  = "sacser";
            db      = "yeast_go";
        }

        this->pageProcess.setSpecies(genome);
        this->pageProcess.setPathwaysDB(db);

        //get the genes database
        this->pageProcess.setGenesDB(ui->pageGenesDBComboBox->currentText());

        //get the geneparts
        QString geneparts;

        if(ui->pagePromotersCheckBox->isChecked()) {
            if(geneparts.isEmpty()) {
                geneparts = "P";
            }
            else {
                geneparts.append(",P");
            }
        }

        if(ui->pageExonsCheckBox->isChecked()) {
            if(geneparts.isEmpty()) {
                geneparts = "E";
            }
            else {
                geneparts.append(",E");
            }
        }

        if(ui->pageDownstreamCheckBox->isChecked()) {
            if(geneparts.isEmpty()) {
                geneparts = "DW";
            }
            else {
                geneparts.append(",DW");
            }
        }

        if(ui->pageIntergenicCheckBox->isChecked()) {
            if(geneparts.isEmpty()) {
                geneparts = "IG";
            }
            else {
                geneparts.append(",IG");
            }
        }

        if(ui->pageDistalCheckBox->isChecked()) {
            if(geneparts.isEmpty()) {
                geneparts = "D";
            }
            else {
                geneparts.append(",D");
            }
        }

        if(ui->pageIntronsCheckBox->isChecked()) {
            if(geneparts.isEmpty()) {
                geneparts = "I";
            }
            else {
                geneparts.append(",I");
            }
        }

        if(ui->pageIntron1CheckBox->isChecked()) {
            if(geneparts.isEmpty()) {
                geneparts = "I1";
            }
            else {
                geneparts.append(",I1");
            }
        }

        if(ui->pageIntron2CheckBox->isChecked()) {
            if(geneparts.isEmpty()) {
                geneparts = "I2";
            }
            else {
                geneparts.append(",I2");
            }
        }

        this->pageProcess.setGeneparts(geneparts);

        //set the arguments
        QStringList inputArguments;
        inputArguments << PAGE_PROGRAM << "--peakfile" << this->pageProcess.getPeaksFile()
                       << "--genome" << this->pageProcess.getSpecies()
                       << "--pathways" << this->pageProcess.getPathwaysDB()
                       << "--db" << this->pageProcess.getGenesDB()
                       << "--geneparts" << this->pageProcess.getGeneparts()
                       << "--prefix" << this->pageProcess.getOutputFile()
                       << "--runit" << "1"
                       << "--fromgui" << "1";

        //start process for INPUT data
        this->pageProcess.start(PERL, inputArguments);

        //start the progress bar
        progressBar->setVisible(1);

        //throw start message
        ui->pageProgressTextEdit->append("<font color=green> PAGE process for peaks has started. </font>");
    }

    //bring progress tab to front
    ui->pageTabWidget->setCurrentIndex(1);
    //enable stop button
    ui->stopRunPAGEProcessButton->setEnabled(true);
    //disable run button
    ui->runPAGEButton->setEnabled(false);
    //disable clear button
    ui->runPAGEClearButton->setEnabled(false);
}

void CSDesktop::readOutputFromPAGEProcess()
{
    ui->pageProgressTextEdit->append(this->pageProcess.readAllStandardOutput());
}

void CSDesktop::readErrorFromPAGEProcess() {
    ui->pageProgressTextEdit->append("<font color=red>" +
                                     this->pageProcess.readAllStandardError() + "</font>");

    if(this->pageProcess.state() == QProcess::Running)
    {
        this->pageProcess.kill();
        //disable stop button
        ui->stopRunPAGEProcessButton->setEnabled(false);
        //enable run button
        ui->runPAGEButton->setEnabled(true);
        //enable clear button
        ui->runPAGEClearButton->setEnabled(true);
        //throw end message
        ui->pageProgressTextEdit->append("<font color=red> PAGE process terminated.</font>");

        this->pageProcess.setFromError(true);

        //start the progress bar
        progressBar->setVisible(0);
    }
}

void CSDesktop::onPAGEProcessFinished()
{

    if(this->pageProcess.state() == QProcess::Running)
    {
        this->pageProcess.kill();
    }

    if(!(this->pageProcess.getFromError()))
    {
        //throw message
        ui->pageProgressTextEdit->append("<font color=green> PAGE process finished successfully.</font>");
        ui->pageProgressTextEdit->append("<font color=green> The results are in " + this->pageProcess.getOutputFile() + "</font>");

        //show results in new tab
        ui->pageTabWidget->addTab(showPageTab, "Results");
        ui->pageTabWidget->setCurrentIndex(ui->pageTabWidget->indexOf(showPageTab));

        QString tmpOut = this->pageProcess.getOutputFile().toStdString().substr(this->pageProcess.getOutputFile().toStdString().find_last_of("/"), this->pageProcess.getOutputFile().toStdString().length()).c_str();

        QString filename = pageProcess.getOutputFile();
        filename.append(".");
        filename.append(pageProcess.getGeneparts());
        filename.append("_PAGE/");
        filename.append(tmpOut);
        filename.append(".");
        filename.append(pageProcess.getGeneparts());
        filename.append(".summary.pdf");

        Poppler::Document* document = Poppler::Document::load(filename);

        if (!document || document->isLocked()) {

            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Information);
            msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                                  | Qt::WindowMinimizeButtonHint);
            msgBox.setText(tr("The document does not exist or it is locked."));
            msgBox.exec();

            delete document;
        }
        else {

            // Access page of the PDF file
            Poppler::Page* pdfPage = document->page(0);  // Document starts at page 0
            if (pdfPage == 0) {

                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Information);
                msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                                      | Qt::WindowMinimizeButtonHint);
                msgBox.setText(tr("Failed to open pdf page."));
                msgBox.exec();
            }
            else {

                // Generate a QImage of the rendered page
                QImage image = pdfPage->renderToImage(200.0, 200.0, -1, -1, -1, -1);
                if (image.isNull()) {

                    QMessageBox msgBox;
                    msgBox.setIcon(QMessageBox::Information);
                    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                                          | Qt::WindowMinimizeButtonHint);
                    msgBox.setText(tr("Failed to render pdf page to image."));
                    msgBox.exec();
                }
                else {

                    pageScrollArea->setGeometry(QRect(0, 13, 641, 571));
                    pageScrollArea->show();

                    pageScrollArea->setWidget(pageImageLabel);

                    pageImageLabel->setPixmap(QPixmap::fromImage(image, Qt::AutoColor));
                    pageImageLabel->resize(0.2*pageImageLabel->pixmap()->size());
                    pageImageLabel->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
                    pageImageLabel->setScaledContents(true);
                    pageImageLabel->show();

                    // after the usage, the page must be deleted
                    delete pdfPage;
                    delete document;
                }
            }
        }
    }

    //disable stop button
    ui->stopRunPAGEProcessButton->setEnabled(false);
    //enable run button
    ui->runPAGEButton->setEnabled(true);
    //enable clear button
    ui->runPAGEClearButton->setEnabled(true);

    //start the progress bar
    progressBar->setVisible(0);

    return;
}

void CSDesktop::on_stopRunPAGEProcessButton_clicked()
{
    if(this->pageProcess.state() == QProcess::Running)
    {
        this->pageProcess.kill();
        ui->pageProgressTextEdit->append("<font color=red> PAGE process was terminated by the user.</font>");
    }
    //disable stop button
    ui->stopRunPAGEProcessButton->setEnabled(false);
    //enable run button
    ui->runPAGEButton->setEnabled(true);
    //enable clear button
    ui->runPAGEClearButton->setEnabled(true);

    this->pageProcess.setFromError(true);

    //start the progress bar
    progressBar->setVisible(0);

    return;
}


//
// Functions that handle the Encode process
//

void CSDesktop::on_runEncodeButton_clicked()
{
    //clear the fromError variable
    this->encodeProcess.setFromError(false);

    //clear the text edit area
    ui->encodeProgressTextEdit->clear();

    //clear the targets results tab fields
    this->encodeTable->clear();
    this->showEncodeStatsTextEdit->clear();

    //get the peaks file
    QString tmpPeaksFile = ui->encodeSelectPeaksLineEdit->text();
    if(tmpPeaksFile.isNull() || tmpPeaksFile.isEmpty()) {
        //bring progress tab to front
        ui->encodeTabWidget->setCurrentIndex(1);
        //throw message
        ui->encodeProgressTextEdit->append("<font color=red> No peaks file was selected. Please try again. </font>");
        return;
    }
    else {
        //set the peaks file
        this->encodeProcess.setPeaksFile(tmpPeaksFile);

        //set the outfile name
        if(ui->encodePrefixFileLineEdit->text().isNull() || ui->encodePrefixFileLineEdit->text().isEmpty()) {
            //bring progress tab to front
            ui->encodeTabWidget->setCurrentIndex(1);
            //throw message
            ui->encodeProgressTextEdit->append("<font color=red> No prefix name was selected. Please try again. </font>");
            return;
        }

        this->encodeProcess.setPrefix(ui->encodePrefixFileLineEdit->text());
        this->encodeProcess.setOutputFile(this->encodeProcess.getPeaksFile().toStdString().substr(0, this->encodeProcess.getPeaksFile().toStdString().find_last_of("/")+1).c_str());

        QString iter;

        if(!ui->encodeALLCheckBox->isChecked()) {

            //get the dataset name
            if(ui->encodeSelectDatasetRadioButton->isChecked()) {
                if(ui->encodeSelectDatasetComboBox->currentText().isNull() || ui->encodeSelectDatasetComboBox->currentText().isEmpty()) {
                    QMessageBox msgBox;
                    msgBox.setIcon(QMessageBox::Critical);
                    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                                          | Qt::WindowMinimizeButtonHint);
                    msgBox.setText(tr("Empty value for the dataset name is not allowed. \nThe ENCODE datasets are missing (read INSTALL file)."));
                    msgBox.exec();
                    return;
                }
                else {
                    encodeProcess.setTFname(ui->encodeSelectDatasetComboBox->currentText());
                }
            }
            else {
                if(ui->encodeWriteDatasetLineEdit->text().isNull() || ui->encodeWriteDatasetLineEdit->text().isEmpty()) {
                    QMessageBox msgBox;
                    msgBox.setIcon(QMessageBox::Critical);
                    msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint
                                          | Qt::WindowMinimizeButtonHint);
                    msgBox.setText(tr("Empty value for the dataset name is not allowed. \n Please try again."));
                    msgBox.exec();
                    return;
                }
                else {
                    encodeProcess.setTFname(ui->encodeWriteDatasetLineEdit->text());
                }
            }

            //get the number of iterations
            this->encodeProcess.setIterations(ui->encodeIterationsSpinBox->text().toInt());

            iter.setNum(this->encodeProcess.getIterations());
        }

        QString tmpSpecies = ui->encodeSpeciesComboBox->currentText();
        QString genome;

        if(tmpSpecies == "Homo sapiens (hg18)") {
            genome = "hg18";
        }
        else if(tmpSpecies == "Homo sapiens (hg19)") {
            genome = "hg19";
        }

        //set the arguments
        QStringList inputArguments;
        inputArguments << NONGENICANNOTATE_PROGRAM << "--targets" << this->encodeProcess.getPeaksFile()
                       << "--type" << "Encode"
                       << "--genome" << genome
                       << "--prefix" << this->encodeProcess.getPrefix();

        if(ui->encodeALLCheckBox->isChecked()) {
            //inputArguments << "--mode" <<"all";
            inputArguments << "--TFname" <<"all";
            this->encodeProcess.setMode("all");
        }
        else {
            inputArguments << "--TFname" << this->encodeProcess.getTFname()
                           << "--iterations" << iter;
            this->encodeProcess.setMode("one");
        }

        //start process for INPUT data
        this->encodeProcess.start(PERL, inputArguments);

        //start the progress bar
        progressBar->setVisible(1);

        //throw start message
        ui->encodeProgressTextEdit->append("<font color=green> Find overlap with ENCODE process for peaks has started. </font>");
    }

    //bring progress tab to front
    ui->encodeTabWidget->setCurrentIndex(1);
    //enable stop button
    ui->stopRunEncodeProcessButton->setEnabled(true);
    //disable run button
    ui->runEncodeButton->setEnabled(false);
    //disable clear button
    ui->runEncodeClearButton->setEnabled(false);
}

void CSDesktop::readOutputFromEncodeProcess()
{
    ui->encodeProgressTextEdit->append(this->encodeProcess.readAllStandardOutput());
}

void CSDesktop::readErrorFromEncodeProcess()
{
    ui->encodeProgressTextEdit->append("<font color=red>" +
                                       this->encodeProcess.readAllStandardError() + "</font>");

    if(this->encodeProcess.state() == QProcess::Running)
    {
        this->encodeProcess.kill();
        //disable stop button
        ui->stopRunEncodeProcessButton->setEnabled(false);
        //enable run button
        ui->runEncodeButton->setEnabled(true);
        //enable clear button
        ui->runEncodeClearButton->setEnabled(true);
        //throw end message
        ui->encodeProgressTextEdit->append("<font color=red> Find overlap with ENCODE process terminated.</font>");

        this->encodeProcess.setFromError(true);

        //start the progress bar
        progressBar->setVisible(0);
    }
}

void CSDesktop::onEncodeProcessFinished()
{

    if(this->encodeProcess.state() == QProcess::Running)
    {
        this->encodeProcess.kill();
    }

    if(!(this->encodeProcess.getFromError()))
    {
        //throw message
        ui->encodeProgressTextEdit->append("<font color=green> Find overlap with ENCODE datasets finished successfully.</font>");

        if(this->encodeProcess.getMode().compare("one") == 0) {
            ui->encodeProgressTextEdit->append("<font color=green> The results are in " + this->encodeProcess.getOutputFile() + this->encodeProcess.getPrefix() + ".ENCODE." + this->encodeProcess.getTFname() +"</font>");

            //show results in new tab
            ui->encodeTabWidget->addTab(showEncodeTab, "Results");
            ui->encodeTabWidget->setCurrentIndex(ui->encodeTabWidget->indexOf(showEncodeTab));

            encodeTable->setGeometry(QRect(0, 23, 641, 381));
            encodeTable->show();

            encodeStatsLabel->setObjectName(QString::fromUtf8("genesStatsLabel"));
            encodeStatsLabel->setText("Information");
            encodeStatsLabel->setGeometry(QRect(0, 406, 641, 21));
            encodeStatsLabel->show();

            showEncodeStatsTextEdit->setGeometry(QRect(0, 429, 641, 101));
            showEncodeStatsTextEdit->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);
            showEncodeStatsTextEdit->show();

            encodeCopyButton->setGeometry(QRect(0, 540, 100, 21));
            encodeCopyButton->setText("Copy table");
            encodeCopyButton->show();
            encodeCopyButton->connect(encodeCopyButton, SIGNAL(clicked()), this, SLOT(encodeTableCopy()));
            encodeCopyButton->setStatusTip("Copy table contents to clipboard");
            encodeCopyButton->setToolTip("Copy table contents to clipboard");

            QFile gfile(this->encodeProcess.getOutputFile() + this->encodeProcess.getPrefix() + ".ENCODE."+this->encodeProcess.getTFname());

            if (!gfile.open(QIODevice::ReadOnly | QIODevice::Text))
                return;

            QTextStream inL(&gfile);

            int lines = 0;
            //count lines in the file
            while (!inL.atEnd())
            {
                QString gline = inL.readLine();
                lines++;
            }

            gfile.close();

            //set the table's rows and columns
            encodeTable->setRowCount(lines);
            encodeTable->setColumnCount(5);
            QStringList headerNames;
            headerNames << "Chromosome" << "Start" << "End" << "Num. of overlap. ENCODE peaks" << "Overlap. ENCODE peaks";
            encodeTable->setHorizontalHeaderLabels(headerNames);

            //open the file to add lines in the table
            QFile gefile(this->encodeProcess.getOutputFile() + this->encodeProcess.getPrefix() + ".ENCODE."+this->encodeProcess.getTFname());

            if (!gefile.open(QIODevice::ReadOnly | QIODevice::Text))
                return;

            QTextStream in(&gefile);

            //fill in the table with each row
            while (!in.atEnd())
            {
                for( int r = 0; r < encodeTable->rowCount(); r++ ){

                    QString gline           = in.readLine();
                    QStringList lineList    = gline.split("\t", QString::SkipEmptyParts);

                    for( int c = 0; c < encodeTable->columnCount(); ++c ){

                        QTableWidgetItem *temp = new QTableWidgetItem(lineList.value(c), 0);
                        temp->setText(lineList.value(c));
                        encodeTable->setItem(r, c, temp);
                    }
                }
            }

            gefile.close();

            encodeTable->connect(encodeTable, SIGNAL(cellDoubleClicked(int,int)), this, SLOT(encodeTableOpenBrowser(int)) );

            //open stats file and print in text edit
            QFile tfile(this->encodeProcess.getOutputFile() + this->encodeProcess.getPrefix() + ".ENCODE."+this->encodeProcess.getTFname() + ".stats");

            if (!tfile.open(QIODevice::ReadOnly | QIODevice::Text))
                return;

            QTextStream inT(&tfile);
            while (!inT.atEnd())
            {
                QString tline = inT.readLine();
                showEncodeStatsTextEdit->append(tline);
            }

            tfile.close();
        }

        else {
            ui->encodeTabWidget->removeTab(ui->encodeTabWidget->indexOf(showEncodeTab));
        }
    }
    //disable stop button
    ui->stopRunEncodeProcessButton->setEnabled(false);
    //enable run button
    ui->runEncodeButton->setEnabled(true);
    //enable clear button
    ui->runEncodeClearButton->setEnabled(true);

    //start the progress bar
    progressBar->setVisible(0);

    return;
}

void CSDesktop::on_stopRunEncodeProcessButton_clicked()
{
    if(this->encodeProcess.state() == QProcess::Running)
    {
        this->encodeProcess.kill();
        ui->encodeProgressTextEdit->append("<font color=red> Find overlap with ENCODE process terminated.</font>");
    }
    //disable stop button
    ui->stopRunEncodeProcessButton->setEnabled(false);
    //enable run button
    ui->runEncodeButton->setEnabled(true);
    //enable clear button
    ui->runEncodeClearButton->setEnabled(true);

    this->encodeProcess.setFromError(true);

    //start the progress bar
    progressBar->setVisible(0);

    return;
}

void CSDesktop::on_encodeALLCheckBox_clicked()
{
    if(ui->encodeALLCheckBox->isChecked()) {
        ui->encodeIterationsSpinBox->setEnabled(false);
        ui->encodeSelectDatasetRadioButton->setEnabled(false);
        ui->encodeSelectDatasetComboBox->setEnabled(false);
        ui->encodeWriteDatasetRadioButton->setEnabled(false);
        ui->encodeWriteDatasetLineEdit->setEnabled(false);
    }
    else{
        ui->encodeIterationsSpinBox->setEnabled(true);
        ui->encodeSelectDatasetRadioButton->setEnabled(true);
        ui->encodeSelectDatasetComboBox->setEnabled(true);
        ui->encodeWriteDatasetRadioButton->setEnabled(true);
        ui->encodeWriteDatasetLineEdit->setEnabled(true);
    }
}

