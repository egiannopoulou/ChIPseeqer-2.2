/*
***************************************************************
*
*   Filename    :   chipseeqerProcess.cpp
*   Description :   This is the class that wraps the
*                   chipseeqer process.
*   Version     :   1.0
*   Created     :   04/02/2010
*
***************************************************************
*
*   Compiler    :   gcc
*   IDE         :   Qt Creator 1.3.1
*
***************************************************************
*
*   Author      :   Eugenia G. Giannopoulou
*   Email       :   eug2002@med.cornell.edu
*
***************************************************************
*/

#include <QString>
#include "chipseeqerProcess.h"
#include "commonPaths.h"

ChipseeqerProcess::ChipseeqerProcess() {
}

QString ChipseeqerProcess::getChipFolder() {
    return this->chipFolder;
}

void ChipseeqerProcess::setChipFolder(QString cFolder) {
    this->chipFolder = cFolder;
}

QString ChipseeqerProcess::getInputFolder() {
    return this->inputFolder;
}

void ChipseeqerProcess::setInputFolder(QString iFolder) {
    this->inputFolder = iFolder;
}

QString ChipseeqerProcess::getOutputFile() {
    return this->outputFile;
}

void ChipseeqerProcess::setOutputFile(QString oFile) {
    this->outputFile = oFile;
}

QString ChipseeqerProcess::getDataFormat() {
    return this->dataFormat;
}

void ChipseeqerProcess::setDataFormat(QString format) {
    this->dataFormat = format;
}

QString ChipseeqerProcess::getChrdata() {
    return this->chrdata;
}

void ChipseeqerProcess::setChrdata(QString chr) {
    this->chrdata = chr;
}

double ChipseeqerProcess::getFold() {
    return this->fold;
}

void ChipseeqerProcess::setFold(double f) {
    this->fold = f;
}

int ChipseeqerProcess::getThreshold() {
    return this->threshold;
}

void ChipseeqerProcess::setThreshold(int t) {
    this->threshold = t;
}

int ChipseeqerProcess::getFragmentLength() {
    return this->fragmentLength;
}

void ChipseeqerProcess::setFragmentLength(int fraglen) {
    this->fragmentLength = fraglen;
}

int ChipseeqerProcess::getMinimumLength() {
    return this->minimumLength;
}

void ChipseeqerProcess::setMinimumLength(int minlen) {
    this->minimumLength = minlen;
}

int ChipseeqerProcess::getMinimumDistance() {
    return this->minimumDistance;
}

void ChipseeqerProcess::setMinimumDistance(int mindist) {
    this->minimumDistance = mindist;
}

int ChipseeqerProcess::getMinPeakHeight() {
    return this->minPeakHeight;
}

void ChipseeqerProcess::setMinPeakHeight(int minheight) {
    this->minPeakHeight = minheight;
}

bool ChipseeqerProcess::getUniqueReads() {
    return this->uniqueReads;
}

void ChipseeqerProcess::setUniqueReads(bool ureads) {
    this->uniqueReads = ureads;
}

bool ChipseeqerProcess::getFromError() {
    return this->fromError;
}

void ChipseeqerProcess::setFromError(bool err) {
    this->fromError = err;
}
