/*
***************************************************************
*
*   Filename    :   formatsDialog.cpp
*   Description :   This is the formatsDialog class.
*   Version     :   1.0
*   Created     :   04/02/2010
*
***************************************************************
*
*   Compiler    :   gcc
*   IDE         :   Qt Creator 1.3.1
*
***************************************************************
*
*   Author      :   Eugenia G. Giannopoulou
*   Email       :   eug2002@med.cornell.edu
*
***************************************************************
*/

#include <QWidget>
#include <QDialog>
#include <QEvent>

#include "formatsDialog.h"
#include "ui_formatsDialog.h"

FormatsDialog::FormatsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FormatsDialog)
{
    ui->setupUi(this);
    this->setFixedSize(size());
}

FormatsDialog::~FormatsDialog()
{
    delete ui;
}

void FormatsDialog::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void FormatsDialog::on_pushButton_clicked()
{
    this->close();
}
