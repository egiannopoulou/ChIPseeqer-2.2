/*
***************************************************************
*
*   Filename    :   aboutDialog.h
*   Description :   This is the header of the aboutDialog
*                   class.
*   Version     :   1.0
*   Created     :   08/04/2010
*
***************************************************************
*
*   Compiler    :   gcc
*   IDE         :   Qt Creator 1.3.1
*
***************************************************************
*
*   Author      :   Eugenia G. Giannopoulou
*   Email       :   eug2002@med.cornell.edu
*
***************************************************************
*/

#ifndef ABOUTDIALOG_H
#define ABOUTDIALOG_H

#include <QDialog>
#include <QWidget>
#include <QEvent>

namespace Ui {
    class AboutDialog;
}

class AboutDialog : public QDialog {
    Q_OBJECT
public:
    AboutDialog(QWidget *parent = 0);
    ~AboutDialog();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::AboutDialog *ui;

private slots:
    void on_pushButton_clicked();
};

#endif // ABOUTDIALOG_H
