/*
***************************************************************
*
*   Filename    :   consProcess.h
*   Description :   This is the header file for the class that
*                   wraps the CS2Cons process.
*   Version     :   1.0
*   Created     :   04/02/2010
*
***************************************************************
*
*   Compiler    :   gcc
*   IDE         :   Qt Creator 1.3.1
*
***************************************************************
*
*   Author      :   Eugenia G. Giannopoulou
*   Email       :   eug2002@med.cornell.edu
*
***************************************************************
*/

#ifndef CONSPROCESS_H
#define CONSPROCESS_H

#include <QProcess>
#include <QString>

namespace Ui {
    class ConsProcess;
}

class ConsProcess : public QProcess
{
public:
    ConsProcess();

    QString getPeaksFile();
    void setPeaksFile(QString peaks);
    QString getOutputFile();
    void setOutputFile(QString oFile);
    QString getOutputRandomFile();
    void setOutputRandomFile(QString orFile);
    QString getConsFolder();
    void setConsFolder(QString cFolder);
    QString getFormat();
    void setFormat(QString form);
    QString getCategory();
    void setCategory(QString cat);
    QString getMethod();
    void setMethod(QString met);
    QString getGenome();
    void setGenome(QString gen);
    bool getMakeRandom();
    void setMakeRandom(bool rand);
    int getRanDistance();
    void setRanDistance(int t);
    bool getShowProfiles();
    void setShowProfiles(bool prof);
    bool getAroundSummit();
    void setAroundSummit(bool as);
    int getWindowSize();
    void setWindowSize(int ws);
    double getThreshold();
    void setThreshold(double t);
    bool getFromError();
    void setFromError(bool err);

private:
    QString peaksFile;
    QString outputFile;
    QString outputRandomFile;
    QString consFolder;
    QString format;
    QString category;
    QString method;
    QString genome;
    bool    makeRandom;
    int     ranDist;
    bool    showProfiles;
    bool    aroundSummit;
    int     windowSize;
    double  threshold;
    bool    fromError;
};

#endif // CONSPROCESS_H
