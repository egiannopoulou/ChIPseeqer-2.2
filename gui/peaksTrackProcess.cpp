/*
***************************************************************
*
*   Filename    :   peaksTrackProcess.cpp
*   Description :   This is the class that wraps the
*                   CS2Track process.
*   Version     :   1.0
*   Created     :   04/02/2010
*
***************************************************************
*
*   Compiler    :   gcc
*   IDE         :   Qt Creator 1.3.1
*
***************************************************************
*
*   Author      :   Eugenia G. Giannopoulou
*   Email       :   eug2002@med.cornell.edu
*
***************************************************************
*/

#include <QString>
#include "peaksTrackProcess.h"
#include "commonPaths.h"

PeaksTrackProcess::PeaksTrackProcess()
{
}

QString PeaksTrackProcess::getPeaksFile() {
    return this->peaksFile;
}

void PeaksTrackProcess::setPeaksFile(QString peaks) {
    this->peaksFile = peaks;
}

QString PeaksTrackProcess::getTrackName() {
    return this->trackName;
}

void PeaksTrackProcess::setTrackName(QString name) {
    this->trackName = name;
}

bool PeaksTrackProcess::getFromError() {
    return this->fromError;
}

void PeaksTrackProcess::setFromError(bool err) {
    this->fromError = err;
}

QString PeaksTrackProcess::getOutputFile() {
    return this->outputFile;
}

void PeaksTrackProcess::setOutputFile(QString oFile) {
    this->outputFile = oFile;
}
