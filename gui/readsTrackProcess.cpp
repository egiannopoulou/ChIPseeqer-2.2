/*
***************************************************************
*
*   Filename    :   readsTrackProcess.cpp
*   Description :   This is the class that wraps
*                   the ChIPsmoothedReads2Track process.
*   Version     :   1.0
*   Created     :   04/02/2010
*
***************************************************************
*
*   Compiler    :   gcc
*   IDE         :   Qt Creator 1.3.1
*
***************************************************************
*
*   Author      :   Eugenia G. Giannopoulou
*   Email       :   eug2002@med.cornell.edu
*
***************************************************************
*/

#include <QString>
#include "readsTrackProcess.h"
#include "commonPaths.h"

ReadsTrackProcess::ReadsTrackProcess()
{
}

QString ReadsTrackProcess::getReadsDir() {
    return this->readsDir;
}

void ReadsTrackProcess::setReadsDir(QString reads) {
    this->readsDir = reads;
}

QString ReadsTrackProcess::getOutputFile() {
    return this->outputFile;
}

void ReadsTrackProcess::setOutputFile(QString oFile) {
    this->outputFile = oFile;
}

QString ReadsTrackProcess::getTrackName() {
    return this->trackName;
}

void ReadsTrackProcess::setTrackName(QString name) {
    this->trackName = name;
}

QString ReadsTrackProcess::getFormat() {
    return this->format;
}

void ReadsTrackProcess::setFormat(QString format) {
    this->format = format;
}

bool ReadsTrackProcess::getUniqueReads() {
    return this->uniqueReads;
}

void ReadsTrackProcess::setUniqueReads(bool ureads) {
    this->uniqueReads = ureads;
}

bool ReadsTrackProcess::getFromError() {
    return this->fromError;
}

void ReadsTrackProcess::setFromError(bool err) {
    this->fromError = err;
}
