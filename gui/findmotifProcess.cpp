/*
***************************************************************
*
*   Filename    :   findmotifProcess.cpp
*   Description :   This is the class that wraps the
*                   CSFindPeaksWithMotifsMatch process.
*   Version     :   1.0
*   Created     :   04/02/2010
*
***************************************************************
*
*   Compiler    :   gcc
*   IDE         :   Qt Creator 1.3.1
*
***************************************************************
*
*   Author      :   Eugenia G. Giannopoulou
*   Email       :   eug2002@med.cornell.edu
*
***************************************************************
*/

#include <QString>
#include "findmotifProcess.h"
#include "commonPaths.h"

FindmotifProcess::FindmotifProcess()
{
}

QString FindmotifProcess::getPeaksFile() {
    return this->peaksFile;
}

void FindmotifProcess::setPeaksFile(QString peaks) {
    this->peaksFile = peaks;
}

QString FindmotifProcess::getOutputFile() {
    return this->outputFile;
}

void FindmotifProcess::setOutputFile(QString oFile) {
    this->outputFile = oFile;
}

QString FindmotifProcess::getGenomeFile() {
    return this->genome;
}

void FindmotifProcess::setGenomeFile(QString gFile) {
    this->genome = gFile;
}

QString FindmotifProcess::getMotifName() {
    return this->motifName;
}

void FindmotifProcess::setMotifName(QString name) {
    this->motifName = name;
}

QString FindmotifProcess::getMotifSequence() {
    return this->motifSequence;
}

void FindmotifProcess::setMotifSequence(QString seq) {
    this->motifSequence = seq;
}

int FindmotifProcess::getThreshold() {
    return this->threshold;
}

void FindmotifProcess::setThreshold(int t) {
    this->threshold = t;
}

bool FindmotifProcess::getFromError() {
    return this->fromError;
}

void FindmotifProcess::setFromError(bool err) {
    this->fromError = err;
}

