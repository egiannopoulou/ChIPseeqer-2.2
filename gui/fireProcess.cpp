/*
***************************************************************
*
*   Filename    :   fireProcess.cpp
*   Description :   This is the class that wraps the
*                   CSFIRE process.
*   Version     :   1.0
*   Created     :   10/14/2010
*
***************************************************************
*
*   Compiler    :   gcc
*   IDE         :   Qt Creator 1.3.1
*
***************************************************************
*
*   Author      :   Eugenia G. Giannopoulou
*   Email       :   eug2002@med.cornell.edu
*
***************************************************************
*/

#include <QString>
#include "fireProcess.h"
#include "commonPaths.h"

FireProcess::FireProcess()
{
}

QString FireProcess::getPeaksFile() {
    return this->peaksFile;
}

void FireProcess::setPeaksFile(QString peaks) {
    this->peaksFile = peaks;
}

QString FireProcess::getPeaksFolder() {
    return this->peaksFolder;
}

void FireProcess::setPeaksFolder(QString folder) {
    this->peaksFolder = folder;
}

QString FireProcess::getOutputFolder() {
    return this->oFolder;
}

void FireProcess::setOutputFolder(QString of) {
    this->oFolder = of;
}

QString FireProcess::getGenome() {
    return this->genome;
}

void FireProcess::setGenome(QString g) {
    this->genome = g;
}

QString FireProcess::getSpecies() {
    return this->species;
}

void FireProcess::setSpecies(QString sp) {
    this->species = sp;
}

QString FireProcess::getRandmode() {
    return this->randmode;
}

void FireProcess::setRandmode(QString rm) {
    this->randmode = rm;
}

bool FireProcess::getSeed() {
    return this->seed;
}

void FireProcess::setSeed(bool s) {
    this->seed = s;
}

bool FireProcess::getIsFile() {
    return this->isFile;
}

void FireProcess::setIsFile(bool isf) {
    this->isFile = isf;
}

bool FireProcess::getFromError() {
    return this->fromError;
}

void FireProcess::setFromError(bool err) {
    this->fromError = err;
}
