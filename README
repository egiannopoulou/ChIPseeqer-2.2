==========================

ChIPseeqer (v2.2)

==========================


== Description ==

ChIPseeqer is a computational framework for the analysis of ChIP-seq datasets. 
It includes quality control tools for the raw data and peak detection. 
Moreover, it offers: (1) Gene-level annotation of peaks, (2) Pathways 
enrichment analysis, (3) Regulatory element analysis, using either a de novo 
approach, known or user-defined motifs, (4) Nongenic peak annotation (repeats, 
CpG islands, duplications), (5) Conservation analysis, (6) Clustering analysis, 
(7) Visualization, (8) Integration and comparison across different ChIP-seq 
experiments.


== License ==

ChIPseeqer is open-source software, released under the
GNU GENERAL PUBLIC LICENSE v.3. For more information,
please see the file called LICENSE.


== Download ==

ChIPseeqer is available at:

	https://gitlab.com/egiannopoulou/ChIPseeqer-2.2/
	
Annotation data are available at:

	http://physiology.med.cornell.edu/faculty/elemento/lab/CS_files/
	

== Installation ==

For installation instructions please take a look at the file called INSTALL.


== Documentation ==

ChIPseeqer is fully documented online at:

	http://physiology.med.cornell.edu/faculty/elemento/lab/chipseq.shtml


== Authors ==

The ChIPseeqer software has been developed in the Elemento lab, at the Institute
of Computational Biology of Weill Cornell Medical College 
(http://physiology.med.cornell.edu/faculty/elemento/lab/), by:

	Eugenia Giannopoulou, PhD	(giannopouloue@hss.edu)
	
	Olivier Elemento, PhD		(ole2001@med.cornell.edu)


== Contact ==

If you have any questions, bug reports or features suggestions please contact:

	Eugenia Giannopoulou (giannopouloue@.hss.edu)
