#!/usr/bin/perl

use lib "$ENV{PERLMODULESDIR}";
use Sets;
use strict;

use Getopt::Long;

if (@ARGV == 0) {
	die "Args --promoters=FILE --peakfile=FILE [ --hicdata=FILE ] \n";
}
my $promoters			= "$ENV{CHIPSEEQERDIR}/DATA/hg18/refSeq.oneperTSS_u2000_d2000.txt";
my $peakfile			= undef;
my $verbose			= 0;
my $dmin			= 0;
my $dmax			= 250000;
my $peakheightcol		= undef;
my $peakheight			= 50;
my $haspeaksummit		= 0;
my $resp			= undef;
my $expfile			= undef;
my $extract_lines		= 1000;
my $extract			= 1;		# if set to 1 it extracts lines, if set to 0 it uses the whole file (takes more time)

my $distances_file		= undef;
my $heights_file		= undef;

my @distances;
my @heights;

GetOptions("promoters=s"	=> \$promoters,
"verbose=s"			=> \$verbose,
"peakfile=s"			=> \$peakfile,
"dmin=s"			=> \$dmin,
"dmax=s"			=> \$dmax,
"resp=s"			=> \$resp,
"expfile=s"			=> \$expfile,
"peakheightcol=s"		=> \$peakheightcol,
"peakheight=s"			=> \$peakheight,
"haspeaksummit=s"		=> \$haspeaksummit,
"extract_lines=s"		=> \$extract_lines,
"extract=s"			=> \$extract);

#if (!defined($resp)) {
#	die "Error: --resp must be defined\n";
#}

if (!defined($expfile)) {
	die "Error: --expfile must be defined\n";
}

# store the height of each peak
my %S = ();
open IN, $peakfile or die "Cannot open $peakfile\n";
while (my $l = <IN>) {
	chomp $l;
	my @a = split /\t/, $l, -1;
	if(defined($peakheightcol)) {
		$S{"$a[0]\t$a[1]\t$a[2]"} = $a[$peakheightcol-1];
	}
	else {
		$S{"$a[0]\t$a[1]\t$a[2]"} = $a[$peakheight];
	}
}
close IN;

open INH, "> $peakfile.heights.txt";
open IND, "> $peakfile.distances.txt";

# run CompareIntervals to find overlap with promoters
my $todo = "$ENV{CHIPSEEQERDIR}/CompareIntervals -peakfile1 $promoters -hasid1 1 -ext1 48000 -peakfile2 $peakfile -show_ov_int 1 -showsummit2 $haspeaksummit ";
my $txt  = `$todo`;

if ($verbose == 1) {
	print "$todo\n";
}

my @a    = split /\n/, $txt;

print INH "GENE\t";
print IND "GENE\t";

for (my $i=1; $i<=200; $i++) {
	if($i!=200) {
		print INH "C$i\t";
		print IND "C$i\t";
	}
	else {
		print INH "C$i\n";
		print IND "C$i\n";
	}
}

# for each promoter
foreach my $r (@a) {
	
	my @b = split /\t/, $r;
	my $d1 = ($b[2] + $b[3])/2;
	my $g = shift @b;
		
	shift @b;
	shift @b;
	shift @b;
	my $num = shift @b;
	
	@distances	=();
	@heights	=();
	
	print INH "$g\t";
	print IND "$g\t";
	
	# parse each peak that overlap with promoter
	for (my $i=0; $i<$num; $i++) {
		my $p = $b[$i];
		my @c = split /\-/, $p;
		my $d2 = undef;
		
		if($haspeaksummit) {
			$d2 = $c[3];
		}
		else {
			$d2 = ($c[2] + $c[1])/2;
		}
		
		my $dist	= abs($d2 - $d1);
		my $pikid	= "$c[0]\t$c[1]\t$c[2]";
		my $height  = $S{$pikid};
		
		push(@distances, $dist);
		push(@heights, $height);
	}
	
	print INH join("\t", @heights) . "\n";
	print IND join("\t", @distances). "\n";
	
}

close INH;
close IND;

# Extract the first $extract_lines lines from distances and heights
if($extract == 1) {
		
	my $extract_todo1 = "head -$extract_lines $peakfile.distances.txt ";
	my $extract_todo2 = "head -$extract_lines $peakfile.heights.txt ";
	
	$extract_todo1 .= " > $peakfile.distances.part.txt";
	$extract_todo2 .= " > $peakfile.heights.part.txt";
	
	if ($verbose == 1) {
		print "$extract_todo1\n";
		print "$extract_todo2\n";
	}
	system($extract_todo1) == 0 or die "Cannot exec $extract_todo1\n";
	system($extract_todo2) == 0 or die "Cannot exec $extract_todo2\n";
	
	$distances_file		= "$peakfile.distances.part.txt";
	$heights_file		= "$peakfile.heights.part.txt";	
}
# or use all the peaks
else {
		
	$distances_file		= "$peakfile.distances.txt";
	$heights_file		= "$peakfile.heights.txt";	
}


#
# make the R script
#

# load distances and heights 
my $rscript = "distances <- read.csv(\"$distances_file\", header=T, row.names=1, sep=\"\\t\", check.names=T)\n";
$rscript .= "heights <- read.csv(\"$heights_file\", header=T, row.names=1, sep=\"\\t\", check.names=T)\n";

#load rpkm file
$rscript .= "rpkm <- read.csv(\"$expfile\", header=TRUE, sep=\"\\t\", row.names=1)\n";
$rscript .= "attach(data.frame(rpkm))\n";

# load estimateScore function
$rscript .= "source(\"$ENV{CHIPPCADIR}/estimateScore.R\")\n";

# run function that estimates scores
$rscript .= "optimize(estimateScore, c($dmin, $dmax), maximum=T)\n";

if ($verbose == 1) {
	print "$rscript\n";
}

# save R commands in file
open OUT, "> tmpscript.R";
print OUT "$rscript";
close OUT;

# Run R commands
my $tmp = undef;
open OUT, "| R --slave > tmp";
print OUT $rscript;
close OUT;

if ($verbose == 1) {
	system("cat tmp");
}
