#!/usr/bin/perl
use lib "$ENV{CHIPSEEQERDIR}";
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;

use Getopt::Long;

if (@ARGV == 0) {
  die "Args --proffile=FILE --pc=STR --topn=INT --th=FLOAT\n";
}

my $projfile = undef;
my $pc       = undef;
my $topn     = 250;
my $th       = undef;     
my $format   = "profile";

GetOptions("projfile=s" => \$projfile,
           "pc=s"       => \$pc,
	   "format=s"   => \$format,
	   "topn=s"     => \$topn,
	   "th=s"       => \$th);

# find column for PC
open IN, $projfile or die "Cannot open the $projfile\n";
my $l = <IN>; chomp $l;
my @a = split /\t/, $l;
my $idxpc = Sets::indexOfMatchingEntry(\@a, $pc);
if ($idxpc == -1) {
  die ("Cannot find $pc\n");
}

my @o = ();
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  push @o, [$a[0], $a[$idxpc]];
}
close IN;

@o = sort { $b->[1] <=> $a->[1] } @o;
if ($format eq "profile") {
  print "GENE\t$a[$idxpc]\n";
} 

if ($format eq "profile") {
  foreach my $r (@o) {
    if ($r->[1] > $th) {
      print "$r->[0]\t1\n";
    } else {
      print "$r->[0]\t0\n";
    }
  }
} else {
  
  if (defined($th)) {
    my $i = 0;
    my $txt = "";
    while ($o[$i]->[1] >= $th) {
      $txt .= "$pc\t$o[$i]->[0]\t$o[$i]->[1]\n";
      $i++;
    }
    
    if ($i >= 25) {
      print $txt;
    }
  } else {
    for (my $i=0; $i<$topn; $i++) {
      print "$o[$i]->[0]\t$o[$i]->[1]\n";
    }
  }

}
