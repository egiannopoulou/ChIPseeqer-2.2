#!/usr/bin/perl

use lib "$ENV{CHIPSEEQERDIR}";

use Getopt::Long;

# variables to store the arguments values
my $inputfile		= undef;
my $verbose			= 0;
my $id              = undef;
my $num             = undef;
my $last            = undef;
my $fasta           = undef;

# handling lack of arguments
if (@ARGV == 0) {
	die "Usage: fasta_concat_exons.pl --input=FILE --verbose=INT \n";
}

# processing command line options
GetOptions("input=s" => \$inputfile,
"verbose=i"		=> \$verbose );


open(IN, "$inputfile") or die "Can't open file $inputfile.";

while (my $line = <IN>) {
    if ($line =~ /^>/){
        chomp $line;
        my @a    = split /\//, $line, -1;
        $last    = $a[-1];
        my @b    = split /-/, $last, -1;
        
        if(defined $fasta && $id ne $b[0]) {
            print "$fasta\n";
        }
        
        $id      = $b[0];
        $num     = $b[1];
        
        if($num == 1){
            my $next = <IN>;
            chomp $next;
            print "$line\n";
            $fasta = $next;
        }
        else {
            my $next = <IN>;
            chomp $next;
            $fasta .= $next;
        }
    }
}
print "$fasta\n";


close IN;