#!/usr/bin/perl

use lib "$ENV{CHIPSEEQERDIR}";

use Getopt::Long;

# variables to store the arguments values

my $inputfile	= undef;
my $annotation	= undef;
my $genome      = "hg19";

# handling lack of arguments
if (@ARGV == 0) {
	die "Usage: convert_genes_to_refseq_promoters.pl --inputfile=FILE --annotation=FILE --genome=STR\n";
}

# processing command line options
GetOptions("inputfile=s"    => \$inputfile,
"annotation=s"              => \$annotation,
"genome=s"                  => \$genome,
"verbose=s"                 => \$verbose);

if (! defined $annotation) {
    $annotation	= "$ENV{CHIPSEEQERDIR}/DATA/$genome/refSeq.new.u2000_d2000";
}

#
# STEP 1
#
my $step1 = "perl $ENV{CHIPSEEQERDIR}/SCRIPTS/transcript_to_gene_transformation.pl --inputfile=$inputfile --type=\"toNM\" --genome=$genome --outtype=\"add\" > $inputfile.NM.txt";
if ($verbose == 1){
    print "$step1\n";
}
print "Step 1: Convert gene names to transcript names (ORF2NM)\n";
system($step1) == 0 or die "Cannot execute $step1.\n";

#
# STEP 2
#
my $step2 = "perl $ENV{CHIPSEEQERDIR}/SCRIPTS/sort_matrix.pl --matrixfile=$annotation --sortby=$inputfile.NM.txt";
if ($verbose == 1){
    print "$step2\n";
}
print "Step 2: Extract 2kb regions around TSS\n";
system($step2) == 0 or die "Cannot execute $step2.\n";