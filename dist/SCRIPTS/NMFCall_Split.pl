#!/usr/bin/perl
use lib "$ENV{CHIPSEEQERDIR}";

if ($ENV{PERLSCRIPTSDIR} eq "") {
	die "Please define \$PERLSCRIPTSDIR\n";
}

use Sets;
use strict;

use Getopt::Long;

# variables to store options
my $matrix		= undef;
my $expfile		= undef;
my $response	= undef;
my $label		= undef;

#other variables
my @seeding		= ("random");
#my @seeding		= ("random", "ica");
#my @seeding		= ("ica", "nndsvd", "random");

my @methods		= ("brunet");
#my @methods		= ("brunet", "lee", "nsNMF", "offset", "pe-nmf", "snmf/r");

my $seeding		= undef;
my $method		= undef;
my $nrun        = 30;

my $rscript_load	= undef;
my $rscript_nmf		= undef;
my $rscript_lm		= undef;

my $k			= undef;
my $rsq			= undef;
my $spear		= undef;

my $model1		= undef;
my @datalabels1	= undef;
my $add2model	= "";
my $start_k		= 2;
my $end_k		= 2;

my $d0				= 5000;
my $peakheightcol	= 4;
my $haspeaksummit	= 0;		# set to 1 when the peaks file have the peak_summit in the 7th column (ChIPseeqer output) 
my $genome			= "hg19";	# either hg19, or hg18

my $verbose		= 0;

# handling of missing arguments
if (@ARGV == 0) {
	die "NMFCall.pl
	Required:
	--matrix		= FILE
	--expfile		= FILE
	--response		= \"log\"(FPKM+1)\"\"
    --label			= STR
	Optional:
	--start_k		= INT
	--end_k			= INT
	--d0			= INT
	--peakheightcol	= INT
	--haspeaksummit	= [1|0]
	--genome		= [hg19|hg18]\n";
}

GetOptions("matrix=s"		=> \$matrix,
"expfile=s"		=> \$expfile,
"response=s"	=> \$response,
"label=s"		=> \$label,
"verbose=s"		=> \$verbose,
"model1=s"		=> \$model1,
"add2model=s"	=> \$add2model,
"start_k=s"		=> \$start_k,
"end_k=s"		=> \$end_k);

if (!defined($matrix) || !defined($expfile) || !defined($response) || !defined($label)) {
	die "NMFCall.pl
Required:
    --matrix    = FILE
    --expfile   = FILE
    --response  = log\"(FPKM+1)\"
    --label     = STR
Optional:
    --start_k   = INT
    --end_k     = INT\n";
}

#
# Make the R script for NMF
#

# Step 1: Load matrix and NMF package (only once)

$rscript_load = "
$label.table <- read.table(\"$matrix\", header=TRUE, row.names=1)
$label.mat <- data.matrix($label.table)
source(\"http://bioconductor.org/biocLite.R\")
biocLite(c(\"GenomicRanges\",\"IRanges\"))
install.packages(\"NMF\", repos = \"http://cran.us.r-project.org\")
library(NMF)

\n";

open OUT, "> tmpscript.R";
print OUT "$rscript_load";
close OUT;

open OUT, "| R --slave > $matrix.load.R";
print OUT $rscript_load;
close OUT;

if ($verbose == 1) {
	print "$rscript_load\n";
}

open FINALSTATS, ">$matrix.FINAL.txt";
print FINALSTATS "Method-parameters\tR-square\tSpearman's rank\n";

#
# Step 2: Run NMF for all methods
#
foreach (@methods) {
	
	$method=$_;
	
	print "Running NMF for method=$method, ";
	
	#... and seeds
	foreach (@seeding) {
		$seeding=$_;
		
		print "seed=$seeding, ";
		
		#... and different ranks (k)
		for($k=$start_k; $k<=$end_k; $k++) {
			
			print "k=$k\n";			
			
			# prepare and run the NMF-related commands.
			$rscript_nmf = "
			$label.mat
            $label.res.$k.$method.$seeding <- nmf($label.mat, method=\"$method\", seed=\"$seeding\", $k, nrun=$nrun)
			pdf(file=\"$matrix.$label.res.$k.$method.$seeding.heatmap.pdf\")
			coefmap($label.res.$k.$method.$seeding)
			dev.off()
            $label.res.$k.$method.$seeding.basis <- basis($label.res.$k.$method.$seeding)
			write.table($label.res.$k.$method.$seeding.basis, \"$matrix.$label.res.$k.$method.$seeding.basis.txt\",row.names=T, sep = \"\t\")
			\n";
			
			open OUT, ">> tmpscript.R";
			print OUT "$rscript_nmf";
			close OUT;
			
			my $Rnmf = "cat tmpscript.R | R --slave > $matrix.$label.res.$k.$method.$seeding.nmf.R";
			if ($verbose == 1) {
				print "$Rnmf\n";
			}
			system($Rnmf) == 0 or die "Cannot exec $Rnmf\n";
			
			my $basis = "$matrix.$label.res.$k.$method.$seeding.basis.txt";
						
			if (-e $basis) {
				
				# get rid of the " in the basis files
				system("perl -pi -e 's/\"//g' $basis");
				system("perl -pi -e 's/\V/GENEID\tV/' $basis");
				
				#
				# Step 3: Split basis file in one file per k (metagene)
				#
				my $splitTodo = "$ENV{CHIPSEEQERDIR}/SCRIPTS/SplitProjFile.pl --proj=$basis --d0=$d0 --genome=$genome ";
				$splitTodo	.= " --peakheightcol=$peakheightcol --haspeaksummit=$haspeaksummit ";
				
				if($verbose == 1) {
					print "$splitTodo\n";
				}
				
				system($splitTodo) == 0 or die "Cannot exec $splitTodo\n";
				
				#
				# Step 4: Merge all scores in one promoter-based matrix
				#				
				my $mergeTodo = "$ENV{CHIPSEEQERDIR}/SCRIPTS/expression_concatenate_matrices.pl $basis.*.PB > $basis.NM.txt ";
				
				if($verbose == 1) {
					print "$mergeTodo\n";
				}
				
				system($mergeTodo) == 0 or die "Cannot exec $mergeTodo\n";
				
				#
				# Step 5: Run regression analysis
				#
				
				open IN, "$basis.NM.txt";
				my $l = <IN>; chomp $l;
				@datalabels1 = split /\t/, $l, -1;
				shift @datalabels1;
				close IN;
				
				# merge the basis files with the expression data.
				my $f		= Sets::filename($expfile);
				my $outfile = "$basis.$f";
				my $todo	= "perl $ENV{PERLSCRIPTSDIR}/expression_add_one_matrix_to_another.pl $basis.NM.txt $expfile ";
				$todo .= " > $outfile";
				
				if ($verbose == 1) {
					print "$todo\n";
				}
				system($todo) == 0 or die "Cannot exec $todo\n";
				
				$model1  =  join(" + ", @datalabels1) . " $add2model";
				
				# run regression
				$rscript_lm = "
				m <- read.csv(\"$outfile\", header=T, row.names=1, sep=\"\\t\", check.names=T)
				attach(data.frame(m))\n";
				
				$rscript_lm .= "fit <- lm($response ~ $model1)\n";
				$rscript_lm .= "print(summary(fit))\n";
				$rscript_lm .= "print(cor.test(predict(fit, m), $response, method=\"spearman\"))\n";
				
				$rscript_lm .= "resi <- predict(fit,m) - $response\n";
				$rscript_lm .= "resi <- as.matrix(resi * resi)\n";
				$rscript_lm .= "rownames(resi) <- rownames(m)\n";
				$rscript_lm .= "write.table(resi, file=\"$outfile.residuals\", sep=\"\\t\", quote=F, row.names=T, col.names=NA)\n";
				
				# save ranks
				$rscript_lm .= "mp <- cbind( rank(predict(fit, m)) ,  rank( $response ),  rank(predict(fit, m)) - rank( $response ) )\n";
				$rscript_lm .= "rownames(mp) <- rownames(m)\n";
				$rscript_lm .= "write.table(mp, file=\"$outfile.ranks.txt\", sep=\"\t\", quote=F, row.names=T, col.names=NA)\n";
				
				# plot real vs response
				$rscript_lm .= "pdf(file=\"$outfile.predvsactual.pdf\")\n";
				$rscript_lm .= "pe <- predict(fit,m)\n";
				$rscript_lm .= "slope <- as.numeric(lm($response ~ pe)[[1]][2]);\n";
				$rscript_lm .= "inter <- as.numeric(lm($response ~ pe)[[1]][1]);\n";
				$rscript_lm .= "plot(pe, $response, xlim=c(min(pe),max(pe)), ylim=c(0,max(pe)), pch=\"+\")\n";
				$rscript_lm .= "abline(b=slope, a=inter, lwd=5, col=\"red\")\n";
				$rscript_lm .= "dev.off()\n";			
				
				if ($verbose == 1) {
					print "$rscript_lm\n";
				}
 
				open OUT2, "> tmpscript2.R";
				print OUT2 "$rscript_lm";
				close OUT2;
				
				my $Rlm = "cat tmpscript2.R | R --slave > $matrix.$label.res.$k.$method.$seeding.lm.R";
				system($Rlm) == 0 or die "Cannot exec $Rlm\n";
				
				if ($verbose == 1) {
					print "$Rlm\n";
				}
				
				#
				# print output of regression
				#
				open IN, "$matrix.$label.res.$k.$method.$seeding.lm.R" or die "Cannot open $matrix.$label.res.$k.$method.$seeding.lm.R\n";
				open STATS,  ">$matrix.$label.res.$k.$method.$seeding.lm.R.stats.txt";
				open STATS2, ">$matrix.$label.res.$k.$method.$seeding.lm.R.stats.simple.txt";
				
				$l = undef;
				do {
					$l = <IN>;
					print STATS "$l";
				} while ($l !~ /Intercept/);
				
				while ($l = <IN>) {
					print STATS "$l";
					chomp $l;
					my @a = split /\ +/, $l;
					if (($a[0] =~ /V/) || ($a[0] =~ /MIMA/)) {
						
						printf STATS2 "$a[0]\t%3.2f\t%3.2f", $a[1], $a[3];
						if ($a[4] eq "<") {
							print STATS2 "\t$a[4]$a[5]\n"; 
						} else {
							print STATS2 "\t$a[4]\n";
						}
					}
					if($l =~ /^Residual standard error/ || $l =~ /^Multiple R-squared/ || $l =~ /^F-statistic/) {
						chomp $l;
					}
					
					if($l =~ /^Multiple R-squared/) {
						$rsq = (split/ /, $l)[-1];
					}
					
					if($l =~ /rho/) {
						my $nextline=<IN>;
						chomp $nextline;
						$spear = (split/:/, $nextline)[-1];
					}
				}
				
				close STATS;
				close STATS2;
				close IN;
				
				print FINALSTATS "$method-$seeding-$k\t$rsq\t$spear\n";
				
				#unlink("$matrix.$label.res.$k.$method.$seeding.lm.R");
				#unlink("$matrix.$label.res.$k.$method.$seeding.nmf.R");
 
			}
			else {
				die("Cannot proceed: $basis file is not created.\n"); 
			}
		}
	}	
} 

close FINALSTATS;

#unlink<tmpscript*>;
#unlink("$matrix.load.R");