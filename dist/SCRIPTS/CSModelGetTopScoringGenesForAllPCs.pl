my $prof = $ARGV[0];

open IN, $prof or die "Cannot open $prof\n";
my $l = <IN>;
chomp $l;
my @a = split /\t/, $l, -1;
close IN;



for (my $i=1; $i<@a; $i++) {
  my $pc = $i;
  my $todo = "perl $ENV{CHIPSEEQERDIR}/SCRIPTS/CSModelGetTopScoringGenesInPC.pl --projfile=$prof --pc=PC$pc --th=2  --format=profilenot";
  system($todo) == 0 or die "Cannot exec $todo\n";
}
