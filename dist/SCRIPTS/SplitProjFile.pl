#!/usr/bin/perl
use lib "$ENV{CHIPSEEQERDIR}";

use Sets;
use strict;

use Getopt::Long;
my $proj		= undef;
my $peak_id		= undef;

my @peaks		= undef;
my @pclabels	= undef;
my %pcvalues	= undef;
my $verbose		= 0;

my $d0				= 5000;
my $peakheightcol	= 4;
my $haspeaksummit	= 0;
my $genome			= "hg19";  #Can be hg19 or hg18

if (@ARGV == 0) {
	die "Args: --proj=FILE		the projected PCs on the peaks\n";
}

GetOptions("proj=s"	=> \$proj,
"d0=s"				=> \$d0,
"peakheightcol=s"	=> \$peakheightcol,
"haspeaksummit=s"	=> \$haspeaksummit,
"genome=s"			=> \$genome,
"verbose=s"			=> \$verbose);

if (!defined($proj)) {
	die "--proj Input file (.proj from PCA) must be defined\n";
}

open IN, $proj;
my $l = <IN>; chomp $l;
@pclabels = split /\t/, $l, -1;
shift @pclabels;

while ($l = <IN>) {
	chomp $l;
	
	# get line
	my @a = split /\t/, $l;
	
	# get peak id
	my @b = split /\-/, $a[0];
	shift @a;
	
	$peak_id = join("\t", $b[0], $b[1], $b[2]);
	push @peaks, $peak_id;
	
	for my $i (0 .. $#a) {		
		push @{ $pcvalues{ $pclabels[$i] } }, $a[$i];	
	}
}
close IN;


foreach my $pc (@pclabels) {
	
	# Output each PC in a separate file
	open PCOUT, ">$proj.$pc";
	
	for my $j (0 .. $#peaks-1) {
		print PCOUT "$peaks[$j+1]\t";
		print PCOUT "${$pcvalues{$pc}}[$j]\n";
	}
	close PCOUT;	
	
	# Estimate the influence of each PC in the promoters
	my $calcTodo = "$ENV{CHIPSEEQERDIR}/SCRIPTS/CalcExtendedPeakScores.pl --peakfile=$proj.$pc --d0=$d0 --label=$pc --genome=$genome --verbose=$verbose ";
	
	$calcTodo.= "--peakheightcol=$peakheightcol --haspeaksummit=$haspeaksummit > $proj.$pc.$d0.PB";
	
	if($verbose == 1) {
		print "$calcTodo\n";
	}
	
	system($calcTodo) == 0 or die "Cannot exec $calcTodo\n";
	
	print "$calcTodo\n";	
}