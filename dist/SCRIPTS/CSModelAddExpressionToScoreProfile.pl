#!/usr/bin/perl
use lib "$ENV{HOME}/PERL_MODULES";
use Sets;
use strict;


# read in exp
open IN, $ARGV[1] or die "Cannot open $ARGV[1]\n";
my %EXP = ();
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  $EXP{$a[0]} = $a[1];
}
close IN;

open IN, $ARGV[0] or die "Cannot open $ARGV[0]\n";
while (my $l = <IN>) {
  chomp $l;
  my @a = split /\t/, $l, -1;
  print $l . "\t" . $EXP{$a[1]} . "\n";
}
close IN;

