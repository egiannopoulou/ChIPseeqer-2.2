#!/usr/bin/perl
use lib "$ENV{CHIPSEEQERDIR}";
use lib "$ENV{PERLSCRIPTSDIR}";

use Sets;
use strict;
use Getopt::Long;

# This script estimates the p-value for physical interactions occuring 
# from random sets of genes

my $genemaniaDir	= undef;
my $inputfile		= undef;
my $listfile		= undef;

my $ensemblefile	= "/Users/eug2002/Development/ANALYSIS/Modelling/H-GM-K_genemania_analysis/ENCODE_FACTORS_list_ENS.txt";
#my $ensemblefile	= "/Users/eug2002/Development/ANALYSIS/Modelling/H-GM-K_genemania_analysis/ENCODE_FACTORS_list_GENENAME.txt";

my $inter_file		= undef;
my $query			= undef;
my $names			= undef;
my $pair			= undef;
my $weight			= undef;
my $iter			= 100;
my $N				= 10;
my @rand_pairs		= ();
my $rand_str		= undef;
my $r1				= undef;
my $r2				= undef;
my %res				= ();
my $sum				= undef;
my $value			= undef;
my $fvalue			= undef;
my @complex_N		= ();
my $totalcntrndm	= undef;
my $size			= undef;
my @all_names       = ();
my $cnt             = undef;
my $cnt_qr          = undef;
my $cnt_pairs_n     = undef;
my $verbose			= 0;

my %INTERACTIONS	= ();

if (@ARGV == 0) {
	die "Args:	--genemaniaDir=FILE		the directory with the interactions files
	--listfile=FILE		the refSeq gene names\n";
}

GetOptions("genemaniaDir=s"	=> \$genemaniaDir,
"listfile=s"		=> \$listfile,
"iter=s"			=> \$iter,
"N=s"				=> \$N,
"verbose=s"			=> \$verbose);

if(!defined($genemaniaDir)) {
	#$genemaniaDir	= "/Users/eug2002/Development/ChIPseeqer/tools/PAGE/PAGE_DATA/ANNOTATIONS/human_go_orf/GO_terms_ORF_out/";
    #$genemaniaDir	= "/Users/eug2002/Development/DATA_ANNOTATIONS/genemania/homo_sapiens/physical_interactions/";
    $genemaniaDir	= "/Users/eug2002/Development/DATA_ANNOTATIONS/genemania/homo_sapiens/co-expression/";
	#$genemaniaDir	= "/Users/eug2002/Development/DATA_ANNOTATIONS/genemania/homo_sapiens/Homo_sapiens.COMBINED/";
}

# Read interactions files and store in Hash
my @contents = ();

opendir(DIR, $genemaniaDir) or die "Cannot open $genemaniaDir\n";	
@contents = grep !/^\.\.?$/, readdir DIR; # skip . and ..
foreach $inter_file (@contents) {
	
	#print "$inter_file\n";
	
	open IN, "$genemaniaDir/$inter_file" or die "Cannot open $inter_file\n";
	while (my $line = <IN>) {
		chomp $line;
		#print "$line\n";
		
		my @c = split /\t/, $line;
		$pair="$c[0]\t$c[1]";
		$weight="$c[2]";
		
		if(exists $INTERACTIONS{$pair}) {
			$INTERACTIONS{$pair} .= "$weight\t$inter_file\n";
		}
		else {
			$INTERACTIONS{$pair} = "$weight\t$inter_file\n";
		}
	}
	close IN;
}
closedir DIR;

#
# open file with listing all complexes files and estimate physical interactions for the whole network
#
open IN, $listfile or die "cannot open $listfile\n";

# read every line
while (my $l = <IN>) {
	chomp $l;
	
	$names = "";
	
	# store complex file name
	my ($inputfile, $N) = split /\t/, $l;  	
	push(@complex_N, $N);
	
	print "$inputfile\t$N\n";
	
	# read complex file, genes per complex
	open INF, "$inputfile" or die "Cannot open $inputfile\n";
	while (my $l = <INF>) {
		chomp $l;
		my @a = split /\t/, $l;
		
		$names		.="$a[0]\t";
	}
	close INF;
	
    push @all_names, $names;
    #print "$names\n";
    
	# Estimate weights for the input
	print "Printing genemania weights for input $inputfile\n";
	&EstimateWeights($names);
	
	$fvalue +=$value;
}

print "@all_names\n";
print "PAIRS IN COMPLEXES: $cnt_pairs_n\n";
print "PAIRS with INTERACTIONS: $fvalue\n";

close IN;


#my @b = split /\t/, $names;

for(my $i==0; $i<=$#all_names; $i++) {
    
    #print "\ni:$all_names[$i]\n";
    my @b = split /\t/, $all_names[$i];
    for(my $k=0; $k<=$#b; $k++) {
        #print "k:$b[$k]\n";        
        for(my $j=0; $j<=$#all_names; $j++) {
            if($i!=$j) {
                #print "j:$all_names[$j]\n";
                my @c = split /\t/, $all_names[$j];
                for(my $l=0; $l<=$#c; $l++) {
                    #print "l:$c[$l]\n";
                    $query	= "$b[$k]\t$c[$l]";
                    #print "PAIR:$query\n";
                    
                    if(exists($INTERACTIONS{$query})) {
                        print "Query:\t$query\n$INTERACTIONS{$query}\n";
                        $cnt_qr++;
                    }
                    
                    $cnt++;
                }
            }
        }
    }
}

print "PAIRS BETWEEN COMPLEXES: $cnt\n";
print "PAIRS with interactions: $cnt_qr\n";

sub EstimateWeights {
	
	my ($names) = @_;	
	
	my @b = split /\t/, $names;
	
	#print "$names\n";
	
	my $cnt_n = 0;
	
	for(my $i=0; $i<=$#b; $i++) {
		for(my $j=0; $j<=$#b; $j++) {
			if($i!=$j) {
				$query	= "$b[$i]\t$b[$j]";
				$query =~s/ //g;
                $cnt_pairs_n++;
				#print "$query\n";
				if(exists($INTERACTIONS{$query})) {
					print "Query:\t$query\n$INTERACTIONS{$query}\n";
					$cnt_n++;
				}
			}
		}
	}
	$value = $cnt_n;	
}