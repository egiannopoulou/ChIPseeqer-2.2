#!/usr/bin/perl
use lib "$ENV{CHIPSEEQERDIR}";
use lib "$ENV{PERLSCRIPTSDIR}";

use Sets;
use strict;
use Getopt::Long;

# Read input matrix file
# For n times
# make a shuffled file
# run NMF and split (not lm)
# transform basis.FPKM file to basis.FPKM_noNM
# run lm
# run rf

my $file		= undef;
my $expfile		= undef;
my $iter		= 10;
my $k			= 2;
my $method		= "lee";
my $label		= "K";
my $verbose		= 0;

if (@ARGV == 0) {
	die "Args:	--file=FILE		the matrix file
	--expfile=FILE		the RNA expression file
	--iterations=INT	number of iterations for shuffling
	--k=INT				rank for NMF
	--method=STR		method for NMF
	--label=STR			label for NMF result\n";
}

GetOptions("file=s"			=> \$file,
"expfile=s"		=> \$expfile,
"iterations=s"	=> \$iter,
"k=s"			=> \$k,
"method=s"		=> \$method,
"label=s"		=> \$label,
"verbose=s"		=> \$verbose);

for (my $i=1; $i<=$iter; $i++) {

	# make shuffled file
	my $shuffleTodo = "shuffle_rows.pl $file > $file.shuffle.$i.txt";
	
	if($verbose == 1) {
		print "$shuffleTodo\n";
	}
	#system($shuffleTodo) == 0 or die "Cannot exec $shuffleTodo\n";	
	
	# call NMF
	my $NMFTodo = "NMFCall_Split.pl --matrix=$file.shuffle.$i.txt --expfile=$expfile --label=$label --start_k=$k --end_k=$k --response=\"log(FPKM+1)\"";

	if($verbose == 1) {
		print "$NMFTodo\n";
	}
	#system($NMFTodo) == 0 or die "Cannot exec $NMFTodo\n";
	
	# get rid of NAs in basis file
	my $n= $k+2;
	
	my $cleanTodo = "awk '{if(NF==$n) print \$0}' $file.shuffle.$i.txt.$label.res.$k.$method.random.basis.txt.K562_FPKM.txt > $file.shuffle.$i.txt.$label.res.$k.$method.random.basis.txt.K562_FPKM_noNM.txt";
	
	if($verbose == 1) {
		print "$cleanTodo\n";
	}
	system($cleanTodo) == 0 or die "Cannot exec $cleanTodo\n";

	# Run LM
	my $lmTodo = "LinearRegressionCall.pl --matrix=$file.shuffle.$i.txt.$label.res.$k.$method.random.basis.txt.K562_FPKM_noNM.txt";
	
	if($verbose == 1) {
		print "$lmTodo\n";
	}
	system($lmTodo) == 0 or die "Cannot exec $lmTodo\n";

	# Run RF
	my $rfTodo = "RandomForestCall.pl --matrix=$file.shuffle.$i.txt.$label.res.$k.$method.random.basis.txt.K562_FPKM_noNM.txt";
	
	if($verbose == 1) {
		print "$rfTodo\n";
	}
	system($rfTodo) == 0 or die "Cannot exec $rfTodo\n";
}