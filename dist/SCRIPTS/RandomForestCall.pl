#!/usr/bin/perl
use strict;

use Getopt::Long;

# variables to store options
my $matrix		= undef;

my $rscript		= undef;
my $verbose		= 0;

# handling of missing arguments
if (@ARGV == 0) {
	die "RandomForestCall.pl
	Required:
	--matrix		= FILE\n";
}

GetOptions("matrix=s"	=> \$matrix,
"verbose=s"				=> \$verbose);

if (!defined($matrix)) {
	die "RandomForestCall.pl
	Required:
	--matrix		= FILE\n";
}

#
# Step 1: Load libraries and data
#

$rscript = "

#load libraries
library(foreach)
library(doMC)
library(randomForest)

#load data
nm <- read.csv(\"$matrix\", header=T, row.names=1, sep=\"\\t\", check.names=T)
attach(data.frame(nm))
\n";

#
# Step 2: Prepare train and test sets
#

$rscript .= "

#prepare train and test sets
scores <- nm[1:ncol(nm)-1]
NMF_TRAIN = 1:nrow(nm) %% 4 != 0;
NMF_TEST = !NMF_TRAIN;
\n";

#
# Step 3: Run RF regression
#

$rscript .= "

#run random forest regression
nmf_forest = randomForest(x = scores[NMF_TRAIN,], y = log(FPKM[NMF_TRAIN]+1));
nmf_forest_predicts = predict(nmf_forest, scores[NMF_TEST,]);
nmf_forest_predictability = cor(nmf_forest_predicts, log(FPKM[NMF_TEST]+1), method = c(\"spearman\"));

#print results
rsq = nmf_forest\$rsq[500]

lapply(\"$matrix\", write, \"$matrix.results.txt\", append=TRUE, ncolumns=1000)

lapply(\"R squared\", write, \"$matrix.results.txt\", append=TRUE, ncolumns=1000)
lapply(rsq, write, \"$matrix.results.txt\", append=TRUE, ncolumns=1000)

lapply(\"RF predictability\", write, \"$matrix.results.txt\", append=TRUE, ncolumns=1000)
lapply(nmf_forest_predictability, write, \"$matrix.results.txt\", append=TRUE, ncolumns=1000)

slope <- as.numeric(lm((log(FPKM[NMF_TEST]+1) ~ nmf_forest_predicts))[[1]][2])
inter <- as.numeric(lm((log(FPKM[NMF_TEST]+1) ~ nmf_forest_predicts))[[1]][1])

pdf(file=\"$matrix.predvsactual.rf.pdf\")
plot(nmf_forest_predicts, log(FPKM[NMF_TEST]+1), xlim=c(min(nmf_forest_predicts),max(nmf_forest_predicts)), ylim=c(0,max(nmf_forest_predicts)), pch=\"+\")
abline(b=slope, a=inter, lwd=5, col=\"red\")
dev.off()

detach(data.frame(nm))

\n";

open OUT, "> tmpscript.R";
print OUT "$rscript";
close OUT;

open OUT, "| R --slave > $matrix.runRF.R";
print OUT $rscript;
close OUT;

if ($verbose == 1) {
	print "$rscript\n";
	system("cat $matrix.runRF.R");
}

#unlink("$matrix.runRF.R");