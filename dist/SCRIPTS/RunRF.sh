#!/bin/sh

for f in $(ls *_FPKM.txt)
do
        echo "file: $f";
		awk -F"\t" '{if($NF!="") print $0}' $f > $f.new
		
		RandomForestCall.pl --matrix=$f.new --response=FPKM --start_k=2 --end_k=11

done
