#!/usr/bin/perl
use lib "$ENV{CHIPSEEQERDIR}";
use lib "$ENV{PERLSCRIPTSDIR}";

use Sets;
use strict;
use Getopt::Long;

# This script estimates the p-value for physical interactions occuring 
# from random sets of genes

my $genemaniaDir	= undef;
my $inputfile		= undef;
my $listfile		= undef;

my $ensemblefile	= "/Users/eug2002/Development/ANALYSIS/Modelling/H-GM-K_genemania_analysis/ENCODE_FACTORS_list_ENS.txt";
#my $ensemblefile	= "/Users/eug2002/Development/ANALYSIS/Modelling/H-GM-K_genemania_analysis/ENCODE_FACTORS_list_GENENAME.txt";

my $inter_file		= undef;
my $query			= undef;
my $names			= undef;
my $pair			= undef;
my $weight			= undef;
my $iter			= 100;
my $N				= 10;
my @rand_pairs		= ();
my $rand_str		= undef;
my $r1				= undef;
my $r2				= undef;
my %res				= ();
my $sum				= undef;
my $value			= undef;
my $fvalue			= undef;
my @complex_N		= ();
my $totalcntrndm	= undef;
my $size			= undef;

my $verbose			= 0;

my %INTERACTIONS	= ();

if (@ARGV == 0) {
	die "Args:	--genemaniaDir=FILE		the directory with the interactions files
	--listfile=FILE		the refSeq gene names\n";
}

GetOptions("genemaniaDir=s"	=> \$genemaniaDir,
"listfile=s"		=> \$listfile,
"iter=s"			=> \$iter,
"N=s"				=> \$N,
"verbose=s"			=> \$verbose);

if(!defined($genemaniaDir)) {
	#$genemaniaDir	= "/Users/eug2002/Development/ChIPseeqer/tools/PAGE/PAGE_DATA/ANNOTATIONS/human_go_orf/GO_terms_ORF_out/";
    $genemaniaDir	= "/Users/eug2002/Development/DATA_ANNOTATIONS/genemania/homo_sapiens/physical_interactions/";
    #$genemaniaDir	= "/Users/eug2002/Development/DATA_ANNOTATIONS/genemania/homo_sapiens/co-expression/";
	#$genemaniaDir	= "/Users/eug2002/Development/DATA_ANNOTATIONS/genemania/homo_sapiens/Homo_sapiens.COMBINED/";
}

# Read interactions files and store in Hash
my @contents = ();

opendir(DIR, $genemaniaDir) or die "Cannot open $genemaniaDir\n";	
@contents = grep !/^\.\.?$/, readdir DIR; # skip . and ..
foreach $inter_file (@contents) {
	
	print "$inter_file\n";
	
	open IN, "$genemaniaDir/$inter_file" or die "Cannot open $inter_file\n";
	while (my $line = <IN>) {
		chomp $line;
		#print "$line\n";
		
		my @c = split /\t/, $line;
		$pair="$c[0]\t$c[1]";
		$weight="$c[2]";
		
		if(exists $INTERACTIONS{$pair}) {
			$INTERACTIONS{$pair} .= "$weight\t$inter_file\n";
		}
		else {
			$INTERACTIONS{$pair} = "$weight\t$inter_file\n";
		}
	}
	close IN;
}
closedir DIR;

#
# open file with listing all complexes files and estimate physical interactions for the whole network
#
open IN, $listfile or die "cannot open $listfile\n";

# read every line
while (my $l = <IN>) {
	chomp $l;
	
	$names = "";
	
	# store complex file name
	my ($inputfile, $N) = split /\t/, $l;  	
	push(@complex_N, $N);
	
	print "$inputfile\t$N\n";
	
	# read complex file, genes per complex
	open INF, "$inputfile" or die "Cannot open $inputfile\n";
	while (my $l = <INF>) {
		chomp $l;
		my @a = split /\t/, $l;
		
		$names		.="$a[0]\t";
	}
	close INF;
	
	# Estimate weights for the input
	print "Printing genemania weights for input $inputfile\n";
	&EstimateWeights($names);
	
	$fvalue +=$value;
}

print "FINAL VALUE:$fvalue\n";
close IN;


# make random networks of complexes, same size with original
for(my $k=0; $k<$iter; $k++) {
	
	#print "Run: $k\n";
	$totalcntrndm = 0;
	
	for(my $i=0; $i<=$#complex_N; $i++) {
		
		@rand_pairs = "";
		#print "Complex size:$complex_N[$i]\n";
		# open Ensemble file and extract random genes
		open ENS, "$ensemblefile" or die "Cannot open $ensemblefile\n";
		while (<ENS>) {
			if (@rand_pairs < $complex_N[$i]) {
				push @rand_pairs, $_;
				($r1, $r2) = (rand(@rand_pairs), rand(@rand_pairs));
				($rand_pairs[$r1], $rand_pairs[$r2]) = ($rand_pairs[$r2], $rand_pairs[$r1]);
			} else {
				rand($.) <= $complex_N[$i] and $rand_pairs[rand(@rand_pairs)] = $_;
			}
		}
		
		for (@rand_pairs) 
		{
			s/\n/\t/g;
		}
		close ENS;
		
		my $rand_str = "@rand_pairs";
		#print "$rand_str\n";
		
		# print the weights for the random pairs
		&EstimateRandomWeights($rand_str, $k);
	}
	
	#print "TOTAL for $k: $totalcntrndm\n";
	$res{$k} = $totalcntrndm;
}

foreach my $key( sort keys %res ) {
	if($res{$key} > $fvalue)	{
		#print "$key\t$res{$key}\n";
		$size++;
	}
}

my $pv		= $size/$iter;
print "real interactions: $fvalue\n";
print "p-val: $size/$iter = $pv\n";


sub EstimateWeights {
	
	my ($names) = @_;	
	
	my @b = split /\t/, $names;
	
	#print "$names\n";
	
	my $cnt = 0;
	
	for(my $i=0; $i<=$#b; $i++) {
		for(my $j=0; $j<=$#b; $j++) {
			if($i!=$j) {
				$query	= "$b[$i]\t$b[$j]";
				$query =~s/ //g;
				#print "$query\n";
				if(exists($INTERACTIONS{$query})) {
					print "Query:\t$query\n$INTERACTIONS{$query}\n";
					$cnt++;
				}
			}
		}
	}
	$value = $cnt;	
}

sub EstimateRandomWeights {
	
	my ($names, $k) = @_;	
	
	my @b = split /\t/, $names;
	
	my $cnt = 0;
	
	#print "$k: $names\n";
	
	for(my $i=0; $i<=$#b; $i++) {
		for(my $j=0; $j<=$#b; $j++) {
			if($i!=$j) {
				$query	= "$b[$i]\t$b[$j]";
				$query =~s/ //g;
				#print "$query\n";
				if(exists($INTERACTIONS{$query})) {
					#print "Query:\t$query\n$INTERACTIONS{$query}\n";
					$cnt++;
				}
			}
		}
	}
	$totalcntrndm += $cnt;
}