#!/usr/bin/perl
use lib "$ENV{CHIPSEEQERDIR}";

if ($ENV{PERLSCRIPTSDIR} eq "") {
	die "Please define \$PERLSCRIPTSDIR\n";
}

use Sets;
use strict;

use Getopt::Long;
my $evec1		= undef;
my $evec2		= undef;
my $expfile		= undef;
my $verbose		= 0;
my $add2model	= "";
my $model1		= undef;
my $model2		= undef;
my $reg			= "log";
my $resp		= undef;
my $anova		= 0;
my $removeNR    = 1;
my $knockdown   = undef;
my $addterms    = undef;
my @terms		= ();
my $verbose		= 1;

my @datalabels1	= undef;
my @datalabels2	= undef;

if (@ARGV == 0) {
	die "Args: --evec1=FILE --expfile=FILE
	where:
	--evec1=FILE             eigenvectors file of model 1
	--evec2=FILE             eigenvectors file of model 2
	--expfile=FILE           expression file in binary format
	\n";
}

GetOptions("evec1=s"	=> \$evec1,
"evec2=s"		=> \$evec2,
"verbose=s"		=> \$verbose,
"add2model=s"	=> \$add2model,
"addterms=s"	=> \$addterms,
"resp=s"		=> \$resp,
"reg=s"			=> \$reg,
"removeNR=s"	=> \$removeNR,
"knockdown=s"	=> \$knockdown,
"model1=s"		=> \$model1,
"expfile=s"		=> \$expfile,
"anova=s"		=> \$anova,
"verbose=s"		=> \$verbose);


if (!defined($resp)) {
	die "--resp must be defined\n";
}

if (!defined($evec1)) {
	die "--evec1 Eigenvectors file must be defined\n";
}

if (defined($addterms)) {
	@terms = split /\,/, $addterms;
}

#open IN, $expfile;
#my $l = <IN>; chomp $l;
#my @a = split /\t/, $l, -1;
#my $explabel = $a[1];
#close IN;

open IN, $evec1;
my $l = <IN>; chomp $l;
@datalabels1 = split /\t/, $l, -1;
shift @datalabels1;
close IN;

# synchronize
my $f		= Sets::filename($expfile);
my $outfile = "$evec1.$f";
my $todo	= "perl $ENV{PERLSCRIPTSDIR}/expression_add_one_matrix_to_another.pl $evec1 $expfile ";

if($verbose==1) {
	print "$todo\n";
}

if ($removeNR == 1) {
	$todo .= " | grep -v NR_ ";
	
	if (defined($knockdown)) {
		system("cat $knockdown | grep -v NR_ > $knockdown.NR");
		$knockdown = "$knockdown.NR";
	}
	
}
$todo .= " > $outfile";
system($todo) == 0 or die "Cannot exec $todo\n";

if (!defined($model1)) {
	$model1  =  join(" + ", @datalabels1) . " $add2model";
	if (@terms > 0) {
		$model1 .= "+" . join(" + ", @terms);
	}
}

#
# Run only regression for model 1
#
if($anova eq 0) {
	#
	# make the R script
	#
	my $rscript = "
	m <- read.csv(\"$outfile\", header=T, row.names=1, sep=\"\\t\", check.names=T)
	attach(data.frame(m))\n";
	
	$rscript .= "fit <- lm($resp ~ $model1)\n";
	$rscript .= "print(summary(fit))\n";
	$rscript .= "print(cor.test(predict(fit, m), $resp, method=\"spearman\"))\n";
	
	$rscript .= "resi <- predict(fit,m) - $resp\n";
 	$rscript .= "resi <- as.matrix(resi * resi)\n";
 	$rscript .= "rownames(resi) <- rownames(m)\n";
	$rscript .= "write.table(resi, file=\"$outfile.residuals\", sep=\"\\t\", quote=F, row.names=T, col.names=NA)\n";
	
	# save ranks
	$rscript .= "mp <- cbind( rank(predict(fit, m)) ,  rank( $resp ),  rank(predict(fit, m)) - rank( $resp ) )\n";
	$rscript .= "rownames(mp) <- rownames(m)\n";
	$rscript .= "write.table(mp, file=\"$outfile.ranks.txt\", sep=\"\t\", quote=F, row.names=T, col.names=NA)\n";
	
	# plot real vs resp
	$rscript .= "pdf(file=\"$outfile.predvsactual.pdf\")\n";
	$rscript .= "pe <- predict(fit,m)\n";
	$rscript .= "slope <- as.numeric(lm($resp ~ pe)[[1]][2]);\n";
	$rscript .= "inter <- as.numeric(lm($resp ~ pe)[[1]][1]);\n";
	$rscript .= "plot(pe, $resp, xlim=c(min(pe),max(pe)), ylim=c(0,max(pe)), pch=\"+\")\n";
	$rscript .= "abline(b=slope, a=inter, lwd=5, col=\"red\")\n";
	$rscript .= "dev.off()\n";
	
	if (defined($knockdown)) {
		$rscript .= "km <- read.csv(\"$knockdown\", header=T, row.names=1, sep=\"\\t\", check.names=T)\n";	
		$rscript .= "pk <- predict(fit, data.frame(km))\n"; 
		$rscript .= "de <- as.matrix(pk - pe)\n";  # NO log(real) - log(knockdown) = log(real/knockdown)
		$rscript .= "rownames(de) <- rownames(m)\n";
		$rscript .= "write.table(de, file=\"$knockdown.diff.txt\", sep=\"\t\", quote=F, row.names=T, col.names=NA)\n";
		$rscript .= "pdf(file=\"$knockdown.kdvsactual.pdf\")\n";
		$rscript .= "plot(pk, pe, xlim=c(min(pk),max(pk)), ylim=c(0,max(pk)), pch=\"+\")\n";
		$rscript .= "dev.off()\n";	  
	}
	
	
	if ($verbose == 1) {
		print "$rscript\n";
	}
	
	open OUT, ">tmpscript.R";
	print OUT "$rscript";
	close OUT;
	
	my $tmp = undef;
	open OUT, "| R --slave > tmp";
	print OUT $rscript;
	close OUT;
	
	if ($verbose == 1) {
		system("cat tmp");
	}
	
	#
	# print output of regression
	#
	open IN, "tmp" or die "Cannot open tmp\n";
	open STATS,  ">$evec1.stats.txt";
	open STATS2, ">$evec1.stats.simple.txt";
	
	$l = undef;
	do {
		$l = <IN>;
		print STATS "$l";
	} while ($l !~ /Intercept/);
	
	while ($l = <IN>) {
		print STATS "$l";
		chomp $l;
		my @a = split /\ +/, $l;
		if (($a[0] =~ /PC/) || ($a[0] =~ /MIMA/) || (Sets::in_array($a[0], @terms))) {
			
			
			#printf "$a[0]\t%3.2f\t%3.2f", $a[1], $a[3];
			printf STATS2 "$a[0]\t%3.2f\t%3.2f", $a[1], $a[3];
			if ($a[4] eq "<") {
				#print "\t$a[4]$a[5]\n";
				print STATS2 "\t$a[4]$a[5]\n"; 
			} else {
				#print "\t$a[4]\n";
				print STATS2 "\t$a[4]\n";
			}
		}
		if($l =~ /^Residual standard error/ || $l =~ /^Multiple R-squared/ || $l =~ /^F-statistic/) {
			chomp $l;
			#print "$l\n";
		}
	}
	
	close STATS;
	close STATS2;
	close IN;
	
}

#
# Run regression for model1 and model2, and anova for the models
#
else {
	if(defined($evec2)) {
		open IN, $evec2;
		my $l = <IN>; chomp $l;
		@datalabels2 = split /\t/, $l, -1;
		shift @datalabels2;
		close IN;
		
		# synchronize
		my $outfile2 = "$evec2.$f";
		my $todo = "perl $ENV{PERLSCRIPTSDIR}/expression_add_one_matrix_to_another.pl $evec2 $expfile > $outfile2";
		system($todo) == 0 or die "Cannot exec $todo\n";
		
		if (!defined($model2)) {
			$model2  =  join(" + ", @datalabels2) . " $add2model";
		}
		
		#
		# make the R script
		#
		
		# read model 1
		my $rscript = "
		m <- read.csv(\"$outfile\", header=T, row.names=1, sep=\"\\t\", check.names=T)
		attach(data.frame(m))\n";
		
		# run regression for model1
		$rscript .= "fit <- lm($resp ~ $model1)\n";
		$rscript .= "print(summary(fit))\n";
		
		# read model 2
		$rscript .= "
		m2 <- read.csv(\"$outfile2\", header=T, row.names=1, sep=\"\\t\", check.names=T)
		attach(data.frame(m2))\n";
		
		# run regression for model 2
		$rscript .= "fit2 <- lm($resp ~ $model2)\n";
		$rscript .= "print(summary(fit2))\n";
		
		# run anova for model1 and model2
		$rscript .= "anova(fit, fit2)\n";
		
		if ($verbose == 1) {
			print "$rscript\n";
		}
		my $tmp = undef;
		open OUT, "| R --slave > tmp";
		print OUT $rscript;
		close OUT;
		
		if ($verbose == 1) {
			system("cat tmp");
		}
		
		#
		# print output of regression and anova
		#
		open IN, "tmp" or die "Cannot open tmp\n";
		open STATS, ">$evec1.stats.txt";
		
		$l = undef;
		do {
			$l = <IN>;
			print STATS "$l";
		} while ($l !~ /Intercept/);
		
		my $an	= 0;
		my $R2	= 0;
		my $F	= 0;
		my $PC1	= 0;
		while ($l = <IN>) {
			
			#print STATS "$l";
			
			# if the line starts with PC or MIMA (for miRNA)
			if($F eq "0") {
				if(($l =~ /^PC/) || ($l =~ /MIMA/)) {
					print STATS "$l";
					
					chomp $l;
					my @a = split /\ +/, $l;
					printf "$a[0]\t%3.2f\t%3.2f", $a[1], $a[3];
					if ($a[4] eq "<") {
						print "\t$a[4]$a[5]\n";
					} else {
						print "\t$a[4]\n";
					}
				}
			}
			
			if($R2 eq "0" || $F eq "0") {
				if($l =~ /^Residual standard error/) {
					print STATS "$l";
					
					chomp $l;
					print "$l\n";
				}
			}
			if($R2 eq "0") {
				if($l =~ /^Multiple R-squared/) {
					$R2 = 1;
					print STATS "$l";
					
					chomp $l;
					print "$l\n";
				}
			}
			if($F eq "0") {
				if($l =~ /^F-statistic/) {
					$F = 1;
					print STATS "$l";
					
					chomp $l;
					print "$l\n";
				}
			}
			
			# if you have found the first anova line print all lines that follow
			if($an) {
				print STATS "$l";
				
				chomp $l;
				print "$l\n";
			}
			
			# if you find the first line of anova
			if($l =~ /^Analysis/) {
				$an=1;
				print STATS "\n$l";
				
				chomp $l;
				print "\n$l\n";
			}
		}
	} else {
		print "Cannot perform anova model comparison.\n";
	}
}