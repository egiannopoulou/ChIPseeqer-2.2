#!/usr/bin/perl
use strict;

use Getopt::Long;

# variables to store options
my $matrix		= undef;
my $response	= "FPKM";

my $start_k		= 2;
my $end_k		= 20;
my $i			= undef;

my $current_matrix = undef;
my $call_RF_script = undef;
my $call_LM_script = undef;

my $runLM		= 1;
my $runRF		= 1;

my $verbose		= 0;

# handling of missing arguments
if (@ARGV == 0) {
	die "RandomForestCall.pl
	Required:
	--matrix		= FILE
	--response		= \"log\"(FPKM+1)\"\"
	Optional:
	--start_k		= INT
	--end_k			= INT
	--runLM			= INT (1 to run Linear Regression)
	--runRF			= INT (1 to run Random Forest)\n";
}

GetOptions("matrix=s"		=> \$matrix,
"response=s"	=> \$response,
"start_k=s"		=> \$start_k,
"end_k=s"		=> \$end_k,
"runLM=s"		=> \$runLM,
"runRF=s"		=> \$runRF,
"verbose=s"		=> \$verbose);

if (!defined($matrix) || !defined($response)) {
	die "RandomForestCall.pl
	Required:
	--matrix		= FILE
	--response		= \"log\"(FPKM+1)\"\"
	Optional:
	--start_k		= INT
	--end_k			= INT
	--runLM			= INT (1 to run Linear Regression)
	--runRF			= INT (1 to run Random Forest)\n";
}

for($i=$start_k; $i<=$end_k; $i++) {
		
	if($runRF == 1) {
		$current_matrix = "$matrix.GM12878.res.$i.offset.random.basis.txt.GM12878_FPKM.txt.noNM";

		print "$current_matrix\n";
		
		$call_RF_script = "RandomForestCall.pl --matrix=$current_matrix";	
		system($call_RF_script) == 0 or die "Cannot exec $call_RF_script\n";
	}
	
	if($runLM == 1) {
		$current_matrix = "$matrix.LY1.res.$i.offset.random.basis.txt.H1_FPKM.txt.noNM";

		print "$current_matrix\n";
		
		$call_LM_script = "LinearRegressionCall.pl --matrix=$current_matrix";	
		system($call_LM_script) == 0 or die "Cannot exec $call_LM_script\n";
	}
}

