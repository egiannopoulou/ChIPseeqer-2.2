#!/usr/bin/perl

use lib "$ENV{CHIPPCADIR}";

use Getopt::Long;
use strict;

# variables to store options
my $coreTFFile		= undef;

# other variables
my $idx				= undef;
my $coredir			= undef;
my %LABELS			= ();

# handling of missing arguments
if (@ARGV == 0) {
	die "Usage: MergeReplicatePeaks.pl --coreTFFile=FILE\n";
}

# handling given options
GetOptions("coreTFFile=s"		=> \$coreTFFile);

if (!defined($coreTFFile)) {
	die("Must provide --coreTFFile=FILE\n");
}

if (defined($coreTFFile)) {
	# get coreTF directory
	$idx	= rindex($coreTFFile, "/");
	$coredir = substr $coreTFFile, 0, $idx+1;  
}


if(defined($coreTFFile)) {
	
	# Sort file first
	
	my $sort = "sort -k 2 $coreTFFile > $coreTFFile.sorted.txt";
	system($sort) == 0 or die "Cannot exec $sort\n";
	
	# open coreTFFile
	open(IN, "$coreTFFile.sorted.txt") or die "Can't open file $coreTFFile.sorted.txt.";
	open OUT, ">$coreTFFile.new";
	
	while (my $line = <IN>) {
		
		chomp $line;
				
		my @a = split /\t/, $line, -1;
		
		my $peakfile	= "$a[0]";
		my $label		= "$a[1]";

		if(exists $LABELS{$label}) {
			my $concat = "cat $peakfile >> $LABELS{$label}";
			system($concat) == 0 or die "Cannot exec $concat\n";
			
			my $mv = "mv $peakfile $peakfile.old";
			system($mv) == 0 or die "Cannot exec $mv\n";
		}
		else {
			$LABELS{$label} = $peakfile;
			print OUT "$peakfile\t$label\n";
		}
		
	}
	close IN;
	close OUT;
}