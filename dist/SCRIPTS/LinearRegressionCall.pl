#!/usr/bin/perl
use strict;

use Getopt::Long;

# variables to store options
my $matrix		= undef;

my $rscript		= undef;
my $verbose		= 0;

# handling of missing arguments
if (@ARGV == 0) {
	die "LinearRegressionCall.pl
	Required:
	--matrix		= FILE\n";
}

GetOptions("matrix=s"		=> \$matrix,
"verbose=s"		=> \$verbose);

if (!defined($matrix)) {
	die "LinearRegressionCall.pl
	Required:
	--matrix		= FILE\n";
}

#
# Step 1: Load libraries and data
#

$rscript = "

#load libraries
#library(foreach)
#library(doMC)
#library(randomForest)

#load data
nm <- read.csv(\"$matrix\", header=T, row.names=1, sep=\"\\t\", check.names=T)
attach(data.frame(nm))
\n";

#
# Step 2: Prepare train and test sets
#

$rscript .= "

#prepare train and test sets
scores <- nm[1:ncol(nm)-1]
TRAIN = 1:nrow(nm) %% 4 != 0;
TEST = !TRAIN;
\n";

#
# Step 3: Run linear regression
#

$rscript .= "

#run linear regression
reg = lm(log(FPKM[TRAIN]+1) ~ as.matrix(scores[TRAIN,]));
reg_predict = cbind(1, as.matrix(scores[TEST,])) %*% coef(reg);
reg_predictability = cor(reg_predict, log(FPKM[TEST]+1), method = c(\"spearman\"));

#print results
rsq = summary(reg)\$r.squared

summary(reg)

lapply(\"$matrix\", write, \"$matrix.results.lm.txt\", append=TRUE, ncolumns=1000)

lapply(\"R squared\", write, \"$matrix.results.lm.txt\", append=TRUE, ncolumns=1000)
lapply(rsq, write, \"$matrix.results.lm.txt\", append=TRUE, ncolumns=1000)

lapply(\"LR predictability\", write, \"$matrix.results.lm.txt\", append=TRUE, ncolumns=1000)
lapply(reg_predictability, write, \"$matrix.results.lm.txt\", append=TRUE, ncolumns=1000)

slope <- as.numeric(lm((log(FPKM[TEST]+1) ~ reg_predict))[[1]][2])
inter <- as.numeric(lm((log(FPKM[TEST]+1) ~ reg_predict))[[1]][1])

pdf(file=\"$matrix.predvsactual.lm.pdf\")
plot(reg_predict, log(FPKM[TEST]+1), xlim=c(min(reg_predict),max(reg_predict)), ylim=c(0,max(reg_predict)), pch=\"+\")
abline(b=slope, a=inter, lwd=5, col=\"red\")
dev.off()

\n";

open OUT, "> tmpscript.R";
print OUT "$rscript";
close OUT;

open OUT, "| R --slave > $matrix.runLR.R";
print OUT $rscript;
close OUT;

if ($verbose == 1) {
	print "$rscript\n";
	system("cat $matrix.runLR.R");
}

#unlink("$matrix.runLR.R");