#!/usr/bin/perl

use lib "$ENV{CHIPSEEQERDIR}";

use Getopt::Long;

# variables to store the arguments values
my $inputfile		= undef;
my $verbose			= 0;
my $id              = undef;
my $num             = undef;
my $last            = undef;
my $fasta           = undef;

# handling lack of arguments
if (@ARGV == 0) {
	die "Usage: fasta_reverse.pl --input=FILE --verbose=INT \n";
}

# processing command line options
GetOptions("input=s" => \$inputfile,
"verbose=i"		=> \$verbose );

open(IN, "$inputfile") or die "Can't open file $inputfile.";

while (my $line = <IN>) {
    chomp $line;
    if ($line =~ /^>/){
        print "$line\n";
    }
    else {
        my $todo="echo $line | rev";
        system($todo) == 0 or die "Cannot execute $todo.\n";
    }
}

close IN;