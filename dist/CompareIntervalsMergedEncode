#!/usr/bin/perl

use lib "$ENV{CHIPSEEQERDIR}";
use Sets;
use strict;
use Getopt::Long;

my $file1			= undef;
my $file2			= undef;
my $type			= "union";	# Can be union or unique 
my $is_encode1		= 0;		# If set to 1, the first file is ENCODE BroadPeak file (used from CompareIntervalsCollapsePeaks)
my $is_encode2		= 0;		# If set to 1, the second file is ENCODE BroadPeak file (used from CompareIntervalsCollapsePeaks)
my $has_signal1		= 0;		# If set to 1, the first file is a collapsed ENCODE BroadPeak file (used from CompareIntervalsMergedMulti)
my $has_signal2		= 0;		# If set to 1, the second file is a collapsed ENCODE BroadPeak file (used from CompareIntervalsMergedMulti)
my $verbose			= 0;
my $overlap_idx		= 3;		# Position of the number of overlaps column.
								# For regular ChIP-seq output, column 12 should be the result of the overlap.
my $run_idx			= 0;		# This index is set from CompareIntervalsMergedMulti. It is used to fill in with 0s the signal values of previous runs.
my $collapse_peaks	= 0;		# If set to 1, the same peak file is used as peakfile1 and peakfile2. Used to "collapse" all the peaks in one peak file.
my $showsignal2		= 1;
my $fromJaccard		= 0;

my %signal_profiles_hash	= ();
my %peaks_hash				= ();

my $tmpfile1			= undef;
my $tmpfile2			= undef;

# handling of missing arguments
if (@ARGV == 0) {
	die "Usage: CompareIntervalsMergedEncode  --peakfile1=FILE --peakfile2=FILE --type=[union|unique] --collapse_peaks[0|1]\n";
}

# handling given options
GetOptions
("peakfile1=s"		=> \$file1,
"peakfile2=s"		=> \$file2,
"type=s"			=> \$type,
"is_encode1=s"		=> \$is_encode1,
"is_encode2=s"		=> \$is_encode2,
"has_signal1=s"		=> \$has_signal1,
"has_signal2=s"		=> \$has_signal2,
"run_idx=s"			=> \$run_idx,
"collapse_peaks=s"	=> \$collapse_peaks,
"showsignal2=s"		=> \$showsignal2,
"fromJaccard=s"		=> \$fromJaccard,
"verbose=s"			=> \$verbose);

if (! -e $file1) {
	die "$file1 cannot be opened\n";
} 
if (! -e $file2) {
	die "$file2 cannot be opened\n";
}

srand();

my $ran      = int(rand(5782763));  
$tmpfile1 = "tmpfile1.$ran";

if($type eq "union") {
	
	my $todo = "$ENV{CHIPSEEQERDIR}/CompareIntervals -peakfile1 $file1 -peakfile2 $file2 -showunion 1 -show_ov_int 1 ";
	
	if($is_encode1 == 1) {
		$todo .=" -is_encode1 1 ";
	}
	elsif($has_signal1 == 1) {
		$todo .=" -has_signal1 1 ";
	}
	else {
		$todo .=" -showpeakdesc 1 ";
	}
	
	if($is_encode2 == 1) {
		$todo .=" -is_encode2 1 ";
	}
	elsif($has_signal2 == 1) {
		$todo .=" -has_signal2 1 ";
	}
	else {
		$todo .=" -showsignal2 $showsignal2 ";
	}
	
	$todo .=" > $tmpfile1";
	
	if($verbose == 1) {
		print "$todo\n";
	}
	
	system($todo) == 0 or die "Cannot exec $todo\n";
	
	open IN, $tmpfile1;
	
	while (my $l = <IN>) {
		chomp $l; 
		my @c = (split /\t/, $l); 
		
		# If the first file is the result of previous run, then field=4 has the signal_profiles (e.g., 547.130005;188.820007;920.549988;0) 
		#if($is_encode1 == 0) {
		#	$overlap_idx = 4;
		#}
		
		# This is the normal run, when the first file is ENCODE (used from CompareIntervalsCollapsePeaks)
		if($is_encode1 == 1) {
			if($collapse_peaks == 1) {
				# print union peak (last one in the line)
				my $last	= (split /\t/, $l)[-1]; 
				my @d		= (split /-/, $last);
				print "$d[0]\t$d[1]\t$d[2]\t$d[3]\n";
			}
		}
		elsif($has_signal1 == 1 && $has_signal2 == 1) {
			
			# Print union peak (last one in the line)
			my $last	= (split /\t/, $l)[-1]; 
			my @d		= (split /-/, $last);
			
			print "$d[0]\t$d[1]\t$d[2]";
			
			# If peak1 didn't overlap with any peak2
			if ($c[$overlap_idx] == 0) {
				print "\t$c[$overlap_idx+1];0";
			} 
			# Else it prints the union peak
			elsif ($c[$overlap_idx] >= 1) {
				print "\t$c[$overlap_idx+1];$d[3]";
			}
			print "\n";
		}
		elsif($has_signal1 == 0 && $has_signal2 == 1) {
			$overlap_idx = 4;
			
			# Print union peak (last one in the line)
			my $last	= (split /\t/, $l)[-1]; 
			my @d		= (split /-/, $last);
			
			print "$d[0]\t$d[1]\t$d[2]";
			
			# If peak1 didn't overlap with any peak2
			if ($c[$overlap_idx] == 0) {
				print "\t$c[$overlap_idx-1];0";
			} 
			# Else it prints the union peak
			elsif ($c[$overlap_idx] >= 1) {
				print "\t$c[$overlap_idx-1];$d[3]";
			}
			
			print "\n";
		}
		# We're in the final step in CompareIntervalsMergedMulti
		else {			
			$overlap_idx = 4;
			
			if ($fromJaccard == 1) {
				# print peak
				print "$c[0]\t$c[1]\t$c[2]\n";
			}
			elsif($c[$overlap_idx] == 1) {
				
				# print peak
				print "$c[0]\t$c[1]\t$c[2]\t";
				
				# replace ; with \t
				$c[$overlap_idx-1] =~ s/;/\t/g;
				
				# print signal values
				print "$c[$overlap_idx-1]\n";
			}
			else {
				# print union peak (last one in the line)					
				my @line	= (split /\t/, $l); 
				my $last	= pop(@line);
				my @d		= (split /-/, $last);
				
				if (!exists $peaks_hash{$last}) {
					$peaks_hash{$last}++;
					
					print "$d[0]\t$d[1]\t$d[2]";
					
					# number of overlaps
					my $cnt = $c[$overlap_idx];
					
					my @arr;
					
					# get the signal values for all the overlapping peaks
					for(my $i=$overlap_idx+1; $i<=$#line; $i++) {
						my $signal_profile	= (split /-/, $line[$i])[-1];
						my @signals			= (split /;/, $signal_profile);
						
						push @arr, [ @signals ];
					}
					
					my @sum	= 0;
					
					# estimates and print sthe average signal value, per column (in case some peaks were merged into the union peak)
					for my $i ( 0 .. $#arr ) {
						my $aref	= $arr[$i];
						my $n		= @$aref - 1;
						
						for my $j ( 0 .. $n ) {
							$sum[$j] += $arr[$i][$j];
							#print "[ij]: $i $j is $arr[$i][$j], sum at $j: $sum[$j]\n";
						}			
					}
					
					for my $j ( 0 .. $#{$arr[0]} ) {
						my $avg = $sum[$j]/$cnt;
						print "\t$avg";
					}
					print "\n";
				}
			}
			
		}
	}
	
	close IN;
}
elsif($type eq "unique") {
	
	my $todo = "$ENV{CHIPSEEQERDIR}/CompareIntervals -peakfile1 $file1 -peakfile2 $file2 > $tmpfile1";
	
	if($verbose == 1) {
		print "$todo\n";
	}
	
	system($todo) == 0 or die "Cannot exec $todo\n";
	
	open IN, $tmpfile1;
	
	while (my $l = <IN>) {
		chomp $l; 
		my @c = (split /\t/, $l); 
		
		if ($c[3] == 0) {
			print "$c[0]\t$c[1]\t$c[2]\n";
		}
	}
	
	close IN;
}

if($collapse_peaks != 1) {
	#the inverse compare intervals
	srand (time);     
	
	my $ran      = int(rand(5782763)); # generate random suffix   
	$tmpfile2 = "tmpfile2.$ran";   
	
	my $todo = "$ENV{CHIPSEEQERDIR}/CompareIntervals -peakfile1 $file2 -peakfile2 $file1 -showunion 1 -show_ov_int 1 ";
	
	if($has_signal1 == 0) {
		$todo .=" -showpeakdesc 1 ";
	}
	
	if($has_signal2 == 1) {
		$todo .=" -showsignal2 1 ";
	}
	
	$todo .=" > $tmpfile2";
	
	
	if($verbose == 1) {
		print "$todo\n";
	}
	
	system($todo) == 0 or die "Cannot exec $todo\n";
	
	open IN, $tmpfile2;
	
	while (my $l = <IN>) {
		chomp $l; 
		my @c = (split /\t/, $l); 
		
		#if($is_encode2 != 0) {
		$overlap_idx = 4;
		
		if ($c[$overlap_idx] == 0) {
			print "$c[0]\t$c[1]\t$c[2]";
			
			if($has_signal2 == 1) {
				
				print "\t";
				
				for(my $j=0; $j<$run_idx-2; $j++) {
					print "0;";
				}
				
				print "0;$c[$overlap_idx-1]";
			}
			print "\n";
		}
	}
	
	close IN;
}

unlink $tmpfile1;
unlink $tmpfile2;